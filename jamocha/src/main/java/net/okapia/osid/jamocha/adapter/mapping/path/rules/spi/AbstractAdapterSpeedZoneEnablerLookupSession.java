//
// AbstractAdapterSpeedZoneEnablerLookupSession.java
//
//    A SpeedZoneEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SpeedZoneEnabler lookup session adapter.
 */

public abstract class AbstractAdapterSpeedZoneEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession {

    private final org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSpeedZoneEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSpeedZoneEnablerLookupSession(org.osid.mapping.path.rules.SpeedZoneEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Map/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.session.getMapId());
    }


    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMap());
    }


    /**
     *  Tests if this user can perform {@code SpeedZoneEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSpeedZoneEnablers() {
        return (this.session.canLookupSpeedZoneEnablers());
    }


    /**
     *  A complete view of the {@code SpeedZoneEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSpeedZoneEnablerView() {
        this.session.useComparativeSpeedZoneEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code SpeedZoneEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySpeedZoneEnablerView() {
        this.session.usePlenarySpeedZoneEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include speed zone enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.session.useFederatedMapView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.session.useIsolatedMapView();
        return;
    }
    

    /**
     *  Only active speed zone enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSpeedZoneEnablerView() {
        this.session.useActiveSpeedZoneEnablerView();
        return;
    }


    /**
     *  Active and inactive speed zone enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSpeedZoneEnablerView() {
        this.session.useAnyStatusSpeedZoneEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code SpeedZoneEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code SpeedZoneEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code SpeedZoneEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param speedZoneEnablerId {@code Id} of the {@code SpeedZoneEnabler}
     *  @return the speed zone enabler
     *  @throws org.osid.NotFoundException {@code speedZoneEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code speedZoneEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnabler getSpeedZoneEnabler(org.osid.id.Id speedZoneEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZoneEnabler(speedZoneEnablerId));
    }


    /**
     *  Gets a {@code SpeedZoneEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  speedZoneEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code SpeedZoneEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code SpeedZoneEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByIds(org.osid.id.IdList speedZoneEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZoneEnablersByIds(speedZoneEnablerIds));
    }


    /**
     *  Gets a {@code SpeedZoneEnablerList} corresponding to the given
     *  speed zone enabler genus {@code Type} which does not include
     *  speed zone enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerGenusType a speedZoneEnabler genus type 
     *  @return the returned {@code SpeedZoneEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByGenusType(org.osid.type.Type speedZoneEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZoneEnablersByGenusType(speedZoneEnablerGenusType));
    }


    /**
     *  Gets a {@code SpeedZoneEnablerList} corresponding to the given
     *  speed zone enabler genus {@code Type} and include any additional
     *  speed zone enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerGenusType a speedZoneEnabler genus type 
     *  @return the returned {@code SpeedZoneEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByParentGenusType(org.osid.type.Type speedZoneEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZoneEnablersByParentGenusType(speedZoneEnablerGenusType));
    }


    /**
     *  Gets a {@code SpeedZoneEnablerList} containing the given
     *  speed zone enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  speedZoneEnablerRecordType a speedZoneEnabler record type 
     *  @return the returned {@code SpeedZoneEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code speedZoneEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersByRecordType(org.osid.type.Type speedZoneEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZoneEnablersByRecordType(speedZoneEnablerRecordType));
    }


    /**
     *  Gets a {@code SpeedZoneEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible
     *  through this session.
     *  
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code SpeedZoneEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZoneEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code SpeedZoneEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible
     *  through this session.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code SpeedZoneEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getSpeedZoneEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code SpeedZoneEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  speed zone enablers or an error results. Otherwise, the returned list
     *  may contain only those speed zone enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, speed zone enablers are returned that are currently
     *  active. In any status mode, active and inactive speed zone enablers
     *  are returned.
     *
     *  @return a list of {@code SpeedZoneEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.SpeedZoneEnablerList getSpeedZoneEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSpeedZoneEnablers());
    }
}
