//
// AbstractImmutableQueueProcessor.java
//
//     Wraps a mutable QueueProcessor to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.queueprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>QueueProcessor</code> to hide modifiers. This
 *  wrapper provides an immutized QueueProcessor from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying queueProcessor whose state changes are visible.
 */

public abstract class AbstractImmutableQueueProcessor
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidProcessor
    implements org.osid.provisioning.rules.QueueProcessor {

    private final org.osid.provisioning.rules.QueueProcessor queueProcessor;


    /**
     *  Constructs a new <code>AbstractImmutableQueueProcessor</code>.
     *
     *  @param queueProcessor the queue processor to immutablize
     *  @throws org.osid.NullArgumentException <code>queueProcessor</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableQueueProcessor(org.osid.provisioning.rules.QueueProcessor queueProcessor) {
        super(queueProcessor);
        this.queueProcessor = queueProcessor;
        return;
    }


    /**
     *  Tests if the processing of the queue is automatic. 
     *
     *  @return <code> true </code> if the queue processing is automatic, 
     *          <code> false </code> if processed manually 
     */

    @OSID @Override
    public boolean isAutomatic() {
        return (this.queueProcessor.isAutomatic());
    }


    /**
     *  Tests if this queue is first in first out. 
     *
     *  @return <code> true </code> if the queue is a fifo, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isFifo() {
        return (this.queueProcessor.isFifo());
    }


    /**
     *  Tests if processed queue entries are removed. 
     *
     *  @return <code> true </code> if the processed entries are removed, 
     *          <code> false </code> if processed queue entries remain but 
     *          marked ineffective 
     */

    @OSID @Override
    public boolean removesProcessedQueueEntries() {
        return (this.queueProcessor.removesProcessedQueueEntries());
    }


    /**
     *  Gets the queue processor record corresponding to the given <code> 
     *  QueueProcessor </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  queueProcessorRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(queueProcessorRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  queueProcessorRecordType the type of queue processor record to 
     *          retrieve 
     *  @return the queue processor record 
     *  @throws org.osid.NullArgumentException <code> queueProcessorRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(queueProcessorRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueProcessorRecord getQueueProcessorRecord(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException {

        return (this.queueProcessor.getQueueProcessorRecord(queueProcessorRecordType));
    }
}

