//
// AbstractDemographicSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDemographicSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resource.demographic.DemographicSearchResults {

    private org.osid.resource.demographic.DemographicList demographics;
    private final org.osid.resource.demographic.DemographicQueryInspector inspector;
    private final java.util.Collection<org.osid.resource.demographic.records.DemographicSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDemographicSearchResults.
     *
     *  @param demographics the result set
     *  @param demographicQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>demographics</code>
     *          or <code>demographicQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDemographicSearchResults(org.osid.resource.demographic.DemographicList demographics,
                                            org.osid.resource.demographic.DemographicQueryInspector demographicQueryInspector) {
        nullarg(demographics, "demographics");
        nullarg(demographicQueryInspector, "demographic query inspectpr");

        this.demographics = demographics;
        this.inspector = demographicQueryInspector;

        return;
    }


    /**
     *  Gets the demographic list resulting from a search.
     *
     *  @return a demographic list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicList getDemographics() {
        if (this.demographics == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resource.demographic.DemographicList demographics = this.demographics;
        this.demographics = null;
	return (demographics);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resource.demographic.DemographicQueryInspector getDemographicQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  demographic search record <code> Type. </code> This method must
     *  be used to retrieve a demographic implementing the requested
     *  record.
     *
     *  @param demographicSearchRecordType a demographic search 
     *         record type 
     *  @return the demographic search
     *  @throws org.osid.NullArgumentException
     *          <code>demographicSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(demographicSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicSearchResultsRecord getDemographicSearchResultsRecord(org.osid.type.Type demographicSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resource.demographic.records.DemographicSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(demographicSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(demographicSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record demographic search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDemographicRecord(org.osid.resource.demographic.records.DemographicSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "demographic record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
