//
// AbstractIndexedMapEngineLookupSession.java
//
//    A simple framework for providing an Engine lookup service
//    backed by a fixed collection of engines with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.search.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Engine lookup service backed by a
 *  fixed collection of engines. The engines are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some engines may be compatible
 *  with more types than are indicated through these engine
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Engines</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapEngineLookupSession
    extends AbstractMapEngineLookupSession
    implements org.osid.search.EngineLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.search.Engine> enginesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.search.Engine>());
    private final MultiMap<org.osid.type.Type, org.osid.search.Engine> enginesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.search.Engine>());


    /**
     *  Makes an <code>Engine</code> available in this session.
     *
     *  @param  engine an engine
     *  @throws org.osid.NullArgumentException <code>engine<code> is
     *          <code>null</code>
     */

    @Override
    protected void putEngine(org.osid.search.Engine engine) {
        super.putEngine(engine);

        this.enginesByGenus.put(engine.getGenusType(), engine);
        
        try (org.osid.type.TypeList types = engine.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.enginesByRecord.put(types.getNextType(), engine);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an engine from this session.
     *
     *  @param engineId the <code>Id</code> of the engine
     *  @throws org.osid.NullArgumentException <code>engineId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeEngine(org.osid.id.Id engineId) {
        org.osid.search.Engine engine;
        try {
            engine = getEngine(engineId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.enginesByGenus.remove(engine.getGenusType());

        try (org.osid.type.TypeList types = engine.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.enginesByRecord.remove(types.getNextType(), engine);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeEngine(engineId);
        return;
    }


    /**
     *  Gets an <code>EngineList</code> corresponding to the given
     *  engine genus <code>Type</code> which does not include
     *  engines of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known engines or an error results. Otherwise,
     *  the returned list may contain only those engines that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  engineGenusType an engine genus type 
     *  @return the returned <code>Engine</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>engineGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.EngineList getEnginesByGenusType(org.osid.type.Type engineGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.search.engine.ArrayEngineList(this.enginesByGenus.get(engineGenusType)));
    }


    /**
     *  Gets an <code>EngineList</code> containing the given
     *  engine record <code>Type</code>. In plenary mode, the
     *  returned list contains all known engines or an error
     *  results. Otherwise, the returned list may contain only those
     *  engines that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  engineRecordType an engine record type 
     *  @return the returned <code>engine</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>engineRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.EngineList getEnginesByRecordType(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.search.engine.ArrayEngineList(this.enginesByRecord.get(engineRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.enginesByGenus.clear();
        this.enginesByRecord.clear();

        super.close();

        return;
    }
}
