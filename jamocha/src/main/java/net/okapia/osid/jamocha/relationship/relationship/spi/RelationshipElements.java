//
// RelationshipElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.relationship.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RelationshipElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the RelationshipElement Id.
     *
     *  @return the relationship element Id
     */

    public static org.osid.id.Id getRelationshipEntityId() {
        return (makeEntityId("osid.relationship.Relationship"));
    }


    /**
     *  Gets the SourceId element Id.
     *
     *  @return the SourceId element Id
     */

    public static org.osid.id.Id getSourceId() {
        return (makeElementId("osid.relationship.relationship.SourceId"));
    }


    /**
     *  Gets the DestinationId element Id.
     *
     *  @return the DestinationId element Id
     */

    public static org.osid.id.Id getDestinationId() {
        return (makeElementId("osid.relationship.relationship.DestinationId"));
    }


    /**
     *  Gets the SamePeerId element Id.
     *
     *  @return the SamePeerId element Id
     */

    public static org.osid.id.Id getSamePeerId() {
        return (makeQueryElementId("osid.relationship.relationship.SamePeerId"));
    }


    /**
     *  Gets the FamilyId element Id.
     *
     *  @return the FamilyId element Id
     */

    public static org.osid.id.Id getFamilyId() {
        return (makeQueryElementId("osid.relationship.relationship.FamilyId"));
    }


    /**
     *  Gets the Family element Id.
     *
     *  @return the Family element Id
     */

    public static org.osid.id.Id getFamily() {
        return (makeQueryElementId("osid.relationship.relationship.Family"));
    }


    /**
     *  Gets the Source element Id.
     *
     *  @return the Source element Id
     */

    public static org.osid.id.Id getSource() {
        return (makeSearchOrderElementId("osid.relationship.relationship.Source"));
    }


    /**
     *  Gets the Destination element Id.
     *
     *  @return the Destination element Id
     */

    public static org.osid.id.Id getDestination() {
        return (makeSearchOrderElementId("osid.relationship.relationship.Destination"));
    }
}
