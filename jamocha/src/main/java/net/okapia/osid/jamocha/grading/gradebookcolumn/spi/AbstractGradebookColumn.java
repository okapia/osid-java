//
// AbstractGradebookColumn.java
//
//     Defines a GradebookColumn.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumn.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>GradebookColumn</code>.
 */

public abstract class AbstractGradebookColumn
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.grading.GradebookColumn {

    private org.osid.grading.GradeSystem gradeSystem;

    private final java.util.Collection<org.osid.grading.records.GradebookColumnRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> GradeSystem Id </code> in which this grade belongs. 
     *
     *  @return the grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeSystemId() {
        return (this.gradeSystem.getId());
    }


    /**
     *  Gets the <code> GradeSystem </code> in which this grade belongs. 
     *
     *  @return the package grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem()
        throws org.osid.OperationFailedException {

        return (this.gradeSystem);
    }


    /**
     *  Sets the grade system.
     *
     *  @param system a grade system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    protected void setGradeSystem(org.osid.grading.GradeSystem system) {
        nullarg(system, "grade system");
        this.gradeSystem = system;
        return;
    }


    /**
     *  Tests if this gradebookColumn supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradebookColumnRecordType a gradebook column record type 
     *  @return <code>true</code> if the gradebookColumnRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookColumnRecordType) {
        for (org.osid.grading.records.GradebookColumnRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>GradebookColumn</code> record <code>Type</code>.
     *
     *  @param  gradebookColumnRecordType the gradebook column record type 
     *  @return the gradebook column record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnRecord getGradebookColumnRecord(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook column. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookColumnRecord the gradebook column record
     *  @param gradebookColumnRecordType gradebook column record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecord</code> or
     *          <code>gradebookColumnRecordTypegradebookColumn</code> is
     *          <code>null</code>
     */
            
    protected void addGradebookColumnRecord(org.osid.grading.records.GradebookColumnRecord gradebookColumnRecord, 
                                            org.osid.type.Type gradebookColumnRecordType) {
        
        nullarg(gradebookColumnRecord, "gradebook column record");
        addRecordType(gradebookColumnRecordType);
        this.records.add(gradebookColumnRecord);
        
        return;
    }
}
