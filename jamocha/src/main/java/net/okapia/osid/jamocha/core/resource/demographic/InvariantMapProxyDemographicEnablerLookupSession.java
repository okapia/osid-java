//
// InvariantMapProxyDemographicEnablerLookupSession
//
//    Implements a DemographicEnabler lookup service backed by a fixed
//    collection of demographicEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic;


/**
 *  Implements a DemographicEnabler lookup service backed by a fixed
 *  collection of demographic enablers. The demographic enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyDemographicEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resource.demographic.spi.AbstractMapDemographicEnablerLookupSession
    implements org.osid.resource.demographic.DemographicEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDemographicEnablerLookupSession} with no
     *  demographic enablers.
     *
     *  @param bin the bin
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.proxy.Proxy proxy) {
        setBin(bin);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyDemographicEnablerLookupSession} with a single
     *  demographic enabler.
     *
     *  @param bin the bin
     *  @param demographicEnabler a single demographic enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code demographicEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.demographic.DemographicEnabler demographicEnabler, org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putDemographicEnabler(demographicEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDemographicEnablerLookupSession} using
     *  an array of demographic enablers.
     *
     *  @param bin the bin
     *  @param demographicEnablers an array of demographic enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code demographicEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.demographic.DemographicEnabler[] demographicEnablers, org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putDemographicEnablers(demographicEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDemographicEnablerLookupSession} using a
     *  collection of demographic enablers.
     *
     *  @param bin the bin
     *  @param demographicEnablers a collection of demographic enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code demographicEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                  java.util.Collection<? extends org.osid.resource.demographic.DemographicEnabler> demographicEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(bin, proxy);
        putDemographicEnablers(demographicEnablers);
        return;
    }
}
