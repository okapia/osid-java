//
// InquiryMiter.java
//
//     Defines an Inquiry miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inquiry.inquiry;


/**
 *  Defines an <code>Inquiry</code> miter for use with the builders.
 */

public interface InquiryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.inquiry.Inquiry {


    /**
     *  Sets the audit.
     *
     *  @param audit an audit
     *  @throws org.osid.NullArgumentException <code>audit</code> is
     *          <code>null</code>
     */

    public void setAudit(org.osid.inquiry.Audit audit);


    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if not
     *         required
     */

    public void setRequired(boolean required);

    
    /**
     *  Sets the affirmation required flag.
     *
     *  @param required {@code true} if required, {@code false} if not
     *         required
     */

    public void setAffirmationRequired(boolean required);
    

    /**
     *  Sets the needs one response flag.
     *
     *  @param one {@code true} if one response is needed, {@code
     *         false} if a response is always needed
     */

    public void setNeedsOneResponse(boolean one);

    
    /**
     *  Sets the question.
     *
     *  @param question a question
     *  @throws org.osid.NullArgumentException <code>question</code>
     *          is <code>null</code>
     */

    public void setQuestion(org.osid.locale.DisplayText question);


    /**
     *  Adds an Inquiry record.
     *
     *  @param record an inquiry record
     *  @param recordType the type of inquiry record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addInquiryRecord(org.osid.inquiry.records.InquiryRecord record, org.osid.type.Type recordType);
}       


