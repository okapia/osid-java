//
// AbstractPoolConstrainerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPoolConstrainerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.PoolConstrainerEnablerSearchResults {

    private org.osid.provisioning.rules.PoolConstrainerEnablerList poolConstrainerEnablers;
    private final org.osid.provisioning.rules.PoolConstrainerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPoolConstrainerEnablerSearchResults.
     *
     *  @param poolConstrainerEnablers the result set
     *  @param poolConstrainerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnablers</code>
     *          or <code>poolConstrainerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPoolConstrainerEnablerSearchResults(org.osid.provisioning.rules.PoolConstrainerEnablerList poolConstrainerEnablers,
                                            org.osid.provisioning.rules.PoolConstrainerEnablerQueryInspector poolConstrainerEnablerQueryInspector) {
        nullarg(poolConstrainerEnablers, "pool constrainer enablers");
        nullarg(poolConstrainerEnablerQueryInspector, "pool constrainer enabler query inspectpr");

        this.poolConstrainerEnablers = poolConstrainerEnablers;
        this.inspector = poolConstrainerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the pool constrainer enabler list resulting from a search.
     *
     *  @return a pool constrainer enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablers() {
        if (this.poolConstrainerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.PoolConstrainerEnablerList poolConstrainerEnablers = this.poolConstrainerEnablers;
        this.poolConstrainerEnablers = null;
	return (poolConstrainerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.PoolConstrainerEnablerQueryInspector getPoolConstrainerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  pool constrainer enabler search record <code> Type. </code> This method must
     *  be used to retrieve a poolConstrainerEnabler implementing the requested
     *  record.
     *
     *  @param poolConstrainerEnablerSearchRecordType a poolConstrainerEnabler search 
     *         record type 
     *  @return the pool constrainer enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(poolConstrainerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchResultsRecord getPoolConstrainerEnablerSearchResultsRecord(org.osid.type.Type poolConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(poolConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(poolConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record pool constrainer enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPoolConstrainerEnablerRecord(org.osid.provisioning.rules.records.PoolConstrainerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "pool constrainer enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
