//
// AbstractFederatingObjectiveLookupSession.java
//
//     An abstract federating adapter for an ObjectiveLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  ObjectiveLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingObjectiveLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.learning.ObjectiveLookupSession>
    implements org.osid.learning.ObjectiveLookupSession {

    private boolean parallel = false;
    private org.osid.learning.ObjectiveBank objectiveBank = new net.okapia.osid.jamocha.nil.learning.objectivebank.UnknownObjectiveBank();


    /**
     *  Constructs a new <code>AbstractFederatingObjectiveLookupSession</code>.
     */

    protected AbstractFederatingObjectiveLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.learning.ObjectiveLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>ObjectiveBank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>ObjectiveBank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.objectiveBank.getId());
    }


    /**
     *  Gets the <code>ObjectiveBank</code> associated with this 
     *  session.
     *
     *  @return the <code>ObjectiveBank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.objectiveBank);
    }


    /**
     *  Sets the <code>ObjectiveBank</code>.
     *
     *  @param  objectiveBank the objective bank for this session
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code>
     *          is <code>null</code>
     */

    protected void setObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        nullarg(objectiveBank, "objective bank");
        this.objectiveBank = objectiveBank;
        return;
    }


    /**
     *  Tests if this user can perform <code>Objective</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObjectives() {
        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            if (session.canLookupObjectives()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Objective</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObjectiveView() {
        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            session.useComparativeObjectiveView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Objective</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObjectiveView() {
        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            session.usePlenaryObjectiveView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include objectives in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            session.useFederatedObjectiveBankView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            session.useIsolatedObjectiveBankView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Objective</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Objective</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Objective</code> and
     *  retained for compatibility.
     *
     *  @param  objectiveId <code>Id</code> of the
     *          <code>Objective</code>
     *  @return the objective
     *  @throws org.osid.NotFoundException <code>objectiveId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>objectiveId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective(org.osid.id.Id objectiveId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            try {
                return (session.getObjective(objectiveId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(objectiveId + " not found");
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  objectives specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Objectives</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  objectiveIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByIds(org.osid.id.IdList objectiveIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.learning.objective.MutableObjectiveList ret = new net.okapia.osid.jamocha.learning.objective.MutableObjectiveList();

        try (org.osid.id.IdList ids = objectiveIds) {
            while (ids.hasNext()) {
                ret.addObjective(getObjective(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  objective genus <code>Type</code> which does not include
     *  objectives of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.learning.objective.FederatingObjectiveList ret = getObjectiveList();

        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            ret.addObjectiveList(session.getObjectivesByGenusType(objectiveGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ObjectiveList</code> corresponding to the given
     *  objective genus <code>Type</code> and include any additional
     *  objectives with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveGenusType an objective genus type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByParentGenusType(org.osid.type.Type objectiveGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.learning.objective.FederatingObjectiveList ret = getObjectiveList();

        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            ret.addObjectiveList(session.getObjectivesByParentGenusType(objectiveGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ObjectiveList</code> containing the given
     *  objective record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveRecordType an objective record type 
     *  @return the returned <code>Objective</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectivesByRecordType(org.osid.type.Type objectiveRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.learning.objective.FederatingObjectiveList ret = getObjectiveList();

        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            ret.addObjectiveList(session.getObjectivesByRecordType(objectiveRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Objectives</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  objectives or an error results. Otherwise, the returned list
     *  may contain only those objectives that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Objectives</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.learning.objective.FederatingObjectiveList ret = getObjectiveList();

        for (org.osid.learning.ObjectiveLookupSession session : getSessions()) {
            ret.addObjectiveList(session.getObjectives());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.learning.objective.FederatingObjectiveList getObjectiveList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.learning.objective.ParallelObjectiveList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.learning.objective.CompositeObjectiveList());
        }
    }
}
