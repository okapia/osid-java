//
// AbstractQueueSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.queue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractQueueSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.tracking.QueueSearchResults {

    private org.osid.tracking.QueueList queues;
    private final org.osid.tracking.QueueQueryInspector inspector;
    private final java.util.Collection<org.osid.tracking.records.QueueSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractQueueSearchResults.
     *
     *  @param queues the result set
     *  @param queueQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>queues</code>
     *          or <code>queueQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractQueueSearchResults(org.osid.tracking.QueueList queues,
                                            org.osid.tracking.QueueQueryInspector queueQueryInspector) {
        nullarg(queues, "queues");
        nullarg(queueQueryInspector, "queue query inspectpr");

        this.queues = queues;
        this.inspector = queueQueryInspector;

        return;
    }


    /**
     *  Gets the queue list resulting from a search.
     *
     *  @return a queue list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.tracking.QueueList getQueues() {
        if (this.queues == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.tracking.QueueList queues = this.queues;
        this.queues = null;
	return (queues);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.tracking.QueueQueryInspector getQueueQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  queue search record <code> Type. </code> This method must
     *  be used to retrieve a queue implementing the requested
     *  record.
     *
     *  @param queueSearchRecordType a queue search 
     *         record type 
     *  @return the queue search
     *  @throws org.osid.NullArgumentException
     *          <code>queueSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(queueSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.QueueSearchResultsRecord getQueueSearchResultsRecord(org.osid.type.Type queueSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.tracking.records.QueueSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(queueSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(queueSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record queue search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addQueueRecord(org.osid.tracking.records.QueueSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "queue record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
