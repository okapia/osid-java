//
// AbstractQueryAuctionHouseLookupSession.java
//
//    An AuctionHouseQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AuctionHouseQuerySession adapter.
 */

public abstract class AbstractAdapterAuctionHouseQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.bidding.AuctionHouseQuerySession {

    private final org.osid.bidding.AuctionHouseQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAuctionHouseQuerySession.
     *
     *  @param session the underlying auction house query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAuctionHouseQuerySession(org.osid.bidding.AuctionHouseQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeAuctionHouse</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAuctionHouses() {
        return (this.session.canSearchAuctionHouses());
    }

      
    /**
     *  Gets an auction house query. The returned query will not have an
     *  extension query.
     *
     *  @return the auction house query 
     */
      
    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        return (this.session.getAuctionHouseQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  auctionHouseQuery the auction house query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code auctionHouseQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code auctionHouseQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseList getAuctionHousesByQuery(org.osid.bidding.AuctionHouseQuery auctionHouseQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAuctionHousesByQuery(auctionHouseQuery));
    }
}
