//
// AbstractAuthorizationBatchProxyManager.java
//
//     An adapter for a AuthorizationBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authorization.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AuthorizationBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAuthorizationBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.authorization.batch.AuthorizationBatchProxyManager>
    implements org.osid.authorization.batch.AuthorizationBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterAuthorizationBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAuthorizationBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAuthorizationBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAuthorizationBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of authorizations is available. 
     *
     *  @return <code> true </code> if an authorization bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationBatchAdmin() {
        return (getAdapteeManager().supportsAuthorizationBatchAdmin());
    }


    /**
     *  Tests if bulk administration of functions is available. 
     *
     *  @return <code> true </code> if a function bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionBatchAdmin() {
        return (getAdapteeManager().supportsFunctionBatchAdmin());
    }


    /**
     *  Tests if bulk administration of qualifiers is available. 
     *
     *  @return <code> true </code> if a qualifier bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierBatchAdmin() {
        return (getAdapteeManager().supportsQualifierBatchAdmin());
    }


    /**
     *  Tests if bulk administration of vaults is available. 
     *
     *  @return <code> true </code> if a vault bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultBatchAdmin() {
        return (getAdapteeManager().supportsVaultBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  authorization administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchAdminSession getAuthorizationBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  authorization administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchAdminSession getAuthorizationBatchAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationBatchAdminSessionForVault(vaultId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk function 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FunctionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.FunctionBatchAdminSession getFunctionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk function 
     *  administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FunctionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.FunctionBatchAdminSession getFunctionBatchAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFunctionBatchAdminSessionForVault(vaultId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk qualifier 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QualifierBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.QualifierBatchAdminSession getQualifierBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk qualifier 
     *  administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.QualifierBatchAdminSession getQualifierBatchAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQualifierBatchAdminSessionForVault(vaultId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk vault 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VaultBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.VaultBatchAdminSession getVaultBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVaultBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
