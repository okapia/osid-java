//
// AbstractOfferingQuery.java
//
//     A template for making an Offering Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for offerings.
 */

public abstract class AbstractOfferingQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.offering.OfferingQuery {

    private final java.util.Collection<org.osid.offering.records.OfferingQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the canonical unit <code> Id </code> for this query. 
     *
     *  @param  canonicalUnitId a canonica unitl <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCanonicalUnitId(org.osid.id.Id canonicalUnitId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the canonical unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCanonicalUnitIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CanonicalUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for a canonical unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the canonical unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuery getCanonicalUnitQuery() {
        throw new org.osid.UnimplementedException("supportsCanonicalUnitQuery() is false");
    }


    /**
     *  Clears the canonical unit terms. 
     */

    @OSID @Override
    public void clearCanonicalUnitTerms() {
        return;
    }


    /**
     *  Sets the time period <code> Id </code> for this query to match 
     *  offerings that have a related term. 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        return;
    }


    /**
     *  Matches a title. 
     *
     *  @param  title a title 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> title </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> title </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchTitle(String title, org.osid.type.Type stringMatchType, 
                           boolean match) {
        return;
    }


    /**
     *  Matches offerings with any title. 
     *
     *  @param  match <code> true </code> to match offerings with any title, 
     *          <code> false </code> to match offerings with no title 
     */

    @OSID @Override
    public void matchAnyTitle(boolean match) {
        return;
    }


    /**
     *  Clears all title terms. 
     */

    @OSID @Override
    public void clearTitleTerms() {
        return;
    }


    /**
     *  Matches a code. 
     *
     *  @param  code a code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches offerings with any code. 
     *
     *  @param  match <code> true </code> to match offerings with any code, 
     *          <code> false </code> to match offerings with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        return;
    }


    /**
     *  Clears all code terms. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        return;
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResultOptionId(org.osid.id.Id gradeSystemId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResultOptionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getResultOptionQuery() {
        throw new org.osid.UnimplementedException("supportsResultOptionQuery() is false");
    }


    /**
     *  Matches offerings that have any grading option. 
     *
     *  @param  match <code> true </code> to match offerings with any grading 
     *          option, <code> false </code> to match offerings with no 
     *          grading options 
     */

    @OSID @Override
    public void matchAnyResultOption(boolean match) {
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearResultOptionTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match offerings 
     *  that have a sponsor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Matches offerings that have any sponsor. 
     *
     *  @param  match <code> true </code> to match offerings with any sponsor, 
     *          <code> false </code> to match offerings with no sponsors 
     */

    @OSID @Override
    public void matchAnySponsor(boolean match) {
        return;
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Sets the catalogue <code> Id </code> for this query to match offerings 
     *  assigned to catalogues. 
     *
     *  @param  catalogueId a catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        return;
    }


    /**
     *  Clears all catalogue <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears all catalogue terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given offering query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offering implementing the requested record.
     *
     *  @param offeringRecordType an offering record type
     *  @return the offering query record
     *  @throws org.osid.NullArgumentException
     *          <code>offeringRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.OfferingQueryRecord getOfferingQueryRecord(org.osid.type.Type offeringRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.OfferingQueryRecord record : this.records) {
            if (record.implementsRecordType(offeringRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering query. 
     *
     *  @param offeringQueryRecord offering query record
     *  @param offeringRecordType offering record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfferingQueryRecord(org.osid.offering.records.OfferingQueryRecord offeringQueryRecord, 
                                          org.osid.type.Type offeringRecordType) {

        addRecordType(offeringRecordType);
        nullarg(offeringQueryRecord, "offering query record");
        this.records.add(offeringQueryRecord);        
        return;
    }
}
