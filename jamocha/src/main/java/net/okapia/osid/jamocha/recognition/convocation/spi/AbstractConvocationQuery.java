//
// AbstractConvocationQuery.java
//
//     A template for making a Convocation Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.convocation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for convocations.
 */

public abstract class AbstractConvocationQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.recognition.ConvocationQuery {

    private final java.util.Collection<org.osid.recognition.records.ConvocationQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets an award <code> Id. </code> 
     *
     *  @param  awardId an award <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> awardId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAwardId(org.osid.id.Id awardId, boolean match) {
        return;
    }


    /**
     *  Clears the award <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAwardIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AwardQuery </code> is available. 
     *
     *  @return <code> true </code> if an award query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the award query 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuery getAwardQuery() {
        throw new org.osid.UnimplementedException("supportsAwardQuery() is false");
    }


    /**
     *  Matches convocations with any award. 
     *
     *  @param  match <code> true </code> to match convocations with any 
     *          award, <code> false </code> to match convocations with no 
     *          awards 
     */

    @OSID @Override
    public void matchAnyAward(boolean match) {
        return;
    }


    /**
     *  Clears the award terms. 
     */

    @OSID @Override
    public void clearAwardTerms() {
        return;
    }


    /**
     *  Matches the date between the given dates inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches convocations with any date. 
     *
     *  @param  match <code> true </code> to match convocations with any date, 
     *          <code> false </code> to match convocations with no date 
     */

    @OSID @Override
    public void matchAnyDate(boolean match) {
        return;
    }


    /**
     *  Clears the date terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        return;
    }


    /**
     *  Sets a time period <code> Id. </code> 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the time period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Matches any time period. 
     *
     *  @param  match <code> true </code> to match convocations with any time 
     *          period, <code> false </code> to match convocations with no 
     *          time period 
     */

    @OSID @Override
    public void matchAnyTimePeriod(boolean match) {
        return;
    }


    /**
     *  Clears the time period terms. 
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        return;
    }


    /**
     *  Sets the conferral <code> Id </code> for this query. 
     *
     *  @param  conferralId a conferral <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> conferralId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchConferralId(org.osid.id.Id conferralId, boolean match) {
        return;
    }


    /**
     *  Clears the conferral <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearConferralIdTerms() {
        return;
    }


    /**
     *  Tests if a conferral query is available. 
     *
     *  @return <code> true </code> if a conferral query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralQuery() {
        return (false);
    }


    /**
     *  Gets the query for an award. 
     *
     *  @return the conferral query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuery getConferralQuery() {
        throw new org.osid.UnimplementedException("supportsConferralQuery() is false");
    }


    /**
     *  Matches convocations with any conferral. 
     *
     *  @param  match <code> true </code> to match convocations with any 
     *          conferral, <code> false </code> to match convocations with no 
     *          conferrals 
     */

    @OSID @Override
    public void matchAnyConferral(boolean match) {
        return;
    }


    /**
     *  Clears the conferral terms. 
     */

    @OSID @Override
    public void clearConferralTerms() {
        return;
    }


    /**
     *  Sets the award <code> Id </code> for this query to match convocations 
     *  assigned to academies. 
     *
     *  @param  academyId an academy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAcademyId(org.osid.id.Id academyId, boolean match) {
        return;
    }


    /**
     *  Clears the academy <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAcademyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AcademyQuery </code> is available. 
     *
     *  @return <code> true </code> if an academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an academy query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the academy query 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuery getAcademyQuery() {
        throw new org.osid.UnimplementedException("supportsAcademyQuery() is false");
    }


    /**
     *  Clears the academy terms. 
     */

    @OSID @Override
    public void clearAcademyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given convocation query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a convocation implementing the requested record.
     *
     *  @param convocationRecordType a convocation record type
     *  @return the convocation query record
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(convocationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationQueryRecord getConvocationQueryRecord(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConvocationQueryRecord record : this.records) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(convocationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this convocation query. 
     *
     *  @param convocationQueryRecord convocation query record
     *  @param convocationRecordType convocation record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConvocationQueryRecord(org.osid.recognition.records.ConvocationQueryRecord convocationQueryRecord, 
                                          org.osid.type.Type convocationRecordType) {

        addRecordType(convocationRecordType);
        nullarg(convocationQueryRecord, "convocation query record");
        this.records.add(convocationQueryRecord);        
        return;
    }
}
