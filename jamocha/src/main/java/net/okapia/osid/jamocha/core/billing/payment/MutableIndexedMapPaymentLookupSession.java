//
// MutableIndexedMapPaymentLookupSession
//
//    Implements a Payment lookup service backed by a collection of
//    payments indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.payment;


/**
 *  Implements a Payment lookup service backed by a collection of
 *  payments. The payments are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some payments may be compatible
 *  with more types than are indicated through these payment
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of payments can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapPaymentLookupSession
    extends net.okapia.osid.jamocha.core.billing.payment.spi.AbstractIndexedMapPaymentLookupSession
    implements org.osid.billing.payment.PaymentLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapPaymentLookupSession} with no payments.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business}
     *          is {@code null}
     */

      public MutableIndexedMapPaymentLookupSession(org.osid.billing.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPaymentLookupSession} with a
     *  single payment.
     *  
     *  @param business the business
     *  @param  payment a single payment
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payment} is {@code null}
     */

    public MutableIndexedMapPaymentLookupSession(org.osid.billing.Business business,
                                                  org.osid.billing.payment.Payment payment) {
        this(business);
        putPayment(payment);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPaymentLookupSession} using an
     *  array of payments.
     *
     *  @param business the business
     *  @param  payments an array of payments
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payments} is {@code null}
     */

    public MutableIndexedMapPaymentLookupSession(org.osid.billing.Business business,
                                                  org.osid.billing.payment.Payment[] payments) {
        this(business);
        putPayments(payments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPaymentLookupSession} using a
     *  collection of payments.
     *
     *  @param business the business
     *  @param  payments a collection of payments
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code payments} is {@code null}
     */

    public MutableIndexedMapPaymentLookupSession(org.osid.billing.Business business,
                                                  java.util.Collection<? extends org.osid.billing.payment.Payment> payments) {

        this(business);
        putPayments(payments);
        return;
    }
    

    /**
     *  Makes a {@code Payment} available in this session.
     *
     *  @param  payment a payment
     *  @throws org.osid.NullArgumentException {@code payment{@code  is
     *          {@code null}
     */

    @Override
    public void putPayment(org.osid.billing.payment.Payment payment) {
        super.putPayment(payment);
        return;
    }


    /**
     *  Makes an array of payments available in this session.
     *
     *  @param  payments an array of payments
     *  @throws org.osid.NullArgumentException {@code payments{@code 
     *          is {@code null}
     */

    @Override
    public void putPayments(org.osid.billing.payment.Payment[] payments) {
        super.putPayments(payments);
        return;
    }


    /**
     *  Makes collection of payments available in this session.
     *
     *  @param  payments a collection of payments
     *  @throws org.osid.NullArgumentException {@code payment{@code  is
     *          {@code null}
     */

    @Override
    public void putPayments(java.util.Collection<? extends org.osid.billing.payment.Payment> payments) {
        super.putPayments(payments);
        return;
    }


    /**
     *  Removes a Payment from this session.
     *
     *  @param paymentId the {@code Id} of the payment
     *  @throws org.osid.NullArgumentException {@code paymentId{@code  is
     *          {@code null}
     */

    @Override
    public void removePayment(org.osid.id.Id paymentId) {
        super.removePayment(paymentId);
        return;
    }    
}
