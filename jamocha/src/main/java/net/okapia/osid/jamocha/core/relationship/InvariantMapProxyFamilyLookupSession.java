//
// InvariantMapProxyFamilyLookupSession
//
//    Implements a Family lookup service backed by a fixed
//    collection of families. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship;


/**
 *  Implements a Family lookup service backed by a fixed
 *  collection of families. The families are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyFamilyLookupSession
    extends net.okapia.osid.jamocha.core.relationship.spi.AbstractMapFamilyLookupSession
    implements org.osid.relationship.FamilyLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyFamilyLookupSession} with no
     *  families.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyFamilyLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyFamilyLookupSession} with a
     *  single family.
     *
     *  @param family a single family
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code family} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyFamilyLookupSession(org.osid.relationship.Family family, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putFamily(family);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyFamilyLookupSession} using
     *  an array of families.
     *
     *  @param families an array of families
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code families} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyFamilyLookupSession(org.osid.relationship.Family[] families, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putFamilies(families);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyFamilyLookupSession} using a
     *  collection of families.
     *
     *  @param families a collection of families
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code families} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyFamilyLookupSession(java.util.Collection<? extends org.osid.relationship.Family> families,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putFamilies(families);
        return;
    }
}
