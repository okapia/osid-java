//
// AbstractStockLookupSession.java
//
//    A starter implementation framework for providing a Stock
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Stock
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getStocks(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractStockLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.inventory.StockLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();
    

    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Stock</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStocks() {
        return (true);
    }


    /**
     *  A complete view of the <code>Stock</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStockView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Stock</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStockView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include stocks in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Stock</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Stock</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Stock</code> and retained for
     *  compatibility.
     *
     *  @param  stockId <code>Id</code> of the
     *          <code>Stock</code>
     *  @return the stock
     *  @throws org.osid.NotFoundException <code>stockId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stockId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.inventory.StockList stocks = getStocks()) {
            while (stocks.hasNext()) {
                org.osid.inventory.Stock stock = stocks.getNextStock();
                if (stock.getId().equals(stockId)) {
                    return (stock);
                }
            }
        } 

        throw new org.osid.NotFoundException(stockId + " not found");
    }


    /**
     *  Gets a <code>StockList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the stocks
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Stocks</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getStocks()</code>.
     *
     *  @param  stockIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>stockIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByIds(org.osid.id.IdList stockIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.inventory.Stock> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = stockIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getStock(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("stock " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.inventory.stock.LinkedStockList(ret));
    }


    /**
     *  Gets a <code>StockList</code> corresponding to the given
     *  stock genus <code>Type</code> which does not include
     *  stocks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getStocks()</code>.
     *
     *  @param  stockGenusType a stock genus type 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stockGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByGenusType(org.osid.type.Type stockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.stock.StockGenusFilterList(getStocks(), stockGenusType));
    }


    /**
     *  Gets a <code>StockList</code> corresponding to the given
     *  stock genus <code>Type</code> and include any additional
     *  stocks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStocks()</code>.
     *
     *  @param  stockGenusType a stock genus type 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stockGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByParentGenusType(org.osid.type.Type stockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStocksByGenusType(stockGenusType));
    }


    /**
     *  Gets a <code>StockList</code> containing the given
     *  stock record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStocks()</code>.
     *
     *  @param  stockRecordType a stock record type 
     *  @return the returned <code>Stock</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stockRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByRecordType(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.stock.StockRecordFilterList(getStocks(), stockRecordType));
    }


    /**
     *  Gets a <code>StockList</code> containing for the given SKU. In
     *  plenary mode, the returned list contains all known stocks or
     *  an error results. Otherwise, the returned list may contain
     *  only those stocks that are accessible through this session.
     *
     *  @param  sku a stock keeping unit 
     *  @return the returned <code>Stock</code> list 
     *  @throws org.osid.NullArgumentException <code>sku</code> is
     *          <code>d null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksBySKU(String sku)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.inventory.stock.StockFilterList(new SKUFilter(sku), getStocks()));
    }


    /**
     *  Gets all <code>Stocks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Stocks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.inventory.StockList getStocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the stock list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of stocks
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.inventory.StockList filterStocksOnViews(org.osid.inventory.StockList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class SKUFilter
        implements net.okapia.osid.jamocha.inline.filter.inventory.stock.StockFilter {
         
        private final String sku;
         
         
        /**
         *  Constructs a new <code>SKUFilter</code>.
         *
         *  @param sku the sku to filter
         *  @throws org.osid.NullArgumentException
         *          <code>sku</code> is <code>null</code>
         */
        
        public SKUFilter(String sku) {
            nullarg(sku, "sku");
            this.sku = sku;
            return;
        }

         
        /**
         *  Used by the StockFilterList to filter the stock list based
         *  on SKU.
         *
         *  @param stock the stock
         *  @return <code>true</code> to pass the stock,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.inventory.Stock stock) {
            return (stock.getSKU().equals(this.sku));
        }
    }
}
