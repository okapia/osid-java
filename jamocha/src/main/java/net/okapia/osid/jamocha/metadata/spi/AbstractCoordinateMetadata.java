//
// AbstractCoordinateMetadata.java
//
//     Defines a coordinate Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.torrefacto.collect.TypeHashMap;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a coordinate Metadata.
 */

public abstract class AbstractCoordinateMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final TypeHashMap<Long> dimensions = new TypeHashMap<>();
    private final TypeHashMap<java.math.BigDecimal[]> minimums = new TypeHashMap<>();
    private final TypeHashMap<java.math.BigDecimal[]> maximums = new TypeHashMap<>();

    private final java.util.Collection<org.osid.mapping.Coordinate> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.Coordinate> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.Coordinate> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractCoordinateMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractCoordinateMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.COORDINATE, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractCoordinateMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractCoordinateMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.COORDINATE, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the set of acceptable coordinate types. 
     *
     *  @return the set of coordinate types 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE or SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getCoordinateTypes() {
        return (this.dimensions.keySet().toArray(new org.osid.type.Type[this.dimensions.size()]));
    }


    /**
     *  Tests if the given coordinate type is supported. 
     *
     *  @param  coordinateType a coordinate Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateType(org.osid.type.Type coordinateType) {
        return (this.dimensions.containsKey(coordinateType));
    }


    /**
     *  Gets the number of axes for a given supported coordinate type. 
     *
     *  @param  coordinateType a coordinate Type 
     *  @return the number of axes 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateType(coordinateType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public long getAxesForCoordinateType(org.osid.type.Type coordinateType) {
        if (this.dimensions.containsKey(coordinateType)) {
            return (this.dimensions.get(coordinateType));
        } else {
            throw new org.osid.UnsupportedException(coordinateType + " not supported");
        }
    }


    /**
     *  Gets the minimum coordinate values given supported coordinate type. 
     *
     *  @param  coordinateType a coordinate Type 
     *  @return the minimum coordinate values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateType(coordinateType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getMinimumCoordinateValues(org.osid.type.Type coordinateType) {
        if (this.dimensions.containsKey(coordinateType)) {
            return (this.minimums.get(coordinateType));
        } else {
            throw new org.osid.UnsupportedException(coordinateType + " not supported");
        }
    }


    /**
     *  Gets the maximum coordinate values given supported coordinate type. 
     *
     *  @param  coordinateType a coordinate Type 
     *  @return the maximum coordinate values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateType(coordinateType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal[] getMaximumCoordinateValues(org.osid.type.Type coordinateType) {
        if (this.dimensions.containsKey(coordinateType)) {
            return (this.maximums.get(coordinateType));
        } else {
            throw new org.osid.UnsupportedException(coordinateType + " not supported");
        }
    }


    /**
     *  Add support for a coordinate type.
     *
     *  @param coordinateType the type of coordinate
     *  @param dimensions the number of dimensions for the type
     *  @param minimum the minimum coordinate values
     *  @param maximum the maximum coordinate values
     *  @throws org.osid.InvalidArgumentException {@code dimensions}
     *          is negative or the number of dimensions in the min/max
     *          is incorrect
     *  @throws org.osid.NullArgumentException {@code coordinateType},
     *          {@code minimum}, or {@code maximum} is {@code null}
     */

    protected void addCoordinateType(org.osid.type.Type coordinateType, long dimensions, 
                                     java.math.BigDecimal[] minimum, java.math.BigDecimal[] maximum) {

        nullarg(coordinateType, "coordinate type");
        cardinalarg(dimensions, "dimensions");

        nullarg(minimum, "minumum coordinate values");
        nullarg(maximum, "maxumum coordinate values");

        if (minimum.length != dimensions) {
            throw new org.osid.InvalidArgumentException("minimum values does not have required dimensions");
        }

        if (maximum.length != dimensions) {
            throw new org.osid.InvalidArgumentException("maximum values does not have required dimensions");
        }

        this.dimensions.put(coordinateType, dimensions);
        this.minimums.put(coordinateType, minimum);
        this.maximums.put(coordinateType, maximum);

        return;
    }


    /**
     *  Gets the set of acceptable coordinate values. 
     *
     *  @return a set of coordinates or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          COORDINATE </code>
     */

    @OSID @Override
    public org.osid.mapping.Coordinate[] getCoordinateSet() {
        return (this.set.toArray(new org.osid.mapping.Coordinate[this.set.size()]));
    }

    
    /**
     *  Sets the coordinate set.
     *
     *  @param values a collection of accepted coordinate values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setCoordinateSet(java.util.Collection<org.osid.mapping.Coordinate> values) {
        clearCoordinateSet();
        addToCoordinateSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the coordinate set.
     *
     *  @param values a collection of accepted coordinate values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToCoordinateSet(java.util.Collection<org.osid.mapping.Coordinate> values) {
        nullarg(values, "coordinate set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the coordinate set.
     *
     *  @param value a coordinate value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addToCoordinateSet(org.osid.mapping.Coordinate value) {
        nullarg(value, "coordinate value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the coordinate set.
     *
     *  @param value a coordinate value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeFromCoordinateSet(org.osid.mapping.Coordinate value) {
        nullarg(value, "coordinate value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the coordinate set.
     */

    protected void clearCoordinateSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default coordinate values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default coordinate values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          COORDINATE </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.mapping.Coordinate[] getDefaultCoordinateValues() {
        return (this.defvals.toArray(new org.osid.mapping.Coordinate[this.defvals.size()]));
    }


    /**
     *  Sets the default coordinate set.
     *
     *  @param values a collection of default coordinate values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultCoordinateValues(java.util.Collection<org.osid.mapping.Coordinate> values) {
        clearDefaultCoordinateValues();
        addDefaultCoordinateValues(values);
        return;
    }


    /**
     *  Adds a collection of default coordinate values.
     *
     *  @param values a collection of default coordinate values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultCoordinateValues(java.util.Collection<org.osid.mapping.Coordinate> values) {
        nullarg(values, "default coordinate values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default coordinate value.
     *
     *  @param value a coordinate value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultCoordinateValue(org.osid.mapping.Coordinate value) {
        nullarg(value, "default coordinate value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default coordinate value.
     *
     *  @param value a coordinate value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultCoordinateValue(org.osid.mapping.Coordinate value) {
        nullarg(value, "default coordinate value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default coordinate values.
     */

    protected void clearDefaultCoordinateValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing coordinate values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing coordinate values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          COORDINATE </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate[] getExistingCoordinateValues() {
        return (this.existing.toArray(new org.osid.mapping.Coordinate[this.existing.size()]));
    }


    /**
     *  Sets the existing coordinate set.
     *
     *  @param values a collection of existing coordinate values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingCoordinateValues(java.util.Collection<org.osid.mapping.Coordinate> values) {
        clearExistingCoordinateValues();
        addExistingCoordinateValues(values);
        return;
    }


    /**
     *  Adds a collection of existing coordinate values.
     *
     *  @param values a collection of existing coordinate values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingCoordinateValues(java.util.Collection<org.osid.mapping.Coordinate> values) {
        nullarg(values, "existing coordinate values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing coordinate value.
     *
     *  @param value a coordinate value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingCoordinateValue(org.osid.mapping.Coordinate value) {
        nullarg(value, "existing coordinate value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing coordinate value.
     *
     *  @param value a coordinate value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingCoordinateValue(org.osid.mapping.Coordinate value) {
        nullarg(value, "existing coordinate value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing coordinate values.
     */

    protected void clearExistingCoordinateValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }
}