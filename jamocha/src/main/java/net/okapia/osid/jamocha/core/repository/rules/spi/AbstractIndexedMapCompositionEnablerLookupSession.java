//
// AbstractIndexedMapCompositionEnablerLookupSession.java
//
//    A simple framework for providing a CompositionEnabler lookup service
//    backed by a fixed collection of composition enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CompositionEnabler lookup service backed by a
 *  fixed collection of composition enablers. The composition enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some composition enablers may be compatible
 *  with more types than are indicated through these composition enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CompositionEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCompositionEnablerLookupSession
    extends AbstractMapCompositionEnablerLookupSession
    implements org.osid.repository.rules.CompositionEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.repository.rules.CompositionEnabler> compositionEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.repository.rules.CompositionEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.repository.rules.CompositionEnabler> compositionEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.repository.rules.CompositionEnabler>());


    /**
     *  Makes a <code>CompositionEnabler</code> available in this session.
     *
     *  @param  compositionEnabler a composition enabler
     *  @throws org.osid.NullArgumentException <code>compositionEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCompositionEnabler(org.osid.repository.rules.CompositionEnabler compositionEnabler) {
        super.putCompositionEnabler(compositionEnabler);

        this.compositionEnablersByGenus.put(compositionEnabler.getGenusType(), compositionEnabler);
        
        try (org.osid.type.TypeList types = compositionEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.compositionEnablersByRecord.put(types.getNextType(), compositionEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a composition enabler from this session.
     *
     *  @param compositionEnablerId the <code>Id</code> of the composition enabler
     *  @throws org.osid.NullArgumentException <code>compositionEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCompositionEnabler(org.osid.id.Id compositionEnablerId) {
        org.osid.repository.rules.CompositionEnabler compositionEnabler;
        try {
            compositionEnabler = getCompositionEnabler(compositionEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.compositionEnablersByGenus.remove(compositionEnabler.getGenusType());

        try (org.osid.type.TypeList types = compositionEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.compositionEnablersByRecord.remove(types.getNextType(), compositionEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCompositionEnabler(compositionEnablerId);
        return;
    }


    /**
     *  Gets a <code>CompositionEnablerList</code> corresponding to the given
     *  composition enabler genus <code>Type</code> which does not include
     *  composition enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known composition enablers or an error results. Otherwise,
     *  the returned list may contain only those composition enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  compositionEnablerGenusType a composition enabler genus type 
     *  @return the returned <code>CompositionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByGenusType(org.osid.type.Type compositionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.rules.compositionenabler.ArrayCompositionEnablerList(this.compositionEnablersByGenus.get(compositionEnablerGenusType)));
    }


    /**
     *  Gets a <code>CompositionEnablerList</code> containing the given
     *  composition enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known composition enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  composition enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  compositionEnablerRecordType a composition enabler record type 
     *  @return the returned <code>compositionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByRecordType(org.osid.type.Type compositionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.rules.compositionenabler.ArrayCompositionEnablerList(this.compositionEnablersByRecord.get(compositionEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.compositionEnablersByGenus.clear();
        this.compositionEnablersByRecord.clear();

        super.close();

        return;
    }
}
