//
// AbstractMapTrustLookupSession
//
//    A simple framework for providing a Trust lookup service
//    backed by a fixed collection of trusts.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Trust lookup service backed by a
 *  fixed collection of trusts. The trusts are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Trusts</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapTrustLookupSession
    extends net.okapia.osid.jamocha.authentication.process.spi.AbstractTrustLookupSession
    implements org.osid.authentication.process.TrustLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authentication.process.Trust> trusts = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authentication.process.Trust>());


    /**
     *  Makes a <code>Trust</code> available in this session.
     *
     *  @param  trust a trust
     *  @throws org.osid.NullArgumentException <code>trust<code>
     *          is <code>null</code>
     */

    protected void putTrust(org.osid.authentication.process.Trust trust) {
        this.trusts.put(trust.getId(), trust);
        return;
    }


    /**
     *  Makes an array of trusts available in this session.
     *
     *  @param  trusts an array of trusts
     *  @throws org.osid.NullArgumentException <code>trusts<code>
     *          is <code>null</code>
     */

    protected void putTrusts(org.osid.authentication.process.Trust[] trusts) {
        putTrusts(java.util.Arrays.asList(trusts));
        return;
    }


    /**
     *  Makes a collection of trusts available in this session.
     *
     *  @param  trusts a collection of trusts
     *  @throws org.osid.NullArgumentException <code>trusts<code>
     *          is <code>null</code>
     */

    protected void putTrusts(java.util.Collection<? extends org.osid.authentication.process.Trust> trusts) {
        for (org.osid.authentication.process.Trust trust : trusts) {
            this.trusts.put(trust.getId(), trust);
        }

        return;
    }


    /**
     *  Removes a Trust from this session.
     *
     *  @param  trustId the <code>Id</code> of the trust
     *  @throws org.osid.NullArgumentException <code>trustId<code> is
     *          <code>null</code>
     */

    protected void removeTrust(org.osid.id.Id trustId) {
        this.trusts.remove(trustId);
        return;
    }


    /**
     *  Gets the <code>Trust</code> specified by its <code>Id</code>.
     *
     *  @param  trustId <code>Id</code> of the <code>Trust</code>
     *  @return the trust
     *  @throws org.osid.NotFoundException <code>trustId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>trustId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.Trust getTrust(org.osid.id.Id trustId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authentication.process.Trust trust = this.trusts.get(trustId);
        if (trust == null) {
            throw new org.osid.NotFoundException("trust not found: " + trustId);
        }

        return (trust);
    }


    /**
     *  Gets all <code>Trusts</code>. In plenary mode, the returned
     *  list contains all known trusts or an error
     *  results. Otherwise, the returned list may contain only those
     *  trusts that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Trusts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrusts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.process.trust.ArrayTrustList(this.trusts.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.trusts.clear();
        super.close();
        return;
    }
}
