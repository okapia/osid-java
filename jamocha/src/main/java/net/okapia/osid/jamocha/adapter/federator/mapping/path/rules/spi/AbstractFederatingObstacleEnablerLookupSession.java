//
// AbstractFederatingObstacleEnablerLookupSession.java
//
//     An abstract federating adapter for an ObstacleEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  ObstacleEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingObstacleEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.mapping.path.rules.ObstacleEnablerLookupSession>
    implements org.osid.mapping.path.rules.ObstacleEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Constructs a new <code>AbstractFederatingObstacleEnablerLookupSession</code>.
     */

    protected AbstractFederatingObstacleEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.mapping.path.rules.ObstacleEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>ObstacleEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObstacleEnablers() {
        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            if (session.canLookupObstacleEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ObstacleEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObstacleEnablerView() {
        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            session.useComparativeObstacleEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ObstacleEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObstacleEnablerView() {
        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            session.usePlenaryObstacleEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include obstacle enablers in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            session.useFederatedMapView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            session.useIsolatedMapView();
        }

        return;
    }


    /**
     *  Only active obstacle enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveObstacleEnablerView() {
        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            session.useActiveObstacleEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive obstacle enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusObstacleEnablerView() {
        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            session.useAnyStatusObstacleEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>ObstacleEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ObstacleEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ObstacleEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, obstacle enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  obstacle enablers are returned.
     *
     *  @param  obstacleEnablerId <code>Id</code> of the
     *          <code>ObstacleEnabler</code>
     *  @return the obstacle enabler
     *  @throws org.osid.NotFoundException <code>obstacleEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>obstacleEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnabler getObstacleEnabler(org.osid.id.Id obstacleEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            try {
                return (session.getObstacleEnabler(obstacleEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(obstacleEnablerId + " not found");
    }


    /**
     *  Gets an <code>ObstacleEnablerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  obstacleEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>ObstacleEnablers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, obstacle enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  obstacle enablers are returned.
     *
     *  @param  obstacleEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ObstacleEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByIds(org.osid.id.IdList obstacleEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.mapping.path.rules.obstacleenabler.MutableObstacleEnablerList ret = new net.okapia.osid.jamocha.mapping.path.rules.obstacleenabler.MutableObstacleEnablerList();

        try (org.osid.id.IdList ids = obstacleEnablerIds) {
            while (ids.hasNext()) {
                ret.addObstacleEnabler(getObstacleEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>ObstacleEnablerList</code> corresponding to the given
     *  obstacle enabler genus <code>Type</code> which does not include
     *  obstacle enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  obstacle enablers or an error results. Otherwise, the returned list
     *  may contain only those obstacle enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacle enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  obstacle enablers are returned.
     *
     *  @param  obstacleEnablerGenusType an obstacleEnabler genus type 
     *  @return the returned <code>ObstacleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByGenusType(org.osid.type.Type obstacleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.FederatingObstacleEnablerList ret = getObstacleEnablerList();

        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            ret.addObstacleEnablerList(session.getObstacleEnablersByGenusType(obstacleEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ObstacleEnablerList</code> corresponding to the given
     *  obstacle enabler genus <code>Type</code> and include any additional
     *  obstacle enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known obstacle
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those obstacle enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, obstacle enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  obstacle enablers are returned.
     *
     *  @param  obstacleEnablerGenusType an obstacleEnabler genus type 
     *  @return the returned <code>ObstacleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByParentGenusType(org.osid.type.Type obstacleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.FederatingObstacleEnablerList ret = getObstacleEnablerList();

        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            ret.addObstacleEnablerList(session.getObstacleEnablersByParentGenusType(obstacleEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ObstacleEnablerList</code> containing the given
     *  obstacle enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known obstacle
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those obstacle enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, obstacle enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  obstacle enablers are returned.
     *
     *  @param  obstacleEnablerRecordType an obstacleEnabler record type 
     *  @return the returned <code>ObstacleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersByRecordType(org.osid.type.Type obstacleEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.FederatingObstacleEnablerList ret = getObstacleEnablerList();

        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            ret.addObstacleEnablerList(session.getObstacleEnablersByRecordType(obstacleEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ObstacleEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known obstacle
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those obstacle enablers that are accessible
     *  through this session.
     *  
     *  In active mode, obstacle enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  obstacle enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ObstacleEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.FederatingObstacleEnablerList ret = getObstacleEnablerList();

        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            ret.addObstacleEnablerList(session.getObstacleEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>ObstacleEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined to
     *  the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known obstacle
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those obstacle enablers that are accessible
     *  through this session.
     *
     *  In active mode, obstacle enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  obstacle enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ObstacleEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.FederatingObstacleEnablerList ret = getObstacleEnablerList();

        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            ret.addObstacleEnablerList(session.getObstacleEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ObstacleEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known obstacle
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those obstacle enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, obstacle enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  obstacle enablers are returned.
     *
     *  @return a list of <code>ObstacleEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.ObstacleEnablerList getObstacleEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.FederatingObstacleEnablerList ret = getObstacleEnablerList();

        for (org.osid.mapping.path.rules.ObstacleEnablerLookupSession session : getSessions()) {
            ret.addObstacleEnablerList(session.getObstacleEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.FederatingObstacleEnablerList getObstacleEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.ParallelObstacleEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.rules.obstacleenabler.CompositeObstacleEnablerList());
        }
    }
}
