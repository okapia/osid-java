//
// AbstractMutableAuthorization.java
//
//     Defines a mutable Authorization.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.authorization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Authorization</code>.
 */

public abstract class AbstractMutableAuthorization
    extends net.okapia.osid.jamocha.authorization.authorization.spi.AbstractAuthorization
    implements org.osid.authorization.Authorization,
               net.okapia.osid.jamocha.builder.authorization.authorization.AuthorizationMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this authorization. 
     *
     *  @param record authorization record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAuthorizationRecord(org.osid.authorization.records.AuthorizationRecord record, org.osid.type.Type recordType) {
        super.addAuthorizationRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this authorization is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this authorization ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this authorization.
     *
     *  @param displayName the name for this authorization
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this authorization.
     *
     *  @param description the description of this authorization
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this authorization
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this authorization is
     *          implicit, <code> false </code> otherwise
     */

    @Override
    public void setImplicit(boolean implicit) {
        super.setImplicit(implicit);
        return;
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    @Override
    public void setResource(org.osid.resource.Resource resource) {
        super.setResource(resource);
        return;
    }


    /**
     *  Sets the trust.
     *
     *  @param trust a trust
     *  @throws org.osid.NullArgumentException
     *          <code>trust</code> is <code>null</code>
     */

    @Override
    public void setTrust(org.osid.authentication.process.Trust trust) {
        super.setTrust(trust);
        return;
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    @Override
    public void setAgent(org.osid.authentication.Agent agent) {
        super.setAgent(agent);
        return;
    }


    /**
     *  Sets the function.
     *
     *  @param function a function
     *  @throws org.osid.NullArgumentException
     *          <code>function</code> is <code>null</code>
     */

    @Override
    public void setFunction(org.osid.authorization.Function function) {
        super.setFunction(function);
        return;
    }


    /**
     *  Sets the qualifier.
     *
     *  @param qualifier a qualifier
     *  @throws org.osid.NullArgumentException
     *          <code>qualifier</code> is <code>null</code>
     */

    @Override
    public void setQualifier(org.osid.authorization.Qualifier qualifier) {
        super.setQualifier(qualifier);
        return;
    }
}

