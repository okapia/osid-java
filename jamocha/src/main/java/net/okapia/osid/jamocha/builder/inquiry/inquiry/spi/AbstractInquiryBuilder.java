//
// AbstractInquiry.java
//
//     Defines an Inquiry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inquiry.inquiry.spi;


/**
 *  Defines an <code>Inquiry</code> builder.
 */

public abstract class AbstractInquiryBuilder<T extends AbstractInquiryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inquiry.inquiry.InquiryMiter inquiry;


    /**
     *  Constructs a new <code>AbstractInquiryBuilder</code>.
     *
     *  @param inquiry the inquiry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractInquiryBuilder(net.okapia.osid.jamocha.builder.inquiry.inquiry.InquiryMiter inquiry) {
        super(inquiry);
        this.inquiry = inquiry;
        return;
    }


    /**
     *  Builds the inquiry.
     *
     *  @return the new inquiry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>inquiry</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inquiry.Inquiry build() {
        (new net.okapia.osid.jamocha.builder.validator.inquiry.inquiry.InquiryValidator(getValidations())).validate(this.inquiry);
        return (new net.okapia.osid.jamocha.builder.inquiry.inquiry.ImmutableInquiry(this.inquiry));
    }


    /**
     *  Gets the inquiry. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new inquiry
     */

    @Override
    public net.okapia.osid.jamocha.builder.inquiry.inquiry.InquiryMiter getMiter() {
        return (this.inquiry);
    }


    /**
     *  Sets the audit.
     *
     *  @param audit an audit
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>audit</code> is <code>null</code>
     */

    public T audit(org.osid.inquiry.Audit audit) {
        getMiter().setAudit(audit);
        return (self());
    }


    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if not
     *         required
     */

    public T required(boolean required) {
        getMiter().setRequired(required);
        return(self());
    }

    
    /**
     *  Sets the affirmation required flag.
     *
     *  @param required {@code true} if required, {@code false} if not
     *         required
     */

    public T affirmationRequired(boolean required) {
        getMiter().setAffirmationRequired(required);
        return(self());
    }
    

    /**
     *  Sets the needs one response flag.
     *
     *  @param one {@code true} if one response is needed, {@code
     *         false} if a response is always needed
     */

    public T needsOneResponse(boolean one) {
        getMiter().setNeedsOneResponse(one);
        return(self());
    }

    
    /**
     *  Sets the question.
     *
     *  @param question a question
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>question</code> is <code>null</code>
     */

    public T question(org.osid.locale.DisplayText question) {
        getMiter().setQuestion(question);
        return (self());
    }


    /**
     *  Adds an Inquiry record.
     *
     *  @param record an inquiry record
     *  @param recordType the type of inquiry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inquiry.records.InquiryRecord record, org.osid.type.Type recordType) {
        getMiter().addInquiryRecord(record, recordType);
        return (self());
    }
}       


