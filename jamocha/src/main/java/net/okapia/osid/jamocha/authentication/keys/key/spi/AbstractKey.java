//
// AbstractKey.java
//
//     Defines a Key.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.keys.key.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Key</code>.
 */

public abstract class AbstractKey
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.authentication.keys.Key {

    private org.osid.authentication.Agent agent;

    private final java.util.Collection<org.osid.authentication.keys.records.KeyRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> AgentId </code> corresponding to this key. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.agent.getId());
    }


    /**
     *  Gets the <code> Agent </code> corresponding to this key. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Tests if this key supports the given record <code>Type</code>.
     *
     *  @param  keyRecordType a key record type 
     *  @return <code>true</code> if the keyRecordType is supported,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type keyRecordType) {
        for (org.osid.authentication.keys.records.KeyRecord record : this.records) {
            if (record.implementsRecordType(keyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Key</code>
     *  record <code>Type</code>.
     *
     *  @param  keyRecordType the key record type 
     *  @return the key record 
     *  @throws org.osid.NullArgumentException
     *          <code>keyRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(keyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeyRecord getKeyRecord(org.osid.type.Type keyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.keys.records.KeyRecord record : this.records) {
            if (record.implementsRecordType(keyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(keyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this key. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param keyRecord the key record
     *  @param keyRecordType key record type
     *  @throws org.osid.NullArgumentException <code>keyRecord</code>
     *          or <code>keyRecordType</code> is <code>null</code>
     */
            
    protected void addKeyRecord(org.osid.authentication.keys.records.KeyRecord keyRecord, 
                                org.osid.type.Type keyRecordType) {

        nullarg(keyRecord, "key record");
        addRecordType(keyRecordType);
        this.records.add(keyRecord);
        
        return;
    }
}
