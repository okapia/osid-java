//
// AbstractIndexedMapCookbookLookupSession.java
//
//    A simple framework for providing a Cookbook lookup service
//    backed by a fixed collection of cookbooks with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Cookbook lookup service backed by a
 *  fixed collection of cookbooks. The cookbooks are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some cookbooks may be compatible
 *  with more types than are indicated through these cookbook
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Cookbooks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCookbookLookupSession
    extends AbstractMapCookbookLookupSession
    implements org.osid.recipe.CookbookLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.recipe.Cookbook> cookbooksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recipe.Cookbook>());
    private final MultiMap<org.osid.type.Type, org.osid.recipe.Cookbook> cookbooksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.recipe.Cookbook>());


    /**
     *  Makes a <code>Cookbook</code> available in this session.
     *
     *  @param  cookbook a cookbook
     *  @throws org.osid.NullArgumentException <code>cookbook<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCookbook(org.osid.recipe.Cookbook cookbook) {
        super.putCookbook(cookbook);

        this.cookbooksByGenus.put(cookbook.getGenusType(), cookbook);
        
        try (org.osid.type.TypeList types = cookbook.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.cookbooksByRecord.put(types.getNextType(), cookbook);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a cookbook from this session.
     *
     *  @param cookbookId the <code>Id</code> of the cookbook
     *  @throws org.osid.NullArgumentException <code>cookbookId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCookbook(org.osid.id.Id cookbookId) {
        org.osid.recipe.Cookbook cookbook;
        try {
            cookbook = getCookbook(cookbookId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.cookbooksByGenus.remove(cookbook.getGenusType());

        try (org.osid.type.TypeList types = cookbook.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.cookbooksByRecord.remove(types.getNextType(), cookbook);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCookbook(cookbookId);
        return;
    }


    /**
     *  Gets a <code>CookbookList</code> corresponding to the given
     *  cookbook genus <code>Type</code> which does not include
     *  cookbooks of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known cookbooks or an error results. Otherwise,
     *  the returned list may contain only those cookbooks that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  cookbookGenusType a cookbook genus type 
     *  @return the returned <code>Cookbook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByGenusType(org.osid.type.Type cookbookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.cookbook.ArrayCookbookList(this.cookbooksByGenus.get(cookbookGenusType)));
    }


    /**
     *  Gets a <code>CookbookList</code> containing the given
     *  cookbook record <code>Type</code>. In plenary mode, the
     *  returned list contains all known cookbooks or an error
     *  results. Otherwise, the returned list may contain only those
     *  cookbooks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  cookbookRecordType a cookbook record type 
     *  @return the returned <code>cookbook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cookbookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByRecordType(org.osid.type.Type cookbookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.recipe.cookbook.ArrayCookbookList(this.cookbooksByRecord.get(cookbookRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.cookbooksByGenus.clear();
        this.cookbooksByRecord.clear();

        super.close();

        return;
    }
}
