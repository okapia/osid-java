//
// AbstractLocaleManager.java
//
//     An adapter for a LocaleManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.locale.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a LocaleManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterLocaleManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.locale.LocaleManager>
    implements org.osid.locale.LocaleManager {


    /**
     *  Constructs a new {@code AbstractAdapterLocaleManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterLocaleManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterLocaleManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterLocaleManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if translation is supported. 
     *
     *  @return <code> true </code> if translation is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTranslation() {
        return (getAdapteeManager().supportsTranslation());
    }


    /**
     *  Tests if translation administration is supported. 
     *
     *  @return <code> true </code> if translation administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTranslationAdmin() {
        return (getAdapteeManager().supportsTranslationAdmin());
    }


    /**
     *  Tests if numeric formatting is supported. 
     *
     *  @return <code> true </code> if numeric formatting is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNumericFormatting() {
        return (getAdapteeManager().supportsNumericFormatting());
    }


    /**
     *  Tests if calendar formatting is supported. 
     *
     *  @return <code> true </code> if calendar formatting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarFormatting() {
        return (getAdapteeManager().supportsCalendarFormatting());
    }


    /**
     *  Tests if currency formatting is supported. 
     *
     *  @return <code> true </code> if currency formatting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCurrencyFormatting() {
        return (getAdapteeManager().supportsCurrencyFormatting());
    }


    /**
     *  Tests if coordinate formatting is supported. 
     *
     *  @return <code> true </code> if coordinate formatting is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCoordinateFormatting() {
        return (getAdapteeManager().supportsCoordinateFormatting());
    }


    /**
     *  Tests if unit conversion is supported. 
     *
     *  @return <code> true </code> if unit conversion is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUnitConversion() {
        return (getAdapteeManager().supportsUnitConversion());
    }


    /**
     *  Tests if currency conversion is supported. 
     *
     *  @return <code> true </code> if currency conversion is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCurrencyConversion() {
        return (getAdapteeManager().supportsCurrencyConversion());
    }


    /**
     *  Tests if calendar conversion is supported. 
     *
     *  @return <code> true </code> if calendar conversion is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarConversion() {
        return (getAdapteeManager().supportsCalendarConversion());
    }


    /**
     *  Tests if coordnate conversion is supported. 
     *
     *  @return <code> true </code> if coordinate conversion is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCoordinateConversion() {
        return (getAdapteeManager().supportsCoordinateConversion());
    }


    /**
     *  Tests if spatial unit conversion is supported. 
     *
     *  @return <code> true </code> if spatial unit conversion is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpatialUnitConversion() {
        return (getAdapteeManager().supportsSpatialUnitConversion());
    }


    /**
     *  Tests if format conversion is supported. 
     *
     *  @return <code> true </code> if format conversion is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFormatConversion() {
        return (getAdapteeManager().supportsFormatConversion());
    }


    /**
     *  Tests if a calendar informational service is supported. 
     *
     *  @return <code> true </code> if calendar info is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarInfo() {
        return (getAdapteeManager().supportsCalendarInfo());
    }


    /**
     *  Tests if a given language translation is supported. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @param  targetLanguageType the type of the target language 
     *  @param  targetScriptType the type of the target script 
     *  @return <code> true </code> if the given source and target translation 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType, 
     *          sourceScriptType, targetLanguageType </code> or <code> 
     *          targetScriptType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLanguageTypesForTranslation(org.osid.type.Type sourceLanguageType, 
                                                       org.osid.type.Type sourceScriptType, 
                                                       org.osid.type.Type targetLanguageType, 
                                                       org.osid.type.Type targetScriptType) {
        return (getAdapteeManager().supportsLanguageTypesForTranslation(sourceLanguageType, sourceScriptType, targetLanguageType, targetScriptType));
    }


    /**
     *  Gets the list of target language types for a given source language 
     *  type. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @return the list of supported types for the given source language type 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getLanguageTypesForSource(org.osid.type.Type sourceLanguageType, 
                                                            org.osid.type.Type sourceScriptType) {
        return (getAdapteeManager().getLanguageTypesForSource(sourceLanguageType, sourceScriptType));
    }


    /**
     *  Gets all the source language types supported. 
     *
     *  @return the list of supported language types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceLanguageTypes() {
        return (getAdapteeManager().getSourceLanguageTypes());
    }


    /**
     *  Gets the list of script types available for a given language type. 
     *
     *  @param  languageType the type of the language 
     *  @return the list of supported script types for the given language type 
     *  @throws org.osid.NullArgumentException <code> languageType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getScriptTypesForLanguageType(org.osid.type.Type languageType) {
        return (getAdapteeManager().getScriptTypesForLanguageType(languageType));
    }


    /**
     *  Tests if a given numeric format type is supported. 
     *
     *  @param  numericFormatType the type of the numeric format 
     *  @return <code> true </code> if the given numeric format type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> numericFormatType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsNumericFormatTypes(org.osid.type.Type numericFormatType) {
        return (getAdapteeManager().supportsNumericFormatTypes(numericFormatType));
    }


    /**
     *  Gets all the numeric format types supported. 
     *
     *  @return the list of supported numeric format types 
     */

    @OSID @Override
    public org.osid.type.TypeList getNumericFormatTypes() {
        return (getAdapteeManager().getNumericFormatTypes());
    }


    /**
     *  Tests if a given calendaring formatting is supported. 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  timeType the type of the time system 
     *  @param  dateFormatType the type of the output date format 
     *  @param  timeFormatType the type of the output time format 
     *  @return <code> true </code> if formatting with the given types is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> calendarType, 
     *          calendarFormatType, timeType, </code> or <code> timeFormatType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarTypesForFormatting(org.osid.type.Type calendarType, 
                                                      org.osid.type.Type timeType, 
                                                      org.osid.type.Type dateFormatType, 
                                                      org.osid.type.Type timeFormatType) {
        return (getAdapteeManager().supportsCalendarTypesForFormatting(calendarType, timeType, dateFormatType, timeFormatType));
    }


    /**
     *  Gets all the calendar types for which formats are available. 
     *
     *  @return the list of calendar types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarTypesForFormatting() {
        return (getAdapteeManager().getCalendarTypesForFormatting());
    }


    /**
     *  Gets the list of date format types for a given calendar type. 
     *
     *  @param  calendarType the type of the calendar 
     *  @return the list of supported date format types 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getDateFormatTypesForCalendarType(org.osid.type.Type calendarType) {
        return (getAdapteeManager().getDateFormatTypesForCalendarType(calendarType));
    }


    /**
     *  Gets all the time types for which formatting is available. 
     *
     *  @return the list of time types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimeTypesForFormatting() {
        return (getAdapteeManager().getTimeTypesForFormatting());
    }


    /**
     *  Gets the list of time format types for a given time type. 
     *
     *  @param  timeType the type of the time 
     *  @return the list of supported time format types 
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimeFormatTypesForTimeType(org.osid.type.Type timeType) {
        return (getAdapteeManager().getTimeFormatTypesForTimeType(timeType));
    }


    /**
     *  Tests if a given currency formatting is supported. 
     *
     *  @param  currencyType the type of the currency 
     *  @param  numericFormatType the type of the output currency format 
     *  @return <code> true </code> if formatting with the given types is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> or 
     *          <code> numericFormatType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCurrencyTypesForFormatting(org.osid.type.Type currencyType, 
                                                      org.osid.type.Type numericFormatType) {
        return (getAdapteeManager().supportsCurrencyTypesForFormatting(currencyType, numericFormatType));
    }


    /**
     *  Gets all the currency types for which formatting is available. 
     *
     *  @return the list of currency types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCurrencyTypesForFormatting() {
        return (getAdapteeManager().getCurrencyTypesForFormatting());
    }


    /**
     *  Gets the list of currency format types for a given currency type. 
     *
     *  @param  currencyType the type of the currency 
     *  @return the list of supported currency format types 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCurrencyFormatTypesForCurrencyType(org.osid.type.Type currencyType) {
        return (getAdapteeManager().getCurrencyFormatTypesForCurrencyType(currencyType));
    }


    /**
     *  Tests if a given coordinate formatting is supported. 
     *
     *  @param  coordinateType the type of the coordinate 
     *  @param  coordinateFormatType the type of the output coordinate format 
     *  @return <code> true </code> if formatting with the given types is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> cooridinateType </code> 
     *          or <code> coodinateFormatType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateTypesForFormatting(org.osid.type.Type coordinateType, 
                                                        org.osid.type.Type coordinateFormatType) {
        return (getAdapteeManager().supportsCoordinateTypesForFormatting(coordinateType, coordinateFormatType));
    }


    /**
     *  Gets all the coordinate types for which formatting is available. 
     *
     *  @return the list of coordinate types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateTypesForFormatting() {
        return (getAdapteeManager().getCoordinateTypesForFormatting());
    }


    /**
     *  Gets the list of coordinate format types for a given coordinate type. 
     *
     *  @param  coordinateType the type of the coordinate 
     *  @return the list of supported coordinate format types 
     *  @throws org.osid.NullArgumentException <code> coordinaterType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateFormatTypesForCoordinateType(org.osid.type.Type coordinateType) {
        return (getAdapteeManager().getCoordinateFormatTypesForCoordinateType(coordinateType));
    }


    /**
     *  Tests if a given measure conversion is supported. 
     *
     *  @param  sourceUnitType the type of the source measure 
     *  @param  targetUnitType the type of the target measure 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceUnitType </code> 
     *          or <code> targetUnitType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsUnitTypesForConversion(org.osid.type.Type sourceUnitType, 
                                                  org.osid.type.Type targetUnitType) {
        return (getAdapteeManager().supportsUnitTypesForConversion(sourceUnitType, targetUnitType));
    }


    /**
     *  Gets the list of target measure types for a given source measure type. 
     *
     *  @param  sourceUnitType the type of the source measure 
     *  @return the list of supported target measure types 
     *  @throws org.osid.NullArgumentException <code> sourceUnitType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getUnitTypesForSource(org.osid.type.Type sourceUnitType) {
        return (getAdapteeManager().getUnitTypesForSource(sourceUnitType));
    }


    /**
     *  Gets all the source unit types supported. 
     *
     *  @return the list of supported source unit types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceUnitTypes() {
        return (getAdapteeManager().getSourceUnitTypes());
    }


    /**
     *  Tests if a given currency conversion is supported. 
     *
     *  @param  sourceCurrencyType the type of the source currency 
     *  @param  targetCurrencyType the type of the target currency 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceCurrencyType 
     *          </code> or <code> targetCurrencyType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCurrencyTypesForConversion(org.osid.type.Type sourceCurrencyType, 
                                                      org.osid.type.Type targetCurrencyType) {
        return (getAdapteeManager().supportsCurrencyTypesForConversion(sourceCurrencyType, targetCurrencyType));
    }


    /**
     *  Gets the list of target currency types for a given source currency 
     *  type. 
     *
     *  @param  sourceCurrencyType the type of the source currency 
     *  @return the list of supported currency types 
     *  @throws org.osid.NullArgumentException <code> sourceCurrencyType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCurrencyTypesForSource(org.osid.type.Type sourceCurrencyType) {
        return (getAdapteeManager().getCurrencyTypesForSource(sourceCurrencyType));
    }


    /**
     *  Gets the list of source currency types. 
     *
     *  @return the list of supported source currency types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceCurrencyTypes() {
        return (getAdapteeManager().getSourceCurrencyTypes());
    }


    /**
     *  Tests if a given calendar conversion is supported. 
     *
     *  @param  sourceCalendarType the type of the source calendar 
     *  @param  targetCalendarType the type of the target calendar 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceCalendarType 
     *          </code> or <code> targetCalendarType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCalendarTypesForConversion(org.osid.type.Type sourceCalendarType, 
                                                      org.osid.type.Type targetCalendarType) {
        return (getAdapteeManager().supportsCalendarTypesForConversion(sourceCalendarType, targetCalendarType));
    }


    /**
     *  Gets the list of target calendar types for a given source calendar 
     *  type. 
     *
     *  @param  sourceCalendarType the type of the source calendar 
     *  @return the list of supported calendar types 
     *  @throws org.osid.NullArgumentException <code> sourceCalendarType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarTypesForSource(org.osid.type.Type sourceCalendarType) {
        return (getAdapteeManager().getCalendarTypesForSource(sourceCalendarType));
    }


    /**
     *  Gets the list of source calendar types. 
     *
     *  @return the list of supported source calendar types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceCalendarTypes() {
        return (getAdapteeManager().getSourceCalendarTypes());
    }


    /**
     *  Tests if a given time conversion is supported. 
     *
     *  @param  sourceTimeType the type of the source time 
     *  @param  targetTimeType the type of the target time 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceTimeType </code> 
     *          or <code> targetTimeType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimeTypesForConversion(org.osid.type.Type sourceTimeType, 
                                                  org.osid.type.Type targetTimeType) {
        return (getAdapteeManager().supportsTimeTypesForConversion(sourceTimeType, targetTimeType));
    }


    /**
     *  Gets the list of target time types for a given source time type. 
     *
     *  @param  sourceTimeType the type of the source time 
     *  @return the list of supported time types 
     *  @throws org.osid.NullArgumentException <code> sourceTimeType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimeTypesForSource(org.osid.type.Type sourceTimeType) {
        return (getAdapteeManager().getTimeTypesForSource(sourceTimeType));
    }


    /**
     *  Gets the list of source time types. 
     *
     *  @return the list of supported source time types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceTimeTypes() {
        return (getAdapteeManager().getSourceTimeTypes());
    }


    /**
     *  Gets the list of time types supported for a given calendar type where 
     *  they are both used in a <code> DateTime. </code> 
     *
     *  @param  calendarType the type of the calendar 
     *  @return the list of supported time types 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimeTypesForCalendarType(org.osid.type.Type calendarType) {
        return (getAdapteeManager().getTimeTypesForCalendarType(calendarType));
    }


    /**
     *  Gets the list of calendar types supported for a given time type where 
     *  they are both used in a <code> DateTime. </code> 
     *
     *  @param  timeType the type of the time system 
     *  @return the list of supported calendar types 
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarTypesForTimeType(org.osid.type.Type timeType) {
        return (getAdapteeManager().getCalendarTypesForTimeType(timeType));
    }


    /**
     *  Tests if a given calendar and time type are used together in a <code> 
     *  DateTime. </code> 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  timeType the type of the time system 
     *  @return <code> true </code> if the given calendar and time types are 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> or 
     *          <code> timeType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarTimeTypes(org.osid.type.Type calendarType, 
                                             org.osid.type.Type timeType) {
        return (getAdapteeManager().supportsCalendarTimeTypes(calendarType, timeType));
    }


    /**
     *  Tests if a given coordinate type for conversion is supported. 
     *
     *  @param  sourceCoordinateType the type of the source coordinate 
     *  @param  targetCoordinateType the type of the target coordinate 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceCoordinateType 
     *          </code> or <code> targetCoordinateType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateTypesForConversion(org.osid.type.Type sourceCoordinateType, 
                                                        org.osid.type.Type targetCoordinateType) {
        return (getAdapteeManager().supportsCoordinateTypesForConversion(sourceCoordinateType, targetCoordinateType));
    }


    /**
     *  Gets the list of target coordinate types for a given source coordinate 
     *  type. 
     *
     *  @param  sourceCoordinateType the type of the source coordinate 
     *  @return the list of supported target coordinate types 
     *  @throws org.osid.NullArgumentException <code> sourceCoordinateType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateTypesForSource(org.osid.type.Type sourceCoordinateType) {
        return (getAdapteeManager().getCoordinateTypesForSource(sourceCoordinateType));
    }


    /**
     *  Gets the list of source coordinate types. 
     *
     *  @return the list of supported source coordinate types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceCoordinateTypes() {
        return (getAdapteeManager().getSourceCoordinateTypes());
    }


    /**
     *  Tests if a given spatial unit conversion is supported. 
     *
     *  @param  sourceSpatialUnitRecordType the type of the source spatial 
     *          unit record 
     *  @param  targetSpatialUnitRecordType the type of the target spatial 
     *          unit record 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          sourceSpatialUnitRecordType </code> or <code> 
     *          targetSpatialUnitRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordTypesForConversion(org.osid.type.Type sourceSpatialUnitRecordType, 
                                                               org.osid.type.Type targetSpatialUnitRecordType) {
        return (getAdapteeManager().supportsSpatialUnitRecordTypesForConversion(sourceSpatialUnitRecordType, targetSpatialUnitRecordType));
    }


    /**
     *  Gets the list of target spatial unit types for a given source spatial 
     *  unit type. 
     *
     *  @param  sourceSpatialUnitRecordType the type of the source spatial 
     *          unit record 
     *  @return the list of supported target spatial unit record types 
     *  @throws org.osid.NullArgumentException <code> 
     *          sourceSpatialUnitRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpatialUnitRecordTypesForSource(org.osid.type.Type sourceSpatialUnitRecordType) {
        return (getAdapteeManager().getSpatialUnitRecordTypesForSource(sourceSpatialUnitRecordType));
    }


    /**
     *  Gets the list of source spatial unit record types. 
     *
     *  @return the list of supported source spatial unit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceSpatialUnitRecordTypes() {
        return (getAdapteeManager().getSourceSpatialUnitRecordTypes());
    }


    /**
     *  Tests if a given format conversion is supported. 
     *
     *  @param  sourceFormatType the type of the source format 
     *  @param  targetFormatType the type of the target format 
     *  @return <code> true </code> if the given source and target conversion 
     *          is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sourceFormatType </code> 
     *          or <code> targetFormatType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFormatTypesForConversion(org.osid.type.Type sourceFormatType, 
                                                    org.osid.type.Type targetFormatType) {
        return (getAdapteeManager().supportsFormatTypesForConversion(sourceFormatType, targetFormatType));
    }


    /**
     *  Gets the list of target format types for a given source spatial unit 
     *  type. 
     *
     *  @param  sourceFormatType the type of the source format 
     *  @return the list of supported target format types 
     *  @throws org.osid.NullArgumentException <code> sourceFormatType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public org.osid.type.TypeList getFormatTypesForSource(org.osid.type.Type sourceFormatType) {
        return (getAdapteeManager().getFormatTypesForSource(sourceFormatType));
    }


    /**
     *  Gets the list of source format types. 
     *
     *  @return the list of supported source format types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSourceFormatTypes() {
        return (getAdapteeManager().getSourceFormatTypes());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the language 
     *  translation service. 
     *
     *  @return a <code> TranslationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTranslation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationSession getTranslationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTranslationSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the language 
     *  translation service and the given language and script types. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @param  targetLanguageType the type of the target language 
     *  @param  targetScriptType the type of the target script 
     *  @return a <code> TranslationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType, 
     *          sourceScriptType, targetLanguageType </code> or <code> 
     *          targetScriptType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTranslation() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsLanguageTypesForTranslation(sourceLanguageType, 
     *          sourceScriptType, targetLanguageType, targetScriptType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationSession getTranslationSessionForType(org.osid.type.Type sourceLanguageType, 
                                                                           org.osid.type.Type sourceScriptType, 
                                                                           org.osid.type.Type targetLanguageType, 
                                                                           org.osid.type.Type targetScriptType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTranslationSessionForType(sourceLanguageType, sourceScriptType, targetLanguageType, targetScriptType));
    }


    /**
     *  Gets a language translation administration service for updating a 
     *  locale dictionary. 
     *
     *  @return a <code> TranslationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTranslationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationAdminSession getTranslationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTranslationAdminSession());
    }


    /**
     *  Gets a language trabslation administration service for updating a 
     *  locale dictionary using the given language and script types. 
     *
     *  @param  sourceLanguageType the type of the source language 
     *  @param  sourceScriptType the type of the source script 
     *  @param  targetLanguageType the type of the target language 
     *  @param  targetScriptType the type of the target script 
     *  @return a <code> TranslationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceLanguageType, 
     *          sourceScriptType, targetLanguageType </code> or <code> 
     *          targetScriptType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTranslationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsLanguageTypesForTranslation(sourceLanguageType, 
     *          sourceScriptType, targetLanguageType, targetScriptType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.TranslationAdminSession getTranslationAdminSessionForType(org.osid.type.Type sourceLanguageType, 
                                                                                     org.osid.type.Type sourceScriptType, 
                                                                                     org.osid.type.Type targetLanguageType, 
                                                                                     org.osid.type.Type targetScriptType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTranslationAdminSessionForType(sourceLanguageType, sourceScriptType, targetLanguageType, targetScriptType));
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the numeric 
     *  formatting service. 
     *
     *  @return a <code> NumericFormattingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNumericFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.NumericFormattingSession getNumericFormattingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNumericFormattingSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the numeric 
     *  formatting service and the given numeric format type. 
     *
     *  @param  numericFormatType the type of the numeric format 
     *  @return a <code> NumericFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> numericFormatType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNumericFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsNumericFormatType(numericFormatType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.locale.NumericFormattingSession getNumericFormattingSessionForType(org.osid.type.Type numericFormatType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getNumericFormattingSessionForType(numericFormatType));
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  formatting service. 
     *
     *  @return a <code> CalendarFormattingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarFormattingSession getCalendarFormattingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarFormattingSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  formatting service and the given calendar and time types. 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  calendarFormatType the type of the calendar format 
     *  @param  timeType the type of the time system 
     *  @param  timeFormatType the type of the time format 
     *  @return a <code> CalendarFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarType, 
     *          calendarFormatType, timeType </code> or <code> timeFormatType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTypesForFormattinge(calendarType, 
     *          calendarFormatType, timeType, timeFormatType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarFormattingSession getCalendarFormattingSessionForType(org.osid.type.Type calendarType, 
                                                                                         org.osid.type.Type calendarFormatType, 
                                                                                         org.osid.type.Type timeType, 
                                                                                         org.osid.type.Type timeFormatType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarFormattingSessionForType(calendarType, calendarFormatType, timeType, timeFormatType));
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  formatting service. 
     *
     *  @return a <code> CurrencyFormattingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyFormattingSession getCurrencyFormattingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCurrencyFormattingSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  formatting service and the given currency and numeric format types. 
     *
     *  @param  currencyType the type of the currency 
     *  @param  numericFormatType the type of the numeric format 
     *  @return a <code> CurrencyFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> currencyType </code> or 
     *          <code> numericFormatType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCurrencyTypesForFomatting(currencyType, 
     *          numericFormatType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyFormattingSession getCurrencyFormattingSessionForType(org.osid.type.Type currencyType, 
                                                                                         org.osid.type.Type numericFormatType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCurrencyFormattingSessionForType(currencyType, numericFormatType));
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  formatting service. 
     *
     *  @return a <code> CoordinateFormattingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateFormatting() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateFormattingSession getCoordinateFormattingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCoordinateFormattingSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  formatting service and the given coordinate and format types. 
     *
     *  @param  coordinateType the type of the coordinate 
     *  @param  coordinateFormatType the type of the coordinate format 
     *  @return a <code> CoordinateFormattingSession </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          or <code> coordinateFormatType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateFormatting() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateTypesForFomatting(coordinateType, 
     *          coordinateFormatType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateFormattingSession getCoordinateFormattingSessionForType(org.osid.type.Type coordinateType, 
                                                                                             org.osid.type.Type coordinateFormatType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCoordinateFormattingSessionForType(coordinateType, coordinateFormatType));
    }


    /**
     *  Gets a unit conversion session. 
     *
     *  @return a <code> UnitConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUnitConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.UnitConversionSession getUnitConversionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUnitConversionSession());
    }


    /**
     *  Gets a currency conversion session. 
     *
     *  @return a <code> CurrencyConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyConversionSession getCurrencyConversionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCurrencyConversionSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the currency 
     *  conversion service and the given currency types. 
     *
     *  @param  sourceCurrencyType the type of the source currency 
     *  @param  targetCurrencyType the type of the target currency 
     *  @return a <code> CurrencyConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCurrencyType 
     *          </code> or <code> targetCurrencyType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCurrencyConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCurrencyTypesForConversion(sourceCurrencyType, 
     *          targetCurrencyType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CurrencyConversionSession getCurrencyConversionSessionForType(org.osid.type.Type sourceCurrencyType, 
                                                                                         org.osid.type.Type targetCurrencyType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCurrencyConversionSessionForType(sourceCurrencyType, targetCurrencyType));
    }


    /**
     *  Gets a calendar conversion session. 
     *
     *  @return a <code> CalendarConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarConversionSession getCalendarConversionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarConversionSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  conversion service and the given calendar types. 
     *
     *  @param  sourceCalendarType the type of the source calendar 
     *  @param  sourceTimeType the type of the source time 
     *  @param  targetCalendarType the type of the target calendar 
     *  @param  targetTimeType the type of the target time 
     *  @return a <code> CalendarConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCalendarType, 
     *          sourceTimeType, </code> <code> targetCalendarType </code> or 
     *          <code> targetTimeType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTypesForConversion(sourceCalendarType, 
     *          targetCalendarType) </code> or <code> 
     *          supportsTimeTypesForConversion(sourceTimeType, targetTimeType) 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarConversionSession getCalendarConversionSessionForType(org.osid.type.Type sourceCalendarType, 
                                                                                         org.osid.type.Type sourceTimeType, 
                                                                                         org.osid.type.Type targetCalendarType, 
                                                                                         org.osid.type.Type targetTimeType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarConversionSessionForType(sourceCalendarType, sourceTimeType, targetCalendarType, targetTimeType));
    }


    /**
     *  Gets a coordinate conversion session. 
     *
     *  @return a <code> CoordinateConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateConversionSession getCoordinateConversionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCoordinateConversionSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the coordinate 
     *  conversion service and the given coordinate types. 
     *
     *  @param  sourceCoordinateType the type of the source coordinate 
     *  @param  targetCoordinateType the type of the target coordinate 
     *  @return a <code> CoordinateConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceCoordinateType 
     *          </code> or <code> targetCoordinateType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCoordinateConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCoordinateRecordTypesForConversion(sourceCoordinateType, 
     *          targetCoordinateType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CoordinateConversionSession getCoordinateConversionSessionForType(org.osid.type.Type sourceCoordinateType, 
                                                                                             org.osid.type.Type targetCoordinateType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCoordinateConversionSessionForType(sourceCoordinateType, targetCoordinateType));
    }


    /**
     *  Gets a spatial unit conversion session. 
     *
     *  @return a <code> SpatialUnitConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpatialUnitConversion() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.locale.SpatialUnitConversionSession getSpatialUnitConversionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpatialUnitConversionSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the spatial unit 
     *  conversion service and the given spatial unit record types. 
     *
     *  @param  sourceSpatialUnitRecordType the type of the source spatial 
     *          unit record 
     *  @param  targetSpatialUnitRecordType the type of the target spatial 
     *          unit record 
     *  @return a <code> SpatialUnitConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sourceSpatialUnitRecordType </code> or <code> 
     *          targetSpatialUnitRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpatialUnitConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsSpatialUnitRecordTypesForConversion(sourceSpatialUnitRecordType, 
     *          targetSpatialUnitRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.SpatialUnitConversionSession getSpatialUnitConversionSessionForType(org.osid.type.Type sourceSpatialUnitRecordType, 
                                                                                               org.osid.type.Type targetSpatialUnitRecordType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSpatialUnitConversionSessionForType(sourceSpatialUnitRecordType, targetSpatialUnitRecordType));
    }


    /**
     *  Gets a text format conversion session. 
     *
     *  @return a <code> FormatConversionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFormatConversion() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.FormatConversionSession getFormatConversionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFormatConversionSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the text format 
     *  conversion service and the given format types. 
     *
     *  @param  sourceFormatType the type of the text format 
     *  @param  targetFormatType the type of the text format 
     *  @return a <code> FormatConversionSession </code> 
     *  @throws org.osid.NullArgumentException <code> sourceFormatType </code> 
     *          or <code> targetFormatType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFormatConversion() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsFormatTypesForConversion(sourceFormatType, 
     *          targetFormatRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.FormatConversionSession getFormatConversionSessionForType(org.osid.type.Type sourceFormatType, 
                                                                                     org.osid.type.Type targetFormatType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFormatConversionSessionForType(sourceFormatType, targetFormatType));
    }


    /**
     *  Gets a calendar informational session session. 
     *
     *  @return a <code> CalendarInfoSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarInfo() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarInfoSession getCalendarInfoSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarInfoSession());
    }


    /**
     *  Gets an <code> OsidSession </code> associated with the calendar 
     *  informational service and the given calendar and time types. 
     *
     *  @param  calendarType the type of the calendar 
     *  @param  timeType the type of the time system 
     *  @return a <code> CalendarInfoSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarType </code> or 
     *          <code> timeType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarType() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsCalendarTimeTypes(calendarType, timeType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.CalendarInfoSession getCalendarInfoSessionForType(org.osid.type.Type calendarType, 
                                                                             org.osid.type.Type timeType)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendarInfoSessionForType(calendarType, timeType));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
