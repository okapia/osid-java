//
// QueueElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.queue.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class QueueElements
    extends net.okapia.osid.jamocha.spi.OsidGovernatorElements {


    /**
     *  Gets the QueueElement Id.
     *
     *  @return the queue element Id
     */

    public static org.osid.id.Id getQueueEntityId() {
        return (makeEntityId("osid.provisioning.Queue"));
    }


    /**
     *  Gets the BrokerId element Id.
     *
     *  @return the BrokerId element Id
     */

    public static org.osid.id.Id getBrokerId() {
        return (makeElementId("osid.provisioning.queue.BrokerId"));
    }


    /**
     *  Gets the Broker element Id.
     *
     *  @return the Broker element Id
     */

    public static org.osid.id.Id getBroker() {
        return (makeElementId("osid.provisioning.queue.Broker"));
    }


    /**
     *  Gets the Size element Id.
     *
     *  @return the Size element Id
     */

    public static org.osid.id.Id getSize() {
        return (makeElementId("osid.provisioning.queue.Size"));
    }


    /**
     *  Gets the EWA element Id.
     *
     *  @return the EWA element Id
     */

    public static org.osid.id.Id getEWA() {
        return (makeElementId("osid.provisioning.queue.EWA"));
    }


    /**
     *  Gets the CanSpecifyProvisionable element Id.
     *
     *  @return the CanSpecifyProvisionable element Id
     */

    public static org.osid.id.Id getCanSpecifyProvisionable() {
        return (makeQueryElementId("osid.provisioning.queue.CanSpecifyProvisionable"));
    }


    /**
     *  Gets the RequestId element Id.
     *
     *  @return the RequestId element Id
     */

    public static org.osid.id.Id getRequestId() {
        return (makeQueryElementId("osid.provisioning.queue.RequestId"));
    }


    /**
     *  Gets the Request element Id.
     *
     *  @return the Request element Id
     */

    public static org.osid.id.Id getRequest() {
        return (makeQueryElementId("osid.provisioning.queue.Request"));
    }


    /**
     *  Gets the DistributorId element Id.
     *
     *  @return the DistributorId element Id
     */

    public static org.osid.id.Id getDistributorId() {
        return (makeQueryElementId("osid.provisioning.queue.DistributorId"));
    }


    /**
     *  Gets the Distributor element Id.
     *
     *  @return the Distributor element Id
     */

    public static org.osid.id.Id getDistributor() {
        return (makeQueryElementId("osid.provisioning.queue.Distributor"));
    }
}
