//
// AbstractBudgetEntryLookupSession.java
//
//    A starter implementation framework for providing a BudgetEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a BudgetEntry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBudgetEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBudgetEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.budgeting.BudgetEntryLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>BudgetEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBudgetEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>BudgetEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBudgetEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>BudgetEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBudgetEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include budget entries in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only budget entries whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveBudgetEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All budget entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveBudgetEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>BudgetEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BudgetEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>BudgetEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  @param  budgetEntryId <code>Id</code> of the
     *          <code>BudgetEntry</code>
     *  @return the budget entry
     *  @throws org.osid.NotFoundException <code>budgetEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>budgetEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntry getBudgetEntry(org.osid.id.Id budgetEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.budgeting.BudgetEntryList budgetEntries = getBudgetEntries()) {
            while (budgetEntries.hasNext()) {
                org.osid.financials.budgeting.BudgetEntry budgetEntry = budgetEntries.getNextBudgetEntry();
                if (budgetEntry.getId().equals(budgetEntryId)) {
                    return (budgetEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(budgetEntryId + " not found");
    }


    /**
     *  Gets a <code>BudgetEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  budgetEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>BudgetEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, budget entries are returned that are currently effective.
     *  In any effective mode, effective budget entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBudgetEntries()</code>.
     *
     *  @param  budgetEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByIds(org.osid.id.IdList budgetEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.financials.budgeting.BudgetEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = budgetEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBudgetEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("budget entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.financials.budgeting.budgetentry.LinkedBudgetEntryList(ret));
    }


    /**
     *  Gets a <code>BudgetEntryList</code> corresponding to the given
     *  budget entry genus <code>Type</code> which does not include
     *  budget entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective budget
     *  entries and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBudgetEntries()</code>.
     *
     *  @param  budgetEntryGenusType a budgetEntry genus type 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByGenusType(org.osid.type.Type budgetEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.BudgetEntryGenusFilterList(getBudgetEntries(), budgetEntryGenusType));
    }


    /**
     *  Gets a <code>BudgetEntryList</code> corresponding to the given
     *  budget entry genus <code>Type</code> and include any additional
     *  budget entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBudgetEntries()</code>.
     *
     *  @param  budgetEntryGenusType a budgetEntry genus type 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByParentGenusType(org.osid.type.Type budgetEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBudgetEntriesByGenusType(budgetEntryGenusType));
    }


    /**
     *  Gets a <code>BudgetEntryList</code> containing the given
     *  budget entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are currently
     *  effective.  In any effective mode, effective budget entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBudgetEntries()</code>.
     *
     *  @param  budgetEntryRecordType a budgetEntry record type 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByRecordType(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.BudgetEntryRecordFilterList(getBudgetEntries(), budgetEntryRecordType));
    }


    /**
     *  Gets a <code>BudgetEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *  
     *  In active mode, budget entries are returned that are currently
     *  active. In any status mode, active and inactive budget entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>BudgetEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.TemporalBudgetEntryFilterList(getBudgetEntries(), from, to));
    }
        

    /**
     *  Gets a list of budget entries corresponding to a budget
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  budgetId the <code>Id</code> of the budget
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>budgetId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudget(org.osid.id.Id budgetId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.BudgetEntryFilterList(new BudgetFilter(budgetId), getBudgetEntries()));
    }


    /**
     *  Gets a list of budget entries corresponding to a budget
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  budgetId the <code>Id</code> of the budget
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>budgetId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetOnDate(org.osid.id.Id budgetId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.TemporalBudgetEntryFilterList(getBudgetEntriesForBudget(budgetId), from, to));
    }


    /**
     *  Gets a list of budget entries corresponding to a account
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  budget entries or an error results. Otherwise, the returned list
     *  may contain only those budget entries that are accessible
     *  through this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  budget entries and those currently expired are returned.
     *
     *  @param  accountId the <code>Id</code> of the account
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>accountId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForAccount(org.osid.id.Id accountId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.BudgetEntryFilterList(new AccountFilter(accountId), getBudgetEntries()));
    }


    /**
     *  Gets a list of budget entries corresponding to a account
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known budget
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those budget entries that are accessible through
     *  this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective budget
     *  entries and those currently expired are returned.
     *
     *  @param  accountId the <code>Id</code> of the account
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>accountId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForAccountOnDate(org.osid.id.Id accountId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.TemporalBudgetEntryFilterList(getBudgetEntriesForAccount(accountId), from, to));
    }


    /**
     *  Gets a list of budget entries corresponding to budget and
     *  account <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known budget
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those budget entries that are accessible through
     *  this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective budget
     *  entries and those currently expired are returned.
     *
     *  @param  budgetId the <code>Id</code> of the budget
     *  @param  accountId the <code>Id</code> of the account
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>budgetId</code>,
     *          <code>accountId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetAndAccount(org.osid.id.Id budgetId,
                                                                                          org.osid.id.Id accountId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.BudgetEntryFilterList(new AccountFilter(accountId), getBudgetEntriesForBudget(budgetId)));
    }


    /**
     *  Gets a list of budget entries corresponding to budget and
     *  account <code>Ids</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known budget
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those budget entries that are accessible through
     *  this session.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective budget
     *  entries and those currently expired are returned.
     *
     *  @param  accountId the <code>Id</code> of the account
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>BudgetEntryList</code>
     *  @throws org.osid.NullArgumentException <code>budgetId</code>,
     *          <code>accountId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesForBudgetAndAccountOnDate(org.osid.id.Id budgetId,
                                                                              org.osid.id.Id accountId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.TemporalBudgetEntryFilterList(getBudgetEntriesForBudgetAndAccount(budgetId, accountId), from, to));
    }


    /**
     *  Gets all <code>BudgetEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known budget
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those budget entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, budget entries are returned that are
     *  currently effective.  In any effective mode, effective budget
     *  entries and those currently expired are returned.
     *
     *  @return a list of <code>BudgetEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.financials.budgeting.BudgetEntryList getBudgetEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the budget entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of budget entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.financials.budgeting.BudgetEntryList filterBudgetEntriesOnViews(org.osid.financials.budgeting.BudgetEntryList list)
        throws org.osid.OperationFailedException {

        org.osid.financials.budgeting.BudgetEntryList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.EffectiveBudgetEntryFilterList(ret);
        }

        return (ret);
    }


    public static class BudgetFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.BudgetEntryFilter {
         
        private final org.osid.id.Id budgetId;
         
         
        /**
         *  Constructs a new <code>BudgetFilter</code>.
         *
         *  @param budgetId the budget to filter
         *  @throws org.osid.NullArgumentException
         *          <code>budgetId</code> is <code>null</code>
         */
        
        public BudgetFilter(org.osid.id.Id budgetId) {
            nullarg(budgetId, "budget Id");
            this.budgetId = budgetId;
            return;
        }

         
        /**
         *  Used by the BudgetEntryFilterList to filter the 
         *  budget entry list based on budget.
         *
         *  @param budgetEntry the budget entry
         *  @return <code>true</code> to pass the budget entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.budgeting.BudgetEntry budgetEntry) {
            return (budgetEntry.getBudgetId().equals(this.budgetId));
        }
    }


    public static class AccountFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.budgeting.budgetentry.BudgetEntryFilter {
         
        private final org.osid.id.Id accountId;
         
         
        /**
         *  Constructs a new <code>AccountFilter</code>.
         *
         *  @param accountId the account to filter
         *  @throws org.osid.NullArgumentException
         *          <code>accountId</code> is <code>null</code>
         */
        
        public AccountFilter(org.osid.id.Id accountId) {
            nullarg(accountId, "account Id");
            this.accountId = accountId;
            return;
        }

         
        /**
         *  Used by the BudgetEntryFilterList to filter the 
         *  budget entry list based on account.
         *
         *  @param budgetEntry the budget entry
         *  @return <code>true</code> to pass the budget entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.budgeting.BudgetEntry budgetEntry) {
            return (budgetEntry.getAccountId().equals(this.accountId));
        }
    }
}
