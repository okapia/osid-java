//
// AbstractFederatingAuthorizationLookupSession.java
//
//     An abstract federating adapter for an AuthorizationLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AuthorizationLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.</p>
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAuthorizationLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.authorization.AuthorizationLookupSession>
    implements org.osid.authorization.AuthorizationLookupSession {

    private boolean parallel = false;
    private org.osid.authorization.Vault vault = new net.okapia.osid.jamocha.nil.authorization.vault.UnknownVault();


    /**
     *  Constructs a new <code>AbstractFederatingAuthorizationLookupSession</code>.
     */

    protected AbstractFederatingAuthorizationLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.authorization.AuthorizationLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Vault/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Vault Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getVaultId() {
        return (this.vault.getId());
    }


    /**
     *  Gets the <code>Vault</code> associated with this 
     *  session.
     *
     *  @return the <code>Vault</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Vault getVault()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.vault);
    }


    /**
     *  Sets the <code>Vault</code>.
     *
     *  @param  vault the vault for this session
     *  @throws org.osid.NullArgumentException <code>vault</code>
     *          is <code>null</code>
     */

    protected void setVault(org.osid.authorization.Vault vault) {
        nullarg(vault, "vault");
        this.vault = vault;
        return;
    }


    /**
     *  Tests if this user can perform <code>Authorization</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuthorizations() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            if (session.canLookupAuthorizations()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Authorization</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuthorizationView() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            session.useComparativeAuthorizationView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Authorization</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuthorizationView() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            session.usePlenaryAuthorizationView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include authorizations in vaults which are children
     *  of this vault in the vault hierarchy.
     */

    @OSID @Override
    public void useFederatedVaultView() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            session.useFederatedVaultView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this vault only.
     */

    @OSID @Override
    public void useIsolatedVaultView() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            session.useIsolatedVaultView();
        }

        return;
    }


    /**
     *  Only authorizations whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveAuthorizationView() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            session.useEffectiveAuthorizationView();
        }

        return;
    }


    /**
     *  All authorizations of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveAuthorizationView() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            session.useAnyEffectiveAuthorizationView();
        }

        return;
    }

    
    /**
     *  Sets the view for methods in this session to implicit
     *  authorizations.  An implicit view will include authorizations
     *  derived from other authorizations as a result of the <code>
     *  Qualifier</code>, <code>Function</code>, or <code> Resource
     *  </code> hierarchies. This method is the opposite of <code>
     *  explicitAuthorizationView()</code>.
     */

    @OSID @Override
    public void useImplicitAuthorizationView() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            session.useImplicitAuthorizationView();
        }

        return;
    }


    /**
     *  Sets the view for methods in this session to explicit
     *  authorizations.  An explicit view includes only those
     *  authorizations that were explicitly defined and not
     *  implied. This method is the opposite of <code>
     *  implicitAuthorizationView(). </code>
     */

    @OSID @Override
    public void useExplicitAuthorizationView() {
        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            session.useExplicitAuthorizationView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Authorization</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Authorization</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Authorization</code> and
     *  retained for compatibility.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  authorizationId <code>Id</code> of the
     *          <code>Authorization</code>
     *  @return the authorization
     *  @throws org.osid.NotFoundException <code>authorizationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>authorizationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Authorization getAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            try {
                return (session.getAuthorization(authorizationId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(authorizationId + " not found");
    }


    /**
     *  Gets an <code>AuthorizationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  authorizations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Authorizations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, authorizations are returned that are currently effective.
     *  In any effective mode, effective authorizations and those currently expired
     *  are returned.
     *
     *  @param  authorizationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByIds(org.osid.id.IdList authorizationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.authorization.authorization.MutableAuthorizationList ret = new net.okapia.osid.jamocha.authorization.authorization.MutableAuthorizationList();

        try (org.osid.id.IdList ids = authorizationIds) {
            while (ids.hasNext()) {
                ret.addAuthorization(getAuthorization(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AuthorizationList</code> corresponding to the given
     *  authorization genus <code>Type</code> which does not include
     *  authorizations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently effective.
     *  In any effective mode, effective authorizations and those currently expired
     *  are returned.
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsByGenusType(authorizationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuthorizationList</code> corresponding to the given
     *  authorization genus <code>Type</code> and include any additional
     *  authorizations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByParentGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsByParentGenusType(authorizationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuthorizationList</code> containing the given
     *  authorization record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @param  authorizationRecordType an authorization record type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByRecordType(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsByRecordType(authorizationRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code> AuthorizationList </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned
     *  list may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective. In any effective mode, active authorizations and those
     *  currently expired are returned.
     *
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> Authorization </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsOnDate(org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceOnDate(org.osid.id.Id resourceId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to a function
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>functionId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForFunction(org.osid.id.Id functionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForFunction(functionId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to a function
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>functionId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForFunctionOnDate(org.osid.id.Id functionId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForFunctionOnDate(functionId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to resource and
     *  function <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  functionId the <code>Id</code> of the function
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>functionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceAndFunction(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id functionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForResourceAndFunction(resourceId, functionId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to resource and function
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>functionId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForResourceAndFunctionOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id functionId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForResourceAndFunctionOnDate(resourceId, functionId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to an agent
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.authorization.AuthorizationList getAuthorizationsForAgent(org.osid.id.Id agentId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForAgent(agentId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to an agent
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentOnDate(org.osid.id.Id agentId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForAgentOnDate(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to agent and
     *  function <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  agentId the <code>Id</code> of the agent
     *  @param  functionId the <code>Id</code> of the function
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>functionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentAndFunction(org.osid.id.Id agentId,
                                                                                            org.osid.id.Id functionId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForAgentAndFunction(agentId, functionId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of authorizations corresponding to agent and function
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible
     *  through this session.
     *
     *  In effective mode, authorizations are returned that are
     *  currently effective.  In any effective mode, effective
     *  authorizations and those currently expired are returned.
     *
     *  @param  functionId the <code>Id</code> of the function
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AuthorizationList</code>
     *  @throws org.osid.NullArgumentException <code>agentId</code>,
     *          <code>functionId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsForAgentAndFunctionOnDate(org.osid.id.Id agentId,
                                                                                                  org.osid.id.Id functionId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsForAgentAndFunctionOnDate(agentId, functionId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of <code> Authorizations </code> associated with a
     *  given qualifier. In plenary mode, the returned list contains
     *  all known authorizations or an error results. Otherwise, the
     *  returned list may contain only those authorizations that are
     *  accessible through this session.
     *
     *  @param  qualifierId a qualifier <code> Id </code>
     *  @return the returned <code> Authorization list </code>
     *  @throws org.osid.NullArgumentException <code> qualifierId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByQualifier(org.osid.id.Id qualifierId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizationsByQualifier(qualifierId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the explicit <code>Authorization</code> that generated
     *  the given implicit authorization. If the given
     *  <code>Authorization</code> is explicit, then the same
     *  <code>Authorization</code> is returned.
     *
     *  @param  authorizationId an authorization
     *  @return the explicit <code>Authorization</code>
     *  @throws org.osid.NotFoundException <code>authorizationId</code> is
     *          not found
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.Authorization getExplicitAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            try {
                return (session.getExplicitAuthorization(authorizationId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(authorizationId + " not found");
    }


    /**
     *  Gets all <code>Authorizations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  authorizations or an error results. Otherwise, the returned list
     *  may contain only those authorizations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, authorizations are returned that are currently
     *  effective.  In any effective mode, effective authorizations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Authorizations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList ret = getAuthorizationList();

        for (org.osid.authorization.AuthorizationLookupSession session : getSessions()) {
            ret.addAuthorizationList(session.getAuthorizations());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.authorization.authorization.FederatingAuthorizationList getAuthorizationList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.authorization.ParallelAuthorizationList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.authorization.authorization.CompositeAuthorizationList());
        }
    }
}
