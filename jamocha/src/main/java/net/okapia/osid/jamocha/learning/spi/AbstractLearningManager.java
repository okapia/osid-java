//
// AbstractLearningManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractLearningManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.learning.LearningManager,
               org.osid.learning.LearningProxyManager {

    private final Types objectiveRecordTypes               = new TypeRefSet();
    private final Types objectiveSearchRecordTypes         = new TypeRefSet();

    private final Types activityRecordTypes                = new TypeRefSet();
    private final Types activitySearchRecordTypes          = new TypeRefSet();

    private final Types proficiencyRecordTypes             = new TypeRefSet();
    private final Types proficiencySearchRecordTypes       = new TypeRefSet();

    private final Types objectiveBankRecordTypes           = new TypeRefSet();
    private final Types objectiveBankSearchRecordTypes     = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractLearningManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractLearningManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if an objective lookup service is supported. An objective lookup 
     *  service defines methods to access objectives. 
     *
     *  @return true if objective lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveLookup() {
        return (false);
    }


    /**
     *  Tests if an objective query service is supported. 
     *
     *  @return <code> true </code> if objective query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveQuery() {
        return (false);
    }


    /**
     *  Tests if an objective search service is supported. 
     *
     *  @return <code> true </code> if objective search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSearch() {
        return (false);
    }


    /**
     *  Tests if an objective administrative service is supported. 
     *
     *  @return <code> true </code> if objective admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveAdmin() {
        return (false);
    }


    /**
     *  Tests if objective notification is supported. Messages may be sent 
     *  when objectives are created, modified, or deleted. 
     *
     *  @return <code> true </code> if objective notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveNotification() {
        return (false);
    }


    /**
     *  Tests if an objective hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an objective hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveHierarchy() {
        return (false);
    }


    /**
     *  Tests if an objective hierarchy design is supported. 
     *
     *  @return <code> true </code> if an objective hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if an objective sequencing design is supported. 
     *
     *  @return <code> true </code> if objective sequencing is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSequencing() {
        return (false);
    }


    /**
     *  Tests if an objective to objective bank lookup session is available. 
     *
     *  @return <code> true </code> if objective objective bank lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveObjectiveBank() {
        return (false);
    }


    /**
     *  Tests if an objective to objective bank assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if objective objective bank assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveObjectiveBankAssignment() {
        return (false);
    }


    /**
     *  Tests if an objective smart objective bank cataloging service is 
     *  supported. 
     *
     *  @return <code> true </code> if objective smart objective banks are 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSmartObjectiveBank() {
        return (false);
    }


    /**
     *  Tests if an objective requisite service is supported.
     *
     *  @return <code> true </code> if objective requisite service is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsObjectiveRequisite() {
        return (false);
    }


    /**
     *  Tests if an objective requisite assignment service is supported.
     *
     *  @return <code> true </code> if objective requisite assignment service
     *          is supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsObjectiveRequisiteAssignment() {
        return (false);
    }


    /**
     *  Tests if an activity lookup service is supported. 
     *
     *  @return <code> true </code> if activity lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityLookup() {
        return (false);
    }


    /**
     *  Tests if an activity query service is supported. 
     *
     *  @return <code> true </code> if activity query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Tests if an activity search service is supported. 
     *
     *  @return <code> true </code> if activity search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearch() {
        return (false);
    }


    /**
     *  Tests if an activity administrative service is supported. 
     *
     *  @return <code> true </code> if activity admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityAdmin() {
        return (false);
    }


    /**
     *  Tests if activity notification is supported. Messages may be sent when 
     *  activities are created, modified, or deleted. 
     *
     *  @return <code> true </code> if activity notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityNotification() {
        return (false);
    }


    /**
     *  Tests if an activity to objective bank lookup session is available. 
     *
     *  @return <code> true </code> if activity objective bank lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityObjectiveBank() {
        return (false);
    }


    /**
     *  Tests if an activity to objective bank assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if activity objective bank assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityObjectiveBankAssignment() {
        return (false);
    }


    /**
     *  Tests if an activity smart objective bank cataloging service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity smart objective banks are 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySmartObjectiveBank() {
        return (false);
    }


    /**
     *  Tests if looking up proficiencies is supported. 
     *
     *  @return <code> true </code> if proficiency lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyLookup() {
        return (false);
    }


    /**
     *  Tests if querying proficiencies is supported. 
     *
     *  @return <code> true </code> if proficiency query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyQuery() {
        return (false);
    }


    /**
     *  Tests if searching proficiencies is supported. 
     *
     *  @return <code> true </code> if proficiency search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencySearch() {
        return (false);
    }


    /**
     *  Tests if proficiency <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if proficiency administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyAdmin() {
        return (false);
    }


    /**
     *  Tests if a proficiency <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if proficiency notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyNotification() {
        return (false);
    }


    /**
     *  Tests if a proficiency objective bank mapping lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a proficiency objective bank lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyObjectiveBank() {
        return (false);
    }


    /**
     *  Tests if a proficiency objective bank mapping service is supported. 
     *
     *  @return <code> true </code> if proficiency to objective bank mapping 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencyObjectiveBankAssignment() {
        return (false);
    }


    /**
     *  Tests if a proficiency smart objective bank cataloging service is 
     *  supported. 
     *
     *  @return <code> true </code> if proficiency smart objective banks are 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProficiencySmartObjectiveBank() {
        return (false);
    }


    /**
     *  Tests if a learning path service is supported for the authenticated 
     *  agent. 
     *
     *  @return <code> true </code> if learning path is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyLearningPath() {
        return (false);
    }


    /**
     *  Tests if a learning path service is supported. 
     *
     *  @return <code> true </code> if learning path is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningPath() {
        return (false);
    }


    /**
     *  Tests if an objective bank lookup service is supported. 
     *
     *  @return <code> true </code> if objective bank lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankLookup() {
        return (false);
    }


    /**
     *  Tests if an objective bank query service is supported. 
     *
     *  @return <code> true </code> if objective bank query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Tests if an objective bank search service is supported. 
     *
     *  @return <code> true </code> if objective bank search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankSearch() {
        return (false);
    }


    /**
     *  Tests if an objective bank administrative service is supported. 
     *
     *  @return <code> true </code> if objective bank admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankAdmin() {
        return (false);
    }


    /**
     *  Tests if objective bank notification is supported. Messages may be 
     *  sent when objective banks are created, modified, or deleted. 
     *
     *  @return <code> true </code> if objective bank notification is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankNotification() {
        return (false);
    }


    /**
     *  Tests if an objective bank hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an objective bank hierarchy traversal 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankHierarchy() {
        return (false);
    }


    /**
     *  Tests if objective bank hierarchy design is supported. 
     *
     *  @return <code> true </code> if an objective bank hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a learning batch service is supported. 
     *
     *  @return <code> true </code> if a learning batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Objective </code> record types. 
     *
     *  @return a list containing the supported <code> Objective </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObjectiveRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.objectiveRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Objective </code> record type is supported. 
     *
     *  @param  objectiveRecordType a <code> Type </code> indicating an <code> 
     *          Objective </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> objectiveRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectiveRecordType(org.osid.type.Type objectiveRecordType) {
        return (this.objectiveRecordTypes.contains(objectiveRecordType));
    }


    /**
     *  Adds support for an objective record type.
     *
     *  @param objectiveRecordType an objective record type
     *  @throws org.osid.NullArgumentException
     *  <code>objectiveRecordType</code> is <code>null</code>
     */

    protected void addObjectiveRecordType(org.osid.type.Type objectiveRecordType) {
        this.objectiveRecordTypes.add(objectiveRecordType);
        return;
    }


    /**
     *  Removes support for an objective record type.
     *
     *  @param objectiveRecordType an objective record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>objectiveRecordType</code> is <code>null</code>
     */

    protected void removeObjectiveRecordType(org.osid.type.Type objectiveRecordType) {
        this.objectiveRecordTypes.remove(objectiveRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Objective </code> search record types. 
     *
     *  @return a list containing the supported <code> Objective </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObjectiveSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.objectiveSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Objective </code> search record type is 
     *  supported. 
     *
     *  @param  objectiveSearchRecordType a <code> Type </code> indicating an 
     *          <code> Objective </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          objectiveSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectiveSearchRecordType(org.osid.type.Type objectiveSearchRecordType) {
        return (this.objectiveSearchRecordTypes.contains(objectiveSearchRecordType));
    }


    /**
     *  Adds support for an objective search record type.
     *
     *  @param objectiveSearchRecordType an objective search record type
     *  @throws org.osid.NullArgumentException
     *  <code>objectiveSearchRecordType</code> is <code>null</code>
     */

    protected void addObjectiveSearchRecordType(org.osid.type.Type objectiveSearchRecordType) {
        this.objectiveSearchRecordTypes.add(objectiveSearchRecordType);
        return;
    }


    /**
     *  Removes support for an objective search record type.
     *
     *  @param objectiveSearchRecordType an objective search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>objectiveSearchRecordType</code> is <code>null</code>
     */

    protected void removeObjectiveSearchRecordType(org.osid.type.Type objectiveSearchRecordType) {
        this.objectiveSearchRecordTypes.remove(objectiveSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Activity </code> record types. 
     *
     *  @return a list containing the supported <code> Activity </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activityRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Activity </code> record type is supported. 
     *
     *  @param  activityRecordType a <code> Type </code> indicating a <code> 
     *          Activity </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityRecordType(org.osid.type.Type activityRecordType) {
        return (this.activityRecordTypes.contains(activityRecordType));
    }


    /**
     *  Adds support for an activity record type.
     *
     *  @param activityRecordType an activity record type
     *  @throws org.osid.NullArgumentException
     *  <code>activityRecordType</code> is <code>null</code>
     */

    protected void addActivityRecordType(org.osid.type.Type activityRecordType) {
        this.activityRecordTypes.add(activityRecordType);
        return;
    }


    /**
     *  Removes support for an activity record type.
     *
     *  @param activityRecordType an activity record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activityRecordType</code> is <code>null</code>
     */

    protected void removeActivityRecordType(org.osid.type.Type activityRecordType) {
        this.activityRecordTypes.remove(activityRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Activity </code> search record types. 
     *
     *  @return a list containing the supported <code> Activity </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivitySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.activitySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Activity </code> search record type is 
     *  supported. 
     *
     *  @param  activitySearchRecordType a <code> Type </code> indicating a 
     *          <code> Activity </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activitySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        return (this.activitySearchRecordTypes.contains(activitySearchRecordType));
    }


    /**
     *  Adds support for an activity search record type.
     *
     *  @param activitySearchRecordType an activity search record type
     *  @throws org.osid.NullArgumentException
     *  <code>activitySearchRecordType</code> is <code>null</code>
     */

    protected void addActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        this.activitySearchRecordTypes.add(activitySearchRecordType);
        return;
    }


    /**
     *  Removes support for an activity search record type.
     *
     *  @param activitySearchRecordType an activity search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>activitySearchRecordType</code> is <code>null</code>
     */

    protected void removeActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        this.activitySearchRecordTypes.remove(activitySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Proficiency </code> record types. 
     *
     *  @return a list containing the supported <code> Proficiency </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProficiencyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.proficiencyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Proficiency </code> record type is 
     *  supported. 
     *
     *  @param  proficiencyRecordType a <code> Type </code> indicating a 
     *          <code> Proficiency </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> proficiencyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProficiencyRecordType(org.osid.type.Type proficiencyRecordType) {
        return (this.proficiencyRecordTypes.contains(proficiencyRecordType));
    }


    /**
     *  Adds support for a proficiency record type.
     *
     *  @param proficiencyRecordType a proficiency record type
     *  @throws org.osid.NullArgumentException
     *  <code>proficiencyRecordType</code> is <code>null</code>
     */

    protected void addProficiencyRecordType(org.osid.type.Type proficiencyRecordType) {
        this.proficiencyRecordTypes.add(proficiencyRecordType);
        return;
    }


    /**
     *  Removes support for a proficiency record type.
     *
     *  @param proficiencyRecordType a proficiency record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>proficiencyRecordType</code> is <code>null</code>
     */

    protected void removeProficiencyRecordType(org.osid.type.Type proficiencyRecordType) {
        this.proficiencyRecordTypes.remove(proficiencyRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Proficiency </code> search types. 
     *
     *  @return a list containing the supported <code> Proficiency </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProficiencySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.proficiencySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Proficiency </code> search type is 
     *  supported. 
     *
     *  @param  proficiencySearchRecordType a <code> Type </code> indicating a 
     *          <code> Proficiency </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          proficiencySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProficiencySearchRecordType(org.osid.type.Type proficiencySearchRecordType) {
        return (this.proficiencySearchRecordTypes.contains(proficiencySearchRecordType));
    }


    /**
     *  Adds support for a proficiency search record type.
     *
     *  @param proficiencySearchRecordType a proficiency search record type
     *  @throws org.osid.NullArgumentException
     *  <code>proficiencySearchRecordType</code> is <code>null</code>
     */

    protected void addProficiencySearchRecordType(org.osid.type.Type proficiencySearchRecordType) {
        this.proficiencySearchRecordTypes.add(proficiencySearchRecordType);
        return;
    }


    /**
     *  Removes support for a proficiency search record type.
     *
     *  @param proficiencySearchRecordType a proficiency search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>proficiencySearchRecordType</code> is <code>null</code>
     */

    protected void removeProficiencySearchRecordType(org.osid.type.Type proficiencySearchRecordType) {
        this.proficiencySearchRecordTypes.remove(proficiencySearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ObjectiveBank </code> record types. 
     *
     *  @return a list containing the supported <code> ObjectiveBank </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObjectiveBankRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.objectiveBankRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ObjectiveBank </code> record type is 
     *  supported. 
     *
     *  @param  objectiveBankRecordType a <code> Type </code> indicating an 
     *          <code> ObjectiveBank </code> type 
     *  @return <code> true </code> if the given objective bank record <code> 
     *          Type </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> objectiveBankRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectiveBankRecordType(org.osid.type.Type objectiveBankRecordType) {
        return (this.objectiveBankRecordTypes.contains(objectiveBankRecordType));
    }


    /**
     *  Adds support for an objective bank record type.
     *
     *  @param objectiveBankRecordType an objective bank record type
     *  @throws org.osid.NullArgumentException
     *  <code>objectiveBankRecordType</code> is <code>null</code>
     */

    protected void addObjectiveBankRecordType(org.osid.type.Type objectiveBankRecordType) {
        this.objectiveBankRecordTypes.add(objectiveBankRecordType);
        return;
    }


    /**
     *  Removes support for an objective bank record type.
     *
     *  @param objectiveBankRecordType an objective bank record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>objectiveBankRecordType</code> is <code>null</code>
     */

    protected void removeObjectiveBankRecordType(org.osid.type.Type objectiveBankRecordType) {
        this.objectiveBankRecordTypes.remove(objectiveBankRecordType);
        return;
    }


    /**
     *  Gets the supported objective bank search record types. 
     *
     *  @return a list containing the supported <code> ObjectiveBank </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObjectiveBankSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.objectiveBankSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given objective bank search record type is supported. 
     *
     *  @param  objectiveBankSearchRecordType a <code> Type </code> indicating 
     *          an <code> ObjectiveBank </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          objectiveBankSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObjectiveBankSearchRecordType(org.osid.type.Type objectiveBankSearchRecordType) {
        return (this.objectiveBankSearchRecordTypes.contains(objectiveBankSearchRecordType));
    }


    /**
     *  Adds support for an objective bank search record type.
     *
     *  @param objectiveBankSearchRecordType an objective bank search record type
     *  @throws org.osid.NullArgumentException
     *  <code>objectiveBankSearchRecordType</code> is <code>null</code>
     */

    protected void addObjectiveBankSearchRecordType(org.osid.type.Type objectiveBankSearchRecordType) {
        this.objectiveBankSearchRecordTypes.add(objectiveBankSearchRecordType);
        return;
    }


    /**
     *  Removes support for an objective bank search record type.
     *
     *  @param objectiveBankSearchRecordType an objective bank search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>objectiveBankSearchRecordType</code> is <code>null</code>
     */

    protected void removeObjectiveBankSearchRecordType(org.osid.type.Type objectiveBankSearchRecordType) {
        this.objectiveBankSearchRecordTypes.remove(objectiveBankSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  lookup service. 
     *
     *  @return an <code> ObjectiveLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveLookupSession getObjectiveLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveLookupSession getObjectiveLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveLookupSession getObjectiveLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveLookupSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return <code> an ObjectiveLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveLookupSession getObjectiveLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveLookupSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  query service. 
     *
     *  @return an <code> ObjectiveQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuerySession getObjectiveQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuerySession getObjectiveQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  query service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuerySession getObjectiveQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveQuerySessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  query service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return <code> an ObjectiveQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuerySession getObjectiveQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveQuerySessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  search service. 
     *
     *  @return an <code> ObjectiveSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchSession getObjectiveSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchSession getObjectiveSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchSession getObjectiveSearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveSearchSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return <code> an ObjectiveSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchSession getObjectiveSearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveSearchSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  administration service. 
     *
     *  @return an <code> ObjectiveAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveAdminSession getObjectiveAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveAdminSession getObjectiveAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  admin service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveAdminSession getObjectiveAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveAdminSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  admin service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return <code> an ObjectiveAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveAdminSession getObjectiveAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveAdminSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  objective changes. 
     *
     *  @param  objectiveReceiver the objective receiver 
     *  @return an <code> ObjectiveNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveNotificationSession getObjectiveNotificationSession(org.osid.learning.ObjectiveReceiver objectiveReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  objective changes. 
     *
     *  @param  objectiveReceiver the objective receiver 
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveNotificationSession getObjectiveNotificationSession(org.osid.learning.ObjectiveReceiver objectiveReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  notification service for the given objective bank. 
     *
     *  @param  objectiveReceiver the objective receiver 
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ObjectiveNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveReceiver 
     *          </code> or <code> objectiveBankId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveNotificationSession getObjectiveNotificationSessionForObjectiveBank(org.osid.learning.ObjectiveReceiver objectiveReceiver, 
                                                                                                          org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveNotificationSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  notification service for the given objective bank. 
     *
     *  @param  objectiveReceiver the objective receiver 
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return <code> an ObjectiveNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveReceiver, 
     *          objectiveBankId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveNotificationSession getObjectiveNotificationSessionForObjectiveBank(org.osid.learning.ObjectiveReceiver objectiveReceiver, 
                                                                                                          org.osid.id.Id objectiveBankId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveNotificationSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the session for traversing objective hierarchies. 
     *
     *  @return an <code> ObjectiveHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchySession getObjectiveHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveHierarchySession not implemented");
    }


    /**
     *  Gets the session for traversing objective hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchySession getObjectiveHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  hierarchy traversal service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ObjectiveHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchySession getObjectiveHierarchySessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveHierarchySessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  hierarchy traversal service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveHierarchySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchy() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchySession getObjectiveHierarchySessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveHierarchySessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the session for designing objective hierarchies. 
     *
     *  @return an <code> ObjectiveHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchyDesignSession getObjectiveHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session for designing objective hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchyDesignSession getObjectiveHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  hierarchy design service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ObjectiveHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchyDesignSession getObjectiveHierarchyDesignSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveHierarchyDesignSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  hierarchy design service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveHierarchyDesignSession getObjectiveHierarchyDesignSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveHierarchyDesignSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the session for sequencing objectives. 
     *
     *  @return an <code> ObjectiveSequencingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSequencingSession getObjectiveSequencingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveSequencingSession not implemented");
    }


    /**
     *  Gets the session for sequencing objectives. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveSequencingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSequencingSession getObjectiveSequencingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveSequencingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  sequencing service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ObjectiveSequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSequencingSession getObjectiveSequencingSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveSequencingSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective 
     *  sequencing service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveSequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSequencingSession getObjectiveSequencingSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveSequencingSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the session for retrieving objective to objective bank mappings. 
     *
     *  @return an <code> ObjectiveObjectiveBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveObjectiveBankSession getObjectiveObjectiveBankSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveObjectiveBankSession not implemented");
    }


    /**
     *  Gets the session for retrieving objective to objective bank mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveObjectiveBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveObjectiveBankSession getObjectiveObjectiveBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveObjectiveBankSession not implemented");
    }


    /**
     *  Gets the session for assigning objective to objective bank mappings. 
     *
     *  @return an <code> ObjectiveObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveObjectiveBankAssignmentSession getObjectiveObjectiveBankAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveObjectiveBankAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning objective to objective bank mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveObjectiveBankAssignmentSession getObjectiveObjectiveBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveObjectiveBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of objectives. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return an <code> ObjectiveSmartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSmartObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSmartObjectiveBankSession getObjectiveSmartObjectiveBankSession(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveSmartObjectiveBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of objectives. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObjectivemartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSmartObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySmartObjectiveBankSession getObjectiveSmartObjectiveBankSession(org.osid.id.Id objectiveBankId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveSmartObjectiveBankSession not implemented");
    }


    /**
     *  Gets the session for examining objective requisites.
     *
     *  @return an <code> ObjectiveRequisiteSession </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code>
     *          supportsObjectiveRequisite() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteSession getObjectiveRequisiteSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveRequisiteSession not implemented");
    }


    /**
     *  Gets the session for examining objective requisites.
     *
     *  @param  proxy a proxy
     *  @return an <code> ObjectiveRequisiteSession </code>
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code>
     *  supportsObjectiveRequisite() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteSession getObjectiveRequisiteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveRequisiteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective
     *  sequencing service for the given objective bank.
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank
     *  @return an <code> ObjectiveRequisiteSession </code>
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not
     *          found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException <code> unable to complete
     *          request </code>
     *  @throws org.osid.UnimplementedException <code>
     *  supportsObjectiveRequisite() </code> or <code>
     *          supportsVisibleFederation() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteSession getObjectiveRequisiteSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveRequisiteSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective
     *  sequencing service for the given objective bank.
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank
     *  @param  proxy a proxy
     *  @return an <code> ObjectiveRequisiteSession </code>
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not
     *          found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code>
     *          or <code> proxy </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException <code> unable to complete
     *          request </code>
     *  @throws org.osid.UnimplementedException <code>
     *          supportsObjectiveRequisite() </code> or <code>
     *          supportsVisibleFederation() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteSession getObjectiveRequisiteSessionForObjectiveBank(org.osid.id.Id objectiveBankId,
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveRequisiteSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the session for managing objective requisites.
     *
     *  @return an <code> ObjectiveRequisiteAssignmentSession </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code>
     *          supportsObjectiveRequisiteAssignment() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteAssignmentSession getObjectiveRequisiteAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveRequisiteAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing objective requisites.
     *
     *  @param  proxy a proxy
     *  @return an <code> ObjectiveRequisiteAssignmentSession </code>
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnimplementedException <code>
     *          supportsObjectiveRequisiteAssignment() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteAssignmentSession getObjectiveRequisiteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveRequisiteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective
     *  sequencing service for the given objective bank.
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank
     *  @return an <code> ObjectiveRequisiteAssignmentSession </code>
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not
     *          found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException <code> unable to complete
     *          request </code>
     *  @throws org.osid.UnimplementedException <code>
     *          supportsObjectiveRequisiteAssignment() </code> or <code>
     *          supportsVisibleFederation() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteAssignmentSession getObjectiveRequisiteAssignmentSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {
     
        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveRequisiteAssignmentSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the objective
     *  sequencing service for the given objective bank.
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank
     *  @param  proxy a proxy
     *  @return an <code> ObjectiveRequisiteAssignmentSession </code>
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not
     *          found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code>
     *          or <code> proxy </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException <code> unable to complete
     *          request </code>
     *  @throws org.osid.UnimplementedException <code>
     *          supportsObjectiveRequisiteAssignment() </code> or <code>
     *          supportsVisibleFederation() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.learning.ObjectiveRequisiteAssignmentSession getObjectiveRequisiteAssignmentSessionForObjectiveBank(org.osid.id.Id objectiveBankId,                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveRequisiteAssignmentSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityLookupSession getActivityLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityLookupSession getActivityLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityLookupSession getActivityLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityLookupSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityLookupSession getActivityLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityLookupSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @return a <code> ActivityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuerySession getActivityQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuerySession getActivityQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuerySession getActivityQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityQuerySessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityQuerySession getActivityQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityQuerySessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @return a <code> ActivitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySearchSession getActivitySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySearchSession getActivitySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivitySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySearchSession getActivitySearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivitySearchSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySearchSession getActivitySearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivitySearchSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @return a <code> ActivityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityAdminSession getActivityAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityAdminSession getActivityAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity admin 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityAdminSession getActivityAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityAdminSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity admin 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return a <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityAdminSession getActivityAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityAdminSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to activity 
     *  changes. 
     *
     *  @param  activityReceiver the activity receiver 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityNotificationSession getActivityNotificationSession(org.osid.learning.ActivityReceiver activityReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to activity 
     *  changes. 
     *
     *  @param  activityReceiver the activity receiver 
     *  @param  proxy a proxy 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityNotificationSession getActivityNotificationSession(org.osid.learning.ActivityReceiver activityReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given objective bank. 
     *
     *  @param  activityReceiver the activity receiver 
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @return <code> an ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> objectiveBankId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityNotificationSession getActivityNotificationSessionForObjectiveBank(org.osid.learning.ActivityReceiver activityReceiver, 
                                                                                                        org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityNotificationSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given objective bank. 
     *
     *  @param  activityReceiver the activity receiver 
     *  @param  objectiveBankId the <code> Id </code> of the objective bank 
     *  @param  proxy a proxy 
     *  @return <code> an ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> activityReceiver, 
     *          objectiveBankId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityNotificationSession getActivityNotificationSessionForObjectiveBank(org.osid.learning.ActivityReceiver activityReceiver, 
                                                                                                        org.osid.id.Id objectiveBankId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityNotificationSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the session for retrieving activity to objective bank mappings. 
     *
     *  @return an <code> ActivityObjectiveBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityObjectiveBankSession getActivityObjectiveBankSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityObjectiveBankSession not implemented");
    }


    /**
     *  Gets the session for retrieving activity to objective bank mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivityObjectiveBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityObjectiveBankSession getActivityObjectiveBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityObjectiveBankSession not implemented");
    }


    /**
     *  Gets the session for assigning activity to objective bank mappings. 
     *
     *  @return an <code> ActivityObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityObjectiveBankAssignmentSession getActivityObjectiveBankAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivityObjectiveBankAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning activity to objective bank mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActivityObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivityObjectiveBankAssignmentSession getActivityObjectiveBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivityObjectiveBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of activities. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return an <code> ActivitySmartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySmartObjectiveBankSession getActivitySmartObjectiveBankSession(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getActivitySmartObjectiveBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of activities. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActivitySmartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ActivitySmartObjectiveBankSession getActivitySmartObjectiveBankSession(org.osid.id.Id objectiveBankId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getActivitySmartObjectiveBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  lookup service. 
     *
     *  @return a <code> ProficiencyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyLookupSession getProficiencyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyLookupSession getProficiencyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the obective bank 
     *  @return a <code> ProficiencyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyLookupSession getProficiencyLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyLookupSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  lookup service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the obective bank 
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyLookupSession getProficiencyLookupSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyLookupSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  query service. 
     *
     *  @return a <code> ProficiencyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyQuerySession getProficiencyQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyQuerySession getProficiencyQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  query service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the obective bank 
     *  @return a <code> ProficiencyQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyQuerySession getProficiencyQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyQuerySessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  query service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the obective bank 
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> ObjectiveBank </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyQuerySession getProficiencyQuerySessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyQuerySessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  search service. 
     *
     *  @return a <code> ProficiencySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySearchSession getProficiencySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySearchSession getProficiencySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> ProficiencySearchSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySearchSession getProficiencySearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencySearchSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  search service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencySearchSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySearchSession getProficiencySearchSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencySearchSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  administration service. 
     *
     *  @return a <code> ProficiencyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyAdminSession getProficiencyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyAdminSession getProficiencyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  administration service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> ProficiencyAdminSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyAdminSession getProficiencyAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyAdminSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  administration service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyAdminSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyAdminSession getProficiencyAdminSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyAdminSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  notification service. 
     *
     *  @param  proficiencyReceiver the notification callback 
     *  @return a <code> ProficiencyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proficiencyReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyNotificationSession getProficiencyNotificationSession(org.osid.learning.ProficiencyReceiver proficiencyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  notification service. 
     *
     *  @param  proficiencyReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proficiencyReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyNotificationSession getProficiencyNotificationSession(org.osid.learning.ProficiencyReceiver proficiencyReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  notification service for the given objective bank. 
     *
     *  @param  proficiencyReceiver the notification callback 
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> ProficiencyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proficiencyReceiver 
     *          </code> or <code> objectiveBankId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyNotificationSession getProficiencyNotificationSessionForObjectiveBank(org.osid.learning.ProficiencyReceiver proficiencyReceiver, 
                                                                                                              org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyNotificationSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the proficiency 
     *  notification service for the given objective bank. 
     *
     *  @param  proficiencyReceiver the notification callback 
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyNotificationSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proficiencyReceiver, 
     *          objectiveBankId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyNotificationSession getProficiencyNotificationSessionForObjectiveBank(org.osid.learning.ProficiencyReceiver proficiencyReceiver, 
                                                                                                              org.osid.id.Id objectiveBankId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyNotificationSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup proficiency/objective 
     *  bank mappings. 
     *
     *  @return a <code> ProficiencyObjectiveBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyObjectiveBankSession getProficiencyObjectiveBankSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyObjectiveBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup proficiency/objective 
     *  bank mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyObjectiveBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyObjectiveBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyObjectiveBankSession getProficiencyObjectiveBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyObjectiveBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  proficiencys to objective banks. 
     *
     *  @return a <code> ProficiencyObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyObjectiveBankAssignmentSession getProficiencyObjectiveBankAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencyObjectiveBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  proficiencies to objective banks. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencyObjectiveBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencyObjectiveBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyObjectiveBankAssignmentSession getProficiencyObjectiveBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencyObjectiveBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of objectives. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> ProficiencySmartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySmartObjectiveBank() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySmartObjectiveBankSession getProficiencySmartObjectiveBankSession(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getProficiencySmartObjectiveBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage dynamic objective banks 
     *  of proficiencies. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProficiencySmartObjectiveBankSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProficiencySmartObjectiveBank() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.learning.ProficiencySmartObjectiveBankSession getProficiencySmartObjectiveBankSession(org.osid.id.Id objectiveBankId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getProficiencySmartObjectiveBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my learning 
     *  path service. 
     *
     *  @return a <code> MyLearningPathSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLearningPath() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.MyLearningPathSession getMyLearningPathSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getMyLearningPathSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my learning 
     *  path service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyLearningPathSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLearningPath() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.MyLearningPathSession getMyLearningPathSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getMyLearningPathSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my learning 
     *  path service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> MyLearningPathSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLearningPath() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.MyLearningPathSession getMyLearningPathSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getMyLearningPathSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my learning 
     *  path service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MyLearningPathSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyLearningPath() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.MyLearningPathSession getMyLearningPathSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getMyLearningPathSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the learning path 
     *  service. 
     *
     *  @return a <code> LearningPathSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLearningPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningPathSession getLearningPathSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getLearningPathSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the learning path 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LearningPathSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLearningPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningPathSession getLearningPathSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getLearningPathSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the learning path 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @return a <code> LearningPathSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportyLearningPath() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningPathSession getLearningPathSessionForObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getLearningPathSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the learning path 
     *  service for the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> of the <code> 
     *          ObjectiveBank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LearningPathSession </code> 
     *  @throws org.osid.NotFoundException no objective bank found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportyLearningPath() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningPathSession getLearningPathSessionForObjectiveBank(org.osid.id.Id objectiveBankId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getLearningPathSessionForObjectiveBank not implemented");
    }


    /**
     *  Gets the OsidSession associated with the objective bank lookup 
     *  service. 
     *
     *  @return an <code> ObjectiveBankLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankLookupSession getObjectiveBankLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveBankLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the objective bank lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBankLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankLookupSession getObjectiveBankLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveBankLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the objective bank query service. 
     *
     *  @return an <code> ObjectiveBankQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankQuery() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuerySession getObjectiveBankQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveBankQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the objective bank query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBankQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankQuery() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuerySession getObjectiveBankQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveBankQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the objective bank search 
     *  service. 
     *
     *  @return an <code> ObjectiveBankSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankSearchSession getObjectiveBankSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveBankSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the objective bank search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBankSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankSearchSession getObjectiveBankSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveBankSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the objective bank administration 
     *  service. 
     *
     *  @return an <code> ObjectiveBankAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankAdmin() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankAdminSession getObjectiveBankAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveBankAdminSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the objective bank administration 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBankAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankAdmin() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankAdminSession getObjectiveBankAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveBankAdminSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  objective bank service changes. 
     *
     *  @param  objectiveBankReceiver the objective bank receiver 
     *  @return an <code> ObjectiveBankNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankNotificationSession getObjectiveBankNotificationSession(org.osid.learning.ObjectiveBankReceiver objectiveBankReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveBankNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  objective bank service changes. 
     *
     *  @param  objectiveBankReceiver the objective bank receiver 
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBankNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> objectiveBankReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankNotificationSession getObjectiveBankNotificationSession(org.osid.learning.ObjectiveBankReceiver objectiveBankReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveBankNotificationSession not implemented");
    }


    /**
     *  Gets the session traversing objective bank hierarchies. 
     *
     *  @return an <code> ObjectiveBankHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankHierarchySession getObjectiveBankHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveBankHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing objective bank hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBankHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankHierarchySession getObjectiveBankHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveBankHierarchySession not implemented");
    }


    /**
     *  Gets the session designing objective bank hierarchies. 
     *
     *  @return an <code> ObjectiveBankHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankHierarchyDesignSession getObjectiveBankHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getObjectiveBankHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing objective bank hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObjectiveBankHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankHierarchyDesignSession getObjectiveBankHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getObjectiveBankHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> LearningBatchManager. </code> 
     *
     *  @return a <code> LearningBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLearningBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.LearningBatchManager getLearningBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningManager.getLearningBatchManager not implemented");
    }


    /**
     *  Gets a <code> LearningBatchProxyManager. </code> 
     *
     *  @return a <code> LearningBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLearningBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.learning.batch.LearningBatchProxyManager getLearningBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.learning.LearningProxyManager.getLearningBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.objectiveRecordTypes.clear();
        this.objectiveRecordTypes.clear();

        this.objectiveSearchRecordTypes.clear();
        this.objectiveSearchRecordTypes.clear();

        this.activityRecordTypes.clear();
        this.activityRecordTypes.clear();

        this.activitySearchRecordTypes.clear();
        this.activitySearchRecordTypes.clear();

        this.proficiencyRecordTypes.clear();
        this.proficiencyRecordTypes.clear();

        this.proficiencySearchRecordTypes.clear();
        this.proficiencySearchRecordTypes.clear();

        this.objectiveBankRecordTypes.clear();
        this.objectiveBankRecordTypes.clear();

        this.objectiveBankSearchRecordTypes.clear();
        this.objectiveBankSearchRecordTypes.clear();

        return;
    }
}
