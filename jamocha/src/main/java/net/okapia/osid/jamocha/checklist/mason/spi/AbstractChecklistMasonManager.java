//
// AbstractChecklistMasonManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractChecklistMasonManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.checklist.mason.ChecklistMasonManager,
               org.osid.checklist.mason.ChecklistMasonProxyManager {

    private final Types todoProducerRecordTypes            = new TypeRefSet();
    private final Types todoProducerSearchRecordTypes      = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractChecklistMasonManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractChecklistMasonManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up todo producers is supported. 
     *
     *  @return <code> true </code> if todo producer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerLookup() {
        return (false);
    }


    /**
     *  Tests if querying todo producers is supported. 
     *
     *  @return <code> true </code> if todo producer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerQuery() {
        return (false);
    }


    /**
     *  Tests if searching todo producers is supported. 
     *
     *  @return <code> true </code> if todo producer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerSearch() {
        return (false);
    }


    /**
     *  Tests if a todo producer administrative service is supported. 
     *
     *  @return <code> true </code> if todo producer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerAdmin() {
        return (false);
    }


    /**
     *  Tests if a todo producer notification service is supported. 
     *
     *  @return <code> true </code> if todo producer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerNotification() {
        return (false);
    }


    /**
     *  Tests if a todo producer checklist lookup service is supported. 
     *
     *  @return <code> true </code> if a todo producer checklist lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerChecklist() {
        return (false);
    }


    /**
     *  Tests if a todo producer checklist service is supported. 
     *
     *  @return <code> true </code> if todo producer checklist assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerChecklistAssignment() {
        return (false);
    }


    /**
     *  Tests if a todo producer checklist lookup service is supported. 
     *
     *  @return <code> true </code> if a todo producer checklist service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerSmartChecklist() {
        return (false);
    }


    /**
     *  Gets the supported <code> TodoProducer </code> record types. 
     *
     *  @return a list containing the supported <code> TodoProducer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTodoProducerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.todoProducerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> TodoProducer </code> record type is 
     *  supported. 
     *
     *  @param  todoProducerRecordType a <code> Type </code> indicating a 
     *          <code> TodoProducer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> todoProducerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTodoProducerRecordType(org.osid.type.Type todoProducerRecordType) {
        return (this.todoProducerRecordTypes.contains(todoProducerRecordType));
    }


    /**
     *  Adds support for a todo producer record type.
     *
     *  @param todoProducerRecordType a todo producer record type
     *  @throws org.osid.NullArgumentException
     *  <code>todoProducerRecordType</code> is <code>null</code>
     */

    protected void addTodoProducerRecordType(org.osid.type.Type todoProducerRecordType) {
        this.todoProducerRecordTypes.add(todoProducerRecordType);
        return;
    }


    /**
     *  Removes support for a todo producer record type.
     *
     *  @param todoProducerRecordType a todo producer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>todoProducerRecordType</code> is <code>null</code>
     */

    protected void removeTodoProducerRecordType(org.osid.type.Type todoProducerRecordType) {
        this.todoProducerRecordTypes.remove(todoProducerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> TodoProducer </code> search record types. 
     *
     *  @return a list containing the supported <code> TodoProducer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTodoProducerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.todoProducerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> TodoProducer </code> search record type is 
     *  supported. 
     *
     *  @param  todoProducerSearchRecordType a <code> Type </code> indicating 
     *          a <code> TodoProducer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          todoProducerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTodoProducerSearchRecordType(org.osid.type.Type todoProducerSearchRecordType) {
        return (this.todoProducerSearchRecordTypes.contains(todoProducerSearchRecordType));
    }


    /**
     *  Adds support for a todo producer search record type.
     *
     *  @param todoProducerSearchRecordType a todo producer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>todoProducerSearchRecordType</code> is <code>null</code>
     */

    protected void addTodoProducerSearchRecordType(org.osid.type.Type todoProducerSearchRecordType) {
        this.todoProducerSearchRecordTypes.add(todoProducerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a todo producer search record type.
     *
     *  @param todoProducerSearchRecordType a todo producer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>todoProducerSearchRecordType</code> is <code>null</code>
     */

    protected void removeTodoProducerSearchRecordType(org.osid.type.Type todoProducerSearchRecordType) {
        this.todoProducerSearchRecordTypes.remove(todoProducerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  lookup service. 
     *
     *  @return a <code> TodoProducerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerLookupSession getTodoProducerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerLookupSession getTodoProducerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  lookup service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerLookupSession getTodoProducerLookupSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerLookupSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  lookup service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerLookupSession getTodoProducerLookupSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerLookupSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  query service. 
     *
     *  @return a <code> TodoProducerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerQuerySession getTodoProducerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerQuerySession getTodoProducerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  query service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerQuerySession getTodoProducerQuerySessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerQuerySessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  query service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerQuerySession getTodoProducerQuerySessionForChecklist(org.osid.id.Id checklistId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerQuerySessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  search service. 
     *
     *  @return a <code> TodoProducerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSearchSession getTodoProducerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSearchSession getTodoProducerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producers 
     *  earch service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSearchSession getTodoProducerSearchSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerSearchSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producers 
     *  earch service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSearchSession getTodoProducerSearchSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerSearchSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  administration service. 
     *
     *  @return a <code> TodoProducerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerAdminSession getTodoProducerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerAdminSession getTodoProducerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  administration service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerAdminSession getTodoProducerAdminSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerAdminSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  administration service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerAdminSession getTodoProducerAdminSessionForChecklist(org.osid.id.Id checklistId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerAdminSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  notification service. 
     *
     *  @param  todoProducerReceiver the notification callback 
     *  @return a <code> TodoProducerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> todoProducerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerNotificationSession getTodoProducerNotificationSession(org.osid.checklist.mason.TodoProducerReceiver todoProducerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  notification service. 
     *
     *  @param  todoProducerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> todoProducerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerNotificationSession getTodoProducerNotificationSession(org.osid.checklist.mason.TodoProducerReceiver todoProducerReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  notification service for the given checklist. 
     *
     *  @param  todoProducerReceiver the notification callback 
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> todoProducerReceiver 
     *          </code> or <code> checklistId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerNotificationSession getTodoProducerNotificationSessionForChecklist(org.osid.checklist.mason.TodoProducerReceiver todoProducerReceiver, 
                                                                                                                   org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerNotificationSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  notification service for the given checklist. 
     *
     *  @param  todoProducerReceiver the notification callback 
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> todoProducerReceiver, 
     *          checklistId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerNotificationSession getTodoProducerNotificationSessionForChecklist(org.osid.checklist.mason.TodoProducerReceiver todoProducerReceiver, 
                                                                                                                   org.osid.id.Id checklistId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerNotificationSessionForChecklist not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup todo producer/checklist 
     *  mappings for todo producers. 
     *
     *  @return a <code> TodoProducerChecklistSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerChecklist() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerChecklistSession getTodoProducerChecklistSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerChecklistSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup todo producer/checklist 
     *  mappings for todo producers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerChecklistSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerChecklist() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerChecklistSession getTodoProducerChecklistSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerChecklistSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning todo 
     *  producers to checklists for todo. 
     *
     *  @return a <code> TodoProducerChecklistAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerChecklistAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerChecklistAssignmentSession getTodoProducerChecklistAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerChecklistAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning todo 
     *  producers to checklists for todo. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerChecklistAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerChecklistAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerChecklistAssignmentSession getTodoProducerChecklistAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerChecklistAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage todo producer smart 
     *  checklists. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerSmartChecklistSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSmartChecklist() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSmartChecklistSession getTodoProducerSmartChecklistSession(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonManager.getTodoProducerSmartChecklistSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage todo producer smart 
     *  checklists. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TodoProducerSmartChecklistSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSmartChecklist() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSmartChecklistSession getTodoProducerSmartChecklistSession(org.osid.id.Id checklistId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.checklist.mason.ChecklistMasonProxyManager.getTodoProducerSmartChecklistSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.todoProducerRecordTypes.clear();
        this.todoProducerRecordTypes.clear();

        this.todoProducerSearchRecordTypes.clear();
        this.todoProducerSearchRecordTypes.clear();

        return;
    }
}
