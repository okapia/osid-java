//
// AbstractPayer.java
//
//     Defines a Payer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.payment.payer.spi;


/**
 *  Defines a <code>Payer</code> builder.
 */

public abstract class AbstractPayerBuilder<T extends AbstractPayerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.billing.payment.payer.PayerMiter payer;


    /**
     *  Constructs a new <code>AbstractPayerBuilder</code>.
     *
     *  @param payer the payer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractPayerBuilder(net.okapia.osid.jamocha.builder.billing.payment.payer.PayerMiter payer) {
        super(payer);
        this.payer = payer;
        return;
    }


    /**
     *  Builds the payer.
     *
     *  @return the new payer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.billing.payment.Payer build() {
        (new net.okapia.osid.jamocha.builder.validator.billing.payment.payer.PayerValidator(getValidations())).validate(this.payer);
        return (new net.okapia.osid.jamocha.builder.billing.payment.payer.ImmutablePayer(this.payer));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the payer miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.billing.payment.payer.PayerMiter getMiter() {
        return (this.payer);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public T resource(org.osid.resource.Resource resource) {
        getMiter().setResource(resource);
        return (self());
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>customer</code> is <code>null</code>
     */

    public T customer(org.osid.billing.Customer customer) {
        getMiter().setCustomer(customer);
        return (self());
    }


    /**
     *  Sets the credit card number.
     *
     *  @param number a credit card number
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public T creditCardNumber(String number) {
        getMiter().setCreditCardNumber(number);
        return (self());
    }


    /**
     *  Sets the credit card expiration.
     *
     *  @param expiration a credit card expiration
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>expiration</code>
     *          is <code>null</code>
     */

    public T creditCardExpiration(org.osid.calendaring.DateTime expiration) {
        getMiter().setCreditCardExpiration(expiration);
        return (self());
    }


    /**
     *  Sets the credit card code.
     *
     *  @param code a credit card code
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     */

    public T creditCardCode(String code) {
        getMiter().setCreditCardCode(code);
        return (self());
    }


    /**
     *  Sets the bank routing number.
     *
     *  @param number a bank routing number
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    public T bankRoutingNumber(String number) {
        getMiter().setBankRoutingNumber(number);
        return (self());
    }


    /**
     *  Sets the bank account number.
     *
     *  @param number a bank account number
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    public T bankAccountNumber(String number) {
        getMiter().setBankAccountNumber(number);
        return (self());
    }


    /**
     *  Adds a Payer record.
     *
     *  @param record a payer record
     *  @param recordType the type of payer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.billing.payment.records.PayerRecord record, org.osid.type.Type recordType) {
        getMiter().addPayerRecord(record, recordType);
        return (self());
    }
}       


