//
// AbstractProgramOfferingNotificationSession.java
//
//     A template for making ProgramOfferingNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code ProgramOffering} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code ProgramOffering} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for program offering entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractProgramOfferingNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.program.ProgramOfferingNotificationSession {

    private boolean federated = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }

    
    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the {@code CourseCatalog}.
     *
     *  @param courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException {@code courseCatalog}
     *          is {@code null}
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can register for {@code ProgramOffering}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForProgramOfferingNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeProgramOfferingNotification() </code>.
     */

    @OSID @Override
    public void reliableProgramOfferingNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableProgramOfferingNotifications() {
        return;
    }


    /**
     *  Acknowledge a program offering notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeProgramOfferingNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for program offerings in
     *  course catalogs which are children of this course catalog in
     *  the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new program offerings. {@code
     *  ProgramOfferingReceiver.newProgramOffering()} is invoked when
     *  a new {@code ProgramOffering} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewProgramOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new programOfferings for the
     *  given program {@code Id}. {@code
     *  ProgramOfferingReceiver.newProgramOffering()} is invoked when
     *  a new {@code ProgramOffering} is created.
     *
     *  @param  programId the {@code Id} of the program to monitor
     *  @throws org.osid.NullArgumentException {@code programId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewProgramOfferingsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new programOfferings for the
     *  given term {@code Id}. {@code
     *  ProgramOfferingReceiver.newProgramOffering()} is invoked when
     *  a new {@code ProgramOffering} is created.
     *
     *  @param  termId the {@code Id} of the term to monitor
     *  @throws org.osid.NullArgumentException {@code termId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewProgramOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated program
     *  offerings. {@code
     *  ProgramOfferingReceiver.changedProgramOffering()} is invoked
     *  when a program offering is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProgramOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated programOfferings for the
     *  given program {@code Id}. {@code
     *  ProgramOfferingReceiver.changedProgramOffering()} is invoked
     *  when a {@code ProgramOffering} in this courseCatalog is
     *  changed.
     *
     *  @param  programId the {@code Id} of the program to monitor
     *  @throws org.osid.NullArgumentException {@code programId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedProgramOfferingsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated programOfferings for the
     *  given term {@code Id}. {@code
     *  ProgramOfferingReceiver.changedProgramOffering()} is invoked
     *  when a {@code ProgramOffering} in this courseCatalog is
     *  changed.
     *
     *  @param  termId the {@code Id} of the term to monitor
     *  @throws org.osid.NullArgumentException {@code termId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedProgramOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated program
     *  offering. {@code
     *  ProgramOfferingReceiver.changedProgramOffering()} is invoked
     *  when the specified program offering is changed.
     *
     *  @param programOfferingId the {@code Id} of the {@code ProgramOffering} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code programOfferingId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedProgramOffering(org.osid.id.Id programOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted program
     *  offerings. {@code
     *  ProgramOfferingReceiver.deletedProgramOffering()} is invoked
     *  when a program offering is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProgramOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted programOfferings for the
     *  given program {@code Id}. {@code
     *  ProgramOfferingReceiver.deletedProgramOffering()} is invoked
     *  when a {@code ProgramOffering} is deleted or removed from this
     *  courseCatalog.
     *
     *  @param  programId the {@code Id} of the program to monitor
     *  @throws org.osid.NullArgumentException {@code programId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedProgramOfferingsForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted programOfferings for the
     *  given term {@code Id}. {@code
     *  ProgramOfferingReceiver.deletedProgramOffering()} is invoked
     *  when a {@code ProgramOffering} is deleted or removed from this
     *  courseCatalog.
     *
     *  @param  termId the {@code Id} of the term to monitor
     *  @throws org.osid.NullArgumentException {@code termId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedProgramOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted program
     *  offering. {@code
     *  ProgramOfferingReceiver.deletedProgramOffering()} is invoked
     *  when the specified program offering is deleted.
     *
     *  @param programOfferingId the {@code Id} of the
     *          {@code ProgramOffering} to monitor
     *  @throws org.osid.NullArgumentException {@code programOfferingId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedProgramOffering(org.osid.id.Id programOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
