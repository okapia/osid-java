//
// InvariantMapProxyPositionLookupSession
//
//    Implements a Position lookup service backed by a fixed
//    collection of positions. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements a Position lookup service backed by a fixed
 *  collection of positions. The positions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyPositionLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractMapPositionLookupSession
    implements org.osid.personnel.PositionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPositionLookupSession} with no
     *  positions.
     *
     *  @param realm the realm
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyPositionLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.proxy.Proxy proxy) {
        setRealm(realm);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyPositionLookupSession} with a single
     *  position.
     *
     *  @param realm the realm
     *  @param position a single position
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code position} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPositionLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.personnel.Position position, org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putPosition(position);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyPositionLookupSession} using
     *  an array of positions.
     *
     *  @param realm the realm
     *  @param positions an array of positions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code positions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPositionLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.personnel.Position[] positions, org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putPositions(positions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyPositionLookupSession} using a
     *  collection of positions.
     *
     *  @param realm the realm
     *  @param positions a collection of positions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code realm},
     *          {@code positions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyPositionLookupSession(org.osid.personnel.Realm realm,
                                                  java.util.Collection<? extends org.osid.personnel.Position> positions,
                                                  org.osid.proxy.Proxy proxy) {

        this(realm, proxy);
        putPositions(positions);
        return;
    }
}
