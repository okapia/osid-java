//
// MutableMapProxyProfileLookupSession
//
//    Implements a Profile lookup service backed by a collection of
//    profiles that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile;


/**
 *  Implements a Profile lookup service backed by a collection of
 *  profiles. The profiles are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of profiles can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyProfileLookupSession
    extends net.okapia.osid.jamocha.core.profile.spi.AbstractMapProfileLookupSession
    implements org.osid.profile.ProfileLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyProfileLookupSession} with no
     *  profiles.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyProfileLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyProfileLookupSession} with a
     *  single profile.
     *
     *  @param profile a profile
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profile} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyProfileLookupSession(org.osid.profile.Profile profile, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putProfile(profile);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyProfileLookupSession} using an
     *  array of profiles.
     *
     *  @param profiles an array of profiles
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profiles} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyProfileLookupSession(org.osid.profile.Profile[] profiles, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putProfiles(profiles);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyProfileLookupSession} using
     *  a collection of profiles.
     *
     *  @param profiles a collection of profiles
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code profiles} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyProfileLookupSession(java.util.Collection<? extends org.osid.profile.Profile> profiles,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putProfiles(profiles);
        return;
    }

    
    /**
     *  Makes a {@code Profile} available in this session.
     *
     *  @param profile an profile
     *  @throws org.osid.NullArgumentException {@code profile{@code 
     *          is {@code null}
     */

    @Override
    public void putProfile(org.osid.profile.Profile profile) {
        super.putProfile(profile);
        return;
    }


    /**
     *  Makes an array of profiles available in this session.
     *
     *  @param profiles an array of profiles
     *  @throws org.osid.NullArgumentException {@code profiles{@code 
     *          is {@code null}
     */

    @Override
    public void putProfiles(org.osid.profile.Profile[] profiles) {
        super.putProfiles(profiles);
        return;
    }


    /**
     *  Makes collection of profiles available in this session.
     *
     *  @param profiles
     *  @throws org.osid.NullArgumentException {@code profile{@code 
     *          is {@code null}
     */

    @Override
    public void putProfiles(java.util.Collection<? extends org.osid.profile.Profile> profiles) {
        super.putProfiles(profiles);
        return;
    }


    /**
     *  Removes a Profile from this session.
     *
     *  @param profileId the {@code Id} of the profile
     *  @throws org.osid.NullArgumentException {@code profileId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProfile(org.osid.id.Id profileId) {
        super.removeProfile(profileId);
        return;
    }    
}
