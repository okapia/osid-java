//
// AbstractModuleSearchOdrer.java
//
//     Defines a ModuleSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.module.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ModuleSearchOrder}.
 */

public abstract class AbstractModuleSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorSearchOrder
    implements org.osid.course.syllabus.ModuleSearchOrder {

    private final java.util.Collection<org.osid.course.syllabus.records.ModuleSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the syllabus. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySyllabus(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a syllabus order is available. 
     *
     *  @return <code> true </code> if a syllabus order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusSearchOrder() {
        return (false);
    }


    /**
     *  Gets the syllabus order. 
     *
     *  @return the syllabus search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSyllabusSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusSearchOrder getSyllabusSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSyllabusSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  moduleRecordType a module record type 
     *  @return {@code true} if the moduleRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code moduleRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type moduleRecordType) {
        for (org.osid.course.syllabus.records.ModuleSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(moduleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  moduleRecordType the module record type 
     *  @return the module search order record
     *  @throws org.osid.NullArgumentException
     *          {@code moduleRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(moduleRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleSearchOrderRecord getModuleSearchOrderRecord(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.ModuleSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(moduleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(moduleRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this module. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param moduleRecord the module search odrer record
     *  @param moduleRecordType module record type
     *  @throws org.osid.NullArgumentException
     *          {@code moduleRecord} or
     *          {@code moduleRecordTypemodule} is
     *          {@code null}
     */
            
    protected void addModuleRecord(org.osid.course.syllabus.records.ModuleSearchOrderRecord moduleSearchOrderRecord, 
                                     org.osid.type.Type moduleRecordType) {

        addRecordType(moduleRecordType);
        this.records.add(moduleSearchOrderRecord);
        
        return;
    }
}
