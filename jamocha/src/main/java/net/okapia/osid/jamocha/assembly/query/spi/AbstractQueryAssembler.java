//
// AbstractQueryAssembler.java
//
//     An interface for assembling a search query.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract class for the QueryAssembler.
 */

public abstract class AbstractQueryAssembler
    implements net.okapia.osid.jamocha.assembly.query.QueryAssembler {

    private final java.util.Collection<net.okapia.osid.jamocha.assembly.query.QueryTerm> terms = new java.util.ArrayList<>();
    private final java.util.Collection<net.okapia.osid.jamocha.assembly.query.SearchOrderTerm> orderings = new java.util.ArrayList<>();
    private final java.util.Collection<String> columns = new java.util.HashSet<>();
    
    private final net.okapia.osid.torrefacto.collect.Types stringMatchTypes = new net.okapia.osid.torrefacto.collect.TypeSet();

    private long start = 0;
    private long end   = 0;


    /**
     *  Adds a supported string match type.
     *  
     *  @param stringMatchType
     *  @throws org.osid.NullArgumentException <code>stringMatchType</code> 
     *          is <code>null</code>
     */

    protected void addStringMatchType(org.osid.type.Type stringMatchType) {
        nullarg(stringMatchType, "string match Type");
        this.stringMatchTypes.add(stringMatchType);
        return;
    }


    /**
     *  Adds supported string match types.
     *  
     *  @param stringMatchTypes
     *  @throws org.osid.NullArgumentException
     *          <code>stringMatchTypes</code> is <code>null</code>
     */

    protected void addStringMatchTypes(java.util.Collection<org.osid.type.Type> stringMatchTypes) {
        nullarg(stringMatchTypes, "string match Types");

        for (org.osid.type.Type type : stringMatchTypes) {
            addStringMatchType(type);
        }

        return;
    }


    /**
     *  Gets the string matching types supported. A string match type
     *  specifies the syntax of the string query, such as matching a
     *  word or including a wildcard or regular expression.
     *
     *  @return a list containing the supported string match types
     */

    @Override
    public org.osid.type.TypeList getStringMatchTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stringMatchTypes.toCollection()));
    }


    /**
     *  Tests if the given string matching stringMatchType is
     *  supported.
     *
     *  @param  stringMatchType a <code> StringMatchType </code>
     *          indicating a string match type
     *  @return <code> true </code> if the given stringMatchType is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>stringMatchType</code> is <code>null</code>
     */

    @Override
    public boolean supportsStringMatchType(org.osid.type.Type stringMatchType) {
        return (this.stringMatchTypes.contains(stringMatchType));
    }


    /**
     *  Adds a query term to this search. 
     *
     *  @param term the query term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    @Override
    public void addTerm(net.okapia.osid.jamocha.assembly.query.QueryTerm term) {
        if (hasColumn(term.getColumn())) {
            this.terms.add(term);
        }

        return;
    }


    /**
     *  Gets the columns of the search queries.
     *
     *  @return an iteratable collection of columns
     */

    @Override
    public Iterable<String> getColumns() {
        return (java.util.Collections.unmodifiableCollection(this.columns));
    }


    /**
     *  Adds a column supported by the query.
     *
     *  @param column a column name
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    public void addColumn(String column) {
        nullarg(column, "column");
        this.columns.add(column);
        return;
    }


    /**
     *  Adds a collection of columns supported by the query.
     *
     *  @param columns a collection of column names
     *  @throws org.osid.NullArgumentException <code>columns</code> is
     *          <code>null</code>
     */

    public void addColumns(java.util.Collection<String> columns) {
        nullarg(columns, "columns");
        this.columns.addAll(columns);
        return;
    }


    /**
     *  Tests if a column is supported.
     *
     *  @param column a column name
     *  @return <code>true</code> if column name is supported,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public boolean hasColumn(String column) {
        nullarg(column, "column");
        return (this.columns.contains(column));
    }


    /**
     *  Gets the query terms for a given column.
     *
     *  @param column a query column
     *  @return an iteratable collection of terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public Iterable<net.okapia.osid.jamocha.assembly.query.QueryTerm> getTerms(String column) {
        nullarg(column, "column");
        java.util.Collection<net.okapia.osid.jamocha.assembly.query.QueryTerm> ret = new java.util.ArrayList<>();

        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : this.terms) {
            if (column.equals(term.getColumn())) {
                ret.add(term);
            }
        }

        return (ret);
    }


    /**
     *  Clears the query terms for a given column.
     *
     *  @param column a query column
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public void clearTerms(String column) {
        nullarg(column, "column");

        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : this.terms) {
            if (column.equals(term.getColumn())) {
                this.terms.remove(term);
            }
        }

        return;
    }


    /**
     *  Gets the boolean query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of boolean terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.BooleanTerm[] getBooleanTerms(String column) {
        java.util.Collection<org.osid.search.terms.BooleanTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.BooleanTerm) {
                ret.add((org.osid.search.terms.BooleanTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.BooleanTerm[ret.size()]));
    }


    /**
     *  Gets the bytes query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of bytes terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.BytesTerm[] getBytesTerms(String column) {
        java.util.Collection<org.osid.search.terms.BytesTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.BytesTerm) {
                ret.add((org.osid.search.terms.BytesTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.BytesTerm[ret.size()]));
    }


    /**
     *  Gets the cardinal query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of cardinal terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.CardinalTerm[] getCardinalTerms(String column) {
        java.util.Collection<org.osid.search.terms.CardinalTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.CardinalTerm) {
                ret.add((org.osid.search.terms.CardinalTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.CardinalTerm[ret.size()]));
    }


    /**
     *  Gets the cardinal range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of cardinal range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.CardinalRangeTerm[] getCardinalRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.CardinalRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.CardinalRangeTerm) {
                ret.add((org.osid.search.terms.CardinalRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.CardinalRangeTerm[ret.size()]));
    }


    /**
     *  Gets the coordinate query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of coordinate terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms(String column) {
        java.util.Collection<org.osid.search.terms.CoordinateTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.CoordinateTerm) {
                ret.add((org.osid.search.terms.CoordinateTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.CoordinateTerm[ret.size()]));
    }


    /**
     *  Gets the coordinate range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of coordinate range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.CoordinateRangeTerm[] getCoordinateRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.CoordinateRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.CoordinateRangeTerm) {
                ret.add((org.osid.search.terms.CoordinateRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.CoordinateRangeTerm[ret.size()]));
    }


    /**
     *  Gets the currency query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of currency terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.CurrencyTerm[] getCurrencyTerms(String column) {
        java.util.Collection<org.osid.search.terms.CurrencyTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.CurrencyTerm) {
                ret.add((org.osid.search.terms.CurrencyTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.CurrencyTerm[ret.size()]));
    }


    /**
     *  Gets the currency range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of currency range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCurrencyRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.CurrencyRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.CurrencyRangeTerm) {
                ret.add((org.osid.search.terms.CurrencyRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.CurrencyRangeTerm[ret.size()]));
    }


    /**
     *  Gets the datetime query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of datetime terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.DateTimeTerm[] getDateTimeTerms(String column) {
        java.util.Collection<org.osid.search.terms.DateTimeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.DateTimeTerm) {
                ret.add((org.osid.search.terms.DateTimeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.DateTimeTerm[ret.size()]));
    }


    /**
     *  Gets the datetime range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of datetime range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTimeRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.DateTimeRangeTerm) {
                ret.add((org.osid.search.terms.DateTimeRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.DateTimeRangeTerm[ret.size()]));
    }


    /**
     *  Gets the decimal query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of decimal terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.DecimalTerm[] getDecimalTerms(String column) {
        java.util.Collection<org.osid.search.terms.DecimalTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.DecimalTerm) {
                ret.add((org.osid.search.terms.DecimalTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.DecimalTerm[ret.size()]));
    }


    /**
     *  Gets the decimal range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of decimal range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.DecimalRangeTerm[] getDecimalRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.DecimalRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.DecimalRangeTerm) {
                ret.add((org.osid.search.terms.DecimalRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.DecimalRangeTerm[ret.size()]));
    }


    /**
     *  Gets the distance query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of distance terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.DistanceTerm[] getDistanceTerms(String column) {
        java.util.Collection<org.osid.search.terms.DistanceTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.DistanceTerm) {
                ret.add((org.osid.search.terms.DistanceTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.DistanceTerm[ret.size()]));
    }


    /**
     *  Gets the distance range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of distance range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.DistanceRangeTerm[] getDistanceRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.DistanceRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.DistanceRangeTerm) {
                ret.add((org.osid.search.terms.DistanceRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.DistanceRangeTerm[ret.size()]));
    }


    /**
     *  Gets the duration query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of duration terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms(String column) {
        java.util.Collection<org.osid.search.terms.DurationTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.DurationTerm) {
                ret.add((org.osid.search.terms.DurationTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.DurationTerm[ret.size()]));
    }


    /**
     *  Gets the duration range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of duration range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.DurationRangeTerm[] getDurationRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.DurationRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.DurationRangeTerm) {
                ret.add((org.osid.search.terms.DurationRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.DurationRangeTerm[ret.size()]));
    }


    /**
     *  Gets the heading query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of heading terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.HeadingTerm[] getHeadingTerms(String column) {
        java.util.Collection<org.osid.search.terms.HeadingTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.HeadingTerm) {
                ret.add((org.osid.search.terms.HeadingTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.HeadingTerm[ret.size()]));
    }


    /**
     *  Gets the heading range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of heading range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.HeadingRangeTerm[] getHeadingRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.HeadingRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.HeadingRangeTerm) {
                ret.add((org.osid.search.terms.HeadingRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.HeadingRangeTerm[ret.size()]));
    }


    /**
     *  Gets the Id query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of Id terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.IdTerm[] getIdTerms(String column) {
        java.util.Collection<org.osid.search.terms.IdTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.IdTerm) {
                ret.add((org.osid.search.terms.IdTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.IdTerm[ret.size()]));
    }


    /**
     *  Gets the Id set query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of Id set terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.IdSetTerm[] getIdSetTerms(String column) {
        java.util.Collection<org.osid.search.terms.IdSetTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.IdSetTerm) {
                ret.add((org.osid.search.terms.IdSetTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.IdSetTerm[ret.size()]));
    }


    /**
     *  Gets the integer query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of integer terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.IntegerTerm[] getIntegerTerms(String column) {
        java.util.Collection<org.osid.search.terms.IntegerTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.IntegerTerm) {
                ret.add((org.osid.search.terms.IntegerTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.IntegerTerm[ret.size()]));
    }


    /**
     *  Gets the integer range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of integer range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.IntegerRangeTerm[] getIntegerRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.IntegerRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.IntegerRangeTerm) {
                ret.add((org.osid.search.terms.IntegerRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.IntegerRangeTerm[ret.size()]));
    }


    /**
     *  Gets the object query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of object terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.ObjectTerm[] getObjectTerms(String column) {
        java.util.Collection<org.osid.search.terms.ObjectTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.ObjectTerm) {
                ret.add((org.osid.search.terms.ObjectTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.ObjectTerm[ret.size()]));
    }


    /**
     *  Gets the spatial unit query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of spatial unit terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.SpatialUnitTerm[] getSpatialUnitTerms(String column) {
        java.util.Collection<org.osid.search.terms.SpatialUnitTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.SpatialUnitTerm) {
                ret.add((org.osid.search.terms.SpatialUnitTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.SpatialUnitTerm[ret.size()]));
    }


    /**
     *  Gets the speed query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of speed terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.SpeedTerm[] getSpeedTerms(String column) {
        java.util.Collection<org.osid.search.terms.SpeedTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.SpeedTerm) {
                ret.add((org.osid.search.terms.SpeedTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.SpeedTerm[ret.size()]));
    }


    /**
     *  Gets the speed range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of speed range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.SpeedRangeTerm[] getSpeedRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.SpeedRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.SpeedRangeTerm) {
                ret.add((org.osid.search.terms.SpeedRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.SpeedRangeTerm[ret.size()]));
    }


    /**
     *  Gets the string query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of string terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.StringTerm[] getStringTerms(String column) {
        java.util.Collection<org.osid.search.terms.StringTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.StringTerm) {
                ret.add((org.osid.search.terms.StringTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.StringTerm[ret.size()]));
    }


    /**
     *  Gets the time query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of time terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.TimeTerm[] getTimeTerms(String column) {
        java.util.Collection<org.osid.search.terms.TimeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.TimeTerm) {
                ret.add((org.osid.search.terms.TimeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.TimeTerm[ret.size()]));
    }


    /**
     *  Gets the syntax query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of syntax terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.SyntaxTerm[] getSyntaxTerms(String column) {
        java.util.Collection<org.osid.search.terms.SyntaxTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.SyntaxTerm) {
                ret.add((org.osid.search.terms.SyntaxTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.SyntaxTerm[ret.size()]));
    }


    /**
     *  Gets the time range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of time range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.TimeRangeTerm[] getTimeRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.TimeRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.TimeRangeTerm) {
                ret.add((org.osid.search.terms.TimeRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.TimeRangeTerm[ret.size()]));
    }


    /**
     *  Gets the type query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of type terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.TypeTerm[] getTypeTerms(String column) {
        java.util.Collection<org.osid.search.terms.TypeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.TypeTerm) {
                ret.add((org.osid.search.terms.TypeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.TypeTerm[ret.size()]));
    }


    /**
     *  Gets the version query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of version terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.VersionTerm[] getVersionTerms(String column) {
        java.util.Collection<org.osid.search.terms.VersionTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.VersionTerm) {
                ret.add((org.osid.search.terms.VersionTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.VersionTerm[ret.size()]));
    }


    /**
     *  Gets the version range query terms for a given column.
     *
     *  @param column a query column
     *  @return an array of version range terms
     *  @throws org.osid.NullArgumentException <code>column</code> is
     *          <code>null</code>
     */

    @Override
    public org.osid.search.terms.VersionRangeTerm[] getVersionRangeTerms(String column) {
        java.util.Collection<org.osid.search.terms.VersionRangeTerm> ret = new java.util.ArrayList<>();
        for (net.okapia.osid.jamocha.assembly.query.QueryTerm term : getTerms(column)) {
            if (term instanceof org.osid.search.terms.VersionRangeTerm) {
                ret.add((org.osid.search.terms.VersionRangeTerm) term);
            }
        }

        return (ret.toArray(new org.osid.search.terms.VersionRangeTerm[ret.size()]));
    }


    /**
     *  Adds an order term.
     *
     *  @param column a column
     *  @param style search order style
     *  @throws org.osid.NullArgumentException <code>column</code> or
     *          <code>style</code> is <code>null</code>
     */

    @Override
    public void addOrder(String column, org.osid.SearchOrderStyle style) {
        this.orderings.add(new net.okapia.osid.jamocha.assembly.query.SearchOrderTerm(column, style));
        return;
    }


    /**
     *  Adds an order term with a qualifying Id.
     *
     *  @param column a column
     *  @param qualifierId a qualifiying Id
     *  @param style search order style
     *  @throws org.osid.NullArgumentException <code>column</code>,
     *          <code>qualifierId</code> or <code>style</code> is
     *          <code>null</code>
     */

    @Override
    public void addOrder(String column, org.osid.id.Id qualifierId, 
                         org.osid.SearchOrderStyle style) {

        this.orderings.add(new net.okapia.osid.jamocha.assembly.query.SearchOrderTerm(column, qualifierId, style));
        return;
    }


    /**
     *  Gets the ordering terms.
     */

    public java.util.Collection<net.okapia.osid.jamocha.assembly.query.SearchOrderTerm> getOrderings() {        
        return (java.util.Collections.unmodifiableCollection(this.orderings));
    }
}

