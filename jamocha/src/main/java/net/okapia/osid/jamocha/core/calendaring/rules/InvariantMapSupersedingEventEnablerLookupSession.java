//
// InvariantMapSupersedingEventEnablerLookupSession
//
//    Implements a SupersedingEventEnabler lookup service backed by a fixed collection of
//    supersedingEventEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules;


/**
 *  Implements a SupersedingEventEnabler lookup service backed by a fixed
 *  collection of superseding event enablers. The superseding event enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapSupersedingEventEnablerLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.rules.spi.AbstractMapSupersedingEventEnablerLookupSession
    implements org.osid.calendaring.rules.SupersedingEventEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapSupersedingEventEnablerLookupSession</code> with no
     *  superseding event enablers.
     *  
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumnetException {@code calendar} is
     *          {@code null}
     */

    public InvariantMapSupersedingEventEnablerLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSupersedingEventEnablerLookupSession</code> with a single
     *  superseding event enabler.
     *  
     *  @param calendar the calendar
     *  @param supersedingEventEnabler a single superseding event enabler
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code supersedingEventEnabler} is <code>null</code>
     */

      public InvariantMapSupersedingEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.rules.SupersedingEventEnabler supersedingEventEnabler) {
        this(calendar);
        putSupersedingEventEnabler(supersedingEventEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSupersedingEventEnablerLookupSession</code> using an array
     *  of superseding event enablers.
     *  
     *  @param calendar the calendar
     *  @param supersedingEventEnablers an array of superseding event enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code supersedingEventEnablers} is <code>null</code>
     */

      public InvariantMapSupersedingEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.rules.SupersedingEventEnabler[] supersedingEventEnablers) {
        this(calendar);
        putSupersedingEventEnablers(supersedingEventEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapSupersedingEventEnablerLookupSession</code> using a
     *  collection of superseding event enablers.
     *
     *  @param calendar the calendar
     *  @param supersedingEventEnablers a collection of superseding event enablers
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code supersedingEventEnablers} is <code>null</code>
     */

      public InvariantMapSupersedingEventEnablerLookupSession(org.osid.calendaring.Calendar calendar,
                                               java.util.Collection<? extends org.osid.calendaring.rules.SupersedingEventEnabler> supersedingEventEnablers) {
        this(calendar);
        putSupersedingEventEnablers(supersedingEventEnablers);
        return;
    }
}
