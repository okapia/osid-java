//
// AbstractRealmSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.realm.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRealmSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.personnel.RealmSearchResults {

    private org.osid.personnel.RealmList realms;
    private final org.osid.personnel.RealmQueryInspector inspector;
    private final java.util.Collection<org.osid.personnel.records.RealmSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRealmSearchResults.
     *
     *  @param realms the result set
     *  @param realmQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>realms</code>
     *          or <code>realmQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRealmSearchResults(org.osid.personnel.RealmList realms,
                                            org.osid.personnel.RealmQueryInspector realmQueryInspector) {
        nullarg(realms, "realms");
        nullarg(realmQueryInspector, "realm query inspectpr");

        this.realms = realms;
        this.inspector = realmQueryInspector;

        return;
    }


    /**
     *  Gets the realm list resulting from a search.
     *
     *  @return a realm list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealms() {
        if (this.realms == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.personnel.RealmList realms = this.realms;
        this.realms = null;
	return (realms);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.personnel.RealmQueryInspector getRealmQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  realm search record <code> Type. </code> This method must
     *  be used to retrieve a realm implementing the requested
     *  record.
     *
     *  @param realmSearchRecordType a realm search 
     *         record type 
     *  @return the realm search
     *  @throws org.osid.NullArgumentException
     *          <code>realmSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(realmSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.RealmSearchResultsRecord getRealmSearchResultsRecord(org.osid.type.Type realmSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.personnel.records.RealmSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(realmSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(realmSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record realm search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRealmRecord(org.osid.personnel.records.RealmSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "realm record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
