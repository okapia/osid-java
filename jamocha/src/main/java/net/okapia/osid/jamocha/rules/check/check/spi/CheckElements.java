//
// CheckElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.check.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CheckElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the CheckElement Id.
     *
     *  @return the check element Id
     */

    public static org.osid.id.Id getCheckEntityId() {
        return (makeEntityId("osid.rules.check.Check"));
    }


    /**
     *  Gets the TimeCheckStartDate element Id.
     *
     *  @return the TimeCheckStartDate element Id
     */

    public static org.osid.id.Id getTimeCheckStartDate() {
        return (makeElementId("osid.rules.check.check.TimeCheckStartDate"));
    }


    /**
     *  Gets the TimeCheckEndDate element Id.
     *
     *  @return the TimeCheckEndDate element Id
     */

    public static org.osid.id.Id getTimeCheckEndDate() {
        return (makeElementId("osid.rules.check.check.TimeCheckEndDate"));
    }


    /**
     *  Gets the TimeCheckDate element Id.
     *
     *  @return the TimeCheckDate element Id
     */

    public static org.osid.id.Id getTimeCheckDate() {
        return (makeQueryElementId("osid.rules.check.check.TimeCheckDate"));
    }


    /**
     *  Gets the TimeCheckEventId element Id.
     *
     *  @return the TimeCheckEventId element Id
     */

    public static org.osid.id.Id getTimeCheckEventId() {
        return (makeElementId("osid.rules.check.check.TimeCheckEventId"));
    }


    /**
     *  Gets the TimeCheckEvent element Id.
     *
     *  @return the TimeCheckEvent element Id
     */

    public static org.osid.id.Id getTimeCheckEvent() {
        return (makeElementId("osid.rules.check.check.TimeCheckEvent"));
    }


    /**
     *  Gets the TimeCheckCyclicEventId element Id.
     *
     *  @return the TimeCheckCyclicEventId element Id
     */

    public static org.osid.id.Id getTimeCheckCyclicEventId() {
        return (makeElementId("osid.rules.check.check.TimeCheckCyclicEventId"));
    }


    /**
     *  Gets the TimeCheckCyclicEvent element Id.
     *
     *  @return the TimeCheckCyclicEvent element Id
     */

    public static org.osid.id.Id getTimeCheckCyclicEvent() {
        return (makeElementId("osid.rules.check.check.TimeCheckCyclicEvent"));
    }


    /**
     *  Gets the HoldCheckBlockId element Id.
     *
     *  @return the HoldCheckBlockId element Id
     */

    public static org.osid.id.Id getHoldCheckBlockId() {
        return (makeElementId("osid.rules.check.check.HoldCheckBlockId"));
    }


    /**
     *  Gets the HoldCheckBlock element Id.
     *
     *  @return the HoldCheckBlock element Id
     */

    public static org.osid.id.Id getHoldCheckBlock() {
        return (makeElementId("osid.rules.check.check.HoldCheckBlock"));
    }


    /**
     *  Gets the ProcessCheckAgendaId element Id.
     *
     *  @return the ProcessCheckAgendaId element Id
     */

    public static org.osid.id.Id getProcessCheckAgendaId() {
        return (makeElementId("osid.rules.check.check.ProcessCheckAgendaId"));
    }


    /**
     *  Gets the ProcessCheckAgenda element Id.
     *
     *  @return the ProcessCheckAgenda element Id
     */

    public static org.osid.id.Id getProcessCheckAgenda() {
        return (makeElementId("osid.rules.check.check.ProcessCheckAgenda"));
    }


    /**
     *  Gets the FailCheck element Id.
     *
     *  @return the FailCheck element Id
     */

    public static org.osid.id.Id getFailCheck() {
        return (makeElementId("osid.rules.check.check.FailCheck"));
    }


    /**
     *  Gets the EngineId element Id.
     *
     *  @return the EngineId element Id
     */

    public static org.osid.id.Id getEngineId() {
        return (makeQueryElementId("osid.rules.check.check.EngineId"));
    }


    /**
     *  Gets the Engine element Id.
     *
     *  @return the Engine element Id
     */

    public static org.osid.id.Id getEngine() {
        return (makeQueryElementId("osid.rules.check.check.Engine"));
    }
}
