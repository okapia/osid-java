//
// AbstractMapBrokerProcessorEnablerLookupSession
//
//    A simple framework for providing a BrokerProcessorEnabler lookup service
//    backed by a fixed collection of broker processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a BrokerProcessorEnabler lookup service backed by a
 *  fixed collection of broker processor enablers. The broker processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BrokerProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBrokerProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractBrokerProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.BrokerProcessorEnabler> brokerProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.BrokerProcessorEnabler>());


    /**
     *  Makes a <code>BrokerProcessorEnabler</code> available in this session.
     *
     *  @param  brokerProcessorEnabler a broker processor enabler
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putBrokerProcessorEnabler(org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler) {
        this.brokerProcessorEnablers.put(brokerProcessorEnabler.getId(), brokerProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of broker processor enablers available in this session.
     *
     *  @param  brokerProcessorEnablers an array of broker processor enablers
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putBrokerProcessorEnablers(org.osid.provisioning.rules.BrokerProcessorEnabler[] brokerProcessorEnablers) {
        putBrokerProcessorEnablers(java.util.Arrays.asList(brokerProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of broker processor enablers available in this session.
     *
     *  @param  brokerProcessorEnablers a collection of broker processor enablers
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putBrokerProcessorEnablers(java.util.Collection<? extends org.osid.provisioning.rules.BrokerProcessorEnabler> brokerProcessorEnablers) {
        for (org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler : brokerProcessorEnablers) {
            this.brokerProcessorEnablers.put(brokerProcessorEnabler.getId(), brokerProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a BrokerProcessorEnabler from this session.
     *
     *  @param  brokerProcessorEnablerId the <code>Id</code> of the broker processor enabler
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeBrokerProcessorEnabler(org.osid.id.Id brokerProcessorEnablerId) {
        this.brokerProcessorEnablers.remove(brokerProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>BrokerProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  brokerProcessorEnablerId <code>Id</code> of the <code>BrokerProcessorEnabler</code>
     *  @return the brokerProcessorEnabler
     *  @throws org.osid.NotFoundException <code>brokerProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnabler getBrokerProcessorEnabler(org.osid.id.Id brokerProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler = this.brokerProcessorEnablers.get(brokerProcessorEnablerId);
        if (brokerProcessorEnabler == null) {
            throw new org.osid.NotFoundException("brokerProcessorEnabler not found: " + brokerProcessorEnablerId);
        }

        return (brokerProcessorEnabler);
    }


    /**
     *  Gets all <code>BrokerProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known brokerProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  brokerProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>BrokerProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerprocessorenabler.ArrayBrokerProcessorEnablerList(this.brokerProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerProcessorEnablers.clear();
        super.close();
        return;
    }
}
