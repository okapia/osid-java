//
// AbstractMapRepositoryLookupSession
//
//    A simple framework for providing a Repository lookup service
//    backed by a fixed collection of repositories.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Repository lookup service backed by a
 *  fixed collection of repositories. The repositories are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Repositories</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRepositoryLookupSession
    extends net.okapia.osid.jamocha.repository.spi.AbstractRepositoryLookupSession
    implements org.osid.repository.RepositoryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.repository.Repository> repositories = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.repository.Repository>());


    /**
     *  Makes a <code>Repository</code> available in this session.
     *
     *  @param  repository a repository
     *  @throws org.osid.NullArgumentException <code>repository<code>
     *          is <code>null</code>
     */

    protected void putRepository(org.osid.repository.Repository repository) {
        this.repositories.put(repository.getId(), repository);
        return;
    }


    /**
     *  Makes an array of repositories available in this session.
     *
     *  @param  repositories an array of repositories
     *  @throws org.osid.NullArgumentException <code>repositories<code>
     *          is <code>null</code>
     */

    protected void putRepositories(org.osid.repository.Repository[] repositories) {
        putRepositories(java.util.Arrays.asList(repositories));
        return;
    }


    /**
     *  Makes a collection of repositories available in this session.
     *
     *  @param  repositories a collection of repositories
     *  @throws org.osid.NullArgumentException <code>repositories<code>
     *          is <code>null</code>
     */

    protected void putRepositories(java.util.Collection<? extends org.osid.repository.Repository> repositories) {
        for (org.osid.repository.Repository repository : repositories) {
            this.repositories.put(repository.getId(), repository);
        }

        return;
    }


    /**
     *  Removes a Repository from this session.
     *
     *  @param  repositoryId the <code>Id</code> of the repository
     *  @throws org.osid.NullArgumentException <code>repositoryId<code> is
     *          <code>null</code>
     */

    protected void removeRepository(org.osid.id.Id repositoryId) {
        this.repositories.remove(repositoryId);
        return;
    }


    /**
     *  Gets the <code>Repository</code> specified by its <code>Id</code>.
     *
     *  @param  repositoryId <code>Id</code> of the <code>Repository</code>
     *  @return the repository
     *  @throws org.osid.NotFoundException <code>repositoryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>repositoryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.repository.Repository repository = this.repositories.get(repositoryId);
        if (repository == null) {
            throw new org.osid.NotFoundException("repository not found: " + repositoryId);
        }

        return (repository);
    }


    /**
     *  Gets all <code>Repositories</code>. In plenary mode, the returned
     *  list contains all known repositories or an error
     *  results. Otherwise, the returned list may contain only those
     *  repositories that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Repositories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.repository.ArrayRepositoryList(this.repositories.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.repositories.clear();
        super.close();
        return;
    }
}
