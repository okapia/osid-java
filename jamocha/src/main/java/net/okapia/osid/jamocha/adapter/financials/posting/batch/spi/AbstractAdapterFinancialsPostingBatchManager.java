//
// AbstractFinancialsPostingBatchManager.java
//
//     An adapter for a FinancialsPostingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.posting.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FinancialsPostingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFinancialsPostingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.financials.posting.batch.FinancialsPostingBatchManager>
    implements org.osid.financials.posting.batch.FinancialsPostingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsPostingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFinancialsPostingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFinancialsPostingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFinancialsPostingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of posts is available. 
     *
     *  @return <code> true </code> if a post bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostBatchAdmin() {
        return (getAdapteeManager().supportsPostBatchAdmin());
    }


    /**
     *  Tests if bulk administration of post entries is available. 
     *
     *  @return <code> true </code> if a post entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryBatchAdmin() {
        return (getAdapteeManager().supportsPostEntryBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk post 
     *  administration service. 
     *
     *  @return a <code> PostBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.batch.PostBatchAdminSession getPostBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk post 
     *  administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.batch.PostBatchAdminSession getPostBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostBatchAdminSessionForBusiness(businessId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk post 
     *  entry administration service. 
     *
     *  @return a <code> PostEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.batch.PostEntryBatchAdminSession getPostEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk post 
     *  entry administration service for the given business. 
     *
     *  @param  businessId the <code> Id </code> of the <code> Business 
     *          </code> 
     *  @return a <code> PostEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Business </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.batch.PostEntryBatchAdminSession getPostEntryBatchAdminSessionForBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostEntryBatchAdminSessionForBusiness(businessId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
