//
// AbstractImmutableCourse.java
//
//     Wraps a mutable Course to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.course.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Course</code> to hide modifiers. This
 *  wrapper provides an immutized Course from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying course whose state changes are visible.
 */

public abstract class AbstractImmutableCourse
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.course.Course {

    private final org.osid.course.Course course;


    /**
     *  Constructs a new <code>AbstractImmutableCourse</code>.
     *
     *  @param course the course to immutablize
     *  @throws org.osid.NullArgumentException <code>course</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCourse(org.osid.course.Course course) {
        super(course);
        this.course = course;
        return;
    }


    /**
     *  Gets the formal title of this course. It may be the same as the 
     *  display name or it may be used to more formally label the course. A 
     *  display name might be Physics 102 where the title is Introduction to 
     *  Electromagentism. 
     *
     *  @return the course title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.course.getTitle());
    }


    /**
     *  Gets the course number which is a label generally used to index the 
     *  course in a catalog, such as T101 or 16.004. 
     *
     *  @return the course number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.course.getNumber());
    }


    /**
     *  Tests if this course has sponsors. 
     *
     *  @return <code> true </code> if this course has sponsors, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.course.hasSponsors());
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        return (this.course.getSponsorIds());
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        return (this.course.getSponsors());
    }


    /**
     *  Gets the credit amount <code>Ids</code>.
     *
     *  @return the grade <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getCreditAmountIds() {
        return (this.course.getCreditAmountIds());
    }


    /**
     *  Gets the credit amounts. 
     *
     *  @return the credit amounts
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeList getCreditAmounts()
        throws org.osid.OperationFailedException {

        return (this.course.getCreditAmounts());
    }


    /**
     *  Gets the an informational string for the course prerequisites. 
     *
     *  @return the course prerequisites 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getPrerequisitesInfo() {
        return (this.course.getPrerequisitesInfo());
    }


    /**
     *  Tests if this course has <code> Requisites </code> for the course 
     *  prerequisites. 
     *
     *  @return <code> true </code> if this course has prerequisites <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasPrerequisites() {
        return (this.course.hasPrerequisites());
    }


    /**
     *  Gets the requisite <code> Ids </code> for the course prerequisites. 
     *
     *  @return the requisite <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasPrerequisites() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPrerequisiteIds() {
        return (this.course.getPrerequisiteIds());
    }


    /**
     *  Gets the requisites for the course prerequisites. Each <code> 
     *  Requisite </code> is an <code> AND </code> term such that all <code> 
     *  Requisites </code> must be true for the prerequisites to be satisifed. 
     *
     *  @return the requisites 
     *  @throws org.osid.IllegalStateException <code> hasPrerequisites() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getPrerequisites()
        throws org.osid.OperationFailedException {

        return (this.course.getPrerequisites());
    }


    /**
     *  Gets the grade level <code> Ids </code> of this course. Multiple 
     *  levels may exist for different systems. 
     *
     *  @return the returned list of grade level <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLevelIds() {
        return (this.course.getLevelIds());
    }


    /**
     *  Gets the grade levels of this course. Multiple levels may exist for 
     *  different systems. 
     *
     *  @return the returned list of grade levels 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeList getLevels()
        throws org.osid.OperationFailedException {

        return (this.course.getLevels());
    }


    /**
     *  Tests if this course is graded. 
     *
     *  @return <code> true </code> if this course is graded, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.course.isGraded());
    }


    /**
     *  Gets the various grading option <code> Ids </code> available to 
     *  register in this course. 
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getGradingOptionIds() {
        return (this.course.getGradingOptionIds());
    }


    /**
     *  Gets the various grading options available to register in this course. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradingOptions()
        throws org.osid.OperationFailedException {

        return (this.course.getGradingOptions());
    }


    /**
     *  Tests if this course has associated learning objectives. 
     *
     *  @return <code> true </code> if this course has a learning objective, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLearningObjectives() {
        return (this.course.hasLearningObjectives());
    }


    /**
     *  Gets the overall learning objective <code> Ids </code> for this 
     *  course. 
     *
     *  @return <code> Ids </code> of the <code> l </code> earning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        return (this.course.getLearningObjectiveIds());
    }


    /**
     *  Gets the overall learning objectives for this course. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        return (this.course.getLearningObjectives());
    }


    /**
     *  Gets the course record corresponding to the given <code> Course 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> courseRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(courseRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  courseRecordType the type of course record to retrieve 
     *  @return the course record 
     *  @throws org.osid.NullArgumentException <code> courseRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(courseRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.records.CourseRecord getCourseRecord(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException {

        return (this.course.getCourseRecord(courseRecordType));
    }
}

