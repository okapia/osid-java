//
// OperableMiter.java
//
//     Miter for Operables.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Miter for Operables.
 */

public interface OperableMiter
    extends Miter,
            org.osid.Operable {


    /**
     *  Sets the enabled flag.
     *
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         if disabled
     */

    public void setEnabled(boolean enabled);


    /**
     *  Sets the disabled flag.
     *
     *  @param disabled <code>true</code> if disabled,
     *         <code>false<code> if disabled
     */

    public void setDisabled(boolean disabled);


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    public void setOperational(boolean operational);
}
