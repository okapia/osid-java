//
// AbstractMapAvailabilityEnablerLookupSession
//
//    A simple framework for providing an AvailabilityEnabler lookup service
//    backed by a fixed collection of availability enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AvailabilityEnabler lookup service backed by a
 *  fixed collection of availability enablers. The availability enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AvailabilityEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAvailabilityEnablerLookupSession
    extends net.okapia.osid.jamocha.resourcing.rules.spi.AbstractAvailabilityEnablerLookupSession
    implements org.osid.resourcing.rules.AvailabilityEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.rules.AvailabilityEnabler> availabilityEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.rules.AvailabilityEnabler>());


    /**
     *  Makes an <code>AvailabilityEnabler</code> available in this session.
     *
     *  @param  availabilityEnabler an availability enabler
     *  @throws org.osid.NullArgumentException <code>availabilityEnabler<code>
     *          is <code>null</code>
     */

    protected void putAvailabilityEnabler(org.osid.resourcing.rules.AvailabilityEnabler availabilityEnabler) {
        this.availabilityEnablers.put(availabilityEnabler.getId(), availabilityEnabler);
        return;
    }


    /**
     *  Makes an array of availability enablers available in this session.
     *
     *  @param  availabilityEnablers an array of availability enablers
     *  @throws org.osid.NullArgumentException <code>availabilityEnablers<code>
     *          is <code>null</code>
     */

    protected void putAvailabilityEnablers(org.osid.resourcing.rules.AvailabilityEnabler[] availabilityEnablers) {
        putAvailabilityEnablers(java.util.Arrays.asList(availabilityEnablers));
        return;
    }


    /**
     *  Makes a collection of availability enablers available in this session.
     *
     *  @param  availabilityEnablers a collection of availability enablers
     *  @throws org.osid.NullArgumentException <code>availabilityEnablers<code>
     *          is <code>null</code>
     */

    protected void putAvailabilityEnablers(java.util.Collection<? extends org.osid.resourcing.rules.AvailabilityEnabler> availabilityEnablers) {
        for (org.osid.resourcing.rules.AvailabilityEnabler availabilityEnabler : availabilityEnablers) {
            this.availabilityEnablers.put(availabilityEnabler.getId(), availabilityEnabler);
        }

        return;
    }


    /**
     *  Removes an AvailabilityEnabler from this session.
     *
     *  @param  availabilityEnablerId the <code>Id</code> of the availability enabler
     *  @throws org.osid.NullArgumentException <code>availabilityEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeAvailabilityEnabler(org.osid.id.Id availabilityEnablerId) {
        this.availabilityEnablers.remove(availabilityEnablerId);
        return;
    }


    /**
     *  Gets the <code>AvailabilityEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  availabilityEnablerId <code>Id</code> of the <code>AvailabilityEnabler</code>
     *  @return the availabilityEnabler
     *  @throws org.osid.NotFoundException <code>availabilityEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>availabilityEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnabler getAvailabilityEnabler(org.osid.id.Id availabilityEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.rules.AvailabilityEnabler availabilityEnabler = this.availabilityEnablers.get(availabilityEnablerId);
        if (availabilityEnabler == null) {
            throw new org.osid.NotFoundException("availabilityEnabler not found: " + availabilityEnablerId);
        }

        return (availabilityEnabler);
    }


    /**
     *  Gets all <code>AvailabilityEnablers</code>. In plenary mode, the returned
     *  list contains all known availabilityEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  availabilityEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AvailabilityEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.ArrayAvailabilityEnablerList(this.availabilityEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.availabilityEnablers.clear();
        super.close();
        return;
    }
}
