//
// AbstractLogEntry.java
//
//     Defines a LogEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.logentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>LogEntry</code>.
 */

public abstract class AbstractLogEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.tracking.LogEntry {

    private org.osid.authentication.Agent agent;
    private org.osid.tracking.Issue issue;
    private org.osid.calendaring.DateTime date;
    private org.osid.tracking.IssueAction action;
    private org.osid.locale.DisplayText summary;
    private org.osid.locale.DisplayText message;

    private final java.util.Collection<org.osid.tracking.records.LogEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the agent. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.agent.getId());
    }


    /**
     *  Gets the agent. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the issue of this log entry. 
     *
     *  @return the issue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getIssueId() {
        return (this.issue.getId());
    }


    /**
     *  Gets the issue of this log entry. 
     *
     *  @return the issue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.tracking.Issue getIssue()
        throws org.osid.OperationFailedException {

        return (this.issue);
    }


    /**
     *  Sets the issue.
     *
     *  @param issue an issue
     *  @throws org.osid.NullArgumentException
     *          <code>issue</code> is <code>null</code>
     */

    protected void setIssue(org.osid.tracking.Issue issue) {
        nullarg(issue, "issue");
        this.issue = issue;
        return;
    }


    /**
     *  Gets the date of this log entry. 
     *
     *  @return the date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.date);
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.date = date;
        return;
    }


    /**
     *  Gets the action of this log entry. 
     *
     *  @return the issue action 
     */

    @OSID @Override
    public org.osid.tracking.IssueAction getAction() {
        return (this.action);
    }


    /**
     *  Sets the action.
     *
     *  @param action an action
     *  @throws org.osid.NullArgumentException
     *          <code>action</code> is <code>null</code>
     */

    protected void setAction(org.osid.tracking.IssueAction action) {
        nullarg(action, "action");
        this.action = action;
        return;
    }


    /**
     *  Gets a summary header for this entry. 
     *
     *  @return the summary 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSummary() {
        return (this.summary);
    }


    /**
     *  Sets the summary.
     *
     *  @param summary a summary
     *  @throws org.osid.NullArgumentException
     *          <code>summary</code> is <code>null</code>
     */

    protected void setSummary(org.osid.locale.DisplayText summary) {
        nullarg(summary, "summary");
        this.summary = summary;
        return;
    }


    /**
     *  Gets the text of this entry. 
     *
     *  @return the entry text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getMessage() {
        return (this.message);
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException
     *          <code>message</code> is <code>null</code>
     */

    protected void setMessage(org.osid.locale.DisplayText message) {
        nullarg(message, "message");
        this.message = message;
        return;
    }


    /**
     *  Tests if this logEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  logEntryRecordType a log entry record type 
     *  @return <code>true</code> if the logEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type logEntryRecordType) {
        for (org.osid.tracking.records.LogEntryRecord record : this.records) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>LogEntry</code> record <code>Type</code>.
     *
     *  @param  logEntryRecordType the log entry record type 
     *  @return the log entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.LogEntryRecord getLogEntryRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.LogEntryRecord record : this.records) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this log entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param logEntryRecord the log entry record
     *  @param logEntryRecordType log entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecord</code> or
     *          <code>logEntryRecordTypelogEntry</code> is
     *          <code>null</code>
     */
            
    protected void addLogEntryRecord(org.osid.tracking.records.LogEntryRecord logEntryRecord, 
                                     org.osid.type.Type logEntryRecordType) {

        nullarg(logEntryRecord, "log entry record");
        addRecordType(logEntryRecordType);
        this.records.add(logEntryRecord);
        
        return;
    }
}
