//
// MutableMapProxyDictionaryLookupSession
//
//    Implements a Dictionary lookup service backed by a collection of
//    dictionaries that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary;


/**
 *  Implements a Dictionary lookup service backed by a collection of
 *  dictionaries. The dictionaries are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of dictionaries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyDictionaryLookupSession
    extends net.okapia.osid.jamocha.core.dictionary.spi.AbstractMapDictionaryLookupSession
    implements org.osid.dictionary.DictionaryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDictionaryLookupSession} with no
     *  dictionaries.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyDictionaryLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyDictionaryLookupSession} with a
     *  single dictionary.
     *
     *  @param dictionary a dictionary
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionary} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDictionaryLookupSession(org.osid.dictionary.Dictionary dictionary, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDictionary(dictionary);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDictionaryLookupSession} using an
     *  array of dictionaries.
     *
     *  @param dictionaries an array of dictionaries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionaries} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDictionaryLookupSession(org.osid.dictionary.Dictionary[] dictionaries, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDictionaries(dictionaries);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyDictionaryLookupSession} using
     *  a collection of dictionaries.
     *
     *  @param dictionaries a collection of dictionaries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code dictionaries} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDictionaryLookupSession(java.util.Collection<? extends org.osid.dictionary.Dictionary> dictionaries,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDictionaries(dictionaries);
        return;
    }

    
    /**
     *  Makes a {@code Dictionary} available in this session.
     *
     *  @param dictionary an dictionary
     *  @throws org.osid.NullArgumentException {@code dictionary{@code 
     *          is {@code null}
     */

    @Override
    public void putDictionary(org.osid.dictionary.Dictionary dictionary) {
        super.putDictionary(dictionary);
        return;
    }


    /**
     *  Makes an array of dictionaries available in this session.
     *
     *  @param dictionaries an array of dictionaries
     *  @throws org.osid.NullArgumentException {@code dictionaries{@code 
     *          is {@code null}
     */

    @Override
    public void putDictionaries(org.osid.dictionary.Dictionary[] dictionaries) {
        super.putDictionaries(dictionaries);
        return;
    }


    /**
     *  Makes collection of dictionaries available in this session.
     *
     *  @param dictionaries
     *  @throws org.osid.NullArgumentException {@code dictionary{@code 
     *          is {@code null}
     */

    @Override
    public void putDictionaries(java.util.Collection<? extends org.osid.dictionary.Dictionary> dictionaries) {
        super.putDictionaries(dictionaries);
        return;
    }


    /**
     *  Removes a Dictionary from this session.
     *
     *  @param dictionaryId the {@code Id} of the dictionary
     *  @throws org.osid.NullArgumentException {@code dictionaryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDictionary(org.osid.id.Id dictionaryId) {
        super.removeDictionary(dictionaryId);
        return;
    }    
}
