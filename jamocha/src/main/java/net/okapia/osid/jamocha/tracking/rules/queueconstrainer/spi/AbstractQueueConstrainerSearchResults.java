//
// AbstractQueueConstrainerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.rules.queueconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractQueueConstrainerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.tracking.rules.QueueConstrainerSearchResults {

    private org.osid.tracking.rules.QueueConstrainerList queueConstrainers;
    private final org.osid.tracking.rules.QueueConstrainerQueryInspector inspector;
    private final java.util.Collection<org.osid.tracking.rules.records.QueueConstrainerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractQueueConstrainerSearchResults.
     *
     *  @param queueConstrainers the result set
     *  @param queueConstrainerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>queueConstrainers</code>
     *          or <code>queueConstrainerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractQueueConstrainerSearchResults(org.osid.tracking.rules.QueueConstrainerList queueConstrainers,
                                            org.osid.tracking.rules.QueueConstrainerQueryInspector queueConstrainerQueryInspector) {
        nullarg(queueConstrainers, "queue constrainers");
        nullarg(queueConstrainerQueryInspector, "queue constrainer query inspectpr");

        this.queueConstrainers = queueConstrainers;
        this.inspector = queueConstrainerQueryInspector;

        return;
    }


    /**
     *  Gets the queue constrainer list resulting from a search.
     *
     *  @return a queue constrainer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerList getQueueConstrainers() {
        if (this.queueConstrainers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.tracking.rules.QueueConstrainerList queueConstrainers = this.queueConstrainers;
        this.queueConstrainers = null;
	return (queueConstrainers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.tracking.rules.QueueConstrainerQueryInspector getQueueConstrainerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  queue constrainer search record <code> Type. </code> This method must
     *  be used to retrieve a queueConstrainer implementing the requested
     *  record.
     *
     *  @param queueConstrainerSearchRecordType a queueConstrainer search 
     *         record type 
     *  @return the queue constrainer search
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(queueConstrainerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueConstrainerSearchResultsRecord getQueueConstrainerSearchResultsRecord(org.osid.type.Type queueConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.tracking.rules.records.QueueConstrainerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(queueConstrainerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(queueConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record queue constrainer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addQueueConstrainerRecord(org.osid.tracking.rules.records.QueueConstrainerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "queue constrainer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
