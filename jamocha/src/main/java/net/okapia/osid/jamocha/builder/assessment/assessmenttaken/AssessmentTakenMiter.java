//
// AssessmentTakenMiter.java
//
//     Defines an AssessmentTaken miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmenttaken;


/**
 *  Defines an <code>AssessmentTaken</code> miter for use with the builders.
 */

public interface AssessmentTakenMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.assessment.AssessmentTaken {


    /**
     *  Sets the assessment offered.
     *
     *  @param assessmentOffered an assessment offered
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOffered</code> is <code>null</code>
     */

    public void setAssessmentOffered(org.osid.assessment.AssessmentOffered assessmentOffered);


    /**
     *  Sets the taker.
     *
     *  @param taker a taker
     *  @throws org.osid.NullArgumentException <code>taker</code> is
     *          <code>null</code>
     */

    public void setTaker(org.osid.resource.Resource taker);


    /**
     *  Sets the taking agent.
     *
     *  @param agent a taking agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setTakingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the actual start time.
     *
     *  @param time an actual start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setActualStartTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the completion time.
     *
     *  @param time a completion time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setCompletionTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the time spent.
     *
     *  @param timeSpent a time spent
     *  @throws org.osid.NullArgumentException <code>timeSpent</code>
     *          is <code>null</code>
     */

    public void setTimeSpent(org.osid.calendaring.Duration timeSpent);


    /**
     *  Sets the completion.
     *
     *  @param completion a completion
     *  @throws org.osid.NullArgumentException <code>completion</code>
     *          is <code>null</code>
     */

    public void setCompletion(long completion);


    /**
     *  Sets the score system.
     *
     *  @param system a score system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public void setScoreSystem(org.osid.grading.GradeSystem system);


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    public void setScore(java.math.BigDecimal score);


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    public void setGrade(org.osid.grading.Grade grade);


    /**
     *  Sets the feedback.
     *
     *  @param feedback the feedback
     *  @throws org.osid.NullArgumentException <code>feedback</code> is
     *          <code>null</code>
     */

    public void setFeedback(org.osid.locale.DisplayText feedback);


    /**
     *  Sets the rubric.
     *
     *  @param assessmentTaken the rubric assessment taken
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTaken</code> is <code>null</code>
     */

    public void setRubric(org.osid.assessment.AssessmentTaken assessmentTaken);


    /**
     *  Adds an AssessmentTaken record.
     *
     *  @param record an assessmentTaken record
     *  @param recordType the type of assessmentTaken record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAssessmentTakenRecord(org.osid.assessment.records.AssessmentTakenRecord record, org.osid.type.Type recordType);
}       


