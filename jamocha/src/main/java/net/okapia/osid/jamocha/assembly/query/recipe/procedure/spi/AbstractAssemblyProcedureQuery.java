//
// AbstractAssemblyProcedureQuery.java
//
//     A ProcedureQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.recipe.procedure.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProcedureQuery that stores terms.
 */

public abstract class AbstractAssemblyProcedureQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.recipe.ProcedureQuery,
               org.osid.recipe.ProcedureQueryInspector,
               org.osid.recipe.ProcedureSearchOrder {

    private final java.util.Collection<org.osid.recipe.records.ProcedureQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.records.ProcedureQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.records.ProcedureSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProcedureQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProcedureQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        getAssembler().addIdTerm(getLearningObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the objective <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        getAssembler().clearTerms(getLearningObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the objective <code> Id </code> terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getLearningObjectiveIdColumn()));
    }


    /**
     *  Gets the LearningObjectiveId column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveIdColumn() {
        return ("learning_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches directions with any objective. 
     *
     *  @param  match <code> true </code> to match directions with any 
     *          objective, <code> false </code> to match directions with no 
     *          objectives 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getLearningObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the objective query terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        getAssembler().clearTerms(getLearningObjectiveColumn());
        return;
    }


    /**
     *  Gets the objective terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the LearningObjective column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveColumn() {
        return ("learning_objective");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match procedures 
     *  assigned to cook books. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCookbookId(org.osid.id.Id cookbookId, boolean match) {
        getAssembler().addIdTerm(getCookbookIdColumn(), cookbookId, match);
        return;
    }


    /**
     *  Clears the cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCookbookIdTerms() {
        getAssembler().clearTerms(getCookbookIdColumn());
        return;
    }


    /**
     *  Gets the cook book <code> Id </code> terms. 
     *
     *  @return the cook book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCookbookIdTerms() {
        return (getAssembler().getIdTerms(getCookbookIdColumn()));
    }


    /**
     *  Gets the CookbookId column name.
     *
     * @return the column name
     */

    protected String getCookbookIdColumn() {
        return ("cookbook_id");
    }


    /**
     *  Tests if an <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cook book query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsCookbookQuery() is false");
    }


    /**
     *  Clears the cook book terms. 
     */

    @OSID @Override
    public void clearCookbookTerms() {
        getAssembler().clearTerms(getCookbookColumn());
        return;
    }


    /**
     *  Gets the cook book terms. 
     *
     *  @return the cook book terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }


    /**
     *  Gets the Cookbook column name.
     *
     * @return the column name
     */

    protected String getCookbookColumn() {
        return ("cookbook");
    }


    /**
     *  Tests if this procedure supports the given record
     *  <code>Type</code>.
     *
     *  @param  procedureRecordType a procedure record type 
     *  @return <code>true</code> if the procedureRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type procedureRecordType) {
        for (org.osid.recipe.records.ProcedureQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(procedureRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  procedureRecordType the procedure record type 
     *  @return the procedure query record 
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(procedureRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.ProcedureQueryRecord getProcedureQueryRecord(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.ProcedureQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(procedureRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(procedureRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  procedureRecordType the procedure record type 
     *  @return the procedure query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(procedureRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.ProcedureQueryInspectorRecord getProcedureQueryInspectorRecord(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.ProcedureQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(procedureRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(procedureRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param procedureRecordType the procedure record type
     *  @return the procedure search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>procedureRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(procedureRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.ProcedureSearchOrderRecord getProcedureSearchOrderRecord(org.osid.type.Type procedureRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.ProcedureSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(procedureRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(procedureRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this procedure. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param procedureQueryRecord the procedure query record
     *  @param procedureQueryInspectorRecord the procedure query inspector
     *         record
     *  @param procedureSearchOrderRecord the procedure search order record
     *  @param procedureRecordType procedure record type
     *  @throws org.osid.NullArgumentException
     *          <code>procedureQueryRecord</code>,
     *          <code>procedureQueryInspectorRecord</code>,
     *          <code>procedureSearchOrderRecord</code> or
     *          <code>procedureRecordTypeprocedure</code> is
     *          <code>null</code>
     */
            
    protected void addProcedureRecords(org.osid.recipe.records.ProcedureQueryRecord procedureQueryRecord, 
                                      org.osid.recipe.records.ProcedureQueryInspectorRecord procedureQueryInspectorRecord, 
                                      org.osid.recipe.records.ProcedureSearchOrderRecord procedureSearchOrderRecord, 
                                      org.osid.type.Type procedureRecordType) {

        addRecordType(procedureRecordType);

        nullarg(procedureQueryRecord, "procedure query record");
        nullarg(procedureQueryInspectorRecord, "procedure query inspector record");
        nullarg(procedureSearchOrderRecord, "procedure search odrer record");

        this.queryRecords.add(procedureQueryRecord);
        this.queryInspectorRecords.add(procedureQueryInspectorRecord);
        this.searchOrderRecords.add(procedureSearchOrderRecord);
        
        return;
    }
}
