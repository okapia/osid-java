//
// AbstractBallotConstrainerEnablerSearch.java
//
//     A template for making a BallotConstrainerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.ballotconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing ballot constrainer enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBallotConstrainerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.rules.BallotConstrainerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.rules.BallotConstrainerEnablerSearchOrder ballotConstrainerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of ballot constrainer enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  ballotConstrainerEnablerIds list of ballot constrainer enablers
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBallotConstrainerEnablers(org.osid.id.IdList ballotConstrainerEnablerIds) {
        while (ballotConstrainerEnablerIds.hasNext()) {
            try {
                this.ids.add(ballotConstrainerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBallotConstrainerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of ballot constrainer enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBallotConstrainerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  ballotConstrainerEnablerSearchOrder ballot constrainer enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>ballotConstrainerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBallotConstrainerEnablerResults(org.osid.voting.rules.BallotConstrainerEnablerSearchOrder ballotConstrainerEnablerSearchOrder) {
	this.ballotConstrainerEnablerSearchOrder = ballotConstrainerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.rules.BallotConstrainerEnablerSearchOrder getBallotConstrainerEnablerSearchOrder() {
	return (this.ballotConstrainerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given ballot constrainer enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a ballot constrainer enabler implementing the requested record.
     *
     *  @param ballotConstrainerEnablerSearchRecordType a ballot constrainer enabler search record
     *         type
     *  @return the ballot constrainer enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerEnablerSearchRecord getBallotConstrainerEnablerSearchRecord(org.osid.type.Type ballotConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.rules.records.BallotConstrainerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(ballotConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ballot constrainer enabler search. 
     *
     *  @param ballotConstrainerEnablerSearchRecord ballot constrainer enabler search record
     *  @param ballotConstrainerEnablerSearchRecordType ballotConstrainerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBallotConstrainerEnablerSearchRecord(org.osid.voting.rules.records.BallotConstrainerEnablerSearchRecord ballotConstrainerEnablerSearchRecord, 
                                           org.osid.type.Type ballotConstrainerEnablerSearchRecordType) {

        addRecordType(ballotConstrainerEnablerSearchRecordType);
        this.records.add(ballotConstrainerEnablerSearchRecord);        
        return;
    }
}
