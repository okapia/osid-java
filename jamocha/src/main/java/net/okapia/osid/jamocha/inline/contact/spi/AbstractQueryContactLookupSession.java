//
// AbstractQueryContactLookupSession.java
//
//    An inline adapter that maps a ContactLookupSession to
//    a ContactQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ContactLookupSession to
 *  a ContactQuerySession.
 */

public abstract class AbstractQueryContactLookupSession
    extends net.okapia.osid.jamocha.contact.spi.AbstractContactLookupSession
    implements org.osid.contact.ContactLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.contact.ContactQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryContactLookupSession.
     *
     *  @param querySession the underlying contact query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryContactLookupSession(org.osid.contact.ContactQuerySession querySession) {
        nullarg(querySession, "contact query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>AddressBook</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AddressBook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.session.getAddressBookId());
    }


    /**
     *  Gets the <code>AddressBook</code> associated with this 
     *  session.
     *
     *  @return the <code>AddressBook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAddressBook());
    }


    /**
     *  Tests if this user can perform <code>Contact</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupContacts() {
        return (this.session.canSearchContacts());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include contacts in address books which are children
     *  of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        this.session.useFederatedAddressBookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        this.session.useIsolatedAddressBookView();
        return;
    }
    

    /**
     *  Only contacts whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveContactView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All contacts of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveContactView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Contact</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Contact</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Contact</code> and
     *  retained for compatibility.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactId <code>Id</code> of the
     *          <code>Contact</code>
     *  @return the contact
     *  @throws org.osid.NotFoundException <code>contactId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>contactId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.Contact getContact(org.osid.id.Id contactId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchId(contactId, true);
        org.osid.contact.ContactList contacts = this.session.getContactsByQuery(query);
        if (contacts.hasNext()) {
            return (contacts.getNextContact());
        } 
        
        throw new org.osid.NotFoundException(contactId + " not found");
    }


    /**
     *  Gets a <code>ContactList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  contacts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Contacts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, contacts are returned that are currently effective.
     *  In any effective mode, effective contacts and those currently expired
     *  are returned.
     *
     *  @param  contactIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Contact</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>contactIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByIds(org.osid.id.IdList contactIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();

        try (org.osid.id.IdList ids = contactIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a <code>ContactList</code> corresponding to the given
     *  contact genus <code>Type</code> which does not include
     *  contacts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, contacts are returned that are currently effective.
     *  In any effective mode, effective contacts and those currently expired
     *  are returned.
     *
     *  @param  contactGenusType a contact genus type 
     *  @return the returned <code>Contact</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusType(org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchGenusType(contactGenusType, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a <code>ContactList</code> corresponding to the given
     *  contact genus <code>Type</code> and include any additional
     *  contacts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactGenusType a contact genus type 
     *  @return the returned <code>Contact</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByParentGenusType(org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchParentGenusType(contactGenusType, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a <code>ContactList</code> containing the given
     *  contact record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactRecordType a contact record type 
     *  @return the returned <code>Contact</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByRecordType(org.osid.type.Type contactRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchRecordType(contactRecordType, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a <code>ContactList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Contact</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.contact.ContactList getContactsOnDate(org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getContactsByQuery(query));
    }
        

    /**
     *  Gets a list of contacts of a genus type and with an effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  contactGenusType a contact genus type 
     *  @param  from the starting date 
     *  @param  to the ending date 
     *  @return the returned <code> ContactList </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> contactGenusType, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeOnDate(org.osid.type.Type contactGenusType, 
                                                                     org.osid.calendaring.DateTime from, 
                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchGenusType(contactGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of contacts corresponding to a reference
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @return the returned <code>ContactList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.contact.ContactList getContactsForReference(org.osid.id.Id referenceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of contacts corresponding to a reference
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ContactList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForReferenceOnDate(org.osid.id.Id referenceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchDate(from, to, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of contacts of the given genus type corresponding
     *  to a reference <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  referenceId the <code> Id </code> of the reference 
     *  @param  contactGenusType the genus type of the contact 
     *  @return the returned <code> ContactList </code> 
     *  @throws org.osid.NullArgumentException <code> referenceId </code> or 
     *          <code> contactGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForReference(org.osid.id.Id referenceId, 
                                                                           org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchGenusType(contactGenusType, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of all contacts of the given genus type
     *  corresponding to a reference <code> Id </code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  referenceId a reference <code> Id </code> 
     *  @param  contactGenusType the genus type of the contact 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> ContactList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> referenceId, 
     *          contactGenusType, from </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForReferenceOnDate(org.osid.id.Id referenceId, 
                                                                                 org.osid.type.Type contactGenusType, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchGenusType(contactGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of contacts corresponding to an address
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  addressId the <code>Id</code> of the address
     *  @return the returned <code>ContactList</code>
     *  @throws org.osid.NullArgumentException <code>addressId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.contact.ContactList getContactsForAddress(org.osid.id.Id addressId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchAddressId(addressId, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of contacts corresponding to an address
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  addressId the <code>Id</code> of the address
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ContactList</code>
     *  @throws org.osid.NullArgumentException <code>addressId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForAddressOnDate(org.osid.id.Id addressId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchAddressId(addressId, true);
        query.matchDate(from, to, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of all contacts of the given genus type
     *  corresponding to an address <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  addressId the <code> Id </code> of the address 
     *  @param  contactGenusType the genus type of the contact 
     *  @return the returned <code> ContactList </code> 
     *  @throws org.osid.NullArgumentException <code> addressId </code> or 
     *          <code> contactGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForAddress(org.osid.id.Id addressId, 
                                                                         org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchAddressId(addressId, true);
        query.matchGenusType(contactGenusType, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of all contacts of the given genus type
     *  corresponding to an address <code> Id </code> and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  contactGenusType the genus type of the contact 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> ContactList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> addressId, 
     *          contactGenusType, from </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForAddressOnDate(org.osid.id.Id addressId, 
                                                                               org.osid.type.Type contactGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchAddressId(addressId, true);
        query.matchGenusType(contactGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getContactsByQuery(query));
    }
        

    /**
     *  Gets a list of contacts corresponding to reference and address
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  addressId the <code>Id</code> of the address
     *  @return the returned <code>ContactList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code>,
     *          <code>addressId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForReferenceAndAddress(org.osid.id.Id referenceId,
                                                                          org.osid.id.Id addressId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchAddressId(addressId, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of contacts corresponding to reference and address
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible
     *  through this session.
     *
     *  In effective mode, contacts are returned that are
     *  currently effective.  In any effective mode, effective
     *  contacts and those currently expired are returned.
     *
     *  @param  referenceId the <code>Id</code> of the reference
     *  @param  addressId the <code>Id</code> of the address
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ContactList</code>
     *  @throws org.osid.NullArgumentException <code>referenceId</code>,
     *          <code>addressId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsForReferenceAndAddressOnDate(org.osid.id.Id referenceId,
                                                                                org.osid.id.Id addressId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.contact.ContactQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchAddressId(addressId, true);
        query.matchDate(from, to, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of all contacts with the given genus type
     *  corresponding to a reference and address <code> Id. </code>
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  referenceId the <code> Id </code> of the reference 
     *  @param  addressId the <code> Id </code> of the address 
     *  @param  contactGenusType the genus type of the contact 
     *  @return the returned <code> ContactList </code> 
     *  @throws org.osid.NullArgumentException <code> referenceId, addressId 
     *          </code> or <code> contactGenusType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForReferenceAndAddress(org.osid.id.Id referenceId, 
                                                                                     org.osid.id.Id addressId, 
                                                                                     org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchAddressId(addressId, true);
        query.matchGenusType(contactGenusType, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets a list of all contacts with the given genus type
     *  corresponding to a reference and address <code> Id </code> and
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known contacts
     *  or an error results. Otherwise, the returned list may contain
     *  only those contacts that are accessible through this session.
     *  
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @param  referenceId the <code> Id </code> of the reference 
     *  @param  addressId an address <code> Id </code> 
     *  @param  contactGenusType the genus type of the contact 
     *  @param  from from date 
     *  @param  to to date 
     *  @return the returned <code> ContactList </code> 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> referenceId, addressId, 
     *          contactGenusType, from </code> or <code> to </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContactsByGenusTypeForReferenceAndAddressOnDate(org.osid.id.Id referenceId, 
                                                                                           org.osid.id.Id addressId, 
                                                                                           org.osid.type.Type contactGenusType, 
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.contact.ContactQuery query = getQuery();
        query.matchReferenceId(referenceId, true);
        query.matchAddressId(addressId, true);
        query.matchGenusType(contactGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets all <code>Contacts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  contacts or an error results. Otherwise, the returned list
     *  may contain only those contacts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, contacts are returned that are currently
     *  effective.  In any effective mode, effective contacts and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Contacts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.ContactList getContacts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.contact.ContactQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getContactsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.contact.ContactQuery getQuery() {
        org.osid.contact.ContactQuery query = this.session.getContactQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
