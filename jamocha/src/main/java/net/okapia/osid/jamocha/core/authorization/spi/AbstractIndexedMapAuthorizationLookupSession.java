//
// AbstractIndexedMapAuthorizationLookupSession.java
//
//    A simple framework for providing an Authorization lookup service
//    backed by a fixed collection of authorizations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Authorization lookup service backed by a
 *  fixed collection of authorizations. The authorizations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some authorizations may be compatible
 *  with more types than are indicated through these authorization
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Authorizations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuthorizationLookupSession
    extends AbstractMapAuthorizationLookupSession
    implements org.osid.authorization.AuthorizationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authorization.Authorization> authorizationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.Authorization>());
    private final MultiMap<org.osid.type.Type, org.osid.authorization.Authorization> authorizationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.Authorization>());


    /**
     *  Makes an <code>Authorization</code> available in this session.
     *
     *  @param  authorization an authorization
     *  @throws org.osid.NullArgumentException <code>authorization<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuthorization(org.osid.authorization.Authorization authorization) {
        super.putAuthorization(authorization);

        this.authorizationsByGenus.put(authorization.getGenusType(), authorization);
        
        try (org.osid.type.TypeList types = authorization.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.authorizationsByRecord.put(types.getNextType(), authorization);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an authorization from this session.
     *
     *  @param authorizationId the <code>Id</code> of the authorization
     *  @throws org.osid.NullArgumentException <code>authorizationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuthorization(org.osid.id.Id authorizationId) {
        org.osid.authorization.Authorization authorization;
        try {
            authorization = getAuthorization(authorizationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.authorizationsByGenus.remove(authorization.getGenusType());

        try (org.osid.type.TypeList types = authorization.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.authorizationsByRecord.remove(types.getNextType(), authorization);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuthorization(authorizationId);
        return;
    }


    /**
     *  Gets an <code>AuthorizationList</code> corresponding to the given
     *  authorization genus <code>Type</code> which does not include
     *  authorizations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known authorizations or an error results. Otherwise,
     *  the returned list may contain only those authorizations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  authorizationGenusType an authorization genus type 
     *  @return the returned <code>Authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByGenusType(org.osid.type.Type authorizationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.authorization.ArrayAuthorizationList(this.authorizationsByGenus.get(authorizationGenusType)));
    }


    /**
     *  Gets an <code>AuthorizationList</code> containing the given
     *  authorization record <code>Type</code>. In plenary mode, the
     *  returned list contains all known authorizations or an error
     *  results. Otherwise, the returned list may contain only those
     *  authorizations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  authorizationRecordType an authorization record type 
     *  @return the returned <code>authorization</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizationsByRecordType(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.authorization.ArrayAuthorizationList(this.authorizationsByRecord.get(authorizationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.authorizationsByGenus.clear();
        this.authorizationsByRecord.clear();

        super.close();

        return;
    }
}
