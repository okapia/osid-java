//
// AbstractProfileItemQuery.java
//
//     A template for making a ProfileItem Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profileitem.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for profile items.
 */

public abstract class AbstractProfileItemQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.profile.ProfileItemQuery {

    private final java.util.Collection<org.osid.profile.records.ProfileItemQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the pofile entry <code> Id </code> for this query. 
     *
     *  @param  profileEntryId a profile entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileEntryId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchProfileEntryId(org.osid.id.Id profileEntryId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the profile entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileEntryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ProfileEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getProfileEntryQuery() {
        throw new org.osid.UnimplementedException("supportsProfileEntryQuery() is false");
    }


    /**
     *  Matches profile items that have any profile entry mapping. 
     *
     *  @param  match <code> true </code> to match items with any entry 
     *          mapping, <code> false </code> to match items with no entry 
     *          mapping 
     */

    @OSID @Override
    public void matchAnyProfileEntry(boolean match) {
        return;
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearProfileEntryTerms() {
        return;
    }


    /**
     *  Sets the profile <code> Id </code> for this query. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileId(org.osid.id.Id profileId, boolean match) {
        return;
    }


    /**
     *  Clears the profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getProfileQuery() {
        throw new org.osid.UnimplementedException("supportsProfileQuery() is false");
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearProfileTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given profile item query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a profile item implementing the requested record.
     *
     *  @param profileItemRecordType a profile item record type
     *  @return the profile item query record
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileItemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileItemQueryRecord getProfileItemQueryRecord(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileItemQueryRecord record : this.records) {
            if (record.implementsRecordType(profileItemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileItemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile item query. 
     *
     *  @param profileItemQueryRecord profile item query record
     *  @param profileItemRecordType profileItem record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileItemQueryRecord(org.osid.profile.records.ProfileItemQueryRecord profileItemQueryRecord, 
                                          org.osid.type.Type profileItemRecordType) {

        addRecordType(profileItemRecordType);
        nullarg(profileItemQueryRecord, "profile item query record");
        this.records.add(profileItemQueryRecord);        
        return;
    }
}
