//
// AbstractVotingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractVotingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.voting.batch.VotingBatchManager,
               org.osid.voting.batch.VotingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractVotingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractVotingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk voting is available. 
     *
     *  @return <code> true </code> if a voting bulk service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of candidates is available. 
     *
     *  @return <code> true </code> if a candidate bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of races is available. 
     *
     *  @return <code> true </code> if a race bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of ballots is available. 
     *
     *  @return <code> true </code> if a ballot bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of polls is available. 
     *
     *  @return <code> true </code> if a polls bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk voting 
     *  service. 
     *
     *  @return a <code> VotingBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchSession getVotingBatchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getVotingBatchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk voting 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VotingBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchSession getVotingBatchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getVotingBatchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk voting 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VotingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchSession getVoteBatchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getVoteBatchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk voting 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> VotingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchSession getVoteBatchSessionForPolls(org.osid.id.Id pollsId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getVoteBatchSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk candidate 
     *  administration service. 
     *
     *  @return a <code> CandidateBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.CandidateBatchAdminSession getCandidateBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getCandidateBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk candidate 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CandidateBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.CandidateBatchAdminSession getCandidateBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getCandidateBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk candidate 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> CandidateBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.CandidateBatchAdminSession getCandidateBatchAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getCandidateBatchAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk candidate 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CandidateBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.CandidateBatchAdminSession getCandidateBatchAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getCandidateBatchAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk race 
     *  administration service. 
     *
     *  @return a <code> RaceBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.RaceBatchAdminSession getRaceBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getRaceBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ballot 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.RaceBatchAdminSession getRaceBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getRaceBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk race 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.RaceBatchAdminSession getRaceBatchAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getRaceBatchAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk race 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RaceBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.RaceBatchAdminSession getRaceBatchAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getRaceBatchAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ballot 
     *  administration service. 
     *
     *  @return a <code> BallotBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.BallotBatchAdminSession getBallotBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getBallotBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ballot 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BallotBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.BallotBatchAdminSession getBallotBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getBallotBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ballot 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.BallotBatchAdminSession getBallotBatchAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getBallotBatchAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ballot 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BallotBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.BallotBatchAdminSession getBallotBatchAdminSessionForPolls(org.osid.id.Id pollsId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getBallotBatchAdminSessionForPolls not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk polls 
     *  administration service. 
     *
     *  @return a <code> PollsBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.PollsBatchAdminSession getPollsBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchManager.getPollsBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk polls 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PollsBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.PollsBatchAdminSession getPollsBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.voting.batch.VotingBatchProxyManager.getPollsBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
