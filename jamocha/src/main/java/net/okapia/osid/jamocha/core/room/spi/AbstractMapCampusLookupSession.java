//
// AbstractMapCampusLookupSession
//
//    A simple framework for providing a Campus lookup service
//    backed by a fixed collection of campuses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Campus lookup service backed by a
 *  fixed collection of campuses. The campuses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Campuses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCampusLookupSession
    extends net.okapia.osid.jamocha.room.spi.AbstractCampusLookupSession
    implements org.osid.room.CampusLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.room.Campus> campuses = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.room.Campus>());


    /**
     *  Makes a <code>Campus</code> available in this session.
     *
     *  @param  campus a campus
     *  @throws org.osid.NullArgumentException <code>campus<code>
     *          is <code>null</code>
     */

    protected void putCampus(org.osid.room.Campus campus) {
        this.campuses.put(campus.getId(), campus);
        return;
    }


    /**
     *  Makes an array of campuses available in this session.
     *
     *  @param  campuses an array of campuses
     *  @throws org.osid.NullArgumentException <code>campuses<code>
     *          is <code>null</code>
     */

    protected void putCampuses(org.osid.room.Campus[] campuses) {
        putCampuses(java.util.Arrays.asList(campuses));
        return;
    }


    /**
     *  Makes a collection of campuses available in this session.
     *
     *  @param  campuses a collection of campuses
     *  @throws org.osid.NullArgumentException <code>campuses<code>
     *          is <code>null</code>
     */

    protected void putCampuses(java.util.Collection<? extends org.osid.room.Campus> campuses) {
        for (org.osid.room.Campus campus : campuses) {
            this.campuses.put(campus.getId(), campus);
        }

        return;
    }


    /**
     *  Removes a Campus from this session.
     *
     *  @param  campusId the <code>Id</code> of the campus
     *  @throws org.osid.NullArgumentException <code>campusId<code> is
     *          <code>null</code>
     */

    protected void removeCampus(org.osid.id.Id campusId) {
        this.campuses.remove(campusId);
        return;
    }


    /**
     *  Gets the <code>Campus</code> specified by its <code>Id</code>.
     *
     *  @param  campusId <code>Id</code> of the <code>Campus</code>
     *  @return the campus
     *  @throws org.osid.NotFoundException <code>campusId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>campusId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.Campus campus = this.campuses.get(campusId);
        if (campus == null) {
            throw new org.osid.NotFoundException("campus not found: " + campusId);
        }

        return (campus);
    }


    /**
     *  Gets all <code>Campuses</code>. In plenary mode, the returned
     *  list contains all known campuses or an error
     *  results. Otherwise, the returned list may contain only those
     *  campuses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Campuses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampuses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.campus.ArrayCampusList(this.campuses.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.campuses.clear();
        super.close();
        return;
    }
}
