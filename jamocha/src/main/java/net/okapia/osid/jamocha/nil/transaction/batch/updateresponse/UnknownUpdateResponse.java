//
// UnknownUpdateResponse
//
//     Defines an unknown UpdateResponse.
//
//
// Tom Coppeto
// Okapia
// 8 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.transaction.batch.updateresponse;


/**
 *  Defines an unknown <code>UpdateResponse</code>.
 */

public final class UnknownUpdateResponse
    extends net.okapia.osid.jamocha.nil.transaction.batch.updateresponse.spi.AbstractUnknownUpdateResponse
    implements org.osid.transaction.batch.UpdateResponse {


    /**
     *  Constructs a new <code>UpdateResponse</code>.
     */

    public UnknownUpdateResponse() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownUpdateResponse</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownUpdateResponse(boolean optional) {
        super(optional);
        return;
    }


    /**
     *  Gets an unknown UpdateResponse.
     *
     *  @return an unknown UpdateResponse
     */

    public static org.osid.transaction.batch.UpdateResponse create() {
        return (net.okapia.osid.jamocha.builder.validator.transaction.batch.updateresponse.UpdateResponseValidator.validateUpdateResponse(new UnknownUpdateResponse()));
    }
}
