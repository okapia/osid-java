//
// MutableMapHoldLookupSession
//
//    Implements a Hold lookup service backed by a collection of
//    holds that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold;


/**
 *  Implements a Hold lookup service backed by a collection of
 *  holds. The holds are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of holds can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapHoldLookupSession
    extends net.okapia.osid.jamocha.core.hold.spi.AbstractMapHoldLookupSession
    implements org.osid.hold.HoldLookupSession {


    /**
     *  Constructs a new {@code MutableMapHoldLookupSession}
     *  with no holds.
     *
     *  @param oubliette the oubliette
     *  @throws org.osid.NullArgumentException {@code oubliette} is
     *          {@code null}
     */

      public MutableMapHoldLookupSession(org.osid.hold.Oubliette oubliette) {
        setOubliette(oubliette);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapHoldLookupSession} with a
     *  single hold.
     *
     *  @param oubliette the oubliette  
     *  @param hold a hold
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code hold} is {@code null}
     */

    public MutableMapHoldLookupSession(org.osid.hold.Oubliette oubliette,
                                           org.osid.hold.Hold hold) {
        this(oubliette);
        putHold(hold);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapHoldLookupSession}
     *  using an array of holds.
     *
     *  @param oubliette the oubliette
     *  @param holds an array of holds
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code holds} is {@code null}
     */

    public MutableMapHoldLookupSession(org.osid.hold.Oubliette oubliette,
                                           org.osid.hold.Hold[] holds) {
        this(oubliette);
        putHolds(holds);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapHoldLookupSession}
     *  using a collection of holds.
     *
     *  @param oubliette the oubliette
     *  @param holds a collection of holds
     *  @throws org.osid.NullArgumentException {@code oubliette} or
     *          {@code holds} is {@code null}
     */

    public MutableMapHoldLookupSession(org.osid.hold.Oubliette oubliette,
                                           java.util.Collection<? extends org.osid.hold.Hold> holds) {

        this(oubliette);
        putHolds(holds);
        return;
    }

    
    /**
     *  Makes a {@code Hold} available in this session.
     *
     *  @param hold a hold
     *  @throws org.osid.NullArgumentException {@code hold{@code  is
     *          {@code null}
     */

    @Override
    public void putHold(org.osid.hold.Hold hold) {
        super.putHold(hold);
        return;
    }


    /**
     *  Makes an array of holds available in this session.
     *
     *  @param holds an array of holds
     *  @throws org.osid.NullArgumentException {@code holds{@code 
     *          is {@code null}
     */

    @Override
    public void putHolds(org.osid.hold.Hold[] holds) {
        super.putHolds(holds);
        return;
    }


    /**
     *  Makes collection of holds available in this session.
     *
     *  @param holds a collection of holds
     *  @throws org.osid.NullArgumentException {@code holds{@code  is
     *          {@code null}
     */

    @Override
    public void putHolds(java.util.Collection<? extends org.osid.hold.Hold> holds) {
        super.putHolds(holds);
        return;
    }


    /**
     *  Removes a Hold from this session.
     *
     *  @param holdId the {@code Id} of the hold
     *  @throws org.osid.NullArgumentException {@code holdId{@code 
     *          is {@code null}
     */

    @Override
    public void removeHold(org.osid.id.Id holdId) {
        super.removeHold(holdId);
        return;
    }    
}
