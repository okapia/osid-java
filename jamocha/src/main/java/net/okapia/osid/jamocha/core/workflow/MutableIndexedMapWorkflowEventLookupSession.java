//
// MutableIndexedMapWorkflowEventLookupSession
//
//    Implements a WorkflowEvent lookup service backed by a collection of
//    workflowEvents indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow;


/**
 *  Implements a WorkflowEvent lookup service backed by a collection of
 *  workflow events. The workflow events are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some workflow events may be compatible
 *  with more types than are indicated through these workflow event
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of workflow events can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapWorkflowEventLookupSession
    extends net.okapia.osid.jamocha.core.workflow.spi.AbstractIndexedMapWorkflowEventLookupSession
    implements org.osid.workflow.WorkflowEventLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapWorkflowEventLookupSession} with no workflow events.
     *
     *  @param office the office
     *  @throws org.osid.NullArgumentException {@code office}
     *          is {@code null}
     */

      public MutableIndexedMapWorkflowEventLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapWorkflowEventLookupSession} with a
     *  single workflow event.
     *  
     *  @param office the office
     *  @param  workflowEvent a single workflowEvent
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code workflowEvent} is {@code null}
     */

    public MutableIndexedMapWorkflowEventLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.WorkflowEvent workflowEvent) {
        this(office);
        putWorkflowEvent(workflowEvent);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapWorkflowEventLookupSession} using an
     *  array of workflow events.
     *
     *  @param office the office
     *  @param  workflowEvents an array of workflow events
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code workflowEvents} is {@code null}
     */

    public MutableIndexedMapWorkflowEventLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.WorkflowEvent[] workflowEvents) {
        this(office);
        putWorkflowEvents(workflowEvents);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapWorkflowEventLookupSession} using a
     *  collection of workflow events.
     *
     *  @param office the office
     *  @param  workflowEvents a collection of workflow events
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code workflowEvents} is {@code null}
     */

    public MutableIndexedMapWorkflowEventLookupSession(org.osid.workflow.Office office,
                                                  java.util.Collection<? extends org.osid.workflow.WorkflowEvent> workflowEvents) {

        this(office);
        putWorkflowEvents(workflowEvents);
        return;
    }
    

    /**
     *  Makes a {@code WorkflowEvent} available in this session.
     *
     *  @param  workflowEvent a workflow event
     *  @throws org.osid.NullArgumentException {@code workflowEvent{@code  is
     *          {@code null}
     */

    @Override
    public void putWorkflowEvent(org.osid.workflow.WorkflowEvent workflowEvent) {
        super.putWorkflowEvent(workflowEvent);
        return;
    }


    /**
     *  Makes an array of workflow events available in this session.
     *
     *  @param  workflowEvents an array of workflow events
     *  @throws org.osid.NullArgumentException {@code workflowEvents{@code 
     *          is {@code null}
     */

    @Override
    public void putWorkflowEvents(org.osid.workflow.WorkflowEvent[] workflowEvents) {
        super.putWorkflowEvents(workflowEvents);
        return;
    }


    /**
     *  Makes collection of workflow events available in this session.
     *
     *  @param  workflowEvents a collection of workflow events
     *  @throws org.osid.NullArgumentException {@code workflowEvent{@code  is
     *          {@code null}
     */

    @Override
    public void putWorkflowEvents(java.util.Collection<? extends org.osid.workflow.WorkflowEvent> workflowEvents) {
        super.putWorkflowEvents(workflowEvents);
        return;
    }


    /**
     *  Removes a WorkflowEvent from this session.
     *
     *  @param workflowEventId the {@code Id} of the workflow event
     *  @throws org.osid.NullArgumentException {@code workflowEventId{@code  is
     *          {@code null}
     */

    @Override
    public void removeWorkflowEvent(org.osid.id.Id workflowEventId) {
        super.removeWorkflowEvent(workflowEventId);
        return;
    }    
}
