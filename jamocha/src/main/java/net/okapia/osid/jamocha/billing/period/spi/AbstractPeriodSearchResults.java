//
// AbstractPeriodSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.period.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPeriodSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.billing.PeriodSearchResults {

    private org.osid.billing.PeriodList periods;
    private final org.osid.billing.PeriodQueryInspector inspector;
    private final java.util.Collection<org.osid.billing.records.PeriodSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPeriodSearchResults.
     *
     *  @param periods the result set
     *  @param periodQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>periods</code>
     *          or <code>periodQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPeriodSearchResults(org.osid.billing.PeriodList periods,
                                            org.osid.billing.PeriodQueryInspector periodQueryInspector) {
        nullarg(periods, "periods");
        nullarg(periodQueryInspector, "period query inspectpr");

        this.periods = periods;
        this.inspector = periodQueryInspector;

        return;
    }


    /**
     *  Gets the period list resulting from a search.
     *
     *  @return a period list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriods() {
        if (this.periods == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.billing.PeriodList periods = this.periods;
        this.periods = null;
	return (periods);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.billing.PeriodQueryInspector getPeriodQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  period search record <code> Type. </code> This method must
     *  be used to retrieve a period implementing the requested
     *  record.
     *
     *  @param periodSearchRecordType a period search 
     *         record type 
     *  @return the period search
     *  @throws org.osid.NullArgumentException
     *          <code>periodSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(periodSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.PeriodSearchResultsRecord getPeriodSearchResultsRecord(org.osid.type.Type periodSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.billing.records.PeriodSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(periodSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(periodSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record period search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPeriodRecord(org.osid.billing.records.PeriodSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "period record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
