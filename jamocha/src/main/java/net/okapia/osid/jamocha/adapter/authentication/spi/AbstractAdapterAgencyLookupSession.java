//
// AbstractAdapterAgencyLookupSession.java
//
//    An Agency lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Agency lookup session adapter.
 */

public abstract class AbstractAdapterAgencyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authentication.AgencyLookupSession {

    private final org.osid.authentication.AgencyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAgencyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAgencyLookupSession(org.osid.authentication.AgencyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Agency} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAgencies() {
        return (this.session.canLookupAgencies());
    }


    /**
     *  A complete view of the {@code Agency} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAgencyView() {
        this.session.useComparativeAgencyView();
        return;
    }


    /**
     *  A complete view of the {@code Agency} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAgencyView() {
        this.session.usePlenaryAgencyView();
        return;
    }

     
    /**
     *  Gets the {@code Agency} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Agency} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Agency} and
     *  retained for compatibility.
     *
     *  @param agencyId {@code Id} of the {@code Agency}
     *  @return the agency
     *  @throws org.osid.NotFoundException {@code agencyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code agencyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgency(agencyId));
    }


    /**
     *  Gets an {@code AgencyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  agencies specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Agencies} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *
     *  @param  agencyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Agency} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code agencyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByIds(org.osid.id.IdList agencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgenciesByIds(agencyIds));
    }


    /**
     *  Gets an {@code AgencyList} corresponding to the given
     *  agency genus {@code Type} which does not include
     *  agencies of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agencyGenusType an agency genus type 
     *  @return the returned {@code Agency} list
     *  @throws org.osid.NullArgumentException
     *          {@code agencyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByGenusType(org.osid.type.Type agencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgenciesByGenusType(agencyGenusType));
    }


    /**
     *  Gets an {@code AgencyList} corresponding to the given
     *  agency genus {@code Type} and include any additional
     *  agencies with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  agencyGenusType an agency genus type 
     *  @return the returned {@code Agency} list
     *  @throws org.osid.NullArgumentException
     *          {@code agencyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByParentGenusType(org.osid.type.Type agencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgenciesByParentGenusType(agencyGenusType));
    }


    /**
     *  Gets an {@code AgencyList} containing the given
     *  agency record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agencyRecordType an agency record type 
     *  @return the returned {@code Agency} list
     *  @throws org.osid.NullArgumentException
     *          {@code agencyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByRecordType(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgenciesByRecordType(agencyRecordType));
    }


    /**
     *  Gets an {@code AgencyList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Agency} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgenciesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Agencies}. 
     *
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Agencies} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgencies());
    }
}
