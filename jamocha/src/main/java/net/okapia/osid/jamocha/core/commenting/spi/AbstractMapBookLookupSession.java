//
// AbstractMapBookLookupSession
//
//    A simple framework for providing a Book lookup service
//    backed by a fixed collection of books.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Book lookup service backed by a
 *  fixed collection of books. The books are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Books</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBookLookupSession
    extends net.okapia.osid.jamocha.commenting.spi.AbstractBookLookupSession
    implements org.osid.commenting.BookLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.commenting.Book> books = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.commenting.Book>());


    /**
     *  Makes a <code>Book</code> available in this session.
     *
     *  @param  book a book
     *  @throws org.osid.NullArgumentException <code>book<code>
     *          is <code>null</code>
     */

    protected void putBook(org.osid.commenting.Book book) {
        this.books.put(book.getId(), book);
        return;
    }


    /**
     *  Makes an array of books available in this session.
     *
     *  @param  books an array of books
     *  @throws org.osid.NullArgumentException <code>books<code>
     *          is <code>null</code>
     */

    protected void putBooks(org.osid.commenting.Book[] books) {
        putBooks(java.util.Arrays.asList(books));
        return;
    }


    /**
     *  Makes a collection of books available in this session.
     *
     *  @param  books a collection of books
     *  @throws org.osid.NullArgumentException <code>books<code>
     *          is <code>null</code>
     */

    protected void putBooks(java.util.Collection<? extends org.osid.commenting.Book> books) {
        for (org.osid.commenting.Book book : books) {
            this.books.put(book.getId(), book);
        }

        return;
    }


    /**
     *  Removes a Book from this session.
     *
     *  @param  bookId the <code>Id</code> of the book
     *  @throws org.osid.NullArgumentException <code>bookId<code> is
     *          <code>null</code>
     */

    protected void removeBook(org.osid.id.Id bookId) {
        this.books.remove(bookId);
        return;
    }


    /**
     *  Gets the <code>Book</code> specified by its <code>Id</code>.
     *
     *  @param  bookId <code>Id</code> of the <code>Book</code>
     *  @return the book
     *  @throws org.osid.NotFoundException <code>bookId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>bookId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.commenting.Book book = this.books.get(bookId);
        if (book == null) {
            throw new org.osid.NotFoundException("book not found: " + bookId);
        }

        return (book);
    }


    /**
     *  Gets all <code>Books</code>. In plenary mode, the returned
     *  list contains all known books or an error
     *  results. Otherwise, the returned list may contain only those
     *  books that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Books</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.commenting.book.ArrayBookList(this.books.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.books.clear();
        super.close();
        return;
    }
}
