//
// AbstractBloggingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractBloggingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.blogging.batch.BloggingBatchManager,
               org.osid.blogging.batch.BloggingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractBloggingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractBloggingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of entries is available. 
     *
     *  @return <code> true </code> if an entry bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of blogs is available. 
     *
     *  @return <code> true </code> if a blog bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service. 
     *
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.EntryBatchAdminSession getEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.batch.BloggingBatchManager.getEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.EntryBatchAdminSession getEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.batch.BloggingBatchProxyManager.getEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the <code> Blog </code> 
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Blog </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.EntryBatchAdminSession getEntryBatchAdminSessionForBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.batch.BloggingBatchManager.getEntryBatchAdminSessionForBlog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk entry 
     *  administration service for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the <code> Blog </code> 
     *  @param  proxy a proxy 
     *  @return an <code> EntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Blog </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.EntryBatchAdminSession getEntryBatchAdminSessionForBlog(org.osid.id.Id blogId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.batch.BloggingBatchProxyManager.getEntryBatchAdminSessionForBlog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk blog 
     *  administration service. 
     *
     *  @return a <code> BlogBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.BlogBatchAdminSession getBlogBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.batch.BloggingBatchManager.getBlogBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk blog 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.BlogBatchAdminSession getBlogBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.batch.BloggingBatchProxyManager.getBlogBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
