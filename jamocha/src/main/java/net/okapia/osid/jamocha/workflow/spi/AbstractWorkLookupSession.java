//
// AbstractWorkLookupSession.java
//
//    A starter implementation framework for providing a Work
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Work
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getWorks(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractWorkLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.WorkLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();
    

    /**
     *  Gets the <code>Office/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this session.
     *
     *  @return the <code>Office</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>Work</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWorks() {
        return (true);
    }


    /**
     *  A complete view of the <code>Work</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWorkView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Work</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWorkView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include works in offices which are children of this
     *  office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Work</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Work</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Work</code> and retained for
     *  compatibility.
     *
     *  @param  workId <code>Id</code> of the
     *          <code>Work</code>
     *  @return the work
     *  @throws org.osid.NotFoundException <code>workId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>workId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Work getWork(org.osid.id.Id workId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.WorkList works = getWorks()) {
            while (works.hasNext()) {
                org.osid.workflow.Work work = works.getNextWork();
                if (work.getId().equals(workId)) {
                    return (work);
                }
            }
        } 

        throw new org.osid.NotFoundException(workId + " not found");
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the works
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Works</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getWorks()</code>.
     *
     *  @param  workIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>workIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorksByIds(org.osid.id.IdList workIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.Work> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = workIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getWork(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("work " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.work.LinkedWorkList(ret));
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given work
     *  genus <code>Type</code> which does not include works of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getWorks()</code>.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorksByGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.work.WorkGenusFilterList(getWorks(), workGenusType));
    }


    /**
     *  Gets a <code>WorkList</code> corresponding to the given work
     *  genus <code>Type</code> and include any additional works with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getWorks()</code>.
     *
     *  @param  workGenusType a work genus type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorksByParentGenusType(org.osid.type.Type workGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getWorksByGenusType(workGenusType));
    }


    /**
     *  Gets a <code>WorkList</code> containing the given work record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getWorks()</code>.
     *
     *  @param  workRecordType a work record type 
     *  @return the returned <code>Work</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkList getWorksByRecordType(org.osid.type.Type workRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.work.WorkRecordFilterList(getWorks(), workRecordType));
    }


    /**
     *  Gets all <code>Works</code>.
     *
     *  In plenary mode, the returned list contains all known works or
     *  an error results. Otherwise, the returned list may contain
     *  only those works that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Works</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.workflow.WorkList getWorks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the work list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of works
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.workflow.WorkList filterWorksOnViews(org.osid.workflow.WorkList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
