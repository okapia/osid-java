//
// AbstractRulesCheckManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRulesCheckManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.rules.check.RulesCheckManager,
               org.osid.rules.check.RulesCheckProxyManager {

    private final Types agendaRecordTypes                  = new TypeRefSet();
    private final Types agendaSearchRecordTypes            = new TypeRefSet();

    private final Types instructionRecordTypes             = new TypeRefSet();
    private final Types instructionSearchRecordTypes       = new TypeRefSet();

    private final Types checkRecordTypes                   = new TypeRefSet();
    private final Types checkSearchRecordTypes             = new TypeRefSet();

    private final Types checkResultRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRulesCheckManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRulesCheckManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any engine federation is exposed. Federation is exposed when 
     *  a specific engine may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  engines appears as a single engine. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if an agenda evaluation service is supported for the current 
     *  agent. 
     *
     *  @return <code> true </code> if an evaluation service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEvaluation() {
        return (false);
    }


    /**
     *  Tests if an agenda processing service is supported . 
     *
     *  @return <code> true </code> if a processing service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessing() {
        return (false);
    }


    /**
     *  Tests if looking up agendas is supported. 
     *
     *  @return <code> true </code> if agenda lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaLookup() {
        return (false);
    }


    /**
     *  Tests if querying agendas is supported. 
     *
     *  @return <code> true </code> if agenda query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaQuery() {
        return (false);
    }


    /**
     *  Tests if searching agendas is supported. 
     *
     *  @return <code> true </code> if agenda search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaSearch() {
        return (false);
    }


    /**
     *  Tests if agenda administrative service is supported. 
     *
     *  @return <code> true </code> if agenda administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaAdmin() {
        return (false);
    }


    /**
     *  Tests if an agenda notification service is supported. 
     *
     *  @return <code> true </code> if agenda notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaNotification() {
        return (false);
    }


    /**
     *  Tests if an agenda engine lookup service is supported. 
     *
     *  @return <code> true </code> if an agenda engine lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaEngine() {
        return (false);
    }


    /**
     *  Tests if an agenda engine service is supported. 
     *
     *  @return <code> true </code> if agenda to engine assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaEngineAssignment() {
        return (false);
    }


    /**
     *  Tests if an agenda smart engine lookup service is supported. 
     *
     *  @return <code> true </code> if an agenda smart engine service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaSmartEngine() {
        return (false);
    }


    /**
     *  Tests if looking up instructions is supported. 
     *
     *  @return <code> true </code> if instruction lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionLookup() {
        return (false);
    }


    /**
     *  Tests if querying instructions is supported. 
     *
     *  @return <code> true </code> if instruction query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionQuery() {
        return (false);
    }


    /**
     *  Tests if searching instructions is supported. 
     *
     *  @return <code> true </code> if instruction search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionSearch() {
        return (false);
    }


    /**
     *  Tests if instruction <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if instruction administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionAdmin() {
        return (false);
    }


    /**
     *  Tests if an instruction <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if instruction notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionNotification() {
        return (false);
    }


    /**
     *  Tests if an instruction engine lookup service is supported. 
     *
     *  @return <code> true </code> if an instruction engine lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionEngine() {
        return (false);
    }


    /**
     *  Tests if an instruction engine assignment service is supported. 
     *
     *  @return <code> true </code> if an instruction to engine assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionEngineAssignment() {
        return (false);
    }


    /**
     *  Tests if an instruction smart engine service is supported. 
     *
     *  @return <code> true </code> if an instruction smart engine service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructionSmartEngine() {
        return (false);
    }


    /**
     *  Tests if looking up checks is supported. 
     *
     *  @return <code> true </code> if check lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckLookup() {
        return (false);
    }


    /**
     *  Tests if querying checks is supported. 
     *
     *  @return <code> true </code> if check query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckQuery() {
        return (false);
    }


    /**
     *  Tests if searching checks is supported. 
     *
     *  @return <code> true </code> if check search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckSearch() {
        return (false);
    }


    /**
     *  Tests if check administrative service is supported. 
     *
     *  @return <code> true </code> if check administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckAdmin() {
        return (false);
    }


    /**
     *  Tests if a check notification service is supported. 
     *
     *  @return <code> true </code> if check notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckNotification() {
        return (false);
    }


    /**
     *  Tests if a check engine lookup service is supported. 
     *
     *  @return <code> true </code> if a check engine lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckEngine() {
        return (false);
    }


    /**
     *  Tests if a check engine service is supported. 
     *
     *  @return <code> true </code> if check to engine assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckEngineAssignment() {
        return (false);
    }


    /**
     *  Tests if a check smart engine lookup service is supported. 
     *
     *  @return <code> true </code> if a check smart engine service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckSmartEngine() {
        return (false);
    }


    /**
     *  Gets the supported <code> Agenda </code> record types. 
     *
     *  @return a list containing the supported <code> Agenda </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAgendaRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.agendaRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Agenda </code> record type is supported. 
     *
     *  @param  agendaRecordType a <code> Type </code> indicating an <code> 
     *          Agenda </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> agendaRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAgendaRecordType(org.osid.type.Type agendaRecordType) {
        return (this.agendaRecordTypes.contains(agendaRecordType));
    }


    /**
     *  Adds support for an agenda record type.
     *
     *  @param agendaRecordType an agenda record type
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     */

    protected void addAgendaRecordType(org.osid.type.Type agendaRecordType) {
        this.agendaRecordTypes.add(agendaRecordType);
        return;
    }


    /**
     *  Removes support for an agenda record type.
     *
     *  @param agendaRecordType an agenda record type
     *
     *  @throws org.osid.NullArgumentException
     *          <code>agendaRecordType</code> is <code>null</code>
     */

    protected void removeAgendaRecordType(org.osid.type.Type agendaRecordType) {
        this.agendaRecordTypes.remove(agendaRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Agenda </code> search record types. 
     *
     *  @return a list containing the supported <code> Agenda </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAgendaSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.agendaSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Agenda </code> search record type is 
     *  supported. 
     *
     *  @param  agendaSearchRecordType a <code> Type </code> indicating an 
     *          <code> Agenda </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> agendaSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAgendaSearchRecordType(org.osid.type.Type agendaSearchRecordType) {
        return (this.agendaSearchRecordTypes.contains(agendaSearchRecordType));
    }


    /**
     *  Adds support for an agenda search record type.
     *
     *  @param agendaSearchRecordType an agenda search record type
     *  @throws org.osid.NullArgumentException
     *          <code>agendaSearchRecordType</code> is
     *          <code>null</code>
     */

    protected void addAgendaSearchRecordType(org.osid.type.Type agendaSearchRecordType) {
        this.agendaSearchRecordTypes.add(agendaSearchRecordType);
        return;
    }


    /**
     *  Removes support for an agenda search record type.
     *
     *  @param agendaSearchRecordType an agenda search record type
     *
     *  @throws org.osid.NullArgumentException
     *          <code>agendaSearchRecordType</code> is
     *          <code>null</code>
     */

    protected void removeAgendaSearchRecordType(org.osid.type.Type agendaSearchRecordType) {
        this.agendaSearchRecordTypes.remove(agendaSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Instruction </code> record types. 
     *
     *  @return a list containing the supported <code> Instruction </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstructionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.instructionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Instruction </code> record type is 
     *  supported. 
     *
     *  @param  instructionRecordType a <code> Type </code> indicating an 
     *          <code> Instruction </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> instructionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstructionRecordType(org.osid.type.Type instructionRecordType) {
        return (this.instructionRecordTypes.contains(instructionRecordType));
    }


    /**
     *  Adds support for an instruction record type.
     *
     *  @param instructionRecordType an instruction record type
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is
     *          <code>null</code>
     */

    protected void addInstructionRecordType(org.osid.type.Type instructionRecordType) {
        this.instructionRecordTypes.add(instructionRecordType);
        return;
    }


    /**
     *  Removes support for an instruction record type.
     *
     *  @param instructionRecordType an instruction record type
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is
     *          <code>null</code>
     */

    protected void removeInstructionRecordType(org.osid.type.Type instructionRecordType) {
        this.instructionRecordTypes.remove(instructionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Instruction </code> search types. 
     *
     *  @return a list containing the supported <code> Instruction </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstructionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.instructionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Instruction </code> search type is 
     *  supported. 
     *
     *  @param  instructionSearchRecordType a <code> Type </code> indicating 
     *          an <code> Instruction </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          instructionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstructionSearchRecordType(org.osid.type.Type instructionSearchRecordType) {
        return (this.instructionSearchRecordTypes.contains(instructionSearchRecordType));
    }


    /**
     *  Adds support for an instruction search record type.
     *
     *  @param instructionSearchRecordType an instruction search record type
     *  @throws org.osid.NullArgumentException
     *          <code>instructionSearchRecordType</code> is
     *          <code>null</code>
     */

    protected void addInstructionSearchRecordType(org.osid.type.Type instructionSearchRecordType) {
        this.instructionSearchRecordTypes.add(instructionSearchRecordType);
        return;
    }


    /**
     *  Removes support for an instruction search record type.
     *
     *  @param instructionSearchRecordType an instruction search record type
     *  @throws org.osid.NullArgumentException
     *          <code>instructionSearchRecordType</code> is
     *          <code>null</code>
     */

    protected void removeInstructionSearchRecordType(org.osid.type.Type instructionSearchRecordType) {
        this.instructionSearchRecordTypes.remove(instructionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Check </code> record types. 
     *
     *  @return a list containing the supported <code> Check </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCheckRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.checkRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Check </code> record type is supported. 
     *
     *  @param  checkRecordType a <code> Type </code> indicating a <code> 
     *          Check </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> checkRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCheckRecordType(org.osid.type.Type checkRecordType) {
        return (this.checkRecordTypes.contains(checkRecordType));
    }


    /**
     *  Adds support for a check record type.
     *
     *  @param checkRecordType a check record type
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is <code>null</code>
     */

    protected void addCheckRecordType(org.osid.type.Type checkRecordType) {
        this.checkRecordTypes.add(checkRecordType);
        return;
    }


    /**
     *  Removes support for a check record type.
     *
     *  @param checkRecordType a check record type
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is <code>null</code>
     */

    protected void removeCheckRecordType(org.osid.type.Type checkRecordType) {
        this.checkRecordTypes.remove(checkRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Check </code> search record types. 
     *
     *  @return a list containing the supported <code> Check </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCheckSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.checkSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Check </code> search record type is 
     *  supported. 
     *
     *  @param  checkSearchRecordType a <code> Type </code> indicating a 
     *          <code> Check </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code>
     *          checkSearchRecordType </code> is <code> null </code>
     */

    @OSID @Override
    public boolean supportsCheckSearchRecordType(org.osid.type.Type checkSearchRecordType) {
        return (this.checkSearchRecordTypes.contains(checkSearchRecordType));
    }


    /**
     *  Adds support for a check search record type.
     *
     *  @param checkSearchRecordType a check search record type
     *  @throws org.osid.NullArgumentException
     *          <code>checkSearchRecordType</code> is
     *          <code>null</code>
     */

    protected void addCheckSearchRecordType(org.osid.type.Type checkSearchRecordType) {
        this.checkSearchRecordTypes.add(checkSearchRecordType);
        return;
    }


    /**
     *  Removes support for a check search record type.
     *
     *  @param checkSearchRecordType a check search record type
     *  @throws org.osid.NullArgumentException
     *          <code>checkSearchRecordType</code> is
     *          <code>null</code>
     */

    protected void removeCheckSearchRecordType(org.osid.type.Type checkSearchRecordType) {
        this.checkSearchRecordTypes.remove(checkSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Checkresult </code> record types. 
     *
     *  @return a list containing the supported <code> CheckResult
     *          </code> record types
     */

    @OSID @Override
    public org.osid.type.TypeList getCheckResultRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.checkResultRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CheckResult </code> record type is
     *  supported.
     *
     *  @param checkResultRecordType a <code> Type </code> indicating
     *         a <code> CheckResult </code> record type
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code>
     *          checkResultRecordType </code> is <code> null </code>
     */

    @OSID @Override
    public boolean supportsCheckResultRecordType(org.osid.type.Type checkResultRecordType) {
        return (this.checkResultRecordTypes.contains(checkResultRecordType));
    }


    /**
     *  Adds support for a check result record type.
     *
     *  @param checkResultRecordType a check record type
     *  @throws org.osid.NullArgumentException
     *          <code>checkResultRecordType</code> is
     *          <code>null</code>
     */

    protected void addCheckResultRecordType(org.osid.type.Type checkResultRecordType) {
        this.checkResultRecordTypes.add(checkResultRecordType);
        return;
    }


    /**
     *  Removes support for a check result record type.
     *
     *  @param checkResultRecordType a check result record type
     *
     *  @throws org.osid.NullArgumentException
     *          <code>checkResultRecordType</code> is
     *          <code>null</code>
     */

    protected void removeCheckResultRecordType(org.osid.type.Type checkResultRecordType) {
        this.checkResultRecordTypes.remove(checkResultRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the evaluation 
     *  service to evaluate an agenda. 
     *
     *  @return an <code> EvaluationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEvaluation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.EvaluationSession getEvaluationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getEvaluationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the evaluation 
     *  service to evaluate an agenda. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EvaluationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEvaluation() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.EvaluationSession getEvaluationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getEvaluationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the evaluation 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @return an <code> EvaluationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEvaluation() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.EvaluationSession getEvaluationSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getEvaluationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the evaluation 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @param  proxy a proxy 
     *  @return an <code> EvaluationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEvaluation() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.EvaluationSession getEvaluationSessionForEngine(org.osid.id.Id engineId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getEvaluationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the processing 
     *  service to run checks. 
     *
     *  @return a <code> ProcessingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessing() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.ProcessingSession getProcessingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getProcessingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the processing 
     *  service to run checks. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessing() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.ProcessingSession getProcessingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getProcessingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the processing 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @return a <code> ProcessingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessing() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.ProcessingSession getProcessingSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getProcessingSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the processing 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessing() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.ProcessingSession getProcessingSessionForEngine(org.osid.id.Id engineId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getProcessingSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda lookup 
     *  service. 
     *
     *  @return an <code> AgendaLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaLookupSession getAgendaLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaLookupSession getAgendaLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> AgendaLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaLookupSession getAgendaLookupSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaLookupSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaLookupSession getAgendaLookupSessionForEngine(org.osid.id.Id engineId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaLookupSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda query 
     *  service. 
     *
     *  @return an <code> AgendaQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuerySession getAgendaQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuerySession getAgendaQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> AgendaQuerySession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuerySession getAgendaQuerySessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaQuerySessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaQuerySession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuerySession getAgendaQuerySessionForEngine(org.osid.id.Id engineId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaQuerySessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda search 
     *  service. 
     *
     *  @return an <code> AgendaSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchSession getAgendaSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchSession getAgendaSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> AgendaSearchSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchSession getAgendaSearchSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaSearchSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchSession getAgendaSearchSessionForEngine(org.osid.id.Id engineId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  administration service. 
     *
     *  @return an <code> AgendaAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaAdminSession getAgendaAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaAdminSession getAgendaAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  administration service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> AgendaAdminSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaAdminSession getAgendaAdminSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaAdminSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  administration service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaAdminSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaAdminSession getAgendaAdminSessionForEngine(org.osid.id.Id engineId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaAdminSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  notification service. 
     *
     *  @param  agendaReceiver the notification callback 
     *  @return an <code> AgendaNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> agendaReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaNotificationSession getAgendaNotificationSession(org.osid.rules.check.AgendaReceiver agendaReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  notification service. 
     *
     *  @param  agendaReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> agendaReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaNotificationSession getAgendaNotificationSession(org.osid.rules.check.AgendaReceiver agendaReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  notification service for the given engine. 
     *
     *  @param  agendaReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> AgendaNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> agendaReceiver </code> 
     *          or <code> engineId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaNotificationSession getAgendaNotificationSessionForEngine(org.osid.rules.check.AgendaReceiver agendaReceiver, 
                                                                                                org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaNotificationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agenda 
     *  notification service for the given engine. 
     *
     *  @param  agendaReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> agendaReceiver, engineId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaNotificationSession getAgendaNotificationSessionForEngine(org.osid.rules.check.AgendaReceiver agendaReceiver, 
                                                                                                org.osid.id.Id engineId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaNotificationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup agenda/engine mappings. 
     *
     *  @return an <code> AgendaEngineSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaEngineSession getAgendaEngineSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup agenda/engine mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaEngineSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaEngineSession getAgendaEngineSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning agendas 
     *  to engines. 
     *
     *  @return an <code> AgendaEngineAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaEngineAssignmentSession getAgendaEngineAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaEngineAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning agendas 
     *  to engines. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgendaEngineAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaEngineAssignmentSession getAgendaEngineAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaEngineAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage agenda smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> AgendaSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaSmartEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSmartEngineSession getAgendaSmartEngineSession(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getAgendaSmartEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage agenda smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AgendaSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgendaSmartEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSmartEngineSession getAgendaSmartEngineSession(org.osid.id.Id engineId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getAgendaSmartEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  lookup service. 
     *
     *  @return an <code> InstructionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionLookupSession getInstructionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionLookupSession getInstructionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  lookup service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @return an <code> InstructionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionLookupSession getInstructionLookupSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionLookupSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  lookup service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the engine 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionLookupSession getInstructionLookupSessionForEngine(org.osid.id.Id engineId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionLookupSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  query service. 
     *
     *  @return an <code> InstructionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQuerySession getInstructionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQuerySession getInstructionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  query service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> InstructionQuerySession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQuerySession getInstructionQuerySessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionQuerySessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  query service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionQuerySession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionQuerySession getInstructionQuerySessionForEngine(org.osid.id.Id engineId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionQuerySessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  search service. 
     *
     *  @return an <code> InstructionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSearchSession getInstructionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSearchSession getInstructionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  search service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> InstructionSearchSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSearchSession getInstructionSearchSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  search service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionSearchSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSearchSession getInstructionSearchSessionForEngine(org.osid.id.Id engineId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  administration service. 
     *
     *  @return an <code> InstructionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionAdminSession getInstructionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionAdminSession getInstructionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  administration service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> InstructionAdminSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionAdminSession getInstructionAdminSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionAdminSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  administration service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionAdminSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionAdminSession getInstructionAdminSessionForEngine(org.osid.id.Id engineId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionAdminSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  notification service. 
     *
     *  @param  instructionReceiver the notification callback 
     *  @return an <code> InstructionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> instructionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionNotificationSession getInstructionNotificationSession(org.osid.rules.check.InstructionReceiver instructionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  notification service. 
     *
     *  @param  instructionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> instructionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionNotificationSession getInstructionNotificationSession(org.osid.rules.check.InstructionReceiver instructionReceiver, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  notification service for the given engine. 
     *
     *  @param  instructionReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> InstructionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> instructionReceiver 
     *          </code> or <code> engineId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionNotificationSession getInstructionNotificationSessionForEngine(org.osid.rules.check.InstructionReceiver instructionReceiver, 
                                                                                                          org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionNotificationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the instruction 
     *  notification service for the given engine. 
     *
     *  @param  instructionReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> instructionReceiver, 
     *          </code> <code> engineId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionNotificationSession getInstructionNotificationSessionForEngine(org.osid.rules.check.InstructionReceiver instructionReceiver, 
                                                                                                          org.osid.id.Id engineId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionNotificationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup instruction/engine 
     *  checks. 
     *
     *  @return an <code> InstructionEngineSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionEngineSession getInstructionEngineSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup instruction/engine 
     *  checks. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionEngineSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionEngineSession getInstructionEngineSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  instructions to engines. 
     *
     *  @return an <code> InstructionEngineAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionEngineAssignmentSession getInstructionEngineAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionEngineAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning mappings 
     *  to engines. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstructionEngineAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionEngineAssignmentSession getInstructionEngineAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionEngineAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return an <code> InstructionSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSmartEngine() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSmartEngineSession getInstructionSmartEngineSession(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getInstructionSmartEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InstructionSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructionSmartEngine() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionSmartEngineSession getInstructionSmartEngineSession(org.osid.id.Id engineId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getInstructionSmartEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check lookup 
     *  service. 
     *
     *  @return a <code> CheckLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckLookupSession getCheckLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckLookupSession getCheckLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return a <code> CheckLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckLookupSession getCheckLookupSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckLookupSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckLookupSession getCheckLookupSessionForEngine(org.osid.id.Id engineId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckLookupSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check query 
     *  service. 
     *
     *  @return a <code> CheckQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQuerySession getCheckQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQuerySession getCheckQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return a <code> CCheckQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQuerySession getCheckQuerySessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckQuerySessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQuerySession getCheckQuerySessionForEngine(org.osid.id.Id engineId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckQuerySessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check search 
     *  service. 
     *
     *  @return a <code> CheckSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSearchSession getCheckSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSearchSession getCheckSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return a <code> CheckSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSearchSession getCheckSearchSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSearchSession getCheckSearchSessionForEngine(org.osid.id.Id engineId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  administrative service. 
     *
     *  @return a <code> CheckAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckAdminSession getCheckAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckAdminSession getCheckAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  administrative service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return a <code> CheckAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckAdminSession getCheckAdminSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckAdminSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  administrative service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckAdminSession getCheckAdminSessionForEngine(org.osid.id.Id engineId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckAdminSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  notification service. 
     *
     *  @param  checkReceiver the notification callback 
     *  @return a <code> CheckNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> checkReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckNotificationSession getCheckNotificationSession(org.osid.rules.check.CheckReceiver checkReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  notification service. 
     *
     *  @param  checkReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CheckNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> checkReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckNotificationSession getCheckNotificationSession(org.osid.rules.check.CheckReceiver checkReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  notification service for the given engine. 
     *
     *  @param  checkReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return a <code> CheckNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> checkReceiver </code> or 
     *          <code> engineId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckNotificationSession getCheckNotificationSessionForEngine(org.osid.rules.check.CheckReceiver checkReceiver, 
                                                                                              org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckNotificationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the check 
     *  notification service for the given engine. 
     *
     *  @param  checkReceiver the notification callback 
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckNotificationSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> checkReceiver, engineId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckNotificationSession getCheckNotificationSessionForEngine(org.osid.rules.check.CheckReceiver checkReceiver, 
                                                                                              org.osid.id.Id engineId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckNotificationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup check/engine mappings. 
     *
     *  @return a <code> CheckEngineSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckEngineSession getCheckEngineSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup check/engine mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckEngineSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCheckEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckEngineSession getCheckEngineSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to engines. 
     *
     *  @return a <code> CheckEngineAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckEngineAssignmentSession getCheckEngineAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckEngineAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to engines. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CheckEngineAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckEngineAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckEngineAssignmentSession getCheckEngineAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckEngineAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage check smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return a <code> CheckSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckSmartEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSmartEngineSession getCheckSmartEngineSession(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckManager.getCheckSmartEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage check smart engines. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CheckSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no engine found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCheckSmartEngine() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSmartEngineSession getCheckSmartEngineSession(org.osid.id.Id engineId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.check.RulesCheckProxyManager.getCheckSmartEngineSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.agendaRecordTypes.clear();
        this.agendaRecordTypes.clear();

        this.agendaSearchRecordTypes.clear();
        this.agendaSearchRecordTypes.clear();

        this.instructionRecordTypes.clear();
        this.instructionRecordTypes.clear();

        this.instructionSearchRecordTypes.clear();
        this.instructionSearchRecordTypes.clear();

        this.checkRecordTypes.clear();
        this.checkRecordTypes.clear();

        this.checkSearchRecordTypes.clear();
        this.checkSearchRecordTypes.clear();

        return;
    }
}
