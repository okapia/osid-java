//
// AbstractMapEnrollmentLookupSession
//
//    A simple framework for providing an Enrollment lookup service
//    backed by a fixed collection of enrollments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Enrollment lookup service backed by a
 *  fixed collection of enrollments. The enrollments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Enrollments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapEnrollmentLookupSession
    extends net.okapia.osid.jamocha.course.program.spi.AbstractEnrollmentLookupSession
    implements org.osid.course.program.EnrollmentLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.program.Enrollment> enrollments = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.program.Enrollment>());


    /**
     *  Makes an <code>Enrollment</code> available in this session.
     *
     *  @param  enrollment an enrollment
     *  @throws org.osid.NullArgumentException <code>enrollment<code>
     *          is <code>null</code>
     */

    protected void putEnrollment(org.osid.course.program.Enrollment enrollment) {
        this.enrollments.put(enrollment.getId(), enrollment);
        return;
    }


    /**
     *  Makes an array of enrollments available in this session.
     *
     *  @param  enrollments an array of enrollments
     *  @throws org.osid.NullArgumentException <code>enrollments<code>
     *          is <code>null</code>
     */

    protected void putEnrollments(org.osid.course.program.Enrollment[] enrollments) {
        putEnrollments(java.util.Arrays.asList(enrollments));
        return;
    }


    /**
     *  Makes a collection of enrollments available in this session.
     *
     *  @param  enrollments a collection of enrollments
     *  @throws org.osid.NullArgumentException <code>enrollments<code>
     *          is <code>null</code>
     */

    protected void putEnrollments(java.util.Collection<? extends org.osid.course.program.Enrollment> enrollments) {
        for (org.osid.course.program.Enrollment enrollment : enrollments) {
            this.enrollments.put(enrollment.getId(), enrollment);
        }

        return;
    }


    /**
     *  Removes an Enrollment from this session.
     *
     *  @param  enrollmentId the <code>Id</code> of the enrollment
     *  @throws org.osid.NullArgumentException <code>enrollmentId<code> is
     *          <code>null</code>
     */

    protected void removeEnrollment(org.osid.id.Id enrollmentId) {
        this.enrollments.remove(enrollmentId);
        return;
    }


    /**
     *  Gets the <code>Enrollment</code> specified by its <code>Id</code>.
     *
     *  @param  enrollmentId <code>Id</code> of the <code>Enrollment</code>
     *  @return the enrollment
     *  @throws org.osid.NotFoundException <code>enrollmentId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>enrollmentId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Enrollment getEnrollment(org.osid.id.Id enrollmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.program.Enrollment enrollment = this.enrollments.get(enrollmentId);
        if (enrollment == null) {
            throw new org.osid.NotFoundException("enrollment not found: " + enrollmentId);
        }

        return (enrollment);
    }


    /**
     *  Gets all <code>Enrollments</code>. In plenary mode, the returned
     *  list contains all known enrollments or an error
     *  results. Otherwise, the returned list may contain only those
     *  enrollments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Enrollments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.enrollment.ArrayEnrollmentList(this.enrollments.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.enrollments.clear();
        super.close();
        return;
    }
}
