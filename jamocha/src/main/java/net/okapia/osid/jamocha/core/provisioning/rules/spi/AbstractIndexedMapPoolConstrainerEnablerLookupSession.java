//
// AbstractIndexedMapPoolConstrainerEnablerLookupSession.java
//
//    A simple framework for providing a PoolConstrainerEnabler lookup service
//    backed by a fixed collection of pool constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a PoolConstrainerEnabler lookup service backed by a
 *  fixed collection of pool constrainer enablers. The pool constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some pool constrainer enablers may be compatible
 *  with more types than are indicated through these pool constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PoolConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPoolConstrainerEnablerLookupSession
    extends AbstractMapPoolConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolConstrainerEnabler> poolConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.PoolConstrainerEnabler> poolConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.PoolConstrainerEnabler>());


    /**
     *  Makes a <code>PoolConstrainerEnabler</code> available in this session.
     *
     *  @param  poolConstrainerEnabler a pool constrainer enabler
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPoolConstrainerEnabler(org.osid.provisioning.rules.PoolConstrainerEnabler poolConstrainerEnabler) {
        super.putPoolConstrainerEnabler(poolConstrainerEnabler);

        this.poolConstrainerEnablersByGenus.put(poolConstrainerEnabler.getGenusType(), poolConstrainerEnabler);
        
        try (org.osid.type.TypeList types = poolConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolConstrainerEnablersByRecord.put(types.getNextType(), poolConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a pool constrainer enabler from this session.
     *
     *  @param poolConstrainerEnablerId the <code>Id</code> of the pool constrainer enabler
     *  @throws org.osid.NullArgumentException <code>poolConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePoolConstrainerEnabler(org.osid.id.Id poolConstrainerEnablerId) {
        org.osid.provisioning.rules.PoolConstrainerEnabler poolConstrainerEnabler;
        try {
            poolConstrainerEnabler = getPoolConstrainerEnabler(poolConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.poolConstrainerEnablersByGenus.remove(poolConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = poolConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.poolConstrainerEnablersByRecord.remove(types.getNextType(), poolConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePoolConstrainerEnabler(poolConstrainerEnablerId);
        return;
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> corresponding to the given
     *  pool constrainer enabler genus <code>Type</code> which does not include
     *  pool constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known pool constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those pool constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  poolConstrainerEnablerGenusType a pool constrainer enabler genus type 
     *  @return the returned <code>PoolConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByGenusType(org.osid.type.Type poolConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolconstrainerenabler.ArrayPoolConstrainerEnablerList(this.poolConstrainerEnablersByGenus.get(poolConstrainerEnablerGenusType)));
    }


    /**
     *  Gets a <code>PoolConstrainerEnablerList</code> containing the given
     *  pool constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known pool constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  pool constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  poolConstrainerEnablerRecordType a pool constrainer enabler record type 
     *  @return the returned <code>poolConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerList getPoolConstrainerEnablersByRecordType(org.osid.type.Type poolConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.poolconstrainerenabler.ArrayPoolConstrainerEnablerList(this.poolConstrainerEnablersByRecord.get(poolConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.poolConstrainerEnablersByGenus.clear();
        this.poolConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
