//
// AbstractCommunique.java
//
//     Defines a Communique builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.communication.communique.spi;


/**
 *  Defines a <code>Communique</code> builder.
 */

public abstract class AbstractCommuniqueBuilder<T extends AbstractCommuniqueBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.communication.communique.CommuniqueMiter communique;


    /**
     *  Constructs a new <code>AbstractCommuniqueBuilder</code>.
     *
     *  @param communique the communique to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCommuniqueBuilder(net.okapia.osid.jamocha.builder.communication.communique.CommuniqueMiter communique) {
        super(communique);
        this.communique = communique;
        return;
    }


    /**
     *  Builds the communique.
     *
     *  @return the new communique
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.communication.Communique build() {
        (new net.okapia.osid.jamocha.builder.validator.communication.communique.CommuniqueValidator(getValidations())).validate(this.communique);
        return (new net.okapia.osid.jamocha.builder.communication.communique.ImmutableCommunique(this.communique));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the communique miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.communication.communique.CommuniqueMiter getMiter() {
        return (this.communique);
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public T message(org.osid.locale.DisplayText message) {
        getMiter().setMessage(message);
        return (self());
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    public T level(org.osid.communication.CommuniqueLevel level) {
        getMiter().setLevel(level);
        return (self());
    }


    /**
     *  Sets the response required flag.
     *
     *  @param responseRequired {@code true} if a response is
     *         required, {@code false} otherwise
     */

    public T responseRequired(boolean responseRequired) {
        getMiter().setResponseRequired(responseRequired);
        return (self());
    }


    /**
     *  Adds a response option.
     *
     *  @param option a response option
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public T responseOption(org.osid.communication.ResponseOption option) {
        getMiter().addResponseOption(option);
        return (self());
    }


    /**
     *  Sets all the response options.
     *
     *  @param options a collection of response options
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public T responseOptions(java.util.Collection<org.osid.communication.ResponseOption> options) {
        getMiter().setResponseOptions(options);
        return (self());
    }


    /**
     *  Sets the respond via form flag.
     *
     *  @param respondViaForm {@code true} if the response should use
     *         a form, @code false} otherwise
     */

    public T respondViaForm(boolean respondViaForm) {
        getMiter().setRespondViaForm(respondViaForm);
        return (self());
    }
}       


