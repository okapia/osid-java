//
// AbstractAssemblySequenceRuleQuery.java
//
//     A SequenceRuleQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.authoring.sequencerule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SequenceRuleQuery that stores terms.
 */

public abstract class AbstractAssemblySequenceRuleQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.assessment.authoring.SequenceRuleQuery,
               org.osid.assessment.authoring.SequenceRuleQueryInspector,
               org.osid.assessment.authoring.SequenceRuleSearchOrder {

    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySequenceRuleQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySequenceRuleQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                      boolean match) {
        getAssembler().addIdTerm(getAssessmentPartIdColumn(), assessmentPartId, match);
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentPartIdTerms() {
        getAssembler().clearTerms(getAssessmentPartIdColumn());
        return;
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentPartIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentPartIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  part. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessmentPart(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAssessmentPartColumn(), style);
        return;
    }


    /**
     *  Gets the AssessmentPartId column name.
     *
     * @return the column name
     */

    protected String getAssessmentPartIdColumn() {
        return ("assessment_part_id");
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentPartQuery() is false");
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearAssessmentPartTerms() {
        getAssembler().clearTerms(getAssessmentPartColumn());
        return;
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Tests if an assessment part search order is available. 
     *
     *  @return <code> true </code> if an assessment part search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment part search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchOrder getAssessmentPartSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentPartSearchOrder() is false");
    }


    /**
     *  Gets the AssessmentPart column name.
     *
     * @return the column name
     */

    protected String getAssessmentPartColumn() {
        return ("assessment_part");
    }


    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchNextAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                          boolean match) {
        getAssembler().addIdTerm(getNextAssessmentPartIdColumn(), assessmentPartId, match);
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearNextAssessmentPartIdTerms() {
        getAssembler().clearTerms(getNextAssessmentPartIdColumn());
        return;
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getNextAssessmentPartIdTerms() {
        return (getAssembler().getIdTerms(getNextAssessmentPartIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  part. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNextAssessmentPart(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getNextAssessmentPartColumn(), style);
        return;
    }


    /**
     *  Gets the NextAssessmentPartId column name.
     *
     * @return the column name
     */

    protected String getNextAssessmentPartIdColumn() {
        return ("next_assessment_part_id");
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNextAssessmentPartQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getNextAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsNextAssessmentPartQuery() is false");
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearNextAssessmentPartTerms() {
        getAssembler().clearTerms(getNextAssessmentPartColumn());
        return;
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getNextAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Tests if an assessment part search order is available. 
     *
     *  @return <code> true </code> if an assessment part search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextAssessmentPartSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment part search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNextAssessmentPartSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchOrder getNextAssessmentPartSearchOrder() {
        throw new org.osid.UnimplementedException("supportsNextAssessmentPartSearchOrder() is false");
    }


    /**
     *  Gets the NextAssessmentPart column name.
     *
     * @return the column name
     */

    protected String getNextAssessmentPartColumn() {
        return ("next_assessment_part");
    }


    /**
     *  Matches minimum scores that fall in between the given scores 
     *  inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchMinimumScore(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getMinimumScoreColumn(), low, high, match);
        return;
    }


    /**
     *  Matches assessment parts with any minimum score assigned. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          minimum score, <code> false </code> to match assessment parts 
     *          with no minimum score 
     */

    @OSID @Override
    public void matchAnyMinimumScore(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMinimumScoreColumn(), match);
        return;
    }


    /**
     *  Clears all minimum score terms. 
     */

    @OSID @Override
    public void clearMinimumScoreTerms() {
        getAssembler().clearTerms(getMinimumScoreColumn());
        return;
    }


    /**
     *  Gets the minimum score query terms. 
     *
     *  @return the minimum score terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumScoreTerms() {
        return (getAssembler().getCardinalRangeTerms(getMinimumScoreColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the minimum 
     *  score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumScore(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMinimumScoreColumn(), style);
        return;
    }


    /**
     *  Gets the MinimumScore column name.
     *
     * @return the column name
     */

    protected String getMinimumScoreColumn() {
        return ("minimum_score");
    }


    /**
     *  Matches maximum scores that fall in between the given scores 
     *  inclusive. 
     *
     *  @param  low low end of range 
     *  @param  high high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchMaximumScore(long low, long high, boolean match) {
        getAssembler().addCardinalRangeTerm(getMaximumScoreColumn(), low, high, match);
        return;
    }


    /**
     *  Matches assessment parts with any maximum score assigned. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          maximum score, <code> false </code> to match assessment parts 
     *          with no maximum score 
     */

    @OSID @Override
    public void matchAnyMaximumScore(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getMaximumScoreColumn(), match);
        return;
    }


    /**
     *  Clears all maximum score terms. 
     */

    @OSID @Override
    public void clearMaximumScoreTerms() {
        getAssembler().clearTerms(getMaximumScoreColumn());
        return;
    }


    /**
     *  Gets the maximum score query terms. 
     *
     *  @return the maximum score terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMaximumScoreTerms() {
        return (getAssembler().getCardinalRangeTerms(getMaximumScoreColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the maximum 
     *  score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumScore(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMaximumScoreColumn(), style);
        return;
    }


    /**
     *  Gets the MaximumScore column name.
     *
     * @return the column name
     */

    protected String getMaximumScoreColumn() {
        return ("maximum_score");
    }


    /**
     *  Matches cumulative rules. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchCumulative(boolean match) {
        getAssembler().addBooleanTerm(getCumulativeColumn(), match);
        return;
    }


    /**
     *  Clears all cumulative terms. 
     */

    @OSID @Override
    public void clearCumulativeTerms() {
        getAssembler().clearTerms(getCumulativeColumn());
        return;
    }


    /**
     *  Gets the minimum score query terms. 
     *
     *  @return the cumulative terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCumulativeTerms() {
        return (getAssembler().getBooleanTerms(getCumulativeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the cumulative. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCumulative(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCumulativeColumn(), style);
        return;
    }


    /**
     *  Gets the Cumulative column name.
     *
     * @return the column name
     */

    protected String getCumulativeColumn() {
        return ("cumulative");
    }


    /**
     *  Sets the assessment part <code> Id </code> for this query. 
     *
     *  @param  assessmentPartId an assessment part <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentPartId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAppliedAssessmentPartId(org.osid.id.Id assessmentPartId, 
                                             boolean match) {
        getAssembler().addIdTerm(getAppliedAssessmentPartIdColumn(), assessmentPartId, match);
        return;
    }


    /**
     *  Clears all assessment part <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAppliedAssessmentPartIdTerms() {
        getAssembler().clearTerms(getAppliedAssessmentPartIdColumn());
        return;
    }


    /**
     *  Gets the assessment part <code> Id </code> query terms. 
     *
     *  @return the assessment parent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAppliedAssessmentPartIdTerms() {
        return (getAssembler().getIdTerms(getAppliedAssessmentPartIdColumn()));
    }


    /**
     *  Gets the AppliedAssessmentPartId column name.
     *
     * @return the column name
     */

    protected String getAppliedAssessmentPartIdColumn() {
        return ("applied_assessment_part_id");
    }


    /**
     *  Tests if an <code> AssessmentPartQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment part query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppliedAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment part. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the assessment part query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppliedAssessmentPartQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuery getAppliedAssessmentPartQuery() {
        throw new org.osid.UnimplementedException("supportsAppliedAssessmentPartQuery() is false");
    }


    /**
     *  Matches assessment parts with any applied assessment part. 
     *
     *  @param  match <code> true </code> to match assessment parts with any 
     *          applied assessment part, <code> false </code> to match 
     *          assessment parts with no applied assessment parts 
     */

    @OSID @Override
    public void matchAnyAppliedAssessmentPart(boolean match) {
        getAssembler().addIdWildcardTerm(getAppliedAssessmentPartColumn(), match);
        return;
    }


    /**
     *  Clears all assessment part terms. 
     */

    @OSID @Override
    public void clearAppliedAssessmentPartTerms() {
        getAssembler().clearTerms(getAppliedAssessmentPartColumn());
        return;
    }


    /**
     *  Gets the assessment part query terms. 
     *
     *  @return the assessment part terms 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQueryInspector[] getAppliedAssessmentPartTerms() {
        return (new org.osid.assessment.authoring.AssessmentPartQueryInspector[0]);
    }


    /**
     *  Gets the AppliedAssessmentPart column name.
     *
     * @return the column name
     */

    protected String getAppliedAssessmentPartColumn() {
        return ("applied_assessment_part");
    }


    /**
     *  Matches constrainers mapped to the bank. 
     *
     *  @param  bankId the bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears the bank <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBankIdTerms() {
        getAssembler().clearTerms(getBankIdColumn());
        return;
    }


    /**
     *  Gets the bank <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBankIdTerms() {
        return (getAssembler().getIdTerms(getBankIdColumn()));
    }


    /**
     *  Gets the BankId column name.
     *
     * @return the column name
     */

    protected String getBankIdColumn() {
        return ("bank_id");
    }


    /**
     *  Tests if an <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bank. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getBankQuery() {
        throw new org.osid.UnimplementedException("supportsBankQuery() is false");
    }


    /**
     *  Clears the bank query terms. 
     */

    @OSID @Override
    public void clearBankTerms() {
        getAssembler().clearTerms(getBankColumn());
        return;
    }


    /**
     *  Gets the bank query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the Bank column name.
     *
     * @return the column name
     */

    protected String getBankColumn() {
        return ("bank");
    }


    /**
     *  Tests if this sequenceRule supports the given record
     *  <code>Type</code>.
     *
     *  @param  sequenceRuleRecordType a sequence rule record type 
     *  @return <code>true</code> if the sequenceRuleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type sequenceRuleRecordType) {
        for (org.osid.assessment.authoring.records.SequenceRuleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  sequenceRuleRecordType the sequence rule record type 
     *  @return the sequence rule query record 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleQueryRecord getSequenceRuleQueryRecord(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  sequenceRuleRecordType the sequence rule record type 
     *  @return the sequence rule query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleQueryInspectorRecord getSequenceRuleQueryInspectorRecord(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param sequenceRuleRecordType the sequence rule record type
     *  @return the sequence rule search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sequenceRuleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord getSequenceRuleSearchOrderRecord(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(sequenceRuleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sequenceRuleRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this sequence rule. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param sequenceRuleQueryRecord the sequence rule query record
     *  @param sequenceRuleQueryInspectorRecord the sequence rule query inspector
     *         record
     *  @param sequenceRuleSearchOrderRecord the sequence rule search order record
     *  @param sequenceRuleRecordType sequence rule record type
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleQueryRecord</code>,
     *          <code>sequenceRuleQueryInspectorRecord</code>,
     *          <code>sequenceRuleSearchOrderRecord</code> or
     *          <code>sequenceRuleRecordTypesequenceRule</code> is
     *          <code>null</code>
     */
            
    protected void addSequenceRuleRecords(org.osid.assessment.authoring.records.SequenceRuleQueryRecord sequenceRuleQueryRecord, 
                                      org.osid.assessment.authoring.records.SequenceRuleQueryInspectorRecord sequenceRuleQueryInspectorRecord, 
                                      org.osid.assessment.authoring.records.SequenceRuleSearchOrderRecord sequenceRuleSearchOrderRecord, 
                                      org.osid.type.Type sequenceRuleRecordType) {

        addRecordType(sequenceRuleRecordType);

        nullarg(sequenceRuleQueryRecord, "sequence rule query record");
        nullarg(sequenceRuleQueryInspectorRecord, "sequence rule query inspector record");
        nullarg(sequenceRuleSearchOrderRecord, "sequence rule search odrer record");

        this.queryRecords.add(sequenceRuleQueryRecord);
        this.queryInspectorRecords.add(sequenceRuleQueryInspectorRecord);
        this.searchOrderRecords.add(sequenceRuleSearchOrderRecord);
        
        return;
    }
}
