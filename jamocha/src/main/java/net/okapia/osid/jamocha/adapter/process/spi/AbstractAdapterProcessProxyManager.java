//
// AbstractProcessProxyManager.java
//
//     An adapter for a ProcessProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ProcessProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterProcessProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.process.ProcessProxyManager>
    implements org.osid.process.ProcessProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterProcessProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterProcessProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterProcessProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterProcessProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if retrieving mappings of state and <code> Ids </code> is 
     *  supported. 
     *
     *  @return <code> true </code> if state mapping retrieval is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsState() {
        return (getAdapteeManager().supportsState());
    }


    /**
     *  Tests if managing mappings of states and <code> Ids </code> is 
     *  supported. 
     *
     *  @return <code> true </code> if state assignment is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateAssignment() {
        return (getAdapteeManager().supportsStateAssignment());
    }


    /**
     *  Tests if subscirbing to state change notifications is supported. 
     *
     *  @return <code> true </code> if state change notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateChangeNotification() {
        return (getAdapteeManager().supportsStateChangeNotification());
    }


    /**
     *  Tests if state lookup is supported. 
     *
     *  @return <code> true </code> if state lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateLookup() {
        return (getAdapteeManager().supportsStateLookup());
    }


    /**
     *  Tests if state query is supported. 
     *
     *  @return <code> true </code> if state query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateQuery() {
        return (getAdapteeManager().supportsStateQuery());
    }


    /**
     *  Tests if state search is supported. 
     *
     *  @return <code> true </code> if state search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateSearch() {
        return (getAdapteeManager().supportsStateSearch());
    }


    /**
     *  Tests if state administration is supported. 
     *
     *  @return <code> true </code> if state administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateAdmin() {
        return (getAdapteeManager().supportsStateAdmin());
    }


    /**
     *  Tests if state sequencing is supported. 
     *
     *  @return <code> true </code> if state sequencing is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateSequencing() {
        return (getAdapteeManager().supportsStateSequencing());
    }


    /**
     *  Tests if state notification is supported. Messages may be sent when 
     *  states are created, modified, or deleted. 
     *
     *  @return <code> true </code> if state notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateNotification() {
        return (getAdapteeManager().supportsStateNotification());
    }


    /**
     *  Tests if process lookup is supported. 
     *
     *  @return <code> true </code> if process lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessLookup() {
        return (getAdapteeManager().supportsProcessLookup());
    }


    /**
     *  Tests if process query is supported. 
     *
     *  @return <code> true </code> if process query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessQuery() {
        return (getAdapteeManager().supportsProcessQuery());
    }


    /**
     *  Tests if process search is supported. 
     *
     *  @return <code> true </code> if process search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessSearch() {
        return (getAdapteeManager().supportsProcessSearch());
    }


    /**
     *  Tests if process administration is supported. 
     *
     *  @return <code> true </code> if process administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessAdmin() {
        return (getAdapteeManager().supportsProcessAdmin());
    }


    /**
     *  Tests if process notification is supported. Messages may be sent when 
     *  <code> Process </code> objects are created, deleted or updated. 
     *  Notifications for states within processes are sent via the state 
     *  notification session. 
     *
     *  @return <code> true </code> if process notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessNotification() {
        return (getAdapteeManager().supportsProcessNotification());
    }


    /**
     *  Tests if a process hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a process hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessHierarchy() {
        return (getAdapteeManager().supportsProcessHierarchy());
    }


    /**
     *  Tests if a process hierarchy design is supported. 
     *
     *  @return <code> true </code> if a process hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessHierarchyDesign() {
        return (getAdapteeManager().supportsProcessHierarchyDesign());
    }


    /**
     *  Gets all the state record types supported. 
     *
     *  @return the list of supported state record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStateRecordTypes() {
        return (getAdapteeManager().getStateRecordTypes());
    }


    /**
     *  Tests if a given state record type is supported. 
     *
     *  @param  stateRecordType the state type 
     *  @return <code> true </code> if the state record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stateRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStateRecordType(org.osid.type.Type stateRecordType) {
        return (getAdapteeManager().supportsStateRecordType(stateRecordType));
    }


    /**
     *  Gets all the state search record types supported. 
     *
     *  @return the list of supported state search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStateSearchRecordTypes() {
        return (getAdapteeManager().getStateSearchRecordTypes());
    }


    /**
     *  Tests if a given state search type is supported. 
     *
     *  @param  stateSearchRecordType the state search type 
     *  @return <code> true </code> if the state search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stateRecordSearchType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStateSearchRecordType(org.osid.type.Type stateSearchRecordType) {
        return (getAdapteeManager().supportsStateSearchRecordType(stateSearchRecordType));
    }


    /**
     *  Gets all the process record types supported. 
     *
     *  @return the list of supported process record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessRecordTypes() {
        return (getAdapteeManager().getProcessRecordTypes());
    }


    /**
     *  Tests if a given process record type is supported. 
     *
     *  @param  processRecordType the process record type 
     *  @return <code> true </code> if the process record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessRecordType(org.osid.type.Type processRecordType) {
        return (getAdapteeManager().supportsProcessRecordType(processRecordType));
    }


    /**
     *  Gets all the process search record types supported. 
     *
     *  @return the list of supported process search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessSearchRecordTypes() {
        return (getAdapteeManager().getProcessSearchRecordTypes());
    }


    /**
     *  Tests if a given process search record type is supported. 
     *
     *  @param  processSearchRecordType the process search record type 
     *  @return <code> true </code> if the process search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessSearchRecordType(org.osid.type.Type processSearchRecordType) {
        return (getAdapteeManager().supportsProcessSearchRecordType(processSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsState() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSession getStateSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state service 
     *  for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateStateSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsState() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSession getStateSessionForProcess(org.osid.id.Id processId, 
                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateSessionForProcess(processId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateAssignmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAssignmentSession getStateAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state 
     *  assignment service for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateAssignmentStateSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAssignmentSession getStateAssignmentSessionForProcess(org.osid.id.Id processId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateAssignmentSessionForProcess(processId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to state 
     *  changes. 
     *
     *  @param  stateChangeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a StateChangeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stateChangeReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateChangeNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateChangeNotificationSession(org.osid.process.StateChangeReceiver stateChangeReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateChangeNotificationSession(stateChangeReceiver, proxy));
    }


    /**
     *  Gets the state change notification session for the given process. 
     *
     *  @param  stateChangeReceiver the notification callback 
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateChangeNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> stateChangeReceiver, 
     *          processId </code> , or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateChangeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateChangeNotificationSessionForProcess(org.osid.process.StateChangeReceiver stateChangeReceiver, 
                                                                                                 org.osid.id.Id processId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateChangeNotificationSessionForProcess(stateChangeReceiver, processId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateLookupSession getStateLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the state lookup 
     *  service for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateLookupSession getStateLookupSessionForProcess(org.osid.id.Id processId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateLookupSessionForProcess(processId, proxy));
    }


    /**
     *  Gets a state query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuerySession getStateQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateQuerySession(proxy));
    }


    /**
     *  Gets a state query session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuerySession getStateQuerySessionForProcess(org.osid.id.Id processId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateQuerySessionForProcess(processId, proxy));
    }


    /**
     *  Gets a state search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchSession getStateSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateSearchSession(proxy));
    }


    /**
     *  Gets a state search session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsStateSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchSession getStateSearchSessionForProcess(org.osid.id.Id processId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateSearchSessionForProcess(processId, proxy));
    }


    /**
     *  Gets a state administration session for creating, updating and 
     *  deleting states. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAdminSession getStateAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateAdminSession(proxy));
    }


    /**
     *  Gets a state administration session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsStateAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateAdminSession getStateAdminSessionForProcess(org.osid.id.Id processId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateAdminSessionForProcess(processId, proxy));
    }


    /**
     *  Gets a state sequencing session for ordering states. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a StateSequencingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateSequencing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSequencingSession getStateSequencingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateSequencingSession(proxy));
    }


    /**
     *  Gets a state sequencing session for the given process. 
     *
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateSequencingSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateSequencing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateSequencingSession getStateSequencingSessionForProcess(org.osid.id.Id processId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateSequencingSessionForProcess(processId, proxy));
    }


    /**
     *  Gets the state notification session for the given process. 
     *
     *  @param  stateReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a StateNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stateReceiver </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateNotificationSession(org.osid.process.StateReceiver stateReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStateNotificationSession(stateReceiver, proxy));
    }


    /**
     *  Gets the state notification session for the given process. 
     *
     *  @param  stateReceiver notification callback 
     *  @param  processId the <code> Id </code> of the process 
     *  @param  proxy a proxy 
     *  @return <code> a StateNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> processId </code> not found 
     *  @throws org.osid.NullArgumentException <code> stateReceiver, processId 
     *          </code> or <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStateNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateNotificationSession getStateNotificationSessionForProcess(org.osid.process.StateReceiver stateReceiver, 
                                                                                           org.osid.id.Id processId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStateNotificationSessionForProcess(stateReceiver, processId, proxy));
    }


    /**
     *  Gets the process lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessLookupSession getProcessLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessLookupSession(proxy));
    }


    /**
     *  Gets the process query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessQuerySession getProcessQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessQuerySession(proxy));
    }


    /**
     *  Gets the process search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessSearchSession getProcessSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessSearchSession(proxy));
    }


    /**
     *  Gets the process administrative session for creating, updating and 
     *  deleteing processes. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProcessAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessAdminSession getProcessAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessAdminSession(proxy));
    }


    /**
     *  Gets the notification session for subscriprocessg to changes to a 
     *  process. 
     *
     *  @param  processReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessNotificationSession getProcessNotificationSession(org.osid.process.ProcessReceiver processReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessNotificationSession(processReceiver, proxy));
    }


    /**
     *  Gets the process hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a ProcessHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessHierarchySession getProcessHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessHierarchySession(proxy));
    }


    /**
     *  Gets the process hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessHierarchyDesignSession getProcessHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessHierarchyDesignSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
