//
// AbstractOrchestrationManager.java
//
//     An adapter for a OrchestrationManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.orchestration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OrchestrationManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOrchestrationManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.orchestration.OrchestrationManager>
    implements org.osid.orchestration.OrchestrationManager {


    /**
     *  Constructs a new {@code AbstractAdapterOrchestrationManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOrchestrationManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOrchestrationManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOrchestrationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if an acknowledgement provider is supported. 
     *
     *  @return <code> true </code> if an acknowledgement provider is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgementProvider() {
        return (getAdapteeManager().supportsAcknowledgementProvider());
    }


    /**
     *  Tests if an assessment provider is supported. 
     *
     *  @return <code> true </code> if an assessment provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentProvider() {
        return (getAdapteeManager().supportsAssessmentProvider());
    }


    /**
     *  Tests if an authentication provider is supported. 
     *
     *  @return <code> true </code> if an authentication provider is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationProvider() {
        return (getAdapteeManager().supportsAuthenticationProvider());
    }


    /**
     *  Tests if an authorization provider is supported. 
     *
     *  @return <code> true </code> if an authorization provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationProvider() {
        return (getAdapteeManager().supportsAuthorizationProvider());
    }


    /**
     *  Tests if a bidding provider is supported. 
     *
     *  @return <code> true </code> if a bidding provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingProvider() {
        return (getAdapteeManager().supportsBiddingProvider());
    }


    /**
     *  Tests if a billing provider is supported. 
     *
     *  @return <code> true </code> if a billing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingProvider() {
        return (getAdapteeManager().supportsBillingProvider());
    }


    /**
     *  Tests if a blogging provider is supported. 
     *
     *  @return <code> true </code> if a blogging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBloggingProvider() {
        return (getAdapteeManager().supportsBloggingProvider());
    }


    /**
     *  Tests if a calendaring provider is supported. 
     *
     *  @return <code> true </code> if a calendaring provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringProvider() {
        return (getAdapteeManager().supportsCalendaringProvider());
    }


    /**
     *  Tests if a cataloging provider is supported. 
     *
     *  @return <code> true </code> if a cataloging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogingProvider() {
        return (getAdapteeManager().supportsCatalogingProvider());
    }


    /**
     *  Tests if a checklist provider is supported. 
     *
     *  @return <code> true </code> if a checklist provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistProvider() {
        return (getAdapteeManager().supportsChecklistProvider());
    }


    /**
     *  Tests if a commenting provider is supported. 
     *
     *  @return <code> true </code> if a commenting provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingProvider() {
        return (getAdapteeManager().supportsCommentingProvider());
    }


    /**
     *  Tests if a communication provider is supported. 
     *
     *  @return <code> true </code> if a communication provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommunicationProvider() {
        return (getAdapteeManager().supportsCommunicationProvider());
    }


    /**
     *  Tests if a configuration provider is supported. 
     *
     *  @return <code> true </code> if a configuration provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationProvider() {
        return (getAdapteeManager().supportsConfigurationProvider());
    }


    /**
     *  Tests if a contact provider is supported. 
     *
     *  @return <code> true </code> if a contact provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactProvider() {
        return (getAdapteeManager().supportsContactProvider());
    }


    /**
     *  Tests if a control provider is supported. 
     *
     *  @return <code> true </code> if a control provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControlProvider() {
        return (getAdapteeManager().supportsControlProvider());
    }


    /**
     *  Tests if a course provider is supported. 
     *
     *  @return <code> true </code> if a course provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProvider() {
        return (getAdapteeManager().supportsCourseProvider());
    }


    /**
     *  Tests if a dictionary provider is supported. 
     *
     *  @return <code> true </code> if a dictionary provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryProvider() {
        return (getAdapteeManager().supportsDictionaryProvider());
    }


    /**
     *  Tests if a filing provider is supported. 
     *
     *  @return <code> true </code> if a filing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingProvider() {
        return (getAdapteeManager().supportsFilingProvider());
    }


    /**
     *  Tests if a financials provider is supported. 
     *
     *  @return <code> true </code> if a financials provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsProvider() {
        return (getAdapteeManager().supportsFinancialsProvider());
    }


    /**
     *  Tests if a forum provider is supported. 
     *
     *  @return <code> true </code> if a forum provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumProvider() {
        return (getAdapteeManager().supportsForumProvider());
    }


    /**
     *  Tests if a grading provider is supported. 
     *
     *  @return <code> true </code> if a grading provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingProvider() {
        return (getAdapteeManager().supportsGradingProvider());
    }


    /**
     *  Tests if a hierarchy provider is supported. 
     *
     *  @return <code> true </code> if a hierarchy provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyProvider() {
        return (getAdapteeManager().supportsHierarchyProvider());
    }


    /**
     *  Tests if a hold provider is supported. 
     *
     *  @return <code> true </code> if hold provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldProvider() {
        return (getAdapteeManager().supportsHoldProvider());
    }


    /**
     *  Tests if an id provider is supported. 
     *
     *  @return <code> true </code> if an id provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdProvider() {
        return (getAdapteeManager().supportsIdProvider());
    }


    /**
     *  Tests if an inquiry provider is supported. 
     *
     *  @return <code> true </code> if an inquiry provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryProvider() {
        return (getAdapteeManager().supportsInquiryProvider());
    }


    /**
     *  Tests if an installation provider is supported. 
     *
     *  @return <code> true </code> if an installation provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationProvider() {
        return (getAdapteeManager().supportsInstallationProvider());
    }


    /**
     *  Tests if an inventory provider is supported. 
     *
     *  @return <code> true </code> if an inventory provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryProvider() {
        return (getAdapteeManager().supportsInventoryProvider());
    }


    /**
     *  Tests if a journaling provider is supported. 
     *
     *  @return <code> true </code> if a journaling provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalingProvider() {
        return (getAdapteeManager().supportsJournalingProvider());
    }


    /**
     *  Tests if a learning provider is supported. 
     *
     *  @return <code> true </code> if a learning provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningProvider() {
        return (getAdapteeManager().supportsLearningProvider());
    }


    /**
     *  Tests if a locale provider is supported. 
     *
     *  @return <code> true </code> if a locale provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocaleProvider() {
        return (getAdapteeManager().supportsLocaleProvider());
    }


    /**
     *  Tests if a logging provider is supported. 
     *
     *  @return <code> true </code> if a logging provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLoggingProvider() {
        return (getAdapteeManager().supportsLoggingProvider());
    }


    /**
     *  Tests if a mapping provider is supported. 
     *
     *  @return <code> true </code> if a mapping provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingProvider() {
        return (getAdapteeManager().supportsMappingProvider());
    }


    /**
     *  Tests if a messaging provider is supported. 
     *
     *  @return <code> true </code> if a messaging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessagingProvider() {
        return (getAdapteeManager().supportsMessagingProvider());
    }


    /**
     *  Tests if a metering provider is supported. 
     *
     *  @return <code> true </code> if a metering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeteringProvider() {
        return (getAdapteeManager().supportsMeteringProvider());
    }


    /**
     *  Tests if an offering provider is supported. 
     *
     *  @return <code> true </code> if an offering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingProvider() {
        return (getAdapteeManager().supportsOfferingProvider());
    }


    /**
     *  Tests if an ontology provider is supported. 
     *
     *  @return <code> true </code> if an ontology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyProvider() {
        return (getAdapteeManager().supportsOntologyProvider());
    }


    /**
     *  Tests if an ordering provider is supported. 
     *
     *  @return <code> true </code> if an ordering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderingProvider() {
        return (getAdapteeManager().supportsOrderingProvider());
    }


    /**
     *  Tests if a personnel provider is supported. 
     *
     *  @return <code> true </code> if a personnel provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonnelProvider() {
        return (getAdapteeManager().supportsPersonnelProvider());
    }


    /**
     *  Tests if a process provider is supported. 
     *
     *  @return <code> true </code> if a process provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessProvider() {
        return (getAdapteeManager().supportsProcessProvider());
    }


    /**
     *  Tests if a profile provider is supported. 
     *
     *  @return <code> true </code> if a profile provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileProvider() {
        return (getAdapteeManager().supportsProfileProvider());
    }


    /**
     *  Tests if a provisioning provider is supported. 
     *
     *  @return <code> true </code> if a provisioning provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisioningProvider() {
        return (getAdapteeManager().supportsProvisioningProvider());
    }


    /**
     *  Tests if a proxy provider is supported. 
     *
     *  @return <code> true </code> if a proxy provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProxyProvider() {
        return (getAdapteeManager().supportsProxyProvider());
    }


    /**
     *  Tests if a recipe provider is supported. 
     *
     *  @return <code> true </code> if a recipe provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeProvider() {
        return (getAdapteeManager().supportsRecipeProvider());
    }


    /**
     *  Tests if a recognition provider is supported. 
     *
     *  @return <code> true </code> if a recognition provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecognitionProvider() {
        return (getAdapteeManager().supportsRecognitionProvider());
    }


    /**
     *  Tests if a relationship provider is supported. 
     *
     *  @return <code> true </code> if a relationship provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipProvider() {
        return (getAdapteeManager().supportsRelationshipProvider());
    }


    /**
     *  Tests if a repository provider is supported. 
     *
     *  @return <code> true </code> if a repository provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryProvider() {
        return (getAdapteeManager().supportsRepositoryProvider());
    }


    /**
     *  Tests if a resource provider is supported. 
     *
     *  @return <code> true </code> if a resource provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceProvider() {
        return (getAdapteeManager().supportsResourceProvider());
    }


    /**
     *  Tests if a resourcing provider is supported. 
     *
     *  @return <code> true </code> if a resourcing provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcingProvider() {
        return (getAdapteeManager().supportsResourcingProvider());
    }


    /**
     *  Tests if a rules provider is supported. 
     *
     *  @return <code> true </code> if a rules provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRulesProvider() {
        return (getAdapteeManager().supportsRulesProvider());
    }


    /**
     *  Tests if a search provider is supported. 
     *
     *  @return <code> true </code> if a search provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSearchProvider() {
        return (getAdapteeManager().supportsSearchProvider());
    }


    /**
     *  Tests if a sequencing provider is supported. 
     *
     *  @return <code> true </code> if a sequencing provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequencingProvider() {
        return (getAdapteeManager().supportsSequencingProvider());
    }


    /**
     *  Tests if a subscription provider is supported. 
     *
     *  @return <code> true </code> if a subscription provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionProvider() {
        return (getAdapteeManager().supportsSubscriptionProvider());
    }


    /**
     *  Tests if a topology provider is supported. 
     *
     *  @return <code> true </code> if a topology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyProvider() {
        return (getAdapteeManager().supportsTopologyProvider());
    }


    /**
     *  Tests if a tracking provider is supported. 
     *
     *  @return <code> true </code> if a tracking provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTrackingProvider() {
        return (getAdapteeManager().supportsTrackingProvider());
    }


    /**
     *  Tests if a transaction provider is supported. 
     *
     *  @return <code> true </code> if a transaction provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactionProvider() {
        return (getAdapteeManager().supportsTransactionProvider());
    }


    /**
     *  Tests if a transport provider is supported. 
     *
     *  @return <code> true </code> if a transport provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransportProvider() {
        return (getAdapteeManager().supportsTransportProvider());
    }


    /**
     *  Tests if a type provider is supported. 
     *
     *  @return <code> true </code> if a type provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTypeProvider() {
        return (getAdapteeManager().supportsTypeProvider());
    }


    /**
     *  Tests if a voting provider is supported. 
     *
     *  @return <code> true </code> if a voting provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingProvider() {
        return (getAdapteeManager().supportsVotingProvider());
    }


    /**
     *  Tests if a workflow provider is supported. 
     *
     *  @return <code> true </code> if a workflow provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowProvider() {
        return (getAdapteeManager().supportsWorkflowProvider());
    }


    /**
     *  Tests if an acknowledgement provider is supported. 
     *
     *  @return <code> true </code> if an acknowledgement provider is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgementProxyProvider() {
        return (getAdapteeManager().supportsAcknowledgementProxyProvider());
    }


    /**
     *  Tests if an assessment provider is supported. 
     *
     *  @return <code> true </code> if an assessment provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentProxyProvider() {
        return (getAdapteeManager().supportsAssessmentProxyProvider());
    }


    /**
     *  Tests if an authentication provider is supported. 
     *
     *  @return <code> true </code> if an authentication provider is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationProxyProvider() {
        return (getAdapteeManager().supportsAuthenticationProxyProvider());
    }


    /**
     *  Tests if an authorization provider is supported. 
     *
     *  @return <code> true </code> if an authorization provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationProxyProvider() {
        return (getAdapteeManager().supportsAuthorizationProxyProvider());
    }


    /**
     *  Tests if a bidding provider is supported. 
     *
     *  @return <code> true </code> if a bidding provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingProxyProvider() {
        return (getAdapteeManager().supportsBiddingProxyProvider());
    }


    /**
     *  Tests if a billing provider is supported. 
     *
     *  @return <code> true </code> if a billing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingProxyProvider() {
        return (getAdapteeManager().supportsBillingProxyProvider());
    }


    /**
     *  Tests if a blogging provider is supported. 
     *
     *  @return <code> true </code> if a blogging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBloggingProxyProvider() {
        return (getAdapteeManager().supportsBloggingProxyProvider());
    }


    /**
     *  Tests if a calendaring provider is supported. 
     *
     *  @return <code> true </code> if a calendaring provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringProxyProvider() {
        return (getAdapteeManager().supportsCalendaringProxyProvider());
    }


    /**
     *  Tests if a cataloging provider is supported. 
     *
     *  @return <code> true </code> if a cataloging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogingProxyProvider() {
        return (getAdapteeManager().supportsCatalogingProxyProvider());
    }


    /**
     *  Tests if a checklist provider is supported. 
     *
     *  @return <code> true </code> if a checklist provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistProxyProvider() {
        return (getAdapteeManager().supportsChecklistProxyProvider());
    }


    /**
     *  Tests if a commenting provider is supported. 
     *
     *  @return <code> true </code> if a commenting provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingProxyProvider() {
        return (getAdapteeManager().supportsCommentingProxyProvider());
    }


    /**
     *  Tests if a communication provider is supported. 
     *
     *  @return <code> true </code> if a communication provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommunicationProxyProvider() {
        return (getAdapteeManager().supportsCommunicationProxyProvider());
    }


    /**
     *  Tests if a configuration provider is supported. 
     *
     *  @return <code> true </code> if a configuration provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationProxyProvider() {
        return (getAdapteeManager().supportsConfigurationProxyProvider());
    }


    /**
     *  Tests if a contact provider is supported. 
     *
     *  @return <code> true </code> if a contact provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactProxyProvider() {
        return (getAdapteeManager().supportsContactProxyProvider());
    }


    /**
     *  Tests if a control provider is supported. 
     *
     *  @return <code> true </code> if a control provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControlProxyProvider() {
        return (getAdapteeManager().supportsControlProxyProvider());
    }


    /**
     *  Tests if a course provider is supported. 
     *
     *  @return <code> true </code> if a course provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProxyProvider() {
        return (getAdapteeManager().supportsCourseProxyProvider());
    }


    /**
     *  Tests if a dictionary provider is supported. 
     *
     *  @return <code> true </code> if a dictionary provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryProxyProvider() {
        return (getAdapteeManager().supportsDictionaryProxyProvider());
    }


    /**
     *  Tests if a filing provider is supported. 
     *
     *  @return <code> true </code> if a filing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingProxyProvider() {
        return (getAdapteeManager().supportsFilingProxyProvider());
    }


    /**
     *  Tests if a financials provider is supported. 
     *
     *  @return <code> true </code> if a financials provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsProxyProvider() {
        return (getAdapteeManager().supportsFinancialsProxyProvider());
    }


    /**
     *  Tests if a forum provider is supported. 
     *
     *  @return <code> true </code> if a forum provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumProxyProvider() {
        return (getAdapteeManager().supportsForumProxyProvider());
    }


    /**
     *  Tests if a grading provider is supported. 
     *
     *  @return <code> true </code> if a grading provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingProxyProvider() {
        return (getAdapteeManager().supportsGradingProxyProvider());
    }


    /**
     *  Tests if a hierarchy provider is supported. 
     *
     *  @return <code> true </code> if a hierarchy provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyProxyProvider() {
        return (getAdapteeManager().supportsHierarchyProxyProvider());
    }


    /**
     *  Tests if a hold provider is supported. 
     *
     *  @return <code> true </code> if a hold provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldProxyProvider() {
        return (getAdapteeManager().supportsHoldProxyProvider());
    }


    /**
     *  Tests if an id provider is supported. 
     *
     *  @return <code> true </code> if an id provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdProxyProvider() {
        return (getAdapteeManager().supportsIdProxyProvider());
    }


    /**
     *  Tests if an inquiry provider is supported. 
     *
     *  @return <code> true </code> if an inquiry provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryProxyProvider() {
        return (getAdapteeManager().supportsInquiryProxyProvider());
    }


    /**
     *  Tests if an installation provider is supported. 
     *
     *  @return <code> true </code> if an installation provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationProxyProvider() {
        return (getAdapteeManager().supportsInstallationProxyProvider());
    }


    /**
     *  Tests if an inventory provider is supported. 
     *
     *  @return <code> true </code> if an inventory provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryProxyProvider() {
        return (getAdapteeManager().supportsInventoryProxyProvider());
    }


    /**
     *  Tests if a journaling provider is supported. 
     *
     *  @return <code> true </code> if a journaling provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalingProxyProvider() {
        return (getAdapteeManager().supportsJournalingProxyProvider());
    }


    /**
     *  Tests if a learning provider is supported. 
     *
     *  @return <code> true </code> if a learning provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningProxyProvider() {
        return (getAdapteeManager().supportsLearningProxyProvider());
    }


    /**
     *  Tests if a locale provider is supported. 
     *
     *  @return <code> true </code> if a locale provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocaleProxyProvider() {
        return (getAdapteeManager().supportsLocaleProxyProvider());
    }


    /**
     *  Tests if a logging provider is supported. 
     *
     *  @return <code> true </code> if a logging provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLoggingProxyProvider() {
        return (getAdapteeManager().supportsLoggingProxyProvider());
    }


    /**
     *  Tests if a mapping provider is supported. 
     *
     *  @return <code> true </code> if a mapping provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingProxyProvider() {
        return (getAdapteeManager().supportsMappingProxyProvider());
    }


    /**
     *  Tests if a messaging provider is supported. 
     *
     *  @return <code> true </code> if a messaging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessagingProxyProvider() {
        return (getAdapteeManager().supportsMessagingProxyProvider());
    }


    /**
     *  Tests if a metering provider is supported. 
     *
     *  @return <code> true </code> if a metering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeteringProxyProvider() {
        return (getAdapteeManager().supportsMeteringProxyProvider());
    }


    /**
     *  Tests if an offering provider is supported. 
     *
     *  @return <code> true </code> if an offering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingProxyProvider() {
        return (getAdapteeManager().supportsOfferingProxyProvider());
    }


    /**
     *  Tests if an ontology provider is supported. 
     *
     *  @return <code> true </code> if an ontology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyProxyProvider() {
        return (getAdapteeManager().supportsOntologyProxyProvider());
    }


    /**
     *  Tests if an ordering provider is supported. 
     *
     *  @return <code> true </code> if an ordering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderingProxyProvider() {
        return (getAdapteeManager().supportsOrderingProxyProvider());
    }


    /**
     *  Tests if a personnel provider is supported. 
     *
     *  @return <code> true </code> if a personnel provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonnelProxyProvider() {
        return (getAdapteeManager().supportsPersonnelProxyProvider());
    }


    /**
     *  Tests if a process provider is supported. 
     *
     *  @return <code> true </code> if a process provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessProxyProvider() {
        return (getAdapteeManager().supportsProcessProxyProvider());
    }


    /**
     *  Tests if a profile provider is supported. 
     *
     *  @return <code> true </code> if a profile provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileProxyProvider() {
        return (getAdapteeManager().supportsProfileProxyProvider());
    }


    /**
     *  Tests if a provisioning provider is supported. 
     *
     *  @return <code> true </code> if a provisioning provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisioningProxyProvider() {
        return (getAdapteeManager().supportsProvisioningProxyProvider());
    }


    /**
     *  Tests if a proxy provider is supported. 
     *
     *  @return <code> true </code> if a proxy provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProxyProxyProvider() {
        return (getAdapteeManager().supportsProxyProxyProvider());
    }


    /**
     *  Tests if a recipe provider is supported. 
     *
     *  @return <code> true </code> if a recipe provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeProxyProvider() {
        return (getAdapteeManager().supportsRecipeProxyProvider());
    }


    /**
     *  Tests if a recognition provider is supported. 
     *
     *  @return <code> true </code> if a recognition provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecognitionProxyProvider() {
        return (getAdapteeManager().supportsRecognitionProxyProvider());
    }


    /**
     *  Tests if a relationship provider is supported. 
     *
     *  @return <code> true </code> if a relationship provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipProxyProvider() {
        return (getAdapteeManager().supportsRelationshipProxyProvider());
    }


    /**
     *  Tests if a repository provider is supported. 
     *
     *  @return <code> true </code> if a repository provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryProxyProvider() {
        return (getAdapteeManager().supportsRepositoryProxyProvider());
    }


    /**
     *  Tests if a resource provider is supported. 
     *
     *  @return <code> true </code> if a resource provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceProxyProvider() {
        return (getAdapteeManager().supportsResourceProxyProvider());
    }


    /**
     *  Tests if a resourcing provider is supported. 
     *
     *  @return <code> true </code> if a resourcing provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcingProxyProvider() {
        return (getAdapteeManager().supportsResourcingProxyProvider());
    }


    /**
     *  Tests if a rules provider is supported. 
     *
     *  @return <code> true </code> if a rules provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRulesProxyProvider() {
        return (getAdapteeManager().supportsRulesProxyProvider());
    }


    /**
     *  Tests if a search provider is supported. 
     *
     *  @return <code> true </code> if a search provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSearchProxyProvider() {
        return (getAdapteeManager().supportsSearchProxyProvider());
    }


    /**
     *  Tests if a sequencing provider is supported. 
     *
     *  @return <code> true </code> if a sequencing provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequencingProxyProvider() {
        return (getAdapteeManager().supportsSequencingProxyProvider());
    }


    /**
     *  Tests if a subscription provider is supported. 
     *
     *  @return <code> true </code> if a subscription provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionProxyProvider() {
        return (getAdapteeManager().supportsSubscriptionProxyProvider());
    }


    /**
     *  Tests if a topology provider is supported. 
     *
     *  @return <code> true </code> if a topology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyProxyProvider() {
        return (getAdapteeManager().supportsTopologyProxyProvider());
    }


    /**
     *  Tests if a tracking provider is supported. 
     *
     *  @return <code> true </code> if a tracking provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTrackingProxyProvider() {
        return (getAdapteeManager().supportsTrackingProxyProvider());
    }


    /**
     *  Tests if a transaction provider is supported. 
     *
     *  @return <code> true </code> if a transaction provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactionProxyProvider() {
        return (getAdapteeManager().supportsTransactionProxyProvider());
    }


    /**
     *  Tests if a transport provider is supported. 
     *
     *  @return <code> true </code> if a transport provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransportProxyProvider() {
        return (getAdapteeManager().supportsTransportProxyProvider());
    }


    /**
     *  Tests if a type provider is supported. 
     *
     *  @return <code> true </code> if a type provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTypeProxyProvider() {
        return (getAdapteeManager().supportsTypeProxyProvider());
    }


    /**
     *  Tests if a voting provider is supported. 
     *
     *  @return <code> true </code> if a voting provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingProxyProvider() {
        return (getAdapteeManager().supportsVotingProxyProvider());
    }


    /**
     *  Tests if a workflow provider is supported. 
     *
     *  @return <code> true </code> if a workflow provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowProxyProvider() {
        return (getAdapteeManager().supportsWorkflowProxyProvider());
    }


    /**
     *  Gets the manager associated with the Acknowledgement service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.AcknowledgementManager getAcknowledgementManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcknowledgementManager());
    }


    /**
     *  Gets the manager associated with the Assessment service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentManager getAssessmentManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentManager());
    }


    /**
     *  Gets the manager associated with the Authentication service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AuthenticationManager getAuthenticationManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthenticationManager());
    }


    /**
     *  Gets the manager associated with the Authorization service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationManager getAuthorizationManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationManager());
    }


    /**
     *  Gets the manager associated with the Bidding service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBiddingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BiddingManager getBiddingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBiddingManager());
    }


    /**
     *  Gets the manager associated with the Billing service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BillingManager getBillingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingManager());
    }


    /**
     *  Gets the manager associated with the Blogging service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBloggingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BloggingManager getBloggingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBloggingManager());
    }


    /**
     *  Gets the manager associated with the Calendaring service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendaringManager getCalendaringManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendaringManager());
    }


    /**
     *  Gets the manager associated with the Cataloging service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogingManager getCatalogingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogingManager());
    }


    /**
     *  Gets the manager associated with the Checklist service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistManager getChecklistManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistManager());
    }


    /**
     *  Gets the manager associated with the Commenting service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentingManager getCommentingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentingManager());
    }


    /**
     *  Gets the manager associated with the Communication service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommunicationProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.communication.CommunicationManager getCommunicationManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommunicationManager());
    }


    /**
     *  Gets the manager associated with the Configuration service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationManager getConfigurationManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationManager());
    }


    /**
     *  Gets the manager associated with the Contact service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactManager getContactManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactManager());
    }


    /**
     *  Gets the manager associated with the Control service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControlProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControlManager getControlManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControlManager());
    }


    /**
     *  Gets the manager associated with the Course service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseManager getCourseManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseManager());
    }


    /**
     *  Gets the manager associated with the Dictionary service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryManager getDictionaryManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryManager());
    }


    /**
     *  Gets the manager associated with the Filing service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFilingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FilingManager getFilingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFilingManager());
    }


    /**
     *  Gets the manager associated with the Financials service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FinancialsManager getFinancialsManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFinancialsManager());
    }


    /**
     *  Gets the manager associated with the Forum service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumManager getForumManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumManager());
    }


    /**
     *  Gets the manager associated with the Grading service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradingManager getGradingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradingManager());
    }


    /**
     *  Gets the manager associated with the Hierarchy service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyManager getHierarchyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyManager());
    }


    /**
     *  Gets the manager associated with the Hold service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldManager getHoldManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldManager());
    }


    /**
     *  Gets the manager associated with the Id service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdManager getIdManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIdManager());
    }


    /**
     *  Gets the manager associated with the Inquiry service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryManager getInquiryManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryManager());
    }


    /**
     *  Gets the manager associated with the Installation service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationManager getInstallationManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationManager());
    }


    /**
     *  Gets the manager associated with the Inventory service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryManager getInventoryManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryManager());
    }


    /**
     *  Gets the manager associated with the Journaling service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdManager getJournalingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalingManager());
    }


    /**
     *  Gets the manager associated with the Learning service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningManager getLearningManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLearningManager());
    }


    /**
     *  Gets the manager associated with the Locale service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocaleProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.LocaleManager getLocaleManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocaleManager());
    }


    /**
     *  Gets the manager associated with the Logging service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLoggingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingManager getLoggingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLoggingManager());
    }


    /**
     *  Gets the manager associated with the Mapping service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MappingManager getMappingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMappingManager());
    }


    /**
     *  Gets the manager associated with the Messaging service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessagingManager getMessagingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessagingManager());
    }


    /**
     *  Gets the manager associated with the Metering service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeteringManager getMeteringManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeteringManager());
    }


    /**
     *  Gets the manager associated with the Ontology service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingManager getOfferingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingManager());
    }


    /**
     *  Gets the manager associated with the Offering service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyManager getOntologyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyManager());
    }


    /**
     *  Gets the manager associated with the Ordering service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderingManager getOrderingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderingManager());
    }


    /**
     *  Gets the manager associated with the Personnel service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonnelProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonnelManager getPersonnelManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonnelManager());
    }


    /**
     *  Gets the manager associated with the Process service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessManager getProcessManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessManager());
    }


    /**
     *  Gets the manager associated with the Profile service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileManager getProfileManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileManager());
    }


    /**
     *  Gets the manager associated with the Provisioning service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisioningManager getProvisioningManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisioningManager());
    }


    /**
     *  Gets the manager associated with the Proxy service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProxyProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.proxy.ProxyManager getProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProxyManager());
    }


    /**
     *  Gets the manager associated with the Recipe service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeManager getRecipieManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipieManager());
    }


    /**
     *  Gets the manager associated with the Recognition service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecognitionProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.RecognitionManager getRecognitionManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecognitionManager());
    }


    /**
     *  Gets the manager associated with the Relationship service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipManager getRelationshipManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipManager());
    }


    /**
     *  Gets the manager associated with the Repository service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryManager getRepositoryManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryManager());
    }


    /**
     *  Gets the manager associated with the Resource service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceManager getResourceManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceManager());
    }


    /**
     *  Gets the manager associated with the Resourcing service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.ResourcingManager getResourcingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourcingManager());
    }


    /**
     *  Gets the manager associated with the Rules service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRulesProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesManager getRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRulesManager());
    }


    /**
     *  Gets the manager associated with the Search service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSearchProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchManager getSearchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSearchManager());
    }


    /**
     *  Gets the manager associated with the Sequencing service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequencingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.SequencingManager getSequencingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequencingManager());
    }


    /**
     *  Gets the manager associated with the Subscription service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionManager getSubscriptionManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionManager());
    }


    /**
     *  Gets the manager associated with the Topology service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransportProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyManager getTopologyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyManager());
    }


    /**
     *  Gets the manager associated with the Tracking service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTrackingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.TrackingManager getTrackingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTrackingManager());
    }


    /**
     *  Gets the manager associated with the Transaction service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransactionProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transaction.TransactionManager getTransactionManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTransactionManager());
    }


    /**
     *  Gets the manager associated with the Transport service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransportProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.TransportManager getTransportManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTransportManager());
    }


    /**
     *  Gets the manager associated with the Type service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.TypeManager getTypeManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTypeManager());
    }


    /**
     *  Gets the manager associated with the Voting service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingManager getVotingManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVotingManager());
    }


    /**
     *  Gets the manager associated with the Workflow service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowManager getWorkflowManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowManager());
    }


    /**
     *  Gets the proxy manager associated with the Acknowledgement service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.AcknowledgementProxyManager getAcknowledgementProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcknowledgementProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Assessment service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentProxyManager getAssessmentProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Authentication service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AuthenticationProxyManager getAuthenticationProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthenticationProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Authorization service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationProxyManager getAuthorizationProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAuthorizationProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Bidding service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBiddingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BiddingProxyManager getBiddingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBiddingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Billing service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BillingProxyManager getBillingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBillingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Blogging service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBloggingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.blogging.BloggingProxyManager getBloggingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBloggingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Calendaring service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendaringProxyManager getCalendaringProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCalendaringProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Cataloging service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogingProxyManager getCatalogingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCatalogingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Checklist service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistProxyManager getChecklistProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChecklistProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Commenting service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentingProxyManager getCommentingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommentingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Communication service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommunicationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.communication.CommunicationProxyManager getCommunicationProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCommunicationProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Configuration service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationProxyManager getConfigurationProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Contact service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactProxyManager getContactProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getContactProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Control service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControlProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControlProxyManager getControlProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControlProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Course service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseProxyManager getCourseProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Dictionary service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryProxyManager getDictionaryProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDictionaryProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Filing service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFilingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FilingProxyManager getFilingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFilingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Financials service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FinancialsManager getFinancialsProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFinancialsProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Forum service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumProxyManager getForumProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Grading service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradingProxyManager getGradingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Hierarchy service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyProxyManager getHierarchyProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHierarchyProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Hold service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldManager getHoldProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getHoldProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Id service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIdProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdProxyManager getIdProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIdProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Inquiry service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryProxyManager getInquiryProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInquiryProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Installation service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationProxyManager getInstallationProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInstallationProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Inventory service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryProxyManager getInventoryProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInventoryProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Journaling service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.id.IdProxyManager getJournalingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getJournalingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Learning service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningProxyManager getLearningProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLearningProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Locale service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocaleProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.LocaleProxyManager getLocaleProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLocaleProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Logging service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLoggingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingProxyManager getLoggingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLoggingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Mapping service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MappingManager getMappingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMappingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Messaging service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessagingProxyManager getMessagingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessagingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Metering service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeteringProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.MeteringProxyManager getMeteringProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeteringProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Offering service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingProxyManager getOfferingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOfferingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Ontology service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyProxyManager getOntologyProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOntologyProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Ordering service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderingProxyManager getOrderingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Personnel service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonnelProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonnelProxyManager getPersonnelProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPersonnelProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Process service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessProxyManager getProcessProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcessProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Profile service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileProxyManager getProfileProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProfileProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Provisioning service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisioningProxyManager getProvisioningProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisioningProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Proxy service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProxyProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.proxy.ProxyProxyManager getProxyProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProxyProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Recipe service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeProxyManager getRecipeProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Recognition service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecognitionProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.RecognitionProxyManager getRecognitionProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecognitionProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Relationship service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipProxyManager getRelationshipProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRelationshipProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Repository service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryProxyManager getRepositoryProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRepositoryProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Resource service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceProxyManager getResourceProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Resourcing service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.ResourcingProxyManager getResourcingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourcingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Rules service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRulesProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesProxyManager getRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRulesProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Search service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSearchProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchProxyManager getSearchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSearchProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Sequencing service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequencingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.SequencingProxyManager getSequencingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequencingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Subscription service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionProxyManager getSubscriptionProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Topology service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransportProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyProxyManager getTopologyProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTopologyProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Tracking service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTrackingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.TrackingManager getTrackingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTrackingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Transaction service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransactionProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.transaction.TransactionProxyManager getTransactionProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTransactionProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Transport service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransportProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.transport.TransportProxyManager getTransportProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTransportProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Type service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.TypeProxyManager getTypeProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTypeProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Voting service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingProxyManager getVotingProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVotingProxyManager());
    }


    /**
     *  Gets the proxy manager associated with the Workflow service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowProxyManager getWorkflowProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getWorkflowProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
