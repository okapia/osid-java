//
// OublietteElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.oubliette.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OublietteElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the OublietteElement Id.
     *
     *  @return the oubliette element Id
     */

    public static org.osid.id.Id getOublietteEntityId() {
        return (makeEntityId("osid.hold.Oubliette"));
    }


    /**
     *  Gets the BlockId element Id.
     *
     *  @return the BlockId element Id
     */

    public static org.osid.id.Id getBlockId() {
        return (makeQueryElementId("osid.hold.oubliette.BlockId"));
    }


    /**
     *  Gets the Block element Id.
     *
     *  @return the Block element Id
     */

    public static org.osid.id.Id getBlock() {
        return (makeQueryElementId("osid.hold.oubliette.Block"));
    }


    /**
     *  Gets the IssueId element Id.
     *
     *  @return the IssueId element Id
     */

    public static org.osid.id.Id getIssueId() {
        return (makeQueryElementId("osid.hold.oubliette.IssueId"));
    }


    /**
     *  Gets the Issue element Id.
     *
     *  @return the Issue element Id
     */

    public static org.osid.id.Id getIssue() {
        return (makeQueryElementId("osid.hold.oubliette.Issue"));
    }


    /**
     *  Gets the HoldId element Id.
     *
     *  @return the HoldId element Id
     */

    public static org.osid.id.Id getHoldId() {
        return (makeQueryElementId("osid.hold.oubliette.HoldId"));
    }


    /**
     *  Gets the Hold element Id.
     *
     *  @return the Hold element Id
     */

    public static org.osid.id.Id getHold() {
        return (makeQueryElementId("osid.hold.oubliette.Hold"));
    }


    /**
     *  Gets the AncestorOublietteId element Id.
     *
     *  @return the AncestorOublietteId element Id
     */

    public static org.osid.id.Id getAncestorOublietteId() {
        return (makeQueryElementId("osid.hold.oubliette.AncestorOublietteId"));
    }


    /**
     *  Gets the AncestorOubliette element Id.
     *
     *  @return the AncestorOubliette element Id
     */

    public static org.osid.id.Id getAncestorOubliette() {
        return (makeQueryElementId("osid.hold.oubliette.AncestorOubliette"));
    }


    /**
     *  Gets the DescendantOublietteId element Id.
     *
     *  @return the DescendantOublietteId element Id
     */

    public static org.osid.id.Id getDescendantOublietteId() {
        return (makeQueryElementId("osid.hold.oubliette.DescendantOublietteId"));
    }


    /**
     *  Gets the DescendantOubliette element Id.
     *
     *  @return the DescendantOubliette element Id
     */

    public static org.osid.id.Id getDescendantOubliette() {
        return (makeQueryElementId("osid.hold.oubliette.DescendantOubliette"));
    }
}
