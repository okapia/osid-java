//
// AbstractAssemblyDocetQuery.java
//
//     A DocetQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.syllabus.docet.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DocetQuery that stores terms.
 */

public abstract class AbstractAssemblyDocetQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.course.syllabus.DocetQuery,
               org.osid.course.syllabus.DocetQueryInspector,
               org.osid.course.syllabus.DocetSearchOrder {

    private final java.util.Collection<org.osid.course.syllabus.records.DocetQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.DocetQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.DocetSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDocetQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDocetQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the module <code> Id </code> for this query. 
     *
     *  @param  moduleId a module <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> moduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModuleId(org.osid.id.Id moduleId, boolean match) {
        getAssembler().addIdTerm(getModuleIdColumn(), moduleId, match);
        return;
    }


    /**
     *  Clears the module <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModuleIdTerms() {
        getAssembler().clearTerms(getModuleIdColumn());
        return;
    }


    /**
     *  Gets the module <code> Id </code> terms. 
     *
     *  @return the module <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModuleIdTerms() {
        return (getAssembler().getIdTerms(getModuleIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the module. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByModule(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getModuleColumn(), style);
        return;
    }


    /**
     *  Gets the ModuleId column name.
     *
     * @return the column name
     */

    protected String getModuleIdColumn() {
        return ("module_id");
    }


    /**
     *  Tests if a module query is available. 
     *
     *  @return <code> true </code> if a module query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a module. 
     *
     *  @return the module query 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuery getModuleQuery() {
        throw new org.osid.UnimplementedException("supportsModuleQuery() is false");
    }


    /**
     *  Clears the module terms. 
     */

    @OSID @Override
    public void clearModuleTerms() {
        getAssembler().clearTerms(getModuleColumn());
        return;
    }


    /**
     *  Gets the module terms. 
     *
     *  @return the module terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQueryInspector[] getModuleTerms() {
        return (new org.osid.course.syllabus.ModuleQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ModuleSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a module search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModuleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a module. 
     *
     *  @return the module search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModuleSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleSearchOrder getModuleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsModuleSearchOrder() is false");
    }


    /**
     *  Gets the Module column name.
     *
     * @return the column name
     */

    protected String getModuleColumn() {
        return ("module");
    }


    /**
     *  Sets the activity unit <code> Id </code> for this query. 
     *
     *  @param  activityUnitId an activity unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityUnitId(org.osid.id.Id activityUnitId, 
                                    boolean match) {
        getAssembler().addIdTerm(getActivityUnitIdColumn(), activityUnitId, match);
        return;
    }


    /**
     *  Clears the activity unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityUnitIdTerms() {
        getAssembler().clearTerms(getActivityUnitIdColumn());
        return;
    }


    /**
     *  Gets the activity unit <code> Id </code> terms. 
     *
     *  @return the activity unit <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityUnitIdTerms() {
        return (getAssembler().getIdTerms(getActivityUnitIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the activity unit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActivityUnit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getActivityUnitColumn(), style);
        return;
    }


    /**
     *  Gets the ActivityUnitId column name.
     *
     * @return the column name
     */

    protected String getActivityUnitIdColumn() {
        return ("activity_unit_id");
    }


    /**
     *  Tests if an activity unit query is available. 
     *
     *  @return <code> true </code> if an activity unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity unit. 
     *
     *  @return the activity unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuery getActivityUnitQuery() {
        throw new org.osid.UnimplementedException("supportsActivityUnitQuery() is false");
    }


    /**
     *  Clears the activity unit terms. 
     */

    @OSID @Override
    public void clearActivityUnitTerms() {
        getAssembler().clearTerms(getActivityUnitColumn());
        return;
    }


    /**
     *  Gets the activity unit terms. 
     *
     *  @return the activity unit terms 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQueryInspector[] getActivityUnitTerms() {
        return (new org.osid.course.ActivityUnitQueryInspector[0]);
    }


    /**
     *  Tests if an <code> ActivityUnitSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if an activity unit search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an actvity unit. 
     *
     *  @return the activity unit search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSearchOrder getActivityUnitSearchOrder() {
        throw new org.osid.UnimplementedException("supportsActivityUnitSearchOrder() is false");
    }


    /**
     *  Gets the ActivityUnit column name.
     *
     * @return the column name
     */

    protected String getActivityUnitColumn() {
        return ("activity_unit");
    }


    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLearningObjectiveId(org.osid.id.Id objectiveId, 
                                         boolean match) {
        getAssembler().addIdTerm(getLearningObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveIdTerms() {
        getAssembler().clearTerms(getLearningObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getLearningObjectiveIdColumn()));
    }


    /**
     *  Gets the LearningObjectiveId column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveIdColumn() {
        return ("learning_objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  objectives. 
     *
     *  @return <code> true </code> if an robjective query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningObjectiveQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getLearningObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsLearningObjectiveQuery() is false");
    }


    /**
     *  Matches a docet that has any objective assigned. 
     *
     *  @param  match <code> true </code> to match docets with any objective, 
     *          <code> false </code> to match docets with no objective 
     */

    @OSID @Override
    public void matchAnyLearningObjective(boolean match) {
        getAssembler().addIdWildcardTerm(getLearningObjectiveColumn(), match);
        return;
    }


    /**
     *  Clears the objective terms. 
     */

    @OSID @Override
    public void clearLearningObjectiveTerms() {
        getAssembler().clearTerms(getLearningObjectiveColumn());
        return;
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the LearningObjective column name.
     *
     * @return the column name
     */

    protected String getLearningObjectiveColumn() {
        return ("learning_objective");
    }


    /**
     *  Matches docets that occur in a class. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInClass(boolean match) {
        getAssembler().addBooleanTerm(getInClassColumn(), match);
        return;
    }


    /**
     *  Clears the in class terms. 
     */

    @OSID @Override
    public void clearInClassTerms() {
        getAssembler().clearTerms(getInClassColumn());
        return;
    }


    /**
     *  Gets the in class query terms. 
     *
     *  @return the in class query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getInClassTerms() {
        return (getAssembler().getBooleanTerms(getInClassColumn()));
    }


    /**
     *  Specified a preference for ordering results by the in class flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInClass(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInClassColumn(), style);
        return;
    }


    /**
     *  Gets the InClass column name.
     *
     * @return the column name
     */

    protected String getInClassColumn() {
        return ("in_class");
    }


    /**
     *  Matches durations between the given durations inclusive. 
     *
     *  @param  from starting duration 
     *  @param  to ending duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration from, 
                              org.osid.calendaring.Duration to, boolean match) {
        getAssembler().addDurationRangeTerm(getDurationColumn(), from, to, match);
        return;
    }


    /**
     *  Matches a docet that has any duration assigned. 
     *
     *  @param  match <code> true </code> to match docets with any duration, 
     *          <code> false </code> to match docets with no duration 
     */

    @OSID @Override
    public void matchAnyDuration(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getDurationColumn(), match);
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        getAssembler().clearTerms(getDurationColumn());
        return;
    }


    /**
     *  Gets the duration query terms. 
     *
     *  @return the duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getDurationColumn()));
    }


    /**
     *  Specified a preference for ordering results by the duration. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDurationColumn(), style);
        return;
    }


    /**
     *  Gets the Duration column name.
     *
     * @return the column name
     */

    protected String getDurationColumn() {
        return ("duration");
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId an asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        getAssembler().addIdTerm(getAssetIdColumn(), assetId, match);
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        getAssembler().clearTerms(getAssetIdColumn());
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (getAssembler().getIdTerms(getAssetIdColumn()));
    }


    /**
     *  Gets the AssetId column name.
     *
     * @return the column name
     */

    protected String getAssetIdColumn() {
        return ("asset_id");
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an robjective query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches a docet that has any asset assigned. 
     *
     *  @param  match <code> true </code> to match docets with any asset, 
     *          <code> false </code> to match docets with no asset 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        getAssembler().addIdWildcardTerm(getAssetColumn(), match);
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        getAssembler().clearTerms(getAssetColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the asset terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the Asset column name.
     *
     * @return the column name
     */

    protected String getAssetColumn() {
        return ("asset");
    }


    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches an objective that has any assessment assigned. 
     *
     *  @param  match <code> true </code> to match docets with any assessment, 
     *          <code> false </code> to match docets with no assessment 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentColumn(), match);
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the asset terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssessmentTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the docet <code> Id </code> for this query to match syllabi 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this docet supports the given record
     *  <code>Type</code>.
     *
     *  @param  docetRecordType a docet record type 
     *  @return <code>true</code> if the docetRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type docetRecordType) {
        for (org.osid.course.syllabus.records.DocetQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(docetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  docetRecordType the docet record type 
     *  @return the docet query record 
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(docetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetQueryRecord getDocetQueryRecord(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.DocetQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(docetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(docetRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  docetRecordType the docet record type 
     *  @return the docet query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(docetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetQueryInspectorRecord getDocetQueryInspectorRecord(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.DocetQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(docetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(docetRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param docetRecordType the docet record type
     *  @return the docet search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(docetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.DocetSearchOrderRecord getDocetSearchOrderRecord(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.DocetSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(docetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(docetRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this docet. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param docetQueryRecord the docet query record
     *  @param docetQueryInspectorRecord the docet query inspector
     *         record
     *  @param docetSearchOrderRecord the docet search order record
     *  @param docetRecordType docet record type
     *  @throws org.osid.NullArgumentException
     *          <code>docetQueryRecord</code>,
     *          <code>docetQueryInspectorRecord</code>,
     *          <code>docetSearchOrderRecord</code> or
     *          <code>docetRecordTypedocet</code> is
     *          <code>null</code>
     */
            
    protected void addDocetRecords(org.osid.course.syllabus.records.DocetQueryRecord docetQueryRecord, 
                                      org.osid.course.syllabus.records.DocetQueryInspectorRecord docetQueryInspectorRecord, 
                                      org.osid.course.syllabus.records.DocetSearchOrderRecord docetSearchOrderRecord, 
                                      org.osid.type.Type docetRecordType) {

        addRecordType(docetRecordType);

        nullarg(docetQueryRecord, "docet query record");
        nullarg(docetQueryInspectorRecord, "docet query inspector record");
        nullarg(docetSearchOrderRecord, "docet search odrer record");

        this.queryRecords.add(docetQueryRecord);
        this.queryInspectorRecords.add(docetQueryInspectorRecord);
        this.searchOrderRecords.add(docetSearchOrderRecord);
        
        return;
    }
}
