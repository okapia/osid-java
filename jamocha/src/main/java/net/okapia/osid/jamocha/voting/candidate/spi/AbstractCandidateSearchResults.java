//
// AbstractCandidateSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.candidate.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCandidateSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.CandidateSearchResults {

    private org.osid.voting.CandidateList candidates;
    private final org.osid.voting.CandidateQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.records.CandidateSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCandidateSearchResults.
     *
     *  @param candidates the result set
     *  @param candidateQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>candidates</code>
     *          or <code>candidateQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCandidateSearchResults(org.osid.voting.CandidateList candidates,
                                            org.osid.voting.CandidateQueryInspector candidateQueryInspector) {
        nullarg(candidates, "candidates");
        nullarg(candidateQueryInspector, "candidate query inspectpr");

        this.candidates = candidates;
        this.inspector = candidateQueryInspector;

        return;
    }


    /**
     *  Gets the candidate list resulting from a search.
     *
     *  @return a candidate list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidates() {
        if (this.candidates == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.CandidateList candidates = this.candidates;
        this.candidates = null;
	return (candidates);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.CandidateQueryInspector getCandidateQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  candidate search record <code> Type. </code> This method must
     *  be used to retrieve a candidate implementing the requested
     *  record.
     *
     *  @param candidateSearchRecordType a candidate search 
     *         record type 
     *  @return the candidate search
     *  @throws org.osid.NullArgumentException
     *          <code>candidateSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(candidateSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.CandidateSearchResultsRecord getCandidateSearchResultsRecord(org.osid.type.Type candidateSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.records.CandidateSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(candidateSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(candidateSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record candidate search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCandidateRecord(org.osid.voting.records.CandidateSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "candidate record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
