//
// AbstractAdapterEntryLookupSession.java
//
//    An Entry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Entry lookup session adapter.
 */

public abstract class AbstractAdapterEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.billing.EntryLookupSession {

    private final org.osid.billing.EntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEntryLookupSession(org.osid.billing.EntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code Entry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupEntries() {
        return (this.session.canLookupEntries());
    }


    /**
     *  A complete view of the {@code Entry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEntryView() {
        this.session.useComparativeEntryView();
        return;
    }


    /**
     *  A complete view of the {@code Entry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEntryView() {
        this.session.usePlenaryEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    

    /**
     *  Only entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveEntryView() {
        this.session.useEffectiveEntryView();
        return;
    }
    

    /**
     *  All entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveEntryView() {
        this.session.useAnyEffectiveEntryView();
        return;
    }

     
    /**
     *  Gets the {@code Entry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Entry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Entry} and
     *  retained for compatibility.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  @param entryId {@code Id} of the {@code Entry}
     *  @return the entry
     *  @throws org.osid.NotFoundException {@code entryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code entryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntry(entryId));
    }


    /**
     *  Gets an {@code EntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  entries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Entries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  entryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Entry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code entryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByIds(org.osid.id.IdList entryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByIds(entryIds));
    }


    /**
     *  Gets an {@code EntryList} corresponding to the given
     *  entry genus {@code Type} which does not include
     *  entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned {@code Entry} list
     *  @throws org.osid.NullArgumentException
     *          {@code entryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByGenusType(entryGenusType));
    }


    /**
     *  Gets an {@code EntryList} corresponding to the given
     *  entry genus {@code Type} and include any additional
     *  entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned {@code Entry} list
     *  @throws org.osid.NullArgumentException
     *          {@code entryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByParentGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByParentGenusType(entryGenusType));
    }


    /**
     *  Gets an {@code EntryList} containing the given
     *  entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned {@code Entry} list
     *  @throws org.osid.NullArgumentException
     *          {@code entryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByRecordType(entryRecordType));
    }


    /**
     *  Gets an {@code EntryList} in the given billing
     *  period.
     *  
     *  <In plenary mode, the returned list contains all known 
     *  entries or an error results. Otherwise, the returned list may contain 
     *  only those entries that are accessible through this session. 
     *  
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  periodId a billing period {@code Id} 
     *  @return the returned {@code Entry} list 
     *  @throws org.osid.NullArgumentException {@code periodId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriod(org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByPeriod(periodId));
    }


    /**
     *  Gets an {@code EntryList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible
     *  through this session.
     *  
     *  In active mode, entries are returned that are currently
     *  active. In any status mode, active and inactive entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Entry} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.billing.EntryList getEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesOnDate(from, to));
    }
        

    /**
     *  Gets a list of entries corresponding to a customer
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session.
     *
     *  In effective mode, entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  entries and those currently expired are returned.
     *
     *  @param  customerId the {@code Id} of the customer
     *  @return the returned {@code EntryList}
     *  @throws org.osid.NullArgumentException {@code customerId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForCustomer(org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesForCustomer(customerId));
    }


    /**
     *  Gets a list of entries corresponding to a customer
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible
     *  through this session.
     *
     *  In effective mode, entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  entries and those currently expired are returned.
     *
     *  @param  customerId the {@code Id} of the customer
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EntryList}
     *  @throws org.osid.NullArgumentException {@code customerId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForCustomerOnDate(org.osid.id.Id customerId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesForCustomerOnDate(customerId, from, to));
    }


    /**
     *  Gets an {@code EntryList} for the given customer in a
     *  billing period. {@code}
     *  
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *  
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId an customer {@code Id} 
     *  @param  periodId a billing period {@code Id} 
     *  @return the returned {@code Entry} list 
     *  @throws org.osid.NullArgumentException {@code customerId} or {@code 
     *          periodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForCustomer(org.osid.id.Id customerId, 
                                                                    org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByPeriodForCustomer(customerId, periodId));
    }


    /**
     *  Gets an {@code EntryList} in the given billing period
     *  for the given customer and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *  
     *  In effective mode, entries are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId an customer {@code Id} 
     *  @param  periodId a billing period {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Entry} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code customerId, periodId, from, 
     *          or to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForCustomerOnDate(org.osid.id.Id customerId, 
                                                                      org.osid.id.Id periodId, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByPeriodForCustomerOnDate(customerId, periodId, from, to));
    }


    /**
     *  Gets a list of entries corresponding to an item
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible
     *  through this session.
     *
     *  In effective mode, entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  entries and those currently expired are returned.
     *
     *  @param  itemId the {@code Id} of the item
     *  @return the returned {@code EntryList}
     *  @throws org.osid.NullArgumentException {@code itemId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForItem(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesForItem(itemId));
    }


    /**
     *  Gets a list of entries corresponding to an item
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible
     *  through this session.
     *
     *  In effective mode, entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  entries and those currently expired are returned.
     *
     *  @param  itemId the {@code Id} of the item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EntryList}
     *  @throws org.osid.NullArgumentException {@code itemId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForItemOnDate(org.osid.id.Id itemId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesForItemOnDate(itemId, from, to));
    }


    /**
     *  Gets an {@code EntryList} for the given item in a
     *  billing period. {@code}
     *  
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *  
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  itemId an item {@code Id} 
     *  @param  periodId a billing period {@code Id} 
     *  @return the returned {@code Entry} list 
     *  @throws org.osid.NullArgumentException {@code itemId} or {@code 
     *          periodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForItem(org.osid.id.Id itemId, 
                                                                org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByPeriodForItem(itemId, periodId));
    }


    /**
     *  Gets an {@code EntryList} in the given billing period
     *  for the given item and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *  
     *  In effective mode, entries are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  itemId an item {@code Id} 
     *  @param  periodId a billing period {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Entry} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code itemId, periodId, from, 
     *          or to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForItemOnDate(org.osid.id.Id itemId, 
                                                                      org.osid.id.Id periodId, 
                                                                      org.osid.calendaring.DateTime from, 
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByPeriodForItemOnDate(itemId, periodId, from, to));
    }
    

    /**
     *  Gets a list of entries corresponding to customer and item
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId the {@code Id} of the customer
     *  @param  itemId the {@code Id} of the item
     *  @return the returned {@code EntryList}
     *  @throws org.osid.NullArgumentException {@code customerId},
     *          {@code itemId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForCustomerAndItem(org.osid.id.Id customerId,
                                                                   org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesForCustomerAndItem(customerId, itemId));
    }


    /**
     *  Gets a list of entries corresponding to customer and item
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible
     *  through this session.
     *
     *  In effective mode, entries are returned that are currently
     *  effective. In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  @param  itemId the {@code Id} of the item
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code EntryList}
     *  @throws org.osid.NullArgumentException {@code customerId},
     *          {@code itemId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesForCustomerAndItemOnDate(org.osid.id.Id customerId,
                                                                         org.osid.id.Id itemId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesForCustomerAndItemOnDate(customerId, itemId, from, to));
    }



    /**
     *  Gets an {@code EntryList} for the given customer and
     *  item in a billing period. 
     *  
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *  
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer {@code Id} 
     *  @param  itemId an item {@code Id} 
     *  @param  periodId a period {@code Id} 
     *  @return the returned {@code Entry} list 
     *  @throws org.osid.NullArgumentException {@code customerId, itemId, 
     *         } or {@code periodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForCustomerAndItem(org.osid.id.Id customerId, 
                                                                           org.osid.id.Id itemId, 
                                                                           org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByPeriodForCustomerAndItem(customerId, itemId, periodId));
    }

    
    /**
     *  Gets an {@code EntryList} for the given customer and
     *  item in a billing period and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *  
     *  In effective mode, entries are returned that are currently
     *  effective in addition to being effective in the given date
     *  range. In any effective mode, effective entries and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer {@code Id} 
     *  @param  itemId an item {@code Id} 
     *  @param  periodId a period {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Entry} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code customerId,
     *          itemId, periodId, from, or to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByPeriodForCustomerAndItemOnDate(org.osid.id.Id customerId, 
                                                                                 org.osid.id.Id itemId, 
                                                                                 org.osid.id.Id periodId, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getEntriesByPeriodForCustomerAndItemOnDate(customerId, itemId, periodId, from, to));
    }


    /**
     *  Gets all {@code Entries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, entries are returned that are currently
     *  effective.  In any effective mode, effective entries and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Entries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntries());
    }
}
