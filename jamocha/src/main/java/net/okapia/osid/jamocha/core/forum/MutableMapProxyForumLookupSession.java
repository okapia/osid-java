//
// MutableMapProxyForumLookupSession
//
//    Implements a Forum lookup service backed by a collection of
//    forums that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum;


/**
 *  Implements a Forum lookup service backed by a collection of
 *  forums. The forums are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of forums can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyForumLookupSession
    extends net.okapia.osid.jamocha.core.forum.spi.AbstractMapForumLookupSession
    implements org.osid.forum.ForumLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyForumLookupSession} with no
     *  forums.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyForumLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyForumLookupSession} with a
     *  single forum.
     *
     *  @param forum a forum
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forum} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyForumLookupSession(org.osid.forum.Forum forum, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForum(forum);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyForumLookupSession} using an
     *  array of forums.
     *
     *  @param forums an array of forums
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forums} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyForumLookupSession(org.osid.forum.Forum[] forums, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForums(forums);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyForumLookupSession} using
     *  a collection of forums.
     *
     *  @param forums a collection of forums
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code forums} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyForumLookupSession(java.util.Collection<? extends org.osid.forum.Forum> forums,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putForums(forums);
        return;
    }

    
    /**
     *  Makes a {@code Forum} available in this session.
     *
     *  @param forum an forum
     *  @throws org.osid.NullArgumentException {@code forum{@code 
     *          is {@code null}
     */

    @Override
    public void putForum(org.osid.forum.Forum forum) {
        super.putForum(forum);
        return;
    }


    /**
     *  Makes an array of forums available in this session.
     *
     *  @param forums an array of forums
     *  @throws org.osid.NullArgumentException {@code forums{@code 
     *          is {@code null}
     */

    @Override
    public void putForums(org.osid.forum.Forum[] forums) {
        super.putForums(forums);
        return;
    }


    /**
     *  Makes collection of forums available in this session.
     *
     *  @param forums
     *  @throws org.osid.NullArgumentException {@code forum{@code 
     *          is {@code null}
     */

    @Override
    public void putForums(java.util.Collection<? extends org.osid.forum.Forum> forums) {
        super.putForums(forums);
        return;
    }


    /**
     *  Removes a Forum from this session.
     *
     *  @param forumId the {@code Id} of the forum
     *  @throws org.osid.NullArgumentException {@code forumId{@code  is
     *          {@code null}
     */

    @Override
    public void removeForum(org.osid.id.Id forumId) {
        super.removeForum(forumId);
        return;
    }    
}
