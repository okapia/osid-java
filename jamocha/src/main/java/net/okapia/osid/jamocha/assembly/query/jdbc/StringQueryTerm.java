//
// StringQueryTerm.java
//
//     Formats SQL for a string query term.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc;

import net.okapia.osid.primordium.types.search.StringMatchTypes;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Formats SQL for a string query term.
 */

public class StringQueryTerm 
    extends net.okapia.osid.primordium.terms.spi.AbstractStringTerm
    implements net.okapia.osid.jamocha.assembly.query.QueryTerm {

    private final String column;
    private final StringMatchTypes stringMatch;


    /**
     *  Constructs a new <code>StringQueryTerm</code>.
     *
     *  @param column name of query column
     *  @param value the string value
     *  @param stringMatchType the type of string matching pattern
     *  @param match <code>true</code> or <code>false</code>
     *  @throws org.osid.NullArgumentException is <code>value</code>
     *          or <code>stringMatchType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>stringMatchType</code> is unsupported
     */

    protected StringQueryTerm(String column, String value, org.osid.type.Type stringMatchType, boolean match) {
        super(value, stringMatchType, match);
        
        nullarg(column, "column");
        this.column = column;
        this.stringMatch = QueryAssembler.getStringMatchType(stringMatchType);

        return;
    }


    /**
     *  Gets the query string for this term.
     *
     *  @return the query string
     */

    @Override
    public String toString() {
        if (this.stringMatch  == StringMatchTypes.IGNORECASE) {
            return ("UPPER(" + getColumn() + ")" + getOperator() + "'" + getValue() +"'");
        } else {
            return (getColumn() + getOperator() + "'" + getValue() +"'");
        }
    }

    
    /**
     *  Gets the query column.
     *
     *  @return the column
     */

    @Override
    public String getColumn() {
        return (this.column);
    }


    /**
     *  Gets the query operator for this term.
     *
     *  @return the query operator
     */

    protected String getOperator() {
        if (isPositive()) {
            if (this.stringMatch == StringMatchTypes.EXACT) {
                return ("=");
            } else {
                return (" LIKE ");
            }
        } else {
            if (this.stringMatch == StringMatchTypes.EXACT) {
                return ("!=");
            } else {
                return (" NOT LIKE ");
            }
        }
    }


    /**
     *  Gets the value for this term.
     *
     *  @return the value
     */

    protected String getValue() {
        if (this.stringMatch == StringMatchTypes.WILDCARD) {
            return (getString().replace("*", "%").replace("?", "_"));
        }
        
        if (this.stringMatch == StringMatchTypes.IGNORECASE) {
            return ("UPPER('" + getString() + "')");
        }

        return (getString());
    }
}
