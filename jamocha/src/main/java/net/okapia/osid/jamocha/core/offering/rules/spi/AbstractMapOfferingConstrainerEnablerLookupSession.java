//
// AbstractMapOfferingConstrainerEnablerLookupSession
//
//    A simple framework for providing an OfferingConstrainerEnabler lookup service
//    backed by a fixed collection of offering constrainer enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an OfferingConstrainerEnabler lookup service backed by a
 *  fixed collection of offering constrainer enablers. The offering constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>OfferingConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOfferingConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.offering.rules.spi.AbstractOfferingConstrainerEnablerLookupSession
    implements org.osid.offering.rules.OfferingConstrainerEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.rules.OfferingConstrainerEnabler> offeringConstrainerEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.rules.OfferingConstrainerEnabler>());


    /**
     *  Makes an <code>OfferingConstrainerEnabler</code> available in this session.
     *
     *  @param  offeringConstrainerEnabler an offering constrainer enabler
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnabler<code>
     *          is <code>null</code>
     */

    protected void putOfferingConstrainerEnabler(org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler) {
        this.offeringConstrainerEnablers.put(offeringConstrainerEnabler.getId(), offeringConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of offering constrainer enablers available in this session.
     *
     *  @param  offeringConstrainerEnablers an array of offering constrainer enablers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putOfferingConstrainerEnablers(org.osid.offering.rules.OfferingConstrainerEnabler[] offeringConstrainerEnablers) {
        putOfferingConstrainerEnablers(java.util.Arrays.asList(offeringConstrainerEnablers));
        return;
    }


    /**
     *  Makes a collection of offering constrainer enablers available in this session.
     *
     *  @param  offeringConstrainerEnablers a collection of offering constrainer enablers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putOfferingConstrainerEnablers(java.util.Collection<? extends org.osid.offering.rules.OfferingConstrainerEnabler> offeringConstrainerEnablers) {
        for (org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler : offeringConstrainerEnablers) {
            this.offeringConstrainerEnablers.put(offeringConstrainerEnabler.getId(), offeringConstrainerEnabler);
        }

        return;
    }


    /**
     *  Removes an OfferingConstrainerEnabler from this session.
     *
     *  @param  offeringConstrainerEnablerId the <code>Id</code> of the offering constrainer enabler
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeOfferingConstrainerEnabler(org.osid.id.Id offeringConstrainerEnablerId) {
        this.offeringConstrainerEnablers.remove(offeringConstrainerEnablerId);
        return;
    }


    /**
     *  Gets the <code>OfferingConstrainerEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  offeringConstrainerEnablerId <code>Id</code> of the <code>OfferingConstrainerEnabler</code>
     *  @return the offeringConstrainerEnabler
     *  @throws org.osid.NotFoundException <code>offeringConstrainerEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnabler getOfferingConstrainerEnabler(org.osid.id.Id offeringConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler = this.offeringConstrainerEnablers.get(offeringConstrainerEnablerId);
        if (offeringConstrainerEnabler == null) {
            throw new org.osid.NotFoundException("offeringConstrainerEnabler not found: " + offeringConstrainerEnablerId);
        }

        return (offeringConstrainerEnabler);
    }


    /**
     *  Gets all <code>OfferingConstrainerEnablers</code>. In plenary mode, the returned
     *  list contains all known offeringConstrainerEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  offeringConstrainerEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>OfferingConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.rules.offeringconstrainerenabler.ArrayOfferingConstrainerEnablerList(this.offeringConstrainerEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offeringConstrainerEnablers.clear();
        super.close();
        return;
    }
}
