//
// AssessmentSectionMiter.java
//
//     Defines an AssessmentSection miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmentsection;


/**
 *  Defines an <code>AssessmentSection</code> miter for use with the builders.
 */

public interface AssessmentSectionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.assessment.AssessmentSection {


    /**
     *  Sets the allocated time.
     *
     *  @param time an allocated time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setAllocatedTime(org.osid.calendaring.Duration time);


    /**
     *  Sets the sequential flag.
     *
     *  @param sequential <code> true </code> if the items are taken
     *         sequentially, <code> false </code> if the items can be
     *         skipped and revisited
     */

    public void setItemsSequential(boolean sequential);


    /**
     *  Sets the are items shuffled.
     *
     *  @param shuffled <code>true</code> if items are shuffled,
     *         <code>false</code> otherwise
     */

    public void setItemsShuffled(boolean shuffled);


    /**
     *  Adds an AssessmentSection record.
     *
     *  @param record an assessmentSection record
     *  @param recordType the type of assessmentSection record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAssessmentSectionRecord(org.osid.assessment.records.AssessmentSectionRecord record, org.osid.type.Type recordType);
}       


