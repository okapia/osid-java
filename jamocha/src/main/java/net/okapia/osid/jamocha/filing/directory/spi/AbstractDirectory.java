//
// AbstractDirectory.java
//
//     Defines a Directory.
//
//
// Tom Coppeto
// Okapia
// 8 October 2002
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.directory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Directory</code>.
 */

public abstract class AbstractDirectory
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalog
    implements org.osid.filing.Directory {

    private String name;
    private boolean alias = false;
    private String path;
    private String realPath;
    private org.osid.authentication.Agent owner;
    private org.osid.calendaring.DateTime created;
    private org.osid.calendaring.DateTime modified;
    private org.osid.calendaring.DateTime accessed;

    private final java.util.Collection<org.osid.filing.records.DirectoryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the name of this entry. The name does not include the path. If 
     *  this entry represents an alias, the name of the alias is returned. 
     *
     *  @return the entry name 
     */

    @OSID @Override
    public String getName() {
        return (this.name);
    }


    /**
     *  Sets the name.
     *  
     *  @param name the file name
     *  @throws org.osid.NullArgumentException <code>name</code> is
     *          <code>null</code>
     */

    protected void setName(String name) {
        nullarg(name, "file name");
        this.name = name;
        return;
    }

                                         
    /**
     *  Tests if this entry is an alias. 
     *
     *  @return <code> true </code> if this is an alias, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean isAlias() {
        return (this.alias);
    }


    /**
     *  Sets the alias flag.
     *  
     *  @param alias <code>true</code> if an alias, <code>false</code>
     *         otherwise
     */

    protected void setAlias(boolean alias) {
        this.alias = alias;
        return;
    }


    /**
     *  Gets the full path of this entry. The path includes the name. Path 
     *  components are separated by a /. If this entry represents an alias, 
     *  the path to the alias is returned. 
     *
     *  @return the path 
     */

    @OSID @Override
    public String getPath() {
        return (this.path);
    }


    /**
     *  Sets the path.
     *  
     *  @param path the file path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    protected void setPath(String path) {
        nullarg(path, "file path");
        this.path = path;
        return;
    }


    /**
     *  Gets the real path of this entry. The path includes the name. Path 
     *  components are separated by a /. If this entry represents an alias, 
     *  the full path to the target file or directory is returned. 
     *
     *  @return the path 
     */

    @OSID @Override
    public String getRealPath() {
        return (this.realPath);
    }


    /**
     *  Sets the real path.
     *  
     *  @param path the file real path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    protected void setRealPath(String path) {
        nullarg(path, "file real path");
        this.realPath = path;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> that owns this 
     *  entry. 
     *
     *  @return the <code> Agent Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOwnerId() {
        return (this.owner.getId());
    }


    /**
     *  Gets the <code> Agent </code> that owns this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException authentication service not 
     *          available 
     */

    @OSID @Override
    public org.osid.authentication.Agent getOwner()
        throws org.osid.OperationFailedException {

        return (this.owner);
    }


    /**
     *  Sets the owner.
     *  
     *  @param agent the file owner
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setOwner(org.osid.authentication.Agent agent) {
        nullarg(agent, "file owner");
        this.owner = agent;
        return;
    }


    /**
     *  Gets the created time of this entry. 
     *
     *  @return the created time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCreatedTime() {
        return (this.created);
    }


    /**
     *  Sets the created time.
     *  
     *  @param time the file created time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setCreatedTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "file created time");
        this.created = time;
        return;
    }


    /**
     *  Gets the last modified time of this entry. 
     *
     *  @return the last modified time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastModifiedTime() {
        return (this.modified);
    }


    /**
     *  Sets the modified time.
     *  
     *  @param time the file modified time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setLastModifiedTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "file last modified time");
        this.modified = time;
        return;
    }


    /**
     *  Gets the last access time of this entry. 
     *
     *  @return the last access time 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastAccessTime() {
        return (this.accessed);
    }


    /**
     *  Sets the access time.
     *  
     *  @param time the file access time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setLastAccessTime(org.osid.calendaring.DateTime time) {
        nullarg(time, "file last access time");
        this.accessed = time;
        return;
    }

    
    /**
     *  Tests if this directory supports the given record
     *  <code>Type</code>.
     *
     *  @param  directoryRecordType a directory record type 
     *  @return <code>true</code> if the directoryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type directoryRecordType) {
        for (org.osid.filing.records.DirectoryRecord record : this.records) {
            if (record.implementsRecordType(directoryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Directory</code>
     *  record <code>Type</code>.
     *
     *  @param  directoryRecordType the directory record type 
     *  @return the directory record 
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directoryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.DirectoryRecord getDirectoryRecord(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.DirectoryRecord record : this.records) {
            if (record.implementsRecordType(directoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this directory. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param directoryRecord the directory record
     *  @param directoryRecordType directory record type
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecord</code> or
     *          <code>directoryRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addDirectoryRecord(org.osid.filing.records.DirectoryRecord directoryRecord, 
                                      org.osid.type.Type directoryRecordType) {
        
        nullarg(directoryRecord, "directory record");
        addRecordType(directoryRecordType);
        this.records.add(directoryRecord);
        
        return;
    }
}
