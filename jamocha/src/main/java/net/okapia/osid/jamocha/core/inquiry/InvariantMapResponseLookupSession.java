//
// InvariantMapResponseLookupSession
//
//    Implements a Response lookup service backed by a fixed collection of
//    responses.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements a Response lookup service backed by a fixed
 *  collection of responses. The responses are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapResponseLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractMapResponseLookupSession
    implements org.osid.inquiry.ResponseLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapResponseLookupSession</code> with no
     *  responses.
     *  
     *  @param inquest the inquest
     *  @throws org.osid.NullArgumnetException {@code inquest} is
     *          {@code null}
     */

    public InvariantMapResponseLookupSession(org.osid.inquiry.Inquest inquest) {
        setInquest(inquest);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResponseLookupSession</code> with a single
     *  response.
     *  
     *  @param inquest the inquest
     *  @param response a single response
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code response} is <code>null</code>
     */

      public InvariantMapResponseLookupSession(org.osid.inquiry.Inquest inquest,
                                               org.osid.inquiry.Response response) {
        this(inquest);
        putResponse(response);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResponseLookupSession</code> using an array
     *  of responses.
     *  
     *  @param inquest the inquest
     *  @param responses an array of responses
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code responses} is <code>null</code>
     */

      public InvariantMapResponseLookupSession(org.osid.inquiry.Inquest inquest,
                                               org.osid.inquiry.Response[] responses) {
        this(inquest);
        putResponses(responses);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapResponseLookupSession</code> using a
     *  collection of responses.
     *
     *  @param inquest the inquest
     *  @param responses a collection of responses
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code responses} is <code>null</code>
     */

      public InvariantMapResponseLookupSession(org.osid.inquiry.Inquest inquest,
                                               java.util.Collection<? extends org.osid.inquiry.Response> responses) {
        this(inquest);
        putResponses(responses);
        return;
    }
}
