//
// AbstractBudgetEntryQuery.java
//
//     A template for making a BudgetEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budgetentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for budget entries.
 */

public abstract class AbstractBudgetEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.financials.budgeting.BudgetEntryQuery {

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the budget <code> Id </code> for this query. 
     *
     *  @param  budgetId a budget <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> budgetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBudgetId(org.osid.id.Id budgetId, boolean match) {
        return;
    }


    /**
     *  Clears the budget <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBudgetIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BudgetQuery </code> is available. 
     *
     *  @return <code> true </code> if a budget query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetQuery() {
        return (false);
    }


    /**
     *  Gets the query for a budget. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the payer query 
     *  @throws org.osid.UnimplementedException the budget query 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQuery getBudgetQuery() {
        throw new org.osid.UnimplementedException("supportsBudgetQuery() is false");
    }


    /**
     *  Clears the budget terms. 
     */

    @OSID @Override
    public void clearBudgetTerms() {
        return;
    }


    /**
     *  Sets the account <code> Id </code> for this query. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> accountId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAccountId(org.osid.id.Id accountId, boolean match) {
        return;
    }


    /**
     *  Clears the account <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAccountIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AccountQuery </code> is available. 
     *
     *  @return <code> true </code> if an account query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAccountQuery() {
        return (false);
    }


    /**
     *  Gets the query for an account. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the account query 
     *  @throws org.osid.UnimplementedException <code> supportsAccountQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.AccountQuery getAccountQuery() {
        throw new org.osid.UnimplementedException("supportsAccountQuery() is false");
    }


    /**
     *  Matches entries that have any account set. 
     *
     *  @param  match <code> true </code> to match entries with any account, 
     *          <code> false </code> to match entries with no account 
     */

    @OSID @Override
    public void matchAnyAccount(boolean match) {
        return;
    }


    /**
     *  Clears the account terms. 
     */

    @OSID @Override
    public void clearAccountTerms() {
        return;
    }


    /**
     *  Matches the amount between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency low, 
                            org.osid.financials.Currency high, boolean match) {
        return;
    }


    /**
     *  Matches entries that have any amount set. 
     *
     *  @param  match <code> true </code> to match entries with any amount, 
     *          <code> false </code> to match entries with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        return;
    }


    /**
     *  Clears the amount terms. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        return;
    }


    /**
     *  Matches entries that have debit amounts. 
     *
     *  @param  match <code> true </code> to match entries with a debit 
     *          amount, <code> false </code> to match entries with a credit 
     *          amount 
     */

    @OSID @Override
    public void matchDebit(boolean match) {
        return;
    }


    /**
     *  Clears the debit terms. 
     */

    @OSID @Override
    public void clearDebitTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match budget 
     *  entries assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given budget entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a budget entry implementing the requested record.
     *
     *  @param budgetEntryRecordType a budget entry record type
     *  @return the budget entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetEntryQueryRecord getBudgetEntryQueryRecord(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(budgetEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this budget entry query. 
     *
     *  @param budgetEntryQueryRecord budget entry query record
     *  @param budgetEntryRecordType budgetEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBudgetEntryQueryRecord(org.osid.financials.budgeting.records.BudgetEntryQueryRecord budgetEntryQueryRecord, 
                                          org.osid.type.Type budgetEntryRecordType) {

        addRecordType(budgetEntryRecordType);
        nullarg(budgetEntryQueryRecord, "budget entry query record");
        this.records.add(budgetEntryQueryRecord);        
        return;
    }
}
