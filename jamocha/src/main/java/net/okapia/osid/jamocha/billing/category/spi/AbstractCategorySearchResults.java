//
// AbstractCategorySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.category.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCategorySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.billing.CategorySearchResults {

    private org.osid.billing.CategoryList categories;
    private final org.osid.billing.CategoryQueryInspector inspector;
    private final java.util.Collection<org.osid.billing.records.CategorySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCategorySearchResults.
     *
     *  @param categories the result set
     *  @param categoryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>categories</code>
     *          or <code>categoryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCategorySearchResults(org.osid.billing.CategoryList categories,
                                            org.osid.billing.CategoryQueryInspector categoryQueryInspector) {
        nullarg(categories, "categories");
        nullarg(categoryQueryInspector, "category query inspectpr");

        this.categories = categories;
        this.inspector = categoryQueryInspector;

        return;
    }


    /**
     *  Gets the category list resulting from a search.
     *
     *  @return a category list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.billing.CategoryList getCategories() {
        if (this.categories == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.billing.CategoryList categories = this.categories;
        this.categories = null;
	return (categories);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.billing.CategoryQueryInspector getCategoryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  category search record <code> Type. </code> This method must
     *  be used to retrieve a category implementing the requested
     *  record.
     *
     *  @param categorySearchRecordType a category search 
     *         record type 
     *  @return the category search
     *  @throws org.osid.NullArgumentException
     *          <code>categorySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(categorySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CategorySearchResultsRecord getCategorySearchResultsRecord(org.osid.type.Type categorySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.billing.records.CategorySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(categorySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(categorySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record category search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCategoryRecord(org.osid.billing.records.CategorySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "category record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
