//
// AbstractAssemblyBankQuery.java
//
//     A BankQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.assessment.bank.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BankQuery that stores terms.
 */

public abstract class AbstractAssemblyBankQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.assessment.BankQuery,
               org.osid.assessment.BankQueryInspector,
               org.osid.assessment.BankSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.BankQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.BankQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.assessment.records.BankSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBankQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBankQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears all item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if a <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches assessment banks that have any item assigned. 
     *
     *  @param  match <code> true </code> to match banks with any item, <code> 
     *          false </code> to match assessments with no item 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears all item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.assessment.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.assessment.ItemQueryInspector[0]);
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears all assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches assessment banks that have any assessment assigned. 
     *
     *  @param  match <code> true </code> to match banks with any assessment, 
     *          <code> false </code> to match banks with no assessment 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentColumn(), match);
        return;
    }


    /**
     *  Clears all assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the assessment offered <code> Id </code> for this query. 
     *
     *  @param  assessmentOfferedId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentOfferedId(org.osid.id.Id assessmentOfferedId, 
                                         boolean match) {
        getAssembler().addIdTerm(getAssessmentOfferedIdColumn(), assessmentOfferedId, match);
        return;
    }


    /**
     *  Clears all assessment offered <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedIdTerms() {
        getAssembler().clearTerms(getAssessmentOfferedIdColumn());
        return;
    }


    /**
     *  Gets the assessment offered <code> Id </code> query terms. 
     *
     *  @return the assessment offered <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentOfferedIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentOfferedIdColumn()));
    }


    /**
     *  Gets the AssessmentOfferedId column name.
     *
     * @return the column name
     */

    protected String getAssessmentOfferedIdColumn() {
        return ("assessment_offered_id");
    }


    /**
     *  Tests if an <code> AssessmentOfferedQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment offered query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment offered. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the assessment offered query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuery getAssessmentOfferedQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentOfferedQuery() is false");
    }


    /**
     *  Matches assessment banks that have any assessment offering assigned. 
     *
     *  @param  match <code> true </code> to match banks with any assessment 
     *          offering, <code> false </code> to match banks with no offering 
     */

    @OSID @Override
    public void matchAnyAssessmentOffered(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentOfferedColumn(), match);
        return;
    }


    /**
     *  Clears all assessment offered terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedTerms() {
        getAssembler().clearTerms(getAssessmentOfferedColumn());
        return;
    }


    /**
     *  Gets the assessment offered query terms. 
     *
     *  @return the assessment offered terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQueryInspector[] getAssessmentOfferedTerms() {
        return (new org.osid.assessment.AssessmentOfferedQueryInspector[0]);
    }


    /**
     *  Gets the AssessmentOffered column name.
     *
     * @return the column name
     */

    protected String getAssessmentOfferedColumn() {
        return ("assessment_offered");
    }


    /**
     *  Sets the bank <code> Id </code> for to match banks in which the 
     *  specified bank is an acestor. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getAncestorBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears all ancestor bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBankIdTerms() {
        getAssembler().clearTerms(getAncestorBankIdColumn());
        return;
    }


    /**
     *  Gets the ancestor bank <code> Id </code> query terms. 
     *
     *  @return the ancestor bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBankIdTerms() {
        return (getAssembler().getIdTerms(getAncestorBankIdColumn()));
    }


    /**
     *  Gets the AncestorBankId column name.
     *
     * @return the column name
     */

    protected String getAncestorBankIdColumn() {
        return ("ancestor_bank_id");
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ancestor bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getAncestorBankQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBankQuery() is false");
    }


    /**
     *  Matches a bank that has any ancestor. 
     *
     *  @param  match <code> true </code> to match banks with any ancestor 
     *          banks, <code> false </code> to match root banks 
     */

    @OSID @Override
    public void matchAnyAncestorBank(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorBankColumn(), match);
        return;
    }


    /**
     *  Clears all ancestor bank terms. 
     */

    @OSID @Override
    public void clearAncestorBankTerms() {
        getAssembler().clearTerms(getAncestorBankColumn());
        return;
    }


    /**
     *  Gets the ancestor bank query terms. 
     *
     *  @return the ancestor bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getAncestorBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the AncestorBank column name.
     *
     * @return the column name
     */

    protected String getAncestorBankColumn() {
        return ("ancestor_bank");
    }


    /**
     *  Sets the bank <code> Id </code> for to match banks in which the 
     *  specified bank is a descendant. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantBankId(org.osid.id.Id bankId, boolean match) {
        getAssembler().addIdTerm(getDescendantBankIdColumn(), bankId, match);
        return;
    }


    /**
     *  Clears all descendant bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBankIdTerms() {
        getAssembler().clearTerms(getDescendantBankIdColumn());
        return;
    }


    /**
     *  Gets the descendant bank <code> Id </code> query terms. 
     *
     *  @return the descendant bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBankIdTerms() {
        return (getAssembler().getIdTerms(getDescendantBankIdColumn()));
    }


    /**
     *  Gets the DescendantBankId column name.
     *
     * @return the column name
     */

    protected String getDescendantBankIdColumn() {
        return ("descendant_bank_id");
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a descendant bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getDescendantBankQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBankQuery() is false");
    }


    /**
     *  Matches a bank that has any descendant. 
     *
     *  @param  match <code> true </code> to match banks with any descendant 
     *          banks, <code> false </code> to match leaf banks 
     */

    @OSID @Override
    public void matchAnyDescendantBank(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantBankColumn(), match);
        return;
    }


    /**
     *  Clears all descendant bank terms. 
     */

    @OSID @Override
    public void clearDescendantBankTerms() {
        getAssembler().clearTerms(getDescendantBankColumn());
        return;
    }


    /**
     *  Gets the descendant bank query terms. 
     *
     *  @return the descendant bank terms 
     */

    @OSID @Override
    public org.osid.assessment.BankQueryInspector[] getDescendantBankTerms() {
        return (new org.osid.assessment.BankQueryInspector[0]);
    }


    /**
     *  Gets the DescendantBank column name.
     *
     * @return the column name
     */

    protected String getDescendantBankColumn() {
        return ("descendant_bank");
    }


    /**
     *  Tests if this bank supports the given record
     *  <code>Type</code>.
     *
     *  @param  bankRecordType a bank record type 
     *  @return <code>true</code> if the bankRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>bankRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type bankRecordType) {
        for (org.osid.assessment.records.BankQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(bankRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  bankRecordType the bank record type 
     *  @return the bank query record 
     *  @throws org.osid.NullArgumentException
     *          <code>bankRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bankRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.BankQueryRecord getBankQueryRecord(org.osid.type.Type bankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.BankQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(bankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bankRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  bankRecordType the bank record type 
     *  @return the bank query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>bankRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bankRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.BankQueryInspectorRecord getBankQueryInspectorRecord(org.osid.type.Type bankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.BankQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(bankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bankRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param bankRecordType the bank record type
     *  @return the bank search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>bankRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bankRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.BankSearchOrderRecord getBankSearchOrderRecord(org.osid.type.Type bankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.BankSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(bankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bankRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this bank. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param bankQueryRecord the bank query record
     *  @param bankQueryInspectorRecord the bank query inspector
     *         record
     *  @param bankSearchOrderRecord the bank search order record
     *  @param bankRecordType bank record type
     *  @throws org.osid.NullArgumentException
     *          <code>bankQueryRecord</code>,
     *          <code>bankQueryInspectorRecord</code>,
     *          <code>bankSearchOrderRecord</code> or
     *          <code>bankRecordTypebank</code> is
     *          <code>null</code>
     */
            
    protected void addBankRecords(org.osid.assessment.records.BankQueryRecord bankQueryRecord, 
                                      org.osid.assessment.records.BankQueryInspectorRecord bankQueryInspectorRecord, 
                                      org.osid.assessment.records.BankSearchOrderRecord bankSearchOrderRecord, 
                                      org.osid.type.Type bankRecordType) {

        addRecordType(bankRecordType);

        nullarg(bankQueryRecord, "bank query record");
        nullarg(bankQueryInspectorRecord, "bank query inspector record");
        nullarg(bankSearchOrderRecord, "bank search odrer record");

        this.queryRecords.add(bankQueryRecord);
        this.queryInspectorRecords.add(bankQueryInspectorRecord);
        this.searchOrderRecords.add(bankSearchOrderRecord);
        
        return;
    }
}
