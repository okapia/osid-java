//
// AbstractAssessmentPartSearchOdrer.java
//
//     Defines an AssessmentPartSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AssessmentPartSearchOrder}.
 */

public abstract class AbstractAssessmentPartSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.assessment.authoring.AssessmentPartSearchOrder {

    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord> records = new java.util.LinkedHashSet<>();

    private final OsidOperableSearchOrder operable = new OsidOperableSearchOrder();
    private final OsidContainableSearchOrder containable = new OsidContainableSearchOrder();


    /**
     *  Specifies a preference for ordering the result set by the
     *  active status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code  style} is {@code  
     *          null} 
     */

    @OSID @Override
    public void orderByActive(org.osid.SearchOrderStyle style) {
        this.operable.orderByActive(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  administratively enabled status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByEnabled(org.osid.SearchOrderStyle style) {
        this.operable.orderByEnabled(style);
        return;
    }        


    /**
     *  Specifies a preference for ordering the result set by the
     *  administratively disabled status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code  style} is
     *          {@code  null}
     */

    @OSID @Override
    public void orderByDisabled(org.osid.SearchOrderStyle style) {
        this.operable.orderByDisabled(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the results by the
     *  operational status.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code  style} is {@code  
     *          null} 
     */

    @OSID @Override
    public void orderByOperational(org.osid.SearchOrderStyle style) {
        this.operable.orderByOperational(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  sequestered flag.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */
    
    @OSID @Override
    public void orderBySequestered(org.osid.SearchOrderStyle style) {
        this.containable.orderBySequestered(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessment(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment search order is available. 
     *
     *  @return <code> true </code> if an assessment search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the assessment order. 
     *
     *  @return the assessment search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchOrder getAssessmentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the section. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySection(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the weight. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeight(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the allocated 
     *  time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatedTime(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  assessmentPartRecordType an assessment part record type 
     *  @return {@code true} if the assessmentPartRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentPartRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentPartRecordType) {
        for (org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  assessmentPartRecordType the assessment part record type 
     *  @return the assessment part search order record
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentPartRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(assessmentPartRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord getAssessmentPartSearchOrderRecord(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentPartRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentPartRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this assessment part. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentPartRecord the assessment part search odrer record
     *  @param assessmentPartRecordType assessment part record type
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentPartRecord} or
     *          {@code assessmentPartRecordTypeassessmentPart} is
     *          {@code null}
     */
            
    protected void addAssessmentPartRecord(org.osid.assessment.authoring.records.AssessmentPartSearchOrderRecord assessmentPartSearchOrderRecord, 
                                     org.osid.type.Type assessmentPartRecordType) {

        addRecordType(assessmentPartRecordType);
        this.records.add(assessmentPartSearchOrderRecord);
        
        return;
    }


    protected class OsidOperableSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidOperableSearchOrder
        implements org.osid.OsidOperableSearchOrder {
    }


    protected class OsidContainableSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableSearchOrder
        implements org.osid.OsidContainableSearchOrder {
    }
}
