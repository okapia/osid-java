//
// AbstractImmutableAuthorizationEnabler.java
//
//     Wraps a mutable AuthorizationEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authorization.rules.authorizationenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AuthorizationEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized AuthorizationEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying authorizationEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableAuthorizationEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.authorization.rules.AuthorizationEnabler {

    private final org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableAuthorizationEnabler</code>.
     *
     *  @param authorizationEnabler the authorization enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>authorizationEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAuthorizationEnabler(org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler) {
        super(authorizationEnabler);
        this.authorizationEnabler = authorizationEnabler;
        return;
    }


    /**
     *  Gets the authorization enabler record corresponding to the given 
     *  <code> AuthorizationEnabler </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> authorizationEnablerRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(authorizationEnablerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  authorizationEnablerRecordType the type of authorization 
     *          enabler record to retrieve 
     *  @return the authorization enabler record 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationEnablerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(authorizationEnablerRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.records.AuthorizationEnablerRecord getAuthorizationEnablerRecord(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.authorizationEnabler.getAuthorizationEnablerRecord(authorizationEnablerRecordType));
    }
}

