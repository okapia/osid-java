//
// AbstractObstacleLookupSession.java
//
//    A starter implementation framework for providing an Obstacle
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Obstacle
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getObstacles(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractObstacleLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.mapping.path.ObstacleLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();
    

    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>Obstacle</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObstacles() {
        return (true);
    }


    /**
     *  A complete view of the <code>Obstacle</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObstacleView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Obstacle</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObstacleView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include obstacles in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active obstacles are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveObstacleView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive obstacles are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusObstacleView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Obstacle</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Obstacle</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Obstacle</code> and
     *  retained for compatibility.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @param  obstacleId <code>Id</code> of the
     *          <code>Obstacle</code>
     *  @return the obstacle
     *  @throws org.osid.NotFoundException <code>obstacleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>obstacleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Obstacle getObstacle(org.osid.id.Id obstacleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.mapping.path.ObstacleList obstacles = getObstacles()) {
            while (obstacles.hasNext()) {
                org.osid.mapping.path.Obstacle obstacle = obstacles.getNextObstacle();
                if (obstacle.getId().equals(obstacleId)) {
                    return (obstacle);
                }
            }
        } 

        throw new org.osid.NotFoundException(obstacleId + " not found");
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  obstacles specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Obstacles</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getObstacles()</code>.
     *
     *  @param  obstacleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByIds(org.osid.id.IdList obstacleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.mapping.path.Obstacle> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = obstacleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getObstacle(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("obstacle " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.mapping.path.obstacle.LinkedObstacleList(ret));
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  obstacle genus <code>Type</code> which does not include
     *  obstacles of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getObstacles()</code>.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.obstacle.ObstacleGenusFilterList(getObstacles(), obstacleGenusType));
    }


    /**
     *  Gets an <code>ObstacleList</code> corresponding to the given
     *  obstacle genus <code>Type</code> and include any additional
     *  obstacles with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getObstacles()</code>.
     *
     *  @param  obstacleGenusType an obstacle genus type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByParentGenusType(org.osid.type.Type obstacleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getObstaclesByGenusType(obstacleGenusType));
    }


    /**
     *  Gets an <code>ObstacleList</code> containing the given
     *  obstacle record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getObstacles()</code>.
     *
     *  @param  obstacleRecordType an obstacle record type 
     *  @return the returned <code>Obstacle</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesByRecordType(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.obstacle.ObstacleRecordFilterList(getObstacles(), obstacleRecordType));
    }


    /**
     *  Gets an <code>ObstacleList</code> containing the given path. 
     *  
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session.
     *  
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles are
     *  returned.
     *
     *  @param  pathId a path <code>Id</code> 
     *  @return the returned <code>Obstacle</code> list 
     *  @throws org.osid.NullArgumentException <code>pathId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesForPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.mapping.path.obstacle.ObstacleFilterList(new PathFilter(pathId), getObstacles()));
    }        


    /**
     *  Gets an <code>ObstacleList</code> containing the given path
     *  between the given coordinates inclusive.
     *  
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session.
     *  
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles are
     *  returned.
     *
     *  This method should be implemented.
     *
     *  @param  pathId a path <code>Id</code> 
     *  @param  coordinate starting coordinate 
     *  @param  distance a distance from coordinate 
     *  @return the returned <code>Obstacle</code> list 
     *  @throws org.osid.NullArgumentException <code>pathId</code>,
     *          <code>coordinate </code> or <code>distance</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleList getObstaclesForPathAtCoordinate(org.osid.id.Id pathId, 
                                                                              org.osid.mapping.Coordinate coordinate, 
                                                                              org.osid.mapping.Distance distance)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getObstaclesForPath(pathId));
    }


    /**
     *  Gets all <code>Obstacles</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  obstacles or an error results. Otherwise, the returned list
     *  may contain only those obstacles that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, obstacles are returned that are currently
     *  active. In any status mode, active and inactive obstacles
     *  are returned.
     *
     *  @return a list of <code>Obstacles</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.mapping.path.ObstacleList getObstacles()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the obstacle list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of obstacles
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.mapping.path.ObstacleList filterObstaclesOnViews(org.osid.mapping.path.ObstacleList list)
        throws org.osid.OperationFailedException {

        org.osid.mapping.path.ObstacleList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.mapping.path.obstacle.ActiveObstacleFilterList(ret);
        }

        return (ret);
    }


    public static class PathFilter
        implements net.okapia.osid.jamocha.inline.filter.mapping.path.obstacle.ObstacleFilter {

        private final org.osid.id.Id pathId;

        
        /**
         *  Constructs a new <code>PathFilterList</code>.
         *
         *  @param pathId the path to filter
         *  @throws org.osid.NullArgumentException
         *          <code>pathId</code> is <code>null</code>
         */

        public PathFilter(org.osid.id.Id pathId) {
            nullarg(pathId, "path Id");
            this.pathId = pathId;
            return;
        }


        /**
         *  Used by the ObstacleFilterList to filter the obstacle list
         *  based on path.
         *
         *  @param obstacle the obstacle
         *  @return <code>true</code> to pass the obstacle,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.mapping.path.Obstacle obstacle) {
            return (obstacle.getPathId().equals(this.pathId));
        }
    }
}
