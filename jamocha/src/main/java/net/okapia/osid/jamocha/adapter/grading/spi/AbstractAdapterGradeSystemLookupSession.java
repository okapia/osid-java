//
// AbstractAdapterGradeSystemLookupSession.java
//
//    A GradeSystem lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A GradeSystem lookup session adapter.
 */

public abstract class AbstractAdapterGradeSystemLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.grading.GradeSystemLookupSession {

    private final org.osid.grading.GradeSystemLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterGradeSystemLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGradeSystemLookupSession(org.osid.grading.GradeSystemLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Gradebook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Gradebook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.session.getGradebookId());
    }


    /**
     *  Gets the {@code Gradebook} associated with this session.
     *
     *  @return the {@code Gradebook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGradebook());
    }


    /**
     *  Tests if this user can perform {@code GradeSystem} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupGradeSystems() {
        return (this.session.canLookupGradeSystems());
    }


    /**
     *  A complete view of the {@code GradeSystem} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradeSystemView() {
        this.session.useComparativeGradeSystemView();
        return;
    }


    /**
     *  A complete view of the {@code GradeSystem} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradeSystemView() {
        this.session.usePlenaryGradeSystemView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade systems in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.session.useFederatedGradebookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.session.useIsolatedGradebookView();
        return;
    }
    
     
    /**
     *  Gets the {@code GradeSystem} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code GradeSystem} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code GradeSystem} and
     *  retained for compatibility.
     *
     *  @param gradeSystemId {@code Id} of the {@code GradeSystem}
     *  @return the grade system
     *  @throws org.osid.NotFoundException {@code gradeSystemId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code gradeSystemId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystem(org.osid.id.Id gradeSystemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystem(gradeSystemId));
    }


    /**
     *  Gets the {@code GradeSystem} by a {@code Grade} {@code 
     *  Id.} 
     *
     *  @param  gradeId {@code Id} of a {@code Grade} 
     *  @return the grade system 
     *  @throws org.osid.NotFoundException {@code gradeId} not found 
     *  @throws org.osid.NullArgumentException {@code gradeId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGradeSystemByGrade(org.osid.id.Id gradeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGradeSystemByGrade(gradeId));
    }


    /**
     *  Gets a {@code GradeSystemList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradeSystems specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code GradeSystems} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  gradeSystemIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code GradeSystem} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByIds(org.osid.id.IdList gradeSystemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemsByIds(gradeSystemIds));
    }


    /**
     *  Gets a {@code GradeSystemList} corresponding to the given
     *  grade system genus {@code Type} which does not include
     *  grade systems of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemGenusType a gradeSystem genus type 
     *  @return the returned {@code GradeSystem} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByGenusType(org.osid.type.Type gradeSystemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemsByGenusType(gradeSystemGenusType));
    }


    /**
     *  Gets a {@code GradeSystemList} corresponding to the given
     *  grade system genus {@code Type} and include any additional
     *  grade systems with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemGenusType a gradeSystem genus type 
     *  @return the returned {@code GradeSystem} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByParentGenusType(org.osid.type.Type gradeSystemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemsByParentGenusType(gradeSystemGenusType));
    }


    /**
     *  Gets a {@code GradeSystemList} containing the given
     *  grade system record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradeSystemRecordType a gradeSystem record type 
     *  @return the returned {@code GradeSystem} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradeSystemRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystemsByRecordType(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystemsByRecordType(gradeSystemRecordType));
    }


    /**
     *  Gets all {@code GradeSystems}. 
     *
     *  In plenary mode, the returned list contains all known
     *  grade systems or an error results. Otherwise, the returned list
     *  may contain only those grade systems that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code GradeSystems} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradeSystems());
    }
}
