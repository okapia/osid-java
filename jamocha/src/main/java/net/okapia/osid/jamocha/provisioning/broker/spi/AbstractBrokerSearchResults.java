//
// AbstractBrokerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.broker.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBrokerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.BrokerSearchResults {

    private org.osid.provisioning.BrokerList brokers;
    private final org.osid.provisioning.BrokerQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.records.BrokerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBrokerSearchResults.
     *
     *  @param brokers the result set
     *  @param brokerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>brokers</code>
     *          or <code>brokerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBrokerSearchResults(org.osid.provisioning.BrokerList brokers,
                                            org.osid.provisioning.BrokerQueryInspector brokerQueryInspector) {
        nullarg(brokers, "brokers");
        nullarg(brokerQueryInspector, "broker query inspectpr");

        this.brokers = brokers;
        this.inspector = brokerQueryInspector;

        return;
    }


    /**
     *  Gets the broker list resulting from a search.
     *
     *  @return a broker list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.BrokerList getBrokers() {
        if (this.brokers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.BrokerList brokers = this.brokers;
        this.brokers = null;
	return (brokers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.BrokerQueryInspector getBrokerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  broker search record <code> Type. </code> This method must
     *  be used to retrieve a broker implementing the requested
     *  record.
     *
     *  @param brokerSearchRecordType a broker search 
     *         record type 
     *  @return the broker search
     *  @throws org.osid.NullArgumentException
     *          <code>brokerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(brokerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.BrokerSearchResultsRecord getBrokerSearchResultsRecord(org.osid.type.Type brokerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.records.BrokerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(brokerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(brokerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record broker search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBrokerRecord(org.osid.provisioning.records.BrokerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "broker record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
