//
// AbstractBrokerProcessorSearch.java
//
//     A template for making a BrokerProcessor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing broker processor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBrokerProcessorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.provisioning.rules.BrokerProcessorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.provisioning.rules.BrokerProcessorSearchOrder brokerProcessorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of broker processors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  brokerProcessorIds list of broker processors
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBrokerProcessors(org.osid.id.IdList brokerProcessorIds) {
        while (brokerProcessorIds.hasNext()) {
            try {
                this.ids.add(brokerProcessorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBrokerProcessors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of broker processor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBrokerProcessorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  brokerProcessorSearchOrder broker processor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>brokerProcessorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBrokerProcessorResults(org.osid.provisioning.rules.BrokerProcessorSearchOrder brokerProcessorSearchOrder) {
	this.brokerProcessorSearchOrder = brokerProcessorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.provisioning.rules.BrokerProcessorSearchOrder getBrokerProcessorSearchOrder() {
	return (this.brokerProcessorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given broker processor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a broker processor implementing the requested record.
     *
     *  @param brokerProcessorSearchRecordType a broker processor search record
     *         type
     *  @return the broker processor search record
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorSearchRecord getBrokerProcessorSearchRecord(org.osid.type.Type brokerProcessorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.provisioning.rules.records.BrokerProcessorSearchRecord record : this.records) {
            if (record.implementsRecordType(brokerProcessorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker processor search. 
     *
     *  @param brokerProcessorSearchRecord broker processor search record
     *  @param brokerProcessorSearchRecordType brokerProcessor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBrokerProcessorSearchRecord(org.osid.provisioning.rules.records.BrokerProcessorSearchRecord brokerProcessorSearchRecord, 
                                           org.osid.type.Type brokerProcessorSearchRecordType) {

        addRecordType(brokerProcessorSearchRecordType);
        this.records.add(brokerProcessorSearchRecord);        
        return;
    }
}
