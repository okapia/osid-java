//
// AbstractObstacleQueryInspector.java
//
//     A template for making an ObstacleQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.obstacle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for obstacles.
 */

public abstract class AbstractObstacleQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQueryInspector
    implements org.osid.mapping.path.ObstacleQueryInspector {

    private final java.util.Collection<org.osid.mapping.path.records.ObstacleQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the path <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPathIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQueryInspector[] getPathTerms() {
        return (new org.osid.mapping.path.PathQueryInspector[0]);
    }


    /**
     *  Gets the coordinate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CoordinateTerm[] getCoordinateTerms() {
        return (new org.osid.search.terms.CoordinateTerm[0]);
    }


    /**
     *  Gets the spatial unit inclusive query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.SpatialUnitTerm[] getContainingSpatialUnitTerms() {
        return (new org.osid.search.terms.SpatialUnitTerm[0]);
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given obstacle query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an obstacle implementing the requested record.
     *
     *  @param obstacleRecordType an obstacle record type
     *  @return the obstacle query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.ObstacleQueryInspectorRecord getObstacleQueryInspectorRecord(org.osid.type.Type obstacleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.ObstacleQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(obstacleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleRecordType + " is not supported");
    }


    /**
     *  Adds a record to this obstacle query. 
     *
     *  @param obstacleQueryInspectorRecord obstacle query inspector
     *         record
     *  @param obstacleRecordType obstacle record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObstacleQueryInspectorRecord(org.osid.mapping.path.records.ObstacleQueryInspectorRecord obstacleQueryInspectorRecord, 
                                                   org.osid.type.Type obstacleRecordType) {

        addRecordType(obstacleRecordType);
        nullarg(obstacleRecordType, "obstacle record type");
        this.records.add(obstacleQueryInspectorRecord);        
        return;
    }
}
