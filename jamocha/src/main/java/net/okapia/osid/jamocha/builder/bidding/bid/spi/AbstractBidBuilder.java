//
// AbstractBid.java
//
//     Defines a Bid builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.bid.spi;


/**
 *  Defines a <code>Bid</code> builder.
 */

public abstract class AbstractBidBuilder<T extends AbstractBidBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.bidding.bid.BidMiter bid;


    /**
     *  Constructs a new <code>AbstractBidBuilder</code>.
     *
     *  @param bid the bid to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractBidBuilder(net.okapia.osid.jamocha.builder.bidding.bid.BidMiter bid) {
        super(bid);
        this.bid = bid;
        return;
    }


    /**
     *  Builds the bid.
     *
     *  @return the new bid
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.bidding.Bid build() {
        (new net.okapia.osid.jamocha.builder.validator.bidding.bid.BidValidator(getValidations())).validate(this.bid);
        return (new net.okapia.osid.jamocha.builder.bidding.bid.ImmutableBid(this.bid));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the bid miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.bidding.bid.BidMiter getMiter() {
        return (this.bid);
    }


    /**
     *  Sets the auction.
     *
     *  @param auction an auction
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>auction</code> is <code>null</code>
     */

    public T auction(org.osid.bidding.Auction auction) {
        getMiter().setAuction(auction);
        return (self());
    }


    /**
     *  Sets the bidder.
     *
     *  @param bidder a bidder
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>bidder</code> is <code>null</code>
     */

    public T bidder(org.osid.resource.Resource bidder) {
        getMiter().setBidder(bidder);
        return (self());
    }


    /**
     *  Sets the bidding agent.
     *
     *  @param biddingAgent a bidding agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>biddingAgent</code> is <code>null</code>
     */

    public T biddingAgent(org.osid.authentication.Agent biddingAgent) {
        getMiter().setBiddingAgent(biddingAgent);
        return (self());
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    public T quantity(long quantity) {
        getMiter().setQuantity(quantity);
        return (self());
    }


    /**
     *  Sets the current bid.
     *
     *  @param bid a current bid
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>bid</code> is
     *          <code>null</code>
     */

    public T currentBid(org.osid.financials.Currency bid) {
        getMiter().setCurrentBid(bid);
        return (self());
    }


    /**
     *  Sets the maximum bid.
     *
     *  @param bid a maximum bid
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>bid</code> is
     *          <code>null</code>
     */

    public T maximumBid(org.osid.financials.Currency bid) {
        getMiter().setMaximumBid(bid);
        return (self());
    }


    /**
     *  Sets the winner flag.
     *
     *  @return the builder
     */

    public T winner() {
        getMiter().setWinner(true);
        return (self());
    }


    /**
     *  Unsets the winner flag.
     *
     *  @return the builder
     */

    public T loser() {
        getMiter().setWinner(false);
        return (self());
    }


    /**
     *  Sets the settlement amount.
     *
     *  @param amount a settlement amount
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T settlementAmount(org.osid.financials.Currency amount) {
        getMiter().setSettlementAmount(amount);
        return (self());
    }


    /**
     *  Adds a Bid record.
     *
     *  @param record a bid record
     *  @param recordType the type of bid record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.bidding.records.BidRecord record, org.osid.type.Type recordType) {
        getMiter().addBidRecord(record, recordType);
        return (self());
    }
}       


