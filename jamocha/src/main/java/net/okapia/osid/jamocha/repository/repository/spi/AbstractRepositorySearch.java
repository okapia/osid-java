//
// AbstractRepositorySearch.java
//
//     A template for making a Repository Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.repository.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing repository searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractRepositorySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.repository.RepositorySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.repository.records.RepositorySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.repository.RepositorySearchOrder repositorySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of repositories. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  repositoryIds list of repositories
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongRepositories(org.osid.id.IdList repositoryIds) {
        while (repositoryIds.hasNext()) {
            try {
                this.ids.add(repositoryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongRepositories</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of repository Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getRepositoryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  repositorySearchOrder repository search order 
     *  @throws org.osid.NullArgumentException
     *          <code>repositorySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>repositorySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderRepositoryResults(org.osid.repository.RepositorySearchOrder repositorySearchOrder) {
	this.repositorySearchOrder = repositorySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.repository.RepositorySearchOrder getRepositorySearchOrder() {
	return (this.repositorySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given repository search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a repository implementing the requested record.
     *
     *  @param repositorySearchRecordType a repository search record
     *         type
     *  @return the repository search record
     *  @throws org.osid.NullArgumentException
     *          <code>repositorySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(repositorySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.RepositorySearchRecord getRepositorySearchRecord(org.osid.type.Type repositorySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.repository.records.RepositorySearchRecord record : this.records) {
            if (record.implementsRecordType(repositorySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(repositorySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this repository search. 
     *
     *  @param repositorySearchRecord repository search record
     *  @param repositorySearchRecordType repository search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRepositorySearchRecord(org.osid.repository.records.RepositorySearchRecord repositorySearchRecord, 
                                           org.osid.type.Type repositorySearchRecordType) {

        addRecordType(repositorySearchRecordType);
        this.records.add(repositorySearchRecord);        
        return;
    }
}
