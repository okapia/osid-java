//
// AbstractControlBatchProxyManager.java
//
//     An adapter for a ControlBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ControlBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterControlBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.control.batch.ControlBatchProxyManager>
    implements org.osid.control.batch.ControlBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterControlBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterControlBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterControlBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterControlBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of devices is available. 
     *
     *  @return <code> true </code> if a device bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceBatchAdmin() {
        return (getAdapteeManager().supportsDeviceBatchAdmin());
    }


    /**
     *  Tests if bulk administration of controllers is available. 
     *
     *  @return <code> true </code> if a controller bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerBatchAdmin() {
        return (getAdapteeManager().supportsControllerBatchAdmin());
    }


    /**
     *  Tests if bulk administration of inputs is available. 
     *
     *  @return <code> true </code> if an input bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputBatchAdmin() {
        return (getAdapteeManager().supportsInputBatchAdmin());
    }


    /**
     *  Tests if bulk administration of setting is available. 
     *
     *  @return <code> true </code> if a setting bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingBatchAdmin() {
        return (getAdapteeManager().supportsSettingBatchAdmin());
    }


    /**
     *  Tests if bulk administration of scenes is available. 
     *
     *  @return <code> true </code> if a scene bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneBatchAdmin() {
        return (getAdapteeManager().supportsSceneBatchAdmin());
    }


    /**
     *  Tests if bulk administration of triggers is available. 
     *
     *  @return <code> true </code> if a trigger bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerBatchAdmin() {
        return (getAdapteeManager().supportsTriggerBatchAdmin());
    }


    /**
     *  Tests if bulk administration of action groups is available. 
     *
     *  @return <code> true </code> if an action group bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupBatchAdmin() {
        return (getAdapteeManager().supportsActionGroupBatchAdmin());
    }


    /**
     *  Tests if bulk administration of systems is available. 
     *
     *  @return <code> true </code> if a system bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemBatchAdmin() {
        return (getAdapteeManager().supportsSystemBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk device 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.DeviceBatchAdminSession getDeviceBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk device 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.DeviceBatchAdminSession getDeviceBatchAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceBatchAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  controller administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.ControllerBatchAdminSession getControllerBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  controller administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.ControllerBatchAdminSession getControllerBatchAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerBatchAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk input 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.InputBatchAdminSession getInputBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk input 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.InputBatchAdminSession getInputBatchAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputBatchAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk setting 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.SettingBatchAdminSession getSettingBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk setting 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.SettingBatchAdminSession getSettingBatchAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingBatchAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk scene 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.SceneBatchAdminSession getSceneBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk scene 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.SceneBatchAdminSession getSceneBatchAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneBatchAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk trigger 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.TriggerBatchAdminSession getTriggerBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk trigger 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.TriggerBatchAdminSession getTriggerBatchAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerBatchAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk action 
     *  group administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.batch.ActionGroupBatchAdminSession getActionGroupBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk action 
     *  group administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.ActionGroupBatchAdminSession getActionGroupBatchAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupBatchAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk system 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.SystemBatchAdminSession getSystemBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSystemBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
