//
// AbstractMutableProxy.java
//
//     Defines a mutable Proxy.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.proxy.proxy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Proxy</code>.
 */

public abstract class AbstractMutableProxy
    extends net.okapia.osid.jamocha.proxy.proxy.spi.AbstractProxy
    implements org.osid.proxy.Proxy,
               net.okapia.osid.jamocha.builder.proxy.proxy.ProxyMiter {


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this proxy. 
     *
     *  @param record proxy record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addProxyRecord(org.osid.proxy.records.ProxyRecord record, org.osid.type.Type recordType) {
        super.addProxyRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }

    
    /**
     *  Sets the authentication.
     *
     *  @param authentication the authentication
     *  @throws org.osid.NullArgumentException
     *          <code>authentication</code> is <code>null</code.
     */

    @Override
    public void setAuthentication(org.osid.authentication.process.Authentication authentication) {
        super.setAuthentication(authentication);
        return;
    }


    /**
     *  Sets the effective agent.
     *
     *  @param agent the agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code.
     */

    @Override
    public void setEffectiveAgent(org.osid.authentication.Agent agent) {
        super.setEffectiveAgent(agent);
        return;
    }

    
    /**
     *  Sets the effective date. 
     *
     *  @param  date a date 
     *  @param  rate the rate at which the clock should tick from the given 
     *          effective date. 0 is a clock that is fixed 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @Override
    public void setEffectiveDate(java.util.Date date, java.math.BigDecimal rate) {
        super.setEffectiveDate(date, rate);
        return;
    }

        
    /**
     *  Sets the locale.
     *
     *  @param locale the locale
     *  @throws org.osid.NullArgumentException <code>locale</code> is
     *          <code>null</code.
     */
    
    @Override
    public void setLocale(org.osid.locale.Locale locale) {
        super.setLocale(locale);
        return;
    }    


    /**
     *  Sets the <code> DisplayText </code> format <code>
     *  Type. </code>
     *
     *  @param formatType the format <code> Type </code>
     *  @throws org.osid.NullArgumentException <code>formatType</code>
     *          is <code>null</code>
     */

    @Override
    public void setFormatType(org.osid.type.Type formatType) {
        super.setFormatType(formatType);
        return;
    }
}

