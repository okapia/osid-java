//
// AbstractAuditEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.rules.auditenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuditEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inquiry.rules.AuditEnablerSearchResults {

    private org.osid.inquiry.rules.AuditEnablerList auditEnablers;
    private final org.osid.inquiry.rules.AuditEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.inquiry.rules.records.AuditEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuditEnablerSearchResults.
     *
     *  @param auditEnablers the result set
     *  @param auditEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>auditEnablers</code>
     *          or <code>auditEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuditEnablerSearchResults(org.osid.inquiry.rules.AuditEnablerList auditEnablers,
                                            org.osid.inquiry.rules.AuditEnablerQueryInspector auditEnablerQueryInspector) {
        nullarg(auditEnablers, "audit enablers");
        nullarg(auditEnablerQueryInspector, "audit enabler query inspectpr");

        this.auditEnablers = auditEnablers;
        this.inspector = auditEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the audit enabler list resulting from a search.
     *
     *  @return an audit enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inquiry.rules.AuditEnablerList getAuditEnablers() {
        if (this.auditEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inquiry.rules.AuditEnablerList auditEnablers = this.auditEnablers;
        this.auditEnablers = null;
	return (auditEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inquiry.rules.AuditEnablerQueryInspector getAuditEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  audit enabler search record <code> Type. </code> This method must
     *  be used to retrieve an auditEnabler implementing the requested
     *  record.
     *
     *  @param auditEnablerSearchRecordType an auditEnabler search 
     *         record type 
     *  @return the audit enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>auditEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(auditEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.rules.records.AuditEnablerSearchResultsRecord getAuditEnablerSearchResultsRecord(org.osid.type.Type auditEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inquiry.rules.records.AuditEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(auditEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(auditEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record audit enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuditEnablerRecord(org.osid.inquiry.rules.records.AuditEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "audit enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
