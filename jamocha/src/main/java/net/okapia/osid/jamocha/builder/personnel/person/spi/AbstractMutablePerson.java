//
// AbstractMutablePerson.java
//
//     Defines a mutable Person.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.person.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Person</code>.
 */

public abstract class AbstractMutablePerson
    extends net.okapia.osid.jamocha.personnel.person.spi.AbstractPerson
    implements org.osid.personnel.Person,
               net.okapia.osid.jamocha.builder.personnel.person.PersonMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this person. 
     *
     *  @param record person record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addPersonRecord(org.osid.personnel.records.PersonRecord record, org.osid.type.Type recordType) {
        super.addPersonRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this person.
     *
     *  @param displayName the name for this person
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this person.
     *
     *  @param description the description of this person
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the salutation.
     *
     *  @param salutation a salutation
     *  @throws org.osid.NullArgumentException <code>salutation</code>
     *          is <code>null</code>
     */

    @Override
    public void setSalutation(org.osid.locale.DisplayText salutation) {
        super.setSalutation(salutation);
        return;
    }


    /**
     *  Sets the given name.
     *
     *  @param givenName a given name
     *  @throws org.osid.NullArgumentException <code>givenName</code>
     *          is <code>null</code>
     */

    @Override
    public void setGivenName(org.osid.locale.DisplayText givenName) {
        super.setGivenName(givenName);
        return;
    }


    /**
     *  Sets the preferred name.
     *
     *  @param preferredName a preferred name
     *  @throws org.osid.NullArgumentException
     *          <code>preferredName</code> is <code>null</code>
     */

    @Override
    public void setPreferredName(org.osid.locale.DisplayText preferredName) {
        super.setPreferredName(preferredName);
        return;
    }


    /**
     *  Adds a forename alias.
     *
     *  @param forenameAlias a forename aliases
     *  @throws org.osid.NullArgumentException
     *          <code>forenameAlias</code> is <code>null</code>
     */

    @Override
    public void addForenameAlias(org.osid.locale.DisplayText forenameAlias) {
        super.addForenameAlias(forenameAlias);
        return;
    }


    /**
     *  Sets all the forename aliases.
     *
     *  @param forenameAliases a collection of forename aliases
     *  @throws org.osid.NullArgumentException
     *          <code>forenameAliases</code> is <code>null</code>
     */

    @Override
    public void setForenameAliases(java.util.Collection<org.osid.locale.DisplayText> forenameAliases) {
        super.setForenameAliases(forenameAliases);
        return;
    }


    /**
     *  Adds a middle name.
     *
     *  @param middleName a middle name
     *  @throws org.osid.NullArgumentException <code>middleName</code>
     *          is <code>null</code>
     */

    @Override
    public void addMiddleName(org.osid.locale.DisplayText middleName) {
        super.addMiddleName(middleName);
        return;
    }


    /**
     *  Sets all the middle names.
     *
     *  @param middleNames a collection of middle names
     *  @throws org.osid.NullArgumentException
     *          <code>middleNames</code> is <code>null</code>
     */

    @Override
    public void setMiddleNames(java.util.Collection<org.osid.locale.DisplayText> middleNames) {
        super.setMiddleNames(middleNames);
        return;
    }


    /**
     *  Sets the surname.
     *
     *  @param surname a surname
     *  @throws org.osid.NullArgumentException <code>surname</code> is
     *          <code>null</code>
     */

    @Override
    public void setSurname(org.osid.locale.DisplayText surname) {
        super.setSurname(surname);
        return;
    }


    /**
     *  Adds a surname alias.
     *
     *  @param surnameAlias a surname alias
     *  @throws org.osid.NullArgumentException
     *          <code>surnameAlias</code> is <code>null</code>
     */

    @Override
    public void addSurnameAlias(org.osid.locale.DisplayText surnameAlias) {
        super.addSurnameAlias(surnameAlias);
        return;
    }


    /**
     *  Sets all the surname aliases.
     *
     *  @param surnameAliases a collection of surname aliases
     *  @throws org.osid.NullArgumentException
     *          <code>surnameAliases</code> is <code>null</code>
     */

    @Override
    public void setSurnameAliases(java.util.Collection<org.osid.locale.DisplayText> surnameAliases) {
        super.setSurnameAliases(surnameAliases);
        return;
    }


    /**
     *  Sets the generation qualifier.
     *
     *  @param qualifier a generation qualifier
     *  @throws org.osid.NullArgumentException <code>qualifier</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenerationQualifier(org.osid.locale.DisplayText qualifier) {
        super.setGenerationQualifier(qualifier);
        return;
    }


    /**
     *  Sets the qualification suffix.
     *
     *  @param suffix a qualification suffix
     *  @throws org.osid.NullArgumentException <code>suffix</code> is
     *          <code>null</code>
     */

    @Override
    public void setQualificationSuffix(org.osid.locale.DisplayText suffix) {
        super.setQualificationSuffix(suffix);
        return;
    }


    /**
     *  Sets the birth date.
     *
     *  @param date a birth date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setBirthDate(org.osid.calendaring.DateTime date) {
        super.setBirthDate(date);
        return;
    }


    /**
     *  Sets the death date.
     *
     *  @param date a death date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setDeathDate(org.osid.calendaring.DateTime date) {
        super.setDeathDate(date);
        return;
    }


    /**
     *  Sets the institutional identifier.
     *
     *  @param identifier an institutional identifier
     *  @throws org.osid.NullArgumentException <code>identifier</code>
     *          is <code>null</code>
     */

    @Override
    public void setInstitutionalIdentifier(String identifier) {
        super.setInstitutionalIdentifier(identifier);
        return;
    }
}

