//
// AbstractCourseOfferingSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.courseoffering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCourseOfferingSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.CourseOfferingSearchResults {

    private org.osid.course.CourseOfferingList courseOfferings;
    private final org.osid.course.CourseOfferingQueryInspector inspector;
    private final java.util.Collection<org.osid.course.records.CourseOfferingSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCourseOfferingSearchResults.
     *
     *  @param courseOfferings the result set
     *  @param courseOfferingQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>courseOfferings</code>
     *          or <code>courseOfferingQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCourseOfferingSearchResults(org.osid.course.CourseOfferingList courseOfferings,
                                            org.osid.course.CourseOfferingQueryInspector courseOfferingQueryInspector) {
        nullarg(courseOfferings, "course offerings");
        nullarg(courseOfferingQueryInspector, "course offering query inspectpr");

        this.courseOfferings = courseOfferings;
        this.inspector = courseOfferingQueryInspector;

        return;
    }


    /**
     *  Gets the course offering list resulting from a search.
     *
     *  @return a course offering list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferings() {
        if (this.courseOfferings == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.CourseOfferingList courseOfferings = this.courseOfferings;
        this.courseOfferings = null;
	return (courseOfferings);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.CourseOfferingQueryInspector getCourseOfferingQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  course offering search record <code> Type. </code> This method must
     *  be used to retrieve a courseOffering implementing the requested
     *  record.
     *
     *  @param courseOfferingSearchRecordType a courseOffering search 
     *         record type 
     *  @return the course offering search
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(courseOfferingSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseOfferingSearchResultsRecord getCourseOfferingSearchResultsRecord(org.osid.type.Type courseOfferingSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.records.CourseOfferingSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(courseOfferingSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(courseOfferingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record course offering search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCourseOfferingRecord(org.osid.course.records.CourseOfferingSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "course offering record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
