//
// ConvocationMiter.java
//
//     Defines a Convocation miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recognition.convocation;


/**
 *  Defines a <code>Convocation</code> miter for use with the builders.
 */

public interface ConvocationMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidGovernatorMiter,
            org.osid.recognition.Convocation {


    /**
     *  Adds an award.
     *
     *  @param award an award
     *  @throws org.osid.NullArgumentException <code>award</code> is
     *          <code>null</code>
     */

    public void addAward(org.osid.recognition.Award award);


    /**
     *  Sets all the awards.
     *
     *  @param awards a collection of awards
     *  @throws org.osid.NullArgumentException <code>awards</code> is
     *          <code>null</code>
     */

    public void setAwards(java.util.Collection<org.osid.recognition.Award> awards);


    /**
     *  Sets the time period.
     *
     *  @param period a time period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public void setTimePeriod(org.osid.calendaring.TimePeriod period);


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a Convocation record.
     *
     *  @param record a convocation record
     *  @param recordType the type of convocation record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addConvocationRecord(org.osid.recognition.records.ConvocationRecord record, org.osid.type.Type recordType);
}       


