//
// AbstractProgramEntry.java
//
//     Defines a ProgramEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.programentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ProgramEntry</code>.
 */

public abstract class AbstractProgramEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.chronicle.ProgramEntry {

    private org.osid.resource.Resource student;
    private org.osid.course.program.Program program;
    private org.osid.calendaring.DateTime admissionDate;

    private boolean complete = false;
    private boolean hasEnrollments = false;

    private org.osid.course.Term term;
    private org.osid.grading.GradeSystem creditScale;
    private java.math.BigDecimal creditsEarned;
    private org.osid.grading.GradeSystem gpaScale;
    private java.math.BigDecimal gpa;

    private final java.util.Collection<org.osid.course.program.Enrollment> enrollments = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.chronicle.records.ProgramEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.student.getId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.student);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    protected void setStudent(org.osid.resource.Resource student) {
        nullarg(student, "student");
        this.student = student;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.program.getId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.program);
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException
     *          <code>program</code> is <code>null</code>
     */

    protected void setProgram(org.osid.course.program.Program program) {
        nullarg(program, "program");
        this.program = program;
        return;
    }


    /**
     *  Gets the date in which the student was admitted. 
     *
     *  @return the admission date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getAdmissionDate() {
        return (this.admissionDate);
    }


    /**
     *  Sets the admission date.
     *
     *  @param date an admission date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setAdmissionDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "admission date");
        this.admissionDate = date;
        return;
    }


    /**
     *  Tests if the program has been completed. If this entry is for
     *  summary information an a past term, <code> isComplete()
     *  </code> may be <code> true. </code>
     *
     *  @return <code> true </code> if the program has been completed,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.complete);
    }


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if the program has been
     *          completed, <code> false </code> otherwise
     */

    protected void setComplete(boolean complete) {
        this.complete = complete;
        return;
    }


    /**
     *  Tests if this entry is a progression entry applying to a
     *  single term.  A program entry may provide summary information
     *  for the entire duration or for a single term. If <code>
     *  isForTerm() </code> is <code> false, </code> this entry
     *  applies to the entire enrollement period.
     *
     *  @return <code> true </code> if the program has a term, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isForTerm() {
        return (this.term != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term. </code> 
     *
     *  @return the term <code> Id </code> 
     *  @throws org.osid.IllegalStateException
     *          <code>isForTerm()</code> is <code>false</code>
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        if (!isForTerm()) {
            throw new org.osid.IllegalStateException("isForTerm() is false");
        }

        return (this.term.getId());
    }


    /**
     *  Gets the <code> Term. </code> 
     *
     *  @return the term 
     *  @throws org.osid.IllegalStateException
     *          <code>isForTerm()</code> is <code>false</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        if (!isForTerm()) {
            throw new org.osid.IllegalStateException("isForTerm() is false");
        }

        return (this.term);
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException
     *          <code>term</code> is <code>null</code>
     */

    protected void setTerm(org.osid.course.Term term) {
        nullarg(term, "term");
        this.term = term;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreditScaleId() {
        return (this.creditScale.getId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getCreditScale()
        throws org.osid.OperationFailedException {

        return (this.creditScale);
    }


    /**
     *  Sets the credit scale.
     *
     *  @param creditScale a credit scale
     *  @throws org.osid.NullArgumentException
     *          <code>creditScale</code> is <code>null</code>
     */

    protected void setCreditScale(org.osid.grading.GradeSystem creditScale) {
        nullarg(creditScale, "credit scale");
        this.creditScale = creditScale;
        return;
    }


    /**
     *  Gets the number of credits earned in this program or earned within the 
     *  included term. 
     *
     *  @return the credits earned 
     */

    @OSID @Override
    public java.math.BigDecimal getCreditsEarned() {
        return (this.creditsEarned);
    }


    /**
     *  Sets the credits earned.
     *
     *  @param credits a credits earned
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    protected void setCreditsEarned(java.math.BigDecimal credits) {
        nullarg(credits, "credits");
        this.creditsEarned = credits;
        return;
    }


    /**
     *  Tests if a cumulative GPA in this program of the GPA for the included 
     *  term. 
     *
     *  @return <code> true </code> if a GPA is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasGPA() {
        return ((this.gpa != null) && (this.gpaScale != null));
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasGPA() </code>
     *          is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getGPAScaleId() {
        if (!hasGPA()) {
            throw new org.osid.IllegalStateException("hasGPA() is false");
        }

        return (this.gpaScale.getId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> hasGPA() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getGPAScale()
        throws org.osid.OperationFailedException {

        if (!hasGPA()) {
            throw new org.osid.IllegalStateException("hasGPA() is false");
        }

        return (this.gpaScale);
    }


    /**
     *  Sets the gpa scale.
     *
     *  @param gpaScale a gpa scale
     *  @throws org.osid.NullArgumentException
     *          <code>gpaScale</code> is <code>null</code>
     */

    protected void setGPAScale(org.osid.grading.GradeSystem gpaScale) {
        nullarg(gpaScale, "gpa scale");
        this.gpaScale = gpaScale;
        return;
    }


    /**
     *  Gets the cumulative GPA in this porgram or within the included term. 
     *
     *  @return the GPA 
     *  @throws org.osid.IllegalStateException <code> hasGPA() </code>
     *          is <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal getGPA() {
        if (!hasGPA()) {
            throw new org.osid.IllegalStateException("hasGPA() is false");
        }

        return (this.gpa);
    }


    /**
     *  Sets the gpa.
     *
     *  @param gpa a gpa
     *  @throws org.osid.NullArgumentException
     *          <code>gpa</code> is <code>null</code>
     */

    protected void setGPA(java.math.BigDecimal gpa) {
        nullarg(gpa, "gpa");
        this.gpa = gpa;
        return;
    }


    /**
     *  Tests if <code> Enrollments </code> are available. 
     *
     *  @return <code> true </code> if enrollments are available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasEnrollments() {
        return (this.hasEnrollments);
    }


    /**
     *  Gets the <code> Ids </code> of the <code> Enrollments. </code> 
     *
     *  @return the enrollment <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasEnrollments() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getEnrollmentIds() {
        if (!hasEnrollments()) {
            throw new org.osid.IllegalStateException("hasEnrollments() is false");
        }

        try {
            org.osid.course.program.EnrollmentList enrollments = getEnrollments();
            return (new net.okapia.osid.jamocha.adapter.converter.course.program.enrollment.EnrollmentToIdList(enrollments));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> Enrollments. </code> 
     *
     *  @return the enrollments 
     *  @throws org.osid.IllegalStateException <code> hasEnrollments() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentList getEnrollments()
        throws org.osid.OperationFailedException {

        if (!hasEnrollments()) {
            throw new org.osid.IllegalStateException("hasEnrollments() is false");
        }

        return (new net.okapia.osid.jamocha.course.program.enrollment.ArrayEnrollmentList(this.enrollments));
    }


    /**
     *  Adds an enrollment.
     *
     *  @param enrollment an enrollment
     *  @throws org.osid.NullArgumentException <code>enrollment</code>
     *          is <code>null</code>
     */

    protected void addEnrollment(org.osid.course.program.Enrollment enrollment) {
        nullarg(enrollment, "enrollment");

        this.enrollments.add(enrollment);
        this.hasEnrollments = true;

        return;
    }


    /**
     *  Sets all the enrollments.
     *
     *  @param enrollments a collection of enrollments
     *  @throws org.osid.NullArgumentException
     *          <code>enrollments</code> is <code>null</code>
     */

    protected void setEnrollments(java.util.Collection<org.osid.course.program.Enrollment> enrollments) {
        nullarg(enrollments, "enrollments");

        this.enrollments.clear();
        this.enrollments.addAll(enrollments);
        this.hasEnrollments = true;

        return;
    }


    /**
     *  Tests if this programEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  programEntryRecordType a program entry record type 
     *  @return <code>true</code> if the programEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type programEntryRecordType) {
        for (org.osid.course.chronicle.records.ProgramEntryRecord record : this.records) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ProgramEntry</code> record <code>Type</code>.
     *
     *  @param  programEntryRecordType the program entry record type 
     *  @return the program entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntryRecord getProgramEntryRecord(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.ProgramEntryRecord record : this.records) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param programEntryRecord the program entry record
     *  @param programEntryRecordType program entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>programEntryRecord</code> or
     *          <code>programEntryRecordTypeprogramEntry</code> is
     *          <code>null</code>
     */
            
    protected void addProgramEntryRecord(org.osid.course.chronicle.records.ProgramEntryRecord programEntryRecord, 
                                         org.osid.type.Type programEntryRecordType) {

        nullarg(programEntryRecord, "program entry record");
        addRecordType(programEntryRecordType);
        this.records.add(programEntryRecord);
        
        return;
    }
}
