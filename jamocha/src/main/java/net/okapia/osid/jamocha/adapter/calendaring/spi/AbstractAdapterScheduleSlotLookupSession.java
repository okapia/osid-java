//
// AbstractAdapterScheduleSlotLookupSession.java
//
//    A ScheduleSlot lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ScheduleSlot lookup session adapter.
 */

public abstract class AbstractAdapterScheduleSlotLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.ScheduleSlotLookupSession {

    private final org.osid.calendaring.ScheduleSlotLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterScheduleSlotLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterScheduleSlotLookupSession(org.osid.calendaring.ScheduleSlotLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code ScheduleSlot} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupScheduleSlots() {
        return (this.session.canLookupScheduleSlots());
    }


    /**
     *  A complete view of the {@code ScheduleSlot} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeScheduleSlotView() {
        this.session.useComparativeScheduleSlotView();
        return;
    }


    /**
     *  A complete view of the {@code ScheduleSlot} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryScheduleSlotView() {
        this.session.usePlenaryScheduleSlotView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedule slots in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  schedule slots.
     */

    @OSID @Override
    public void useSequesteredScheduleSlotView() {
        this.session.useSequesteredScheduleSlotView();
        return;
    }


    /**
     *  All schedule slots are returned including sequestered schedule slots.
     */

    @OSID @Override
    public void useUnsequesteredScheduleSlotView() {
        this.session.useUnsequesteredScheduleSlotView();
        return;
    }

     
    /**
     *  Gets the {@code ScheduleSlot} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ScheduleSlot} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ScheduleSlot} and
     *  retained for compatibility.
     *
     *  @param scheduleSlotId {@code Id} of the {@code ScheduleSlot}
     *  @return the schedule slot
     *  @throws org.osid.NotFoundException {@code scheduleSlotId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code scheduleSlotId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlot getScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScheduleSlot(scheduleSlotId));
    }


    /**
     *  Gets a {@code ScheduleSlotList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  scheduleSlots specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ScheduleSlots} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  scheduleSlotIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ScheduleSlot} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleSlotIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByIds(org.osid.id.IdList scheduleSlotIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScheduleSlotsByIds(scheduleSlotIds));
    }


    /**
     *  Gets a {@code ScheduleSlotList} corresponding to the given
     *  schedule slot genus {@code Type} which does not include
     *  schedule slots of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotGenusType a scheduleSlot genus type 
     *  @return the returned {@code ScheduleSlot} list
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleSlotGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScheduleSlotsByGenusType(scheduleSlotGenusType));
    }


    /**
     *  Gets a {@code ScheduleSlotList} corresponding to the given
     *  schedule slot genus {@code Type} and include any additional
     *  schedule slots with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotGenusType a scheduleSlot genus type 
     *  @return the returned {@code ScheduleSlot} list
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleSlotGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByParentGenusType(org.osid.type.Type scheduleSlotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScheduleSlotsByParentGenusType(scheduleSlotGenusType));
    }


    /**
     *  Gets a {@code ScheduleSlotList} containing the given
     *  schedule slot record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleSlotRecordType a scheduleSlot record type 
     *  @return the returned {@code ScheduleSlot} list
     *  @throws org.osid.NullArgumentException
     *          {@code scheduleSlotRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByRecordType(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScheduleSlotsByRecordType(scheduleSlotRecordType));
    }


    /**
     *  Gets a {@code ScheduleSlotList} containing the given set of
     *  weekdays.
     *  
     *  In plenary mode, the returned list contains all known schedule
     *  slots or an error results. Otherwise, the returned list may
     *  contain only those schedule slots that are accessible through
     *  this session.
     *  
     *  In sequestered mode, no sequestered schedule slots are
     *  returned. In unsequestered mode, all schedule slots are
     *  returned.
     *
     *  @param  weekdays a set of weekdays 
     *  @return the returned {@code ScheduleSlot} list 
     *  @throws org.osid.InvalidArgumentException a {@code weekday} is 
     *          negative 
     *  @throws org.osid.NullArgumentException {@code weekdays} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByWeekdays(long[] weekdays)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScheduleSlotsByWeekdays(weekdays));
    }


    /**
     *  Gets a {@code ScheduleSlotList} matching the given time.
     *  
     *  In plenary mode, the returned list contains all known schedule
     *  slots or an error results. Otherwise, the returned list may
     *  contain only those schedule slots that are accessible through
     *  this session.
     *  
     *  In sequestered mode, no sequestered schedule slots are returned. In 
     *  unsequestered mode, all schedule slots are returned. 
     *
     *  @param  time a time 
     *  @return the returned {@code ScheduleSlot} list 
     *  @throws org.osid.NullArgumentException {@code time} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlotsByTime(org.osid.calendaring.Time time)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getScheduleSlotsByTime(time));
    }


    /**
     *  Gets all {@code ScheduleSlots}. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedule slots or an error results. Otherwise, the returned list
     *  may contain only those schedule slots that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code ScheduleSlots} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotList getScheduleSlots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getScheduleSlots());
    }
}
