//
// AbstractAssetContent.java
//
//     Defines an AssetContent.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.assetcontent.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AssetContent</code>.
 */

public abstract class AbstractAssetContent
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.repository.AssetContent {

    private org.osid.repository.Asset asset;
    private long dataLength = -1;
    private String url;
    
    private java.nio.ByteBuffer data;

    private final java.util.Collection<org.osid.type.Type> accessibilityTypes = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.records.AssetContentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Asset Id </code> corresponding to this content. 
     *
     *  @return the asset <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssetId() {
        return (this.asset.getId());
    }


    /**
     *  Gets the <code> Asset </code> corresponding to this content. 
     *
     *  @return the asset 
     */

    @OSID @Override
    public org.osid.repository.Asset getAsset() {
        return (this.asset);
    }


    /**
     *  Sets the asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException
     *          <code>asset</code> is <code>null</code>
     */

    protected void setAsset(org.osid.repository.Asset asset) {
        nullarg(asset, "asset");
        this.asset = asset;
        return;
    }


    /**
     *  Gets the accessibility types associated with this content. 
     *
     *  @return list of content accessibility types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAccessibilityTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.accessibilityTypes));
    }


    /**
     *  Adds an accessibility type.
     *
     *  @param accessibilityType an accessibility type
     *  @throws org.osid.NullArgumentException
     *          <code>accessibilityType</code> is <code>null</code>
     */

    protected void addAccessibilityType(org.osid.type.Type accessibilityType) {
        nullarg(accessibilityType, "accessibility type");
        this.accessibilityTypes.add(accessibilityType);
        return;
    }


    /**
     *  Sets all the accessibility types.
     *
     *  @param accessibilityTypes a collection of accessibility types
     *  @throws org.osid.NullArgumentException
     *          <code>accessibilityTypes</code> is <code>null</code>
     */

    protected void setAccessibilityTypes(java.util.Collection<org.osid.type.Type> accessibilityTypes) {
        nullarg(accessibilityTypes, "accessibility types");
        this.accessibilityTypes.clear();
        this.accessibilityTypes.addAll(accessibilityTypes);
        return;
    }


    /**
     *  Tests if a data length is available. 
     *
     *  @return <code> true </code> if a length is available for this
     *          content, <code> false </code> otherwise.
     */

    @OSID @Override
    public boolean hasDataLength() {
        if (this.dataLength >= 0) {
            return (true);
        } else if (this.data != null) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Gets the length of the data represented by this content in
     *  bytes.
     *
     *  @return the length of the data stream 
     *  @throws org.osid.IllegalStateException <code> hasDataLength() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public long getDataLength() {
        if (!hasDataLength()) {
            throw new org.osid.IllegalStateException("hasDataLength() is false");
        }

        if (this.dataLength >= 0) {
            return (this.dataLength);
        } else {
            return (this.data.limit());
        }
    }


    /**
     *  Sets the data length. This overrides the length of the data
     *  buffer.
     *
     *  @param length a data length
     *  @throws org.osid.InvalidArgumentException <code>length</code>
     *          is negative
     */

    protected void setDataLength(long length) {
        cardinalarg(length, "data length");
        this.dataLength = length;
        return;
    }


    /**
     *  Tests if a URL is associated with this content. 
     *
     *  @return <code> true </code> if a URL is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasURL() {
        return (this.url != null);
    }


    /**
     *  Gets the asset content data. 
     *
     *  @return the length of the content data 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.transport.DataInputStream getData()
        throws org.osid.OperationFailedException {
        
        return (new net.okapia.osid.torrefacto.transport.ByteBufferDataInputStream(this.data));
    }


    /**
     *  Sets the asset content data.
     *
     *  @param data a flipped byte buffer
     *  @throws org.osid.NullArgumentException <code>data</code> is
     *          <code>null</code>
     */

    protected void setData(java.nio.ByteBuffer data) {
        nullarg(data, "data");
        this.data = data;
        return;
    }


    /**
     *  Gets the URL associated with this content for web-based retrieval. 
     *
     *  @return the url for this data 
     *  @throws org.osid.IllegalStateException <code> hasURL() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public String getURL() {
        if (!hasURL()) {
            throw new org.osid.IllegalStateException("hasURL() is false");
        }

        return (this.url);
    }


    /**
     *  Sets the url.
     *
     *  @param url a url
     *  @throws org.osid.NullArgumentException
     *          <code>url</code> is <code>null</code>
     */

    protected void setURL(String url) {
        nullarg(url, "url");
        this.url = url;
        return;
    }


    /**
     *  Tests if this assetContent supports the given record
     *  <code>Type</code>.
     *
     *  @param  assetContentRecordType an asset content record type 
     *  @return <code>true</code> if the assetContentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assetContentRecordType) {
        for (org.osid.repository.records.AssetContentRecord record : this.records) {
            if (record.implementsRecordType(assetContentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AssetContent</code> record <code>Type</code>.
     *
     *  @param  assetContentRecordType the asset content record type 
     *  @return the asset content record 
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assetContentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.AssetContentRecord getAssetContentRecord(org.osid.type.Type assetContentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.AssetContentRecord record : this.records) {
            if (record.implementsRecordType(assetContentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assetContentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this asset content. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assetContentRecord the asset content record
     *  @param assetContentRecordType asset content record type
     *  @throws org.osid.NullArgumentException
     *          <code>assetContentRecord</code> or
     *          <code>assetContentRecordTypeassetContent</code> is
     *          <code>null</code>
     */
            
    protected void addAssetContentRecord(org.osid.repository.records.AssetContentRecord assetContentRecord, 
                                         org.osid.type.Type assetContentRecordType) {

        nullarg(assetContentRecord, "asset content record");
        addRecordType(assetContentRecordType);
        this.records.add(assetContentRecord);
        
        return;
    }
}
