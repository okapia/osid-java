//
// AbstractJournalEntryNotificationSession.java
//
//     A template for making JournalEntryNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code JournalEntry} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code JournalEntry} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for journal entry entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractJournalEntryNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.journaling.JournalEntryNotificationSession {

    private boolean federated = false;
    private org.osid.journaling.Journal journal = new net.okapia.osid.jamocha.nil.journaling.journal.UnknownJournal();


    /**
     *  Gets the {@code Journal/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Journal Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.journal.getId());
    }

    
    /**
     *  Gets the {@code Journal} associated with this session.
     *
     *  @return the {@code Journal} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.journal);
    }


    /**
     *  Sets the {@code Journal}.
     *
     *  @param journal the journal for this session
     *  @throws org.osid.NullArgumentException {@code journal}
     *          is {@code null}
     */

    protected void setJournal(org.osid.journaling.Journal journal) {
        nullarg(journal, "journal");
        this.journal = journal;
        return;
    }


    /**
     *  Tests if this user can register for {@code JournalEntry}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForJournalEntryNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeJournalEntryNotification() </code>.
     */

    @OSID @Override
    public void reliableJournalEntryNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableJournalEntryNotifications() {
        return;
    }


    /**
     *  Acknowledge a journal entry notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeJournalEntryNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for entries in journals which
     *  are children of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this journal only.
     */

    @OSID @Override
    public void useIsolatedJournalView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new journal entries. {@code
     *  JournalEntryReceiver.newJournalEntry()} is invoked when a new
     *  {@code JournalEntry} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewJournalEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new journal entries for the
     *  given branch {@code Id}. {@code
     *  JournalEntryReceiver.newJournalEntry()} is invoked when a new
     *  {@code JournalEntry} is created.
     *
     *  @param  branchId the {@code Id} of the branch to monitor
     *  @throws org.osid.NullArgumentException {@code branchId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewJournalEntriesForBranch(org.osid.id.Id branchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of new journal entries for the
     *  given source {@code Id}. {@code
     *  JournalEntryReceiver.newJournalEntry()} is invoked when a new
     *  {@code JournalEntry} is created.
     *
     *  @param  sourceId the {@code Id} of the source to monitor 
     *  @throws org.osid.NullArgumentException {@code sourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewJournalEntriesForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new journal entries for the
     *  given resource {@code Id.} {@code
     *  JournalEntryReceiver.newJournalEntry()} is invoked when a new
     *  {@code JournalEntry} is created.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor 
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewJournalEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated journal entries. {@code
     *  JournalEntryReceiver.changedJournalEntry()} is invoked when a
     *  journal entry is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedJournalEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated journalEntries for the
     *  given branch {@code Id}. {@code
     *  JournalEntryReceiver.changedJournalEntry()} is invoked when a
     *  {@code JournalEntry} in this journal is changed.
     *
     *  @param  branchId the {@code Id} of the branch to monitor
     *  @throws org.osid.NullArgumentException {@code branchId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedJournalEntriesForBranch(org.osid.id.Id branchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed journal entries for the
     *  given source {@code Id.} {@code
     *  JournalEntryReceiver.changedJournalEntry()} is invoked when a
     *  {@code JournalEntry} for the source is changed.
     *
     *  @param  sourceId the {@code Id} of the source to monitor 
     *  @throws org.osid.NullArgumentException {@code sourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedJournalEntriesForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed journal entries for the
     *  given resource {@code Id.} {@code
     *  JournalEntryReceiver.changedJournalEntry()} is invoked when a
     *  {@code JournalEntry} for the source is changed.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor 
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedJournalEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated journal entry. {@code
     *  JournalEntryReceiver.changedJournalEntry()} is invoked when
     *  the specified journal entry is changed.
     *
     *  @param journalEntryId the {@code Id} of the {@code JournalEntry} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code journalEntryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedJournalEntry(org.osid.id.Id journalEntryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted journal entries. {@code
     *  JournalEntryReceiver.deletedJournalEntry()} is invoked when a
     *  journal entry is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedJournalEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted journalEntries for the
     *  given branch {@code Id}. {@code
     *  JournalEntryReceiver.deletedJournalEntry()} is invoked when a
     *  {@code JournalEntry} is deleted or removed from this journal.
     *
     *  @param  branchId the {@code Id} of the branch to monitor
     *  @throws org.osid.NullArgumentException {@code branchId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedJournalEntriesForBranch(org.osid.id.Id branchId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted journal entries for the
     *  given source {@code Id.} {@code
     *  JournalEntryReceiver.deletedJournalEntry()} is invoked when a
     *  {@code JournalEntry} for the source is deleted.
     *
     *  @param  sourceId the {@code Id} of the source to monitor 
     *  @throws org.osid.NullArgumentException {@code sourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedJournalEntriesForSource(org.osid.id.Id sourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted journal entries for the
     *  given resource {@code Id.} {@code
     *  JournalEntryReceiver.deletedJournalEntry()} is invoked when a
     *  {@code JournalEntry} for the source is deleted.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor 
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedJournalEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted journal entry. {@code
     *  JournalEntryReceiver.deletedJournalEntry()} is invoked when
     *  the specified journal entry is deleted.
     *
     *  @param journalEntryId the {@code Id} of the
     *          {@code JournalEntry} to monitor
     *  @throws org.osid.NullArgumentException {@code journalEntryId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedJournalEntry(org.osid.id.Id journalEntryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
