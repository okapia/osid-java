//
// AbstractSearchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.search.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractSearchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.search.SearchManager,
               org.osid.search.SearchProxyManager {

    private final Types queryRecordTypes                   = new TypeRefSet();
    private final Types searchRecordTypes                  = new TypeRefSet();
    private final Types engineRecordTypes                  = new TypeRefSet();
    private final Types engineSearchRecordTypes            = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractSearchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractSearchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any engine federation is exposed. Federation is exposed when 
     *  a specific engine may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  engines appears as a single engine. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if search is supported. 
     *
     *  @return <code> true </code> if search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine lookup service. 
     *
     *  @return <code> true </code> if engine lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineLookup() {
        return (false);
    }


    /**
     *  Tests if querying for engines is available. 
     *
     *  @return <code> true </code> if engine query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Tests if searching for engines is available. 
     *
     *  @return <code> true </code> if engine search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine administrative service for 
     *  creating and deleting engines. 
     *
     *  @return <code> true </code> if engine administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine notification service. 
     *
     *  @return <code> true </code> if engine notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine hierarchy traversal service. 
     *
     *  @return <code> true </code> if engine hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine hierarchy design service. 
     *
     *  @return <code> true </code> if engine hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineHierarchyDesign() {
        return (false);
    }


    /**
     *  Gets the supported query record types. 
     *
     *  @return a list containing the supported query record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given query record type is supported. 
     *
     *  @param  queryRecordType a <code> Type </code> indicating a query 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueryRecordType(org.osid.type.Type queryRecordType) {
        return (this.queryRecordTypes.contains(queryRecordType));
    }


    /**
     *  Adds support for a query record type.
     *
     *  @param queryRecordType a query record type
     *  @throws org.osid.NullArgumentException
     *  <code>queryRecordType</code> is <code>null</code>
     */

    protected void addQueryRecordType(org.osid.type.Type queryRecordType) {
        this.queryRecordTypes.add(queryRecordType);
        return;
    }


    /**
     *  Removes support for a query record type.
     *
     *  @param queryRecordType a query record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queryRecordType</code> is <code>null</code>
     */

    protected void removeQueryRecordType(org.osid.type.Type queryRecordType) {
        this.queryRecordTypes.remove(queryRecordType);
        return;
    }


    /**
     *  Gets the supported search record types. 
     *
     *  @return a list containing the supported search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.searchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given search record type is supported. 
     *
     *  @param  searchRecordType a <code> Type </code> indicating a search 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> searchRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSearchRecordType(org.osid.type.Type searchRecordType) {
        return (this.searchRecordTypes.contains(searchRecordType));
    }


    /**
     *  Adds support for a search record type.
     *
     *  @param searchRecordType a search record type
     *  @throws org.osid.NullArgumentException
     *  <code>searchRecordType</code> is <code>null</code>
     */

    protected void addSearchRecordType(org.osid.type.Type searchRecordType) {
        this.searchRecordTypes.add(searchRecordType);
        return;
    }


    /**
     *  Removes support for a search record type.
     *
     *  @param searchRecordType a search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>searchRecordType</code> is <code>null</code>
     */

    protected void removeSearchRecordType(org.osid.type.Type searchRecordType) {
        this.searchRecordTypes.remove(searchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Engine </code> record types. 
     *
     *  @return a list containing the supported engine record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEngineRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.engineRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Engine </code> record type is supported. 
     *
     *  @param  engineRecordType a <code> Type </code> indicating an <code> 
     *          Engine </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> engineRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEngineRecordType(org.osid.type.Type engineRecordType) {
        return (this.engineRecordTypes.contains(engineRecordType));
    }


    /**
     *  Adds support for an engine record type.
     *
     *  @param engineRecordType an engine record type
     *  @throws org.osid.NullArgumentException
     *  <code>engineRecordType</code> is <code>null</code>
     */

    protected void addEngineRecordType(org.osid.type.Type engineRecordType) {
        this.engineRecordTypes.add(engineRecordType);
        return;
    }


    /**
     *  Removes support for an engine record type.
     *
     *  @param engineRecordType an engine record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>engineRecordType</code> is <code>null</code>
     */

    protected void removeEngineRecordType(org.osid.type.Type engineRecordType) {
        this.engineRecordTypes.remove(engineRecordType);
        return;
    }


    /**
     *  Gets the supported engine search record types. 
     *
     *  @return a list containing the supported engine search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEngineSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.engineSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given engine search record type is supported. 
     *
     *  @param  engineSearchRecordType a <code> Type </code> indicating an 
     *          engine record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> engineSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEngineSearchRecordType(org.osid.type.Type engineSearchRecordType) {
        return (this.engineSearchRecordTypes.contains(engineSearchRecordType));
    }


    /**
     *  Adds support for an engine search record type.
     *
     *  @param engineSearchRecordType an engine search record type
     *  @throws org.osid.NullArgumentException
     *  <code>engineSearchRecordType</code> is <code>null</code>
     */

    protected void addEngineSearchRecordType(org.osid.type.Type engineSearchRecordType) {
        this.engineSearchRecordTypes.add(engineSearchRecordType);
        return;
    }


    /**
     *  Removes support for an engine search record type.
     *
     *  @param engineSearchRecordType an engine search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>engineSearchRecordType</code> is <code>null</code>
     */

    protected void removeEngineSearchRecordType(org.osid.type.Type engineSearchRecordType) {
        this.engineSearchRecordTypes.remove(engineSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the search 
     *  service. 
     *
     *  @return a <code> SearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchSession getSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the search 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return a <code> SearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchSession getSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the search service 
     *  for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Engine </code> 
     *  @return a <code> SearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchSession getSearchSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the search service 
     *  for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an engine 
     *  @param  proxy the proxy 
     *  @return a <code> SearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchSession getSearchSessionForEngine(org.osid.id.Id engineId, 
                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine lookup 
     *  service. 
     *
     *  @return an <code> EngineLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineLookupSession getEngineLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getEngineLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine lookup 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineLookupSession getEngineLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getEngineLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine query 
     *  service. 
     *
     *  @return an <code> EngineQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineQuerySession getEngineQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getEngineQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine query 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineQuerySession getEngineQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getEngineQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine search 
     *  service. 
     *
     *  @return an <code> EngineSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineSearchSession getEngineSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getEngineSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine search 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineSearchSession getEngineSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getEngineSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  administrative service. 
     *
     *  @return an <code> EngineAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineAdminSession getEngineAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getEngineAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  administrative service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineAdminSession getEngineAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getEngineAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  notification service. 
     *
     *  @param  engineReceiver the receiver 
     *  @return an <code> EngineNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> engineReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineNotificationSession getEngineNotificationSession(org.osid.search.EngineReceiver engineReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getEngineNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  notification service. 
     *
     *  @param  engineReceiver the receiver 
     *  @param  proxy the proxy 
     *  @return an <code> EngineNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> engineReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineNotificationSession getEngineNotificationSession(org.osid.search.EngineReceiver engineReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getEngineNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy service. 
     *
     *  @return an <code> EngineHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineHierarchySession getEngineHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getEngineHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.EngineHierarchySession getEngineHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getEngineHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy design service. 
     *
     *  @return an <code> EngineierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.search.EngineHierarchyDesignSession getEngineHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchManager.getEngineHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy design service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.search.EngineHierarchyDesignSession getEngineHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.search.SearchProxyManager.getEngineHierarchyDesignSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.queryRecordTypes.clear();
        this.queryRecordTypes.clear();

        this.searchRecordTypes.clear();
        this.searchRecordTypes.clear();

        this.engineRecordTypes.clear();
        this.engineRecordTypes.clear();

        this.engineSearchRecordTypes.clear();
        this.engineSearchRecordTypes.clear();

        return;
    }
}
