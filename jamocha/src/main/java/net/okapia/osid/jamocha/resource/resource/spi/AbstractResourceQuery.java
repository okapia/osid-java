//
// AbstractResourceQuery.java
//
//     A template for making a Resource Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resource.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for resources.
 */

public abstract class AbstractResourceQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.resource.ResourceQuery {

    private final java.util.Collection<org.osid.resource.records.ResourceQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches resources that are also groups. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchGroup(boolean match) {
        return;
    }


    /**
     *  Clears the group terms. 
     */

    @OSID @Override
    public void clearGroupTerms() {
        return;
    }


    /**
     *  Matches resources that are also demographics. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchDemographic(boolean match) {
        return;
    }


    /**
     *  Clears the demographic terms. 
     */

    @OSID @Override
    public void clearDemographicTerms() {
        return;
    }


    /**
     *  Sets the group <code> Id </code> for this query to match resources 
     *  within the given group. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingGroupId(org.osid.id.Id resourceId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the group <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingGroupIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  containing groups. 
     *
     *  @return <code> true </code> if a group resource query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for a a containing group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getContainingGroupQuery() {
        throw new org.osid.UnimplementedException("supportsContainingGroupQuery() is false");
    }


    /**
     *  Matches resources inside any group. 
     *
     *  @param  match <code> true </code> to match any containing group, 
     *          <code> false </code> to match resources part of no groups 
     */

    @OSID @Override
    public void matchAnyContainingGroup(boolean match) {
        return;
    }


    /**
     *  Clears the containing group terms. 
     */

    @OSID @Override
    public void clearContainingGroupTerms() {
        return;
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAvatarId(org.osid.id.Id assetId, boolean match) {
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAvatarIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAvatarQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAvatarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAvatarQuery() {
        throw new org.osid.UnimplementedException("supportsAvatarQuery() is false");
    }


    /**
     *  Matches resources with any asset. 
     *
     *  @param  match <code> true </code> to match any asset, <code> false 
     *          </code> to match resources with no asset 
     */

    @OSID @Override
    public void matchAnyAvatar(boolean match) {
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAvatarTerms() {
        return;
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Matches resources with any agent. 
     *
     *  @param  match <code> true </code> to match any agent, <code> false 
     *          </code> to match resources with no agent 
     */

    @OSID @Override
    public void matchAnyAgent(boolean match) {
        return;
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Sets the resource relationship <code> Id </code> for this query. 
     *
     *  @param  resourceRelationshipId the resource relationship <code> Id 
     *          </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceRelationshipId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchResourceRelationshipId(org.osid.id.Id resourceRelationshipId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the resource relationship <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceRelationshipIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceRelationshipQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource relationship query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query for aa resource relationship. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource relationship query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipQuery getResourceRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsResourceRelationshipQuery() is false");
    }


    /**
     *  Matches resources with any resource relationship. 
     *
     *  @param  match <code> true </code> to match any resource relationship, 
     *          <code> false </code> to match resources with no relationship 
     */

    @OSID @Override
    public void matchAnyResourceRelationship(boolean match) {
        return;
    }


    /**
     *  Clears the resource relationship terms. 
     */

    @OSID @Override
    public void clearResourceRelationshipTerms() {
        return;
    }


    /**
     *  Sets the bin <code> Id </code> for this query. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        return;
    }


    /**
     *  Clears the bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given resource query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a resource implementing the requested record.
     *
     *  @param resourceRecordType a resource record type
     *  @return the resource query record
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceQueryRecord getResourceQueryRecord(org.osid.type.Type resourceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceQueryRecord record : this.records) {
            if (record.implementsRecordType(resourceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource query. 
     *
     *  @param resourceQueryRecord resource query record
     *  @param resourceRecordType resource record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResourceQueryRecord(org.osid.resource.records.ResourceQueryRecord resourceQueryRecord, 
                                          org.osid.type.Type resourceRecordType) {

        addRecordType(resourceRecordType);
        nullarg(resourceQueryRecord, "resource query record");
        this.records.add(resourceQueryRecord);        
        return;
    }
}
