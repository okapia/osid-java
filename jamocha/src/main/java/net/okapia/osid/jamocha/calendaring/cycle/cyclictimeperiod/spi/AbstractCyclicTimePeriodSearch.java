//
// AbstractCyclicTimePeriodSearch.java
//
//     A template for making a CyclicTimePeriod Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing cyclic time period searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCyclicTimePeriodSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.cycle.CyclicTimePeriodSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicTimePeriodSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.cycle.CyclicTimePeriodSearchOrder cyclicTimePeriodSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of cyclic time periods. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  cyclicTimePeriodIds list of cyclic time periods
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCyclicTimePeriods(org.osid.id.IdList cyclicTimePeriodIds) {
        while (cyclicTimePeriodIds.hasNext()) {
            try {
                this.ids.add(cyclicTimePeriodIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCyclicTimePeriods</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of cyclic time period Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCyclicTimePeriodIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  cyclicTimePeriodSearchOrder cyclic time period search order 
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>cyclicTimePeriodSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCyclicTimePeriodResults(org.osid.calendaring.cycle.CyclicTimePeriodSearchOrder cyclicTimePeriodSearchOrder) {
	this.cyclicTimePeriodSearchOrder = cyclicTimePeriodSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.cycle.CyclicTimePeriodSearchOrder getCyclicTimePeriodSearchOrder() {
	return (this.cyclicTimePeriodSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given cyclic time period search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a cyclic time period implementing the requested record.
     *
     *  @param cyclicTimePeriodSearchRecordType a cyclic time period search record
     *         type
     *  @return the cyclic time period search record
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(cyclicTimePeriodSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicTimePeriodSearchRecord getCyclicTimePeriodSearchRecord(org.osid.type.Type cyclicTimePeriodSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.cycle.records.CyclicTimePeriodSearchRecord record : this.records) {
            if (record.implementsRecordType(cyclicTimePeriodSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(cyclicTimePeriodSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this cyclic time period search. 
     *
     *  @param cyclicTimePeriodSearchRecord cyclic time period search record
     *  @param cyclicTimePeriodSearchRecordType cyclicTimePeriod search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCyclicTimePeriodSearchRecord(org.osid.calendaring.cycle.records.CyclicTimePeriodSearchRecord cyclicTimePeriodSearchRecord, 
                                           org.osid.type.Type cyclicTimePeriodSearchRecordType) {

        addRecordType(cyclicTimePeriodSearchRecordType);
        this.records.add(cyclicTimePeriodSearchRecord);        
        return;
    }
}
