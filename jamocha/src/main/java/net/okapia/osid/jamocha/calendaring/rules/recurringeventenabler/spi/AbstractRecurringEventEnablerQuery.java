//
// AbstractRecurringEventEnablerQuery.java
//
//     A template for making a RecurringEventEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for recurring event enablers.
 */

public abstract class AbstractRecurringEventEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.calendaring.rules.RecurringEventEnablerQuery {

    private final java.util.Collection<org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the recurring event. 
     *
     *  @param  recurringEventId the recurring event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recurringEventId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRecurringEventId(org.osid.id.Id recurringEventId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the recurring event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRecurringEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> RecurringEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a recurring event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRecurringEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a supsreding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the recurring event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRecurringEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuery getRuledRecurringEventQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRecurringEventQuery() is false");
    }


    /**
     *  Matches enablers mapped to any supsreding event. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          supsreding event, <code> false </code> to match enablers 
     *          mapped to no supsreding events 
     */

    @OSID @Override
    public void matchAnyRuledRecurringEvent(boolean match) {
        return;
    }


    /**
     *  Clears the recurring event query terms. 
     */

    @OSID @Override
    public void clearRuledRecurringEventTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the calendar. 
     *
     *  @param  calendarId the calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar query terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given recurring event enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a recurring event enabler implementing the requested record.
     *
     *  @param recurringEventEnablerRecordType a recurring event enabler record type
     *  @return the recurring event enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord getRecurringEventEnablerQueryRecord(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(recurringEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recurring event enabler query. 
     *
     *  @param recurringEventEnablerQueryRecord recurring event enabler query record
     *  @param recurringEventEnablerRecordType recurringEventEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecurringEventEnablerQueryRecord(org.osid.calendaring.rules.records.RecurringEventEnablerQueryRecord recurringEventEnablerQueryRecord, 
                                          org.osid.type.Type recurringEventEnablerRecordType) {

        addRecordType(recurringEventEnablerRecordType);
        nullarg(recurringEventEnablerQueryRecord, "recurring event enabler query record");
        this.records.add(recurringEventEnablerQueryRecord);        
        return;
    }
}
