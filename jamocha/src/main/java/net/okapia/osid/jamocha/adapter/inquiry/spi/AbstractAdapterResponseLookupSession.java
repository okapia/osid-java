//
// AbstractAdapterResponseLookupSession.java
//
//    A Response lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inquiry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Response lookup session adapter.
 */

public abstract class AbstractAdapterResponseLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inquiry.ResponseLookupSession {

    private final org.osid.inquiry.ResponseLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterResponseLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterResponseLookupSession(org.osid.inquiry.ResponseLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Inquest/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Inquest Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getInquestId() {
        return (this.session.getInquestId());
    }


    /**
     *  Gets the {@code Inquest} associated with this session.
     *
     *  @return the {@code Inquest} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Inquest getInquest()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getInquest());
    }


    /**
     *  Tests if this user can perform {@code Response} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupResponses() {
        return (this.session.canLookupResponses());
    }


    /**
     *  A complete view of the {@code Response} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResponseView() {
        this.session.useComparativeResponseView();
        return;
    }


    /**
     *  A complete view of the {@code Response} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResponseView() {
        this.session.usePlenaryResponseView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include responses in inquests which are children
     *  of this inquest in the inquest hierarchy.
     */

    @OSID @Override
    public void useFederatedInquestView() {
        this.session.useFederatedInquestView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this inquest only.
     */

    @OSID @Override
    public void useIsolatedInquestView() {
        this.session.useIsolatedInquestView();
        return;
    }
    

    /**
     *  Only responses whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveResponseView() {
        this.session.useEffectiveResponseView();
        return;
    }
    

    /**
     *  All responses of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveResponseView() {
        this.session.useAnyEffectiveResponseView();
        return;
    }

     
    /**
     *  Gets the {@code Response} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Response} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Response} and
     *  retained for compatibility.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param responseId {@code Id} of the {@code Response}
     *  @return the response
     *  @throws org.osid.NotFoundException {@code responseId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code responseId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.Response getResponse(org.osid.id.Id responseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponse(responseId));
    }


    /**
     *  Gets a {@code ResponseList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  responses specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Responses} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  responseIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Response} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code responseIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByIds(org.osid.id.IdList responseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesByIds(responseIds));
    }


    /**
     *  Gets a {@code ResponseList} corresponding to the given
     *  response genus {@code Type} which does not include
     *  responses of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  responseGenusType a response genus type 
     *  @return the returned {@code Response} list
     *  @throws org.osid.NullArgumentException
     *          {@code responseGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByGenusType(org.osid.type.Type responseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesByGenusType(responseGenusType));
    }


    /**
     *  Gets a {@code ResponseList} corresponding to the given
     *  response genus {@code Type} and include any additional
     *  responses with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  responseGenusType a response genus type 
     *  @return the returned {@code Response} list
     *  @throws org.osid.NullArgumentException
     *          {@code responseGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByParentGenusType(org.osid.type.Type responseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesByParentGenusType(responseGenusType));
    }


    /**
     *  Gets a {@code ResponseList} containing the given
     *  response record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  responseRecordType a response record type 
     *  @return the returned {@code Response} list
     *  @throws org.osid.NullArgumentException
     *          {@code responseRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesByRecordType(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesByRecordType(responseRecordType));
    }


    /**
     *  Gets a {@code ResponseList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session.
     *  
     *  In active mode, responses are returned that are currently
     *  active. In any status mode, active and inactive responses
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Response} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesOnDate(from, to));
    }
        
    
    /**
     *  Gets a list of responses corresponding to an inquiry {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  inquiryId the {@code Id} of the inquiry
     *  @return the returned {@code ResponseList}
     *  @throws org.osid.NullArgumentException {@code inquiryId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiry(org.osid.id.Id inquiryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesForInquiry(inquiryId));
    }


    /**
     *  Gets a list of responses corresponding to an inquiry {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  inquiryId the {@code Id} of the inquiry
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ResponseList}
     *  @throws org.osid.NullArgumentException {@code inquiryId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryOnDate(org.osid.id.Id inquiryId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesForInquiryOnDate(inquiryId, from, to));
    }


    /**
     *  Gets a list of responses corresponding to a responder {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the responder
     *  @return the returned {@code ResponseList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForResponder(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesForResponder(resourceId));
    }


    /**
     *  Gets a list of responses corresponding to a responder {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the responder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ResponseList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForResponderOnDate(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesForResponderOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of responses corresponding to inquiry and
     *  responder {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  inquiryId the {@code Id} of the inquiry
     *  @param  resourceId the {@code Id} of the responder
     *  @return the returned {@code ResponseList}
     *  @throws org.osid.NullArgumentException {@code inquiryId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryAndResponder(org.osid.id.Id inquiryId,
                                                                            org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesForInquiryAndResponder(inquiryId, resourceId));
    }


    /**
     *  Gets a list of responses corresponding to inquiry and
     *  responder {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session.
     *
     *  In effective mode, responses are returned that are currently
     *  effective. In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the responder
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ResponseList}
     *  @throws org.osid.NullArgumentException {@code inquiryId},
     *          {@code resourceId}, {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponsesForInquiryAndResponderOnDate(org.osid.id.Id inquiryId,
                                                                                  org.osid.id.Id resourceId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponsesForInquiryAndResponderOnDate(inquiryId, resourceId, from, to));
    }


    /**
     *  Gets all {@code Responses}. 
     *
     *  In plenary mode, the returned list contains all known
     *  responses or an error results. Otherwise, the returned list
     *  may contain only those responses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, responses are returned that are currently
     *  effective.  In any effective mode, effective responses and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Responses} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.ResponseList getResponses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResponses());
    }
}
