//
// AbstractLogEntryQuery.java
//
//     A template for making a LogEntry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.logentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for log entries.
 */

public abstract class AbstractLogEntryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.tracking.LogEntryQuery {

    private final java.util.Collection<org.osid.tracking.records.LogEntryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent.. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Sets the issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available for an ending 
     *  issue. 
     *
     *  @return <code> true </code> if an issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for an issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        return;
    }


    /**
     *  Matches log entries that have the specified date inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Clears the date query terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        return;
    }


    /**
     *  Matches a summary. 
     *
     *  @param  summary summary 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> summary </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> summary </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> stringMatchType </code> 
     *          not supported 
     */

    @OSID @Override
    public void matchSummary(String summary, 
                             org.osid.type.Type stringMatchType, boolean match) {
        return;
    }


    /**
     *  Matches log entries that has any summary. 
     *
     *  @param  match <code> true </code> to match log entries with any 
     *          summary, <code> false </code> to match log entries with no 
     *          summary 
     */

    @OSID @Override
    public void matchAnySummary(boolean match) {
        return;
    }


    /**
     *  Clears the summary query terms. 
     */

    @OSID @Override
    public void clearSummaryTerms() {
        return;
    }


    /**
     *  Matches a message. 
     *
     *  @param  message a message 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> message </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> message </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> stringMatchType </code> 
     *          not supported 
     */

    @OSID @Override
    public void matchMessage(String message, 
                             org.osid.type.Type stringMatchType, boolean match) {
        return;
    }


    /**
     *  Matches log entries that has any message. 
     *
     *  @param  match <code> true </code> to match log entries with any 
     *          message, <code> false </code> to match log entries with no 
     *          message 
     */

    @OSID @Override
    public void matchAnyMessage(boolean match) {
        return;
    }


    /**
     *  Clears the message query terms. 
     */

    @OSID @Override
    public void clearMessageTerms() {
        return;
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match log 
     *  entries assigned to frontOffices. 
     *
     *  @param  frontOfficeId the front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFrontOfficeId(org.osid.id.Id frontOfficeId, boolean match) {
        return;
    }


    /**
     *  Clears the front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a front office. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsFrontOfficeQuery() is false");
    }


    /**
     *  Clears the front office query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given log entry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a log entry implementing the requested record.
     *
     *  @param logEntryRecordType a log entry record type
     *  @return the log entry query record
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.LogEntryQueryRecord getLogEntryQueryRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.LogEntryQueryRecord record : this.records) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this log entry query. 
     *
     *  @param logEntryQueryRecord log entry query record
     *  @param logEntryRecordType logEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLogEntryQueryRecord(org.osid.tracking.records.LogEntryQueryRecord logEntryQueryRecord, 
                                          org.osid.type.Type logEntryRecordType) {

        addRecordType(logEntryRecordType);
        nullarg(logEntryQueryRecord, "log entry query record");
        this.records.add(logEntryQueryRecord);        
        return;
    }
}
