//
// InvariantMapProxyDirectionLookupSession
//
//    Implements a Direction lookup service backed by a fixed
//    collection of directions. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  Implements a Direction lookup service backed by a fixed
 *  collection of directions. The directions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyDirectionLookupSession
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractMapDirectionLookupSession
    implements org.osid.recipe.DirectionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDirectionLookupSession} with no
     *  directions.
     *
     *  @param cookbook the cookbook
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                                  org.osid.proxy.Proxy proxy) {
        setCookbook(cookbook);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyDirectionLookupSession} with a single
     *  direction.
     *
     *  @param cookbook the cookbook
     *  @param direction a single direction
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code direction} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                                  org.osid.recipe.Direction direction, org.osid.proxy.Proxy proxy) {

        this(cookbook, proxy);
        putDirection(direction);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDirectionLookupSession} using
     *  an array of directions.
     *
     *  @param cookbook the cookbook
     *  @param directions an array of directions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code directions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                                  org.osid.recipe.Direction[] directions, org.osid.proxy.Proxy proxy) {

        this(cookbook, proxy);
        putDirections(directions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDirectionLookupSession} using a
     *  collection of directions.
     *
     *  @param cookbook the cookbook
     *  @param directions a collection of directions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code directions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDirectionLookupSession(org.osid.recipe.Cookbook cookbook,
                                                  java.util.Collection<? extends org.osid.recipe.Direction> directions,
                                                  org.osid.proxy.Proxy proxy) {

        this(cookbook, proxy);
        putDirections(directions);
        return;
    }
}
