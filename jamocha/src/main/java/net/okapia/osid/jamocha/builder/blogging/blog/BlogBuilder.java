//
// Blog.java
//
//     Defines a Blog builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.blogging.blog;


/**
 *  Defines a <code>Blog</code> builder.
 */

public final class BlogBuilder
    extends net.okapia.osid.jamocha.builder.blogging.blog.spi.AbstractBlogBuilder<BlogBuilder> {
    

    /**
     *  Constructs a new <code>BlogBuilder</code> using a
     *  <code>MutableBlog</code>.
     */

    public BlogBuilder() {
        super(new MutableBlog());
        return;
    }


    /**
     *  Constructs a new <code>BlogBuilder</code> using the given
     *  mutable blog.
     * 
     *  @param blog
     */

    public BlogBuilder(BlogMiter blog) {
        super(blog);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return BlogBuilder
     */

    @Override
    protected BlogBuilder self() {
        return (this);
    }
}       


