//
// ManagerConfiguration.java
//
//     handles configuration in the managers.
//
//
// Tom Coppeto
// Okapia
// 27 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Handles configuration in the managers.
 */

public class ManagerConfiguration {
    private final org.osid.OsidRuntimeManager runtime;
    private boolean init = false;
    private org.osid.configuration.ValueRetrievalSession configuration;
    

    /**
     *  Constructs a new <code>ManagerConfiguration</code>.
     *
     *  @param runtime
     *  @throws org.osid.NullArgumentException <code>runtime</code> is
     *          <code>null</code>
     */

    public ManagerConfiguration(org.osid.OsidRuntimeManager runtime) {
        nullarg(runtime, "runtime");
        this.runtime = runtime;
        return;
    }


    /*
     * why the lazy init? Any OSID used by the runtime will expect to
     * get its configuration from the runtime. Ideally, this
     * bootstrapping problem ought to be fixed in the rumtime impl itself.
     */

    private void initialize()
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        if (this.init) {
            return;
        }

        this.init = true;

        try {
            this.configuration = runtime.getConfiguration();
        } catch (org.osid.PermissionDeniedException | org.osid.UnimplementedException e) {
            throw new org.osid.ConfigurationErrorException(e);
        }

        this.configuration.useComparativeValueView();
        this.configuration.useFederatedConfigurationView();
        this.configuration.useConditionalView();

        return;
    }


    /**
     *  Gets a <code> Value </code> for the given parameter <code>
     *  Id. </code> If more than one value exists for the given
     *  parameter, the most preferred value is returned. This method
     *  can be used as a convenience when only one value is
     *  expected. <code> getValuesByParameters() </code> should be
     *  used for getting all the active values.
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value 
     *  @throws org.osid.ConfigurationErrorException the <code>
     *          parameterId </code> not found or no value available
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    public org.osid.configuration.Value getConfigurationValue(org.osid.id.Id parameterId)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        initialize();
        try {
            return (this.configuration.getValueByParameter(parameterId));
        } catch (org.osid.PermissionDeniedException | org.osid.NotFoundException e) {
            throw new org.osid.ConfigurationErrorException(e);
        }
    }


    /**
     *  Gets all the <code> Values </code> for the given parameter
     *  <code> Id</code>.
     *
     *  @param  parameterId the <code> Id </code> of the <code> Parameter 
     *          </code> to retrieve 
     *  @return the value list 
     *  @throws org.osid.ConfigurationErrorException the <code>
     *          parameterId </code> not found
     *  @throws org.osid.NullArgumentException the <code> parameterId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    public org.osid.configuration.ValueList getValuesByParameter(org.osid.id.Id parameterId)
        throws org.osid.ConfigurationErrorException,
               org.osid.OperationFailedException {

        initialize();
        try {
            return (this.configuration.getValuesByParameter(parameterId));
        } catch (org.osid.PermissionDeniedException | org.osid.NotFoundException e) {
            throw new org.osid.ConfigurationErrorException(e);
        }
    }


    /**
     *  Closes this configuration.
     */

    public void close() {
        if (this.configuration != null) {
            this.configuration.close();
        }

        return;
    }
}
