//
// AbstractCalendaringCycleBatchProxyManager.java
//
//     An adapter for a CalendaringCycleBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.cycle.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CalendaringCycleBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCalendaringCycleBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager>
    implements org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringCycleBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCalendaringCycleBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCalendaringCycleBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCalendaringCycleBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of cyclic events is available. 
     *
     *  @return <code> true </code> if a cyclic event bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventBatchAdmin() {
        return (getAdapteeManager().supportsCyclicEventBatchAdmin());
    }


    /**
     *  Tests if bulk administration of cyclic time periods is available. 
     *
     *  @return <code> true </code> if a cyclic time period bulk 
     *          administrative service is available, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodBatchAdmin() {
        return (getAdapteeManager().supportsCyclicTimePeriodBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  event administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicEventBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicEventBatchAdminSession getCyclicEventBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  event administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicEventBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicEventBatchAdminSession getCyclicEventBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicEventBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  time period administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchAdminSession getCyclicTimePeriodBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  time period administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchAdminSession getCyclicTimePeriodBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCyclicTimePeriodBatchAdminSessionForCalendar(calendarId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
