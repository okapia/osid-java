//
// AbstractIndexedMapCyclicEventLookupSession.java
//
//    A simple framework for providing a CyclicEvent lookup service
//    backed by a fixed collection of cyclic events with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CyclicEvent lookup service backed by a
 *  fixed collection of cyclic events. The cyclic events are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some cyclic events may be compatible
 *  with more types than are indicated through these cyclic event
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CyclicEvents</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCyclicEventLookupSession
    extends AbstractMapCyclicEventLookupSession
    implements org.osid.calendaring.cycle.CyclicEventLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.cycle.CyclicEvent> cyclicEventsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.cycle.CyclicEvent>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.cycle.CyclicEvent> cyclicEventsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.cycle.CyclicEvent>());


    /**
     *  Makes a <code>CyclicEvent</code> available in this session.
     *
     *  @param  cyclicEvent a cyclic event
     *  @throws org.osid.NullArgumentException <code>cyclicEvent<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        super.putCyclicEvent(cyclicEvent);

        this.cyclicEventsByGenus.put(cyclicEvent.getGenusType(), cyclicEvent);
        
        try (org.osid.type.TypeList types = cyclicEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.cyclicEventsByRecord.put(types.getNextType(), cyclicEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a cyclic event from this session.
     *
     *  @param cyclicEventId the <code>Id</code> of the cyclic event
     *  @throws org.osid.NullArgumentException <code>cyclicEventId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCyclicEvent(org.osid.id.Id cyclicEventId) {
        org.osid.calendaring.cycle.CyclicEvent cyclicEvent;
        try {
            cyclicEvent = getCyclicEvent(cyclicEventId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.cyclicEventsByGenus.remove(cyclicEvent.getGenusType());

        try (org.osid.type.TypeList types = cyclicEvent.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.cyclicEventsByRecord.remove(types.getNextType(), cyclicEvent);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCyclicEvent(cyclicEventId);
        return;
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  cyclic event genus <code>Type</code> which does not include
     *  cyclic events of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known cyclic events or an error results. Otherwise,
     *  the returned list may contain only those cyclic events that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  cyclicEventGenusType a cyclic event genus type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.ArrayCyclicEventList(this.cyclicEventsByGenus.get(cyclicEventGenusType)));
    }


    /**
     *  Gets a <code>CyclicEventList</code> containing the given
     *  cyclic event record <code>Type</code>. In plenary mode, the
     *  returned list contains all known cyclic events or an error
     *  results. Otherwise, the returned list may contain only those
     *  cyclic events that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  cyclicEventRecordType a cyclic event record type 
     *  @return the returned <code>cyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByRecordType(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.ArrayCyclicEventList(this.cyclicEventsByRecord.get(cyclicEventRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.cyclicEventsByGenus.clear();
        this.cyclicEventsByRecord.clear();

        super.close();

        return;
    }
}
