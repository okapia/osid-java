//
// AbstractIndexedMapProfileEntryLookupSession.java
//
//    A simple framework for providing a ProfileEntry lookup service
//    backed by a fixed collection of profile entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.profile.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a ProfileEntry lookup service backed by a
 *  fixed collection of profile entries. The profile entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some profile entries may be compatible
 *  with more types than are indicated through these profile entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ProfileEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProfileEntryLookupSession
    extends AbstractMapProfileEntryLookupSession
    implements org.osid.profile.ProfileEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.profile.ProfileEntry> profileEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.profile.ProfileEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.profile.ProfileEntry> profileEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.profile.ProfileEntry>());


    /**
     *  Makes a <code>ProfileEntry</code> available in this session.
     *
     *  @param  profileEntry a profile entry
     *  @throws org.osid.NullArgumentException <code>profileEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProfileEntry(org.osid.profile.ProfileEntry profileEntry) {
        super.putProfileEntry(profileEntry);

        this.profileEntriesByGenus.put(profileEntry.getGenusType(), profileEntry);
        
        try (org.osid.type.TypeList types = profileEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.profileEntriesByRecord.put(types.getNextType(), profileEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a profile entry from this session.
     *
     *  @param profileEntryId the <code>Id</code> of the profile entry
     *  @throws org.osid.NullArgumentException <code>profileEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProfileEntry(org.osid.id.Id profileEntryId) {
        org.osid.profile.ProfileEntry profileEntry;
        try {
            profileEntry = getProfileEntry(profileEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.profileEntriesByGenus.remove(profileEntry.getGenusType());

        try (org.osid.type.TypeList types = profileEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.profileEntriesByRecord.remove(types.getNextType(), profileEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProfileEntry(profileEntryId);
        return;
    }


    /**
     *  Gets a <code>ProfileEntryList</code> corresponding to the given
     *  profile entry genus <code>Type</code> which does not include
     *  profile entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known profile entries or an error results. Otherwise,
     *  the returned list may contain only those profile entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  profileEntryGenusType a profile entry genus type 
     *  @return the returned <code>ProfileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByGenusType(org.osid.type.Type profileEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profileentry.ArrayProfileEntryList(this.profileEntriesByGenus.get(profileEntryGenusType)));
    }


    /**
     *  Gets a <code>ProfileEntryList</code> containing the given
     *  profile entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known profile entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  profile entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  profileEntryRecordType a profile entry record type 
     *  @return the returned <code>profileEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryList getProfileEntriesByRecordType(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.profile.profileentry.ArrayProfileEntryList(this.profileEntriesByRecord.get(profileEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.profileEntriesByGenus.clear();
        this.profileEntriesByRecord.clear();

        super.close();

        return;
    }
}
