//
// AbstractCompetencyLookupSession.java
//
//    A starter implementation framework for providing a Competency
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Competency
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCompetencies(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCompetencyLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.CompetencyLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    

    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this session.
     *
     *  @return the <code>Foundry</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>Competency</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCompetencies() {
        return (true);
    }


    /**
     *  A complete view of the <code>Competency</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCompetencyView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Competency</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCompetencyView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include competencies in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Competency</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Competency</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Competency</code> and
     *  retained for compatibility.
     *
     *  @param competencyId <code>Id</code> of the
     *          <code>Competency</code>
     *  @return the competency
     *  @throws org.osid.NotFoundException <code>competencyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>competencyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency(org.osid.id.Id competencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.CompetencyList competencies = getCompetencies()) {
            while (competencies.hasNext()) {
                org.osid.resourcing.Competency competency = competencies.getNextCompetency();
                if (competency.getId().equals(competencyId)) {
                    return (competency);
                }
            }
        } 

        throw new org.osid.NotFoundException(competencyId + " not found");
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  competencies specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Competencies</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCompetencies()</code>.
     *
     *  @param  competencyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>competencyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByIds(org.osid.id.IdList competencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.Competency> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = competencyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCompetency(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("competency " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.competency.LinkedCompetencyList(ret));
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  competency genus <code>Type</code> which does not include
     *  competencies of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCompetencies()</code>.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.competency.CompetencyGenusFilterList(getCompetencies(), competencyGenusType));
    }


    /**
     *  Gets a <code>CompetencyList</code> corresponding to the given
     *  competency genus <code>Type</code> and include any additional
     *  competencies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCompetencies()</code>.
     *
     *  @param  competencyGenusType a competency genus type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByParentGenusType(org.osid.type.Type competencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCompetenciesByGenusType(competencyGenusType));
    }


    /**
     *  Gets a <code>CompetencyList</code> containing the given
     *  competency record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCompetencies()</code>.
     *
     *  @param  competencyRecordType a competency record type 
     *  @return the returned <code>Competency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>competencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.CompetencyList getCompetenciesByRecordType(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.competency.CompetencyRecordFilterList(getCompetencies(), competencyRecordType));
    }


    /**
     *  Gets all <code>Competencies</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  competencies or an error results. Otherwise, the returned list
     *  may contain only those competencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  @return a list of <code>Competencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.CompetencyList getCompetencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the competency list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of competencies
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.CompetencyList filterCompetenciesOnViews(org.osid.resourcing.CompetencyList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
