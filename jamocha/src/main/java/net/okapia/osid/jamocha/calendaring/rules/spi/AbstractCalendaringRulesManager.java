//
// AbstractCalendaringRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCalendaringRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.calendaring.rules.CalendaringRulesManager,
               org.osid.calendaring.rules.CalendaringRulesProxyManager {

    private final Types recurringEventEnablerRecordTypes   = new TypeRefSet();
    private final Types recurringEventEnablerSearchRecordTypes= new TypeRefSet();

    private final Types offsetEventEnablerRecordTypes      = new TypeRefSet();
    private final Types offsetEventEnablerSearchRecordTypes= new TypeRefSet();

    private final Types supersedingEventEnablerRecordTypes = new TypeRefSet();
    private final Types supersedingEventEnablerSearchRecordTypes= new TypeRefSet();

    private final Types commitmentEnablerRecordTypes       = new TypeRefSet();
    private final Types commitmentEnablerSearchRecordTypes = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCalendaringRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCalendaringRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up recurring event enablers is supported. 
     *
     *  @return true if recurring event enabler lookup is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying recurring event enablers is supported. 
     *
     *  @return true if recurring event enabler query is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching recurring event enablers is supported. 
     *
     *  @return true if recurring event enabler search is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an recurring event enabler administrative service is 
     *  supported. 
     *
     *  @return true if recurring event enabler administration is supported, 
     *          false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an recurring event enabler notification service is supported. 
     *
     *  @return true if recurring event enabler notification is supported, 
     *          false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an recurring event enabler calendar lookup service is 
     *  supported. 
     *
     *  @return true if an recurring event enabler calendar lookup service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerCalendar() {
        return (false);
    }


    /**
     *  Tests if an recurring event enabler calendar service is supported. 
     *
     *  @return true if recurring event enabler calendar assignment service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if an recurring event enabler calendar lookup service is 
     *  supported. 
     *
     *  @return true if an recurring event enabler calendar service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if an recurring event enabler rule lookup service is supported. 
     *
     *  @return true if an recurring event enabler rule lookup service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an recurring event enabler rule application service is 
     *  supported. 
     *
     *  @return true if recurring event enabler rule application service is 
     *          supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up offset event enablers is supported. 
     *
     *  @return <code> true </code> if offset event enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying offset event enablers is supported. 
     *
     *  @return <code> true </code> if offset event enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching offset event enablers is supported. 
     *
     *  @return <code> true </code> if offset event enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an offset event enabler administrative service is supported. 
     *
     *  @return <code> true </code> if offset event enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an offset event enabler notification service is supported. 
     *
     *  @return <code> true </code> if offset event enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an offset event enabler calendar lookup service is supported. 
     *
     *  @return <code> true </code> if an offset event enabler calendar lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerCalendar() {
        return (false);
    }


    /**
     *  Tests if an offset event enabler calendar service is supported. 
     *
     *  @return <code> true </code> if offset event enabler calendar 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if an offset event enabler calendar lookup service is supported. 
     *
     *  @return <code> true </code> if an offset event enabler calendar 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if an offset event enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an offset event enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an offset event enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if offset event enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up superseding event enablers is supported. 
     *
     *  @return <code> true </code> if superseding event enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying superseding event enablers is supported. 
     *
     *  @return <code> true </code> if superseding event enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching superseding event enablers is supported. 
     *
     *  @return <code> true </code> if superseding event enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an superseding event enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if superseding event enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an superseding event enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if superseding event enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an superseding event enabler calendar lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an superseding event enabler calendar 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerCalendar() {
        return (false);
    }


    /**
     *  Tests if an superseding event enabler calendar service is supported. 
     *
     *  @return <code> true </code> if superseding event enabler calendar 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if an superseding event enabler calendar lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an superseding event enabler calendar 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if an superseding event enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an superseding event enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an superseding event enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if superseding event enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up commitment enablers is supported. 
     *
     *  @return <code> true </code> if commitment enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying commitment enablers is supported. 
     *
     *  @return <code> true </code> if commitment enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching commitment enablers is supported. 
     *
     *  @return <code> true </code> if commitment enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a commitment enabler administrative service is supported. 
     *
     *  @return <code> true </code> if commitment enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a commitment enabler notification service is supported. 
     *
     *  @return <code> true </code> if commitment enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a commitment enabler calendar lookup service is supported. 
     *
     *  @return <code> true </code> if a commitment enabler calendar lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerCalendar() {
        return (false);
    }


    /**
     *  Tests if a commitment enabler calendar service is supported. 
     *
     *  @return <code> true </code> if commitment enabler calendar assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if a commitment enabler calendar lookup service is supported. 
     *
     *  @return <code> true </code> if a commitment enabler calendar service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a commitment enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a commitment enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a commitment enabler rule application service is supported. 
     *
     *  @return <code> true </code> if commitment enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> RecurringEventEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> RecurringEventEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecurringEventEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recurringEventEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RecurringEventEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  recurringEventEnablerRecordType a <code> Type </code> 
     *          indicating a <code> RecurringEventEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerRecordType(org.osid.type.Type recurringEventEnablerRecordType) {
        return (this.recurringEventEnablerRecordTypes.contains(recurringEventEnablerRecordType));
    }


    /**
     *  Adds support for a recurring event enabler record type.
     *
     *  @param recurringEventEnablerRecordType a recurring event enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>recurringEventEnablerRecordType</code> is <code>null</code>
     */

    protected void addRecurringEventEnablerRecordType(org.osid.type.Type recurringEventEnablerRecordType) {
        this.recurringEventEnablerRecordTypes.add(recurringEventEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a recurring event enabler record type.
     *
     *  @param recurringEventEnablerRecordType a recurring event enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>recurringEventEnablerRecordType</code> is <code>null</code>
     */

    protected void removeRecurringEventEnablerRecordType(org.osid.type.Type recurringEventEnablerRecordType) {
        this.recurringEventEnablerRecordTypes.remove(recurringEventEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RecurringEventEnabler </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> RecurringEventEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecurringEventEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recurringEventEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RecurringEventEnabler </code> search record 
     *  interface type is supported. 
     *
     *  @param  recurringEventEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> RecurringEventEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsRecurringEventEnablerSearchRecordType(org.osid.type.Type recurringEventEnablerSearchRecordType) {
        return (this.recurringEventEnablerSearchRecordTypes.contains(recurringEventEnablerSearchRecordType));
    }


    /**
     *  Adds support for a recurring event enabler search record type.
     *
     *  @param recurringEventEnablerSearchRecordType a recurring event enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>recurringEventEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addRecurringEventEnablerSearchRecordType(org.osid.type.Type recurringEventEnablerSearchRecordType) {
        this.recurringEventEnablerSearchRecordTypes.add(recurringEventEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a recurring event enabler search record type.
     *
     *  @param recurringEventEnablerSearchRecordType a recurring event enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>recurringEventEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeRecurringEventEnablerSearchRecordType(org.osid.type.Type recurringEventEnablerSearchRecordType) {
        this.recurringEventEnablerSearchRecordTypes.remove(recurringEventEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> OffsetEventEnabler </code> record interface 
     *  types. 
     *
     *  @return a list containing the supported <code> OffsetEventEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOffsetEventEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offsetEventEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> OffsetEventEnabler </code> record interface 
     *  type is supported. 
     *
     *  @param  offsetEventEnablerRecordType a <code> Type </code> indicating 
     *          an <code> OffsetEventEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerRecordType(org.osid.type.Type offsetEventEnablerRecordType) {
        return (this.offsetEventEnablerRecordTypes.contains(offsetEventEnablerRecordType));
    }


    /**
     *  Adds support for an offset event enabler record type.
     *
     *  @param offsetEventEnablerRecordType an offset event enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>offsetEventEnablerRecordType</code> is <code>null</code>
     */

    protected void addOffsetEventEnablerRecordType(org.osid.type.Type offsetEventEnablerRecordType) {
        this.offsetEventEnablerRecordTypes.add(offsetEventEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an offset event enabler record type.
     *
     *  @param offsetEventEnablerRecordType an offset event enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offsetEventEnablerRecordType</code> is <code>null</code>
     */

    protected void removeOffsetEventEnablerRecordType(org.osid.type.Type offsetEventEnablerRecordType) {
        this.offsetEventEnablerRecordTypes.remove(offsetEventEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> OffsetEventEnabler </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> OffsetEventEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOffsetEventEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offsetEventEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> OffsetEventEnabler </code> search record 
     *  interface type is supported. 
     *
     *  @param  offsetEventEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> OffsetEventEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsOffsetEventEnablerSearchRecordType(org.osid.type.Type offsetEventEnablerSearchRecordType) {
        return (this.offsetEventEnablerSearchRecordTypes.contains(offsetEventEnablerSearchRecordType));
    }


    /**
     *  Adds support for an offset event enabler search record type.
     *
     *  @param offsetEventEnablerSearchRecordType an offset event enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>offsetEventEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addOffsetEventEnablerSearchRecordType(org.osid.type.Type offsetEventEnablerSearchRecordType) {
        this.offsetEventEnablerSearchRecordTypes.add(offsetEventEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an offset event enabler search record type.
     *
     *  @param offsetEventEnablerSearchRecordType an offset event enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offsetEventEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeOffsetEventEnablerSearchRecordType(org.osid.type.Type offsetEventEnablerSearchRecordType) {
        this.offsetEventEnablerSearchRecordTypes.remove(offsetEventEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SupersedingEventEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> SupersedingEventEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSupersedingEventEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.supersedingEventEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SupersedingEventEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  supersedingEventEnablerRecordType a <code> Type </code> 
     *          indicating an <code> SupersedingEventEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerRecordType(org.osid.type.Type supersedingEventEnablerRecordType) {
        return (this.supersedingEventEnablerRecordTypes.contains(supersedingEventEnablerRecordType));
    }


    /**
     *  Adds support for a superseding event enabler record type.
     *
     *  @param supersedingEventEnablerRecordType a superseding event enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>supersedingEventEnablerRecordType</code> is <code>null</code>
     */

    protected void addSupersedingEventEnablerRecordType(org.osid.type.Type supersedingEventEnablerRecordType) {
        this.supersedingEventEnablerRecordTypes.add(supersedingEventEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a superseding event enabler record type.
     *
     *  @param supersedingEventEnablerRecordType a superseding event enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>supersedingEventEnablerRecordType</code> is <code>null</code>
     */

    protected void removeSupersedingEventEnablerRecordType(org.osid.type.Type supersedingEventEnablerRecordType) {
        this.supersedingEventEnablerRecordTypes.remove(supersedingEventEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SupersedingEventEnabler </code> search 
     *  record interface types. 
     *
     *  @return a list containing the supported <code> SupersedingEventEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSupersedingEventEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.supersedingEventEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SupersedingEventEnabler </code> search 
     *  record interface type is supported. 
     *
     *  @param  supersedingEventEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> SupersedingEventEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSupersedingEventEnablerSearchRecordType(org.osid.type.Type supersedingEventEnablerSearchRecordType) {
        return (this.supersedingEventEnablerSearchRecordTypes.contains(supersedingEventEnablerSearchRecordType));
    }


    /**
     *  Adds support for a superseding event enabler search record type.
     *
     *  @param supersedingEventEnablerSearchRecordType a superseding event enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>supersedingEventEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addSupersedingEventEnablerSearchRecordType(org.osid.type.Type supersedingEventEnablerSearchRecordType) {
        this.supersedingEventEnablerSearchRecordTypes.add(supersedingEventEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a superseding event enabler search record type.
     *
     *  @param supersedingEventEnablerSearchRecordType a superseding event enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>supersedingEventEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeSupersedingEventEnablerSearchRecordType(org.osid.type.Type supersedingEventEnablerSearchRecordType) {
        this.supersedingEventEnablerSearchRecordTypes.remove(supersedingEventEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CommitmentEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CommitmentEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommitmentEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commitmentEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CommitmentEnabler </code> record type is 
     *  supported. 
     *
     *  @param  commitmentEnablerRecordType a <code> Type </code> indicating a 
     *          <code> CommitmentEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerRecordType(org.osid.type.Type commitmentEnablerRecordType) {
        return (this.commitmentEnablerRecordTypes.contains(commitmentEnablerRecordType));
    }


    /**
     *  Adds support for a commitment enabler record type.
     *
     *  @param commitmentEnablerRecordType a commitment enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>commitmentEnablerRecordType</code> is <code>null</code>
     */

    protected void addCommitmentEnablerRecordType(org.osid.type.Type commitmentEnablerRecordType) {
        this.commitmentEnablerRecordTypes.add(commitmentEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a commitment enabler record type.
     *
     *  @param commitmentEnablerRecordType a commitment enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commitmentEnablerRecordType</code> is <code>null</code>
     */

    protected void removeCommitmentEnablerRecordType(org.osid.type.Type commitmentEnablerRecordType) {
        this.commitmentEnablerRecordTypes.remove(commitmentEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CommitmentEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CommitmentEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommitmentEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commitmentEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CommitmentEnabler </code> search record type 
     *  is supported. 
     *
     *  @param  commitmentEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CommitmentEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCommitmentEnablerSearchRecordType(org.osid.type.Type commitmentEnablerSearchRecordType) {
        return (this.commitmentEnablerSearchRecordTypes.contains(commitmentEnablerSearchRecordType));
    }


    /**
     *  Adds support for a commitment enabler search record type.
     *
     *  @param commitmentEnablerSearchRecordType a commitment enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>commitmentEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addCommitmentEnablerSearchRecordType(org.osid.type.Type commitmentEnablerSearchRecordType) {
        this.commitmentEnablerSearchRecordTypes.add(commitmentEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a commitment enabler search record type.
     *
     *  @param commitmentEnablerSearchRecordType a commitment enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commitmentEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeCommitmentEnablerSearchRecordType(org.osid.type.Type commitmentEnablerSearchRecordType) {
        this.commitmentEnablerSearchRecordTypes.remove(commitmentEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler lookup service. 
     *
     *  @return a <code> RecurringEventEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerLookupSession getRecurringEventEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerLookupSession getRecurringEventEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerLookupSession getRecurringEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerLookupSession getRecurringEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler query service. 
     *
     *  @return a <code> RecurringEventEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerQuerySession getRecurringEventEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerQuerySession getRecurringEventEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerQuerySession getRecurringEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerQuerySession getRecurringEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler search service. 
     *
     *  @return a <code> RecurringtEventEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSearchSession getRecurringEventEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringtEventEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSearchSession getRecurringEventEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSearchSession getRecurringEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSearchSession getRecurringEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler administration service. 
     *
     *  @return a <code> RecurringEventEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerAdminSession getRecurringEventEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerAdminSession getRecurringEventEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerAdminSession getRecurringEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerAdminSession getRecurringEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler notification service. 
     *
     *  @param  recurringEventEnablerReceiver the notification callback 
     *  @return a <code> RecurringEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerNotificationSession getRecurringEventEnablerNotificationSession(org.osid.calendaring.rules.RecurringEventEnablerReceiver recurringEventEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler notification service. 
     *
     *  @param  recurringEventEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerNotificationSession getRecurringEventEnablerNotificationSession(org.osid.calendaring.rules.RecurringEventEnablerReceiver recurringEventEnablerReceiver, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler notification service for the given calendar. 
     *
     *  @param  recurringEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerReceiver </code> or <code> calendarId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerNotificationSession getRecurringEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.RecurringEventEnablerReceiver recurringEventEnablerReceiver, 
                                                                                                                                      org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler notification service for the given calendar. 
     *
     *  @param  recurringEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventEnablerReceiver, calendarId, </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerNotificationSession getRecurringEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.RecurringEventEnablerReceiver recurringEventEnablerReceiver, 
                                                                                                                                      org.osid.id.Id calendarId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup recurring event 
     *  enabler/calendar mappings for recurring event enablers. 
     *
     *  @return a <code> RecurringEventEnablerCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerCalendarSession getRecurringEventEnablerCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup recurring event 
     *  enabler/calendar mappings for recurring event enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerCalendarSession getRecurringEventEnablerCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  recurring event enablers to calendars. 
     *
     *  @return an <code> RecurringEventEnablerCalendarAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringtEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerCalendarAssignmentSession getRecurringEventEnablerCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  recurring event enablers to calendars. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> RecurringEventEnablerCalendarAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringtEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerCalendarAssignmentSession getRecurringEventEnablerCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage recurring event enabler 
     *  smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSmartCalendarSession getRecurringEventEnablerSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage recurring event enabler 
     *  smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerSmartCalendarSession getRecurringEventEnablerSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler mapping lookup service for looking up the rules. 
     *
     *  @return a <code> RecurringEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleLookupSession getRecurringEventEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler mapping lookup service for looking up the rules. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleLookupSession getRecurringEventEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler mapping lookup service for the given calendar for 
     *  looking up rules. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleLookupSession getrecurringEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getrecurringEventEnablerRuleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler mapping lookup service for the given calendar for 
     *  looking up rules. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleLookupSession getrecurringEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getrecurringEventEnablerRuleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler assignment service to apply enablers. 
     *
     *  @return a <code> RecurringEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleApplicationSession getRecurringEventEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler assignment service to apply enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleApplicationSession getRecurringEventEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler assignment service for the given calendar to apply 
     *  enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> RecurringEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleApplicationSession getRecurringEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getRecurringEventEnablerRuleApplicationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event enabler assignment service for the given calendar to apply 
     *  enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerRuleApplicationSession getRecurringEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getRecurringEventEnablerRuleApplicationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler lookup service. 
     *
     *  @return an <code> OffsetEventEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerLookupSession getOffsetEventEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerLookupSession getOffsetEventEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerLookupSession getOffsetEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerLookupSession getOffsetEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler query service. 
     *
     *  @return an <code> OffsetEventEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerQuerySession getOffsetEventEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerQuerySession getOffsetEventEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerQuerySession getOffsetEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerQuerySession getOffsetEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler search service. 
     *
     *  @return an <code> OffsetEventEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSearchSession getOffsetEventEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSearchSession getOffsetEventEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSearchSession getOffsetEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSearchSession getOffsetEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler administration service. 
     *
     *  @return an <code> OffsetEventEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerAdminSession getOffsetEventEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerAdminSession getOffsetEventEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerAdminSession getOffsetEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerAdminSession getOffsetEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler notification service. 
     *
     *  @param  offsetEventEnablerReceiver the notification callback 
     *  @return an <code> OffsetEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerNotificationSession getOffsetEventEnablerNotificationSession(org.osid.calendaring.rules.OffsetEventEnablerReceiver offsetEventEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler notification service. 
     *
     *  @param  offsetEventEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerNotificationSession getOffsetEventEnablerNotificationSession(org.osid.calendaring.rules.OffsetEventEnablerReceiver offsetEventEnablerReceiver, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler notification service for the given calendar. 
     *
     *  @param  offsetEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerReceiver </code> or <code> calendarId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerNotificationSession getOffsetEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.OffsetEventEnablerReceiver offsetEventEnablerReceiver, 
                                                                                                                                org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler notification service for the given calendar. 
     *
     *  @param  offsetEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventEnablerReceiver, calendarId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerNotificationSession getOffsetEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.OffsetEventEnablerReceiver offsetEventEnablerReceiver, 
                                                                                                                                org.osid.id.Id calendarId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offset event 
     *  enabler/calendar mappings for offset event enablers. 
     *
     *  @return an <code> OffsetEventEnablerCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerCalendarSession getOffsetEventEnablerCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offset event 
     *  enabler/calendar mappings for offset event enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerCalendarSession getOffsetEventEnablerCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offset 
     *  event enablers to calendars. 
     *
     *  @return an <code> OffsetEventEnablerCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerCalendarAssignmentSession getOffsetEventEnablerCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offset 
     *  event enablers to calendars. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerCalendarAssignmentSession getOffsetEventEnablerCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offset event enabler 
     *  smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSmartCalendarSession getOffsetEventEnablerSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offset event enabler 
     *  smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerSmartCalendarSession getOffsetEventEnablerSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler mapping lookup service for looking up the rules. 
     *
     *  @return an <code> OffsetEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleLookupSession getOffsetEventEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler mapping lookup service for looking up the rules. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerCalendarRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleLookupSession getOffsetEventEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler mapping lookup service for the given calendar for looking up 
     *  rules. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleLookupSession getOffsetEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerRuleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler mapping lookup service for the given calendar for looking up 
     *  rules. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleLookupSession getOffsetEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerRuleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler assignment service to apply enablers. 
     *
     *  @return an <code> OffsetEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleApplicationSession getOffsetEventEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  assignment service to apply enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleApplicationSession getOffsetEventEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler assignment service for the given calendar to apply enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return an <code> OffsetEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleApplicationSession getOffsetEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getOffsetEventEnablerRuleApplicationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  enabler assignment service for the given calendar to apply enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerRuleApplicationSession getOffsetEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getOffsetEventEnablerRuleApplicationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler lookup service. 
     *
     *  @return a <code> SupersedingEventEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerLookupSession getSupersedingEventEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerLookupSession getSupersedingEventEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerLookupSession getSupersedingEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerLookupSession getSupersedingEventEnablerLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler query service. 
     *
     *  @return a <code> SupersedingEventEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerQuerySession getSupersedingEventEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerQuerySession getSupersedingEventEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerQuerySession getSupersedingEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerQuerySession getSupersedingEventEnablerQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler search service. 
     *
     *  @return a <code> SupersedingEventEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSearchSession getSupersedingEventEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSearchSession getSupersedingEventEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSearchSession getSupersedingEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSearchSession getSupersedingEventEnablerSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler administration service. 
     *
     *  @return a <code> SupersedingEventEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerAdminSession getSupersedingEventEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerAdminSession getSupersedingEventEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerAdminSession getSupersedingEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerAdminSession getSupersedingEventEnablerAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler notification service. 
     *
     *  @param  supersedingEventEnablerReceiver the notification callback 
     *  @return a <code> SupersedingEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerNotificationSession getSupersedingEventEnablerNotificationSession(org.osid.calendaring.rules.SupersedingEventEnablerReceiver supersedingEventEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler notification service. 
     *
     *  @param  supersedingEventEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerNotificationSession getSupersedingEventEnablerNotificationSession(org.osid.calendaring.rules.SupersedingEventEnablerReceiver supersedingEventEnablerReceiver, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler notification service for the given calendar. 
     *
     *  @param  supersedingEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerReceiver </code> or <code> calendarId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerNotificationSession getSupersedingEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.SupersedingEventEnablerReceiver supersedingEventEnablerReceiver, 
                                                                                                                                          org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler notification service for the given calendar. 
     *
     *  @param  supersedingEventEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventEnablerReceiver, calendarId, </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerNotificationSession getSupersedingEventEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.SupersedingEventEnablerReceiver supersedingEventEnablerReceiver, 
                                                                                                                                          org.osid.id.Id calendarId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup superseding event 
     *  enabler/calendar mappings for superseding event enablers. 
     *
     *  @return a <code> SupersedingEventEnablerCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerCalendarSession getSupersedingEventEnablerCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup superseding event 
     *  enabler/calendar mappings for superseding event enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerCalendarSession getSupersedingEventEnablerCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  superseding event enablers to calendars. 
     *
     *  @return a <code> SupersedingEventEnablerCalendarAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerCalendarAssignmentSession getSupersedingEventEnablerCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  superseding event enablers to calendars. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerCalendarAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerCalendarAssignmentSession getSupersedingEventEnablerCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage superseding event 
     *  enabler smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSmartCalendar() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSmartCalendarSession getSupersedingEventEnablerSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage superseding event 
     *  enabler smart calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerSmartCalendar() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerSmartCalendarSession getSupersedingEventEnablerSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler mapping lookup service for looking up the rules. 
     *
     *  @return a <code> SupersedingEventEnablerRuleSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleLookupSession getSupersedingEventEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleLookupSession getSupersedingEventEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler mapping lookup service for the given calendar for 
     *  looking up rules. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleLookupSession getSupersedingEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerRuleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler mapping lookup service for the given calendar for 
     *  looking up rules applied to a superseding event. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleLookupSession getSupersedingEventEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerRuleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler assignment service to apply enablers. 
     *
     *  @return a <code> SupersedingEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleApplicationSession getSupersedingEventEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleApplicationSession getSupersedingEventEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler assignment service for the given calendar to apply 
     *  enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> SupersedingEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleApplicationSession getSupersedingEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getSupersedingEventEnablerRuleApplicationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event enabler assignment service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerRuleApplicationSession getSupersedingEventEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getSupersedingEventEnablerRuleApplicationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler lookup service. 
     *
     *  @return a <code> CommitmentEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerLookupSession getCommitmentEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerLookupSession getCommitmentEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerLookupSession getCommitmentEnablerLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerLookupSession getCommitmentEnablerLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler query service. 
     *
     *  @return a <code> CommitmentEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerQuerySession getCommitmentEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerQuerySession getCommitmentEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerQuerySession getCommitmentEnablerQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerQuerySession getCommitmentEnablerQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler search service. 
     *
     *  @return a <code> CommitmentEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSearchSession getCommitmentEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSearchSession getCommitmentEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSearchSession getCommitmentEnablerSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enablers earch service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSearchSession getCommitmentEnablerSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler administration service. 
     *
     *  @return a <code> CommitmentEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerAdminSession getCommitmentEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerAdminSession getCommitmentEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerAdminSession getCommitmentEnablerAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerAdminSession getCommitmentEnablerAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler notification service. 
     *
     *  @param  commitmentEnablerReceiver the notification callback 
     *  @return a <code> CommitmentEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerNotificationSession getCommitmentEnablerNotificationSession(org.osid.calendaring.rules.CommitmentEnablerReceiver commitmentEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler notification service. 
     *
     *  @param  commitmentEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerNotificationSession getCommitmentEnablerNotificationSession(org.osid.calendaring.rules.CommitmentEnablerReceiver commitmentEnablerReceiver, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler notification service for the given calendar. 
     *
     *  @param  commitmentEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerReceiver </code> or <code> calendarId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerNotificationSession getCommitmentEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.CommitmentEnablerReceiver commitmentEnablerReceiver, 
                                                                                                                              org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler notification service for the given calendar. 
     *
     *  @param  commitmentEnablerReceiver the notification callback 
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no calendar found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentEnablerReceiver, calendarId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerNotificationSession getCommitmentEnablerNotificationSessionForCalendar(org.osid.calendaring.rules.CommitmentEnablerReceiver commitmentEnablerReceiver, 
                                                                                                                              org.osid.id.Id calendarId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commitment 
     *  enabler/calendar mappings for commitment enablers. 
     *
     *  @return a <code> CommitmentEnablerCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerCalendarSession getCommitmentEnablerCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup commitment 
     *  enabler/calendar mappings for commitment enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerCalendarSession getCommitmentEnablerCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  commitment enablers to calendars for commitment. 
     *
     *  @return a <code> CommitmentEnablerCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerCalendarAssignmentSession getCommitmentEnablerCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  commitment enablers to calendars for commitment. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerCalendarAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerCalendarAssignmentSession getCommitmentEnablerCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commitment enabler smart 
     *  calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSmartCalendarSession getCommitmentEnablerSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage commitment enabler smart 
     *  calendars. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerSmartCalendarSession getCommitmentEnablerSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> CommitmentEnablertRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleLookupSession getCommitmentEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  calendar. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleLookupSession getCommitmentEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler mapping lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleLookupSession getCommitmentEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerRuleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler mapping lookup service for the given calendar for looking up 
     *  rules applied to a calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleLookupSession getCommitmentEnablerRuleLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerRuleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler assignment service to apply enablers. 
     *
     *  @return a <code> CommitmentEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleApplicationSession getCommitmentEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler assignment service to apply enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleApplicationSession getCommitmentEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler assignment service for the given calendar to apply enablers. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CommitmentEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleApplicationSession getCommitmentEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesManager.getCommitmentEnablerRuleApplicationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  enabler assignment service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerRuleApplicationSession getCommitmentEnablerRuleApplicationSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.rules.CalendaringRulesProxyManager.getCommitmentEnablerRuleApplicationSessionForCalendar not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.recurringEventEnablerRecordTypes.clear();
        this.recurringEventEnablerRecordTypes.clear();

        this.recurringEventEnablerSearchRecordTypes.clear();
        this.recurringEventEnablerSearchRecordTypes.clear();

        this.offsetEventEnablerRecordTypes.clear();
        this.offsetEventEnablerRecordTypes.clear();

        this.offsetEventEnablerSearchRecordTypes.clear();
        this.offsetEventEnablerSearchRecordTypes.clear();

        this.supersedingEventEnablerRecordTypes.clear();
        this.supersedingEventEnablerRecordTypes.clear();

        this.supersedingEventEnablerSearchRecordTypes.clear();
        this.supersedingEventEnablerSearchRecordTypes.clear();

        this.commitmentEnablerRecordTypes.clear();
        this.commitmentEnablerRecordTypes.clear();

        this.commitmentEnablerSearchRecordTypes.clear();
        this.commitmentEnablerSearchRecordTypes.clear();

        return;
    }
}
