//
// AbstractDeviceSearch.java
//
//     A template for making a Device Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.device.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing device searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractDeviceSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.control.DeviceSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.control.records.DeviceSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.control.DeviceSearchOrder deviceSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of devices. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  deviceIds list of devices
     *  @throws org.osid.NullArgumentException
     *          <code>deviceIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongDevices(org.osid.id.IdList deviceIds) {
        while (deviceIds.hasNext()) {
            try {
                this.ids.add(deviceIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongDevices</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of device Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getDeviceIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  deviceSearchOrder device search order 
     *  @throws org.osid.NullArgumentException
     *          <code>deviceSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>deviceSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderDeviceResults(org.osid.control.DeviceSearchOrder deviceSearchOrder) {
	this.deviceSearchOrder = deviceSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.control.DeviceSearchOrder getDeviceSearchOrder() {
	return (this.deviceSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given device search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a device implementing the requested record.
     *
     *  @param deviceSearchRecordType a device search record
     *         type
     *  @return the device search record
     *  @throws org.osid.NullArgumentException
     *          <code>deviceSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(deviceSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.DeviceSearchRecord getDeviceSearchRecord(org.osid.type.Type deviceSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.control.records.DeviceSearchRecord record : this.records) {
            if (record.implementsRecordType(deviceSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(deviceSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this device search. 
     *
     *  @param deviceSearchRecord device search record
     *  @param deviceSearchRecordType device search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDeviceSearchRecord(org.osid.control.records.DeviceSearchRecord deviceSearchRecord, 
                                           org.osid.type.Type deviceSearchRecordType) {

        addRecordType(deviceSearchRecordType);
        this.records.add(deviceSearchRecord);        
        return;
    }
}
