//
// InvariantMapProxyLeaseLookupSession
//
//    Implements a Lease lookup service backed by a fixed
//    collection of leases. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.squatting;


/**
 *  Implements a Lease lookup service backed by a fixed
 *  collection of leases. The leases are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyLeaseLookupSession
    extends net.okapia.osid.jamocha.core.room.squatting.spi.AbstractMapLeaseLookupSession
    implements org.osid.room.squatting.LeaseLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyLeaseLookupSession} with no
     *  leases.
     *
     *  @param campus the campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyLeaseLookupSession(org.osid.room.Campus campus,
                                                  org.osid.proxy.Proxy proxy) {
        setCampus(campus);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyLeaseLookupSession} with a single
     *  lease.
     *
     *  @param campus the campus
     *  @param lease a single lease
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code lease} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyLeaseLookupSession(org.osid.room.Campus campus,
                                                  org.osid.room.squatting.Lease lease, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putLease(lease);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyLeaseLookupSession} using
     *  an array of leases.
     *
     *  @param campus the campus
     *  @param leases an array of leases
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code leases} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyLeaseLookupSession(org.osid.room.Campus campus,
                                                  org.osid.room.squatting.Lease[] leases, org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putLeases(leases);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyLeaseLookupSession} using a
     *  collection of leases.
     *
     *  @param campus the campus
     *  @param leases a collection of leases
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code leases} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyLeaseLookupSession(org.osid.room.Campus campus,
                                                  java.util.Collection<? extends org.osid.room.squatting.Lease> leases,
                                                  org.osid.proxy.Proxy proxy) {

        this(campus, proxy);
        putLeases(leases);
        return;
    }
}
