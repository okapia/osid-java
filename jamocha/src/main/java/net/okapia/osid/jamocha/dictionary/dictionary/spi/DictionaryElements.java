//
// DictionaryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.dictionary.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class DictionaryElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the DictionaryElement Id.
     *
     *  @return the dictionary element Id
     */

    public static org.osid.id.Id getDictionaryEntityId() {
        return (makeEntityId("osid.dictionary.Dictionary"));
    }


    /**
     *  Gets the EntryId element Id.
     *
     *  @return the EntryId element Id
     */

    public static org.osid.id.Id getEntryId() {
        return (makeQueryElementId("osid.dictionary.dictionary.EntryId"));
    }


    /**
     *  Gets the Entry element Id.
     *
     *  @return the Entry element Id
     */

    public static org.osid.id.Id getEntry() {
        return (makeQueryElementId("osid.dictionary.dictionary.Entry"));
    }


    /**
     *  Gets the AncestorDictionaryId element Id.
     *
     *  @return the AncestorDictionaryId element Id
     */

    public static org.osid.id.Id getAncestorDictionaryId() {
        return (makeQueryElementId("osid.dictionary.dictionary.AncestorDictionaryId"));
    }


    /**
     *  Gets the AncestorDictionary element Id.
     *
     *  @return the AncestorDictionary element Id
     */

    public static org.osid.id.Id getAncestorDictionary() {
        return (makeQueryElementId("osid.dictionary.dictionary.AncestorDictionary"));
    }


    /**
     *  Gets the DescendantDictionaryId element Id.
     *
     *  @return the DescendantDictionaryId element Id
     */

    public static org.osid.id.Id getDescendantDictionaryId() {
        return (makeQueryElementId("osid.dictionary.dictionary.DescendantDictionaryId"));
    }


    /**
     *  Gets the DescendantDictionary element Id.
     *
     *  @return the DescendantDictionary element Id
     */

    public static org.osid.id.Id getDescendantDictionary() {
        return (makeQueryElementId("osid.dictionary.dictionary.DescendantDictionary"));
    }
}
