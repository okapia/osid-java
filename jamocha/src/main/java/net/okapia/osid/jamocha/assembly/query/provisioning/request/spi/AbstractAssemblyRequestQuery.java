//
// AbstractAssemblyRequestQuery.java
//
//     A RequestQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RequestQuery that stores terms.
 */

public abstract class AbstractAssemblyRequestQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.provisioning.RequestQuery,
               org.osid.provisioning.RequestQueryInspector,
               org.osid.provisioning.RequestSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.RequestQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.RequestQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.RequestSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRequestQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRequestQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the request transaction <code> Id </code> for this query. 
     *
     *  @param  transactionId the request transaction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> transactionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestTransactionId(org.osid.id.Id transactionId, 
                                          boolean match) {
        getAssembler().addIdTerm(getRequestTransactionIdColumn(), transactionId, match);
        return;
    }


    /**
     *  Clears the request transaction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestTransactionIdTerms() {
        getAssembler().clearTerms(getRequestTransactionIdColumn());
        return;
    }


    /**
     *  Gets the request transaction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestTransactionIdTerms() {
        return (getAssembler().getIdTerms(getRequestTransactionIdColumn()));
    }


    /**
     *  Orders the results by the request transaction. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequestTransaction(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequestTransactionColumn(), style);
        return;
    }


    /**
     *  Gets the RequestTransactionId column name.
     *
     * @return the column name
     */

    protected String getRequestTransactionIdColumn() {
        return ("request_transaction_id");
    }


    /**
     *  Tests if a <code> RequestTransactionQuery </code> is available. 
     *
     *  @return <code> true </code> if a request transaction query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a request transaction. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the request transaction query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionQuery getRequestTransactionQuery() {
        throw new org.osid.UnimplementedException("supportsRequestTransactionQuery() is false");
    }


    /**
     *  Clears the request transaction query terms. 
     */

    @OSID @Override
    public void clearRequestTransactionTerms() {
        getAssembler().clearTerms(getRequestTransactionColumn());
        return;
    }


    /**
     *  Gets the request transaction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionQueryInspector[] getRequestTransactionTerms() {
        return (new org.osid.provisioning.RequestTransactionQueryInspector[0]);
    }


    /**
     *  Tests if a request transaction search order is available. 
     *
     *  @return <code> true </code> if a request transaction search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the request transaction search order. 
     *
     *  @return the request transaction search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRequestTransactionSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionSearchOrder getRequestTransactionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRequestTransactionSearchOrder() is false");
    }


    /**
     *  Gets the RequestTransaction column name.
     *
     * @return the column name
     */

    protected String getRequestTransactionColumn() {
        return ("request_transaction");
    }


    /**
     *  Sets the queue <code> Id </code> for this query. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchQueueId(org.osid.id.Id queueId, boolean match) {
        getAssembler().addIdTerm(getQueueIdColumn(), queueId, match);
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearQueueIdTerms() {
        getAssembler().clearTerms(getQueueIdColumn());
        return;
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQueueIdTerms() {
        return (getAssembler().getIdTerms(getQueueIdColumn()));
    }


    /**
     *  Orders the results by queue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQueue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQueueColumn(), style);
        return;
    }


    /**
     *  Gets the QueueId column name.
     *
     * @return the column name
     */

    protected String getQueueIdColumn() {
        return ("queue_id");
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getQueueQuery() {
        throw new org.osid.UnimplementedException("supportsQueueQuery() is false");
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearQueueTerms() {
        getAssembler().clearTerms(getQueueColumn());
        return;
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQueryInspector[] getQueueTerms() {
        return (new org.osid.provisioning.QueueQueryInspector[0]);
    }


    /**
     *  Tests if a queue search order is available. 
     *
     *  @return <code> true </code> if a queue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the queue search order. 
     *
     *  @return the queue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsQueueSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSearchOrder getQueueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsQueueSearchOrder() is false");
    }


    /**
     *  Gets the Queue column name.
     *
     * @return the column name
     */

    protected String getQueueColumn() {
        return ("queue");
    }


    /**
     *  Matches requests with a date in the given range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRequestDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getRequestDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the request date query terms. 
     */

    @OSID @Override
    public void clearRequestDateTerms() {
        getAssembler().clearTerms(getRequestDateColumn());
        return;
    }


    /**
     *  Gets the request date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getRequestDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getRequestDateColumn()));
    }


    /**
     *  Orders the results by request date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequestDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequestDateColumn(), style);
        return;
    }


    /**
     *  Gets the RequestDate column name.
     *
     * @return the column name
     */

    protected String getRequestDateColumn() {
        return ("request_date");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequesterId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getRequesterIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequesterIdTerms() {
        getAssembler().clearTerms(getRequesterIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequesterIdTerms() {
        return (getAssembler().getIdTerms(getRequesterIdColumn()));
    }


    /**
     *  Orders the results by the requester. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequester(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequesterColumn(), style);
        return;
    }


    /**
     *  Gets the RequesterId column name.
     *
     * @return the column name
     */

    protected String getRequesterIdColumn() {
        return ("requester_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequesterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequesterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRequesterQuery() {
        throw new org.osid.UnimplementedException("supportsRequesterQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearRequesterTerms() {
        getAssembler().clearTerms(getRequesterColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRequesterTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequesterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRequesterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getRequesterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRequesterSearchOrder() is false");
    }


    /**
     *  Gets the Requester column name.
     *
     * @return the column name
     */

    protected String getRequesterColumn() {
        return ("requester");
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getRequestingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestingAgentIdTerms() {
        getAssembler().clearTerms(getRequestingAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestingAgentIdTerms() {
        return (getAssembler().getIdTerms(getRequestingAgentIdColumn()));
    }


    /**
     *  Orders the results by requesting agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequestingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequestingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the RequestingAgentId column name.
     *
     * @return the column name
     */

    protected String getRequestingAgentIdColumn() {
        return ("requesting_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getRequestingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsRequestingAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearRequestingAgentTerms() {
        getAssembler().clearTerms(getRequestingAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getRequestingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRequestingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getRequestingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRequestingAgentSearchOrder() is false");
    }


    /**
     *  Gets the RequestingAgent column name.
     *
     * @return the column name
     */

    protected String getRequestingAgentColumn() {
        return ("requesting_agent");
    }


    /**
     *  Sets the pool <code> Id </code> for this query. 
     *
     *  @param  poolId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> poolId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPoolId(org.osid.id.Id poolId, boolean match) {
        getAssembler().addIdTerm(getPoolIdColumn(), poolId, match);
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPoolIdTerms() {
        getAssembler().clearTerms(getPoolIdColumn());
        return;
    }


    /**
     *  Gets the pool <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPoolIdTerms() {
        return (getAssembler().getIdTerms(getPoolIdColumn()));
    }


    /**
     *  Orders the results by pool. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPool(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPoolColumn(), style);
        return;
    }


    /**
     *  Gets the PoolId column name.
     *
     * @return the column name
     */

    protected String getPoolIdColumn() {
        return ("pool_id");
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getPoolQuery() {
        throw new org.osid.UnimplementedException("supportsPoolQuery() is false");
    }


    /**
     *  Matches requests with any pool. 
     *
     *  @param  match <code> true </code> to match requests with any poo, 
     *          <code> false </code> to match requests with no pool 
     */

    @OSID @Override
    public void matchAnyPool(boolean match) {
        getAssembler().addIdWildcardTerm(getPoolColumn(), match);
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearPoolTerms() {
        getAssembler().clearTerms(getPoolColumn());
        return;
    }


    /**
     *  Gets the pool query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQueryInspector[] getPoolTerms() {
        return (new org.osid.provisioning.PoolQueryInspector[0]);
    }


    /**
     *  Tests if a pool search order is available. 
     *
     *  @return <code> true </code> if a pool search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolSearchOrder() {
        return (false);
    }


    /**
     *  Gets the pool search order. 
     *
     *  @return the pool search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsPoolSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchOrder getPoolSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPoolSearchOrder() is false");
    }


    /**
     *  Gets the Pool column name.
     *
     * @return the column name
     */

    protected String getPoolColumn() {
        return ("pool");
    }


    /**
     *  Sets the requested provisionable <code> Id </code> for this query. 
     *
     *  @param  provisionableId the provisionable <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionableId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRequestedProvisionableId(org.osid.id.Id provisionableId, 
                                              boolean match) {
        getAssembler().addIdTerm(getRequestedProvisionableIdColumn(), provisionableId, match);
        return;
    }


    /**
     *  Clears the requested provisionable <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestedProvisionableIdTerms() {
        getAssembler().clearTerms(getRequestedProvisionableIdColumn());
        return;
    }


    /**
     *  Gets the requested provisionable <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestedProvisionableIdTerms() {
        return (getAssembler().getIdTerms(getRequestedProvisionableIdColumn()));
    }


    /**
     *  Gets the RequestedProvisionableId column name.
     *
     * @return the column name
     */

    protected String getRequestedProvisionableIdColumn() {
        return ("requested_provisionable_id");
    }


    /**
     *  Tests if a <code> ProvisionableQuery </code> is available. 
     *
     *  @return <code> true </code> if a provisionable query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestedProvisionableQuery() {
        return (false);
    }


    /**
     *  Gets the query for a requested provisionable. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the provisionable query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestedprovisionableQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQuery getRequestedProvisionableQuery() {
        throw new org.osid.UnimplementedException("supportsRequestedProvisionableQuery() is false");
    }


    /**
     *  Matches requests with any requested provisionable. 
     *
     *  @param  match <code> true </code> to match requests with any requested 
     *          provisionable, <code> false </code> to match requests with no 
     *          requested provisionables 
     */

    @OSID @Override
    public void matchAnyRequestedProvisionable(boolean match) {
        getAssembler().addIdWildcardTerm(getRequestedProvisionableColumn(), match);
        return;
    }


    /**
     *  Clears the requested provisionable query terms. 
     */

    @OSID @Override
    public void clearRequestedProvisionableTerms() {
        getAssembler().clearTerms(getRequestedProvisionableColumn());
        return;
    }


    /**
     *  Gets the requested provisionable query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQueryInspector[] getRequestedProvisionableTerms() {
        return (new org.osid.provisioning.ProvisionableQueryInspector[0]);
    }


    /**
     *  Gets the RequestedProvisionable column name.
     *
     * @return the column name
     */

    protected String getRequestedProvisionableColumn() {
        return ("requested_provisionable");
    }


    /**
     *  Sets the exchange provision <code> Id </code> for this query. 
     *
     *  @param  provisionId the provision <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchExchangeProvisionId(org.osid.id.Id provisionId, 
                                         boolean match) {
        getAssembler().addIdTerm(getExchangeProvisionIdColumn(), provisionId, match);
        return;
    }


    /**
     *  Clears the exchange provision <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearExchangeProvisionIdTerms() {
        getAssembler().clearTerms(getExchangeProvisionIdColumn());
        return;
    }


    /**
     *  Gets the exchange provision <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getExchangeProvisionIdTerms() {
        return (getAssembler().getIdTerms(getExchangeProvisionIdColumn()));
    }


    /**
     *  Orders the results by exchange resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByExchangeProvision(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getExchangeProvisionColumn(), style);
        return;
    }


    /**
     *  Gets the ExchangeProvisionId column name.
     *
     * @return the column name
     */

    protected String getExchangeProvisionIdColumn() {
        return ("exchange_provision_id");
    }


    /**
     *  Tests if a <code> ProvisionQuery </code> is available. 
     *
     *  @return <code> true </code> if a provision query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExchangeProvisionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an exchange provision. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the provision query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsExchangeProvisionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuery getExchangeProvisionQuery() {
        throw new org.osid.UnimplementedException("supportsExchangeProvisionQuery() is false");
    }


    /**
     *  Matches requests with any exchange provision. 
     *
     *  @param  match <code> true </code> to match requests with any exchange 
     *          provision, <code> false </code> to match requests with no 
     *          exchange provision 
     */

    @OSID @Override
    public void matchAnyExchangeProvision(boolean match) {
        getAssembler().addIdWildcardTerm(getExchangeProvisionColumn(), match);
        return;
    }


    /**
     *  Clears the exchange provision query terms. 
     */

    @OSID @Override
    public void clearExchangeProvisionTerms() {
        getAssembler().clearTerms(getExchangeProvisionColumn());
        return;
    }


    /**
     *  Gets the exchange provision query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQueryInspector[] getExchangeProvisionTerms() {
        return (new org.osid.provisioning.ProvisionQueryInspector[0]);
    }


    /**
     *  Tests if an exchange provision search order is available. 
     *
     *  @return <code> true </code> if a provision search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExchangeProvisionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the exchange provision search order. 
     *
     *  @return the provision search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsExchangeProvisionSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchOrder getExchangeProvisionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsExchangeProvisionSearchOrder() is false");
    }


    /**
     *  Gets the ExchangeProvision column name.
     *
     * @return the column name
     */

    protected String getExchangeProvisionColumn() {
        return ("exchange_provision");
    }


    /**
     *  Sets the origin provision <code> Id </code> for this query. 
     *
     *  @param  provisionId the provision <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOriginProvisionId(org.osid.id.Id provisionId, 
                                       boolean match) {
        getAssembler().addIdTerm(getOriginProvisionIdColumn(), provisionId, match);
        return;
    }


    /**
     *  Clears the origin provision <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOriginProvisionIdTerms() {
        getAssembler().clearTerms(getOriginProvisionIdColumn());
        return;
    }


    /**
     *  Gets the origin provision <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOriginProvisionIdTerms() {
        return (getAssembler().getIdTerms(getOriginProvisionIdColumn()));
    }


    /**
     *  Orders the results by origin provision. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOriginProvision(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOriginProvisionColumn(), style);
        return;
    }


    /**
     *  Gets the OriginProvisionId column name.
     *
     * @return the column name
     */

    protected String getOriginProvisionIdColumn() {
        return ("origin_provision_id");
    }


    /**
     *  Tests if a <code> ProvisionQuery </code> is available. 
     *
     *  @return <code> true </code> if a provision query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOriginProvisionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an origin provision. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the provision query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOriginProvisionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuery getOriginProvisionQuery() {
        throw new org.osid.UnimplementedException("supportsOriginProvisionQuery() is false");
    }


    /**
     *  Matches requests with any originating provision. 
     *
     *  @param  match <code> true </code> to match requests with any 
     *          originating provision, <code> false </code> to match requests 
     *          with no origininating provision 
     */

    @OSID @Override
    public void matchAnyOriginProvision(boolean match) {
        getAssembler().addIdWildcardTerm(getOriginProvisionColumn(), match);
        return;
    }


    /**
     *  Clears the origin provision query terms. 
     */

    @OSID @Override
    public void clearOriginProvisionTerms() {
        getAssembler().clearTerms(getOriginProvisionColumn());
        return;
    }


    /**
     *  Gets the origin provision query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQueryInspector[] getOriginProvisionTerms() {
        return (new org.osid.provisioning.ProvisionQueryInspector[0]);
    }


    /**
     *  Tests if an origin provision search order is available. 
     *
     *  @return <code> true </code> if a provision search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOriginProvisionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the origin provision search order. 
     *
     *  @return the provision search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsOriginProvisionSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchOrder getOriginProvisionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOriginProvisionSearchOrder() is false");
    }


    /**
     *  Gets the OriginProvision column name.
     *
     * @return the column name
     */

    protected String getOriginProvisionColumn() {
        return ("origin_provision");
    }


    /**
     *  Matches requests whose position is in the given range inclusive,. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchPosition(long start, long end, boolean match) {
        getAssembler().addIntegerRangeTerm(getPositionColumn(), start, end, match);
        return;
    }


    /**
     *  Matches requests with any position. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyPosition(boolean match) {
        getAssembler().addIntegerRangeWildcardTerm(getPositionColumn(), match);
        return;
    }


    /**
     *  Clears the position query terms. 
     */

    @OSID @Override
    public void clearPositionTerms() {
        getAssembler().clearTerms(getPositionColumn());
        return;
    }


    /**
     *  Gets the position query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getPositionTerms() {
        return (getAssembler().getIntegerRangeTerms(getPositionColumn()));
    }


    /**
     *  Orders the results by position. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPosition(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPositionColumn(), style);
        return;
    }


    /**
     *  Gets the Position column name.
     *
     * @return the column name
     */

    protected String getPositionColumn() {
        return ("position");
    }


    /**
     *  Matches requests whose estimated waiting time is in the given range 
     *  inclusive,. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEWA(org.osid.calendaring.Duration start, 
                         org.osid.calendaring.Duration end, boolean match) {
        getAssembler().addDurationRangeTerm(getEWAColumn(), start, end, match);
        return;
    }


    /**
     *  Matches requests with any ewa. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyEWA(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getEWAColumn(), match);
        return;
    }


    /**
     *  Clears the ewa query terms. 
     */

    @OSID @Override
    public void clearEWATerms() {
        getAssembler().clearTerms(getEWAColumn());
        return;
    }


    /**
     *  Gets the ewa query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getEWATerms() {
        return (getAssembler().getDurationRangeTerms(getEWAColumn()));
    }


    /**
     *  Orders the results by the estimated waiting time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEWA(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEWAColumn(), style);
        return;
    }


    /**
     *  Gets the EWA column name.
     *
     * @return the column name
     */

    protected String getEWAColumn() {
        return ("e_wa");
    }


    /**
     *  Sets the distributor <code> Id </code> for this query. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this request supports the given record
     *  <code>Type</code>.
     *
     *  @param  requestRecordType a request record type 
     *  @return <code>true</code> if the requestRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type requestRecordType) {
        for (org.osid.provisioning.records.RequestQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(requestRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  requestRecordType the request record type 
     *  @return the request query record 
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestQueryRecord getRequestQueryRecord(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(requestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  requestRecordType the request record type 
     *  @return the request query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestQueryInspectorRecord getRequestQueryInspectorRecord(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(requestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param requestRecordType the request record type
     *  @return the request search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestSearchOrderRecord getRequestSearchOrderRecord(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(requestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this request. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param requestQueryRecord the request query record
     *  @param requestQueryInspectorRecord the request query inspector
     *         record
     *  @param requestSearchOrderRecord the request search order record
     *  @param requestRecordType request record type
     *  @throws org.osid.NullArgumentException
     *          <code>requestQueryRecord</code>,
     *          <code>requestQueryInspectorRecord</code>,
     *          <code>requestSearchOrderRecord</code> or
     *          <code>requestRecordTyperequest</code> is
     *          <code>null</code>
     */
            
    protected void addRequestRecords(org.osid.provisioning.records.RequestQueryRecord requestQueryRecord, 
                                      org.osid.provisioning.records.RequestQueryInspectorRecord requestQueryInspectorRecord, 
                                      org.osid.provisioning.records.RequestSearchOrderRecord requestSearchOrderRecord, 
                                      org.osid.type.Type requestRecordType) {

        addRecordType(requestRecordType);

        nullarg(requestQueryRecord, "request query record");
        nullarg(requestQueryInspectorRecord, "request query inspector record");
        nullarg(requestSearchOrderRecord, "request search odrer record");

        this.queryRecords.add(requestQueryRecord);
        this.queryInspectorRecords.add(requestQueryInspectorRecord);
        this.searchOrderRecords.add(requestSearchOrderRecord);
        
        return;
    }
}
