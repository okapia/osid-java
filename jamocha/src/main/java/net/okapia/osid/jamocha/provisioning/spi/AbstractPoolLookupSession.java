//
// AbstractPoolLookupSession.java
//
//    A starter implementation framework for providing a Pool
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Pool
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPools(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPoolLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.PoolLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();
    

    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>Pool</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPools() {
        return (true);
    }


    /**
     *  A complete view of the <code>Pool</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Pool</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pools in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active pools are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePoolView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive pools are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Pool</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Pool</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Pool</code> and
     *  retained for compatibility.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolId <code>Id</code> of the
     *          <code>Pool</code>
     *  @return the pool
     *  @throws org.osid.NotFoundException <code>poolId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>poolId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Pool getPool(org.osid.id.Id poolId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.PoolList pools = getPools()) {
            while (pools.hasNext()) {
                org.osid.provisioning.Pool pool = pools.getNextPool();
                if (pool.getId().equals(poolId)) {
                    return (pool);
                }
            }
        } 

        throw new org.osid.NotFoundException(poolId + " not found");
    }


    /**
     *  Gets a <code>PoolList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  pools specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Pools</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPools()</code>.
     *
     *  @param  poolIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>poolIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByIds(org.osid.id.IdList poolIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.Pool> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = poolIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPool(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("pool " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.pool.LinkedPoolList(ret));
    }


    /**
     *  Gets a <code>PoolList</code> corresponding to the given
     *  pool genus <code>Type</code> which does not include
     *  pools of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPools()</code>.
     *
     *  @param  poolGenusType a pool genus type 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByGenusType(org.osid.type.Type poolGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.pool.PoolGenusFilterList(getPools(), poolGenusType));
    }


    /**
     *  Gets a <code>PoolList</code> corresponding to the given
     *  pool genus <code>Type</code> and include any additional
     *  pools with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPools()</code>.
     *
     *  @param  poolGenusType a pool genus type 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByParentGenusType(org.osid.type.Type poolGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPoolsByGenusType(poolGenusType));
    }


    /**
     *  Gets a <code>PoolList</code> containing the given
     *  pool record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPools()</code>.
     *
     *  @param  poolRecordType a pool record type 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByRecordType(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.pool.PoolRecordFilterList(getPools(), poolRecordType));
    }


    /**
     *  Gets a <code>PoolList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known pools or
     *  an error results. Otherwise, the returned list may contain
     *  only those pools that are accessible through this session.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Pool</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.provisioning.pool.PoolProviderFilterList(getPools(), resourceId));
    }


    /**
     *  Gets a <code>PoolList</code> by broker.
     *  
     *  In plenary mode, the returned list contains all known pools or
     *  an error results. Otherwise, the returned list may contain
     *  only those pools that are accessible through this session.
     *  
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pool are
     *  returned.
     *
     *  @param  brokerId a broker <code> Id </code> 
     *  @return the returned <code>Pool</code> list 
     *  @throws org.osid.NullArgumentException <code>brokerId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.pool.PoolFilterList(new BrokerFilter(brokerId), getPools()));
    }
        

    /**
     *  Gets all <code>Pools</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @return a list of <code>Pools</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.provisioning.PoolList getPools()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the pool list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of pools
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.provisioning.PoolList filterPoolsOnViews(org.osid.provisioning.PoolList list)
        throws org.osid.OperationFailedException {

        org.osid.provisioning.PoolList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.provisioning.pool.ActivePoolFilterList(ret);
        }

        return (ret);
    }


    public static class BrokerFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.pool.PoolFilter {

        private final org.osid.id.Id brokerId;

        
        /**
         *  Constructs a new <code>BrokerFilter</code>.
         *
         *  @param brokerId the broker to filter
         *  @throws org.osid.NullArgumentException
         *          <code>brokerId</code> is <code>null</code>
         */

        public BrokerFilter(org.osid.id.Id brokerId) {
            nullarg(brokerId, "broker Id");
            this.brokerId = brokerId;
            return;
        }


        /**
         *  Used by the PoolFilterList to filter the pool list
         *  based on broker.
         *
         *  @param pool the pool
         *  @return <code>true</code> to pass the pool,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Pool pool) {
            return (pool.getBrokerId().equals(this.brokerId));
        }
    }
}
