//
// AbstractMapProductLookupSession
//
//    A simple framework for providing a Product lookup service
//    backed by a fixed collection of products.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Product lookup service backed by a
 *  fixed collection of products. The products are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Products</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProductLookupSession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractProductLookupSession
    implements org.osid.ordering.ProductLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.ordering.Product> products = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.ordering.Product>());


    /**
     *  Makes a <code>Product</code> available in this session.
     *
     *  @param  product a product
     *  @throws org.osid.NullArgumentException <code>product<code>
     *          is <code>null</code>
     */

    protected void putProduct(org.osid.ordering.Product product) {
        this.products.put(product.getId(), product);
        return;
    }


    /**
     *  Makes an array of products available in this session.
     *
     *  @param  products an array of products
     *  @throws org.osid.NullArgumentException <code>products<code>
     *          is <code>null</code>
     */

    protected void putProducts(org.osid.ordering.Product[] products) {
        putProducts(java.util.Arrays.asList(products));
        return;
    }


    /**
     *  Makes a collection of products available in this session.
     *
     *  @param  products a collection of products
     *  @throws org.osid.NullArgumentException <code>products<code>
     *          is <code>null</code>
     */

    protected void putProducts(java.util.Collection<? extends org.osid.ordering.Product> products) {
        for (org.osid.ordering.Product product : products) {
            this.products.put(product.getId(), product);
        }

        return;
    }


    /**
     *  Removes a Product from this session.
     *
     *  @param  productId the <code>Id</code> of the product
     *  @throws org.osid.NullArgumentException <code>productId<code> is
     *          <code>null</code>
     */

    protected void removeProduct(org.osid.id.Id productId) {
        this.products.remove(productId);
        return;
    }


    /**
     *  Gets the <code>Product</code> specified by its <code>Id</code>.
     *
     *  @param  productId <code>Id</code> of the <code>Product</code>
     *  @return the product
     *  @throws org.osid.NotFoundException <code>productId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>productId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Product getProduct(org.osid.id.Id productId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ordering.Product product = this.products.get(productId);
        if (product == null) {
            throw new org.osid.NotFoundException("product not found: " + productId);
        }

        return (product);
    }


    /**
     *  Gets all <code>Products</code>. In plenary mode, the returned
     *  list contains all known products or an error
     *  results. Otherwise, the returned list may contain only those
     *  products that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Products</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProducts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.product.ArrayProductList(this.products.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.products.clear();
        super.close();
        return;
    }
}
