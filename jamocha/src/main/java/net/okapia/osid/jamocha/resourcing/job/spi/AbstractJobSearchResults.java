//
// AbstractJobSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.job.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractJobSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.JobSearchResults {

    private org.osid.resourcing.JobList jobs;
    private final org.osid.resourcing.JobQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.records.JobSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractJobSearchResults.
     *
     *  @param jobs the result set
     *  @param jobQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>jobs</code>
     *          or <code>jobQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractJobSearchResults(org.osid.resourcing.JobList jobs,
                                            org.osid.resourcing.JobQueryInspector jobQueryInspector) {
        nullarg(jobs, "jobs");
        nullarg(jobQueryInspector, "job query inspectpr");

        this.jobs = jobs;
        this.inspector = jobQueryInspector;

        return;
    }


    /**
     *  Gets the job list resulting from a search.
     *
     *  @return a job list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobs() {
        if (this.jobs == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.JobList jobs = this.jobs;
        this.jobs = null;
	return (jobs);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.JobQueryInspector getJobQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  job search record <code> Type. </code> This method must
     *  be used to retrieve a job implementing the requested
     *  record.
     *
     *  @param jobSearchRecordType a job search 
     *         record type 
     *  @return the job search
     *  @throws org.osid.NullArgumentException
     *          <code>jobSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(jobSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.JobSearchResultsRecord getJobSearchResultsRecord(org.osid.type.Type jobSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.records.JobSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(jobSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(jobSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record job search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addJobRecord(org.osid.resourcing.records.JobSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "job record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
