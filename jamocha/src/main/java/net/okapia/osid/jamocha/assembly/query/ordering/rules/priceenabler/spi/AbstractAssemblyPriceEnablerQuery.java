//
// AbstractAssemblyPriceEnablerQuery.java
//
//     A PriceEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ordering.rules.priceenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PriceEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyPriceEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.ordering.rules.PriceEnablerQuery,
               org.osid.ordering.rules.PriceEnablerQueryInspector,
               org.osid.ordering.rules.PriceEnablerSearchOrder {

    private final java.util.Collection<org.osid.ordering.rules.records.PriceEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.rules.records.PriceEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.rules.records.PriceEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPriceEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPriceEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the price. 
     *
     *  @param  priceId the price <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledPriceId(org.osid.id.Id priceId, boolean match) {
        getAssembler().addIdTerm(getRuledPriceIdColumn(), priceId, match);
        return;
    }


    /**
     *  Clears the price <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledPriceIdTerms() {
        getAssembler().clearTerms(getRuledPriceIdColumn());
        return;
    }


    /**
     *  Gets the price <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledPriceIdTerms() {
        return (getAssembler().getIdTerms(getRuledPriceIdColumn()));
    }


    /**
     *  Gets the RuledPriceId column name.
     *
     * @return the column name
     */

    protected String getRuledPriceIdColumn() {
        return ("ruled_price_id");
    }


    /**
     *  Tests if a <code> PriceQuery </code> is available. 
     *
     *  @return <code> true </code> if a price query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledPriceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the price query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledPriceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceQuery getRuledPriceQuery() {
        throw new org.osid.UnimplementedException("supportsRuledPriceQuery() is false");
    }


    /**
     *  Matches enablers mapped to any price. 
     *
     *  @param  match <code> true </code> for enablers mapped to any price, 
     *          <code> false </code> to match enablers mapped to no price 
     */

    @OSID @Override
    public void matchAnyRuledPrice(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledPriceColumn(), match);
        return;
    }


    /**
     *  Clears the price query terms. 
     */

    @OSID @Override
    public void clearRuledPriceTerms() {
        getAssembler().clearTerms(getRuledPriceColumn());
        return;
    }


    /**
     *  Gets the price query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getRuledPriceTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the RuledPrice column name.
     *
     * @return the column name
     */

    protected String getRuledPriceColumn() {
        return ("ruled_price");
    }


    /**
     *  Matches enablers mapped to the store. 
     *
     *  @param  storeId the store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        getAssembler().addIdTerm(getStoreIdColumn(), storeId, match);
        return;
    }


    /**
     *  Clears the store <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        getAssembler().clearTerms(getStoreIdColumn());
        return;
    }


    /**
     *  Gets the store <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (getAssembler().getIdTerms(getStoreIdColumn()));
    }


    /**
     *  Gets the StoreId column name.
     *
     * @return the column name
     */

    protected String getStoreIdColumn() {
        return ("store_id");
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store query terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        getAssembler().clearTerms(getStoreColumn());
        return;
    }


    /**
     *  Gets the store query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the Store column name.
     *
     * @return the column name
     */

    protected String getStoreColumn() {
        return ("store");
    }


    /**
     *  Tests if this priceEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  priceEnablerRecordType a price enabler record type 
     *  @return <code>true</code> if the priceEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type priceEnablerRecordType) {
        for (org.osid.ordering.rules.records.PriceEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(priceEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  priceEnablerRecordType the price enabler record type 
     *  @return the price enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.rules.records.PriceEnablerQueryRecord getPriceEnablerQueryRecord(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.rules.records.PriceEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(priceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  priceEnablerRecordType the price enabler record type 
     *  @return the price enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.rules.records.PriceEnablerQueryInspectorRecord getPriceEnablerQueryInspectorRecord(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.rules.records.PriceEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(priceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param priceEnablerRecordType the price enabler record type
     *  @return the price enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.rules.records.PriceEnablerSearchOrderRecord getPriceEnablerSearchOrderRecord(org.osid.type.Type priceEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.rules.records.PriceEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(priceEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this price enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param priceEnablerQueryRecord the price enabler query record
     *  @param priceEnablerQueryInspectorRecord the price enabler query inspector
     *         record
     *  @param priceEnablerSearchOrderRecord the price enabler search order record
     *  @param priceEnablerRecordType price enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>priceEnablerQueryRecord</code>,
     *          <code>priceEnablerQueryInspectorRecord</code>,
     *          <code>priceEnablerSearchOrderRecord</code> or
     *          <code>priceEnablerRecordTypepriceEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addPriceEnablerRecords(org.osid.ordering.rules.records.PriceEnablerQueryRecord priceEnablerQueryRecord, 
                                      org.osid.ordering.rules.records.PriceEnablerQueryInspectorRecord priceEnablerQueryInspectorRecord, 
                                      org.osid.ordering.rules.records.PriceEnablerSearchOrderRecord priceEnablerSearchOrderRecord, 
                                      org.osid.type.Type priceEnablerRecordType) {

        addRecordType(priceEnablerRecordType);

        nullarg(priceEnablerQueryRecord, "price enabler query record");
        nullarg(priceEnablerQueryInspectorRecord, "price enabler query inspector record");
        nullarg(priceEnablerSearchOrderRecord, "price enabler search odrer record");

        this.queryRecords.add(priceEnablerQueryRecord);
        this.queryInspectorRecords.add(priceEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(priceEnablerSearchOrderRecord);
        
        return;
    }
}
