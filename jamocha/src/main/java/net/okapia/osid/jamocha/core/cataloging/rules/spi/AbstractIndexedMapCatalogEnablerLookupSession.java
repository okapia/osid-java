//
// AbstractIndexedMapCatalogEnablerLookupSession.java
//
//    A simple framework for providing a CatalogEnabler lookup service
//    backed by a fixed collection of catalog enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CatalogEnabler lookup service backed by a
 *  fixed collection of catalog enablers. The catalog enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some catalog enablers may be compatible
 *  with more types than are indicated through these catalog enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CatalogEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCatalogEnablerLookupSession
    extends AbstractMapCatalogEnablerLookupSession
    implements org.osid.cataloging.rules.CatalogEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.cataloging.rules.CatalogEnabler> catalogEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.cataloging.rules.CatalogEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.cataloging.rules.CatalogEnabler> catalogEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.cataloging.rules.CatalogEnabler>());


    /**
     *  Makes a <code>CatalogEnabler</code> available in this session.
     *
     *  @param  catalogEnabler a catalog enabler
     *  @throws org.osid.NullArgumentException <code>catalogEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCatalogEnabler(org.osid.cataloging.rules.CatalogEnabler catalogEnabler) {
        super.putCatalogEnabler(catalogEnabler);

        this.catalogEnablersByGenus.put(catalogEnabler.getGenusType(), catalogEnabler);
        
        try (org.osid.type.TypeList types = catalogEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.catalogEnablersByRecord.put(types.getNextType(), catalogEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a catalog enabler from this session.
     *
     *  @param catalogEnablerId the <code>Id</code> of the catalog enabler
     *  @throws org.osid.NullArgumentException <code>catalogEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCatalogEnabler(org.osid.id.Id catalogEnablerId) {
        org.osid.cataloging.rules.CatalogEnabler catalogEnabler;
        try {
            catalogEnabler = getCatalogEnabler(catalogEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.catalogEnablersByGenus.remove(catalogEnabler.getGenusType());

        try (org.osid.type.TypeList types = catalogEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.catalogEnablersByRecord.remove(types.getNextType(), catalogEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCatalogEnabler(catalogEnablerId);
        return;
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> corresponding to the given
     *  catalog enabler genus <code>Type</code> which does not include
     *  catalog enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known catalog enablers or an error results. Otherwise,
     *  the returned list may contain only those catalog enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  catalogEnablerGenusType a catalog enabler genus type 
     *  @return the returned <code>CatalogEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByGenusType(org.osid.type.Type catalogEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.cataloging.rules.catalogenabler.ArrayCatalogEnablerList(this.catalogEnablersByGenus.get(catalogEnablerGenusType)));
    }


    /**
     *  Gets a <code>CatalogEnablerList</code> containing the given
     *  catalog enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known catalog enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  catalog enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  catalogEnablerRecordType a catalog enabler record type 
     *  @return the returned <code>catalogEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByRecordType(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.cataloging.rules.catalogenabler.ArrayCatalogEnablerList(this.catalogEnablersByRecord.get(catalogEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.catalogEnablersByGenus.clear();
        this.catalogEnablersByRecord.clear();

        super.close();

        return;
    }
}
