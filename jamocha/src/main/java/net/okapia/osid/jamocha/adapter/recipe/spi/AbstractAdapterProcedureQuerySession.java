//
// AbstractQueryProcedureLookupSession.java
//
//    A ProcedureQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProcedureQuerySession adapter.
 */

public abstract class AbstractAdapterProcedureQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recipe.ProcedureQuerySession {

    private final org.osid.recipe.ProcedureQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterProcedureQuerySession.
     *
     *  @param session the underlying procedure query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProcedureQuerySession(org.osid.recipe.ProcedureQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeCookbook</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeCookbook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCookbookId() {
        return (this.session.getCookbookId());
    }


    /**
     *  Gets the {@codeCookbook</code> associated with this 
     *  session.
     *
     *  @return the {@codeCookbook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCookbook());
    }


    /**
     *  Tests if this user can perform {@codeProcedure</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchProcedures() {
        return (this.session.canSearchProcedures());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include procedures in cookbooks which are children
     *  of this cookbook in the cookbook hierarchy.
     */

    @OSID @Override
    public void useFederatedCookbookView() {
        this.session.useFederatedCookbookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this cookbook only.
     */
    
    @OSID @Override
    public void useIsolatedCookbookView() {
        this.session.useIsolatedCookbookView();
        return;
    }
    
      
    /**
     *  Gets a procedure query. The returned query will not have an
     *  extension query.
     *
     *  @return the procedure query 
     */
      
    @OSID @Override
    public org.osid.recipe.ProcedureQuery getProcedureQuery() {
        return (this.session.getProcedureQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  procedureQuery the procedure query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code procedureQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code procedureQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProceduresByQuery(org.osid.recipe.ProcedureQuery procedureQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getProceduresByQuery(procedureQuery));
    }
}
