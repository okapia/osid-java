//
// EventMiter.java
//
//     Defines an Event miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.event;


/**
 *  Defines an <code>Event</code> miter for use with the builders.
 */

public interface EventMiter
    extends net.okapia.osid.jamocha.builder.spi.TemporalOsidObjectMiter,
            net.okapia.osid.jamocha.builder.spi.ContainableMiter,
            org.osid.calendaring.Event {


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this event is implicit,
     *         <code> false </code> if explicitly defined
     */

    public void setImplicit(boolean implicit);


    /**
     *  Sets the in recurring series flag.
     *
     *  @param recurring <code> true </code> if this event is part of
     *         a recurring series, <code> false </code> otherwise
     */

    public void setInRecurringSeries(boolean recurring);


    /**
     *  Sets the superseding event flag.
     *
     *  @param superseding <code> true </code> if this event is
     *         superseding event, <code> false </code> otherwise
     */

    public void setSupersedingEvent(boolean superseding);


    /**
     *  Sets the offset event flag.
     *
     *  @param offset <code> true </code> if this event is an offset
     *         event, <code> false </code> otherwise
     */

    public void setOffsetEvent(boolean offset);


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setDuration(org.osid.calendaring.Duration duration);


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    public void setLocationDescription(org.osid.locale.DisplayText description);


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public void setLocation(org.osid.mapping.Location location);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Adds an Event record.
     *
     *  @param record an event record
     *  @param recordType the type of event record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addEventRecord(org.osid.calendaring.records.EventRecord record, org.osid.type.Type recordType);
}       


