//
// InvariantMapTodoProducerLookupSession
//
//    Implements a TodoProducer lookup service backed by a fixed collection of
//    todoProducers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.mason;


/**
 *  Implements a TodoProducer lookup service backed by a fixed
 *  collection of todo producers. The todo producers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapTodoProducerLookupSession
    extends net.okapia.osid.jamocha.core.checklist.mason.spi.AbstractMapTodoProducerLookupSession
    implements org.osid.checklist.mason.TodoProducerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapTodoProducerLookupSession</code> with no
     *  todo producers.
     *  
     *  @param checklist the checklist
     *  @throws org.osid.NullArgumnetException {@code checklist} is
     *          {@code null}
     */

    public InvariantMapTodoProducerLookupSession(org.osid.checklist.Checklist checklist) {
        setChecklist(checklist);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTodoProducerLookupSession</code> with a single
     *  todo producer.
     *  
     *  @param checklist the checklist
     *  @param todoProducer a single todo producer
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todoProducer} is <code>null</code>
     */

      public InvariantMapTodoProducerLookupSession(org.osid.checklist.Checklist checklist,
                                               org.osid.checklist.mason.TodoProducer todoProducer) {
        this(checklist);
        putTodoProducer(todoProducer);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTodoProducerLookupSession</code> using an array
     *  of todo producers.
     *  
     *  @param checklist the checklist
     *  @param todoProducers an array of todo producers
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todoProducers} is <code>null</code>
     */

      public InvariantMapTodoProducerLookupSession(org.osid.checklist.Checklist checklist,
                                               org.osid.checklist.mason.TodoProducer[] todoProducers) {
        this(checklist);
        putTodoProducers(todoProducers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapTodoProducerLookupSession</code> using a
     *  collection of todo producers.
     *
     *  @param checklist the checklist
     *  @param todoProducers a collection of todo producers
     *  @throws org.osid.NullArgumentException {@code checklist} or
     *          {@code todoProducers} is <code>null</code>
     */

      public InvariantMapTodoProducerLookupSession(org.osid.checklist.Checklist checklist,
                                               java.util.Collection<? extends org.osid.checklist.mason.TodoProducer> todoProducers) {
        this(checklist);
        putTodoProducers(todoProducers);
        return;
    }
}
