//
// AbstractAllocation.java
//
//     Defines an Allocation builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.allocation.allocation.spi;


/**
 *  Defines an <code>Allocation</code> builder.
 */

public abstract class AbstractAllocationBuilder<T extends AbstractAllocationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBrowsableBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.filing.allocation.allocation.AllocationMiter allocation;


    /**
     *  Constructs a new <code>AbstractAllocationBuilder</code>.
     *
     *  @param allocation the allocation to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAllocationBuilder(net.okapia.osid.jamocha.builder.filing.allocation.allocation.AllocationMiter allocation) {
        super(allocation);
        this.allocation = allocation;
        return;
    }


    /**
     *  Builds the allocation.
     *
     *  @return the new allocation
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.filing.allocation.Allocation build() {
        (new net.okapia.osid.jamocha.builder.validator.filing.allocation.allocation.AllocationValidator(getValidations())).validate(this.allocation);
        return (new net.okapia.osid.jamocha.builder.filing.allocation.allocation.ImmutableAllocation(this.allocation));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the allocation miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.filing.allocation.allocation.AllocationMiter getMiter() {
        return (this.allocation);
    }


    /**
     *  Sets the directory.
     *
     *  @param directory a directory
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>directory</code> is <code>null</code>
     */

    public T directory(org.osid.filing.Directory directory) {
        getMiter().setDirectory(directory);
        return (self());
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Sets the total space.
     *
     *  @param bytes the total space
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>bytes</code>
     *          is negative
     */

    public T totalSpace(long bytes) {
        getMiter().setTotalSpace(bytes);
        return (self());
    }


    /**
     *  Sets the used space.
     *
     *  @param bytes the used space
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>bytes</code>
     *          is negative
     */

    public T usedSpace(long bytes) {
        getMiter().setUsedSpace(bytes);
        return (self());
    }


    /**
     *  Sets the total files.
     *
     *  @param files the total files
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>files</code>
     *          is negative
     */

    public T totalFiles(long files) {
        getMiter().setTotalFiles(files);
        return (self());
    }


    /**
     *  Sets the used files.
     *
     *  @param files the used used files
     *  @return the builder
     *  @throws org.osid.InvalieArgumentException <code>files</code>
     *          is negative
     */

    public T usedFiles(long files) {
        getMiter().setUsedFiles(files);
        return (self());
    }


    /**
     *  Adds an Allocation record.
     *
     *  @param record an allocation record
     *  @param recordType the type of allocation record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.filing.allocation.records.AllocationRecord record, org.osid.type.Type recordType) {
        getMiter().addAllocationRecord(record, recordType);
        return (self());
    }
}       


