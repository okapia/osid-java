//
// AbstractAuthorizationBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAuthorizationBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.authorization.batch.AuthorizationBatchManager,
               org.osid.authorization.batch.AuthorizationBatchProxyManager {


    /**
     *  Constructs a new
     *  <code>AbstractAuthorizationBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAuthorizationBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of authorizations is available. 
     *
     *  @return <code> true </code> if an authorization bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of functions is available. 
     *
     *  @return <code> true </code> if a function bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of qualifiers is available. 
     *
     *  @return <code> true </code> if a qualifier bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of vaults is available. 
     *
     *  @return <code> true </code> if a vault bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVaultBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  authorization administration service. 
     *
     *  @return an <code> AuthorizationBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchAdminSession getAuthorizationBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchManager.getAuthorizationBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  authorization administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchAdminSession getAuthorizationBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchProxyManager.getAuthorizationBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  authorization administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchAdminSession getAuthorizationBatchAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchManager.getAuthorizationBatchAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  authorization administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationBatchAdminSession getAuthorizationBatchAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchProxyManager.getAuthorizationBatchAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk function 
     *  administration service. 
     *
     *  @return a <code> FunctionBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.FunctionBatchAdminSession getFunctionBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchManager.getFunctionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk function 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FunctionBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.FunctionBatchAdminSession getFunctionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchProxyManager.getFunctionBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk function 
     *  administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return a <code> FunctionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.FunctionBatchAdminSession getFunctionBatchAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchManager.getFunctionBatchAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk function 
     *  administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FunctionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.FunctionBatchAdminSession getFunctionBatchAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchProxyManager.getFunctionBatchAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk qualifier 
     *  administration service. 
     *
     *  @return a <code> QualifierBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.QualifierBatchAdminSession getQualifierBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchManager.getQualifierBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk qualifier 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QualifierBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.QualifierBatchAdminSession getQualifierBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchProxyManager.getQualifierBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk qualifier 
     *  administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return a <code> QualifierBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.QualifierBatchAdminSession getQualifierBatchAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchManager.getQualifierBatchAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk qualifier 
     *  administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QualifierBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.QualifierBatchAdminSession getQualifierBatchAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchProxyManager.getQualifierBatchAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk vault 
     *  administration service. 
     *
     *  @return a <code> VaultBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.VaultBatchAdminSession getVaultBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchManager.getVaultBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk vault 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> VaultBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVaultBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.batch.VaultBatchAdminSession getVaultBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.batch.AuthorizationBatchProxyManager.getVaultBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
