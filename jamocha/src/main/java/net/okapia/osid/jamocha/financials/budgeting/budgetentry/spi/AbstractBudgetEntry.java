//
// AbstractBudgetEntry.java
//
//     Defines a BudgetEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budgetentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>BudgetEntry</code>.
 */

public abstract class AbstractBudgetEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.financials.budgeting.BudgetEntry {

    private org.osid.financials.budgeting.Budget budget;
    private org.osid.financials.Account account;
    private org.osid.financials.Currency amount;
    private boolean debit = false;

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the budget <code> Id. </code> 
     *
     *  @return the budget <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBudgetId() {
        return (this.budget.getId());
    }


    /**
     *  Gets the budget. 
     *
     *  @return the budget 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.budgeting.Budget getBudget()
        throws org.osid.OperationFailedException {

        return (this.budget);
    }


    /**
     *  Sets the budget.
     *
     *  @param budget a budget
     *  @throws org.osid.NullArgumentException
     *          <code>budget</code> is <code>null</code>
     */

    protected void setBudget(org.osid.financials.budgeting.Budget budget) {
        nullarg(budget, "budget");
        this.budget = budget;
        return;
    }


    /**
     *  Gets the account <code> Id. </code> 
     *
     *  @return the account <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAccountId() {
        return (this.account.getId());
    }


    /**
     *  Gets the account. 
     *
     *  @return the account 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount()
        throws org.osid.OperationFailedException {

        return (this.account);
    }


    /**
     *  Sets the account.
     *
     *  @param account an account
     *  @throws org.osid.NullArgumentException
     *          <code>account</code> is <code>null</code>
     */

    protected void setAccount(org.osid.financials.Account account) {
        nullarg(account, "account");
        this.account = account;
        return;
    }


    /**
     *  Gets the amount of this budget entries. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.amount);
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException
     *          <code>amount</code> is <code>null</code>
     */

    protected void setAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "amount");
        this.amount = amount;
        return;
    }


    /**
     *  Tests if the budgeted amount is to be debited or a credited to
     *  this activity.
     *
     *  @return <code> true </code> if this item amount is a debit,
     *          <code> false </code> if it is a credit
     */

    @OSID @Override
    public boolean isDebit() {
        return (this.debit);
    }


    /**
     *  Sets the debit flag.
     *
     *  @param debit <code> true </code> if this item amount is a
     *         debit, <code> false </code> if it is a credit
     */

    protected void setDebit(boolean debit) {
        this.debit = debit;
        return;
    }


    /**
     *  Tests if this budgetEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  budgetEntryRecordType a budget entry record type 
     *  @return <code>true</code> if the budgetEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type budgetEntryRecordType) {
        for (org.osid.financials.budgeting.records.BudgetEntryRecord record : this.records) {
            if (record.implementsRecordType(budgetEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>BudgetEntry</code> record <code>Type</code>.
     *
     *  @param  budgetEntryRecordType the budget entry record type 
     *  @return the budget entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetEntryRecord getBudgetEntryRecord(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetEntryRecord record : this.records) {
            if (record.implementsRecordType(budgetEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this budget entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param budgetEntryRecord the budget entry record
     *  @param budgetEntryRecordType budget entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecord</code> or
     *          <code>budgetEntryRecordTypebudgetEntry</code> is
     *          <code>null</code>
     */
            
    protected void addBudgetEntryRecord(org.osid.financials.budgeting.records.BudgetEntryRecord budgetEntryRecord, 
                                        org.osid.type.Type budgetEntryRecordType) {
        
        nullarg(budgetEntryRecord, "budget entry record");
        addRecordType(budgetEntryRecordType);
        this.records.add(budgetEntryRecord);
        
        return;
    }
}
