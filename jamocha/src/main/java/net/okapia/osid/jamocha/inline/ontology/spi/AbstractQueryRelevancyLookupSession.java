//
// AbstractQueryRelevancyLookupSession.java
//
//    An inline adapter that maps a RelevancyLookupSession to
//    a RelevancyQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a RelevancyLookupSession to
 *  a RelevancyQuerySession.
 */

public abstract class AbstractQueryRelevancyLookupSession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractRelevancyLookupSession
    implements org.osid.ontology.RelevancyLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.ontology.RelevancyQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryRelevancyLookupSession.
     *
     *  @param querySession the underlying relevancy query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryRelevancyLookupSession(org.osid.ontology.RelevancyQuerySession querySession) {
        nullarg(querySession, "relevancy query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Ontology</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Ontology Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.session.getOntologyId());
    }


    /**
     *  Gets the <code>Ontology</code> associated with this 
     *  session.
     *
     *  @return the <code>Ontology</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOntology());
    }


    /**
     *  Tests if this user can perform <code>Relevancy</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRelevancies() {
        return (this.session.canSearchRelevancies());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relevancies in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.session.useFederatedOntologyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this ontology only.
     */

    @OSID @Override
    public void useIsolatedOntologyView() {
        this.session.useIsolatedOntologyView();
        return;
    }
    

    /**
     *  Only relevancies whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRelevancyView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All relevancies of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRelevancyView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Relevancy</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Relevancy</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Relevancy</code> and
     *  retained for compatibility.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyId <code>Id</code> of the
     *          <code>Relevancy</code>
     *  @return the relevancy
     *  @throws org.osid.NotFoundException <code>relevancyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>relevancyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Relevancy getRelevancy(org.osid.id.Id relevancyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchId(relevancyId, true);
        org.osid.ontology.RelevancyList relevancies = this.session.getRelevanciesByQuery(query);
        if (relevancies.hasNext()) {
            return (relevancies.getNextRelevancy());
        } 
        
        throw new org.osid.NotFoundException(relevancyId + " not found");
    }


    /**
     *  Gets a <code>RelevancyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relevancies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Relevancies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByIds(org.osid.id.IdList relevancyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();

        try (org.osid.id.IdList ids = relevancyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a <code>RelevancyList</code> corresponding to the given
     *  relevancy genus <code>Type</code> which does not include
     *  relevancies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyGenusType a relevancy genus type 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchGenusType(relevancyGenusType, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a <code>RelevancyList</code> corresponding to the given
     *  relevancy genus <code>Type</code> and include any additional
     *  relevancies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyGenusType a relevancy genus type 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByParentGenusType(org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchParentGenusType(relevancyGenusType, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a <code>RelevancyList</code> containing the given
     *  relevancy record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  relevancyRecordType a relevancy record type 
     *  @return the returned <code>Relevancy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByRecordType(org.osid.type.Type relevancyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchRecordType(relevancyRecordType, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a <code>RelevancyList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Relevancy</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesOnDate(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getRelevanciesByQuery(query));
    }
        

    /**
     *  Gets a list of relevancies corresponding to a subject
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the <code>Id</code> of the subject
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>subjectId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.ontology.RelevancyList getRelevanciesForSubject(org.osid.id.Id subjectId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchSubjectId(subjectId, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a list of relevancies corresponding to a subject
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the <code>Id</code> of the subject
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>subjectId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectOnDate(org.osid.id.Id subjectId,
                                                                          org.osid.calendaring.DateTime from,
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchSubjectId(subjectId, true);
        query.matchDate(from, to, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets the <code> Relevancy </code> mapped to a subject <code>
     *  Id </code> and relevancy <code> genus Type. </code> Genus
     *  <code> Types </code> derived from the given genus <code> Typ
     *  </code> e are included.
     *  
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code> Relevancy </code> may have a different <code> Id
     *  </code> than requested, such as the case where a duplicate
     *  <code> Id </code> was assigned to a <code> Relevancy </code>
     *  and retained for compatibility.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectId the subject <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> or 
     *          <code> relevancyGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubject(org.osid.id.Id subjectId, 
                                                                               org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchSubjectId(subjectId, true);
        query.matchGenusType(relevancyGenusType, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a <code> RelevancyList </code> of the given genus type
     *  for the given subject effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectId the subject <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> subjectId,
     *          relevancyGenusType, from </code> or <code> to </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectOnDate(org.osid.id.Id subjectId, 
                                                                                     org.osid.type.Type relevancyGenusType, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchSubjectId(subjectId, true);
        query.matchGenusType(relevancyGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets the relevancies for the given subject <code> Ids. </code>
     *  
     *  In plenary mode, the returned list contains all of the
     *  relevancies specified in the subject <code> Id </code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if a relevancy <code> Id </code> in the supplied list
     *  is not found or inaccessible.  Otherwise, inaccessible
     *  relevancies may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectIds list of subject <code> Ids </code> 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code> subjectIds </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjects(org.osid.id.IdList subjectIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        try (org.osid.id.IdList ids = subjectIds) {
            while (ids.hasNext()) {
                query.matchSubjectId(ids.getNextId(), true);
            }
        }
        
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a list of relevancies corresponding to a mapped Id
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  mappedId the <code>Id</code> of the mapped Id
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>mappedId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.ontology.RelevancyList getRelevanciesForMappedId(org.osid.id.Id mappedId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchMappedId(mappedId, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a list of relevancies corresponding to a mapped Id
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  mappedId the <code>Id</code> of the mapped Id
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>mappedId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForMappedIdOnDate(org.osid.id.Id mappedId,
                                                                           org.osid.calendaring.DateTime from,
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchMappedId(mappedId, true);
        query.matchDate(from, to, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets the <code> Relevancy </code> elements mapped to an <code>
     *  Id </code> of the given relevancy genus <code> Type </code>
     *  which includes derived genus <code> Types. </code>
     *  
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code> Relevancy </code> may have a different <code> Id
     *  </code> than requested, such as the case where a duplicate
     *  <code> Id </code> was assigned to a <code> Relevancy </code>
     *  and retained for compatibility.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  id an <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code> id </code> or <code> 
     *          relevancyGenusType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForMappedId(org.osid.id.Id id, 
                                                                                org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchMappedId(id, true);
        query.matchGenusType(relevancyGenusType, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a <code> RelevancyList </code> of the given genus type
     *  for the given mapped <code> Id </code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  id an <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> id,
     *          relevancyGenusType, from </code> or <code> to </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForMappedIdOnDate(org.osid.id.Id id, 
                                                                                      org.osid.type.Type relevancyGenusType, 
                                                                                      org.osid.calendaring.DateTime from, 
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchMappedId(id, true);
        query.matchGenusType(relevancyGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets the relevancies for the given mapped <code> Ids. </code>
     *  
     *  In plenary mode, the returned list contains all of the
     *  relevancies mapped to the <code> Id </code> or an error
     *  results if an <code> Id </code> in the supplied list is not
     *  found or inaccessible.  Otherwise, inaccessible relevancies
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  mappedIds a list of <code> Ids </code> 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code> mappedIds
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForMappedIds(org.osid.id.IdList mappedIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        try (org.osid.id.IdList ids = mappedIds) {
            while (ids.hasNext()) {
                query.matchMappedId(ids.getNextId(), true);
            }
        }
        
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a list of relevancies corresponding to subject and mapped
     *  Id <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the <code>Id</code> of the subject
     *  @param  mappedId the <code>Id</code> of the mapped Id
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>subjectId</code>,
     *          <code>mappedId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectAndMappedId(org.osid.id.Id subjectId,
                                                                               org.osid.id.Id mappedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchSubjectId(subjectId, true);
        query.matchMappedId(mappedId, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a list of relevancies corresponding to subject and mapped Id
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible
     *  through this session.
     *
     *  In effective mode, relevancies are returned that are
     *  currently effective.  In any effective mode, effective
     *  relevancies and those currently expired are returned.
     *
     *  @param  subjectId the <code>Id</code> of the subject
     *  @param  mappedId the <code>Id</code> of the mapped Id
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelevancyList</code>
     *  @throws org.osid.NullArgumentException <code>subjectId</code>,
     *          <code>mappedId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesForSubjectAndMappedIdOnDate(org.osid.id.Id subjectId,
                                                                                     org.osid.id.Id mappedId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchSubjectId(subjectId, true);
        query.matchMappedId(mappedId, true);
        query.matchDate(from, to, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets the <code> Relevancy </code> of the given genus type and
     *  mapped to a subject and mapped <code> Id. </code>
     *  
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code> Relevancy </code> may have a different <code> Id
     *  </code> than requested, such as the case where a duplicate
     *  <code> Id </code> was assigned to a <code> Relevancy </code>
     *  and retained for compatibility.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectId the subject <code> Id </code> 
     *  @param  id the mapped <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @return list of relevancies 
     *  @throws org.osid.NullArgumentException <code> subjectId, id
     *          </code> , or <code> relevancyGenusType </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectAndMappedId(org.osid.id.Id subjectId, 
                                                                                          org.osid.id.Id id, 
                                                                                          org.osid.type.Type relevancyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchSubjectId(subjectId, true);
        query.matchMappedId(id, true);
        query.matchGenusType(relevancyGenusType, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets a <code> RelevancyList </code> of the given genus type
     *  and related to the given subject and mapped <code> Id </code>
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session.
     *  
     *  In effective mode, relevancies are returned that are currently
     *  effective. In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @param  subjectId the subject <code> Id </code> 
     *  @param  id the mapped <code> Id </code> 
     *  @param  relevancyGenusType relevancy genus type 
     *  @param  from a starting date 
     *  @param  to an ending date 
     *  @return list of relevancies 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> subjectId, id,
     *          relevancyGenusTYpe, from </code> or <code> to </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByGenusTypeForSubjectAndMappedIdOnDate(org.osid.id.Id subjectId, 
                                                                                                org.osid.id.Id id, 
                                                                                                org.osid.type.Type relevancyGenusType, 
                                                                                                org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchSubjectId(subjectId, true);
        query.matchMappedId(id, true);
        query.matchGenusType(relevancyGenusType, true);
        query.matchDate(from, to, true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets all <code>Relevancies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  relevancies or an error results. Otherwise, the returned list
     *  may contain only those relevancies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relevancies are returned that are currently
     *  effective.  In any effective mode, effective relevancies and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Relevancies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevancies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.RelevancyQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getRelevanciesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.ontology.RelevancyQuery getQuery() {
        org.osid.ontology.RelevancyQuery query = this.session.getRelevancyQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
