//
// AbstractMapTriggerEnablerLookupSession
//
//    A simple framework for providing a TriggerEnabler lookup service
//    backed by a fixed collection of trigger enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a TriggerEnabler lookup service backed by a
 *  fixed collection of trigger enablers. The trigger enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>TriggerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapTriggerEnablerLookupSession
    extends net.okapia.osid.jamocha.control.rules.spi.AbstractTriggerEnablerLookupSession
    implements org.osid.control.rules.TriggerEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.rules.TriggerEnabler> triggerEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.rules.TriggerEnabler>());


    /**
     *  Makes a <code>TriggerEnabler</code> available in this session.
     *
     *  @param  triggerEnabler a trigger enabler
     *  @throws org.osid.NullArgumentException <code>triggerEnabler<code>
     *          is <code>null</code>
     */

    protected void putTriggerEnabler(org.osid.control.rules.TriggerEnabler triggerEnabler) {
        this.triggerEnablers.put(triggerEnabler.getId(), triggerEnabler);
        return;
    }


    /**
     *  Makes an array of trigger enablers available in this session.
     *
     *  @param  triggerEnablers an array of trigger enablers
     *  @throws org.osid.NullArgumentException <code>triggerEnablers<code>
     *          is <code>null</code>
     */

    protected void putTriggerEnablers(org.osid.control.rules.TriggerEnabler[] triggerEnablers) {
        putTriggerEnablers(java.util.Arrays.asList(triggerEnablers));
        return;
    }


    /**
     *  Makes a collection of trigger enablers available in this session.
     *
     *  @param  triggerEnablers a collection of trigger enablers
     *  @throws org.osid.NullArgumentException <code>triggerEnablers<code>
     *          is <code>null</code>
     */

    protected void putTriggerEnablers(java.util.Collection<? extends org.osid.control.rules.TriggerEnabler> triggerEnablers) {
        for (org.osid.control.rules.TriggerEnabler triggerEnabler : triggerEnablers) {
            this.triggerEnablers.put(triggerEnabler.getId(), triggerEnabler);
        }

        return;
    }


    /**
     *  Removes a TriggerEnabler from this session.
     *
     *  @param  triggerEnablerId the <code>Id</code> of the trigger enabler
     *  @throws org.osid.NullArgumentException <code>triggerEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeTriggerEnabler(org.osid.id.Id triggerEnablerId) {
        this.triggerEnablers.remove(triggerEnablerId);
        return;
    }


    /**
     *  Gets the <code>TriggerEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  triggerEnablerId <code>Id</code> of the <code>TriggerEnabler</code>
     *  @return the triggerEnabler
     *  @throws org.osid.NotFoundException <code>triggerEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>triggerEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnabler getTriggerEnabler(org.osid.id.Id triggerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.rules.TriggerEnabler triggerEnabler = this.triggerEnablers.get(triggerEnablerId);
        if (triggerEnabler == null) {
            throw new org.osid.NotFoundException("triggerEnabler not found: " + triggerEnablerId);
        }

        return (triggerEnabler);
    }


    /**
     *  Gets all <code>TriggerEnablers</code>. In plenary mode, the returned
     *  list contains all known triggerEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  triggerEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>TriggerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.triggerenabler.ArrayTriggerEnablerList(this.triggerEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.triggerEnablers.clear();
        super.close();
        return;
    }
}
