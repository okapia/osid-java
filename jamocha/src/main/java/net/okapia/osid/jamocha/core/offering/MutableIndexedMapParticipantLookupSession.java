//
// MutableIndexedMapParticipantLookupSession
//
//    Implements a Participant lookup service backed by a collection of
//    participants indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering;


/**
 *  Implements a Participant lookup service backed by a collection of
 *  participants. The participants are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some participants may be compatible
 *  with more types than are indicated through these participant
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of participants can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapParticipantLookupSession
    extends net.okapia.osid.jamocha.core.offering.spi.AbstractIndexedMapParticipantLookupSession
    implements org.osid.offering.ParticipantLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapParticipantLookupSession} with no participants.
     *
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumentException {@code catalogue}
     *          is {@code null}
     */

      public MutableIndexedMapParticipantLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParticipantLookupSession} with a
     *  single participant.
     *  
     *  @param catalogue the catalogue
     *  @param  participant a single participant
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code participant} is {@code null}
     */

    public MutableIndexedMapParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.Participant participant) {
        this(catalogue);
        putParticipant(participant);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParticipantLookupSession} using an
     *  array of participants.
     *
     *  @param catalogue the catalogue
     *  @param  participants an array of participants
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code participants} is {@code null}
     */

    public MutableIndexedMapParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.Participant[] participants) {
        this(catalogue);
        putParticipants(participants);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapParticipantLookupSession} using a
     *  collection of participants.
     *
     *  @param catalogue the catalogue
     *  @param  participants a collection of participants
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code participants} is {@code null}
     */

    public MutableIndexedMapParticipantLookupSession(org.osid.offering.Catalogue catalogue,
                                                  java.util.Collection<? extends org.osid.offering.Participant> participants) {

        this(catalogue);
        putParticipants(participants);
        return;
    }
    

    /**
     *  Makes a {@code Participant} available in this session.
     *
     *  @param  participant a participant
     *  @throws org.osid.NullArgumentException {@code participant{@code  is
     *          {@code null}
     */

    @Override
    public void putParticipant(org.osid.offering.Participant participant) {
        super.putParticipant(participant);
        return;
    }


    /**
     *  Makes an array of participants available in this session.
     *
     *  @param  participants an array of participants
     *  @throws org.osid.NullArgumentException {@code participants{@code 
     *          is {@code null}
     */

    @Override
    public void putParticipants(org.osid.offering.Participant[] participants) {
        super.putParticipants(participants);
        return;
    }


    /**
     *  Makes collection of participants available in this session.
     *
     *  @param  participants a collection of participants
     *  @throws org.osid.NullArgumentException {@code participant{@code  is
     *          {@code null}
     */

    @Override
    public void putParticipants(java.util.Collection<? extends org.osid.offering.Participant> participants) {
        super.putParticipants(participants);
        return;
    }


    /**
     *  Removes a Participant from this session.
     *
     *  @param participantId the {@code Id} of the participant
     *  @throws org.osid.NullArgumentException {@code participantId{@code  is
     *          {@code null}
     */

    @Override
    public void removeParticipant(org.osid.id.Id participantId) {
        super.removeParticipant(participantId);
        return;
    }    
}
