//
// AbstractMapProgramLookupSession
//
//    A simple framework for providing a Program lookup service
//    backed by a fixed collection of programs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Program lookup service backed by a
 *  fixed collection of programs. The programs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Programs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapProgramLookupSession
    extends net.okapia.osid.jamocha.course.program.spi.AbstractProgramLookupSession
    implements org.osid.course.program.ProgramLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.program.Program> programs = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.program.Program>());


    /**
     *  Makes a <code>Program</code> available in this session.
     *
     *  @param  program a program
     *  @throws org.osid.NullArgumentException <code>program<code>
     *          is <code>null</code>
     */

    protected void putProgram(org.osid.course.program.Program program) {
        this.programs.put(program.getId(), program);
        return;
    }


    /**
     *  Makes an array of programs available in this session.
     *
     *  @param  programs an array of programs
     *  @throws org.osid.NullArgumentException <code>programs<code>
     *          is <code>null</code>
     */

    protected void putPrograms(org.osid.course.program.Program[] programs) {
        putPrograms(java.util.Arrays.asList(programs));
        return;
    }


    /**
     *  Makes a collection of programs available in this session.
     *
     *  @param  programs a collection of programs
     *  @throws org.osid.NullArgumentException <code>programs<code>
     *          is <code>null</code>
     */

    protected void putPrograms(java.util.Collection<? extends org.osid.course.program.Program> programs) {
        for (org.osid.course.program.Program program : programs) {
            this.programs.put(program.getId(), program);
        }

        return;
    }


    /**
     *  Removes a Program from this session.
     *
     *  @param  programId the <code>Id</code> of the program
     *  @throws org.osid.NullArgumentException <code>programId<code> is
     *          <code>null</code>
     */

    protected void removeProgram(org.osid.id.Id programId) {
        this.programs.remove(programId);
        return;
    }


    /**
     *  Gets the <code>Program</code> specified by its <code>Id</code>.
     *
     *  @param  programId <code>Id</code> of the <code>Program</code>
     *  @return the program
     *  @throws org.osid.NotFoundException <code>programId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>programId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram(org.osid.id.Id programId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.program.Program program = this.programs.get(programId);
        if (program == null) {
            throw new org.osid.NotFoundException("program not found: " + programId);
        }

        return (program);
    }


    /**
     *  Gets all <code>Programs</code>. In plenary mode, the returned
     *  list contains all known programs or an error
     *  results. Otherwise, the returned list may contain only those
     *  programs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Programs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getPrograms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.program.ArrayProgramList(this.programs.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.programs.clear();
        super.close();
        return;
    }
}
