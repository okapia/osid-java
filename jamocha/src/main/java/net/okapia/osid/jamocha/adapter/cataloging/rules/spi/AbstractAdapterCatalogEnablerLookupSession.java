//
// AbstractAdapterCatalogEnablerLookupSession.java
//
//    A CatalogEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CatalogEnabler lookup session adapter.
 */

public abstract class AbstractAdapterCatalogEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.cataloging.rules.CatalogEnablerLookupSession {

    private final org.osid.cataloging.rules.CatalogEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCatalogEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCatalogEnablerLookupSession(org.osid.cataloging.rules.CatalogEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogId() {
        return (this.session.getCatalogId());
    }


    /**
     *  Gets the {@code Catalog} associated with this session.
     *
     *  @return the {@code Catalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.Catalog getCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalog());
    }


    /**
     *  Tests if this user can perform {@code CatalogEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCatalogEnablers() {
        return (this.session.canLookupCatalogEnablers());
    }


    /**
     *  A complete view of the {@code CatalogEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCatalogEnablerView() {
        this.session.useComparativeCatalogEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code CatalogEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCatalogEnablerView() {
        this.session.usePlenaryCatalogEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include catalog enablers in catalogs which are children
     *  of this catalog in the catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogView() {
        this.session.useFederatedCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalog only.
     */

    @OSID @Override
    public void useIsolatedCatalogView() {
        this.session.useIsolatedCatalogView();
        return;
    }
    

    /**
     *  Only active catalog enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCatalogEnablerView() {
        this.session.useActiveCatalogEnablerView();
        return;
    }


    /**
     *  Active and inactive catalog enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCatalogEnablerView() {
        this.session.useAnyStatusCatalogEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code CatalogEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CatalogEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CatalogEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @param catalogEnablerId {@code Id} of the {@code CatalogEnabler}
     *  @return the catalog enabler
     *  @throws org.osid.NotFoundException {@code catalogEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code catalogEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnabler getCatalogEnabler(org.osid.id.Id catalogEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogEnabler(catalogEnablerId));
    }


    /**
     *  Gets a {@code CatalogEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  catalogEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CatalogEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @param  catalogEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CatalogEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code catalogEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByIds(org.osid.id.IdList catalogEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogEnablersByIds(catalogEnablerIds));
    }


    /**
     *  Gets a {@code CatalogEnablerList} corresponding to the given
     *  catalog enabler genus {@code Type} which does not include
     *  catalog enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @param  catalogEnablerGenusType a catalogEnabler genus type 
     *  @return the returned {@code CatalogEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByGenusType(org.osid.type.Type catalogEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogEnablersByGenusType(catalogEnablerGenusType));
    }


    /**
     *  Gets a {@code CatalogEnablerList} corresponding to the given
     *  catalog enabler genus {@code Type} and include any additional
     *  catalog enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @param  catalogEnablerGenusType a catalogEnabler genus type 
     *  @return the returned {@code CatalogEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByParentGenusType(org.osid.type.Type catalogEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogEnablersByParentGenusType(catalogEnablerGenusType));
    }


    /**
     *  Gets a {@code CatalogEnablerList} containing the given
     *  catalog enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @param  catalogEnablerRecordType a catalogEnabler record type 
     *  @return the returned {@code CatalogEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByRecordType(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogEnablersByRecordType(catalogEnablerRecordType));
    }


    /**
     *  Gets a {@code CatalogEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible
     *  through this session.
     *  
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CatalogEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code CatalogEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible
     *  through this session.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code CatalogEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getCatalogEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code CatalogEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  catalog enablers or an error results. Otherwise, the returned list
     *  may contain only those catalog enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, catalog enablers are returned that are currently
     *  active. In any status mode, active and inactive catalog enablers
     *  are returned.
     *
     *  @return a list of {@code CatalogEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogEnablers());
    }
}
