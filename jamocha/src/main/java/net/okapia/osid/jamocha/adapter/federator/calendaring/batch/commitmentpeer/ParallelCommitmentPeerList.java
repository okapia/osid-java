//
// ParallelCommitmentPeerList.java
//
//     Implements a CommitmentPeerList. This list reads from 
//     multiple lists.
//
//
// Tom Coppeto
// Okapia
// 17 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.batch.commitmentpeer;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a <code>CommitmentPeerList</code>. This list allows
 *  CommitmentPeerLists to be added a bit at a time and are read off in
 *  parallel. <code>noMore</code> must be invoked when all the lists
 *  have been added.
 *
 *  Lists that are added to added to this list are automatically
 *  wrapped in a <code>BufferedCommitmentPeerList</code>.
 *
 *  Two error handlers are offered. A hard error throws an exception
 *  for any error received by an underlying list. A soft error ignores
 *  errors in underlying lists and proceeds to the next list.
 */

public class ParallelCommitmentPeerList
    extends net.okapia.osid.jamocha.calendaring.batch.commitmentpeer.spi.AbstractCommitmentPeerList
    implements org.osid.calendaring.batch.CommitmentPeerList,
               net.okapia.osid.jamocha.adapter.federator.calendaring.batch.commitmentpeer.FederatingCommitmentPeerList,
               net.okapia.osid.jamocha.inline.calendaring.batch.commitmentpeer.CommitmentPeerListCallback {

    private final java.util.Collection<CommitmentPeerListReader> readers = java.util.Collections.synchronizedList(new java.util.LinkedList<CommitmentPeerListReader>());
    private int bufferSize = 10;
    private boolean soft = false;
    private boolean eol = false;


    /**
     *  Creates a new empty <code>ParallelCommitmentPeerList</code>. The
     *  default buffer size is 10 elemets per list and underlying
     *  errors fail hard.
     */

    public ParallelCommitmentPeerList() {
        return;
    }


    /**
     *  Creates a new empty <code>ParallelCommitmentPeerList</code>.
     *
     *  @param bufferSize the number of elements to buffer for each
     *         list
     *  @param soft <code>true</code> to fail soft, <code>false</code>
     *         to fail hard
     */

    public ParallelCommitmentPeerList(int bufferSize, boolean soft) {
        this.bufferSize = bufferSize;
        this.soft = soft;
        return;
    }


    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in this 
     *          list, <code> false </code> if the end of the list has been 
     *          reached 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        while (true) {
            if (this.eol && (this.readers.size() == 0)) {
                return (false);
            }
            
            for (CommitmentPeerListReader reader : this.readers) {
                if (!reader.isFinished()) {
                    return (true);
                }
            }

            synchronized(this.readers) {
                try {
                    this.readers.wait(100);
                } catch (InterruptedException ie) { }
            }
        }
    }


    /**
     *  Gets the number of elements available for retrieval. The
     *  number returned by this method may be less than or equal to
     *  the total number of elements in this list. To determine if the
     *  end of the list has been reached, the method <code> hasNext()
     *  </code> should be used. This method conveys what is known
     *  about the number of remaining elements at a point in time and
     *  can be used to determine a minimum size of the remaining
     *  elements, if known. A valid return is zero even if <code>
     *  hasNext() </code> is true.
     *  
     *  This method does not imply asynchronous usage. All OSID
     *  methods may block.
     *
     *  @return the number of elements available for retrieval 
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public long available() {
        int total = 0;
        for (CommitmentPeerListReader reader : this.readers) {
            total += reader.available();
        }

        return (total);
    }


    /**
     *  Skip the specified number of elements in the list. If the
     *  number skipped is greater than the number of elements in the
     *  list, hasNext() becomes false and available() returns zero as
     *  there are no more elements to retrieve.
     *  
     *  This method makes no sense in a randomly parallel iterator.
     *
     *  @param n the number of elements to skip
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public void skip(long n) {
        long skipped = 0;
        
        while (skipped < n) {
            if (this.eol && (this.readers.size() == 0)) {
                return;
            }

            for (CommitmentPeerListReader reader : this.readers) {
                if (!reader.isFinished()) {
                    if (n > skipped) {
                        long s = reader.available();
                        if ((s + skipped) > n) {
                            s = n - skipped;
                        }
                        
                        reader.skip(s);
                        skipped += s;
                    }
                } 
            }

            synchronized (this.readers) {
                try {
                    this.readers.wait(100);
                } catch (InterruptedException ie) { }
            }
        }

        return;
    }


    /**
     *  Gets the next <code> CommitmentPeer </code> in this list. 
     *
     *  @return the next <code> CommitmentPeer </code> in this list. The
     *          <code> hasNext() </code> method should be used to test
     *          that a next <code> CommitmentPeer </code> is available
     *          before calling this method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     */

    @OSID @Override
    public org.osid.calendaring.batch.CommitmentPeer getNextCommitmentPeer()
        throws org.osid.OperationFailedException {

        while (true) {
            if (this.eol && (this.readers.size() == 0)) {
                throw new org.osid.IllegalStateException("no more elements in list");
            }

            for (CommitmentPeerListReader reader : this.readers) {
                try {
                    if (reader.ready()) {
                        return (reader.getNextCommitmentPeer());
                    } 
                } catch (org.osid.OperationFailedException ofe) {
                    if (!reader.getSoftError()) {
                        throw ofe;
                    }
                    
                    this.readers.remove(reader);
                }
            }
                
            synchronized (this.readers) {
                try {
                    this.readers.wait(100);
                } catch (InterruptedException ie) { }
            }
        }
    }

        
    /**
     *  Gets the next set of <code> CommitmentPeer </code> elements in this
     *  list. The specified amount must be less than or equal to the
     *  return from <code> available(). </code>
     *
     *  @param n the number of <code> CommitmentPeer </code> elements
     *          requested which must be less than or equal to <code>
     *          available() </code>
     *  @return an array of <code> CommitmentPeer </code> elements. <code>
     *          </code> The length of the array is less than or equal
     *          to the number specified.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.InvalidArgumentException <code>n</code> is
     *          less than zero
     */

    @OSID @Override
    public org.osid.calendaring.batch.CommitmentPeer[] getNextCommitmentPeers(long n)
        throws org.osid.IllegalStateException,
               org.osid.OperationFailedException {
        
        java.util.Collection<org.osid.calendaring.batch.CommitmentPeer> ret = new java.util.ArrayList<>((int) n);

        for (CommitmentPeerListReader reader : this.readers) {
            try {
                for (org.osid.calendaring.batch.CommitmentPeer commitmentPeer : reader.getNextCommitmentPeers(reader.available())) {
                    ret.add(commitmentPeer);
                }
            } catch (org.osid.OperationFailedException ofe) {
                if (!reader.getSoftError()) {
                    throw ofe;
                }
                
                this.readers.remove(reader);
            }
        }

        return (ret.toArray(new org.osid.calendaring.batch.CommitmentPeer[ret.size()]));      
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        for (CommitmentPeerListReader reader : this.readers) {
            reader.close();
        }

        this.readers.clear();
        this.eol     = true;
        return;
    }


    /**
     *  Adds a <code>CommitmentPeerList</code> to this
     *  <code>CommitmentPeerList</code> using the default buffer size.
     *
     *  @param commitmentPeerList the <code>CommitmentPeerList</code> to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentPeerList</code> is <code>null</code>
     */

    @Override
    public void addCommitmentPeerList(org.osid.calendaring.batch.CommitmentPeerList commitmentPeerList) {
        addCommitmentPeerList(commitmentPeerList, this.bufferSize, this.soft);
        return;
    }


    /**
     *  Adds a collection of <code>CommitmentPeerLists</code> to this
     *  <code>CommitmentPeerList</code>.
     *
     *  @param commitmentPeerLists the <code>CommitmentPeerList</code> collection
     *         to add
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentPeerLists</code> is <code>null</code>
     */

    @Override
    public void addCommitmentPeerLists(java.util.Collection<org.osid.calendaring.batch.CommitmentPeerList> commitmentPeerLists) {
        nullarg(commitmentPeerLists, "commitmentpeer lists");

        for (org.osid.calendaring.batch.CommitmentPeerList list : commitmentPeerLists) {
            addCommitmentPeerList(list);
        }

        return;
    }

        
    /**
     *  Adds a <code>CommitmentPeerList</code> to this
     *  <code>CommitmentPeerList</code>.
     *
     *  @param commitmentPeerList the <code>CommitmentPeerList</code> to add
     *  @param bufferSize a buffer size for this list
     *  @param soft <code>true</code> to fail soft, <code>false</code>
     *         to fail hard
     *  @throws org.osid.IllegalStateException this list already closed
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentPeerList</code> is <code>null</code>
     */

    public void addCommitmentPeerList(org.osid.calendaring.batch.CommitmentPeerList commitmentPeerList, int bufferSize, boolean soft) {
        nullarg(commitmentPeerList, "commitmentpeer list");

        net.okapia.osid.jamocha.inline.calendaring.batch.commitmentpeer.BufferedCommitmentPeerList bpl = new net.okapia.osid.jamocha.inline.calendaring.batch.commitmentpeer.BufferedCommitmentPeerList(commitmentPeerList, bufferSize, this);
        CommitmentPeerListReader reader = new CommitmentPeerListReader(bpl, soft);
        this.readers.add(reader);

        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    /**
     *  No more. Invoked when no more lists are to be added.
     */

    @Override
    public void noMore() {
        this.eol = true;
        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    /**
     *  Tests if the list is operational. A list is operational if
     *  <code>close()</code> has not been invoked.
     *
     *  @return <code>true</code> if the list is operational,
     *          <code>false</code> otherwise.
     */

    @Override
    public boolean isOperational() {
        if ((this.readers.size() == 0) && this.eol) {
            return (false);
        }

        return (true);
    }


    /**
     *  Tests if the list is empty.
     *
     *  @return <code>true</code> if the list is empty
     *          retrieved, <code>false</code> otherwise.
     */

    @Override
    public boolean isEmpty() {
        if ((this.readers.size() == 0) && this.eol) {
            return (true);
        } else if (this.readers.size() > 0) {
            return (false);
        }

        return (true);
    }


    @Override
    public void newCommitmentPeerInList(org.osid.calendaring.batch.CommitmentPeerList list) {
        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    @Override
    public void errorInList(org.osid.calendaring.batch.CommitmentPeerList list) {
        this.readers.remove(list);

        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    @Override
    public void doneForList(org.osid.calendaring.batch.CommitmentPeerList list) {
        this.readers.remove(list);

        synchronized (this.readers) {
            this.readers.notifyAll();
        }

        return;
    }


    class CommitmentPeerListReader {
        private net.okapia.osid.jamocha.inline.calendaring.batch.commitmentpeer.BufferedCommitmentPeerList list;
        private boolean soft = false;


        CommitmentPeerListReader(net.okapia.osid.jamocha.inline.calendaring.batch.commitmentpeer.BufferedCommitmentPeerList list, boolean soft) {
            this.list = list;
            this.soft = soft;
            this.list.run();
            return;
        }


        public net.okapia.osid.jamocha.inline.calendaring.batch.commitmentpeer.BufferedCommitmentPeerList getList() {
            return (this.list);
        }


        public boolean getSoftError() {
            return (this.soft);
        }


        public boolean ready() {            
            if (this.list.available() > 0) {
                return (true);
            } else {
                return (false);
            }
        }

        
        public boolean isFinished() {
            if (this.list.isEmpty() && !this.list.isRunning()) {
                return (true);
            } else {
                return (false);
            }
        }


        public long available() {
            return (this.list.available());
        }


        public void skip(long n) {
            this.list.skip(n);
            return;
        }


        public org.osid.calendaring.batch.CommitmentPeer getNextCommitmentPeer()
            throws org.osid.OperationFailedException {

            if (this.list.hasNext()) {
                return (this.list.getNextCommitmentPeer());           
            } else {
                try {
                    this.list.close();
                } catch (Exception e) {}
            }

            throw new org.osid.OperationFailedException("no more elements in buffered list");
        }


        public org.osid.calendaring.batch.CommitmentPeer[] getNextCommitmentPeers(long n)
            throws org.osid.OperationFailedException {

            if (n > this.list.available()) {
                n = this.list.available();
            }

            return (this.list.getNextCommitmentPeers(n));
        }


        public void close() {
            try {
                this.list.close();
            } catch (Exception e) {}

            return;
        }
    }       
}
