//
// AbstractMapRequisiteLookupSession
//
//    A simple framework for providing a Requisite lookup service
//    backed by a fixed collection of requisites.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Requisite lookup service backed by a
 *  fixed collection of requisites. The requisites are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Requisites</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRequisiteLookupSession
    extends net.okapia.osid.jamocha.course.requisite.spi.AbstractRequisiteLookupSession
    implements org.osid.course.requisite.RequisiteLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.requisite.Requisite> requisites = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.requisite.Requisite>());
    private final java.util.Map<org.osid.id.Id, org.osid.course.requisite.CourseRequirement> coursereq = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.requisite.CourseRequirement>());
    private final java.util.Map<org.osid.id.Id, org.osid.course.requisite.ProgramRequirement> programreq = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.requisite.ProgramRequirement>());
    private final java.util.Map<org.osid.id.Id, org.osid.course.requisite.CredentialRequirement> credentialreq = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.requisite.CredentialRequirement>());
    private final java.util.Map<org.osid.id.Id, org.osid.course.requisite.LearningObjectiveRequirement> loreq = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.requisite.LearningObjectiveRequirement>());
    private final java.util.Map<org.osid.id.Id, org.osid.course.requisite.AssessmentRequirement> assessmentreq = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.requisite.AssessmentRequirement>());
    private final java.util.Map<org.osid.id.Id, org.osid.course.requisite.AwardRequirement> awardreq = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.requisite.AwardRequirement>());


    /**
     *  Makes a <code>Requisite</code> available in this session.
     *
     *  @param  requisite a requisite
     *  @throws org.osid.NullArgumentException <code>requisite<code>
     *          is <code>null</code>
     */

    protected void putRequisite(org.osid.course.requisite.Requisite requisite) {
        this.requisites.put(requisite.getId(), requisite);
        return;
    }


    /**
     *  Makes an array of requisites available in this session.
     *
     *  @param  requisites an array of requisites
     *  @throws org.osid.NullArgumentException <code>requisites<code>
     *          is <code>null</code>
     */

    protected void putRequisites(org.osid.course.requisite.Requisite[] requisites) {
        putRequisites(java.util.Arrays.asList(requisites));
        return;
    }


    /**
     *  Makes a collection of requisites available in this session.
     *
     *  @param  requisites a collection of requisites
     *  @throws org.osid.NullArgumentException <code>requisites<code>
     *          is <code>null</code>
     */

    protected void putRequisites(java.util.Collection<? extends org.osid.course.requisite.Requisite> requisites) {
        for (org.osid.course.requisite.Requisite requisite : requisites) {
            this.requisites.put(requisite.getId(), requisite);
        }

        return;
    }


    /**
     *  Removes a Requisite from this session.
     *
     *  @param  requisiteId the <code>Id</code> of the requisite
     *  @throws org.osid.NullArgumentException <code>requisiteId<code> is
     *          <code>null</code>
     */

    protected void removeRequisite(org.osid.id.Id requisiteId) {
        this.requisites.remove(requisiteId);
        return;
    }


    /**
     *  Gets the <code>Requisite</code> specified by its <code>Id</code>.
     *
     *  @param  requisiteId <code>Id</code> of the <code>Requisite</code>
     *  @return the requisite
     *  @throws org.osid.NotFoundException <code>requisiteId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>requisiteId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite getRequisite(org.osid.id.Id requisiteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.requisite.Requisite requisite = this.requisites.get(requisiteId);
        if (requisite == null) {
            throw new org.osid.NotFoundException("requisite not found: " + requisiteId);
        }

        return (requisite);
    }


    /**
     *  Gets all <code>Requisites</code>. In plenary mode, the returned
     *  list contains all known requisites or an error
     *  results. Otherwise, the returned list may contain only those
     *  requisites that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.requisite.ArrayRequisiteList(this.requisites.values()));
    }


    /**
     *  Makes a <code>CourseRequirement</code> available in this
     *  session.
     *
     *  @param  courseRequirement a course requirement
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirement<code> is <code>null</code>
     */

    protected void putCourseRequirement(org.osid.course.requisite.CourseRequirement courseRequirement) {
        this.coursereq.put(courseRequirement.getId(), courseRequirement);
        return;
    }


    /**
     *  Makes an array of course requirements available in this
     *  session.
     *
     *  @param  courseRequirements an array of course requirements
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirements<code> is <code>null</code>
     */

    protected void putCourseRequirements(org.osid.course.requisite.CourseRequirement[] courseRequirements) {
        putCourseRequirements(java.util.Arrays.asList(courseRequirements));
        return;
    }


    /**
     *  Makes a collection of course requirements available in this
     *  session.
     *
     *  @param  courseRequirements a collection of course requirements
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirements<code> is <code>null</code>
     */

    protected void putCourseRequirements(java.util.Collection<? extends org.osid.course.requisite.CourseRequirement> courseRequirements) {
        for (org.osid.course.requisite.CourseRequirement courseRequirement : courseRequirements) {
            this.coursereq.put(courseRequirement.getId(), courseRequirement);
        }

        return;
    }


    /**
     *  Removes a CourseRequirement from this session.
     *
     *  @param courseRequirementId the <code>Id</code> of the course
     *         requirement
     *  @throws org.osid.NullArgumentException <code>courseRequirementId<code> is
     *          <code>null</code>
     */

    protected void removeCourseRequirement(org.osid.id.Id courseRequirementId) {
        this.coursereq.remove(courseRequirementId);
        return;
    }


    /**
     *  Gets the <code>CourseRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CourseRequirement</code> and retained for compatibility.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementId <code>Id</code> of the
     *          <code>CourseRequirement</code>
     *  @return the course requirement
     *  @throws org.osid.NotFoundException <code>courseRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirement getCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.requisite.CourseRequirement req = this.coursereq.get(courseRequirementId);
        if (req == null) {
            throw new org.osid.NotFoundException("course requirement Id not found: " + courseRequirementId);
        }

        return (req);
    }


    /**
     *  Gets all <code>CourseRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @return a list of <code>CourseRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.courserequirement.ArrayCourseRequirementList(this.coursereq.values()));
    }


    /**
     *  Makes a <code>ProgramRequirement</code> available in this
     *  session.
     *
     *  @param  programRequirement a program requirement
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirement<code> is <code>null</code>
     */

    protected void putProgramRequirement(org.osid.course.requisite.ProgramRequirement programRequirement) {
        this.programreq.put(programRequirement.getId(), programRequirement);
        return;
    }


    /**
     *  Makes an array of program requirements available in this
     *  session.
     *
     *  @param  programRequirements an array of program requirements
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirements<code> is <code>null</code>
     */

    protected void putProgramRequirements(org.osid.course.requisite.ProgramRequirement[] programRequirements) {
        putProgramRequirements(java.util.Arrays.asList(programRequirements));
        return;
    }


    /**
     *  Makes a collection of program requirements available in this
     *  session.
     *
     *  @param  programRequirements a collection of program requirements
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirements<code> is <code>null</code>
     */

    protected void putProgramRequirements(java.util.Collection<? extends org.osid.course.requisite.ProgramRequirement> programRequirements) {
        for (org.osid.course.requisite.ProgramRequirement programRequirement : programRequirements) {
            this.programreq.put(programRequirement.getId(), programRequirement);
        }

        return;
    }


    /**
     *  Removes a ProgramRequirement from this session.
     *
     *  @param programRequirementId the <code>Id</code> of the program
     *         requirement
     *  @throws org.osid.NullArgumentException <code>programRequirementId<code> is
     *          <code>null</code>
     */

    protected void removeProgramRequirement(org.osid.id.Id programRequirementId) {
        this.programreq.remove(programRequirementId);
        return;
    }


    /**
     *  Gets the <code>ProgramRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProgramRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ProgramRequirement</code> and retained for compatibility.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementId <code>Id</code> of the
     *          <code>ProgramRequirement</code>
     *  @return the program requirement
     *  @throws org.osid.NotFoundException
     *          <code>programRequirementId</code> not found
     *  @throws org.osid.NullArgumentException <code>programRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirement getProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.requisite.ProgramRequirement req = this.programreq.get(programRequirementId);
        if (req == null) {
            throw new org.osid.NotFoundException("program requirement Id not found: " + programRequirementId);
        }

        return (req);
    }


    /**
     *  Gets all <code>ProgramRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @return a list of <code>ProgramRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.programrequirement.ArrayProgramRequirementList(this.programreq.values()));
    }


    /**
     *  Makes a <code>CredentialRequirement</code> available in this
     *  session.
     *
     *  @param  credentialRequirement a credential requirement
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirement<code> is <code>null</code>
     */

    protected void putCredentialRequirement(org.osid.course.requisite.CredentialRequirement credentialRequirement) {
        this.credentialreq.put(credentialRequirement.getId(), credentialRequirement);
        return;
    }


    /**
     *  Makes an array of credential requirements available in this
     *  session.
     *
     *  @param  credentialRequirements an array of credential requirements
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirements<code> is
     *          <code>null</code>
     */

    protected void putCredentialRequirements(org.osid.course.requisite.CredentialRequirement[] credentialRequirements) {
        putCredentialRequirements(java.util.Arrays.asList(credentialRequirements));
        return;
    }


    /**
     *  Makes a collection of credential requirements available in
     *  this session.
     *
     *  @param  credentialRequirements a collection of credential requirements
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirements<code> is
     *          <code>null</code>
     */

    protected void putCredentialRequirements(java.util.Collection<? extends org.osid.course.requisite.CredentialRequirement> credentialRequirements) {
        for (org.osid.course.requisite.CredentialRequirement credentialRequirement : credentialRequirements) {
            this.credentialreq.put(credentialRequirement.getId(), credentialRequirement);
        }

        return;
    }


    /**
     *  Removes a CredentialRequirement from this session.
     *
     *  @param credentialRequirementId the <code>Id</code> of the
     *         credential requirement
     *  @throws org.osid.NullArgumentException <code>credentialRequirementId<code> is
     *          <code>null</code>
     */

    protected void removeCredentialRequirement(org.osid.id.Id credentialRequirementId) {
        this.credentialreq.remove(credentialRequirementId);
        return;
    }


    /**
     *  Gets the <code>CredentialRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CredentialRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CredentialRequirement</code> and retained for
     *  compatibility.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementId <code>Id</code> of the
     *          <code>CredentialRequirement</code>
     *  @return the credential requirement
     *  @throws org.osid.NotFoundException <code>credentialRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>credentialRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirement getCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.requisite.CredentialRequirement req = this.credentialreq.get(credentialRequirementId);
        if (req == null) {
            throw new org.osid.NotFoundException("credential requirement Id not found: " + credentialRequirementId);
        }

        return (req);
    }


    /**
     *  Gets all <code>CredentialRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @return a list of <code>CredentialRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.credentialrequirement.ArrayCredentialRequirementList(this.credentialreq.values()));
    }
    

    /**
     *  Makes a <code>LearningObjectiveRequirement</code> available in
     *  this session.
     *
     *  @param learningObjectiveRequirement a learning objective
     *         requirement
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirement<code> is
     *          <code>null</code>
     */

    protected void putLearningObjectiveRequirement(org.osid.course.requisite.LearningObjectiveRequirement learningObjectiveRequirement) {
        this.loreq.put(learningObjectiveRequirement.getId(), learningObjectiveRequirement);
        return;
    }


    /**
     *  Makes an array of learning objective requirements available in
     *  this session.
     *
     *  @param learningObjectiveRequirements an array of learning
     *         objective requirements
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirements<code> is
     *          <code>null</code>
     */

    protected void putLearningObjectiveRequirements(org.osid.course.requisite.LearningObjectiveRequirement[] learningObjectiveRequirements) {
        putLearningObjectiveRequirements(java.util.Arrays.asList(learningObjectiveRequirements));
        return;
    }


    /**
     *  Makes a collection of learning objective requirements
     *  available in this session.
     *
     *  @param learningObjectiveRequirements a collection of
     *         learningObjective requirements
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirements<code> is
     *          <code>null</code>
     */

    protected void putLearningObjectiveRequirements(java.util.Collection<? extends org.osid.course.requisite.LearningObjectiveRequirement> learningObjectiveRequirements) {
        for (org.osid.course.requisite.LearningObjectiveRequirement learningObjectiveRequirement : learningObjectiveRequirements) {
            this.loreq.put(learningObjectiveRequirement.getId(), learningObjectiveRequirement);
        }

        return;
    }


    /**
     *  Removes a LearningObjectiveRequirement from this session.
     *
     *  @param learningObjectiveRequirementId the <code>Id</code> of
     *         the learning objective requirement
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementId<code> is
     *          <code>null</code>
     */

    protected void removeLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId) {
        this.loreq.remove(learningObjectiveRequirementId);
        return;
    }


    /**
     *  Gets the <code>LearningObjectiveRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>LearningObjectiveRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>LearningObjectiveRequirement</code> and retained for compatibility.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementId <code>Id</code> of the
     *          <code>LearningObjectiveRequirement</code>
     *  @return the learning objective requirement
     *  @throws org.osid.NotFoundException
     *          <code>learningObjectiveRequirementId</code> not found
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementId</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirement getLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.requisite.LearningObjectiveRequirement req = this.loreq.get(learningObjectiveRequirementId);
        if (req == null) {
            throw new org.osid.NotFoundException("learning objective requirement Id not found: " + learningObjectiveRequirementId);
        }

        return (req);
    }


    /**
     *  Gets all <code>LearningObjectiveRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @return a list of <code>LearningObjectiveRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.learningobjectiverequirement.ArrayLearningObjectiveRequirementList(this.loreq.values()));
    }


    /**
     *  Makes a <code>AssessmentRequirement</code> available in this
     *  session.
     *
     *  @param  assessmentRequirement an assessment requirement
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirement<code> is <code>null</code>
     */

    protected void putAssessmentRequirement(org.osid.course.requisite.AssessmentRequirement assessmentRequirement) {
        this.assessmentreq.put(assessmentRequirement.getId(), assessmentRequirement);
        return;
    }


    /**
     *  Makes an array of assessment requirements available in this
     *  session.
     *
     *  @param  assessmentRequirements an array of assessment requirements
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirements<code> is
     *          <code>null</code>
     */

    protected void putAssessmentRequirements(org.osid.course.requisite.AssessmentRequirement[] assessmentRequirements) {
        putAssessmentRequirements(java.util.Arrays.asList(assessmentRequirements));
        return;
    }


    /**
     *  Makes a collection of assessment requirements available in
     *  this session.
     *
     *  @param  assessmentRequirements a collection of assessment requirements
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirements<code> is <code>null</code>
     */

    protected void putAssessmentRequirements(java.util.Collection<? extends org.osid.course.requisite.AssessmentRequirement> assessmentRequirements) {
        for (org.osid.course.requisite.AssessmentRequirement assessmentRequirement : assessmentRequirements) {
            this.assessmentreq.put(assessmentRequirement.getId(), assessmentRequirement);
        }

        return;
    }


    /**
     *  Removes an AssessmentRequirement from this session.
     *
     *  @param assessmentRequirementId the <code>Id</code> of the
     *         assessment requirement
     *  @throws org.osid.NullArgumentException <code>assessmentRequirementId<code> is
     *          <code>null</code>
     */

    protected void removeAssessmentRequirement(org.osid.id.Id assessmentRequirementId) {
        this.assessmentreq.remove(assessmentRequirementId);
        return;
    }


    /**
     *  Gets the <code>AssessmentRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AssessmentRequirement</code> and retained for
     *  compatibility.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentRequirementId <code>Id</code> of the
     *          <code>AssessmentRequirement</code>
     *  @return the assessment requirement
     *  @throws org.osid.NotFoundException <code>assessmentRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirement getAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.requisite.AssessmentRequirement req = this.assessmentreq.get(assessmentRequirementId);
        if (req == null) {
            throw new org.osid.NotFoundException("assessment requirement Id not found: " + assessmentRequirementId);
        }

        return (req);
    }


    /**
     *  Gets all <code>AssessmentRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @return a list of <code>AssessmentRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.assessmentrequirement.ArrayAssessmentRequirementList(this.assessmentreq.values()));
    }


    /**
     *  Makes a <code>AwardRequirement</code> available in this
     *  session.
     *
     *  @param  awardRequirement an award requirement
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirement<code> is <code>null</code>
     */

    protected void putAwardRequirement(org.osid.course.requisite.AwardRequirement awardRequirement) {
        this.awardreq.put(awardRequirement.getId(), awardRequirement);
        return;
    }


    /**
     *  Makes an array of award requirements available in this
     *  session.
     *
     *  @param  awardRequirements an array of award requirements
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirements<code> is <code>null</code>
     */

    protected void putAwardRequirements(org.osid.course.requisite.AwardRequirement[] awardRequirements) {
        putAwardRequirements(java.util.Arrays.asList(awardRequirements));
        return;
    }


    /**
     *  Makes a collection of award requirements available in this
     *  session.
     *
     *  @param  awardRequirements a collection of award requirements
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirements<code> is <code>null</code>
     */

    protected void putAwardRequirements(java.util.Collection<? extends org.osid.course.requisite.AwardRequirement> awardRequirements) {
        for (org.osid.course.requisite.AwardRequirement awardRequirement : awardRequirements) {
            this.awardreq.put(awardRequirement.getId(), awardRequirement);
        }

        return;
    }


    /**
     *  Removes an AwardRequirement from this session.
     *
     *  @param awardRequirementId the <code>Id</code> of the award
     *         requirement
     *  @throws org.osid.NullArgumentException <code>awardRequirementId<code> is
     *          <code>null</code>
     */

    protected void removeAwardRequirement(org.osid.id.Id awardRequirementId) {
        this.awardreq.remove(awardRequirementId);
        return;
    }


    /**
     *  Gets the <code>AwardRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AwardRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AwardRequirement</code> and retained for compatibility.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardRequirementId <code>Id</code> of the
     *          <code>AwardRequirement</code>
     *  @return the award requirement
     *  @throws org.osid.NotFoundException <code>awardRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>awardRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirement getAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.requisite.AwardRequirement req = this.awardreq.get(awardRequirementId);
        if (req == null) {
            throw new org.osid.NotFoundException("award requirement Id not found: " + awardRequirementId);
        }

        return (req);
    }


    /**
     *  Gets all <code>AwardRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @return a list of <code>AwardRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.requisite.awardrequirement.ArrayAwardRequirementList(this.awardreq.values()));
    }



    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.requisites.clear();
        super.close();
        return;
    }
}
