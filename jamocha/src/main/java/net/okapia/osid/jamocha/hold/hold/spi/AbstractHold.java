//
// AbstractHold.java
//
//     Defines a Hold.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Hold</code>.
 */

public abstract class AbstractHold
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.hold.Hold {

    private org.osid.hold.Issue issue;
    private org.osid.resource.Resource resource;
    private org.osid.authentication.Agent agent;

    private final java.util.Collection<org.osid.hold.records.HoldRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the issue. 
     *
     *  @return the issue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getIssueId() {
        return (this.issue.getId());
    }


    /**
     *  Gets the issue. 
     *
     *  @return the issue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.hold.Issue getIssue()
        throws org.osid.OperationFailedException {

        return (this.issue);
    }


    /**
     *  Sets the issue.
     *
     *  @param issue an issue
     *  @throws org.osid.NullArgumentException
     *          <code>issue</code> is <code>null</code>
     */

    protected void setIssue(org.osid.hold.Issue issue) {
        nullarg(issue, "issue");
        this.issue = issue;
        return;
    }


    /**
     *  Tests if this hold is for a <code> Resource. </code> 
     *
     *  @return <code> true </code> if this hold has a <code> Resource, 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasResource() {
        return (this.resource != null);
    }


    /**
     *  Gets the <code> Id </code> of resource. 
     *
     *  @return the resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        if (!hasResource()) {
            throw new org.osid.IllegalStateException("hasResource() is false");
        }

        return (this.resource.getId());
    }


    /**
     *  Gets the resources that can be assigned to an issue. 
     *
     *  @return the resource 
     *  @throws org.osid.IllegalStateException <code> hasResource() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        if (!hasResource()) {
            throw new org.osid.IllegalStateException("hasResource() is false");
        }

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if this hold is for an <code> Agent. </code> 
     *
     *  @return <code> true </code> if this hold has an <code> Agent, </code> 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAgent() {
        return (this.agent != null);
    }


    /**
     *  Gets the <code> Agent Id </code> for this hold. 
     *
     *  @return the <code> Agent Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        if (!hasAgent()) {
            throw new org.osid.IllegalStateException("hasAgent() is false");
        }

        return (this.agent.getId());
    }


    /**
     *  Gets the <code> Agent </code> for this hold. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.IllegalStateException <code> hasAgent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        if (!hasAgent()) {
            throw new org.osid.IllegalStateException("hasAgent() is false");
        }

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Tests if this hold supports the given record
     *  <code>Type</code>.
     *
     *  @param  holdRecordType a hold record type 
     *  @return <code>true</code> if the holdRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type holdRecordType) {
        for (org.osid.hold.records.HoldRecord record : this.records) {
            if (record.implementsRecordType(holdRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Hold</code> record <code>Type</code>.
     *
     *  @param  holdRecordType the hold record type 
     *  @return the hold record 
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.records.HoldRecord getHoldRecord(org.osid.type.Type holdRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.records.HoldRecord record : this.records) {
            if (record.implementsRecordType(holdRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdRecordType + " is not supported");
    }


    /**
     *  Adds a record to this hold. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param holdRecord the hold record
     *  @param holdRecordType hold record type
     *  @throws org.osid.NullArgumentException
     *          <code>holdRecord</code> or
     *          <code>holdRecordTypehold</code> is
     *          <code>null</code>
     */
            
    protected void addHoldRecord(org.osid.hold.records.HoldRecord holdRecord, 
                                 org.osid.type.Type holdRecordType) {
        
        nullarg(holdRecord, "hold record");
        addRecordType(holdRecordType);
        this.records.add(holdRecord);
        
        return;
    }
}
