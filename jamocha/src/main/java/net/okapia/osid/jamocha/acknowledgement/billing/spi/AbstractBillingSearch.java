//
// AbstractBillingSearch.java
//
//     A template for making a Billing Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.billing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing billing searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBillingSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.acknowledgement.BillingSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.acknowledgement.records.BillingSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.acknowledgement.BillingSearchOrder billingSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of billings. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  billingIds list of billings
     *  @throws org.osid.NullArgumentException
     *          <code>billingIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBillings(org.osid.id.IdList billingIds) {
        while (billingIds.hasNext()) {
            try {
                this.ids.add(billingIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBillings</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of billing Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBillingIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  billingSearchOrder billing search order 
     *  @throws org.osid.NullArgumentException
     *          <code>billingSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>billingSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBillingResults(org.osid.acknowledgement.BillingSearchOrder billingSearchOrder) {
	this.billingSearchOrder = billingSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.acknowledgement.BillingSearchOrder getBillingSearchOrder() {
	return (this.billingSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given billing search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a billing implementing the requested record.
     *
     *  @param billingSearchRecordType a billing search record
     *         type
     *  @return the billing search record
     *  @throws org.osid.NullArgumentException
     *          <code>billingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(billingSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.BillingSearchRecord getBillingSearchRecord(org.osid.type.Type billingSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.acknowledgement.records.BillingSearchRecord record : this.records) {
            if (record.implementsRecordType(billingSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(billingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this billing search. 
     *
     *  @param billingSearchRecord billing search record
     *  @param billingSearchRecordType billing search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBillingSearchRecord(org.osid.acknowledgement.records.BillingSearchRecord billingSearchRecord, 
                                           org.osid.type.Type billingSearchRecordType) {

        addRecordType(billingSearchRecordType);
        this.records.add(billingSearchRecord);        
        return;
    }
}
