//
// AbstractFileQuery.java
//
//     A template for making a File Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.file.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for files.
 */

public abstract class AbstractFileQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.filing.FileQuery {

    private final java.util.Collection<org.osid.filing.records.FileQueryRecord> records = new java.util.ArrayList<>();


    /**
     *  Matches entry names. Supplying multiple strings behaves like a
     *  boolean <code> AND </code> among the elements each which must
     *  correspond to the <code> stringMatchType. </code> An <code> OR
     *  </code> can be performed with multiple queries.
     *
     *  @param  name name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> name </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchName(String name, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the name terms. 
     */

    @OSID @Override
    public void clearNameTerms() {
        return;
    }


    /**
     *  Matches an absolute pathname of a directory entry. Supplying multiple 
     *  strings behaves like a boolean <code> AND </code> among the elements 
     *  each which must correspond to the <code> stringMatchType. </code> An 
     *  <code> OR </code> can be performed with multiple queries. 
     *
     *  @param  path path to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> path </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchPath(String path, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the path terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        return;
    }


    /**
     *  Tests if a <code> DirectoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a directory query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a directory to match the parent directory. There is 
     *  only one <code> DirectoryQuery </code> per <code> 
     *  DifrectoryEntryQuery. </code> Multiple retrievals return the same 
     *  object. 
     *
     *  @return the directory query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuery getDirectoryQuery() {
        throw new org.osid.UnimplementedException("supportsDirectoryQuery() is false");
    }


    /**
     *  Clears the directory terms. 
     */

    @OSID @Override
    public void clearDirectoryTerms() {
        return;
    }


    /**
     *  Matches aliases only. 
     *
     *  @param  match <code> true </code> to match aliases, <code> false 
     *          </code> to match target files 
     */

    @OSID @Override
    public void matchAliases(boolean match) {
        return;
    }


    /**
     *  Matches a file that has any aliases. 
     *
     *  @param  match <code> true </code> to match any alias, <code> false 
     *          </code> to match objects with no aliases 
     */

    @OSID @Override
    public void matchAnyAliases(boolean match) {
        return;
    }


    /**
     *  Clears the aliases terms. 
     */

    @OSID @Override
    public void clearAliasesTerms() {
        return;
    }


    /**
     *  Matches files whose entries are owned by the given agent id. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOwnerId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the owner <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOwnerIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  agents. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOwnerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getOwnerQuery() {
        throw new org.osid.UnimplementedException("supportsOwnerQuery() is false");
    }


    /**
     *  Clears the owner terms. 
     */

    @OSID @Override
    public void clearOwnerTerms() {
        return;
    }


    /**
     *  Match directory entries that are created between the specified time 
     *  period inclusive. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCreatedTime(org.osid.calendaring.DateTime start, 
                                 org.osid.calendaring.DateTime end, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the created time terms. 
     */

    @OSID @Override
    public void clearCreatedTimeTerms() {
        return;
    }


    /**
     *  Match directory entries that are modified between the specified time 
     *  period inclusive. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchModifiedTime(org.osid.calendaring.DateTime start, 
                                  org.osid.calendaring.DateTime end, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the modified time terms. 
     */

    @OSID @Override
    public void clearModifiedTimeTerms() {
        return;
    }


    /**
     *  Match directory entries that were last accessed between the specified 
     *  time period. 
     *
     *  @param  start start time of the query 
     *  @param  end end time of the query 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is les 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchLastAccessTime(org.osid.calendaring.DateTime start, 
                                    org.osid.calendaring.DateTime end, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the last access time terms. 
     */

    @OSID @Override
    public void clearLastAccessTimeTerms() {
        return;
    }

    
    /**
     *  Matches files whose size is within and including the given range. 
     *
     *  @param  from low file size 
     *  @param  to high file size 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is les 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchSize(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches a file that has any known size. 
     *
     *  @param  match <code> true </code> to match any size, <code> false 
     *          </code> to match files with no known size 
     */

    @OSID @Override
    public void matchAnySize(boolean match) {
        return;
    }


    /**
     *  Clears all file size terms. 
     */

    @OSID @Override
    public void clearSizeTerms() {
        return;
    }


    /**
     *  Adds data strings to this query to match files whose content contains 
     *  these strings. Supplying multiple strings behaves like a boolean AND 
     *  among the elements each which must correspond to the <code> 
     *  stringMatchType. </code> An OR can be performed with multiple queries. 
     *
     *  @param  data string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> data </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> data </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDataString(String data, 
                                org.osid.type.Type stringMatchType, 
                                boolean match) {
        return;
    }


    /**
     *  Clears all file data string terms. 
     */

    @OSID @Override
    public void clearDataStringTerms() {
        return;
    }


    /**
     *  Matches files who data contains the given bytes. 
     *
     *  @param  data data to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @param  partial <code> true </code> for a partial match, <code> false 
     *          </code> for a complete match 
     *  @throws org.osid.NullArgumentException <code> data </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchData(byte[] data, boolean match, boolean partial) {
        return;
    }


    /**
     *  Matches a file that has any data. 
     *
     *  @param  match <code> true </code> to match any data, <code> false 
     *          </code> to match files with no data 
     */

    @OSID @Override
    public void matchAnyData(boolean match) {
        return;
    }


    /**
     *  Clears all file data terms. 
     */

    @OSID @Override
    public void clearDataTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given file query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a file implementing the requested record.
     *
     *  @param fileRecordType a file record type
     *  @return the file query record
     *  @throws org.osid.NullArgumentException
     *          <code>fileRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fileRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.FileQueryRecord getFileQueryRecord(org.osid.type.Type fileRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.FileQueryRecord record : this.records) {
            if (record.implementsRecordType(fileRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fileRecordType + " is not supported");
    }


    /**
     *  Adds a record to this file query. 
     *
     *  @param fileQueryRecord file query record
     *  @param fileRecordType file record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFileQueryRecord(org.osid.filing.records.FileQueryRecord fileQueryRecord, 
                                          org.osid.type.Type fileRecordType) {

        addRecordType(fileRecordType);
        nullarg(fileQueryRecord, "file query record");
        this.records.add(fileQueryRecord);        
        return;
    }
}
