//
// AbstractMappingPathManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractMappingPathManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.mapping.path.MappingPathManager,
               org.osid.mapping.path.MappingPathProxyManager {

    private final Types pathRecordTypes                    = new TypeRefSet();
    private final Types pathSearchRecordTypes              = new TypeRefSet();

    private final Types intersectionRecordTypes            = new TypeRefSet();
    private final Types intersectionSearchRecordTypes      = new TypeRefSet();

    private final Types speedZoneRecordTypes               = new TypeRefSet();
    private final Types speedZoneSearchRecordTypes         = new TypeRefSet();

    private final Types signalRecordTypes                  = new TypeRefSet();
    private final Types signalSearchRecordTypes            = new TypeRefSet();

    private final Types obstacleRecordTypes                = new TypeRefSet();
    private final Types obstacleSearchRecordTypes          = new TypeRefSet();

    private final Types resourceVelocityRecordTypes        = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractMappingPathManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractMappingPathManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any map federation is exposed. Federation is exposed when a 
     *  specific map may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of maps appears 
     *  as a single map. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up paths is supported. 
     *
     *  @return <code> true </code> if path lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathLookup() {
        return (false);
    }


    /**
     *  Tests if querying paths is supported. 
     *
     *  @return <code> true </code> if path query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Tests if searching paths is supported. 
     *
     *  @return <code> true </code> if path search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearch() {
        return (false);
    }


    /**
     *  Tests if path <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if path administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathAdmin() {
        return (false);
    }


    /**
     *  Tests if a path <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if path notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathNotification() {
        return (false);
    }


    /**
     *  Tests if a path map lookup service is supported. 
     *
     *  @return <code> true </code> if a path map lookup service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathMap() {
        return (false);
    }


    /**
     *  Tests if a path map service is supported. 
     *
     *  @return <code> true </code> if path to map assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathMapAssignment() {
        return (false);
    }


    /**
     *  Tests if a path smart map lookup service is supported. 
     *
     *  @return <code> true </code> if a path smart map service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSmartMap() {
        return (false);
    }


    /**
     *  Tests if a path spatial lookup service is supported. 
     *
     *  @return <code> true </code> if a path spatial service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSpatial() {
        return (false);
    }


    /**
     *  Tests if a path spatial design service is supported. 
     *
     *  @return <code> true </code> if a path spatial design service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSpatialDesign() {
        return (false);
    }


    /**
     *  Tests if a path travel service is supported. 
     *
     *  @return <code> true </code> if a path travel service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathTravel() {
        return (false);
    }


    /**
     *  Tests if a resource path notification service is supported. 
     *
     *  @return <code> true </code> if a resource path notification service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcePathNotification() {
        return (false);
    }


    /**
     *  Tests if a resource velocity service is supported. 
     *
     *  @return <code> true </code> if a resource velocity service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceVelocity() {
        return (false);
    }


    /**
     *  Tests if a resource velocity update service is supported. 
     *
     *  @return <code> true </code> if a resource velocity update service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceVelocityUpdate() {
        return (false);
    }


    /**
     *  Tests if a resource velocity notification service is supported. 
     *
     *  @return <code> true </code> if a resource velocity notification 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceVelocityNotification() {
        return (false);
    }


    /**
     *  Tests if a my path service is supported. 
     *
     *  @return <code> true </code> if a my path service is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyPath() {
        return (false);
    }


    /**
     *  Tests if an intersection lookup service is supported. 
     *
     *  @return <code> true </code> if an intersection lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionLookup() {
        return (false);
    }


    /**
     *  Tests if querying intersections is supported. 
     *
     *  @return <code> true </code> if intersection query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionQuery() {
        return (false);
    }


    /**
     *  Tests if searching intersections is supported. 
     *
     *  @return <code> true </code> if intersection search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionSearch() {
        return (false);
    }


    /**
     *  Tests if an intersection administrative service is supported. 
     *
     *  @return <code> true </code> if an intersection administrative service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionAdmin() {
        return (false);
    }


    /**
     *  Tests if an intersection <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if intersection notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionNotification() {
        return (false);
    }


    /**
     *  Tests if an intersection map lookup service is supported. 
     *
     *  @return <code> true </code> if an intersection map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionMap() {
        return (false);
    }


    /**
     *  Tests if an intersection map service is supported. 
     *
     *  @return <code> true </code> if intersection to map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionMapAssignment() {
        return (false);
    }


    /**
     *  Tests if an intersection smart map lookup service is supported. 
     *
     *  @return <code> true </code> if an intersection smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIntersectionSmartMap() {
        return (false);
    }


    /**
     *  Tests if looking up speed zones is supported. 
     *
     *  @return <code> true </code> if speed zone lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneLookup() {
        return (false);
    }


    /**
     *  Tests if querying speed zones is supported. 
     *
     *  @return <code> true </code> if speed zone query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneQuery() {
        return (false);
    }


    /**
     *  Tests if searching speed zones is supported. 
     *
     *  @return <code> true </code> if speed zone search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneSearch() {
        return (false);
    }


    /**
     *  Tests if speed zone <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if speed zone administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneAdmin() {
        return (false);
    }


    /**
     *  Tests if a speed zone <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if speed zone notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneNotification() {
        return (false);
    }


    /**
     *  Tests if a speed zone map lookup service is supported. 
     *
     *  @return <code> true </code> if a speed zone map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneMap() {
        return (false);
    }


    /**
     *  Tests if a speed zone map assignment service is supported. 
     *
     *  @return <code> true </code> if a speed zone to map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneMapAssignment() {
        return (false);
    }


    /**
     *  Tests if a speed zone smart map service is supported. 
     *
     *  @return <code> true </code> if a speed zone smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSpeedZoneSmartMap() {
        return (false);
    }


    /**
     *  Tests if looking up signals is supported. 
     *
     *  @return <code> true </code> if signal lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalLookup() {
        return (false);
    }


    /**
     *  Tests if querying signals is supported. 
     *
     *  @return <code> true </code> if signal query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalQuery() {
        return (false);
    }


    /**
     *  Tests if searching signals is supported. 
     *
     *  @return <code> true </code> if signal search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalSearch() {
        return (false);
    }


    /**
     *  Tests if signal <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if signal administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalAdmin() {
        return (false);
    }


    /**
     *  Tests if a signal <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if signal notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalNotification() {
        return (false);
    }


    /**
     *  Tests if a signal map lookup service is supported. 
     *
     *  @return <code> true </code> if a signal map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalMap() {
        return (false);
    }


    /**
     *  Tests if a signal map assignment service is supported. 
     *
     *  @return <code> true </code> if a signal to map assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalMapAssignment() {
        return (false);
    }


    /**
     *  Tests if a signal smart map service is supported. 
     *
     *  @return <code> true </code> if a signal smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSignalSmartMap() {
        return (false);
    }


    /**
     *  Tests if a resource signal notification service is supported. 
     *
     *  @return <code> true </code> if a resource signal notification service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSignalNotification() {
        return (false);
    }


    /**
     *  Tests if looking up obstacles is supported. 
     *
     *  @return <code> true </code> if obstacle lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleLookup() {
        return (false);
    }


    /**
     *  Tests if querying obstacles is supported. 
     *
     *  @return <code> true </code> if obstacle query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleQuery() {
        return (false);
    }


    /**
     *  Tests if searching obstacles is supported. 
     *
     *  @return <code> true </code> if obstacle search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleSearch() {
        return (false);
    }


    /**
     *  Tests if obstacle administrative service is supported. 
     *
     *  @return <code> true </code> if obstacle administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleAdmin() {
        return (false);
    }


    /**
     *  Tests if an obstacle <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if obstacle notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleNotification() {
        return (false);
    }


    /**
     *  Tests if an obstacle <code> </code> hierarchy service is supported. 
     *
     *  @return <code> true </code> if obstacle hierarchy is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleHierarchy() {
        return (false);
    }


    /**
     *  Tests if an obstacle hierarchy design service is supported. 
     *
     *  @return <code> true </code> if obstacle hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if an obstacle map lookup service is supported. 
     *
     *  @return <code> true </code> if an obstacle map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleMap() {
        return (false);
    }


    /**
     *  Tests if an obstacle map assignment service is supported. 
     *
     *  @return <code> true </code> if an obstacle to map assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleMapAssignment() {
        return (false);
    }


    /**
     *  Tests if an obstacle smart map service is supported. 
     *
     *  @return <code> true </code> if an obstacle smart map service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObstacleSmartMap() {
        return (false);
    }


    /**
     *  Tests if a batch mapping path service is supported. 
     *
     *  @return <code> true </code> if a mapping path batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingPathBatch() {
        return (false);
    }


    /**
     *  Tests if a mapping path rules service is supported. 
     *
     *  @return <code> true </code> if a mapping path rules service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingPathRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Path </code> record types. 
     *
     *  @return a list containing the supported <code> Path </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPathRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.pathRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Path </code> record type is supported. 
     *
     *  @param  pathRecordType a <code> Type </code> indicating a <code> Path 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pathRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPathRecordType(org.osid.type.Type pathRecordType) {
        return (this.pathRecordTypes.contains(pathRecordType));
    }


    /**
     *  Adds support for a path record type.
     *
     *  @param pathRecordType a path record type
     *  @throws org.osid.NullArgumentException
     *  <code>pathRecordType</code> is <code>null</code>
     */

    protected void addPathRecordType(org.osid.type.Type pathRecordType) {
        this.pathRecordTypes.add(pathRecordType);
        return;
    }


    /**
     *  Removes support for a path record type.
     *
     *  @param pathRecordType a path record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>pathRecordType</code> is <code>null</code>
     */

    protected void removePathRecordType(org.osid.type.Type pathRecordType) {
        this.pathRecordTypes.remove(pathRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Path </code> search record types. 
     *
     *  @return a list containing the supported <code> Path </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPathSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.pathSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Path </code> search record type is 
     *  supported. 
     *
     *  @param  pathSearchRecordType a <code> Type </code> indicating a <code> 
     *          Path </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pathSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPathSearchRecordType(org.osid.type.Type pathSearchRecordType) {
        return (this.pathSearchRecordTypes.contains(pathSearchRecordType));
    }


    /**
     *  Adds support for a path search record type.
     *
     *  @param pathSearchRecordType a path search record type
     *  @throws org.osid.NullArgumentException
     *  <code>pathSearchRecordType</code> is <code>null</code>
     */

    protected void addPathSearchRecordType(org.osid.type.Type pathSearchRecordType) {
        this.pathSearchRecordTypes.add(pathSearchRecordType);
        return;
    }


    /**
     *  Removes support for a path search record type.
     *
     *  @param pathSearchRecordType a path search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>pathSearchRecordType</code> is <code>null</code>
     */

    protected void removePathSearchRecordType(org.osid.type.Type pathSearchRecordType) {
        this.pathSearchRecordTypes.remove(pathSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Intersection </code> record types. 
     *
     *  @return a list containing the supported <code> Intersection </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIntersectionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.intersectionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Intersection </code> record type is 
     *  supported. 
     *
     *  @param  intersectionRecordType a <code> Type </code> indicating an 
     *          <code> Intersection </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> intersectionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIntersectionRecordType(org.osid.type.Type intersectionRecordType) {
        return (this.intersectionRecordTypes.contains(intersectionRecordType));
    }


    /**
     *  Adds support for an intersection record type.
     *
     *  @param intersectionRecordType an intersection record type
     *  @throws org.osid.NullArgumentException
     *  <code>intersectionRecordType</code> is <code>null</code>
     */

    protected void addIntersectionRecordType(org.osid.type.Type intersectionRecordType) {
        this.intersectionRecordTypes.add(intersectionRecordType);
        return;
    }


    /**
     *  Removes support for an intersection record type.
     *
     *  @param intersectionRecordType an intersection record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>intersectionRecordType</code> is <code>null</code>
     */

    protected void removeIntersectionRecordType(org.osid.type.Type intersectionRecordType) {
        this.intersectionRecordTypes.remove(intersectionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Intersection </code> search record types. 
     *
     *  @return a list containing the supported <code> Intersection </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIntersectionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.intersectionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Intersection </code> search record type is 
     *  supported. 
     *
     *  @param  intersectionSearchRecordType a <code> Type </code> indicating 
     *          an <code> Intersection </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          intersectionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIntersectionSearchRecordType(org.osid.type.Type intersectionSearchRecordType) {
        return (this.intersectionSearchRecordTypes.contains(intersectionSearchRecordType));
    }


    /**
     *  Adds support for an intersection search record type.
     *
     *  @param intersectionSearchRecordType an intersection search record type
     *  @throws org.osid.NullArgumentException
     *  <code>intersectionSearchRecordType</code> is <code>null</code>
     */

    protected void addIntersectionSearchRecordType(org.osid.type.Type intersectionSearchRecordType) {
        this.intersectionSearchRecordTypes.add(intersectionSearchRecordType);
        return;
    }


    /**
     *  Removes support for an intersection search record type.
     *
     *  @param intersectionSearchRecordType an intersection search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>intersectionSearchRecordType</code> is <code>null</code>
     */

    protected void removeIntersectionSearchRecordType(org.osid.type.Type intersectionSearchRecordType) {
        this.intersectionSearchRecordTypes.remove(intersectionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SpeedZone </code> record types. 
     *
     *  @return a list containing the supported <code> SpeedZone </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpeedZoneRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.speedZoneRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SpeedZone </code> record type is supported. 
     *
     *  @param  speedZoneRecordType a <code> Type </code> indicating a <code> 
     *          SpeedZone </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> speedZoneRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpeedZoneRecordType(org.osid.type.Type speedZoneRecordType) {
        return (this.speedZoneRecordTypes.contains(speedZoneRecordType));
    }


    /**
     *  Adds support for a speed zone record type.
     *
     *  @param speedZoneRecordType a speed zone record type
     *  @throws org.osid.NullArgumentException
     *  <code>speedZoneRecordType</code> is <code>null</code>
     */

    protected void addSpeedZoneRecordType(org.osid.type.Type speedZoneRecordType) {
        this.speedZoneRecordTypes.add(speedZoneRecordType);
        return;
    }


    /**
     *  Removes support for a speed zone record type.
     *
     *  @param speedZoneRecordType a speed zone record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>speedZoneRecordType</code> is <code>null</code>
     */

    protected void removeSpeedZoneRecordType(org.osid.type.Type speedZoneRecordType) {
        this.speedZoneRecordTypes.remove(speedZoneRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SpeedZone </code> search types. 
     *
     *  @return a list containing the supported <code> SpeedZone </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpeedZoneSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.speedZoneSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SpeedZone </code> search type is supported. 
     *
     *  @param  speedZoneSearchRecordType a <code> Type </code> indicating a 
     *          <code> SpeedZone </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          speedZoneSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpeedZoneSearchRecordType(org.osid.type.Type speedZoneSearchRecordType) {
        return (this.speedZoneSearchRecordTypes.contains(speedZoneSearchRecordType));
    }


    /**
     *  Adds support for a speed zone search record type.
     *
     *  @param speedZoneSearchRecordType a speed zone search record type
     *  @throws org.osid.NullArgumentException
     *  <code>speedZoneSearchRecordType</code> is <code>null</code>
     */

    protected void addSpeedZoneSearchRecordType(org.osid.type.Type speedZoneSearchRecordType) {
        this.speedZoneSearchRecordTypes.add(speedZoneSearchRecordType);
        return;
    }


    /**
     *  Removes support for a speed zone search record type.
     *
     *  @param speedZoneSearchRecordType a speed zone search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>speedZoneSearchRecordType</code> is <code>null</code>
     */

    protected void removeSpeedZoneSearchRecordType(org.osid.type.Type speedZoneSearchRecordType) {
        this.speedZoneSearchRecordTypes.remove(speedZoneSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Signal </code> record types. 
     *
     *  @return a list containing the supported <code> Signal </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSignalRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.signalRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Signal </code> record type is supported. 
     *
     *  @param  signalRecordType a <code> Type </code> indicating a <code> 
     *          Signal </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> signalRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSignalRecordType(org.osid.type.Type signalRecordType) {
        return (this.signalRecordTypes.contains(signalRecordType));
    }


    /**
     *  Adds support for a signal record type.
     *
     *  @param signalRecordType a signal record type
     *  @throws org.osid.NullArgumentException
     *  <code>signalRecordType</code> is <code>null</code>
     */

    protected void addSignalRecordType(org.osid.type.Type signalRecordType) {
        this.signalRecordTypes.add(signalRecordType);
        return;
    }


    /**
     *  Removes support for a signal record type.
     *
     *  @param signalRecordType a signal record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>signalRecordType</code> is <code>null</code>
     */

    protected void removeSignalRecordType(org.osid.type.Type signalRecordType) {
        this.signalRecordTypes.remove(signalRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Signal </code> search types. 
     *
     *  @return a list containing the supported <code> Signal </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSignalSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.signalSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Signal </code> search type is supported. 
     *
     *  @param  signalSearchRecordType a <code> Type </code> indicating a 
     *          <code> Signal </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> signalSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSignalSearchRecordType(org.osid.type.Type signalSearchRecordType) {
        return (this.signalSearchRecordTypes.contains(signalSearchRecordType));
    }


    /**
     *  Adds support for a signal search record type.
     *
     *  @param signalSearchRecordType a signal search record type
     *  @throws org.osid.NullArgumentException
     *  <code>signalSearchRecordType</code> is <code>null</code>
     */

    protected void addSignalSearchRecordType(org.osid.type.Type signalSearchRecordType) {
        this.signalSearchRecordTypes.add(signalSearchRecordType);
        return;
    }


    /**
     *  Removes support for a signal search record type.
     *
     *  @param signalSearchRecordType a signal search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>signalSearchRecordType</code> is <code>null</code>
     */

    protected void removeSignalSearchRecordType(org.osid.type.Type signalSearchRecordType) {
        this.signalSearchRecordTypes.remove(signalSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Obstacle </code> record types. 
     *
     *  @return a list containing the supported <code> Obstacle </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObstacleRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.obstacleRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Obstacle </code> record type is supported. 
     *
     *  @param  obstacleRecordType a <code> Type </code> indicating an <code> 
     *          Obstacle </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> obstacleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObstacleRecordType(org.osid.type.Type obstacleRecordType) {
        return (this.obstacleRecordTypes.contains(obstacleRecordType));
    }


    /**
     *  Adds support for an obstacle record type.
     *
     *  @param obstacleRecordType an obstacle record type
     *  @throws org.osid.NullArgumentException
     *  <code>obstacleRecordType</code> is <code>null</code>
     */

    protected void addObstacleRecordType(org.osid.type.Type obstacleRecordType) {
        this.obstacleRecordTypes.add(obstacleRecordType);
        return;
    }


    /**
     *  Removes support for an obstacle record type.
     *
     *  @param obstacleRecordType an obstacle record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>obstacleRecordType</code> is <code>null</code>
     */

    protected void removeObstacleRecordType(org.osid.type.Type obstacleRecordType) {
        this.obstacleRecordTypes.remove(obstacleRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Obstacle </code> search types. 
     *
     *  @return a list containing the supported <code> Obstacle </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getObstacleSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.obstacleSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Obstacle </code> search type is supported. 
     *
     *  @param  obstacleSearchRecordType a <code> Type </code> indicating an 
     *          <code> Obstacle </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> obstacleSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsObstacleSearchRecordType(org.osid.type.Type obstacleSearchRecordType) {
        return (this.obstacleSearchRecordTypes.contains(obstacleSearchRecordType));
    }


    /**
     *  Adds support for an obstacle search record type.
     *
     *  @param obstacleSearchRecordType an obstacle search record type
     *  @throws org.osid.NullArgumentException
     *  <code>obstacleSearchRecordType</code> is <code>null</code>
     */

    protected void addObstacleSearchRecordType(org.osid.type.Type obstacleSearchRecordType) {
        this.obstacleSearchRecordTypes.add(obstacleSearchRecordType);
        return;
    }


    /**
     *  Removes support for an obstacle search record type.
     *
     *  @param obstacleSearchRecordType an obstacle search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>obstacleSearchRecordType</code> is <code>null</code>
     */

    protected void removeObstacleSearchRecordType(org.osid.type.Type obstacleSearchRecordType) {
        this.obstacleSearchRecordTypes.remove(obstacleSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ResourceVelocity </code> record types. 
     *
     *  @return a list containing the supported <code>
     *          ResourceVelocity </code> record types
     */

    @OSID @Override
    public org.osid.type.TypeList getResourceVelocityRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.resourceVelocityRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ResourceVelocity </code> record type
     *  is supported.
     *
     *  @param resourceVelocityRecordType a <code> Type </code>
     *         indicating a <code> ResourceVelocity </code> record
     *         type
     *  @return <code> true </code> if the given record type is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code>
     *          resourceVelocityRecordType </code> is <code> null
     *          </code>
     */

    @OSID @Override
    public boolean supportsResourceVelocityRecordType(org.osid.type.Type resourceVelocityRecordType) {
        return (this.resourceVelocityRecordTypes.contains(resourceVelocityRecordType));
    }


    /**
     *  Adds support for a resource velocity record type.
     *
     *  @param resourceVelocityRecordType a resource velocity record
     *         type
     *  @throws org.osid.NullArgumentException
     *          <code>resourceVelocityRecordType</code> is
     *          <code>null</code>
     */

    protected void addResourceVelocityRecordType(org.osid.type.Type resourceVelocityRecordType) {
        this.resourceVelocityRecordTypes.add(resourceVelocityRecordType);
        return;
    }


    /**
     *  Removes support for a resource velocity record type.
     *
     *  @param resourceVelocityRecordType a resource velocity record
     *         type
     *  @throws org.osid.NullArgumentException
     *          <code>resourceVelocityRecordType</code> is <code>null</code>
     */

    protected void removeResourceVelocityRecordType(org.osid.type.Type resourceVelocityRecordType) {
        this.resourceVelocityRecordTypes.remove(resourceVelocityRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service. 
     *
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathLookupSession getPathLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathLookupSession getPathLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathLookupSession getPathLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathLookupSession getPathLookupSessionForMap(org.osid.id.Id mapId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service. 
     *
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuerySession getPathQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuerySession getPathQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuerySession getPathQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuerySession getPathQuerySessionForMap(org.osid.id.Id mapId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service. 
     *
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchSession getPathSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchSession getPathSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchSession getPathSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSearchSession getPathSearchSessionForMap(org.osid.id.Id mapId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service. 
     *
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathAdminSession getPathAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathAdminSession getPathAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathAdminSession getPathAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathAdminSession getPathAdminSessionForMap(org.osid.id.Id mapId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service. 
     *
     *  @param  pathReceiver the notification callback 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathNotificationSession getPathNotificationSession(org.osid.mapping.path.PathReceiver pathReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathNotificationSession getPathNotificationSession(org.osid.mapping.path.PathReceiver pathReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service for the given map. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> or 
     *          <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathNotificationSession getPathNotificationSessionForMap(org.osid.mapping.path.PathReceiver pathReceiver, 
                                                                                          org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service for the given map. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver, mapId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathNotificationSession getPathNotificationSessionForMap(org.osid.mapping.path.PathReceiver pathReceiver, 
                                                                                          org.osid.id.Id mapId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup path/map mappings. 
     *
     *  @return a <code> PathMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathMapSession getPathMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup path/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathMapSession getPathMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning paths to 
     *  maps. 
     *
     *  @return a <code> PathMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathMapAssignmentSession getPathMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning paths to 
     *  maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathMapAssignmentSession getPathMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage path smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSmartMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSmartMapSession getPathSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage path smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSmartMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSmartMapSession getPathSmartMapSession(org.osid.id.Id mapId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  service. 
     *
     *  @return a <code> PathSpatialSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSpatial() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialSession getPathSpatialSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathSpatialSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathSpatialSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSpatial() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialSession getPathSpatialSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathSpatialSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathSpatialSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSpatial() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialSession getPathSpatialSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathSpatialSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSpatialSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSpatial() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialSession getPathSpatialSessionForMap(org.osid.id.Id mapId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathSpatialSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  design service. 
     *
     *  @return a <code> PathSpatialDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSpatialDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialDesignSession getPathSpatialDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathSpatialDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathSpatialDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSpatialDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialDesignSession getPathSpatialDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathSpatialDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  design service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathSpatialDesignSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSpatialDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialDesignSession getPathSpatialDesignSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathSpatialDesignSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path spatial 
     *  design service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSpatialDesignSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSpatialDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathSpatialDesignSession getPathSpatialDesignSessionForMap(org.osid.id.Id mapId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathSpatialDesignSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path travel 
     *  service. 
     *
     *  @return a <code> PathTravelSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathTravel() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathTravelSession getPathTravelSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathTravelSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path travel 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathTravelSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathTravel() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathTravelSession getPathTravelSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathTravelSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path travel 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> PathTravelSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathTravel() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathTravelSession getPathTravelSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getPathTravelSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path travel 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathTravelSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathTravel() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathTravelSession getPathTravelSessionForMap(org.osid.id.Id mapId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getPathTravelSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource path 
     *  notification service. 
     *
     *  @param  resourcePathReceiver the notification callback 
     *  @return a <code> ResourcePathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePathReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePathNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourcePathNotificationSession getResourcePathNotificationSession(org.osid.mapping.path.ResourcePathReceiver resourcePathReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourcePathNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource path 
     *  notification service. 
     *
     *  @param  resourcePathReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourcePathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePathReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePathNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourcePathNotificationSession getResourcePathNotificationSession(org.osid.mapping.path.ResourcePathReceiver resourcePathReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourcePathNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource path 
     *  notification service for the given map. 
     *
     *  @param  resourcePathReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourcePathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePathReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourcePathNotificationSession getResourcePathNotificationSessionForMap(org.osid.mapping.path.ResourcePathReceiver resourcePathReceiver, 
                                                                                                          org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourcePathNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource path 
     *  notification service for the given map. 
     *
     *  @param  resourcePathReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourcePathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourcePathReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcePathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourcePathNotificationSession getResourcePathNotificationSessionForMap(org.osid.mapping.path.ResourcePathReceiver resourcePathReceiver, 
                                                                                                          org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourcePathNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity service. 
     *
     *  @return a <code> ResourceVelocitySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocity() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocitySession getResourceVelocitySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourceVelocitySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocitySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocity() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocitySession getResourceVelocitySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourceVelocitySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceVelocitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocity() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocitySession getResourceVelocitySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourceVelocitySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocity() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocitySession getResourceVelocitySessionForMap(org.osid.id.Id mapId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourceVelocitySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity update service. 
     *
     *  @return a <code> ResourceVelocityUpdateSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityUpdate() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityUpdateSession getResourceVelocityUpdateSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourceVelocityUpdateSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity update service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocityUpdateSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityUpdate() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityUpdateSession getResourceVelocityUpdateSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourceVelocityUpdateSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity update service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceVelocityUpdateSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityUpdateSession getResourceVelocityUpdateSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourceVelocityUpdateSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity update service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocityUpdateSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityUpdateSession getResourceVelocityUpdateSessionForMap(org.osid.id.Id mapId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourceVelocityUpdateSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity notification service. 
     *
     *  @param  resourceVelocityReceiver the notification callback 
     *  @return a <code> ResourceVelocityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceVelocityReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityNotificationSession getResourceVelocityNotificationSession(org.osid.mapping.path.ResourceVelocityReceiver resourceVelocityReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourceVelocityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity notification service. 
     *
     *  @param  resourceVelocityReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceVelocityReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityNotificationSession getResourceVelocityNotificationSession(org.osid.mapping.path.ResourceVelocityReceiver resourceVelocityReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourceVelocityNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity notification service for the given map. 
     *
     *  @param  resourceVelocityReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceVelocityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceVelocityReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityNotificationSession getResourceVelocityNotificationSessionForMap(org.osid.mapping.path.ResourceVelocityReceiver resourceVelocityReceiver, 
                                                                                                                  org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourceVelocityNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  velocity notification service for the given map. 
     *
     *  @param  resourceVelocityReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceVelocityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          resourceVelocityReceiver, mapId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceVelocityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceVelocityNotificationSession getResourceVelocityNotificationSessionForMap(org.osid.mapping.path.ResourceVelocityReceiver resourceVelocityReceiver, 
                                                                                                                  org.osid.id.Id mapId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourceVelocityNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my path 
     *  service. 
     *
     *  @return a <code> MyPathSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MyPathSession getMyPathSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getMyPathSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my path 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyPathSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyPath() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MyPathSession getMyPathSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getMyPathSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my path 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> MyPathSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyPath() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MyPathSession getMyPathSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getMyPathSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my path 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MyPathSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyPath() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.MyPathSession getMyPathSessionForMap(org.osid.id.Id mapId, 
                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getMyPathSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  lookup service. 
     *
     *  @return an <code> IntersectionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionLookupSession getIntersectionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionLookupSession getIntersectionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> IntersectionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionLookupSession getIntersectionLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionLookupSession getIntersectionLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  query service. 
     *
     *  @return an <code> IntersectionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionQuerySession getIntersectionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionQuerySession getIntersectionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> IntersectionQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionQuerySession getIntersectionQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionQuerySession getIntersectionQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  search service. 
     *
     *  @return an <code> IntersectionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSearchSession getIntersectionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSearchSession getIntersectionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> IntersectionSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSearchSession getIntersectionSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSearchSession getIntersectionSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  administrative service. 
     *
     *  @return an <code> IntersectionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionAdminSession getIntersectionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionAdminSession getIntersectionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  administrative service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> IntersectionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionAdminSession getIntersectionAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  administrative service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionAdminSession getIntersectionAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  notification service. 
     *
     *  @param  intersectionReceiver the notification callback 
     *  @return an <code> IntersectionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> intersectionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionNotificationSession getIntersectionNotificationSession(org.osid.mapping.path.IntersectionReceiver intersectionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  notification service. 
     *
     *  @param  intersectionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> intersectionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionNotificationSession getIntersectionNotificationSession(org.osid.mapping.path.IntersectionReceiver intersectionReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  notification service for the given map. 
     *
     *  @param  intersectionReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> IntersectionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> intersectionReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionNotificationSession getIntersectionNotificationSessionForMap(org.osid.mapping.path.IntersectionReceiver intersectionReceiver, 
                                                                                                          org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the intersection 
     *  notification service for the given map. 
     *
     *  @param  intersectionReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> intersectionReceiver, 
     *          mapId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionNotificationSession getIntersectionNotificationSessionForMap(org.osid.mapping.path.IntersectionReceiver intersectionReceiver, 
                                                                                                          org.osid.id.Id mapId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup intersection/map 
     *  mappings. 
     *
     *  @return an <code> IntersectionMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionMapSession getIntersectionMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup intersection/map 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionMapSession getIntersectionMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  intersections to maps. 
     *
     *  @return an <code> IntersectionMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionMapAssignmentSession getIntersectionMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  intersections to maps. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionMapAssignmentSession getIntersectionMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage intersection smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> IntersectionSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSmartMapSession getIntersectionSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getIntersectionSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage intersection smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IntersectionSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIntersectionSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.IntersectionSmartMapSession getIntersectionSmartMapSession(org.osid.id.Id mapId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getIntersectionSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  lookup service. 
     *
     *  @return a <code> SpeedZoneLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneLookupSession getSpeedZoneLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneLookupSession getSpeedZoneLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> SpeedZoneLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneLookupSession getSpeedZoneLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneLookupSession getSpeedZoneLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  query service. 
     *
     *  @return a <code> SpeedZoneQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQuerySession getSpeedZoneQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQuerySession getSpeedZoneQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQuerySession getSpeedZoneQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  query service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneQuerySession getSpeedZoneQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  search service. 
     *
     *  @return a <code> SpeedZoneSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSearchSession getSpeedZoneSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSearchSession getSpeedZoneSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSearchSession getSpeedZoneSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSearchSession getSpeedZoneSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  administration service. 
     *
     *  @return a <code> SpeedZoneAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneAdminSession getSpeedZoneAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneAdminSession getSpeedZoneAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneAdminSession getSpeedZoneAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneAdminSession getSpeedZoneAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  notification service. 
     *
     *  @param  speedZoneReceiver the notification callback 
     *  @return a <code> SpeedZoneNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneNotificationSession getSpeedZoneNotificationSession(org.osid.mapping.path.SpeedZoneReceiver speedZoneReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  notification service. 
     *
     *  @param  speedZoneReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneNotificationSession getSpeedZoneNotificationSession(org.osid.mapping.path.SpeedZoneReceiver speedZoneReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  notification service for the given map. 
     *
     *  @param  speedZoneReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneNotificationSession getSpeedZoneNotificationSessionForMap(org.osid.mapping.path.SpeedZoneReceiver speedZoneReceiver, 
                                                                                                    org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the speed zone 
     *  notification service for the given map. 
     *
     *  @param  speedZoneReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> speedZoneReceiver, mapId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneNotificationSession getSpeedZoneNotificationSessionForMap(org.osid.mapping.path.SpeedZoneReceiver speedZoneReceiver, 
                                                                                                    org.osid.id.Id mapId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup speed zone/map mappings. 
     *
     *  @return a <code> SpeedZoneMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSpeedZoneMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneMapSession getSpeedZoneMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup speed zone/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSpeedZoneMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneMapSession getSpeedZoneMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning speed 
     *  zones to maps. 
     *
     *  @return a <code> SpeedZoneMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneMapAssignmentSession getSpeedZoneMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning speed 
     *  zones to maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneMapAssignmentSession getSpeedZoneMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SpeedZoneSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSmartMapSession getSpeedZoneSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSpeedZoneSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage speed zone smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SpeedZoneSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSpeedZoneSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SpeedZoneSmartMapSession getSpeedZoneSmartMapSession(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSpeedZoneSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal lookup 
     *  service. 
     *
     *  @return a <code> SignalLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalLookupSession getSignalLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalLookupSession getSignalLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> SignalLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalLookupSession getSignalLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> SignalLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalLookupSession getSignalLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal query 
     *  service. 
     *
     *  @return a <code> SignalQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQuerySession getSignalQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQuerySession getSignalQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQuerySession getSignalQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return a <code> SignalQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQuerySession getSignalQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal search 
     *  service. 
     *
     *  @return a <code> SignalSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSearchSession getSignalSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSearchSession getSignalSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSearchSession getSignalSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSearchSession getSignalSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  administration service. 
     *
     *  @return a <code> SignalAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalAdminSession getSignalAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalAdminSession getSignalAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalAdminSession getSignalAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalAdminSession getSignalAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  notification service. 
     *
     *  @param  signalReceiver the notification callback 
     *  @return a <code> SignalNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> signalReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalNotificationSession getSignalNotificationSession(org.osid.mapping.path.SignalReceiver signalReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  notification service. 
     *
     *  @param  signalReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SignalNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> signalReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalNotificationSession getSignalNotificationSession(org.osid.mapping.path.SignalReceiver signalReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  notification service for the given map. 
     *
     *  @param  signalReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> signalReceiver </code> 
     *          or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalNotificationSession getSignalNotificationSessionForMap(org.osid.mapping.path.SignalReceiver signalReceiver, 
                                                                                              org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the signal 
     *  notification service for the given map. 
     *
     *  @param  signalReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> signalReceiver, mapId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalNotificationSession getSignalNotificationSessionForMap(org.osid.mapping.path.SignalReceiver signalReceiver, 
                                                                                              org.osid.id.Id mapId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup signal/map mappings. 
     *
     *  @return a <code> SignalMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalMapSession getSignalMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup signal/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSignalMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalMapSession getSignalMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning signals 
     *  to maps. 
     *
     *  @return a <code> SignalMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalMapAssignmentSession getSignalMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning signals 
     *  to maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SignalMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalMapAssignmentSession getSignalMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> SignalSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSmartMapSession getSignalSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getSignalSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage signal smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SignalSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSignalSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalSmartMapSession getSignalSmartMapSession(org.osid.id.Id mapId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getSignalSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  signal notification service. 
     *
     *  @param  resourceSignaReceiver the notification callback 
     *  @return a <code> ResourceSignaNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceSignaReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSignaNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceSignalNotificationSession getResourceSignalNotificationSession(org.osid.mapping.path.ResourceVelocityReceiver resourceSignaReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourceSignalNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  signal notification service. 
     *
     *  @param  resourceSignaReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceSignaNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceSignaReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSignaNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceSignalNotificationSession getResourceSignalNotificationSession(org.osid.mapping.path.ResourceVelocityReceiver resourceSignaReceiver, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourceSignalNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  signal notification service for the given map. 
     *
     *  @param  resourceSignaReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceSignaNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceSignaReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSignaNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceSignalNotificationSession getResourceSignaNotificationSessionForMap(org.osid.mapping.path.ResourceVelocityReceiver resourceSignaReceiver, 
                                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getResourceSignaNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource 
     *  signal notification service for the given map. 
     *
     *  @param  resourceSignaReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceSignaNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceSignaReceiver, 
     *          </code> <code> mapId, </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSignaNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ResourceSignalNotificationSession getResourceSignaNotificationSessionForMap(org.osid.mapping.path.ResourceVelocityReceiver resourceSignaReceiver, 
                                                                                                             org.osid.id.Id mapId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getResourceSignaNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  lookup service. 
     *
     *  @return an <code> ObstacleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleLookupSession getObstacleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleLookupSession getObstacleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return an <code> ObstacleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleLookupSession getObstacleLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  lookup service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleLookupSession getObstacleLookupSessionForMap(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleLookupSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle query 
     *  service. 
     *
     *  @return an <code> ObstacleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQuerySession getObstacleQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQuerySession getObstacleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQuerySession getObstacleQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleQuerySession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQuerySession getObstacleQuerySessionForMap(org.osid.id.Id mapId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleQuerySessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  search service. 
     *
     *  @return an <code> ObstacleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSearchSession getObstacleSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSearchSession getObstacleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSearchSession getObstacleSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  search service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSearchSession getObstacleSearchSessionForMap(org.osid.id.Id mapId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleSearchSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  administration service. 
     *
     *  @return an <code> ObstacleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleAdminSession getObstacleAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleAdminSession getObstacleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleAdminSession getObstacleAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleAdminSession getObstacleAdminSessionForMap(org.osid.id.Id mapId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleAdminSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  notification service. 
     *
     *  @param  obstacleReceiver the notification callback 
     *  @return an <code> ObstacleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleNotificationSession getObstacleNotificationSession(org.osid.mapping.path.ObstacleReceiver obstacleReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  notification service. 
     *
     *  @param  obstacleReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleNotificationSession getObstacleNotificationSession(org.osid.mapping.path.ObstacleReceiver obstacleReceiver, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  notification service for the given map. 
     *
     *  @param  obstacleReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleReceiver </code> 
     *          or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleNotificationSession getObstacleNotificationSessionForMap(org.osid.mapping.path.ObstacleReceiver obstacleReceiver, 
                                                                                                  org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the obstacle 
     *  notification service for the given map. 
     *
     *  @param  obstacleReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> obstacleReceiver, mapId, 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleNotificationSession getObstacleNotificationSessionForMap(org.osid.mapping.path.ObstacleReceiver obstacleReceiver, 
                                                                                                  org.osid.id.Id mapId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleNotificationSessionForMap not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup obstacle/map mappings. 
     *
     *  @return an <code> ObstacleMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleMapSession getObstacleMapSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup obstacle/map mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleMapSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsObstacleMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleMapSession getObstacleMapSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  obstacles to maps. 
     *
     *  @return an <code> ObstacleMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleMapAssignmentSession getObstacleMapAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  obstacles to maps. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleMapAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleMapAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleMapAssignmentSession getObstacleMapAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleMapAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return an <code> ObstacleSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSmartMapSession getObstacleSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getObstacleSmartMapSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ObstacleSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObstacleSmartMap() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleSmartMapSession getObstacleSmartMapSession(org.osid.id.Id mapId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getObstacleSmartMapSession not implemented");
    }


    /**
     *  Gets a <code> MappingPathBatchManager. </code> 
     *
     *  @return a <code> MappingPathBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingPathBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.MappingPathBatchManager getMappingPathBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getMappingPathBatchManager not implemented");
    }


    /**
     *  Gets a <code> MappingPathBatchproxyManager. </code> 
     *
     *  @return a <code> MappingPathBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingPathBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.batch.MappingPathBatchProxyManager getMappingPathBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getMappingPathBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> MappingPathRulesManager. </code> 
     *
     *  @return a <code> MappingPathRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingPathRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.MappingPathRulesManager getMappingPathRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathManager.getMappingPathRulesManager not implemented");
    }


    /**
     *  Gets a <code> MappingPathRulesProxyManager. </code> 
     *
     *  @return a <code> MappingPathRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingPathRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.rules.MappingPathRulesProxyManager getMappingPathRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.mapping.path.MappingPathProxyManager.getMappingPathRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.pathRecordTypes.clear();
        this.pathRecordTypes.clear();

        this.pathSearchRecordTypes.clear();
        this.pathSearchRecordTypes.clear();

        this.intersectionRecordTypes.clear();
        this.intersectionRecordTypes.clear();

        this.intersectionSearchRecordTypes.clear();
        this.intersectionSearchRecordTypes.clear();

        this.speedZoneRecordTypes.clear();
        this.speedZoneRecordTypes.clear();

        this.speedZoneSearchRecordTypes.clear();
        this.speedZoneSearchRecordTypes.clear();

        this.signalRecordTypes.clear();
        this.signalRecordTypes.clear();

        this.signalSearchRecordTypes.clear();
        this.signalSearchRecordTypes.clear();

        this.obstacleRecordTypes.clear();
        this.obstacleRecordTypes.clear();

        this.obstacleSearchRecordTypes.clear();
        this.obstacleSearchRecordTypes.clear();

        this.resourceVelocityRecordTypes.clear();

        return;
    }
}
