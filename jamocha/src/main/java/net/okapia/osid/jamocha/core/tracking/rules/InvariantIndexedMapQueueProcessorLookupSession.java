//
// InvariantIndexedMapQueueProcessorLookupSession
//
//    Implements a QueueProcessor lookup service backed by a fixed
//    collection of queueProcessors indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules;


/**
 *  Implements a QueueProcessor lookup service backed by a fixed
 *  collection of queue processors. The queue processors are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some queue processors may be compatible
 *  with more types than are indicated through these queue processor
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapQueueProcessorLookupSession
    extends net.okapia.osid.jamocha.core.tracking.rules.spi.AbstractIndexedMapQueueProcessorLookupSession
    implements org.osid.tracking.rules.QueueProcessorLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapQueueProcessorLookupSession} using an
     *  array of queueProcessors.
     *
     *  @param frontOffice the front office
     *  @param queueProcessors an array of queue processors
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueProcessors} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                    org.osid.tracking.rules.QueueProcessor[] queueProcessors) {

        setFrontOffice(frontOffice);
        putQueueProcessors(queueProcessors);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapQueueProcessorLookupSession} using a
     *  collection of queue processors.
     *
     *  @param frontOffice the front office
     *  @param queueProcessors a collection of queue processors
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueProcessors} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapQueueProcessorLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                    java.util.Collection<? extends org.osid.tracking.rules.QueueProcessor> queueProcessors) {

        setFrontOffice(frontOffice);
        putQueueProcessors(queueProcessors);
        return;
    }
}
