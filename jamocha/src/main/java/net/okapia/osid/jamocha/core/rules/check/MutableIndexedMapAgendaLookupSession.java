//
// MutableIndexedMapAgendaLookupSession
//
//    Implements an Agenda lookup service backed by a collection of
//    agendas indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check;


/**
 *  Implements an Agenda lookup service backed by a collection of
 *  agendas. The agendas are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some agendas may be compatible
 *  with more types than are indicated through these agenda
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of agendas can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapAgendaLookupSession
    extends net.okapia.osid.jamocha.core.rules.check.spi.AbstractIndexedMapAgendaLookupSession
    implements org.osid.rules.check.AgendaLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapAgendaLookupSession} with no agendas.
     *
     *  @param engine the engine
     *  @throws org.osid.NullArgumentException {@code engine}
     *          is {@code null}
     */

      public MutableIndexedMapAgendaLookupSession(org.osid.rules.Engine engine) {
        setEngine(engine);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAgendaLookupSession} with a
     *  single agenda.
     *  
     *  @param engine the engine
     *  @param  agenda an single agenda
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code agenda} is {@code null}
     */

    public MutableIndexedMapAgendaLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.rules.check.Agenda agenda) {
        this(engine);
        putAgenda(agenda);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAgendaLookupSession} using an
     *  array of agendas.
     *
     *  @param engine the engine
     *  @param  agendas an array of agendas
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code agendas} is {@code null}
     */

    public MutableIndexedMapAgendaLookupSession(org.osid.rules.Engine engine,
                                                  org.osid.rules.check.Agenda[] agendas) {
        this(engine);
        putAgendas(agendas);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapAgendaLookupSession} using a
     *  collection of agendas.
     *
     *  @param engine the engine
     *  @param  agendas a collection of agendas
     *  @throws org.osid.NullArgumentException {@code engine} or
     *          {@code agendas} is {@code null}
     */

    public MutableIndexedMapAgendaLookupSession(org.osid.rules.Engine engine,
                                                  java.util.Collection<? extends org.osid.rules.check.Agenda> agendas) {

        this(engine);
        putAgendas(agendas);
        return;
    }
    

    /**
     *  Makes an {@code Agenda} available in this session.
     *
     *  @param  agenda an agenda
     *  @throws org.osid.NullArgumentException {@code agenda{@code  is
     *          {@code null}
     */

    @Override
    public void putAgenda(org.osid.rules.check.Agenda agenda) {
        super.putAgenda(agenda);
        return;
    }


    /**
     *  Makes an array of agendas available in this session.
     *
     *  @param  agendas an array of agendas
     *  @throws org.osid.NullArgumentException {@code agendas{@code 
     *          is {@code null}
     */

    @Override
    public void putAgendas(org.osid.rules.check.Agenda[] agendas) {
        super.putAgendas(agendas);
        return;
    }


    /**
     *  Makes collection of agendas available in this session.
     *
     *  @param  agendas a collection of agendas
     *  @throws org.osid.NullArgumentException {@code agenda{@code  is
     *          {@code null}
     */

    @Override
    public void putAgendas(java.util.Collection<? extends org.osid.rules.check.Agenda> agendas) {
        super.putAgendas(agendas);
        return;
    }


    /**
     *  Removes an Agenda from this session.
     *
     *  @param agendaId the {@code Id} of the agenda
     *  @throws org.osid.NullArgumentException {@code agendaId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAgenda(org.osid.id.Id agendaId) {
        super.removeAgenda(agendaId);
        return;
    }    
}
