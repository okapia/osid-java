//
// MutableMapProxyMapLookupSession
//
//    Implements a Map lookup service backed by a collection of
//    maps that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping;


/**
 *  Implements a Map lookup service backed by a collection of
 *  maps. The maps are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of maps can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyMapLookupSession
    extends net.okapia.osid.jamocha.core.mapping.spi.AbstractMapMapLookupSession
    implements org.osid.mapping.MapLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyMapLookupSession} with no
     *  maps.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyMapLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyMapLookupSession} with a
     *  single map.
     *
     *  @param map a map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyMapLookupSession(org.osid.mapping.Map map, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putMap(map);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyMapLookupSession} using an
     *  array of maps.
     *
     *  @param maps an array of maps
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code maps} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyMapLookupSession(org.osid.mapping.Map[] maps, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putMaps(maps);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyMapLookupSession} using
     *  a collection of maps.
     *
     *  @param maps a collection of maps
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code maps} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyMapLookupSession(java.util.Collection<? extends org.osid.mapping.Map> maps,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putMaps(maps);
        return;
    }

    
    /**
     *  Makes a {@code Map} available in this session.
     *
     *  @param map an map
     *  @throws org.osid.NullArgumentException {@code map{@code 
     *          is {@code null}
     */

    @Override
    public void putMap(org.osid.mapping.Map map) {
        super.putMap(map);
        return;
    }


    /**
     *  Makes an array of maps available in this session.
     *
     *  @param maps an array of maps
     *  @throws org.osid.NullArgumentException {@code maps{@code 
     *          is {@code null}
     */

    @Override
    public void putMaps(org.osid.mapping.Map[] maps) {
        super.putMaps(maps);
        return;
    }


    /**
     *  Makes collection of maps available in this session.
     *
     *  @param maps
     *  @throws org.osid.NullArgumentException {@code map{@code 
     *          is {@code null}
     */

    @Override
    public void putMaps(java.util.Collection<? extends org.osid.mapping.Map> maps) {
        super.putMaps(maps);
        return;
    }


    /**
     *  Removes a Map from this session.
     *
     *  @param mapId the {@code Id} of the map
     *  @throws org.osid.NullArgumentException {@code mapId{@code  is
     *          {@code null}
     */

    @Override
    public void removeMap(org.osid.id.Id mapId) {
        super.removeMap(mapId);
        return;
    }    
}
