//
// AbstractFederatingModuleLookupSession.java
//
//     An abstract federating adapter for a ModuleLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ModuleLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingModuleLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.syllabus.ModuleLookupSession>
    implements org.osid.course.syllabus.ModuleLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingModuleLookupSession</code>.
     */

    protected AbstractFederatingModuleLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.syllabus.ModuleLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Module</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupModules() {
        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            if (session.canLookupModules()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Module</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeModuleView() {
        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            session.useComparativeModuleView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Module</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryModuleView() {
        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            session.usePlenaryModuleView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include modules in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only active modules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveModuleView() {
        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            session.useActiveModuleView();
        }

        return;
    }


    /**
     *  Active and inactive modules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusModuleView() {
        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            session.useAnyStatusModuleView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>Module</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Module</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Module</code> and
     *  retained for compatibility.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleId <code>Id</code> of the
     *          <code>Module</code>
     *  @return the module
     *  @throws org.osid.NotFoundException <code>moduleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>moduleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Module getModule(org.osid.id.Id moduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            try {
                return (session.getModule(moduleId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(moduleId + " not found");
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  modules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Modules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>moduleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByIds(org.osid.id.IdList moduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.syllabus.module.MutableModuleList ret = new net.okapia.osid.jamocha.course.syllabus.module.MutableModuleList();

        try (org.osid.id.IdList ids = moduleIds) {
            while (ids.hasNext()) {
                ret.addModule(getModule(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  module genus <code>Type</code> which does not include
     *  modules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.FederatingModuleList ret = getModuleList();

        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            ret.addModuleList(session.getModulesByGenusType(moduleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  module genus <code>Type</code> and include any additional
     *  modules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByParentGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.FederatingModuleList ret = getModuleList();

        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            ret.addModuleList(session.getModulesByParentGenusType(moduleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ModuleList</code> containing the given
     *  module record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  moduleRecordType a module record type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByRecordType(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.FederatingModuleList ret = getModuleList();

        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            ret.addModuleList(session.getModulesByRecordType(moduleRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ModuleList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known modules or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  modules that are accessible through this session. 
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Module</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.FederatingModuleList ret = getModuleList();

        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            ret.addModuleList(session.getModulesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> ModuleList </code> for the given syllabus <code>
     *  Id.</code>
     *
     *  In plenary mode, the returned list contains all known modules
     *  or an error results. Otherwise, the returned list may contain
     *  only those modules that are accessible through this session.
     *
     *  In active mode, modeules are returned that are currently
     *  active. In any status mode, active and inactive modules are
     *  returned.
     *
     *  @param  syllabusId a syllabus <code> Id </code>
     *  @return the returned <code> Module </code> list
     *  @throws org.osid.NullArgumentException <code> syllabusId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesForSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.FederatingModuleList ret = getModuleList();

        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            ret.addModuleList(session.getModulesForSyllabus(syllabusId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Modules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  modules or an error results. Otherwise, the returned list
     *  may contain only those modules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, modules are returned that are currently
     *  active. In any status mode, active and inactive modules
     *  are returned.
     *
     *  @return a list of <code>Modules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.FederatingModuleList ret = getModuleList();

        for (org.osid.course.syllabus.ModuleLookupSession session : getSessions()) {
            ret.addModuleList(session.getModules());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.FederatingModuleList getModuleList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.ParallelModuleList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.syllabus.module.CompositeModuleList());
        }
    }
}
