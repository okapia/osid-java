//
// QueueConstrainerMiter.java
//
//     Defines a QueueConstrainer miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.queueconstrainer;


/**
 *  Defines a <code>QueueConstrainer</code> miter for use with the builders.
 */

public interface QueueConstrainerMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidConstrainerMiter,
            org.osid.provisioning.rules.QueueConstrainer {


    /**
     *  Sets the size limit.
     *
     *  @param limit a size limit
     *  @throws org.osid.InvalidArgumentException <code>limit</code> is
     *          negative
     */

    public void setSizeLimit(long limit);


    /**
     *  Adds a required provision pool.
     *
     *  @param pool a required provision pool
     *  @throws org.osid.NullArgumentException <code>pool</code> is
     *          <code>null</code>
     */

    public void addRequiredProvisionPool(org.osid.provisioning.Pool pool);


    /**
     *  Sets all the required provision pools.
     *
     *  @param pools a collection of required provision pools
     *  @throws org.osid.NullArgumentException <code>pools</code> is
     *          <code>null</code>
     */

    public void setRequiredProvisionPools(java.util.Collection<org.osid.provisioning.Pool> pools);


    /**
     *  Adds a QueueConstrainer record.
     *
     *  @param record a queueConstrainer record
     *  @param recordType the type of queueConstrainer record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addQueueConstrainerRecord(org.osid.provisioning.rules.records.QueueConstrainerRecord record, org.osid.type.Type recordType);
}       


