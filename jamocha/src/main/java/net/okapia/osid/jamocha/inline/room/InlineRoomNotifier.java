//
// InlineRoomNotifier.java
//
//     A callback interface for performing room
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.room.spi;


/**
 *  A callback interface for performing room
 *  notifications.
 */

public interface InlineRoomNotifier {



    /**
     *  Notifies the creation of a new room.
     *
     *  @param roomId the {@code Id} of the new 
     *         room
     *  @param peer1Id the {@code Id} of the peer1
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          or {@code peer1Id} is {@code null}
     */

    public void newRoom(org.osid.id.Id roomId, org.osid.id.Id peer1Id);


    /**
     *  Notifies the change of an updated room.
     *
     *  @param roomId the {@code Id} of the changed
     *         room
     *  @param peer1Id the {@code Id} of the peer1
     *  @throws org.osid.NullArgumentException {@code [objectId]} or
     *          {@code peer1Id} is {@code null}
     */

    public void changedRoom(org.osid.id.Id roomId, org.osid.id.Id peer1Id);


    /**
     *  Notifies a deleted room.
     *
     *  @param roomId the {@code Id} of the deleted
     *         room
     *  @param peer1Id the {@code Id} of the peer1
     *  @throws org.osid.NullArgumentException {@code [objectId]} or
     *          {@code peer1Id} is {@code null}
     */

    public void deletedRoom(org.osid.id.Id roomId, org.osid.id.Id peer1Id);

}
