//
// MutableIndexedMapProxyQueueProcessorEnablerLookupSession
//
//    Implements a QueueProcessorEnabler lookup service backed by a collection of
//    queueProcessorEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules;


/**
 *  Implements a QueueProcessorEnabler lookup service backed by a collection of
 *  queueProcessorEnablers. The queue processor enablers are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some queueProcessorEnablers may be compatible
 *  with more types than are indicated through these queueProcessorEnabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of queue processor enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyQueueProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.tracking.rules.spi.AbstractIndexedMapQueueProcessorEnablerLookupSession
    implements org.osid.tracking.rules.QueueProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyQueueProcessorEnablerLookupSession} with
     *  no queue processor enabler.
     *
     *  @param frontOffice the front office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyQueueProcessorEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                       org.osid.proxy.Proxy proxy) {
        setFrontOffice(frontOffice);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyQueueProcessorEnablerLookupSession} with
     *  a single queue processor enabler.
     *
     *  @param frontOffice the front office
     *  @param  queueProcessorEnabler an queue processor enabler
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueProcessorEnabler}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyQueueProcessorEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                       org.osid.tracking.rules.QueueProcessorEnabler queueProcessorEnabler, org.osid.proxy.Proxy proxy) {

        this(frontOffice, proxy);
        putQueueProcessorEnabler(queueProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyQueueProcessorEnablerLookupSession} using
     *  an array of queue processor enablers.
     *
     *  @param frontOffice the front office
     *  @param  queueProcessorEnablers an array of queue processor enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueProcessorEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyQueueProcessorEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                       org.osid.tracking.rules.QueueProcessorEnabler[] queueProcessorEnablers, org.osid.proxy.Proxy proxy) {

        this(frontOffice, proxy);
        putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyQueueProcessorEnablerLookupSession} using
     *  a collection of queue processor enablers.
     *
     *  @param frontOffice the front office
     *  @param  queueProcessorEnablers a collection of queue processor enablers
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code frontOffice},
     *          {@code queueProcessorEnablers}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyQueueProcessorEnablerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                       java.util.Collection<? extends org.osid.tracking.rules.QueueProcessorEnabler> queueProcessorEnablers,
                                                       org.osid.proxy.Proxy proxy) {
        this(frontOffice, proxy);
        putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }

    
    /**
     *  Makes a {@code QueueProcessorEnabler} available in this session.
     *
     *  @param  queueProcessorEnabler a queue processor enabler
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueProcessorEnabler(org.osid.tracking.rules.QueueProcessorEnabler queueProcessorEnabler) {
        super.putQueueProcessorEnabler(queueProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of queue processor enablers available in this session.
     *
     *  @param  queueProcessorEnablers an array of queue processor enablers
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueProcessorEnablers(org.osid.tracking.rules.QueueProcessorEnabler[] queueProcessorEnablers) {
        super.putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }


    /**
     *  Makes collection of queue processor enablers available in this session.
     *
     *  @param  queueProcessorEnablers a collection of queue processor enablers
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnabler{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueProcessorEnablers(java.util.Collection<? extends org.osid.tracking.rules.QueueProcessorEnabler> queueProcessorEnablers) {
        super.putQueueProcessorEnablers(queueProcessorEnablers);
        return;
    }


    /**
     *  Removes a QueueProcessorEnabler from this session.
     *
     *  @param queueProcessorEnablerId the {@code Id} of the queue processor enabler
     *  @throws org.osid.NullArgumentException {@code queueProcessorEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeQueueProcessorEnabler(org.osid.id.Id queueProcessorEnablerId) {
        super.removeQueueProcessorEnabler(queueProcessorEnablerId);
        return;
    }    
}
