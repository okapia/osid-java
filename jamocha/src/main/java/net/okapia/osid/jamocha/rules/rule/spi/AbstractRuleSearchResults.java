//
// AbstractRuleSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.rule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRuleSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.rules.RuleSearchResults {

    private org.osid.rules.RuleList rules;
    private final org.osid.rules.RuleQueryInspector inspector;
    private final java.util.Collection<org.osid.rules.records.RuleSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRuleSearchResults.
     *
     *  @param rules the result set
     *  @param ruleQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>rules</code>
     *          or <code>ruleQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRuleSearchResults(org.osid.rules.RuleList rules,
                                            org.osid.rules.RuleQueryInspector ruleQueryInspector) {
        nullarg(rules, "rules");
        nullarg(ruleQueryInspector, "rule query inspectpr");

        this.rules = rules;
        this.inspector = ruleQueryInspector;

        return;
    }


    /**
     *  Gets the rule list resulting from a search.
     *
     *  @return a rule list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.rules.RuleList getRules() {
        if (this.rules == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.rules.RuleList rules = this.rules;
        this.rules = null;
	return (rules);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.rules.RuleQueryInspector getRuleQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  rule search record <code> Type. </code> This method must
     *  be used to retrieve a rule implementing the requested
     *  record.
     *
     *  @param ruleSearchRecordType a rule search 
     *         record type 
     *  @return the rule search
     *  @throws org.osid.NullArgumentException
     *          <code>ruleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(ruleSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.records.RuleSearchResultsRecord getRuleSearchResultsRecord(org.osid.type.Type ruleSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.rules.records.RuleSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(ruleSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(ruleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record rule search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRuleRecord(org.osid.rules.records.RuleSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "rule record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
