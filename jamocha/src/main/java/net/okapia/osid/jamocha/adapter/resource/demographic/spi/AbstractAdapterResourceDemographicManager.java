//
// AbstractResourceDemographicManager.java
//
//     An adapter for a ResourceDemographicManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ResourceDemographicManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterResourceDemographicManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.resource.demographic.ResourceDemographicManager>
    implements org.osid.resource.demographic.ResourceDemographicManager {


    /**
     *  Constructs a new {@code AbstractAdapterResourceDemographicManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterResourceDemographicManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterResourceDemographicManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterResourceDemographicManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if demographic is supported. 
     *
     *  @return <code> true </code> if demographic query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographics() {
        return (getAdapteeManager().supportsDemographics());
    }


    /**
     *  Tests if looking up demographic is supported. 
     *
     *  @return <code> true </code> if demographic lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicLookup() {
        return (getAdapteeManager().supportsDemographicLookup());
    }


    /**
     *  Tests if querying demographic is supported. 
     *
     *  @return <code> true </code> if demographic query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicQuery() {
        return (getAdapteeManager().supportsDemographicQuery());
    }


    /**
     *  Tests if searching demographic is supported. 
     *
     *  @return <code> true </code> if demographic search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicSearch() {
        return (getAdapteeManager().supportsDemographicSearch());
    }


    /**
     *  Tests if a demographic administrative service is supported. 
     *
     *  @return <code> true </code> if demographic administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicAdmin() {
        return (getAdapteeManager().supportsDemographicAdmin());
    }


    /**
     *  Tests if a demographic builder service is supported. 
     *
     *  @return <code> true </code> if demographic builder service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicBuilder() {
        return (getAdapteeManager().supportsDemographicBuilder());
    }


    /**
     *  Tests if a demographic notification service is supported. 
     *
     *  @return <code> true </code> if demographic notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicNotification() {
        return (getAdapteeManager().supportsDemographicNotification());
    }


    /**
     *  Tests if a demographic bin lookup service is supported. 
     *
     *  @return <code> true </code> if a demographic bin lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicBin() {
        return (getAdapteeManager().supportsDemographicBin());
    }


    /**
     *  Tests if a demographic bin service is supported. 
     *
     *  @return <code> true </code> if demographic bin assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicBinAssignment() {
        return (getAdapteeManager().supportsDemographicBinAssignment());
    }


    /**
     *  Tests if a demographic bin lookup service is supported. 
     *
     *  @return <code> true </code> if a demographic bin service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicSmartBin() {
        return (getAdapteeManager().supportsDemographicSmartBin());
    }


    /**
     *  Tests if looking up demographic enablers is supported. 
     *
     *  @return <code> true </code> if demographic enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerLookup() {
        return (getAdapteeManager().supportsDemographicEnablerLookup());
    }


    /**
     *  Tests if querying demographic enablers is supported. 
     *
     *  @return <code> true </code> if demographic enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerQuery() {
        return (getAdapteeManager().supportsDemographicEnablerQuery());
    }


    /**
     *  Tests if searching demographic enablers is supported. 
     *
     *  @return <code> true </code> if demographic enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerSearch() {
        return (getAdapteeManager().supportsDemographicEnablerSearch());
    }


    /**
     *  Tests if a demographic enabler administrative service is supported. 
     *
     *  @return <code> true </code> if demographic enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerAdmin() {
        return (getAdapteeManager().supportsDemographicEnablerAdmin());
    }


    /**
     *  Tests if a demographic enabler notification service is supported. 
     *
     *  @return <code> true </code> if demographic enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerNotification() {
        return (getAdapteeManager().supportsDemographicEnablerNotification());
    }


    /**
     *  Tests if a demographic enabler bin lookup service is supported. 
     *
     *  @return <code> true </code> if a demographic enabler bin lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerBin() {
        return (getAdapteeManager().supportsDemographicEnablerBin());
    }


    /**
     *  Tests if a demographic enabler bin service is supported. 
     *
     *  @return <code> true </code> if demographic enabler bin assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerBinAssignment() {
        return (getAdapteeManager().supportsDemographicEnablerBinAssignment());
    }


    /**
     *  Tests if a demographic enabler bin lookup service is supported. 
     *
     *  @return <code> true </code> if a demographic enabler bin service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerSmartBin() {
        return (getAdapteeManager().supportsDemographicEnablerSmartBin());
    }


    /**
     *  Tests if a demographic enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a processor enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerRuleLookup() {
        return (getAdapteeManager().supportsDemographicEnablerRuleLookup());
    }


    /**
     *  Tests if a demographic enabler rule application service is supported. 
     *
     *  @return <code> true </code> if demographic enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerRuleApplication() {
        return (getAdapteeManager().supportsDemographicEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> Demographic </code> record types. 
     *
     *  @return a list containing the supported <code> Demographic </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDemographicRecordTypes() {
        return (getAdapteeManager().getDemographicRecordTypes());
    }


    /**
     *  Tests if the given <code> Demographic </code> record type is 
     *  supported. 
     *
     *  @param  demographicRecordType a <code> Type </code> indicating a 
     *          <code> Demographic </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> demographicRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDemographicRecordType(org.osid.type.Type demographicRecordType) {
        return (getAdapteeManager().supportsDemographicRecordType(demographicRecordType));
    }


    /**
     *  Gets the supported <code> Demographic </code> search record types. 
     *
     *  @return a list containing the supported <code> Demographic </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDemographicSearchRecordTypes() {
        return (getAdapteeManager().getDemographicSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Demographic </code> search record type is 
     *  supported. 
     *
     *  @param  demographicSearchRecordType a <code> Type </code> indicating a 
     *          <code> Demographic </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDemographicSearchRecordType(org.osid.type.Type demographicSearchRecordType) {
        return (getAdapteeManager().supportsDemographicSearchRecordType(demographicSearchRecordType));
    }


    /**
     *  Gets the supported <code> DemographicEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> DemographicEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDemographicEnablerRecordTypes() {
        return (getAdapteeManager().getDemographicEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> DemographicEnabler </code> record type is 
     *  supported. 
     *
     *  @param  demographicEnablerRecordType a <code> Type </code> indicating 
     *          a <code> DemographicEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerRecordType(org.osid.type.Type demographicEnablerRecordType) {
        return (getAdapteeManager().supportsDemographicEnablerRecordType(demographicEnablerRecordType));
    }


    /**
     *  Gets the supported <code> DemographicEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> DemographicEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDemographicEnablerSearchRecordTypes() {
        return (getAdapteeManager().getDemographicEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> DemographicEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  demographicEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> DemographicEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerSearchRecordType(org.osid.type.Type demographicEnablerSearchRecordType) {
        return (getAdapteeManager().supportsDemographicEnablerSearchRecordType(demographicEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  lookup service. 
     *
     *  @return a <code> DemographicLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicLookupSession getDemographicLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicLookupSession getDemographicLookupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicLookupSessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  query service. 
     *
     *  @return a <code> DemographicQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuerySession getDemographicQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuerySession getDemographicQuerySessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicQuerySessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  search service. 
     *
     *  @return a <code> DemographicSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSearchSession getDemographicSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  earch service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSearchSession getDemographicSearchSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicSearchSessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  administration service. 
     *
     *  @return a <code> DemographicAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicAdminSession getDemographicAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicAdminSession getDemographicAdminSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicAdminSessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  builder service. 
     *
     *  @return a <code> DemographicBuilderSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBuilder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBuilderSession getDemographicBuilderSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicBuilderSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  builder service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicBuilderSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBuilder() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBuilderSession getDemographicBuilderSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicBuilderSessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  notification service. 
     *
     *  @param  demographicReceiver the notification callback 
     *  @return a <code> DemographicNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> demographicReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicNotificationSession getDemographicNotificationSession(org.osid.resource.demographic.DemographicReceiver demographicReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicNotificationSession(demographicReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  notification service for the given bin. 
     *
     *  @param  demographicReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> demographicReceiver 
     *          </code> or <code> binId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicNotificationSession getDemographicNotificationSessionForBin(org.osid.resource.demographic.DemographicReceiver demographicReceiver, 
                                                                                                                org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicNotificationSessionForBin(demographicReceiver, binId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup demographic/bin mappings 
     *  for demographics. 
     *
     *  @return a <code> DemographicBinSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBinSession getDemographicBinSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicBinSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  demographic to bins. 
     *
     *  @return a <code> DemographicBinAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBinAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBinAssignmentSession getDemographicBinAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicBinAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage demographic smart bins. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicSmartBinSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSmartBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSmartBinSession getDemographicSmartBinSession(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicSmartBinSession(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler lookup service. 
     *
     *  @return a <code> DemographicEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerLookupSession getDemographicEnablerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerLookupSession getDemographicEnablerLookupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerLookupSessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler query service. 
     *
     *  @return a <code> DemographicEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerQuerySession getDemographicEnablerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerQuerySession getDemographicEnablerQuerySessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerQuerySessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler search service. 
     *
     *  @return a <code> DemographicEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSearchSession getDemographicEnablerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enablers earch service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSearchSession getDemographicEnablerSearchSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerSearchSessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler administration service. 
     *
     *  @return a <code> DemographicEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerAdminSession getDemographicEnablerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerAdminSession getDemographicEnablerAdminSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerAdminSessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler notification service. 
     *
     *  @param  demographicEnablerReceiver the notification callback 
     *  @return a <code> DemographicEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerNotificationSession getDemographicEnablerNotificationSession(org.osid.resource.demographic.DemographicEnablerReceiver demographicEnablerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerNotificationSession(demographicEnablerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler notification service for the given bin. 
     *
     *  @param  demographicEnablerReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerReceiver </code> or <code> binId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerNotificationSession getDemographicEnablerNotificationSessionForBin(org.osid.resource.demographic.DemographicEnablerReceiver demographicEnablerReceiver, 
                                                                                                                              org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerNotificationSessionForBin(demographicEnablerReceiver, binId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup demographic enabler/bin 
     *  mappings for demographic enablers. 
     *
     *  @return a <code> DemographicEnablerBinSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerBinSession getDemographicEnablerBinSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerBinSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  demographic enablers to bins. 
     *
     *  @return a <code> DemographicEnablerBinAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerBinAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerBinAssignmentSession getDemographicEnablerBinAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerBinAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage demographic enabler 
     *  smart bins. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerSmartBinSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSmartBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSmartBinSession getDemographicEnablerSmartBinSession(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerSmartBinSession(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> DemographicEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleLookupSession getDemographicEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler mapping lookup service. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleLookupSession getDemographicEnablerRuleLookupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerRuleLookupSessionForBin(binId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler assignment service. 
     *
     *  @return a <code> DemographicEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleApplicationSession getDemographicEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerRuleApplicationSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler assignment service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleApplicationSession getDemographicEnablerRuleApplicationSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDemographicEnablerRuleApplicationSessionForBin(binId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
