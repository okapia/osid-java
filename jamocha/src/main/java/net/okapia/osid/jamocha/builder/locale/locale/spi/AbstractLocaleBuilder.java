//
// AbstractLocale.java
//
//     Defines a Locale builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.locale.locale.spi;


/**
 *  Defines a <code>Locale</code> builder.
 */

public abstract class AbstractLocaleBuilder<T extends AbstractLocaleBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.locale.locale.LocaleMiter locale;


    /**
     *  Constructs a new <code>AbstractLocaleBuilder</code>.
     *
     *  @param locale the locale to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractLocaleBuilder(net.okapia.osid.jamocha.builder.locale.locale.LocaleMiter locale) {
        this.locale = locale;
        return;
    }


    /**
     *  Builds the locale.
     *
     *  @return the new locale
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.locale.Locale build() {
        (new net.okapia.osid.jamocha.builder.validator.locale.locale.LocaleValidator(getValidations())).validate(this.locale);
        return (new net.okapia.osid.jamocha.builder.locale.locale.ImmutableLocale(this.locale));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the locale miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.locale.locale.LocaleMiter getMiter() {
        return (this.locale);
    }


    /**
     *  Sets the language <code>Type</code>.
     *
     *  @param type the language type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T language(org.osid.type.Type type) {
        getMiter().setLanguageType(type);
        return (self());
    }


    /**
     *  Sets the script <code>Type</code>.
     *
     *  @param type the script type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T script(org.osid.type.Type type) {
        getMiter().setScriptType(type);
        return (self());
    }


    /**
     *  Sets the calendar <code>Type</code>.
     *
     *  @param type the calendar type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T calendar(org.osid.type.Type type) {
        getMiter().setCalendarType(type);
        return (self());
    }


    /**
     *  Sets the time <code>Type</code>.
     *
     *  @param type the time type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T time(org.osid.type.Type type) {
        getMiter().setTimeType(type);
        return (self());
    }


    /**
     *  Sets the currency <code>Type</code>.
     *
     *  @param type the currency type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T currency(org.osid.type.Type type) {
        getMiter().setCurrencyType(type);
        return (self());
    }


    /**
     *  Sets the unit system <code>Type</code>.
     *
     *  @param type the unit system type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T unitSystem(org.osid.type.Type type) {
        getMiter().setUnitSystemType(type);
        return (self());
    }


    /**
     *  Sets the numeric format <code>Type</code>.
     *
     *  @param type the numeric format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T numericFormat(org.osid.type.Type type) {
        getMiter().setNumericFormatType(type);
        return (self());
    }


    /**
     *  Sets the calendar format <code>Type</code>.
     *
     *  @param type the calendar format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T calendarFormat(org.osid.type.Type type) {
        getMiter().setCalendarFormatType(type);
        return (self());
    }


    /**
     *  Sets the time format <code>Type</code>.
     *
     *  @param type the time format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T timeFormat(org.osid.type.Type type) {
        getMiter().setTimeFormatType(type);
        return (self());
    }


    /**
     *  Sets the currency format <code>Type</code>.
     *
     *  @param type the currency format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T currencyFormat(org.osid.type.Type type) {
        getMiter().setCurrencyFormatType(type);
        return (self());
    }


    /**
     *  Sets the coordinate format <code>Type</code>.
     *
     *  @param type the coordinate format type 
     *  @throws org.osid.NullArgumentException <code>type</code>
     *          is <code>null</code>
     */

    public T coordinateFormat(org.osid.type.Type type) {
        getMiter().setCoordinateFormatType(type);
        return (self());
    }
}       

