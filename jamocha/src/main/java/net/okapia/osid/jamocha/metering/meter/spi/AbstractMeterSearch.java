//
// AbstractMeterSearch.java
//
//     A template for making a Meter Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.meter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing meter searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractMeterSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.metering.MeterSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.metering.records.MeterSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.metering.MeterSearchOrder meterSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of meters. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  meterIds list of meters
     *  @throws org.osid.NullArgumentException
     *          <code>meterIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongMeters(org.osid.id.IdList meterIds) {
        while (meterIds.hasNext()) {
            try {
                this.ids.add(meterIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongMeters</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of meter Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getMeterIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  meterSearchOrder meter search order 
     *  @throws org.osid.NullArgumentException
     *          <code>meterSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>meterSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderMeterResults(org.osid.metering.MeterSearchOrder meterSearchOrder) {
	this.meterSearchOrder = meterSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.metering.MeterSearchOrder getMeterSearchOrder() {
	return (this.meterSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given meter search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a meter implementing the requested record.
     *
     *  @param meterSearchRecordType a meter search record
     *         type
     *  @return the meter search record
     *  @throws org.osid.NullArgumentException
     *          <code>meterSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(meterSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.MeterSearchRecord getMeterSearchRecord(org.osid.type.Type meterSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.metering.records.MeterSearchRecord record : this.records) {
            if (record.implementsRecordType(meterSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(meterSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this meter search. 
     *
     *  @param meterSearchRecord meter search record
     *  @param meterSearchRecordType meter search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMeterSearchRecord(org.osid.metering.records.MeterSearchRecord meterSearchRecord, 
                                           org.osid.type.Type meterSearchRecordType) {

        addRecordType(meterSearchRecordType);
        this.records.add(meterSearchRecord);        
        return;
    }
}
