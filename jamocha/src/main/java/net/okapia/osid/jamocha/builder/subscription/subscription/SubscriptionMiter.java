//
// SubscriptionMiter.java
//
//     Defines a Subscription miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.subscription.subscription;


/**
 *  Defines a <code>Subscription</code> miter for use with the builders.
 */

public interface SubscriptionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.subscription.Subscription {


    /**
     *  Sets the dispatch.
     *
     *  @param dispatch a dispatch
     *  @throws org.osid.NullArgumentException <code>dispatch</code>
     *          is <code>null</code>
     */

    public void setDispatch(org.osid.subscription.Dispatch dispatch);


    /**
     *  Sets the subscriber.
     *
     *  @param subscriber a subscriber
     *  @throws org.osid.NullArgumentException <code>subscriber</code>
     *          is <code>null</code>
     */

    public void setSubscriber(org.osid.resource.Resource subscriber);


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException <code>address</code> is
     *          <code>null</code>
     */

    public void setAddress(org.osid.contact.Address address);


    /**
     *  Adds a Subscription record.
     *
     *  @param record a subscription record
     *  @param recordType the type of subscription record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addSubscriptionRecord(org.osid.subscription.records.SubscriptionRecord record, org.osid.type.Type recordType);
}       


