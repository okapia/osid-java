//
// AbstractReplySearch.java
//
//     A template for making a Reply Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.reply.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing reply searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractReplySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.forum.ReplySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.forum.records.ReplySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.forum.ReplySearchOrder replySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of replies. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  replyIds list of replies
     *  @throws org.osid.NullArgumentException
     *          <code>replyIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongReplies(org.osid.id.IdList replyIds) {
        while (replyIds.hasNext()) {
            try {
                this.ids.add(replyIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongReplies</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of reply Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getReplyIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  replySearchOrder reply search order 
     *  @throws org.osid.NullArgumentException
     *          <code>replySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>replySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderReplyResults(org.osid.forum.ReplySearchOrder replySearchOrder) {
	this.replySearchOrder = replySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.forum.ReplySearchOrder getReplySearchOrder() {
	return (this.replySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given reply search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a reply implementing the requested record.
     *
     *  @param replySearchRecordType a reply search record
     *         type
     *  @return the reply search record
     *  @throws org.osid.NullArgumentException
     *          <code>replySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(replySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ReplySearchRecord getReplySearchRecord(org.osid.type.Type replySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.forum.records.ReplySearchRecord record : this.records) {
            if (record.implementsRecordType(replySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(replySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this reply search. 
     *
     *  @param replySearchRecord reply search record
     *  @param replySearchRecordType reply search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addReplySearchRecord(org.osid.forum.records.ReplySearchRecord replySearchRecord, 
                                           org.osid.type.Type replySearchRecordType) {

        addRecordType(replySearchRecordType);
        this.records.add(replySearchRecord);        
        return;
    }
}
