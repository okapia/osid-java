//
// AbstractAdapterRenovationLookupSession.java
//
//    A Renovation lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.room.construction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Renovation lookup session adapter.
 */

public abstract class AbstractAdapterRenovationLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.room.construction.RenovationLookupSession {

    private final org.osid.room.construction.RenovationLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRenovationLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRenovationLookupSession(org.osid.room.construction.RenovationLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Campus/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Campus Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.session.getCampusId());
    }


    /**
     *  Gets the {@code Campus} associated with this session.
     *
     *  @return the {@code Campus} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCampus());
    }


    /**
     *  Tests if this user can perform {@code Renovation} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRenovations() {
        return (this.session.canLookupRenovations());
    }


    /**
     *  A complete view of the {@code Renovation} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRenovationView() {
        this.session.useComparativeRenovationView();
        return;
    }


    /**
     *  A complete view of the {@code Renovation} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRenovationView() {
        this.session.usePlenaryRenovationView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include renovations in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.session.useFederatedCampusView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.session.useIsolatedCampusView();
        return;
    }
    

    /**
     *  Only renovations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRenovationView() {
        this.session.useEffectiveRenovationView();
        return;
    }
    

    /**
     *  All renovations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRenovationView() {
        this.session.useAnyEffectiveRenovationView();
        return;
    }

     
    /**
     *  Gets the {@code Renovation} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Renovation} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Renovation} and
     *  retained for compatibility.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param renovationId {@code Id} of the {@code Renovation}
     *  @return the renovation
     *  @throws org.osid.NotFoundException {@code renovationId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code renovationId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Renovation getRenovation(org.osid.id.Id renovationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovation(renovationId));
    }


    /**
     *  Gets a {@code RenovationList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  renovations specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Renovations} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Renovation} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code renovationIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByIds(org.osid.id.IdList renovationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByIds(renovationIds));
    }


    /**
     *  Gets a {@code RenovationList} corresponding to the given
     *  renovation genus {@code Type} which does not include
     *  renovations of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned {@code Renovation} list
     *  @throws org.osid.NullArgumentException
     *          {@code renovationGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByGenusType(renovationGenusType));
    }


    /**
     *  Gets a {@code RenovationList} corresponding to the given
     *  renovation genus {@code Type} and include any additional
     *  renovations with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned {@code Renovation} list
     *  @throws org.osid.NullArgumentException
     *          {@code renovationGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByParentGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByParentGenusType(renovationGenusType));
    }


    /**
     *  Gets a {@code RenovationList} containing the given
     *  renovation record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationRecordType a renovation record type 
     *  @return the returned {@code Renovation} list
     *  @throws org.osid.NullArgumentException
     *          {@code renovationRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByRecordType(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByRecordType(renovationRecordType));
    }


    /**
     *  Gets a {@code RenovationList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible
     *  through this session.
     *  
     *  In active mode, renovations are returned that are currently
     *  active. In any status mode, active and inactive renovations
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Renovation} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsOnDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsOnDate(from, to));
    }
        

    /**
     *  Gets a list of all renovations of a genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code renovationGenusType, 
     *          from} or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeOnDate(org.osid.type.Type renovationGenusType, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByGenusTypeOnDate(renovationGenusType, from, to));
    }


    /**
     *  Gets a list of all renovations corresponding to a room {@code
     *  Id}.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId the {@code Id} of the room 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.NullArgumentException {@code roomId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForRoom(org.osid.id.Id roomId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsForRoom(roomId));
    }


    /**
     *  Gets a list of all renovations corresponding to a room and
     *  renovation genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room {@code Id} 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code roomId} or {@code 
     *          renovationGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForRoom(org.osid.id.Id roomId, 
                                                                                      org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByGenusTypeForRoom(roomId, renovationGenusType));
    }


    /**
     *  Gets a list of all renovations for a room with an effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code roomId, from} or 
     *          {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForRoomOnDate(org.osid.id.Id roomId, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsForRoomOnDate(roomId, from, to));
    }

    
    /**
     *  Gets a list of all renovations for a room and of a renovation
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room {@code Id} 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code roomId, 
     *          renovationGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForRoomOnDate(org.osid.id.Id roomId, 
                                                                                            org.osid.type.Type renovationGenusType, 
                                                                                            org.osid.calendaring.DateTime from, 
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByGenusTypeForRoomOnDate(roomId, renovationGenusType, from, to));
    }


    /**
     *  Gets a list of all renovations on a floor {@code Id}.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId the {@code Id} of the floor 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.NullArgumentException {@code floorId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForFloor(org.osid.id.Id floorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsForFloor(floorId));
    }


    /**
     *  Gets a list of all renovations on a floor and of a renovation
     *  genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor {@code Id} 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code floorId} or 
     *          {@code renovationGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForFloor(org.osid.id.Id floorId, 
                                                                                       org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByGenusTypeForFloor(floorId, renovationGenusType));
    }

    
    /**
     *  Gets a list of all renovations on a floor with an effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code floorId, from,} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForFloorOnDate(org.osid.id.Id floorId, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRenovationsForFloorOnDate(floorId, from, to));
    }

               
    /**
     *  Gets a list of all renovations on a floor and of a renovation
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor {@code Id} 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code floorId, 
     *          renovationGenusType, from,} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForFloorOnDate(org.osid.id.Id floorId, 
                                                                                             org.osid.type.Type renovationGenusType, 
                                                                                             org.osid.calendaring.DateTime from, 
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRenovationsByGenusTypeForFloorOnDate(floorId, renovationGenusType, from, to));
    }



    /**
     *  Gets a list of all renovations in a building. 
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId the {@code Id} of the building 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.NullArgumentException {@code buildingId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsForBuilding(buildingId));
    }

    
    /**
     *  Gets a list of all renovations in a building and of a
     *  renovation genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NullArgumentException {@code buildingId} or 
     *          {@code renovationGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForBuilding(org.osid.id.Id buildingId, 
                                                                                          org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovationsByGenusTypeForBuilding(buildingId, renovationGenusType));
    }


    /**
     *  Gets a list of all renovations in a building with an effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code buildingId, from} 
     *          or {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRenovationsForBuildingOnDate(buildingId, from, to));
    }


    /**
     *  Gets a list of all renovations in a building and of a
     *  renovation genus type effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *  
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building {@code Id} 
     *  @param  renovationGenusType a renovation genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RenovationList} 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code buildingId, 
     *          renovationGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId, 
                                                                                                org.osid.type.Type renovationGenusType, 
                                                                                                org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRenovationsByGenusTypeForBuildingOnDate(buildingId, renovationGenusType, from, to));
    }


    /**
     *  Gets all {@code Renovations}. 
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Renovations} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRenovations());
    }
}
