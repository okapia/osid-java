//
// AbstractQueueProcessorQuery.java
//
//     A template for making a QueueProcessor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for queue processors.
 */

public abstract class AbstractQueueProcessorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQuery
    implements org.osid.provisioning.rules.QueueProcessorQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.QueueProcessorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches automatic processors. 
     *
     *  @param  match <code> true </code> to match automatic processors, 
     *          <code> false </code> to match manual processors 
     */

    @OSID @Override
    public void matchAutomatic(boolean match) {
        return;
    }


    /**
     *  Clears the automatic query terms. 
     */

    @OSID @Override
    public void clearAutomaticTerms() {
        return;
    }


    /**
     *  Matches first-in-first-out processors. 
     *
     *  @param  match <code> true </code> to match fifo processors, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public void matchFifo(boolean match) {
        return;
    }


    /**
     *  Clears the fifo query terms. 
     */

    @OSID @Override
    public void clearFifoTerms() {
        return;
    }


    /**
     *  Matches processors that remove processed queue entries. 
     *
     *  @param  match <code> true </code> to match removing queue entry 
     *          processors, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchRemovesProcessedQueueEntries(boolean match) {
        return;
    }


    /**
     *  Clears the removes processed queue entries query terms. 
     */

    @OSID @Override
    public void clearRemovesProcessedQueueEntriesTerms() {
        return;
    }


    /**
     *  Matches mapped to the queue. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledQueueId(org.osid.id.Id queueId, boolean match) {
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledQueueIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledQueueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuery getRuledQueueQuery() {
        throw new org.osid.UnimplementedException("supportsRuledQueueQuery() is false");
    }


    /**
     *  Matches mapped to any queue. 
     *
     *  @param  match <code> true </code> for mapped to any queue, <code> 
     *          false </code> to match mapped to no queue 
     */

    @OSID @Override
    public void matchAnyRuledQueue(boolean match) {
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearRuledQueueTerms() {
        return;
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given queue processor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue processor implementing the requested record.
     *
     *  @param queueProcessorRecordType a queue processor record type
     *  @return the queue processor query record
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueProcessorQueryRecord getQueueProcessorQueryRecord(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueProcessorQueryRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue processor query. 
     *
     *  @param queueProcessorQueryRecord queue processor query record
     *  @param queueProcessorRecordType queueProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueProcessorQueryRecord(org.osid.provisioning.rules.records.QueueProcessorQueryRecord queueProcessorQueryRecord, 
                                          org.osid.type.Type queueProcessorRecordType) {

        addRecordType(queueProcessorRecordType);
        nullarg(queueProcessorQueryRecord, "queue processor query record");
        this.records.add(queueProcessorQueryRecord);        
        return;
    }
}
