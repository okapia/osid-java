//
// AbstractIndexedMapTodoLookupSession.java
//
//    A simple framework for providing a Todo lookup service
//    backed by a fixed collection of todos with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Todo lookup service backed by a
 *  fixed collection of todos. The todos are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some todos may be compatible
 *  with more types than are indicated through these todo
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Todos</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapTodoLookupSession
    extends AbstractMapTodoLookupSession
    implements org.osid.checklist.TodoLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.checklist.Todo> todosByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.checklist.Todo>());
    private final MultiMap<org.osid.type.Type, org.osid.checklist.Todo> todosByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.checklist.Todo>());


    /**
     *  Makes a <code>Todo</code> available in this session.
     *
     *  @param  todo a todo
     *  @throws org.osid.NullArgumentException <code>todo<code> is
     *          <code>null</code>
     */

    @Override
    protected void putTodo(org.osid.checklist.Todo todo) {
        super.putTodo(todo);

        this.todosByGenus.put(todo.getGenusType(), todo);
        
        try (org.osid.type.TypeList types = todo.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.todosByRecord.put(types.getNextType(), todo);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a todo from this session.
     *
     *  @param todoId the <code>Id</code> of the todo
     *  @throws org.osid.NullArgumentException <code>todoId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeTodo(org.osid.id.Id todoId) {
        org.osid.checklist.Todo todo;
        try {
            todo = getTodo(todoId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.todosByGenus.remove(todo.getGenusType());

        try (org.osid.type.TypeList types = todo.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.todosByRecord.remove(types.getNextType(), todo);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeTodo(todoId);
        return;
    }


    /**
     *  Gets a <code>TodoList</code> corresponding to the given
     *  todo genus <code>Type</code> which does not include
     *  todos of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known todos or an error results. Otherwise,
     *  the returned list may contain only those todos that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  todoGenusType a todo genus type 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByGenusType(org.osid.type.Type todoGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.todo.ArrayTodoList(this.todosByGenus.get(todoGenusType)));
    }


    /**
     *  Gets a <code>TodoList</code> containing the given
     *  todo record <code>Type</code>. In plenary mode, the
     *  returned list contains all known todos or an error
     *  results. Otherwise, the returned list may contain only those
     *  todos that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  todoRecordType a todo record type 
     *  @return the returned <code>todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByRecordType(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.todo.ArrayTodoList(this.todosByRecord.get(todoRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.todosByGenus.clear();
        this.todosByRecord.clear();

        super.close();

        return;
    }
}
