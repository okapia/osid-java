//
// TodoNodeFilterList.java
//
//     Implements a filtering TodoNodeList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.checklist.todonode;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering TodoNodeList.
 */

public final class TodoNodeFilterList
    extends net.okapia.osid.jamocha.inline.filter.checklist.todonode.spi.AbstractTodoNodeFilterList
    implements org.osid.checklist.TodoNodeList,
               TodoNodeFilter {

    private final TodoNodeFilter filter;


    /**
     *  Creates a new <code>TodoNodeFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>TodoNodeList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public TodoNodeFilterList(TodoNodeFilter filter, org.osid.checklist.TodoNodeList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters TodoNodes.
     *
     *  @param todoNode the todo node to filter
     *  @return <code>true</code> if the todo node passes the filter,
     *          <code>false</code> if the todo node should be filtered
     */

    @Override
    public boolean pass(org.osid.checklist.TodoNode todoNode) {
        return (this.filter.pass(todoNode));
    }
}
