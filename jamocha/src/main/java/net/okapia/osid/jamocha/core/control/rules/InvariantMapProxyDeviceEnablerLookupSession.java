//
// InvariantMapProxyDeviceEnablerLookupSession
//
//    Implements a DeviceEnabler lookup service backed by a fixed
//    collection of deviceEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements a DeviceEnabler lookup service backed by a fixed
 *  collection of device enablers. The device enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyDeviceEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractMapDeviceEnablerLookupSession
    implements org.osid.control.rules.DeviceEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDeviceEnablerLookupSession} with no
     *  device enablers.
     *
     *  @param system the system
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyDeviceEnablerLookupSession(org.osid.control.System system,
                                                  org.osid.proxy.Proxy proxy) {
        setSystem(system);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyDeviceEnablerLookupSession} with a single
     *  device enabler.
     *
     *  @param system the system
     *  @param deviceEnabler a single device enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code deviceEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDeviceEnablerLookupSession(org.osid.control.System system,
                                                  org.osid.control.rules.DeviceEnabler deviceEnabler, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putDeviceEnabler(deviceEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyDeviceEnablerLookupSession} using
     *  an array of device enablers.
     *
     *  @param system the system
     *  @param deviceEnablers an array of device enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code deviceEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDeviceEnablerLookupSession(org.osid.control.System system,
                                                  org.osid.control.rules.DeviceEnabler[] deviceEnablers, org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putDeviceEnablers(deviceEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyDeviceEnablerLookupSession} using a
     *  collection of device enablers.
     *
     *  @param system the system
     *  @param deviceEnablers a collection of device enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code system},
     *          {@code deviceEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyDeviceEnablerLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.rules.DeviceEnabler> deviceEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(system, proxy);
        putDeviceEnablers(deviceEnablers);
        return;
    }
}
