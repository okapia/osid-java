//
// AbstractProfileQueryInspector.java
//
//     A template for making a ProfileQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for profiles.
 */

public abstract class AbstractProfileQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.profile.ProfileQueryInspector {

    private final java.util.Collection<org.osid.profile.records.ProfileQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the profile item <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the profile item query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQueryInspector[] getProfileItemTerms() {
        return (new org.osid.profile.ProfileItemQueryInspector[0]);
    }


    /**
     *  Gets the profile entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the profile entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQueryInspector[] getProfileEntryTerms() {
        return (new org.osid.profile.ProfileEntryQueryInspector[0]);
    }


    /**
     *  Gets the ancestor profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorProfileIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getAncestorProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }


    /**
     *  Gets the descendant profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantProfileIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getDescendantProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given profile query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a profile implementing the requested record.
     *
     *  @param profileRecordType a profile record type
     *  @return the profile query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>profileRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileQueryInspectorRecord getProfileQueryInspectorRecord(org.osid.type.Type profileRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(profileRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile query. 
     *
     *  @param profileQueryInspectorRecord profile query inspector
     *         record
     *  @param profileRecordType profile record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileQueryInspectorRecord(org.osid.profile.records.ProfileQueryInspectorRecord profileQueryInspectorRecord, 
                                                   org.osid.type.Type profileRecordType) {

        addRecordType(profileRecordType);
        nullarg(profileRecordType, "profile record type");
        this.records.add(profileQueryInspectorRecord);        
        return;
    }
}
