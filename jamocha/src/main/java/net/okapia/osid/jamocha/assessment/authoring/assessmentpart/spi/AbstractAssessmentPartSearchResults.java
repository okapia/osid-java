//
// AbstractAssessmentPartSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.assessmentpart.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAssessmentPartSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.assessment.authoring.AssessmentPartSearchResults {

    private org.osid.assessment.authoring.AssessmentPartList assessmentParts;
    private final org.osid.assessment.authoring.AssessmentPartQueryInspector inspector;
    private final java.util.Collection<org.osid.assessment.authoring.records.AssessmentPartSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAssessmentPartSearchResults.
     *
     *  @param assessmentParts the result set
     *  @param assessmentPartQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>assessmentParts</code>
     *          or <code>assessmentPartQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAssessmentPartSearchResults(org.osid.assessment.authoring.AssessmentPartList assessmentParts,
                                            org.osid.assessment.authoring.AssessmentPartQueryInspector assessmentPartQueryInspector) {
        nullarg(assessmentParts, "assessment parts");
        nullarg(assessmentPartQueryInspector, "assessment part query inspectpr");

        this.assessmentParts = assessmentParts;
        this.inspector = assessmentPartQueryInspector;

        return;
    }


    /**
     *  Gets the assessment part list resulting from a search.
     *
     *  @return an assessment part list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentParts() {
        if (this.assessmentParts == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.assessment.authoring.AssessmentPartList assessmentParts = this.assessmentParts;
        this.assessmentParts = null;
	return (assessmentParts);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.assessment.authoring.AssessmentPartQueryInspector getAssessmentPartQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  assessment part search record <code> Type. </code> This method must
     *  be used to retrieve an assessmentPart implementing the requested
     *  record.
     *
     *  @param assessmentPartSearchRecordType an assessmentPart search 
     *         record type 
     *  @return the assessment part search
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(assessmentPartSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.authoring.records.AssessmentPartSearchResultsRecord getAssessmentPartSearchResultsRecord(org.osid.type.Type assessmentPartSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.assessment.authoring.records.AssessmentPartSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(assessmentPartSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(assessmentPartSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record assessment part search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAssessmentPartRecord(org.osid.assessment.authoring.records.AssessmentPartSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "assessment part record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
