//
// AbstractImmutableSite.java
//
//     Wraps a mutable Site to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.site.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Site</code> to hide modifiers. This
 *  wrapper provides an immutized Site from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying site whose state changes are visible.
 */

public abstract class AbstractImmutableSite
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.installation.Site {

    private final org.osid.installation.Site site;


    /**
     *  Constructs a new <code>AbstractImmutableSite</code>.
     *
     *  @param site the site to immutablize
     *  @throws org.osid.NullArgumentException <code>site</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSite(org.osid.installation.Site site) {
        super(site);
        this.site = site;
        return;
    }


    /**
     *  Gets the path to this site. 
     *
     *  @return the path 
     */

    @OSID @Override
    public String getPath() {
        return (this.site.getPath());
    }


    /**
     *  Gets the site record corresponding to the given <code> Site </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> siteRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(siteRecordType) </code> is <code> true </code> . 
     *
     *  @param  siteRecordType the type of the record to retrieve 
     *  @return the site record 
     *  @throws org.osid.NullArgumentException <code> siteRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(siteRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.records.SiteRecord getSiteRecord(org.osid.type.Type siteRecordType)
        throws org.osid.OperationFailedException {

        return (this.site.getSiteRecord(siteRecordType));
    }
}

