//
// AbstractComment.java
//
//     Defines a Comment builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.commenting.comment.spi;


/**
 *  Defines a <code>Comment</code> builder.
 */

public abstract class AbstractCommentBuilder<T extends AbstractCommentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.commenting.comment.CommentMiter comment;


    /**
     *  Constructs a new <code>AbstractCommentBuilder</code>.
     *
     *  @param comment the comment to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCommentBuilder(net.okapia.osid.jamocha.builder.commenting.comment.CommentMiter comment) {
        super(comment);
        this.comment = comment;
        return;
    }


    /**
     *  Builds the comment.
     *
     *  @return the new comment
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.commenting.Comment build() {
        (new net.okapia.osid.jamocha.builder.validator.commenting.comment.CommentValidator(getValidations())).validate(this.comment);
        return (new net.okapia.osid.jamocha.builder.commenting.comment.ImmutableComment(this.comment));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the comment miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.commenting.comment.CommentMiter getMiter() {
        return (this.comment);
    }


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    public T referenceId(org.osid.id.Id referenceId) {
        getMiter().setReferenceId(referenceId);
        return (self());
    }


    /**
     *  Sets the commentor.
     *
     *  @param commentor a commentor
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>commentor</code>
     *          is <code>null</code>
     */

    public T commentor(org.osid.resource.Resource commentor) {
        getMiter().setCommentor(commentor);
        return (self());
    }


    /**
     *  Sets the commenting agent.
     *
     *  @param agent a commenting agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T commentingAgent(org.osid.authentication.Agent agent) {
        getMiter().setCommentingAgent(agent);
        return (self());
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    public T text(org.osid.locale.DisplayText text) {
        getMiter().setText(text);
        return (self());
    }


    /**
     *  Sets the rating.
     *
     *  @param rating a rating
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>rating</code> is
     *          <code>null</code>
     */

    public T rating(org.osid.grading.Grade rating) {
        getMiter().setRating(rating);
        return (self());
    }


    /**
     *  Adds a Comment record.
     *
     *  @param record a comment record
     *  @param recordType the type of comment record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.commenting.records.CommentRecord record, org.osid.type.Type recordType) {
        getMiter().addCommentRecord(record, recordType);
        return (self());
    }
}       


