//
// AbstractGradebookColumnQueryInspector.java
//
//     A template for making a GradebookColumnQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumn.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for gradebook columns.
 */

public abstract class AbstractGradebookColumnQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.grading.GradebookColumnQueryInspector {

    private final java.util.Collection<org.osid.grading.records.GradebookColumnQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the grade system <code> Id </code> terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradeSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the grade entry <code> Id </code> terms. 
     *
     *  @return the grade entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade entry terms. 
     *
     *  @return the grade entry terms 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQueryInspector[] getGradeEntryTerms() {
        return (new org.osid.grading.GradeEntryQueryInspector[0]);
    }


    /**
     *  Gets the gradebook column summary terms. 
     *
     *  @return the gradebook column summary terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSummaryQueryInspector[] getGradebookColumnSummaryTerms() {
        return (new org.osid.grading.GradebookColumnSummaryQueryInspector[0]);
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given gradebook column query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a gradebook column implementing the requested record.
     *
     *  @param gradebookColumnRecordType a gradebook column record type
     *  @return the gradebook column query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnQueryInspectorRecord getGradebookColumnQueryInspectorRecord(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookColumnQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook column query. 
     *
     *  @param gradebookColumnQueryInspectorRecord gradebook column query inspector
     *         record
     *  @param gradebookColumnRecordType gradebookColumn record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookColumnQueryInspectorRecord(org.osid.grading.records.GradebookColumnQueryInspectorRecord gradebookColumnQueryInspectorRecord, 
                                                   org.osid.type.Type gradebookColumnRecordType) {

        addRecordType(gradebookColumnRecordType);
        nullarg(gradebookColumnRecordType, "gradebook column record type");
        this.records.add(gradebookColumnQueryInspectorRecord);        
        return;
    }
}
