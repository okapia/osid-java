//
// AbstractOsidRuleForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic OsidRuleForm.
 */

public abstract class AbstractOsidRuleForm
    extends AbstractOperableOsidObjectForm
    implements org.osid.OsidRuleForm {
    
    private org.osid.id.Id rule;
    private boolean ruleCleared = false;
    private org.osid.Metadata ruleMetadata = getDefaultIdMetadata(getRuleId(), "rule");    


    /** 
     *  Constructs a new {@code AbstractOsidRuleForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidRuleForm(org.osid.locale.Locale locale, boolean update) {
        super(locale, update);
        return;
    }

    
    /**
     *  Gets the metadata for an associated rule. 
     *
     *  @return metadata for the rule 
     */

    @OSID @Override
    public org.osid.Metadata getRuleMetadata() {
        return (this.ruleMetadata);
    }


    /**
     *  Sets the rule metadata.
     *
     *  @param metadata the rule metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setRuleMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "rule metadata");
        this.ruleMetadata = metadata;
        return;
    }


    /**
     *  Gets the Id for the rule Id field.
     *
     *  @return the rule field Id
     */

    protected org.osid.id.Id getRuleId() {
        return (OsidRuleElements.getRuleId());
    }


    /**
     *  Sets a rule. 
     *
     *  @param  ruleId the new rule 
     *  @throws org.osid.InvalidArgumentException <code> ruleId </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadOnly() 
     *          </code> is <code> true </code> 
     *  @throws org.osid.NullArgumentException <code> ruleId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void setRule(org.osid.id.Id ruleId) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(ruleId, getRuleMetadata());

        this.rule = ruleId;
        this.ruleCleared = false;

        return;
    }


    /**
     *  Removes the rule. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> is <code> true </code> or <code> Metadata.isReadOnly() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public void clearRule() {
        if (getRuleMetadata().isRequired()) {
            throw new org.osid.NoAccessException("rule is required");
        }

        this.rule = null;
        this.ruleCleared = true;

        return;
    }


    /**
     *  Tests if the rule has been set in this form.
     *
     *  @return {@code true} if the rule has been set, {@code false}
     *          otherwise
     */

    protected boolean isRuleSet() {
        return (this.rule != null);
    }


    /**
     *  Tests if the rule has been cleared in this form.
     *
     *  @return {@code true} if the rule has been cleared, {@code
     *          false} otherwise
     */

    protected boolean isRuleCleared() {
        return (this.ruleCleared);
    }


    /**
     *  Returns the current rule value.
     *
     *  @return the rule value or {@code null} if {@code
     *          isRuleSet()} is {@code false}
     */

    protected org.osid.id.Id getRule() {
        return (this.rule);
    }    
}
