//
// AbstractFederatingCourseOfferingLookupSession.java
//
//     An abstract federating adapter for a CourseOfferingLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CourseOfferingLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCourseOfferingLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.CourseOfferingLookupSession>
    implements org.osid.course.CourseOfferingLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingCourseOfferingLookupSession</code>.
     */

    protected AbstractFederatingCourseOfferingLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.CourseOfferingLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>CourseOffering</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourseOfferings() {
        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            if (session.canLookupCourseOfferings()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>CourseOffering</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseOfferingView() {
        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            session.useComparativeCourseOfferingView();
        }

        return;
    }


    /**
     *  A complete view of the <code>CourseOffering</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseOfferingView() {
        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            session.usePlenaryCourseOfferingView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include course offerings in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }

     
    /**
     *  Only course offerings whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveCourseOfferingView() {
        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            session.useEffectiveCourseOfferingView();
        }

        return;
    }


    /**
     *  All course offerings of any effective dates are returned by
     *  all methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCourseOfferingView() {
        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            session.useAnyEffectiveCourseOfferingView();
        }

        return;
    }


    /**
     *  Gets the <code>CourseOffering</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseOffering</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CourseOffering</code> and retained for compatibility.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingId <code>Id</code> of the
     *          <code>CourseOffering</code>
     *  @return the course offering
     *  @throws org.osid.NotFoundException <code>courseOfferingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            try {
                return (session.getCourseOffering(courseOfferingId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(courseOfferingId + " not found");
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courseOfferings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CourseOfferings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByIds(org.osid.id.IdList courseOfferingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.courseoffering.MutableCourseOfferingList ret = new net.okapia.osid.jamocha.course.courseoffering.MutableCourseOfferingList();

        try (org.osid.id.IdList ids = courseOfferingIds) {
            while (ids.hasNext()) {
                ret.addCourseOffering(getCourseOffering(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the given
     *  course offering genus <code>Type</code> which does not include
     *  course offerings of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  course offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingGenusType a courseOffering genus type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByGenusType(org.osid.type.Type courseOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsByGenusType(courseOfferingGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseOfferingList</code> corresponding to the given
     *  course offering genus <code>Type</code> and include any additional
     *  course offerings with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  course offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingGenusType a courseOffering genus type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByParentGenusType(org.osid.type.Type courseOfferingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsByParentGenusType(courseOfferingGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseOfferingList</code> containing the given
     *  course offering record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseOfferingRecordType a courseOffering record type 
     *  @return the returned <code>CourseOffering</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByRecordType(org.osid.type.Type courseOfferingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsByRecordType(courseOfferingRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseOfferingList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsOnDate(org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code> CourseOfferings </code> associated with a
     *  given <code> Course. </code> 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseId a course <code> Id </code>
     *  @return a list of <code> CourseOfferings </code>
     *  @throws org.osid.NullArgumentException <code> courseId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsForCourse(courseId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseOfferingList</code> for a course and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In active mode, course offerings are returned that are
     *  currently active. In any status mode, active and inactive
     *  course offerings are returned.
     *
     *  @param courseId a course <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseOnDate(org.osid.id.Id courseId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsForCourseOnDate(courseId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code> CourseOfferings </code> associated with a
     *  given <code> Term. </code> In plenary mode, the returned list
     *  contains all known course offerings or an error
     *  results. Otherwise, the returned list may contain only those
     *  course offerings that are accessible through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code>
     *  @return a list of <code> CourseOfferings </code>
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code>
     *          null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsForTerm(termId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseOfferingList</code> for a term and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In active mode, course offerings are returned that are
     *  currently active. In any status mode, active and inactive
     *  course offerings are returned.
     *
     *  @param termId a term <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>termId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForTermOnDate(org.osid.id.Id termId, 
                                                                                org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsForTermOnDate(termId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code> CourseOfferings </code> associated with a
     *  given <code>Course</code> and <code>Term</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results.  Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  courseId a course <code> Id </code>
     *  @param  termId a term <code> Id </code>
     *  @return a list of <code> CourseOfferings </code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          or <code>termId</code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseAndTerm(org.osid.id.Id courseId,
                                                                                 org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsForCourseAndTerm(courseId, termId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CourseOfferingList</code> for a course, term, and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *  
     *  In active mode, course offerings are returned that are
     *  currently active. In any status mode, active and inactive
     *  course offerings are returned.
     *
     *  @param courseId a course <code>Id</code>
     *  @param termId a term <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseOffering</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>,
     *          <code>termId</code> <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsForCourseAndTermOnDate(org.osid.id.Id courseId, 
                                                                                       org.osid.id.Id termId, 
                                                                                       org.osid.calendaring.DateTime from, 
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsForCourseAndTermOnDate(courseId, termId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code> CourseOfferings ny number and </code>
     *  associated with a given <code> Term. </code> 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  number a course offering number 
     *  @return a list of <code> CourseOfferings </code> 
     *  @throws org.osid.NullArgumentException <code> termId </code> or <code> 
     *          number </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferingsByNumberForTerm(org.osid.id.Id termId, 
                                                                                String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferingsByNumberForTerm(termId, number));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets all <code>CourseOfferings</code>. 
     *
     *  In plenary mode, the returned list contains all known course
     *  offerings or an error results. Otherwise, the returned list
     *  may contain only those course offerings that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, course offerings are returned that are
     *  currently effective.  In any effective mode, effective course
     *  offerings and those currently expired are returned.
     *
     *  @return a list of <code>CourseOfferings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList ret = getCourseOfferingList();

        for (org.osid.course.CourseOfferingLookupSession session : getSessions()) {
            ret.addCourseOfferingList(session.getCourseOfferings());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.courseoffering.FederatingCourseOfferingList getCourseOfferingList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.courseoffering.ParallelCourseOfferingList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.courseoffering.CompositeCourseOfferingList());
        }
    }
}
