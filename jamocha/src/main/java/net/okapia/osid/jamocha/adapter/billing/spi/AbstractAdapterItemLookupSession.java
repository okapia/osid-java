//
// AbstractAdapterItemLookupSession.java
//
//    An Item lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Item lookup session adapter.
 */

public abstract class AbstractAdapterItemLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.billing.ItemLookupSession {

    private final org.osid.billing.ItemLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterItemLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterItemLookupSession(org.osid.billing.ItemLookupSession session) {
        super(session);
        this.session = session;
        return;
    }

    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code Item} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupItems() {
        return (this.session.canLookupItems());
    }


    /**
     *  A complete view of the {@code Item} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeItemView() {
        this.session.useComparativeItemView();
        return;
    }


    /**
     *  A complete view of the {@code Item} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryItemView() {
        this.session.usePlenaryItemView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include items in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the {@code Item} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Item} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Item} and
     *  retained for compatibility.
     *
     *  @param itemId {@code Id} of the {@code Item}
     *  @return the item
     *  @throws org.osid.NotFoundException {@code itemId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code itemId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItem(itemId));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  items specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Items} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *
     *  @param  itemIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code itemIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByIds(org.osid.id.IdList itemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByIds(itemIds));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  item genus {@code Type} which does not include
     *  items of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByGenusType(itemGenusType));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  item genus {@code Type} and include any additional
     *  items with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByParentGenusType(itemGenusType));
    }


    /**
     *  Gets an {@code ItemList} containing the given
     *  item record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByRecordType(itemRecordType));
    }


    /**
     *  Gets an {@code ItemList} of the given category. In plenary
     *  mode, the returned list contains all known items or an error
     *  results.  Otherwise, the returned list may contain only those
     *  items that are accessible through this session.
     *
     *  @param  categoryId an item category {@code Id} 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code categoryId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByCategory(org.osid.id.Id categoryId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByCategory(categoryId));
    }


    /**
     *  Gets an {@code ItemList} of the given general ledger account.
     *  In plenary mode, the returned list contains all known items or
     *  an error results. Otherwise, the returned list may contain
     *  only those items that are accessible through this session.
     *
     *  @param  accountId a category {@code Id} 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code accountId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByAccount(org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByAccount(accountId));
    }


    /**
     *  Gets an {@code ItemList} of the given product. In plenary
     *  mode, the returned list contains all known items or an error
     *  results. Otherwise, the returned list may contain only those
     *  items that are accessible through this session.
     *
     *  @param  productId a product {@code Id} 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code productId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItemsByProduct(org.osid.id.Id productId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByProduct(productId));
    }


    /**
     *  Gets all {@code Items}. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Items} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItems());
    }
}
