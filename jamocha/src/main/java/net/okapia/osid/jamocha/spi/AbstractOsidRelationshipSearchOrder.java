//
// AbstractOsidRelationshipSearchOrder.java
//
//     Defines a simple OSID search order to draw from.
//
//
// Tom Coppeto
// Okapia
// 14 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a simple OsidSearchOrder to extend. This class
 *  does nothing.
 */

public abstract class AbstractOsidRelationshipSearchOrder
    extends AbstractTemporalOsidObjectSearchOrder
    implements org.osid.OsidRelationshipSearchOrder {
    
    
    /**
     *  Specifies a preference for ordering the results by the end reason 
     *  state. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByEndReason(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a {@code StateSearchOrder} is available. 
     *
     *  @return {@code true} if a state search order is available, 
     *          {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsEndReasonSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a state. 
     *
     *  @return the state search order 
     *  @throws org.osid.UnimplementedException {@code
     *          supportsEndReasonSearchOrder()} is {@code false}
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getEndReasonSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEndReason() is false");
    }
}