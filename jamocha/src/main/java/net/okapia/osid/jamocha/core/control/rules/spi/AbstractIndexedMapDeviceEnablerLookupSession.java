//
// AbstractIndexedMapDeviceEnablerLookupSession.java
//
//    A simple framework for providing a DeviceEnabler lookup service
//    backed by a fixed collection of device enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a DeviceEnabler lookup service backed by a
 *  fixed collection of device enablers. The device enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some device enablers may be compatible
 *  with more types than are indicated through these device enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>DeviceEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDeviceEnablerLookupSession
    extends AbstractMapDeviceEnablerLookupSession
    implements org.osid.control.rules.DeviceEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.rules.DeviceEnabler> deviceEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.rules.DeviceEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.control.rules.DeviceEnabler> deviceEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.rules.DeviceEnabler>());


    /**
     *  Makes a <code>DeviceEnabler</code> available in this session.
     *
     *  @param  deviceEnabler a device enabler
     *  @throws org.osid.NullArgumentException <code>deviceEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDeviceEnabler(org.osid.control.rules.DeviceEnabler deviceEnabler) {
        super.putDeviceEnabler(deviceEnabler);

        this.deviceEnablersByGenus.put(deviceEnabler.getGenusType(), deviceEnabler);
        
        try (org.osid.type.TypeList types = deviceEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.deviceEnablersByRecord.put(types.getNextType(), deviceEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of device enablers available in this session.
     *
     *  @param  deviceEnablers an array of device enablers
     *  @throws org.osid.NullArgumentException <code>deviceEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putDeviceEnablers(org.osid.control.rules.DeviceEnabler[] deviceEnablers) {
        for (org.osid.control.rules.DeviceEnabler deviceEnabler : deviceEnablers) {
            putDeviceEnabler(deviceEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of device enablers available in this session.
     *
     *  @param  deviceEnablers a collection of device enablers
     *  @throws org.osid.NullArgumentException <code>deviceEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putDeviceEnablers(java.util.Collection<? extends org.osid.control.rules.DeviceEnabler> deviceEnablers) {
        for (org.osid.control.rules.DeviceEnabler deviceEnabler : deviceEnablers) {
            putDeviceEnabler(deviceEnabler);
        }

        return;
    }


    /**
     *  Removes a device enabler from this session.
     *
     *  @param deviceEnablerId the <code>Id</code> of the device enabler
     *  @throws org.osid.NullArgumentException <code>deviceEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDeviceEnabler(org.osid.id.Id deviceEnablerId) {
        org.osid.control.rules.DeviceEnabler deviceEnabler;
        try {
            deviceEnabler = getDeviceEnabler(deviceEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.deviceEnablersByGenus.remove(deviceEnabler.getGenusType());

        try (org.osid.type.TypeList types = deviceEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.deviceEnablersByRecord.remove(types.getNextType(), deviceEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDeviceEnabler(deviceEnablerId);
        return;
    }


    /**
     *  Gets a <code>DeviceEnablerList</code> corresponding to the given
     *  device enabler genus <code>Type</code> which does not include
     *  device enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known device enablers or an error results. Otherwise,
     *  the returned list may contain only those device enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  deviceEnablerGenusType a device enabler genus type 
     *  @return the returned <code>DeviceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablersByGenusType(org.osid.type.Type deviceEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.deviceenabler.ArrayDeviceEnablerList(this.deviceEnablersByGenus.get(deviceEnablerGenusType)));
    }


    /**
     *  Gets a <code>DeviceEnablerList</code> containing the given
     *  device enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known device enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  device enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  deviceEnablerRecordType a device enabler record type 
     *  @return the returned <code>deviceEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablersByRecordType(org.osid.type.Type deviceEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.deviceenabler.ArrayDeviceEnablerList(this.deviceEnablersByRecord.get(deviceEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.deviceEnablersByGenus.clear();
        this.deviceEnablersByRecord.clear();

        super.close();

        return;
    }
}
