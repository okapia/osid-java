//
// AbstractInputEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.inputenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractInputEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.rules.InputEnablerSearchResults {

    private org.osid.control.rules.InputEnablerList inputEnablers;
    private final org.osid.control.rules.InputEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.control.rules.records.InputEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractInputEnablerSearchResults.
     *
     *  @param inputEnablers the result set
     *  @param inputEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>inputEnablers</code>
     *          or <code>inputEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractInputEnablerSearchResults(org.osid.control.rules.InputEnablerList inputEnablers,
                                            org.osid.control.rules.InputEnablerQueryInspector inputEnablerQueryInspector) {
        nullarg(inputEnablers, "input enablers");
        nullarg(inputEnablerQueryInspector, "input enabler query inspectpr");

        this.inputEnablers = inputEnablers;
        this.inspector = inputEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the input enabler list resulting from a search.
     *
     *  @return an input enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerList getInputEnablers() {
        if (this.inputEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.rules.InputEnablerList inputEnablers = this.inputEnablers;
        this.inputEnablers = null;
	return (inputEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.rules.InputEnablerQueryInspector getInputEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  input enabler search record <code> Type. </code> This method must
     *  be used to retrieve an inputEnabler implementing the requested
     *  record.
     *
     *  @param inputEnablerSearchRecordType an inputEnabler search 
     *         record type 
     *  @return the input enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>inputEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(inputEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.InputEnablerSearchResultsRecord getInputEnablerSearchResultsRecord(org.osid.type.Type inputEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.rules.records.InputEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(inputEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(inputEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record input enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addInputEnablerRecord(org.osid.control.rules.records.InputEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "input enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
