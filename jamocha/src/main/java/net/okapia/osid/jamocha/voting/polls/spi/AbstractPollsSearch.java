//
// AbstractPollsSearch.java
//
//     A template for making a Polls Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.polls.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing polls searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPollsSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.voting.PollsSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.voting.records.PollsSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.voting.PollsSearchOrder pollsSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of pollses. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  pollsIds list of pollses
     *  @throws org.osid.NullArgumentException
     *          <code>pollsIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPolls(org.osid.id.IdList pollsIds) {
        while (pollsIds.hasNext()) {
            try {
                this.ids.add(pollsIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPolls</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of polls Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPollsIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  pollsSearchOrder polls search order 
     *  @throws org.osid.NullArgumentException
     *          <code>pollsSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>pollsSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPollsResults(org.osid.voting.PollsSearchOrder pollsSearchOrder) {
	this.pollsSearchOrder = pollsSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.voting.PollsSearchOrder getPollsSearchOrder() {
	return (this.pollsSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given polls search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a polls implementing the requested record.
     *
     *  @param pollsSearchRecordType a polls search record
     *         type
     *  @return the polls search record
     *  @throws org.osid.NullArgumentException
     *          <code>pollsSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pollsSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.PollsSearchRecord getPollsSearchRecord(org.osid.type.Type pollsSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.voting.records.PollsSearchRecord record : this.records) {
            if (record.implementsRecordType(pollsSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pollsSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this polls search. 
     *
     *  @param pollsSearchRecord polls search record
     *  @param pollsSearchRecordType polls search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPollsSearchRecord(org.osid.voting.records.PollsSearchRecord pollsSearchRecord, 
                                           org.osid.type.Type pollsSearchRecordType) {

        addRecordType(pollsSearchRecordType);
        this.records.add(pollsSearchRecord);        
        return;
    }
}
