//
// UnknownCanonicalUnitEnabler.java
//
//     Defines an unknown CanonicalUnitEnabler.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.offering.rules.canonicalunitenabler;


/**
 *  Defines an unknown <code>CanonicalUnitEnabler</code>.
 */

public final class UnknownCanonicalUnitEnabler
    extends net.okapia.osid.jamocha.nil.offering.rules.canonicalunitenabler.spi.AbstractUnknownCanonicalUnitEnabler
    implements org.osid.offering.rules.CanonicalUnitEnabler {


    /**
     *  Constructs a new Unknown<code>CanonicalUnitEnabler</code>.
     */

    public UnknownCanonicalUnitEnabler() {
        return;
    }


    /**
     *  Constructs a new Unknown<code>CanonicalUnitEnabler</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownCanonicalUnitEnabler(boolean optional) {
        super(optional);
        addCanonicalUnitEnablerRecord(new CanonicalUnitEnablerRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown CanonicalUnitEnabler.
     *
     *  @return an unknown CanonicalUnitEnabler
     */

    public static org.osid.offering.rules.CanonicalUnitEnabler create() {
        return (net.okapia.osid.jamocha.builder.validator.offering.rules.canonicalunitenabler.CanonicalUnitEnablerValidator.validateCanonicalUnitEnabler(new UnknownCanonicalUnitEnabler()));
    }


    public class CanonicalUnitEnablerRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.offering.rules.records.CanonicalUnitEnablerRecord {

        
        protected CanonicalUnitEnablerRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
