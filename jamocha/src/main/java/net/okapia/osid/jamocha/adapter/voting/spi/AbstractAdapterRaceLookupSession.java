//
// AbstractAdapterRaceLookupSession.java
//
//    A Race lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Race lookup session adapter.
 */

public abstract class AbstractAdapterRaceLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.RaceLookupSession {

    private final org.osid.voting.RaceLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRaceLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRaceLookupSession(org.osid.voting.RaceLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls} {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code Race} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRaces() {
        return (this.session.canLookupRaces());
    }


    /**
     *  A complete view of the {@code Race} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceView() {
        this.session.useComparativeRaceView();
        return;
    }


    /**
     *  A complete view of the {@code Race} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceView() {
        this.session.usePlenaryRaceView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include races in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active races are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRaceView() {
        this.session.useActiveRaceView();
        return;
    }


    /**
     *  Active and inactive races are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceView() {
        this.session.useAnyStatusRaceView();
        return;
    }
    
     
    /**
     *  Gets the {@code Race} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Race} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Race} and
     *  retained for compatibility.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param raceId {@code Id} of the {@code Race}
     *  @return the race
     *  @throws org.osid.NotFoundException {@code raceId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code raceId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Race getRace(org.osid.id.Id raceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRace(raceId));
    }


    /**
     *  Gets a {@code RaceList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  races specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Races} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Race} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code raceIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByIds(org.osid.id.IdList raceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRacesByIds(raceIds));
    }


    /**
     *  Gets a {@code RaceList} corresponding to the given
     *  race genus {@code Type} which does not include
     *  races of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceGenusType a race genus type 
     *  @return the returned {@code Race} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByGenusType(org.osid.type.Type raceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRacesByGenusType(raceGenusType));
    }


    /**
     *  Gets a {@code RaceList} corresponding to the given
     *  race genus {@code Type} and include any additional
     *  races with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceGenusType a race genus type 
     *  @return the returned {@code Race} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByParentGenusType(org.osid.type.Type raceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRacesByParentGenusType(raceGenusType));
    }


    /**
     *  Gets a {@code RaceList} containing the given
     *  race record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  raceRecordType a race record type 
     *  @return the returned {@code Race} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByRecordType(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRacesByRecordType(raceRecordType));
    }


    /**
     *  Gets a {@code RaceList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Race} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRacesByProvider(resourceId));
    }


    /**
     *  Gets a {@code RaceList} for the given {@code Ballot}.
     *  
     *  In plenary mode, the returned list contains all known races or
     *  an error results. Otherwise, the returned list may contain
     *  only those races that are accessible through this session.
     *  
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races are
     *  returned.
     *
     *  @param  ballotId a ballot {@code Id} 
     *  @return the returned {@code Race} list 
     *  @throws org.osid.NullArgumentException {@code ballotId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRacesForBallot(org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRacesForBallot(ballotId));
    }

    
    /**
     *  Gets all {@code Races}. 
     *
     *  In plenary mode, the returned list contains all known
     *  races or an error results. Otherwise, the returned list
     *  may contain only those races that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, races are returned that are currently
     *  active. In any status mode, active and inactive races
     *  are returned.
     *
     *  @return a list of {@code Races} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.RaceList getRaces()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaces());
    }
}
