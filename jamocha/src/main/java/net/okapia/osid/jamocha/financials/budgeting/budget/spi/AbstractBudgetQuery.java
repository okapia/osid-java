//
// AbstractBudgetQuery.java
//
//     A template for making a Budget Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budget.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for budgets.
 */

public abstract class AbstractBudgetQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.financials.budgeting.BudgetQuery {

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        return;
    }


    /**
     *  Sets the fiscal period <code> Id </code> for this query. 
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foscalPeriodId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchFiscalPeriodId(org.osid.id.Id fiscalPeriodId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the fiscal period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FiscalPeriod </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a fiscal period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the fiscal period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodQuery() is false");
    }


    /**
     *  Matches any fiscal period. 
     *
     *  @param  match <code> true </code> to match budgets with any fiscal 
     *          period, <code> false </code> to match budgets with no fiscal 
     *          period 
     */

    @OSID @Override
    public void matchAnyFiscalPeriod(boolean match) {
        return;
    }


    /**
     *  Clears the fiscal period terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodTerms() {
        return;
    }


    /**
     *  Sets the budget entry <code> Id </code> for this query. 
     *
     *  @param  budgetEntryId a budget entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> budgetEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBudgetEntryId(org.osid.id.Id budgetEntryId, boolean match) {
        return;
    }


    /**
     *  Clears the budget entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBudgetEntryIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> BudgetEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a budget entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBudgetEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a budget entries. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the budget entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBudgetEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryQuery getBudgetEntryQuery() {
        throw new org.osid.UnimplementedException("supportsBudgetEntryQuery() is false");
    }


    /**
     *  Matches any related budget entries. 
     *
     *  @param  match <code> true </code> to match budgets with any budget 
     *          entry, <code> false </code> to match budgets with no entries 
     */

    @OSID @Override
    public void matchAnyBudgetEntry(boolean match) {
        return;
    }


    /**
     *  Clears the budget entry terms. 
     */

    @OSID @Override
    public void clearBudgetEntryTerms() {
        return;
    }


    /**
     *  Sets the business <code> Id </code> for this query to match budgets 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given budget query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a budget implementing the requested record.
     *
     *  @param budgetRecordType a budget record type
     *  @return the budget query record
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetQueryRecord getBudgetQueryRecord(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetQueryRecord record : this.records) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this budget query. 
     *
     *  @param budgetQueryRecord budget query record
     *  @param budgetRecordType budget record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBudgetQueryRecord(org.osid.financials.budgeting.records.BudgetQueryRecord budgetQueryRecord, 
                                          org.osid.type.Type budgetRecordType) {

        addRecordType(budgetRecordType);
        nullarg(budgetQueryRecord, "budget query record");
        this.records.add(budgetQueryRecord);        
        return;
    }
}
