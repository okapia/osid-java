//
// AbstractAuthorizationEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.rules.authorizationenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAuthorizationEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.authorization.rules.AuthorizationEnablerSearchResults {

    private org.osid.authorization.rules.AuthorizationEnablerList authorizationEnablers;
    private final org.osid.authorization.rules.AuthorizationEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.authorization.rules.records.AuthorizationEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAuthorizationEnablerSearchResults.
     *
     *  @param authorizationEnablers the result set
     *  @param authorizationEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>authorizationEnablers</code>
     *          or <code>authorizationEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAuthorizationEnablerSearchResults(org.osid.authorization.rules.AuthorizationEnablerList authorizationEnablers,
                                            org.osid.authorization.rules.AuthorizationEnablerQueryInspector authorizationEnablerQueryInspector) {
        nullarg(authorizationEnablers, "authorization enablers");
        nullarg(authorizationEnablerQueryInspector, "authorization enabler query inspectpr");

        this.authorizationEnablers = authorizationEnablers;
        this.inspector = authorizationEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the authorization enabler list resulting from a search.
     *
     *  @return an authorization enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablers() {
        if (this.authorizationEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.authorization.rules.AuthorizationEnablerList authorizationEnablers = this.authorizationEnablers;
        this.authorizationEnablers = null;
	return (authorizationEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.authorization.rules.AuthorizationEnablerQueryInspector getAuthorizationEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  authorization enabler search record <code> Type. </code> This method must
     *  be used to retrieve an authorizationEnabler implementing the requested
     *  record.
     *
     *  @param authorizationEnablerSearchRecordType an authorizationEnabler search 
     *         record type 
     *  @return the authorization enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(authorizationEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.rules.records.AuthorizationEnablerSearchResultsRecord getAuthorizationEnablerSearchResultsRecord(org.osid.type.Type authorizationEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.authorization.rules.records.AuthorizationEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(authorizationEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(authorizationEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record authorization enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAuthorizationEnablerRecord(org.osid.authorization.rules.records.AuthorizationEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "authorization enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
