//
// InvariantMapDeviceEnablerLookupSession
//
//    Implements a DeviceEnabler lookup service backed by a fixed collection of
//    deviceEnablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements a DeviceEnabler lookup service backed by a fixed
 *  collection of device enablers. The device enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapDeviceEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractMapDeviceEnablerLookupSession
    implements org.osid.control.rules.DeviceEnablerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapDeviceEnablerLookupSession</code> with no
     *  device enablers.
     *  
     *  @param system the system
     *  @throws org.osid.NullArgumnetException {@code system} is
     *          {@code null}
     */

    public InvariantMapDeviceEnablerLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDeviceEnablerLookupSession</code> with a single
     *  device enabler.
     *  
     *  @param system the system
     *  @param deviceEnabler a single device enabler
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code deviceEnabler} is <code>null</code>
     */

      public InvariantMapDeviceEnablerLookupSession(org.osid.control.System system,
                                               org.osid.control.rules.DeviceEnabler deviceEnabler) {
        this(system);
        putDeviceEnabler(deviceEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDeviceEnablerLookupSession</code> using an array
     *  of device enablers.
     *  
     *  @param system the system
     *  @param deviceEnablers an array of device enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code deviceEnablers} is <code>null</code>
     */

      public InvariantMapDeviceEnablerLookupSession(org.osid.control.System system,
                                               org.osid.control.rules.DeviceEnabler[] deviceEnablers) {
        this(system);
        putDeviceEnablers(deviceEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDeviceEnablerLookupSession</code> using a
     *  collection of device enablers.
     *
     *  @param system the system
     *  @param deviceEnablers a collection of device enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code deviceEnablers} is <code>null</code>
     */

      public InvariantMapDeviceEnablerLookupSession(org.osid.control.System system,
                                               java.util.Collection<? extends org.osid.control.rules.DeviceEnabler> deviceEnablers) {
        this(system);
        putDeviceEnablers(deviceEnablers);
        return;
    }
}
