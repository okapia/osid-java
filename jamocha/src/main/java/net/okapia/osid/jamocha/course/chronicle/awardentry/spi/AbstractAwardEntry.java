//
// AbstractAwardEntry.java
//
//     Defines an AwardEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.awardentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>AwardEntry</code>.
 */

public abstract class AbstractAwardEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.chronicle.AwardEntry {

    private org.osid.resource.Resource student;
    private org.osid.recognition.Award award;
    private org.osid.calendaring.DateTime dateAwarded;
    private org.osid.course.program.Program program;
    private org.osid.course.Course course;
    private org.osid.assessment.Assessment assessment;

    private final java.util.Collection<org.osid.course.chronicle.records.AwardEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.student.getId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.student);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    protected void setStudent(org.osid.resource.Resource student) {
        nullarg(student, "student");
        this.student = student;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Award. </code> 
     *
     *  @return the award <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAwardId() {
        return (this.award.getId());
    }


    /**
     *  Gets the <code> Award. </code> 
     *
     *  @return the award 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward()
        throws org.osid.OperationFailedException {

        return (this.award);
    }


    /**
     *  Sets the award.
     *
     *  @param award an award
     *  @throws org.osid.NullArgumentException <code>award</code> is
     *          <code>null</code>
     */

    protected void setAward(org.osid.recognition.Award award) {
        nullarg(award, "award");
        this.award = award;
        return;
    }


    /**
     *  Gets the award date. 
     *
     *  @return the date awarded 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateAwarded() {
        return (this.dateAwarded);
    }


    /**
     *  Sets the date awarded.
     *
     *  @param date a date awarded
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setDateAwarded(org.osid.calendaring.DateTime date) {
        nullarg(date, "date awarded");
        this.dateAwarded = date;
        return;
    }


    /**
     *  Tests if this award applies to a specific program. If <code> 
     *  hasProgram() </code> is <code> true, </code> <code> hasCourse() 
     *  </code> and <code> hasAssessment() </code> must be false. 
     *
     *  @return <code> true </code> if a program is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasProgram() {
        return (this.program != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        if (!hasProgram()) {
            throw new org.osid.IllegalStateException("hasProgram() is false");
        }

        return (this.program.getId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        if (!hasProgram()) {
            throw new org.osid.IllegalStateException("hasProgram() is false");
        }

        return (this.program);
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    protected void setProgram(org.osid.course.program.Program program) {
        nullarg(program, "program");
        this.program = program;
        return;
    }


    /**
     *  Tests if this award applies to a specific course. If <code>
     *  hasCourse() </code> is <code> true, </code> <code>
     *  hasProgram() </code> and <code> hasAssessment() </code> must
     *  be <code> false.  </code>
     *
     *  @return <code> true </code> if a course is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasCourse() {
        return (this.course != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Course. </code> 
     *
     *  @return the course <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasCourse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        if (!hasCourse()) {
            throw new org.osid.IllegalStateException("hasCourse() is false");
        }

        return (this.course.getId());
    }


    /**
     *  Gets the <code> Course. </code> 
     *
     *  @return the course 
     *  @throws org.osid.IllegalStateException <code> hasCourse() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        if (!hasCourse()) {
            throw new org.osid.IllegalStateException("hasCourse() is false");
        }

        return (this.course);
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException
     *          <code>course</code> is <code>null</code>
     */

    protected void setCourse(org.osid.course.Course course) {
        nullarg(course, "course");
        this.course = course;
        return;
    }


    /**
     *  Tests if this award applies to a specific assessment. If <code> 
     *  hasAssessment() </code> is <code> true, </code> <code> hasCourse() 
     *  </code> and <code> hasProgram() </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if an assessment is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAssessment() {
        return (this.assessment != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Assessment. </code> 
     *
     *  @return the assessment <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasAssessment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAssessmentId() {
        if (!hasAssessment()) {
            throw new org.osid.IllegalStateException("hasAssessment() is false");
        }

        return (this.assessment.getId());
    }


    /**
     *  Gets the <code> Assessment. </code> 
     *
     *  @return the assessment 
     *  @throws org.osid.IllegalStateException <code> hasAssessment() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment()
        throws org.osid.OperationFailedException {

        if (!hasAssessment()) {
            throw new org.osid.IllegalStateException("hasAssessment() is false");
        }

        return (this.assessment);
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment</code>
     *          is <code>null</code>
     */

    protected void setAssessment(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "assessment");
        this.assessment = assessment;
        return;
    }


    /**
     *  Tests if this awardEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  awardEntryRecordType an award entry record type 
     *  @return <code>true</code> if the awardEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type awardEntryRecordType) {
        for (org.osid.course.chronicle.records.AwardEntryRecord record : this.records) {
            if (record.implementsRecordType(awardEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>AwardEntry</code> record <code>Type</code>.
     *
     *  @param  awardEntryRecordType the award entry record type 
     *  @return the award entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(awardEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.AwardEntryRecord getAwardEntryRecord(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.AwardEntryRecord record : this.records) {
            if (record.implementsRecordType(awardEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(awardEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this award entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param awardEntryRecord the award entry record
     *  @param awardEntryRecordType award entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecord</code> or
     *          <code>awardEntryRecordTypeawardEntry</code> is
     *          <code>null</code>
     */
            
    protected void addAwardEntryRecord(org.osid.course.chronicle.records.AwardEntryRecord awardEntryRecord, 
                                       org.osid.type.Type awardEntryRecordType) {

        nullarg(awardEntryRecord, "award entry record");
        addRecordType(awardEntryRecordType);
        this.records.add(awardEntryRecord);
        
        return;
    }
}
