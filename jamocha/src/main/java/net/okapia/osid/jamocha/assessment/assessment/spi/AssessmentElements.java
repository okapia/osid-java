//
// AssessmentElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessment.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AssessmentElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the AssessmentElement Id.
     *
     *  @return the assessment element Id
     */

    public static org.osid.id.Id getAssessmentEntityId() {
        return (makeEntityId("osid.assessment.Assessment"));
    }


    /**
     *  Gets the LevelId element Id.
     *
     *  @return the LevelId element Id
     */

    public static org.osid.id.Id getLevelId() {
        return (makeElementId("osid.assessment.assessment.LevelId"));
    }


    /**
     *  Gets the Level element Id.
     *
     *  @return the Level element Id
     */

    public static org.osid.id.Id getLevel() {
        return (makeElementId("osid.assessment.assessment.Level"));
    }


    /**
     *  Gets the RubricId element Id.
     *
     *  @return the RubricId element Id
     */

    public static org.osid.id.Id getRubricId() {
        return (makeElementId("osid.assessment.assessment.RubricId"));
    }


    /**
     *  Gets the Rubric element Id.
     *
     *  @return the Rubric element Id
     */

    public static org.osid.id.Id getRubric() {
        return (makeElementId("osid.assessment.assessment.Rubric"));
    }


    /**
     *  Gets the ItemId element Id.
     *
     *  @return the ItemId element Id
     */

    public static org.osid.id.Id getItemId() {
        return (makeQueryElementId("osid.assessment.assessment.ItemId"));
    }


    /**
     *  Gets the Item element Id.
     *
     *  @return the Item element Id
     */

    public static org.osid.id.Id getItem() {
        return (makeQueryElementId("osid.assessment.assessment.Item"));
    }


    /**
     *  Gets the AssessmentOfferedId element Id.
     *
     *  @return the AssessmentOfferedId element Id
     */

    public static org.osid.id.Id getAssessmentOfferedId() {
        return (makeQueryElementId("osid.assessment.assessment.AssessmentOfferedId"));
    }


    /**
     *  Gets the AssessmentOffered element Id.
     *
     *  @return the AssessmentOffered element Id
     */

    public static org.osid.id.Id getAssessmentOffered() {
        return (makeQueryElementId("osid.assessment.assessment.AssessmentOffered"));
    }


    /**
     *  Gets the AssessmentTakenId element Id.
     *
     *  @return the AssessmentTakenId element Id
     */

    public static org.osid.id.Id getAssessmentTakenId() {
        return (makeQueryElementId("osid.assessment.assessment.AssessmentTakenId"));
    }


    /**
     *  Gets the AssessmentTaken element Id.
     *
     *  @return the AssessmentTaken element Id
     */

    public static org.osid.id.Id getAssessmentTaken() {
        return (makeQueryElementId("osid.assessment.assessment.AssessmentTaken"));
    }


    /**
     *  Gets the BankId element Id.
     *
     *  @return the BankId element Id
     */

    public static org.osid.id.Id getBankId() {
        return (makeQueryElementId("osid.assessment.assessment.BankId"));
    }


    /**
     *  Gets the Bank element Id.
     *
     *  @return the Bank element Id
     */

    public static org.osid.id.Id getBank() {
        return (makeQueryElementId("osid.assessment.assessment.Bank"));
    }
}
