//
// AbstractRequisiteLookupSession.java
//
//    A starter implementation framework for providing a Requisite
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Requisite
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRequisites(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRequisiteLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.requisite.RequisiteLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean sequestered   = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Requisite</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRequisites() {
        return (true);
    }


    /**
     *  A complete view of the <code>Requisite</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRequisiteView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Requisite</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRequisiteView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include requisites in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active requisites are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRequisiteView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive requisites are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRequisiteView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  requisites.
     */

    @OSID @Override
    public void useSequesteredRequisiteView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All requisites are returned including sequestered requisites.
     */

    @OSID @Override
    public void useUnsequesteredRequisiteView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if unsequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }

     
    /**
     *  Gets the <code>Requisite</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Requisite</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Requisite</code> and
     *  retained for compatibility.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  @param  requisiteId <code>Id</code> of the
     *          <code>Requisite</code>
     *  @return the requisite
     *  @throws org.osid.NotFoundException <code>requisiteId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>requisiteId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite getRequisite(org.osid.id.Id requisiteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.requisite.RequisiteList requisites = getRequisites()) {
            while (requisites.hasNext()) {
                org.osid.course.requisite.Requisite requisite = requisites.getNextRequisite();
                if (requisite.getId().equals(requisiteId)) {
                    return (requisite);
                }
            }
        } 

        throw new org.osid.NotFoundException(requisiteId + " not found");
    }


    /**
     *  Gets a <code>RequisiteList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  requisites specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Requisites</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRequisites()</code>.
     *
     *  @param  requisiteIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByIds(org.osid.id.IdList requisiteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.requisite.Requisite> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = requisiteIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRequisite(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("requisite " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.requisite.requisite.LinkedRequisiteList(ret));
    }


    /**
     *  Gets a <code>RequisiteList</code> corresponding to the given
     *  requisite genus <code>Type</code> which does not include
     *  requisites of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRequisites()</code>.
     *
     *  @param  requisiteGenusType a requisite genus type 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByGenusType(org.osid.type.Type requisiteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteGenusFilterList(getRequisites(), requisiteGenusType));
    }


    /**
     *  Gets a <code>RequisiteList</code> corresponding to the given
     *  requisite genus <code>Type</code> and include any additional
     *  requisites with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRequisites()</code>.
     *
     *  @param  requisiteGenusType a requisite genus type 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByParentGenusType(org.osid.type.Type requisiteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRequisitesByGenusType(requisiteGenusType));
    }


    /**
     *  Gets a <code>RequisiteList</code> containing the given
     *  requisite record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRequisites()</code>.
     *
     *  @param  requisiteRecordType a requisite record type 
     *  @return the returned <code>Requisite</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByRecordType(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteRecordFilterList(getRequisites(), requisiteRecordType));
    }


    /**
     *  Gets a <code>RequisiteList</code> immediately containing the
     *  given requisite option.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites are
     *  returned.
     *  
     *  In sequestered mode, no sequestered requisites are
     *  returned. In unsequestered mode, all requisites are returned.
     *
     *  @param  requisiteOptionId a requisite option <code>Id</code> 
     *  @return the returned <code>RequisiteList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteOptionId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForRequisiteOption(org.osid.id.Id requisiteOptionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilterList(new RequisiteOptionFilter(requisiteOptionId), getRequisites()));
    }


    /**
     *  Gets all <code>Requisites</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, requisites are returned that are currently
     *  active. In any status mode, active and inactive requisites are
     *  returned.
     *
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.requisite.RequisiteList getRequisites()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the <code>CourseRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CourseRequirement</code> and retained for compatibility.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseRequirementId <code>Id</code> of the
     *          <code>CourseRequirement</code>
     *  @return the course requirement
     *  @throws org.osid.NotFoundException <code>courseRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirement getCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.requisite.CourseRequirementList requirements = getCourseRequirements()) {
            while (requirements.hasNext()) {
                org.osid.course.requisite.CourseRequirement requirement = requirements.getNextCourseRequirement();
                if (requirement.getId().equals(courseRequirementId)) {
                    return (requirement);
                }
            }
        } 

        throw new org.osid.NotFoundException(courseRequirementId + " not found");
    }


    /**
     *  Gets a <code>CourseRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the course
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>CourseRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCourseRequirements()</code>.
     *
     *  @param  courseRequirementIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CourseRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByIds(org.osid.id.IdList courseRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.requisite.CourseRequirement> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = courseRequirementIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCourseRequirement(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("course requirement " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.requisite.courserequirement.LinkedCourseRequirementList(ret));
    }


    /**
     *  Gets a <code>CourseRequirementList</code> corresponding to the
     *  given course requirement genus <code>Type</code> which does
     *  not include course requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCourseRequirements()</code>.
     *
     *  @param  courseRequirementGenusType a course requirement genus type 
     *  @return the returned <code>CourseRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByGenusType(org.osid.type.Type courseRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.courserequirement.CourseRequirementGenusFilterList(getCourseRequirements(), courseRequirementGenusType));
    }


    /**
     *  Gets a <code>CourseRequirementList</code> corresponding to the
     *  given course requirement genus <code>Type</code> and include
     *  any additional course requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourseRequirements()</code>.
     *
     *  @param  courseRequirementGenusType a course requirement genus type 
     *  @return the returned <code>CourseRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByParentGenusType(org.osid.type.Type courseRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCourseRequirementsByGenusType(courseRequirementGenusType));
    }


    /**
     *  Gets a <code>CourseRequirementList</code> containing the given
     *  course requirement record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourseRequirements()</code>.
     *
     *  @param  courseRequirementRecordType a course requirement record type 
     *  @return the returned <code>CourseRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByRecordType(org.osid.type.Type courseRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.courserequirement.CourseRequirementRecordFilterList(getCourseRequirements(), courseRequirementRecordType));
    }


    /**
     *  Gets a <code>CourseRequirementList</code> containing the given
     *  course.
     *  
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *  
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  courseId a course <code>Id</code> 
     *  @return the returned <code>CourseRequirementList</code> 
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.courserequirement.CourseRequirementFilterList(new CourseFilter(courseId), getCourseRequirements()));
    }        


    /**
     *  Gets a <code>CourseRequirementList</code> with the given
     *  <code>Requisite</code>.
     *  
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session.
     *  
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code> 
     *  @return the returned <code>CourseRequirementList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirementList getCourseRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.courserequirement.CourseRequirementFilterList(new CourseAltRequisiteFilter(requisiteId), getCourseRequirements()));
    }

    
    /**
     *  Gets a <code>RequisiteList</code> immediately containing the
     *  given course requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, course requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  courseRequirementId a course requirement <code>Id</code> 
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>courseRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForCourseRequirement(org.osid.id.Id courseRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilterList(new CourseRequirementFilter(courseRequirementId), getRequisites()));
    }


    /**
     *  Gets all <code>CourseRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known course
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those course requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, course requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  course requirements are returned.
     *
     *  @return a list of <code>CourseRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.requisite.CourseRequirementList getCourseRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the <code>ProgramRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProgramRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ProgramRequirement</code> and retained for compatibility.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programRequirementId <code>Id</code> of the
     *          <code>ProgramRequirement</code>
     *  @return the program requirement
     *  @throws org.osid.NotFoundException <code>programRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>programRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirement getProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.requisite.ProgramRequirementList requirements = getProgramRequirements()) {
            while (requirements.hasNext()) {
                org.osid.course.requisite.ProgramRequirement requirement = requirements.getNextProgramRequirement();
                if (requirement.getId().equals(programRequirementId)) {
                    return (requirement);
                }
            }
        } 

        throw new org.osid.NotFoundException(programRequirementId + " not found");
    }


    /**
     *  Gets a <code>ProgramRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the program
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>ProgramRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProgramRequirements()</code>.
     *
     *  @param  programRequirementIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProgramRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByIds(org.osid.id.IdList programRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.requisite.ProgramRequirement> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = programRequirementIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProgramRequirement(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("program requirement " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.requisite.programrequirement.LinkedProgramRequirementList(ret));
    }


    /**
     *  Gets a <code>ProgramRequirementList</code> corresponding to the
     *  given program requirement genus <code>Type</code> which does
     *  not include program requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProgramRequirements()</code>.
     *
     *  @param  programRequirementGenusType a program requirement genus type 
     *  @return the returned <code>ProgramRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByGenusType(org.osid.type.Type programRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.programrequirement.ProgramRequirementGenusFilterList(getProgramRequirements(), programRequirementGenusType));
    }


    /**
     *  Gets a <code>ProgramRequirementList</code> corresponding to the
     *  given program requirement genus <code>Type</code> and include
     *  any additional program requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProgramRequirements()</code>.
     *
     *  @param  programRequirementGenusType a program requirement genus type 
     *  @return the returned <code>ProgramRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByParentGenusType(org.osid.type.Type programRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProgramRequirementsByGenusType(programRequirementGenusType));
    }


    /**
     *  Gets a <code>ProgramRequirementList</code> containing the given
     *  program requirement record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProgramRequirements()</code>.
     *
     *  @param  programRequirementRecordType a program requirement record type 
     *  @return the returned <code>ProgramRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByRecordType(org.osid.type.Type programRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.programrequirement.ProgramRequirementRecordFilterList(getProgramRequirements(), programRequirementRecordType));
    }


    /**
     *  Gets a <code>ProgramRequirementList</code> containing the
     *  given program.
     *  
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *  
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  programId a program <code>Id</code> 
     *  @return the returned <code>ProgramRequirementList</code> 
     *  @throws org.osid.NullArgumentException <code>programId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.programrequirement.ProgramRequirementFilterList(new ProgramFilter(programId), getProgramRequirements()));
    }        


    /**
     *  Gets a <code>ProgramRequirementList</code> with the given
     *  <code>Requisite</code>.
     *  
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session.
     *  
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code> 
     *  @return the returned <code>ProgramRequirementList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirementList getProgramRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.programrequirement.ProgramRequirementFilterList(new ProgramAltRequisiteFilter(requisiteId), getProgramRequirements()));
    }


    /**
     *  Gets a <code>RequisiteList</code> immediately containing the
     *  given program requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, program requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  programRequirementId a program requirement <code>Id</code> 
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForProgramRequirement(org.osid.id.Id programRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilterList(new ProgramRequirementFilter(programRequirementId), getRequisites()));
    }


    /**
     *  Gets all <code>ProgramRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known program
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those program requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, program requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  program requirements are returned.
     *
     *  @return a list of <code>ProgramRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.requisite.ProgramRequirementList getProgramRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the <code>CredentialRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CredentialRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>CredentialRequirement</code> and retained for compatibility.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialRequirementId <code>Id</code> of the
     *          <code>CredentialRequirement</code>
     *  @return the credential requirement
     *  @throws org.osid.NotFoundException <code>credentialRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>credentialRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirement getCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.requisite.CredentialRequirementList requirements = getCredentialRequirements()) {
            while (requirements.hasNext()) {
                org.osid.course.requisite.CredentialRequirement requirement = requirements.getNextCredentialRequirement();
                if (requirement.getId().equals(credentialRequirementId)) {
                    return (requirement);
                }
            }
        } 

        throw new org.osid.NotFoundException(credentialRequirementId + " not found");
    }


    /**
     *  Gets a <code>CredentialRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the credential
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>CredentialRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCredentialRequirements()</code>.
     *
     *  @param  credentialRequirementIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CredentialRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByIds(org.osid.id.IdList credentialRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.requisite.CredentialRequirement> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = credentialRequirementIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCredentialRequirement(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("credential requirement " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.requisite.credentialrequirement.LinkedCredentialRequirementList(ret));
    }


    /**
     *  Gets a <code>CredentialRequirementList</code> corresponding to the
     *  given credential requirement genus <code>Type</code> which does
     *  not include credential requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCredentialRequirements()</code>.
     *
     *  @param  credentialRequirementGenusType a credential requirement genus type 
     *  @return the returned <code>CredentialRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByGenusType(org.osid.type.Type credentialRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.credentialrequirement.CredentialRequirementGenusFilterList(getCredentialRequirements(), credentialRequirementGenusType));
    }


    /**
     *  Gets a <code>CredentialRequirementList</code> corresponding to the
     *  given credential requirement genus <code>Type</code> and include
     *  any additional credential requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCredentialRequirements()</code>.
     *
     *  @param  credentialRequirementGenusType a credential requirement genus type 
     *  @return the returned <code>CredentialRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByParentGenusType(org.osid.type.Type credentialRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCredentialRequirementsByGenusType(credentialRequirementGenusType));
    }


    /**
     *  Gets a <code>CredentialRequirementList</code> containing the given
     *  credential requirement record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCredentialRequirements()</code>.
     *
     *  @param  credentialRequirementRecordType a credential requirement record type 
     *  @return the returned <code>CredentialRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByRecordType(org.osid.type.Type credentialRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.credentialrequirement.CredentialRequirementRecordFilterList(getCredentialRequirements(), credentialRequirementRecordType));
    }


    /**
     *  Gets a <code>CredentialRequirementList</code> containing the
     *  given credential.
     *  
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *  
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  credentialId a credential <code>Id</code> 
     *  @return the returned <code>CredentialRequirementList</code> 
     *  @throws org.osid.NullArgumentException <code>credentialId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByCredential(org.osid.id.Id credentialId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.credentialrequirement.CredentialRequirementFilterList(new CredentialFilter(credentialId), getCredentialRequirements()));
    }        


    /**
     *  Gets a <code>CredentialRequirementList</code> with the given
     *  <code>Requisite</code>.
     *  
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session.
     *  
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code> 
     *  @return the returned <code>CredentialRequirementList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirementList getCredentialRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.credentialrequirement.CredentialRequirementFilterList(new CredentialAltRequisiteFilter(requisiteId), getCredentialRequirements()));
    }


    /**
     *  Gets a <code>RequisiteList</code> immediately containing the
     *  given credential requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, credential requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param credentialRequirementId a credential requirement
     *         <code>Id</code>
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForCredentialRequirement(org.osid.id.Id credentialRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilterList(new CredentialRequirementFilter(credentialRequirementId), getRequisites()));
    }


    /**
     *  Gets all <code>CredentialRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known credential
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those credential requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, credential requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  credential requirements are returned.
     *
     *  @return a list of <code>CredentialRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.requisite.CredentialRequirementList getCredentialRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the <code>LearningObjectiveRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>LearningObjectiveRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>LearningObjectiveRequirement</code> and retained for compatibility.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveRequirementId <code>Id</code> of the
     *          <code>LearningObjectiveRequirement</code>
     *  @return the learning objective requirement
     *  @throws org.osid.NotFoundException
     *          <code>learningObjectiveRequirementId</code> not found
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementId</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirement getLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.requisite.LearningObjectiveRequirementList requirements = getLearningObjectiveRequirements()) {
            while (requirements.hasNext()) {
                org.osid.course.requisite.LearningObjectiveRequirement requirement = requirements.getNextLearningObjectiveRequirement();
                if (requirement.getId().equals(learningObjectiveRequirementId)) {
                    return (requirement);
                }
            }
        } 

        throw new org.osid.NotFoundException(learningObjectiveRequirementId + " not found");
    }


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  corresponding to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  learning objective requirements specified in the
     *  <code>Id</code> list, in the order of the list, including
     *  duplicates, or an error results if an <code>Id</code> in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible <code>LearningObjectiveRequirements</code> may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getLearningObjectiveRequirements()</code>.
     *
     *  @param  learningObjectiveRequirementIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>LearningObjectiveRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementIds</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByIds(org.osid.id.IdList learningObjectiveRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.requisite.LearningObjectiveRequirement> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = learningObjectiveRequirementIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getLearningObjectiveRequirement(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("learning objective requirement " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.requisite.learningobjectiverequirement.LinkedLearningObjectiveRequirementList(ret));
    }


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  corresponding to the given learning objective requirement
     *  genus <code>Type</code> which does not include learning
     *  objective requirements of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getLearningObjectiveRequirements()</code>.
     *
     *  @param learningObjectiveRequirementGenusType a learning
     *         objective requirement genus type
     *  @return the returned <code>LearningObjectiveRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByGenusType(org.osid.type.Type learningObjectiveRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.learningobjectiverequirement.LearningObjectiveRequirementGenusFilterList(getLearningObjectiveRequirements(), learningObjectiveRequirementGenusType));
    }


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  corresponding to the given learning objective requirement
     *  genus <code>Type</code> and include any additional learning
     *  objective requirements with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getLearningObjectiveRequirements()</code>.
     *
     *  @param learningObjectiveRequirementGenusType a learning
     *         objective requirement genus type
     *  @return the returned <code>LearningObjectiveRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementGenusType</code> is
     *          <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByParentGenusType(org.osid.type.Type learningObjectiveRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getLearningObjectiveRequirementsByGenusType(learningObjectiveRequirementGenusType));
    }


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  containing the given learning objective requirement record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getLearningObjectiveRequirements()</code>.
     *
     *  @param learningObjectiveRequirementRecordType a learning
     *         objective requirement record type
     *  @return the returned <code>LearningObjectiveRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByRecordType(org.osid.type.Type learningObjectiveRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.learningobjectiverequirement.LearningObjectiveRequirementRecordFilterList(getLearningObjectiveRequirements(), learningObjectiveRequirementRecordType));
    }


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code>
     *  containing the given learning objective.
     *  
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learningObjective
     *  requirements that are accessible through this session.
     *  
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @param  learningObjectiveId a learning objective <code>Id</code> 
     *  @return the returned <code>LearningObjectiveRequirementList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByObjective(org.osid.id.Id learningObjectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.learningobjectiverequirement.LearningObjectiveRequirementFilterList(new LearningObjectiveFilter(learningObjectiveId), getLearningObjectiveRequirements()));
    }        


    /**
     *  Gets a <code>LearningObjectiveRequirementList</code> with the
     *  given <code>Requisite</code>.
     *  
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session.
     *  
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learningObjective requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code> 
     *  @return the returned <code>LearningObjectiveRequirementList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.learningobjectiverequirement.LearningObjectiveRequirementFilterList(new LearningObjectiveAltRequisiteFilter(requisiteId), getLearningObjectiveRequirements()));
    }


    /**
     *  Gets a <code>RequisiteList</code> immediately containing the
     *  given learning objective requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, learning objective requirements are processed
     *  and requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param learningObjectiveRequirementId an learning objective
     *         requirement <code>Id</code>
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForLearningObjectiveRequirement(org.osid.id.Id learningObjectiveRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilterList(new LearningObjectiveRequirementFilter(learningObjectiveRequirementId), getRequisites()));
    }


    /**
     *  Gets all <code>LearningObjectiveRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known learning
     *  objective requirements or an error results. Otherwise, the
     *  returned list may contain only those learning objective
     *  requirements that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, learning objective requirements are returned
     *  that are currently active. In any status mode, active and
     *  inactive learning objective requirements are returned.
     *
     *  @return a list of <code>LearningObjectiveRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.requisite.LearningObjectiveRequirementList getLearningObjectiveRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the <code>AssessmentRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AssessmentRequirement</code> and retained for compatibility.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentRequirementId <code>Id</code> of the
     *          <code>AssessmentRequirement</code>
     *  @return the assessment requirement
     *  @throws org.osid.NotFoundException <code>assessmentRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirement getAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.requisite.AssessmentRequirementList requirements = getAssessmentRequirements()) {
            while (requirements.hasNext()) {
                org.osid.course.requisite.AssessmentRequirement requirement = requirements.getNextAssessmentRequirement();
                if (requirement.getId().equals(assessmentRequirementId)) {
                    return (requirement);
                }
            }
        } 

        throw new org.osid.NotFoundException(assessmentRequirementId + " not found");
    }


    /**
     *  Gets an <code>AssessmentRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the assessment
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>AssessmentRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAssessmentRequirements()</code>.
     *
     *  @param  assessmentRequirementIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByIds(org.osid.id.IdList assessmentRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.requisite.AssessmentRequirement> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = assessmentRequirementIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAssessmentRequirement(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("assessment requirement " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.requisite.assessmentrequirement.LinkedAssessmentRequirementList(ret));
    }


    /**
     *  Gets an <code>AssessmentRequirementList</code> corresponding to the
     *  given assessment requirement genus <code>Type</code> which does
     *  not include assessment requirements of types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAssessmentRequirements()</code>.
     *
     *  @param  assessmentRequirementGenusType an assessment requirement genus type 
     *  @return the returned <code>AssessmentRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByGenusType(org.osid.type.Type assessmentRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.assessmentrequirement.AssessmentRequirementGenusFilterList(getAssessmentRequirements(), assessmentRequirementGenusType));
    }


    /**
     *  Gets an <code>AssessmentRequirementList</code> corresponding to the
     *  given assessment requirement genus <code>Type</code> and include
     *  any additional assessment requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssessmentRequirements()</code>.
     *
     *  @param  assessmentRequirementGenusType an assessment requirement genus type 
     *  @return the returned <code>AssessmentRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByParentGenusType(org.osid.type.Type assessmentRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAssessmentRequirementsByGenusType(assessmentRequirementGenusType));
    }


    /**
     *  Gets an <code>AssessmentRequirementList</code> containing the given
     *  assessment requirement record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssessmentRequirements()</code>.
     *
     *  @param  assessmentRequirementRecordType an assessment requirement record type 
     *  @return the returned <code>AssessmentRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByRecordType(org.osid.type.Type assessmentRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.assessmentrequirement.AssessmentRequirementRecordFilterList(getAssessmentRequirements(), assessmentRequirementRecordType));
    }


    /**
     *  Gets an <code>AssessmentRequirementList</code> containing the
     *  given assessment.
     *  
     *  In plenary mode, the returned list contains all known
     *  assessment requirements or an error results. Otherwise, the
     *  returned list may contain only those assessment requirements
     *  that are accessible through this session.
     *  
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  assessmentId an assessment <code>Id</code> 
     *  @return the returned <code>AssessmentRequirementList</code> 
     *  @throws org.osid.NullArgumentException <code>assessmentId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.assessmentrequirement.AssessmentRequirementFilterList(new AssessmentFilter(assessmentId), getAssessmentRequirements()));
    }        


    /**
     *  Gets an <code>AssessmentRequirementList</code> with the given
     *  <code>Requisite</code>.
     *  
     *  In plenary mode, the returned list contains all known
     *  assessment requirements or an error results. Otherwise, the
     *  returned list may contain only those assessment requirements
     *  that are accessible through this session.
     *  
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code> 
     *  @return the returned <code>AssessmentRequirementList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.assessmentrequirement.AssessmentRequirementFilterList(new AssessmentAltRequisiteFilter(requisiteId), getAssessmentRequirements()));
    }


    /**
     *  Gets a <code>RequisiteList</code> immediately containing the
     *  given assessment requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, assessment requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  assessmentRequirementId an assessment requirement <code>Id</code> 
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForAssessmentRequirement(org.osid.id.Id assessmentRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilterList(new AssessmentRequirementFilter(assessmentRequirementId), getRequisites()));
    }


    /**
     *  Gets all <code>AssessmentRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known assessment
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those assessment requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, assessment requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment requirements are returned.
     *
     *  @return a list of <code>AssessmentRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.requisite.AssessmentRequirementList getAssessmentRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Gets the <code>AwardRequirement</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AwardRequirement</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AwardRequirement</code> and retained for compatibility.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardRequirementId <code>Id</code> of the
     *          <code>AwardRequirement</code>
     *  @return the award requirement
     *  @throws org.osid.NotFoundException <code>awardRequirementId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>awardRequirementId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirement getAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.requisite.AwardRequirementList requirements = getAwardRequirements()) {
            while (requirements.hasNext()) {
                org.osid.course.requisite.AwardRequirement requirement = requirements.getNextAwardRequirement();
                if (requirement.getId().equals(awardRequirementId)) {
                    return (requirement);
                }
            }
        } 

        throw new org.osid.NotFoundException(awardRequirementId + " not found");
    }


    /**
     *  Gets an <code>AwardRequirementList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the award
     *  requirements specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>AwardRequirements</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAwardRequirements()</code>.
     *
     *  @param  awardRequirementIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AwardRequirement</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByIds(org.osid.id.IdList awardRequirementIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.requisite.AwardRequirement> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = awardRequirementIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAwardRequirement(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("award requirement " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.requisite.awardrequirement.LinkedAwardRequirementList(ret));
    }


    /**
     *  Gets an <code>AwardRequirementList</code> corresponding to the
     *  given award requirement genus <code>Type</code> which does not
     *  include award requirements of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAwardRequirements()</code>.
     *
     *  @param  awardRequirementGenusType an award requirement genus type 
     *  @return the returned <code>AwardRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByGenusType(org.osid.type.Type awardRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.awardrequirement.AwardRequirementGenusFilterList(getAwardRequirements(), awardRequirementGenusType));
    }


    /**
     *  Gets an <code>AwardRequirementList</code> corresponding to the
     *  given award requirement genus <code>Type</code> and include
     *  any additional award requirements with genus types derived
     *  from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAwardRequirements()</code>.
     *
     *  @param  awardRequirementGenusType an award requirement genus type 
     *  @return the returned <code>AwardRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByParentGenusType(org.osid.type.Type awardRequirementGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAwardRequirementsByGenusType(awardRequirementGenusType));
    }


    /**
     *  Gets an <code>AwardRequirementList</code> containing the given
     *  award requirement record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAwardRequirements()</code>.
     *
     *  @param  awardRequirementRecordType an award requirement record type 
     *  @return the returned <code>AwardRequirement</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByRecordType(org.osid.type.Type awardRequirementRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.awardrequirement.AwardRequirementRecordFilterList(getAwardRequirements(), awardRequirementRecordType));
    }


    /**
     *  Gets an <code>AwardRequirementList</code> containing the
     *  given award.
     *  
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *  
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  awardId an award <code>Id</code> 
     *  @return the returned <code>AwardRequirementList</code> 
     *  @throws org.osid.NullArgumentException <code>awardId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByAward(org.osid.id.Id awardId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.awardrequirement.AwardRequirementFilterList(new AwardFilter(awardId), getAwardRequirements()));
    }        


    /**
     *  Gets an <code>AwardRequirementList</code> with the given
     *  <code>Requisite</code>.
     *  
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session.
     *  
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @param  requisiteId a requisite <code>Id</code> 
     *  @return the returned <code>AwardRequirementList</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirementList getAwardRequirementsByAltRequisite(org.osid.id.Id requisiteId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.awardrequirement.AwardRequirementFilterList(new AwardAltRequisiteFilter(requisiteId), getAwardRequirements()));
    }


    /**
     *  Gets an <code>RequisiteList</code> immediately containing the
     *  given award requirement.
     *  
     *  In plenary mode, the returned list contains all known
     *  requisites or an error results. Otherwise, the returned list
     *  may contain only those requisites that are accessible through
     *  this session.
     *  
     *  In active mode, award requirements are processed and
     *  requisites are returned that are currently active. In any
     *  status mode, active and inactive requisites are returned.
     *
     *  @param  awardRequirementId an award requirement <code>Id</code> 
     *  @return a list of <code>Requisites</code> 
     *  @throws org.osid.NullArgumentException
     *          <code>awardRequirementId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesForAwardRequirement(org.osid.id.Id awardRequirementId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilterList(new AwardRequirementFilter(awardRequirementId), getRequisites()));
    }


    /**
     *  Gets all <code>AwardRequirements</code>. 
     *
     *  In plenary mode, the returned list contains all known award
     *  requirements or an error results. Otherwise, the returned list
     *  may contain only those award requirements that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, award requirements are returned that are
     *  currently active. In any status mode, active and inactive
     *  award requirements are returned.
     *
     *  @return a list of <code>AwardRequirements</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.requisite.AwardRequirementList getAwardRequirements()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the requisite list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of requisites
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.requisite.RequisiteList filterRequisitesOnViews(org.osid.course.requisite.RequisiteList list)
        throws org.osid.OperationFailedException {

        org.osid.course.requisite.RequisiteList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.ActiveRequisiteFilterList(ret);
        }

        if (isSequestered()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.SequesteredRequisiteFilterList(ret);
        }

        return (ret);
    }


    public static class RequisiteOptionFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilter {

        private final org.osid.id.Id requisiteOptionId;

        
        /**
         *  Constructs a new <code>RequisiteOptionFilter</code>.
         *
         *  @param requisiteOptionId the requisite option to filter
         *  @throws org.osid.NullArgumentException
         *          <code>requisiteOptionId</code> is
         *          <code>null</code>
         */

        public RequisiteOptionFilter(org.osid.id.Id requisiteOptionId) {
            nullarg(requisiteOptionId, "requisite option Id");
            this.requisiteOptionId = requisiteOptionId;
            return;
        }


        /**
         *  Used by the RequisiteFilterList to filter the requisite
         *  list based on requisite option.
         *
         *  @param requisite the requisite
         *  @return <code>true</code> to pass the requisite,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.Requisite requisite) {
            if (requisite.hasRequisiteOptions()) {
                for (org.osid.course.requisite.Requisite req : requisite.getRequisiteOptions()) {
                    if (req.getId().equals(this.requisiteOptionId)) {
                        return (true);
                    }
                }
            }

            return (false);
        }
    }        


    public static class CourseRequirementFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilter {

        private final org.osid.id.Id courseRequirementId;

        
        /**
         *  Constructs a new <code>CourseRequisiteFilter</code>.
         *
         *  @param courseRequirementId the course requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>courseRequirementId</code> is
         *          <code>null</code>
         */

        public CourseRequirementFilter(org.osid.id.Id courseRequirementId) {
            nullarg(courseRequirementId, "course requisite Id");
            this.courseRequirementId = courseRequirementId;
            return;
        }


        /**
         *  Used by the RequisiteFilterList to filter the requisite
         *  list based on course requisite.
         *
         *  @param requisite the requisite
         *  @return <code>true</code> to pass the requisite,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.Requisite requisite) {
            if (requisite.hasCourseRequirements()) {
                for (org.osid.course.requisite.CourseRequirement req : requisite.getCourseRequirements()) {
                    if (req.getId().equals(this.courseRequirementId)) {
                        return (true);
                    }
                }
            }

            return (false);
        }
    }        


    public static class CourseFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.courserequirement.CourseRequirementFilter {

        private final org.osid.id.Id courseId;

        
        /**
         *  Constructs a new <code>CourseFilter</code>.
         *
         *  @param courseId the course to filter
         *  @throws org.osid.NullArgumentException
         *          <code>courseId</code> is <code>null</code>
         */

        public CourseFilter(org.osid.id.Id courseId) {
            nullarg(courseId, "course Id");
            this.courseId = courseId;
            return;
        }


        /**
         *  Used by the CourseRequirementFilterList to filter the
         *  course requirement list based on course.
         *
         *  @param courseRequirement the course requirement
         *  @return <code>true</code> to pass the course requirement,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.CourseRequirement courseRequirement) {
            return (courseRequirement.getCourseId().equals(this.courseId));
        }
    }        


    public static class CourseAltRequisiteFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.courserequirement.CourseRequirementFilter {

        private final org.osid.id.Id requisiteId;;

        
        /**
         *  Constructs a new <code>CourseAltRequisiteFilter</code>.
         *
         *  @param requisiteId the requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>requisiteId</code> is <code>null</code>
         */

        public CourseAltRequisiteFilter(org.osid.id.Id requisiteId) {
            nullarg(requisiteId, "requisite Id");
            this.requisiteId = requisiteId;
            return;
        }


        /**
         *  Used by the CourseRequirementFilterList to filter the
         *  course requirement list based on alt requisite.
         *
         *  @param courseRequirement the course requirement
         *  @return <code>true</code> to pass the course requirement,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.CourseRequirement courseRequirement) {
            for (org.osid.course.requisite.Requisite req : courseRequirement.getAltRequisites()) {
                if (req.getId().equals(this.requisiteId)) {
                    return (true);
                }
            }

            return (false);
        }
    }        


    public static class ProgramRequirementFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilter {

        private final org.osid.id.Id programRequirementId;

        
        /**
         *  Constructs a new <code>ProgramRequisiteFilter</code>.
         *
         *  @param programRequirementId the program requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>programRequirementId</code> is
         *          <code>null</code>
         */

        public ProgramRequirementFilter(org.osid.id.Id programRequirementId) {
            nullarg(programRequirementId, "program requisite Id");
            this.programRequirementId = programRequirementId;
            return;
        }


        /**
         *  Used by the RequisiteFilterList to filter the requisite
         *  list based on program requisite.
         *
         *  @param requisite the requisite
         *  @return <code>true</code> to pass the requisite,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.Requisite requisite) {
            if (requisite.hasProgramRequirements()) {
                for (org.osid.course.requisite.ProgramRequirement req : requisite.getProgramRequirements()) {
                    if (req.getId().equals(this.programRequirementId)) {
                        return (true);
                    }
                }
            }

            return (false);
        }
    }        


    public static class ProgramFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.programrequirement.ProgramRequirementFilter {

        private final org.osid.id.Id programId;

        
        /**
         *  Constructs a new <code>ProgramFilter</code>.
         *
         *  @param programId the program to filter
         *  @throws org.osid.NullArgumentException
         *          <code>programId</code> is <code>null</code>
         */

        public ProgramFilter(org.osid.id.Id programId) {
            nullarg(programId, "program Id");
            this.programId = programId;
            return;
        }


        /**
         *  Used by the ProgramRequirementFilterList to filter the
         *  program requirement list based on program.
         *
         *  @param programRequirement the program requirement
         *  @return <code>true</code> to pass the program requirement,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.ProgramRequirement programRequirement) {
            return (programRequirement.getProgramId().equals(this.programId));
        }
    }        


    public static class ProgramAltRequisiteFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.programrequirement.ProgramRequirementFilter {

        private final org.osid.id.Id requisiteId;;

        
        /**
         *  Constructs a new <code>ProgramAltRequisiteFilter</code>.
         *
         *  @param requisiteId the requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>requisiteId</code> is <code>null</code>
         */

        public ProgramAltRequisiteFilter(org.osid.id.Id requisiteId) {
            nullarg(requisiteId, "requisite Id");
            this.requisiteId = requisiteId;
            return;
        }


        /**
         *  Used by the ProgramRequirementFilterList to filter the
         *  program requirement list based on alt requisite.
         *
         *  @param programRequirement the program requirement
         *  @return <code>true</code> to pass the program requirement,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.ProgramRequirement programRequirement) {
            for (org.osid.course.requisite.Requisite req : programRequirement.getAltRequisites()) {
                if (req.getId().equals(this.requisiteId)) {
                    return (true);
                }
            }

            return (false);
        }
    }        


    public static class CredentialRequirementFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilter {

        private final org.osid.id.Id credentialRequirementId;

        
        /**
         *  Constructs a new <code>CredentialRequisiteFilter</code>.
         *
         *  @param credentialRequirementId the credential requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>credentialRequirementId</code> is
         *          <code>null</code>
         */

        public CredentialRequirementFilter(org.osid.id.Id credentialRequirementId) {
            nullarg(credentialRequirementId, "credential requisite Id");
            this.credentialRequirementId = credentialRequirementId;
            return;
        }


        /**
         *  Used by the RequisiteFilterList to filter the requisite
         *  list based on credential requisite.
         *
         *  @param requisite the requisite
         *  @return <code>true</code> to pass the requisite,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.Requisite requisite) {
            if (requisite.hasCredentialRequirements()) {
                for (org.osid.course.requisite.CredentialRequirement req : requisite.getCredentialRequirements()) {
                    if (req.getId().equals(this.credentialRequirementId)) {
                        return (true);
                    }
                }
            }

            return (false);
        }
    }        


    public static class CredentialFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.credentialrequirement.CredentialRequirementFilter {

        private final org.osid.id.Id credentialId;

        
        /**
         *  Constructs a new <code>CredentialFilter</code>.
         *
         *  @param credentialId the credential to filter
         *  @throws org.osid.NullArgumentException
         *          <code>credentialId</code> is <code>null</code>
         */

        public CredentialFilter(org.osid.id.Id credentialId) {
            nullarg(credentialId, "credential Id");
            this.credentialId = credentialId;
            return;
        }


        /**
         *  Used by the CredentialRequirementFilterList to filter the
         *  credential requirement list based on credential.
         *
         *  @param credentialRequirement the credential requirement
         *  @return <code>true</code> to pass the credential requirement,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.CredentialRequirement credentialRequirement) {
            return (credentialRequirement.getCredentialId().equals(this.credentialId));
        }
    }        


    public static class CredentialAltRequisiteFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.credentialrequirement.CredentialRequirementFilter {

        private final org.osid.id.Id requisiteId;;

        
        /**
         *  Constructs a new
         *  <code>CredentialAltRequisiteFilter</code>.
         *
         *  @param requisiteId the requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>requisiteId</code> is <code>null</code>
         */

        public CredentialAltRequisiteFilter(org.osid.id.Id requisiteId) {
            nullarg(requisiteId, "requisite Id");
            this.requisiteId = requisiteId;
            return;
        }


        /**
         *  Used by the CredentialRequirementFilterList to filter the
         *  credential requirement list based on alt requisite.
         *
         *  @param credentialRequirement the credential requirement
         *  @return <code>true</code> to pass the credential
         *          requirement, <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.CredentialRequirement credentialRequirement) {
            for (org.osid.course.requisite.Requisite req : credentialRequirement.getAltRequisites()) {
                if (req.getId().equals(this.requisiteId)) {
                    return (true);
                }
            }

            return (false);
        }
    }        

    
    public static class LearningObjectiveRequirementFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilter {

        private final org.osid.id.Id learningObjectiveRequirementId;

        
        /**
         *  Constructs a new
         *  <code>LearningObjectiveRequisiteFilter</code>.
         *
         *  @param learningObjectiveRequirementId the learning
         *         objective requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>learningObjectiveRequirementId</code> is
         *          <code>null</code>
         */

        public LearningObjectiveRequirementFilter(org.osid.id.Id learningObjectiveRequirementId) {
            nullarg(learningObjectiveRequirementId, "learning objective requisite Id");
            this.learningObjectiveRequirementId = learningObjectiveRequirementId;
            return;
        }


        /**
         *  Used by the RequisiteFilterList to filter the requisite
         *  list based on learning objective requisite.
         *
         *  @param requisite the requisite
         *  @return <code>true</code> to pass the requisite,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.Requisite requisite) {
            if (requisite.hasLearningObjectiveRequirements()) {
                for (org.osid.course.requisite.LearningObjectiveRequirement req : requisite.getLearningObjectiveRequirements()) {
                    if (req.getId().equals(this.learningObjectiveRequirementId)) {
                        return (true);
                    }
                }
            }

            return (false);
        }
    }        


    public static class LearningObjectiveFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.learningobjectiverequirement.LearningObjectiveRequirementFilter {

        private final org.osid.id.Id learningObjectiveId;

        
        /**
         *  Constructs a new <code>LearningObjectiveFilter</code>.
         *
         *  @param learningObjectiveId the learning objective to filter
         *  @throws org.osid.NullArgumentException
         *          <code>learningObjectiveId</code> is <code>null</code>
         */

        public LearningObjectiveFilter(org.osid.id.Id learningObjectiveId) {
            nullarg(learningObjectiveId, "learning objective Id");
            this.learningObjectiveId = learningObjectiveId;
            return;
        }


        /**
         *  Used by the LearningObjectiveRequirementFilterList to
         *  filter the learning objective requirement list based on
         *  learning objective.
         *
         *  @param learningObjectiveRequirement the learning objective
         *         requirement
         *  @return <code>true</code> to pass the learning objective
         *          requirement, <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.LearningObjectiveRequirement learningObjectiveRequirement) {
            return (learningObjectiveRequirement.getLearningObjectiveId().equals(this.learningObjectiveId));
        }
    }        


    public static class LearningObjectiveAltRequisiteFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.learningobjectiverequirement.LearningObjectiveRequirementFilter {

        private final org.osid.id.Id requisiteId;;

        
        /**
         *  Constructs a new
         *  <code>LearningObjectiveAltRequisiteFilter</code>.
         *
         *  @param requisiteId the requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>requisiteId</code> is <code>null</code>
         */

        public LearningObjectiveAltRequisiteFilter(org.osid.id.Id requisiteId) {
            nullarg(requisiteId, "requisite Id");
            this.requisiteId = requisiteId;
            return;
        }


        /**
         *  Used by the LearningObjectiveRequirementFilterList to
         *  filter the learning objective requirement list based on
         *  alt requisite.
         *
         *  @param learningObjectiveRequirement the learning objective requirement
         *  @return <code>true</code> to pass the learningObjective
         *          requirement, <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.LearningObjectiveRequirement learningObjectiveRequirement) {
            for (org.osid.course.requisite.Requisite req : learningObjectiveRequirement.getAltRequisites()) {
                if (req.getId().equals(this.requisiteId)) {
                    return (true);
                }
            }

            return (false);
        }
    }        


    public static class AssessmentFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.assessmentrequirement.AssessmentRequirementFilter {

        private final org.osid.id.Id assessmentId;

        
        /**
         *  Constructs a new <code>AssessmentFilter</code>.
         *
         *  @param assessmentId the assessment to filter
         *  @throws org.osid.NullArgumentException
         *          <code>assessmentId</code> is <code>null</code>
         */

        public AssessmentFilter(org.osid.id.Id assessmentId) {
            nullarg(assessmentId, "assessment Id");
            this.assessmentId = assessmentId;
            return;
        }


        /**
         *  Used by the AssessmentRequirementFilterList to filter the
         *  assessment requirement list based on assessment.
         *
         *  @param assessmentRequirement the assessment requirement
         *  @return <code>true</code> to pass the assessment requirement,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.AssessmentRequirement assessmentRequirement) {
            return (assessmentRequirement.getAssessmentId().equals(this.assessmentId));
        }
    }        


    public static class AssessmentAltRequisiteFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.assessmentrequirement.AssessmentRequirementFilter {

        private final org.osid.id.Id requisiteId;;

        
        /**
         *  Constructs a new
         *  <code>AssessmentAltRequisiteFilter</code>.
         *
         *  @param requisiteId the requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>requisiteId</code> is <code>null</code>
         */

        public AssessmentAltRequisiteFilter(org.osid.id.Id requisiteId) {
            nullarg(requisiteId, "requisite Id");
            this.requisiteId = requisiteId;
            return;
        }


        /**
         *  Used by the AssessmentRequirementFilterList to filter the
         *  assessment requirement list based on alt requisite.
         *
         *  @param assessmentRequirement the assessment requirement
         *  @return <code>true</code> to pass the assessment
         *          requirement, <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.AssessmentRequirement assessmentRequirement) {
            for (org.osid.course.requisite.Requisite req : assessmentRequirement.getAltRequisites()) {
                if (req.getId().equals(this.requisiteId)) {
                    return (true);
                }
            }

            return (false);
        }
    }        


    public static class AssessmentRequirementFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilter {

        private final org.osid.id.Id assessmentRequirementId;

        
        /**
         *  Constructs a new <code>AssessmentRequisiteFilter</code>.
         *
         *  @param assessmentRequirementId the assessment requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>assessmentRequirementId</code> is
         *          <code>null</code>
         */

        public AssessmentRequirementFilter(org.osid.id.Id assessmentRequirementId) {
            nullarg(assessmentRequirementId, "assessment requisite Id");
            this.assessmentRequirementId = assessmentRequirementId;
            return;
        }


        /**
         *  Used by the RequisiteFilterList to filter the requisite
         *  list based on assessment requisite.
         *
         *  @param requisite the requisite
         *  @return <code>true</code> to pass the requisite,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.Requisite requisite) { 
            if (requisite.hasAssessmentRequirements()) {
                for (org.osid.course.requisite.AssessmentRequirement req : requisite.getAssessmentRequirements()) {
                    if (req.getId().equals(this.assessmentRequirementId)) {
                        return (true);
                    }
                }
            }

            return (false);
        }
    }        


    public static class AwardFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.awardrequirement.AwardRequirementFilter {

        private final org.osid.id.Id awardId;

        
        /**
         *  Constructs a new <code>AwardFilter</code>.
         *
         *  @param awardId the award to filter
         *  @throws org.osid.NullArgumentException
         *          <code>awardId</code> is <code>null</code>
         */

        public AwardFilter(org.osid.id.Id awardId) {
            nullarg(awardId, "award Id");
            this.awardId = awardId;
            return;
        }


        /**
         *  Used by the AwardRequirementFilterList to filter the
         *  award requirement list based on award.
         *
         *  @param awardRequirement the award requirement
         *  @return <code>true</code> to pass the award requirement,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.AwardRequirement awardRequirement) {
            return (awardRequirement.getAwardId().equals(this.awardId));
        }
    }        


    public static class AwardAltRequisiteFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.awardrequirement.AwardRequirementFilter {

        private final org.osid.id.Id requisiteId;;

        
        /**
         *  Constructs a new
         *  <code>AwardAltRequisiteFilter</code>.
         *
         *  @param requisiteId the requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>requisiteId</code> is <code>null</code>
         */

        public AwardAltRequisiteFilter(org.osid.id.Id requisiteId) {
            nullarg(requisiteId, "requisite Id");
            this.requisiteId = requisiteId;
            return;
        }


        /**
         *  Used by the AwardRequirementFilterList to filter the
         *  award requirement list based on alt requisite.
         *
         *  @param awardRequirement the award requirement
         *  @return <code>true</code> to pass the award
         *          requirement, <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.AwardRequirement awardRequirement) {
            for (org.osid.course.requisite.Requisite req : awardRequirement.getAltRequisites()) {
                if (req.getId().equals(this.requisiteId)) {
                    return (true);
                }
            }

            return (false);
        }
    }        


    public static class AwardRequirementFilter
        implements net.okapia.osid.jamocha.inline.filter.course.requisite.requisite.RequisiteFilter {

        private final org.osid.id.Id awardRequirementId;

        
        /**
         *  Constructs a new <code>AwardRequisiteFilter</code>.
         *
         *  @param awardRequirementId the award requisite to filter
         *  @throws org.osid.NullArgumentException
         *          <code>awardRequirementId</code> is
         *          <code>null</code>
         */

        public AwardRequirementFilter(org.osid.id.Id awardRequirementId) {
            nullarg(awardRequirementId, "award requisite Id");
            this.awardRequirementId = awardRequirementId;
            return;
        }


        /**
         *  Used by the RequisiteFilterList to filter the requisite
         *  list based on award requisite.
         *
         *  @param requisite the requisite
         *  @return <code>true</code> to pass the requisite,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.requisite.Requisite requisite) {
            if (requisite.hasAwardRequirements()) {
                for (org.osid.course.requisite.AwardRequirement req : requisite.getAwardRequirements()) {
                    if (req.getId().equals(this.awardRequirementId)) {
                        return (true);
                    }
                }
            }

            return (false);
        }
    }        
}
