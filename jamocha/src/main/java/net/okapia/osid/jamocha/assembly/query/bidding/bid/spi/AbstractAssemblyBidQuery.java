//
// AbstractAssemblyBidQuery.java
//
//     A BidQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.bidding.bid.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BidQuery that stores terms.
 */

public abstract class AbstractAssemblyBidQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.bidding.BidQuery,
               org.osid.bidding.BidQueryInspector,
               org.osid.bidding.BidSearchOrder {

    private final java.util.Collection<org.osid.bidding.records.BidQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.records.BidQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.bidding.records.BidSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBidQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBidQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the auction <code> Id </code> for this query. 
     *
     *  @param  auctionId the auction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionId(org.osid.id.Id auctionId, boolean match) {
        getAssembler().addIdTerm(getAuctionIdColumn(), auctionId, match);
        return;
    }


    /**
     *  Clears the auction <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionIdTerms() {
        getAssembler().clearTerms(getAuctionIdColumn());
        return;
    }


    /**
     *  Gets the auction <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionIdTerms() {
        return (getAssembler().getIdTerms(getAuctionIdColumn()));
    }


    /**
     *  Orders the results by auction. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAuction(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAuctionColumn(), style);
        return;
    }


    /**
     *  Gets the AuctionId column name.
     *
     * @return the column name
     */

    protected String getAuctionIdColumn() {
        return ("auction_id");
    }


    /**
     *  Tests if an <code> AuctionQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the auction query 
     *  @throws org.osid.UnimplementedException <code> supportsAuctionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQuery getAuctionQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionQuery() is false");
    }


    /**
     *  Clears the auction query terms. 
     */

    @OSID @Override
    public void clearAuctionTerms() {
        getAssembler().clearTerms(getAuctionColumn());
        return;
    }


    /**
     *  Gets the auction query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionQueryInspector[] getAuctionTerms() {
        return (new org.osid.bidding.AuctionQueryInspector[0]);
    }


    /**
     *  Tests if an auction search order is available. 
     *
     *  @return <code> true </code> if an auction search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the auction search order. 
     *
     *  @return the auction search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAuctionSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionSearchOrder getAuctionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAuctionSearchOrder() is false");
    }


    /**
     *  Gets the Auction column name.
     *
     * @return the column name
     */

    protected String getAuctionColumn() {
        return ("auction");
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBidderId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getBidderIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBidderIdTerms() {
        getAssembler().clearTerms(getBidderIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBidderIdTerms() {
        return (getAssembler().getIdTerms(getBidderIdColumn()));
    }


    /**
     *  Orders the results by bidder. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBidder(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBidderColumn(), style);
        return;
    }


    /**
     *  Gets the BidderId column name.
     *
     * @return the column name
     */

    protected String getBidderIdColumn() {
        return ("bidder_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidderQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsBidderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getBidderQuery() {
        throw new org.osid.UnimplementedException("supportsBidderQuery() is false");
    }


    /**
     *  Clears the resource query terms. 
     */

    @OSID @Override
    public void clearBidderTerms() {
        getAssembler().clearTerms(getBidderColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getBidderTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a bidder search order is available. 
     *
     *  @return <code> true </code> if a bidder search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBidderSearchOrder() {
        return (false);
    }


    /**
     *  Gets the bidder search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBidderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getBidderSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBidderSearchOrder() is false");
    }


    /**
     *  Gets the Bidder column name.
     *
     * @return the column name
     */

    protected String getBidderColumn() {
        return ("bidder");
    }


    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBiddingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getBiddingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBiddingAgentIdTerms() {
        getAssembler().clearTerms(getBiddingAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBiddingAgentIdTerms() {
        return (getAssembler().getIdTerms(getBiddingAgentIdColumn()));
    }


    /**
     *  Orders the results by bidding agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBiddingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBiddingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the BiddingAgentId column name.
     *
     * @return the column name
     */

    protected String getBiddingAgentIdColumn() {
        return ("bidding_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBiddingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getBiddingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsBiddingAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearBiddingAgentTerms() {
        getAssembler().clearTerms(getBiddingAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getBiddingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if a bidding agent search order is available. 
     *
     *  @return <code> true </code> if a bidding agent search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the bidding agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBiddingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getBiddingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBiddingAgentSearchOrder() is false");
    }


    /**
     *  Gets the BiddingAgent column name.
     *
     * @return the column name
     */

    protected String getBiddingAgentColumn() {
        return ("bidding_agent");
    }


    /**
     *  Matches bids with quantities between the given range inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchQuantity(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getQuantityColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the quantity query terms. 
     */

    @OSID @Override
    public void clearQuantityTerms() {
        getAssembler().clearTerms(getQuantityColumn());
        return;
    }


    /**
     *  Gets the quantity query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getQuantityTerms() {
        return (getAssembler().getCardinalRangeTerms(getQuantityColumn()));
    }


    /**
     *  Orders the results by the quantity. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuantity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQuantityColumn(), style);
        return;
    }


    /**
     *  Gets the Quantity column name.
     *
     * @return the column name
     */

    protected String getQuantityColumn() {
        return ("quantity");
    }


    /**
     *  Matches bids with a current bidsbetween the given range inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> currency </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCurrentBid(org.osid.financials.Currency start, 
                                org.osid.financials.Currency end, 
                                boolean match) {
        getAssembler().addCurrencyRangeTerm(getCurrentBidColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the current bid query terms. 
     */

    @OSID @Override
    public void clearCurrentBidTerms() {
        getAssembler().clearTerms(getCurrentBidColumn());
        return;
    }


    /**
     *  Gets the current bid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCurrentBidTerms() {
        return (getAssembler().getCurrencyRangeTerms(getCurrentBidColumn()));
    }


    /**
     *  Orders the results by the current bid. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCurrentBid(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCurrentBidColumn(), style);
        return;
    }


    /**
     *  Gets the CurrentBid column name.
     *
     * @return the column name
     */

    protected String getCurrentBidColumn() {
        return ("current_bid");
    }


    /**
     *  Matches bids with a max bid between the given range inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> currency </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMaximumBid(org.osid.financials.Currency start, 
                                org.osid.financials.Currency end, 
                                boolean match) {
        getAssembler().addCurrencyRangeTerm(getMaximumBidColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the max bid query terms. 
     */

    @OSID @Override
    public void clearMaximumBidTerms() {
        getAssembler().clearTerms(getMaximumBidColumn());
        return;
    }


    /**
     *  Gets the maximum bid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getMaximumBidTerms() {
        return (getAssembler().getCurrencyRangeTerms(getMaximumBidColumn()));
    }


    /**
     *  Orders the results by the maximum bid. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMaximumBid(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMaximumBidColumn(), style);
        return;
    }


    /**
     *  Gets the MaximumBid column name.
     *
     * @return the column name
     */

    protected String getMaximumBidColumn() {
        return ("maximum_bid");
    }


    /**
     *  Matches winning bids. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchWinner(boolean match) {
        getAssembler().addBooleanTerm(getWinnerColumn(), match);
        return;
    }


    /**
     *  Clears the winner query terms. 
     */

    @OSID @Override
    public void clearWinnerTerms() {
        getAssembler().clearTerms(getWinnerColumn());
        return;
    }


    /**
     *  Gets the winning bid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getWinnerTerms() {
        return (getAssembler().getBooleanTerms(getWinnerColumn()));
    }


    /**
     *  Orders the results by winning bids. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWinner(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWinnerColumn(), style);
        return;
    }


    /**
     *  Gets the Winner column name.
     *
     * @return the column name
     */

    protected String getWinnerColumn() {
        return ("winner");
    }


    /**
     *  Matches bids with a settlement amount between the given range 
     *  inclusive. 
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> or <code> 
     *          start.getCurencyType() </code> != <code> end.getCurrencyType() 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> currency </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettlementAmount(org.osid.financials.Currency start, 
                                      org.osid.financials.Currency end, 
                                      boolean match) {
        getAssembler().addCurrencyRangeTerm(getSettlementAmountColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the settlement amount query terms. 
     */

    @OSID @Override
    public void clearSettlementAmountTerms() {
        getAssembler().clearTerms(getSettlementAmountColumn());
        return;
    }


    /**
     *  Gets the settlement amount query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getSettlementAmountTerms() {
        return (getAssembler().getCurrencyRangeTerms(getSettlementAmountColumn()));
    }


    /**
     *  Orders the results by the settlement amount. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySettlementAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSettlementAmountColumn(), style);
        return;
    }


    /**
     *  Gets the SettlementAmount column name.
     *
     * @return the column name
     */

    protected String getSettlementAmountColumn() {
        return ("settlement_amount");
    }


    /**
     *  Sets the auction house <code> Id </code> for this query to match bids 
     *  assigned to auction houses. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        getAssembler().addIdTerm(getAuctionHouseIdColumn(), auctionHouseId, match);
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        getAssembler().clearTerms(getAuctionHouseIdColumn());
        return;
    }


    /**
     *  Gets the auction house <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuctionHouseIdTerms() {
        return (getAssembler().getIdTerms(getAuctionHouseIdColumn()));
    }


    /**
     *  Gets the AuctionHouseId column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseIdColumn() {
        return ("auction_house_id");
    }


    /**
     *  Tests if a <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        getAssembler().clearTerms(getAuctionHouseColumn());
        return;
    }


    /**
     *  Gets the auction house query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQueryInspector[] getAuctionHouseTerms() {
        return (new org.osid.bidding.AuctionHouseQueryInspector[0]);
    }


    /**
     *  Gets the AuctionHouse column name.
     *
     * @return the column name
     */

    protected String getAuctionHouseColumn() {
        return ("auction_house");
    }


    /**
     *  Tests if this bid supports the given record
     *  <code>Type</code>.
     *
     *  @param  bidRecordType a bid record type 
     *  @return <code>true</code> if the bidRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type bidRecordType) {
        for (org.osid.bidding.records.BidQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(bidRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  bidRecordType the bid record type 
     *  @return the bid query record 
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bidRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.BidQueryRecord getBidQueryRecord(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.BidQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(bidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bidRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  bidRecordType the bid record type 
     *  @return the bid query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bidRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.BidQueryInspectorRecord getBidQueryInspectorRecord(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.BidQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(bidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bidRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param bidRecordType the bid record type
     *  @return the bid search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bidRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.BidSearchOrderRecord getBidSearchOrderRecord(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.BidSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(bidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bidRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this bid. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param bidQueryRecord the bid query record
     *  @param bidQueryInspectorRecord the bid query inspector
     *         record
     *  @param bidSearchOrderRecord the bid search order record
     *  @param bidRecordType bid record type
     *  @throws org.osid.NullArgumentException
     *          <code>bidQueryRecord</code>,
     *          <code>bidQueryInspectorRecord</code>,
     *          <code>bidSearchOrderRecord</code> or
     *          <code>bidRecordTypebid</code> is
     *          <code>null</code>
     */
            
    protected void addBidRecords(org.osid.bidding.records.BidQueryRecord bidQueryRecord, 
                                      org.osid.bidding.records.BidQueryInspectorRecord bidQueryInspectorRecord, 
                                      org.osid.bidding.records.BidSearchOrderRecord bidSearchOrderRecord, 
                                      org.osid.type.Type bidRecordType) {

        addRecordType(bidRecordType);

        nullarg(bidQueryRecord, "bid query record");
        nullarg(bidQueryInspectorRecord, "bid query inspector record");
        nullarg(bidSearchOrderRecord, "bid search odrer record");

        this.queryRecords.add(bidQueryRecord);
        this.queryInspectorRecords.add(bidQueryInspectorRecord);
        this.searchOrderRecords.add(bidSearchOrderRecord);
        
        return;
    }
}
