//
// AbstractMapUtilityLookupSession
//
//    A simple framework for providing an Utility lookup service
//    backed by a fixed collection of utilities.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.metering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Utility lookup service backed by a
 *  fixed collection of utilities. The utilities are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Utilities</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapUtilityLookupSession
    extends net.okapia.osid.jamocha.metering.spi.AbstractUtilityLookupSession
    implements org.osid.metering.UtilityLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.metering.Utility> utilities = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.metering.Utility>());


    /**
     *  Makes an <code>Utility</code> available in this session.
     *
     *  @param  utility an utility
     *  @throws org.osid.NullArgumentException <code>utility<code>
     *          is <code>null</code>
     */

    protected void putUtility(org.osid.metering.Utility utility) {
        this.utilities.put(utility.getId(), utility);
        return;
    }


    /**
     *  Makes an array of utilities available in this session.
     *
     *  @param  utilities an array of utilities
     *  @throws org.osid.NullArgumentException <code>utilities<code>
     *          is <code>null</code>
     */

    protected void putUtilities(org.osid.metering.Utility[] utilities) {
        putUtilities(java.util.Arrays.asList(utilities));
        return;
    }


    /**
     *  Makes a collection of utilities available in this session.
     *
     *  @param  utilities a collection of utilities
     *  @throws org.osid.NullArgumentException <code>utilities<code>
     *          is <code>null</code>
     */

    protected void putUtilities(java.util.Collection<? extends org.osid.metering.Utility> utilities) {
        for (org.osid.metering.Utility utility : utilities) {
            this.utilities.put(utility.getId(), utility);
        }

        return;
    }


    /**
     *  Removes an Utility from this session.
     *
     *  @param  utilityId the <code>Id</code> of the utility
     *  @throws org.osid.NullArgumentException <code>utilityId<code> is
     *          <code>null</code>
     */

    protected void removeUtility(org.osid.id.Id utilityId) {
        this.utilities.remove(utilityId);
        return;
    }


    /**
     *  Gets the <code>Utility</code> specified by its <code>Id</code>.
     *
     *  @param  utilityId <code>Id</code> of the <code>Utility</code>
     *  @return the utility
     *  @throws org.osid.NotFoundException <code>utilityId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>utilityId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.metering.Utility utility = this.utilities.get(utilityId);
        if (utility == null) {
            throw new org.osid.NotFoundException("utility not found: " + utilityId);
        }

        return (utility);
    }


    /**
     *  Gets all <code>Utilities</code>. In plenary mode, the returned
     *  list contains all known utilities or an error
     *  results. Otherwise, the returned list may contain only those
     *  utilities that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Utilities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.metering.utility.ArrayUtilityList(this.utilities.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.utilities.clear();
        super.close();
        return;
    }
}
