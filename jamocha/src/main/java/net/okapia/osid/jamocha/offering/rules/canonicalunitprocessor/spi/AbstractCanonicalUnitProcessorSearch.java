//
// AbstractCanonicalUnitProcessorSearch.java
//
//     A template for making a CanonicalUnitProcessor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing canonical unit processor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCanonicalUnitProcessorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.offering.rules.CanonicalUnitProcessorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.offering.rules.CanonicalUnitProcessorSearchOrder canonicalUnitProcessorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of canonical unit processors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  canonicalUnitProcessorIds list of canonical unit processors
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongCanonicalUnitProcessors(org.osid.id.IdList canonicalUnitProcessorIds) {
        while (canonicalUnitProcessorIds.hasNext()) {
            try {
                this.ids.add(canonicalUnitProcessorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongCanonicalUnitProcessors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of canonical unit processor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCanonicalUnitProcessorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  canonicalUnitProcessorSearchOrder canonical unit processor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>canonicalUnitProcessorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCanonicalUnitProcessorResults(org.osid.offering.rules.CanonicalUnitProcessorSearchOrder canonicalUnitProcessorSearchOrder) {
	this.canonicalUnitProcessorSearchOrder = canonicalUnitProcessorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.offering.rules.CanonicalUnitProcessorSearchOrder getCanonicalUnitProcessorSearchOrder() {
	return (this.canonicalUnitProcessorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given canonical unit processor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a canonical unit processor implementing the requested record.
     *
     *  @param canonicalUnitProcessorSearchRecordType a canonical unit processor search record
     *         type
     *  @return the canonical unit processor search record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorSearchRecord getCanonicalUnitProcessorSearchRecord(org.osid.type.Type canonicalUnitProcessorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.offering.rules.records.CanonicalUnitProcessorSearchRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit processor search. 
     *
     *  @param canonicalUnitProcessorSearchRecord canonical unit processor search record
     *  @param canonicalUnitProcessorSearchRecordType canonicalUnitProcessor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitProcessorSearchRecord(org.osid.offering.rules.records.CanonicalUnitProcessorSearchRecord canonicalUnitProcessorSearchRecord, 
                                           org.osid.type.Type canonicalUnitProcessorSearchRecordType) {

        addRecordType(canonicalUnitProcessorSearchRecordType);
        this.records.add(canonicalUnitProcessorSearchRecord);        
        return;
    }
}
