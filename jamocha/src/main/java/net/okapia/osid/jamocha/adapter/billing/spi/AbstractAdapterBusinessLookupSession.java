//
// AbstractAdapterBusinessLookupSession.java
//
//    A Business lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.billing.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Business lookup session adapter.
 */

public abstract class AbstractAdapterBusinessLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.billing.BusinessLookupSession {

    private final org.osid.billing.BusinessLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBusinessLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBusinessLookupSession(org.osid.billing.BusinessLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Business} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBusinesses() {
        return (this.session.canLookupBusinesses());
    }


    /**
     *  A complete view of the {@code Business} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBusinessView() {
        this.session.useComparativeBusinessView();
        return;
    }


    /**
     *  A complete view of the {@code Business} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBusinessView() {
        this.session.usePlenaryBusinessView();
        return;
    }

     
    /**
     *  Gets the {@code Business} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Business} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Business} and
     *  retained for compatibility.
     *
     *  @param businessId {@code Id} of the {@code Business}
     *  @return the business
     *  @throws org.osid.NotFoundException {@code businessId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code businessId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness(org.osid.id.Id businessId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBusiness(businessId));
    }


    /**
     *  Gets a {@code BusinessList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  businesses specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Businesses} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *
     *  @param  businessIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Business} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code businessIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByIds(org.osid.id.IdList businessIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBusinessesByIds(businessIds));
    }


    /**
     *  Gets a {@code BusinessList} corresponding to the given
     *  business genus {@code Type} which does not include
     *  businesses of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  businessGenusType a business genus type 
     *  @return the returned {@code Business} list
     *  @throws org.osid.NullArgumentException
     *          {@code businessGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByGenusType(org.osid.type.Type businessGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBusinessesByGenusType(businessGenusType));
    }


    /**
     *  Gets a {@code BusinessList} corresponding to the given
     *  business genus {@code Type} and include any additional
     *  businesses with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  businessGenusType a business genus type 
     *  @return the returned {@code Business} list
     *  @throws org.osid.NullArgumentException
     *          {@code businessGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByParentGenusType(org.osid.type.Type businessGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBusinessesByParentGenusType(businessGenusType));
    }


    /**
     *  Gets a {@code BusinessList} containing the given
     *  business record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  businessRecordType a business record type 
     *  @return the returned {@code Business} list
     *  @throws org.osid.NullArgumentException
     *          {@code businessRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByRecordType(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBusinessesByRecordType(businessRecordType));
    }


    /**
     *  Gets a {@code BusinessList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Business} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinessesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBusinessesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Businesses}. 
     *
     *  In plenary mode, the returned list contains all known
     *  businesses or an error results. Otherwise, the returned list
     *  may contain only those businesses that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Businesses} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.BusinessList getBusinesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBusinesses());
    }
}
