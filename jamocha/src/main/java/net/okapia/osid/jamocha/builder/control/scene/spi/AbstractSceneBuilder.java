//
// AbstractScene.java
//
//     Defines a Scene builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.scene.spi;


/**
 *  Defines a <code>Scene</code> builder.
 */

public abstract class AbstractSceneBuilder<T extends AbstractSceneBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.control.scene.SceneMiter scene;


    /**
     *  Constructs a new <code>AbstractSceneBuilder</code>.
     *
     *  @param scene the scene to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSceneBuilder(net.okapia.osid.jamocha.builder.control.scene.SceneMiter scene) {
        super(scene);
        this.scene = scene;
        return;
    }


    /**
     *  Builds the scene.
     *
     *  @return the new scene
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>scene</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.control.Scene build() {
        (new net.okapia.osid.jamocha.builder.validator.control.scene.SceneValidator(getValidations())).validate(this.scene);
        return (new net.okapia.osid.jamocha.builder.control.scene.ImmutableScene(this.scene));
    }


    /**
     *  Gets the scene. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new scene
     */

    @Override
    public net.okapia.osid.jamocha.builder.control.scene.SceneMiter getMiter() {
        return (this.scene);
    }


    /**
     *  Adds a setting.
     *
     *  @param setting a setting
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>setting</code> is
     *          <code>null</code>
     */

    public T setting(org.osid.control.Setting setting) {
        getMiter().addSetting(setting);
        return (self());
    }


    /**
     *  Sets all the settings.
     *
     *  @param settings a collection of settings
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>settings</code>
     *          is <code>null</code>
     */

    public T settings(java.util.Collection<org.osid.control.Setting> settings) {
        getMiter().setSettings(settings);
        return (self());
    }


    /**
     *  Adds a Scene record.
     *
     *  @param record a scene record
     *  @param recordType the type of scene record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.control.records.SceneRecord record, org.osid.type.Type recordType) {
        getMiter().addSceneRecord(record, recordType);
        return (self());
    }
}       


