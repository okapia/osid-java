//
// AbstractRoomConstructionManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRoomConstructionManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.room.construction.RoomConstructionManager,
               org.osid.room.construction.RoomConstructionProxyManager {

    private final Types renovationRecordTypes              = new TypeRefSet();
    private final Types renovationSearchRecordTypes        = new TypeRefSet();

    private final Types projectRecordTypes                 = new TypeRefSet();
    private final Types projectSearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRoomConstructionManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRoomConstructionManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any project federation is exposed. Federation is exposed when 
     *  a specific project may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  projects appears as a single project. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of an renovation lookup service. 
     *
     *  @return <code> true </code> if renovation lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationLookup() {
        return (false);
    }


    /**
     *  Tests if querying renovationes is available. 
     *
     *  @return <code> true </code> if renovation query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationQuery() {
        return (false);
    }


    /**
     *  Tests if searching for renovationes is available. 
     *
     *  @return <code> true </code> if renovation search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a renovation administrative service for 
     *  creating and deleting renovationes. 
     *
     *  @return <code> true </code> if renovation administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a renovation notification service. 
     *
     *  @return <code> true </code> if renovation notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationNotification() {
        return (false);
    }


    /**
     *  Tests if a renovation to campus lookup session is available. 
     *
     *  @return <code> true </code> if renovation campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationCampus() {
        return (false);
    }


    /**
     *  Tests if a renovation to campus assignment session is available. 
     *
     *  @return <code> true </code> if renovation campus assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationCampusAssignment() {
        return (false);
    }


    /**
     *  Tests if a renovation smart campus session is available. 
     *
     *  @return <code> true </code> if renovation smart campus is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRenovationSmartCampus() {
        return (false);
    }


    /**
     *  Tests for the availability of an project lookup service. 
     *
     *  @return <code> true </code> if project lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectLookup() {
        return (false);
    }


    /**
     *  Tests if querying projects is available. 
     *
     *  @return <code> true </code> if project query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectQuery() {
        return (false);
    }


    /**
     *  Tests if searching for projects is available. 
     *
     *  @return <code> true </code> if project search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a project administrative service for 
     *  creating and deleting projects. 
     *
     *  @return <code> true </code> if project administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a project notification service. 
     *
     *  @return <code> true </code> if project notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectNotification() {
        return (false);
    }


    /**
     *  Tests if a project to campus lookup session is available. 
     *
     *  @return <code> true </code> if project campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectCampus() {
        return (false);
    }


    /**
     *  Tests if a project to campus assignment session is available. 
     *
     *  @return <code> true </code> if project campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectCampusAssignment() {
        return (false);
    }


    /**
     *  Tests if a project smart campus session is available. 
     *
     *  @return <code> true </code> if project smart campus is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProjectSmartCampus() {
        return (false);
    }


    /**
     *  Tests if a service to manage construction in bulk is available. 
     *
     *  @return <code> true </code> if a room construction batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomConstructionBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Renovation </code> record types. 
     *
     *  @return a list containing the supported renovation record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRenovationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.renovationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Renovation </code> record type is supported. 
     *
     *  @param  renovationRecordType a <code> Type </code> indicating a <code> 
     *          Renovation </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> renovationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRenovationRecordType(org.osid.type.Type renovationRecordType) {
        return (this.renovationRecordTypes.contains(renovationRecordType));
    }


    /**
     *  Adds support for a renovation record type.
     *
     *  @param renovationRecordType a renovation record type
     *  @throws org.osid.NullArgumentException
     *  <code>renovationRecordType</code> is <code>null</code>
     */

    protected void addRenovationRecordType(org.osid.type.Type renovationRecordType) {
        this.renovationRecordTypes.add(renovationRecordType);
        return;
    }


    /**
     *  Removes support for a renovation record type.
     *
     *  @param renovationRecordType a renovation record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>renovationRecordType</code> is <code>null</code>
     */

    protected void removeRenovationRecordType(org.osid.type.Type renovationRecordType) {
        this.renovationRecordTypes.remove(renovationRecordType);
        return;
    }


    /**
     *  Gets the supported renovation search record types. 
     *
     *  @return a list containing the supported renovation search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRenovationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.renovationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given renovation search record type is supported. 
     *
     *  @param  renovationSearchRecordType a <code> Type </code> indicating a 
     *          renovation record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          renovationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRenovationSearchRecordType(org.osid.type.Type renovationSearchRecordType) {
        return (this.renovationSearchRecordTypes.contains(renovationSearchRecordType));
    }


    /**
     *  Adds support for a renovation search record type.
     *
     *  @param renovationSearchRecordType a renovation search record type
     *  @throws org.osid.NullArgumentException
     *  <code>renovationSearchRecordType</code> is <code>null</code>
     */

    protected void addRenovationSearchRecordType(org.osid.type.Type renovationSearchRecordType) {
        this.renovationSearchRecordTypes.add(renovationSearchRecordType);
        return;
    }


    /**
     *  Removes support for a renovation search record type.
     *
     *  @param renovationSearchRecordType a renovation search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>renovationSearchRecordType</code> is <code>null</code>
     */

    protected void removeRenovationSearchRecordType(org.osid.type.Type renovationSearchRecordType) {
        this.renovationSearchRecordTypes.remove(renovationSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Project </code> record types. 
     *
     *  @return a list containing the supported project record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProjectRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.projectRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Project </code> record type is supported. 
     *
     *  @param  projectRecordType a <code> Type </code> indicating a <code> 
     *          Project </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> projectRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProjectRecordType(org.osid.type.Type projectRecordType) {
        return (this.projectRecordTypes.contains(projectRecordType));
    }


    /**
     *  Adds support for a project record type.
     *
     *  @param projectRecordType a project record type
     *  @throws org.osid.NullArgumentException
     *  <code>projectRecordType</code> is <code>null</code>
     */

    protected void addProjectRecordType(org.osid.type.Type projectRecordType) {
        this.projectRecordTypes.add(projectRecordType);
        return;
    }


    /**
     *  Removes support for a project record type.
     *
     *  @param projectRecordType a project record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>projectRecordType</code> is <code>null</code>
     */

    protected void removeProjectRecordType(org.osid.type.Type projectRecordType) {
        this.projectRecordTypes.remove(projectRecordType);
        return;
    }


    /**
     *  Gets the supported project search record types. 
     *
     *  @return a list containing the supported project search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProjectSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.projectSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given project search record type is supported. 
     *
     *  @param  projectSearchRecordType a <code> Type </code> indicating a 
     *          project record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> projectSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProjectSearchRecordType(org.osid.type.Type projectSearchRecordType) {
        return (this.projectSearchRecordTypes.contains(projectSearchRecordType));
    }


    /**
     *  Adds support for a project search record type.
     *
     *  @param projectSearchRecordType a project search record type
     *  @throws org.osid.NullArgumentException
     *  <code>projectSearchRecordType</code> is <code>null</code>
     */

    protected void addProjectSearchRecordType(org.osid.type.Type projectSearchRecordType) {
        this.projectSearchRecordTypes.add(projectSearchRecordType);
        return;
    }


    /**
     *  Removes support for a project search record type.
     *
     *  @param projectSearchRecordType a project search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>projectSearchRecordType</code> is <code>null</code>
     */

    protected void removeProjectSearchRecordType(org.osid.type.Type projectSearchRecordType) {
        this.projectSearchRecordTypes.remove(projectSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  lookup service. 
     *
     *  @return a <code> RenovationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationLookupSession getRenovationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RenovationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationLookupSession getRenovationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  lookup service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Renovation 
     *          </code> 
     *  @return a <code> RenovationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationLookupSession getRenovationLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  lookup service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RenovationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationLookupSession getRenovationLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  query service. 
     *
     *  @return a <code> RenovationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationQuerySession getRenovationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RenovationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationQuerySession getRenovationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  query service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Renovation 
     *          </code> 
     *  @return a <code> RenovationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationQuerySession getRenovationQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  query service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RenovationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Renovation </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationQuerySession getRenovationQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  search service. 
     *
     *  @return a <code> RenovationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSearchSession getRenovationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RenovationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSearchSession getRenovationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  search service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Renovation 
     *          </code> 
     *  @return a <code> RenovationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSearchSession getRenovationSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  search service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RenovationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Renovation </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSearchSession getRenovationSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  administrative service. 
     *
     *  @return a <code> RenovationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationAdminSession getRenovationAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RenovationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationAdminSession getRenovationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Renovation 
     *          </code> 
     *  @return a <code> RenovationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationAdminSession getRenovationAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RenovationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Renovation </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationAdminSession getRenovationAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  notification service. 
     *
     *  @param  renovationReceiver the receiver 
     *  @return a <code> RenovationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> renovationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationNotificationSession getRenovationNotificationSession(org.osid.room.construction.RenovationReceiver renovationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  notification service. 
     *
     *  @param  renovationReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RenovationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> renovationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationNotificationSession getRenovationNotificationSession(org.osid.room.construction.RenovationReceiver renovationReceiver, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  notification service for the given campus. 
     *
     *  @param  renovationReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> RenovationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> renovationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationNotificationSession getRenovationNotificationSessionForCampus(org.osid.room.construction.RenovationReceiver renovationReceiver, 
                                                                                                              org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the renovation 
     *  notification service for the given campus. 
     *
     *  @param  renovationReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RenovationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Renovation </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> renovationReceiver, 
     *          campusId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationNotificationSession getRenovationNotificationSessionForCampus(org.osid.room.construction.RenovationReceiver renovationReceiver, 
                                                                                                              org.osid.id.Id campusId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the session for retrieving renovation to campus mappings. 
     *
     *  @return a <code> RenovationCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationCampusSession getRenovationCampusSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationCampusSession not implemented");
    }


    /**
     *  Gets the session for retrieving renovation to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RenovationCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationCampusSession getRenovationCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationCampusSession not implemented");
    }


    /**
     *  Gets the session for assigning renovation to campus mappings. 
     *
     *  @return a <code> RenovationCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationCampusAssignmentSession getRenovationCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning renovation to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RenovationCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationCampusAssignmentSession getRenovationCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the renovation smart campus for the 
     *  given campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> RenovationSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSmartCampus() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSmartCampusSession getRenovationSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRenovationSmartCampusSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic renovation campuses for the 
     *  given campus. 
     *
     *  @param  campusId the <code> Id </code> of a campus 
     *  @param  proxy a proxy 
     *  @return a <code> RenovationSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRenovationSmartCampus() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationSmartCampusSession getRenovationSmartCampusSession(org.osid.id.Id campusId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRenovationSmartCampusSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project lookup 
     *  service. 
     *
     *  @return a <code> ProjectLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectLookupSession getProjectLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProjectLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectLookupSession getProjectLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Project </code> 
     *  @return a <code> ProjectLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectLookupSession getProjectLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProjectLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectLookupSession getProjectLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project query 
     *  service. 
     *
     *  @return a <code> ProjectQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectQuerySession getProjectQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProjectQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectQuerySession getProjectQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Project </code> 
     *  @return a <code> ProjectQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectQuerySession getProjectQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProjectQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Project </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectQuerySession getProjectQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project search 
     *  service. 
     *
     *  @return a <code> ProjectSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSearchSession getProjectSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProjectSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSearchSession getProjectSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Project </code> 
     *  @return a <code> ProjectSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSearchSession getProjectSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProjectSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Project </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSearchSession getProjectSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  administrative service. 
     *
     *  @return a <code> ProjectAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectAdminSession getProjectAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProjectAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectAdminSession getProjectAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Project </code> 
     *  @return a <code> ProjectAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectAdminSession getProjectAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProjectAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Project </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectAdminSession getProjectAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  notification service. 
     *
     *  @param  ProjectReceiver the receiver 
     *  @return a <code> ProjectNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ProjectReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectNotificationSession getProjectNotificationSession(org.osid.room.construction.ProjectReceiver ProjectReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  notification service. 
     *
     *  @param  ProjectReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProjectNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ProjectReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectNotificationSession getProjectNotificationSession(org.osid.room.construction.ProjectReceiver ProjectReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  notification service for the given campus. 
     *
     *  @param  ProjectReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> ProjectNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ProjectReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectNotificationSession getProjectNotificationSessionForCampus(org.osid.room.construction.ProjectReceiver ProjectReceiver, 
                                                                                                        org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the project 
     *  notification service for the given campus. 
     *
     *  @param  ProjectReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProjectNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Project </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ProjectReceiver, 
     *          campusId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectNotificationSession getProjectNotificationSessionForCampus(org.osid.room.construction.ProjectReceiver ProjectReceiver, 
                                                                                                        org.osid.id.Id campusId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the session for retrieving project to campus mappings. 
     *
     *  @return a <code> ProjectCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectCampusSession getProjectCampusSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectCampusSession not implemented");
    }


    /**
     *  Gets the session for retrieving project to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProjectCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProjectCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectCampusSession getProjectCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectCampusSession not implemented");
    }


    /**
     *  Gets the session for assigning project to campus mappings. 
     *
     *  @return a <code> ProjectCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectCampusAssignmentSession getProjectCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning project to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProjectCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectCampusAssignmentSession getProjectCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the project smart campus for the 
     *  given campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> ProjectSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSmartCampusSession getProjectSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getProjectSmartCampusSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic project campuses for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of a campus 
     *  @param  proxy a proxy 
     *  @return a <code> ProjectSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProjectSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.ProjectSmartCampusSession getProjectSmartCampusSession(org.osid.id.Id campusId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getProjectSmartCampusSession not implemented");
    }


    /**
     *  Gets a <code> RoomConstructionBatchManager. </code> 
     *
     *  @return a <code> RoomConstructionBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomBatchConstruction() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.batch.RoomConstructionBatchManager getRoomConstructionBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionManager.getRoomConstructionBatchManager not implemented");
    }


    /**
     *  Gets a <code> RoomConstructionBatchProxyManager. </code> 
     *
     *  @return a <code> RoomConstructionBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomConstructionBatch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.construction.batch.RoomConstructionBatchProxyManager getRoomConstructionBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.construction.RoomConstructionProxyManager.getRoomConstructionBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.renovationRecordTypes.clear();
        this.renovationRecordTypes.clear();

        this.renovationSearchRecordTypes.clear();
        this.renovationSearchRecordTypes.clear();

        this.projectRecordTypes.clear();
        this.projectRecordTypes.clear();

        this.projectSearchRecordTypes.clear();
        this.projectSearchRecordTypes.clear();

        return;
    }
}
