//
// AbstractRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.rules.RulesManager,
               org.osid.rules.RulesProxyManager {

    private final Types ruleRecordTypes                    = new TypeRefSet();
    private final Types ruleSearchRecordTypes              = new TypeRefSet();

    private final Types engineRecordTypes                  = new TypeRefSet();
    private final Types engineSearchRecordTypes            = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any engine federation is exposed. Federation is exposed when 
     *  a specific engine may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  engines appears as a single engine. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if rule evaluation is supported. 
     *
     *  @return <code> true </code> if rule evaluation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRules() {
        return (false);
    }


    /**
     *  Tests for the availability of a rule lookup service. 
     *
     *  @return <code> true </code> if rule lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleLookup() {
        return (false);
    }


    /**
     *  Tests if querying rules is available. 
     *
     *  @return <code> true </code> if rule query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleQuery() {
        return (false);
    }


    /**
     *  Tests if searching for rules is available. 
     *
     *  @return <code> true </code> if rule search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleSearch() {
        return (false);
    }


    /**
     *  Tests if managing rules is available. 
     *
     *  @return <code> true </code> if rule admin is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleAdmin() {
        return (false);
    }


    /**
     *  Tests if rule notification is available. 
     *
     *  @return <code> true </code> if rule notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleNotification() {
        return (false);
    }


    /**
     *  Tests if rule cataloging is available. 
     *
     *  @return <code> true </code> if rule cataloging is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleEngine() {
        return (false);
    }


    /**
     *  Tests if a rule cataloging assignment service is supported. A rule 
     *  cataloging service maps rules to engines. 
     *
     *  @return <code> true </code> if rule cataloging is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleEngineAssignment() {
        return (false);
    }


    /**
     *  Tests if rule smart engines is available. 
     *
     *  @return <code> true </code> if rule smart engines is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleSmartEngine() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine lookup service. 
     *
     *  @return <code> true </code> if engine lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine query service. 
     *
     *  @return <code> true </code> if engine query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Tests if searching for engines is available. 
     *
     *  @return <code> true </code> if engine search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a engine administrative service for 
     *  creating and deleting engines. 
     *
     *  @return <code> true </code> if engine administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine notification service. 
     *
     *  @return <code> true </code> if engine notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine hierarchy traversal service. 
     *
     *  @return <code> true </code> if engine hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of an engine hierarchy design service. 
     *
     *  @return <code> true </code> if engine hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a rules check service. 
     *
     *  @return <code> true </code> if a rules check service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRulesCheck() {
        return (false);
    }


    /**
     *  Gets the supported <code> Rule </code> record types. 
     *
     *  @return a list containing the supported rule record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRuleRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ruleRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Rule </code> record type is supported. 
     *
     *  @param  ruleRecordType a <code> Type </code> indicating a <code> Rule 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ruleRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRuleRecordType(org.osid.type.Type ruleRecordType) {
        return (this.ruleRecordTypes.contains(ruleRecordType));
    }


    /**
     *  Adds support for a rule record type.
     *
     *  @param ruleRecordType a rule record type
     *  @throws org.osid.NullArgumentException
     *  <code>ruleRecordType</code> is <code>null</code>
     */

    protected void addRuleRecordType(org.osid.type.Type ruleRecordType) {
        this.ruleRecordTypes.add(ruleRecordType);
        return;
    }


    /**
     *  Removes support for a rule record type.
     *
     *  @param ruleRecordType a rule record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ruleRecordType</code> is <code>null</code>
     */

    protected void removeRuleRecordType(org.osid.type.Type ruleRecordType) {
        this.ruleRecordTypes.remove(ruleRecordType);
        return;
    }


    /**
     *  Gets the supported rule search record types. 
     *
     *  @return a list containing the supported rule search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRuleSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ruleSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given rule search record type is supported. 
     *
     *  @param  ruleSearchRecordType a <code> Type </code> indicating a rule 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ruleSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRuleSearchRecordType(org.osid.type.Type ruleSearchRecordType) {
        return (this.ruleSearchRecordTypes.contains(ruleSearchRecordType));
    }


    /**
     *  Adds support for a rule search record type.
     *
     *  @param ruleSearchRecordType a rule search record type
     *  @throws org.osid.NullArgumentException
     *  <code>ruleSearchRecordType</code> is <code>null</code>
     */

    protected void addRuleSearchRecordType(org.osid.type.Type ruleSearchRecordType) {
        this.ruleSearchRecordTypes.add(ruleSearchRecordType);
        return;
    }


    /**
     *  Removes support for a rule search record type.
     *
     *  @param ruleSearchRecordType a rule search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ruleSearchRecordType</code> is <code>null</code>
     */

    protected void removeRuleSearchRecordType(org.osid.type.Type ruleSearchRecordType) {
        this.ruleSearchRecordTypes.remove(ruleSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Engine </code> record types. 
     *
     *  @return a list containing the supported engine record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEngineRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.engineRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Engine </code> record type is supported. 
     *
     *  @param  engineRecordType a <code> Type </code> indicating an <code> 
     *          Engine </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> engineRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEngineRecordType(org.osid.type.Type engineRecordType) {
        return (this.engineRecordTypes.contains(engineRecordType));
    }


    /**
     *  Adds support for an engine record type.
     *
     *  @param engineRecordType an engine record type
     *  @throws org.osid.NullArgumentException
     *  <code>engineRecordType</code> is <code>null</code>
     */

    protected void addEngineRecordType(org.osid.type.Type engineRecordType) {
        this.engineRecordTypes.add(engineRecordType);
        return;
    }


    /**
     *  Removes support for an engine record type.
     *
     *  @param engineRecordType an engine record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>engineRecordType</code> is <code>null</code>
     */

    protected void removeEngineRecordType(org.osid.type.Type engineRecordType) {
        this.engineRecordTypes.remove(engineRecordType);
        return;
    }


    /**
     *  Gets the supported engine search record types. 
     *
     *  @return a list containing the supported engine search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEngineSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.engineSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given engine search record type is supported. 
     *
     *  @param  engineSearchRecordType a <code> Type </code> indicating an 
     *          engine record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> engineSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEngineSearchRecordType(org.osid.type.Type engineSearchRecordType) {
        return (this.engineSearchRecordTypes.contains(engineSearchRecordType));
    }


    /**
     *  Adds support for an engine search record type.
     *
     *  @param engineSearchRecordType an engine search record type
     *  @throws org.osid.NullArgumentException
     *  <code>engineSearchRecordType</code> is <code>null</code>
     */

    protected void addEngineSearchRecordType(org.osid.type.Type engineSearchRecordType) {
        this.engineSearchRecordTypes.add(engineSearchRecordType);
        return;
    }


    /**
     *  Removes support for an engine search record type.
     *
     *  @param engineSearchRecordType an engine search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>engineSearchRecordType</code> is <code>null</code>
     */

    protected void removeEngineSearchRecordType(org.osid.type.Type engineSearchRecordType) {
        this.engineSearchRecordTypes.remove(engineSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  evaluation service. 
     *
     *  @return a <code> RulesSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRules() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesSession getRulesSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRulesSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  evaluation service. 
     *
     *  @param  proxy the proxy 
     *  @return a <code> RulesSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRules() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesSession getRulesSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRulesSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  evaluation service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RulesSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRules() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesSession getRulesSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRulesSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  evaluation service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @param  proxy the proxy 
     *  @return a <code> RulesSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRules() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesSession getRulesSessionForEngine(org.osid.id.Id engineId, 
                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRulesSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule lookup 
     *  service. 
     *
     *  @return a <code> RuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleLookupSession getRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule lookup 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return a <code> RuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleLookupSession getRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleLookupSession getRuleLookupSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleLookupSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @param  proxy the proxy 
     *  @return a <code> RuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleLookupSession getRuleLookupSessionForEngine(org.osid.id.Id engineId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleLookupSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule query 
     *  service. 
     *
     *  @return a <code> RuleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleQuerySession getRuleQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule query 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return a <code> RuleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleQuerySession getRuleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleQuerySession getRuleQuerySessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleQuerySessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @param  proxy the proxy 
     *  @return a <code> RuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleQuerySession getRuleQuerySessionForEngine(org.osid.id.Id engineId, 
                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleQuerySessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule search 
     *  service. 
     *
     *  @return a <code> RuleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSearchSession getRuleSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule search 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return a <code> RuleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSearchSession getRuleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSearchSession getRuleSearchSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @param  proxy the proxy 
     *  @return a <code> RuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engindId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSearchSession getRuleSearchSessionForEngine(org.osid.id.Id engineId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleSearchSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  administrative service. 
     *
     *  @return a <code> RuleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleAdminSession getRuleAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  administration service. 
     *
     *  @param  proxy the proxy 
     *  @return a <code> RuleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleAdminSession getRuleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  administrative service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleAdminSession getRuleAdminSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleAdminSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  administration service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Rule </code> 
     *  @param  proxy the proxy 
     *  @return a <code> RuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engindId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleAdminSession getRuleAdminSessionForEngine(org.osid.id.Id engineId, 
                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleAdminSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  notification service. 
     *
     *  @param  ruleReceiver the receiver 
     *  @return a <code> RuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ruleReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleNotificationSession getRuleNotificationSession(org.osid.rules.RuleReceiver ruleReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  notification service. 
     *
     *  @param  ruleReceiver the receiver 
     *  @param  proxy the proxy 
     *  @return a <code> RuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ruleReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleNotificationSession getRuleNotificationSession(org.osid.rules.RuleReceiver ruleReceiver, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  notification service for the given engine. 
     *
     *  @param  ruleReceiver the receiver 
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Rule </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ruleReceiver </code> or 
     *          <code> engineId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleNotificationSession getRuleNotificationSessionForEngine(org.osid.rules.RuleReceiver ruleReceiver, 
                                                                                      org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleNotificationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  notification service for the given engine. 
     *
     *  @param  ruleReceiver the receiver 
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @param  proxy the proxy 
     *  @return a <code> RuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ruleReceiver, engineId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleNotificationSession getRuleNotificationSessionForEngine(org.osid.rules.RuleReceiver ruleReceiver, 
                                                                                      org.osid.id.Id engineId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleNotificationSessionForEngine not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup rule/engine mappings. 
     *
     *  @return a <code> RuleEngineSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleEngineSession getRuleEngineSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup rule/engine mappings. 
     *
     *  @param  proxy the proxy 
     *  @return a <code> RuleEngineSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleEngineSession getRuleEngineSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning rules to 
     *  engines. 
     *
     *  @return a <code> RuleEngineAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleEngineAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleEngineAssignmentSession getRuleEngineAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleEngineAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning rules to 
     *  engines. 
     *
     *  @param  proxy the proxy 
     *  @return a <code> RuleEngineAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleEngineAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleEngineAssignmentSession getRuleEngineAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleEngineAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule smart 
     *  engine service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleSmartEngine() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSmartEngineSession getRuleSmartEngineSession(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRuleSmartEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule smart 
     *  engine service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of the <code> Rule </code> 
     *  @param  proxy the proxy 
     *  @return a <code> RuleSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engindId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleSmartEngine() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSmartEngineSession getRuleSmartEngineSession(org.osid.id.Id engineId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRuleSmartEngineSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine lookup 
     *  service. 
     *
     *  @return an <code> EngineLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineLookupSession getEngineLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getEngineLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine lookup 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineLookupSession getEngineLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getEngineLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine query 
     *  service. 
     *
     *  @return an <code> EngineQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuerySession getEngineQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getEngineQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine query 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuerySession getEngineQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getEngineQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine search 
     *  service. 
     *
     *  @return an <code> EngineSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineSearchSession getEngineSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getEngineSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine search 
     *  service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineSearchSession getEngineSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getEngineSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  administrative service. 
     *
     *  @return an <code> EngineAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineAdminSession getEngineAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getEngineAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  administrative service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineAdminSession getEngineAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getEngineAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  notification service. 
     *
     *  @param  engineReceiver the receiver 
     *  @return an <code> EngineNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> engineReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineNotificationSession getEngineNotificationSession(org.osid.rules.EngineReceiver engineReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getEngineNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  notification service. 
     *
     *  @param  engineReceiver the receiver 
     *  @param  proxy the proxy 
     *  @return an <code> EngineNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> engineReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineNotificationSession getEngineNotificationSession(org.osid.rules.EngineReceiver engineReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getEngineNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy service. 
     *
     *  @return an <code> EngineHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineHierarchySession getEngineHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getEngineHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineHierarchySession getEngineHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getEngineHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy design service. 
     *
     *  @return an <code> EngineierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineHierarchyDesignSession getEngineHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getEngineHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy design service. 
     *
     *  @param  proxy the proxy 
     *  @return an <code> EngineHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineHierarchyDesignSession getEngineHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getEngineHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> RulesCheckManager. </code> 
     *
     *  @return a <code> RulesCheckManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRulesCheckManager() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.RulesCheckManager getRulesCheckManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesManager.getRulesCheckManager not implemented");
    }


    /**
     *  Gets the <code> RulesCheckProxyManager. </code> 
     *
     *  @return a <code> RulesCheckProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRulesCheckManager() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.RulesCheckProxyManager getRulesCheckProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.rules.RulesProxyManager.getRulesCheckProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.ruleRecordTypes.clear();
        this.ruleRecordTypes.clear();

        this.ruleSearchRecordTypes.clear();
        this.ruleSearchRecordTypes.clear();

        this.engineRecordTypes.clear();
        this.engineRecordTypes.clear();

        this.engineSearchRecordTypes.clear();
        this.engineSearchRecordTypes.clear();

        return;
    }
}
