//
// AbstractMessagingBatchProxyManager.java
//
//     An adapter for a MessagingBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.messaging.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MessagingBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMessagingBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.messaging.batch.MessagingBatchProxyManager>
    implements org.osid.messaging.batch.MessagingBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterMessagingBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMessagingBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMessagingBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMessagingBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of messages is available. 
     *
     *  @return <code> true </code> if a message bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageBatchAdmin() {
        return (getAdapteeManager().supportsMessageBatchAdmin());
    }


    /**
     *  Tests if bulk administration of receipts is available. 
     *
     *  @return <code> true </code> if a receipt bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptBatchAdmin() {
        return (getAdapteeManager().supportsReceiptBatchAdmin());
    }


    /**
     *  Tests if bulk administration of mailboxes is available. 
     *
     *  @return <code> true </code> if a mailbox bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxBatchAdmin() {
        return (getAdapteeManager().supportsMailboxBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk message 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MessageBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessageBatchAdminSession getMessageBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk message 
     *  administration service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the <code> Mailbox </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MessageBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Mailbox </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessageBatchAdminSession getMessageBatchAdminSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageBatchAdminSessionForMailbox(mailboxId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk receipt 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ReceiptBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.ReceiptBatchAdminSession getReceiptBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk receipt 
     *  administration service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the <code> Mailbox </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ReceiptBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Mailbox </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.ReceiptBatchAdminSession getReceiptBatchAdminSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptBatchAdminSessionForMailbox(mailboxId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk mailbox 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MailboxBatchAdminSession getMailboxBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
