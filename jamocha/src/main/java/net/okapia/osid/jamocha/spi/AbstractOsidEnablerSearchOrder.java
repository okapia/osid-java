//
// AbstractOsidEnablerSearchOrder.java
//
//     Defines a simple OSID search order to draw from.
//
//
// Tom Coppeto
// Okapia
// 14 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a simple OsidSearchOrder to extend. This class
 *  does nothing.
 */

public abstract class AbstractOsidEnablerSearchOrder
    extends AbstractOsidRuleSearchOrder
    implements org.osid.OsidEnablerSearchOrder {    

    private final OsidTemporalSearchOrder order = new OsidTemporalSearchOrder();

    
    /**
     *  Specifies a preference for ordering the result set by the
     *  effective status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByEffective(org.osid.SearchOrderStyle style) {
        this.order.orderByEffective(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  start date.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        this.order.orderByStartDate(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the end
     *  date.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        this.order.orderByEndDate(style);
        return;
    }

    
    /**
     *  Specifies a preference for ordering the results by the
     *  associated schedule.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderBySchedule(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a {@code ScheduleSearchOrder} is available. 
     *
     *  @return {@code true} if a schedule search order is available, 
     *          {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a schedule. 
     *
     *  @return the schedule search order 
     *  @throws org.osid.UnimplementedException {@code
     *          supportsScheduleSearchOrder() is false}
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSearchOrder getScheduleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScheduleSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the results by the
     *  associated event.
     *
     *  @param style search order style
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByEvent(org.osid.SearchOrderStyle style) {
        return;
    }

    
    /**
     *  Tests if an {@code EventSearchOrder} is available. 
     *
     *  @return {@code true} if an event search order is available, 
     *          {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for an event. 
     *
     *  @return the event search order 
     *  @throws org.osid.UnimplementedException {@code
     *          supportsEventSearchOrder() is false}
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchOrder getEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEventSearchOrder() is false");
    }


    /**
     *  Orders the results by cyclic event. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByCyclicEvent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a cyclic event search order is available. 
     *
     *  @return {@code true} if a cyclic event search order is
     *          available, {@code false} otherwise
     */

    @OSID @Override
    public boolean supportsCyclicEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the cyclic event search order. 
     *
     *  @return the cyclic event search order 
     *  @throws org.osid.IllegalStateException {@code
     *          supportsCyclicEventSearchOrder()} is {@code false}
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchOrder getCyclicEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCyclicEventSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the results by the
     *  associated demographic resource.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByDemographic(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a {@code ResourceSearchOrder} is available. 
     *
     *  @return {@code true} if a resource search order is available, 
     *          {@code false} otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a demographic resource. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException {@code
     *          supportsDemographicSearchOrder()} is {@code false}
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getDemographicSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDemographicSearchOrder() is false");
    }


    protected class OsidTemporalSearchOrder
        extends AbstractOsidTemporalSearchOrder
        implements org.osid.OsidTemporalSearchOrder {
    }    
}