//
// AbstractCompositionQuery.java
//
//     A template for making a Composition Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.composition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for compositions.
 */

public abstract class AbstractCompositionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractSourceableOsidObjectQuery
    implements org.osid.repository.CompositionQuery {

    private final java.util.Collection<org.osid.repository.records.CompositionQueryRecord> records = new java.util.ArrayList<>();
    private final OsidOperableQuery operableQuery = new OsidOperableQuery();
    private final OsidContainableQuery containableQuery = new OsidContainableQuery();

    
    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches compositions that has any asset mapping. 
     *
     *  @param  match <code> true </code> to match compositions with any 
     *          asset, <code> false </code> to match compositions with no 
     *          asset 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        return;
    }


    /**
     *  Sets the composition <code> Id </code> for this query to match 
     *  compositions that have the specified composition as an ancestor. 
     *
     *  @param  compositionId a composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingCompositionId(org.osid.id.Id compositionId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the containing composition <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingCompositionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingCompositionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getContainingCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsContainingCompositionQuery() is false");
    }


    /**
     *  Matches compositions with any ancestor. 
     *
     *  @param  match <code> true </code> to match composition with any 
     *          ancestor, <code> false </code> to match root compositions 
     */

    @OSID @Override
    public void matchAnyContainingComposition(boolean match) {
        return;
    }


    /**
     *  Clears the containing composition terms. 
     */

    @OSID @Override
    public void clearContainingCompositionTerms() {
        return;
    }


    /**
     *  Sets the composition <code> Id </code> for this query to match 
     *  compositions that contain the specified composition. 
     *
     *  @param  compositionId a composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainedCompositionId(org.osid.id.Id compositionId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the contained composition <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainedCompositionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainedCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainedCompositionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getContainedCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsContainedCompositionQuery() is false");
    }


    /**
     *  Matches compositions that contain any other compositions. 
     *
     *  @param  match <code> true </code> to match composition with any 
     *          descendant, <code> false </code> to match leaf compositions 
     */

    @OSID @Override
    public void matchAnyContainedComposition(boolean match) {
        return;
    }


    /**
     *  Clears the contained composition terms. 
     */

    @OSID @Override
    public void clearContainedCompositionTerms() {
        return;
    }


    /**
     *  Sets the repository <code> Id </code> for this query. 
     *
     *  @param  repositoryId the repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRepositoryId(org.osid.id.Id repositoryId, boolean match) {
        return;
    }


    /**
     *  Clears the repository <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRepositoryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple queries can be retrieved for 
     *  a nested <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsRepositoryQuery() is false");
    }


    /**
     *  Clears the repository terms. 
     */

    @OSID @Override
    public void clearRepositoryTerms() {
        return;
    }


    /**
     *  Matches active. 
     *
     *  @param  match <code> true </code> to match active, <code> false 
     *          </code> to match inactive 
     */

    @OSID @Override
    public void matchActive(boolean match) {
        this.operableQuery.matchActive(match);
        return;
    }


    /**
     *  Clears the active query terms. 
     */

    @OSID @Override
    public void clearActiveTerms() {
        this.operableQuery.clearActiveTerms();
        return;
    }


    /**
     *  Matches administratively enabled. 
     *
     *  @param  match <code> true </code> to match administratively enabled, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchEnabled(boolean match) {
        this.operableQuery.matchEnabled(match);
        return;
    }


    /**
     *  Clears the administratively enabled query terms. 
     */

    @OSID @Override
    public void clearEnabledTerms() {
        this.operableQuery.clearEnabledTerms();
        return;
    }


    /**
     *  Matches administratively disabled. 
     *
     *  @param  match <code> true </code> to match administratively disabled, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchDisabled(boolean match) {
        this.operableQuery.matchDisabled(match);
        return;
    }


    /**
     *  Clears the administratively disabled query terms. 
     */

    @OSID @Override
    public void clearDisabledTerms() {
        this.operableQuery.clearEnabledTerms();
        return;
    }


    /**
     *  Matches operational operables. 
     *
     *  @param  match <code> true </code> to match operational, <code> false 
     *          </code> to match not operational 
     */

    @OSID @Override
    public void matchOperational(boolean match) {
        this.operableQuery.matchOperational(match);
        return;
    }


    /**
     *  Clears the operational query terms. 
     */

    @OSID @Override
    public void clearOperationalTerms() {
        this.operableQuery.clearOperationalTerms();
        return;
    }


    /**
     *  Match containables that are sequestered.
     *
     *  @param  match <code> true </code> to match any sequestered
     *          containables, <code> false </code> to match non-sequestered
     *          containables
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.containableQuery.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms.
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.containableQuery.clearSequesteredTerms();
        return;
    }


    /**
     *  Gets the record corresponding to the given composition query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a composition implementing the requested record.
     *
     *  @param compositionRecordType a composition record type
     *  @return the composition query record
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.records.CompositionQueryRecord getCompositionQueryRecord(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.records.CompositionQueryRecord record : this.records) {
            if (record.implementsRecordType(compositionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this composition query. 
     *
     *  @param compositionQueryRecord composition query record
     *  @param compositionRecordType composition record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCompositionQueryRecord(org.osid.repository.records.CompositionQueryRecord compositionQueryRecord, 
                                             org.osid.type.Type compositionRecordType) {
        
        addRecordType(compositionRecordType);
        nullarg(compositionQueryRecord, "composition query record");
        this.records.add(compositionQueryRecord);        
        return;
    }


    protected class OsidOperableQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidOperableQuery
        implements org.osid.OsidOperableQuery {
    }


    protected class OsidContainableQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQuery
        implements org.osid.OsidContainableQuery {
    }    
}
