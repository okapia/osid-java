//
// MutableMapProxyDistributorLookupSession
//
//    Implements a Distributor lookup service backed by a collection of
//    distributors that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Distributor lookup service backed by a collection of
 *  distributors. The distributors are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of distributors can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyDistributorLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapDistributorLookupSession
    implements org.osid.provisioning.DistributorLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDistributorLookupSession} with no
     *  distributors.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyDistributorLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyDistributorLookupSession} with a
     *  single distributor.
     *
     *  @param distributor a distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDistributorLookupSession(org.osid.provisioning.Distributor distributor, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDistributorLookupSession} using an
     *  array of distributors.
     *
     *  @param distributors an array of distributors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributors} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDistributorLookupSession(org.osid.provisioning.Distributor[] distributors, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDistributors(distributors);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyDistributorLookupSession} using
     *  a collection of distributors.
     *
     *  @param distributors a collection of distributors
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributors} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyDistributorLookupSession(java.util.Collection<? extends org.osid.provisioning.Distributor> distributors,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putDistributors(distributors);
        return;
    }

    
    /**
     *  Makes a {@code Distributor} available in this session.
     *
     *  @param distributor an distributor
     *  @throws org.osid.NullArgumentException {@code distributor{@code 
     *          is {@code null}
     */

    @Override
    public void putDistributor(org.osid.provisioning.Distributor distributor) {
        super.putDistributor(distributor);
        return;
    }


    /**
     *  Makes an array of distributors available in this session.
     *
     *  @param distributors an array of distributors
     *  @throws org.osid.NullArgumentException {@code distributors{@code 
     *          is {@code null}
     */

    @Override
    public void putDistributors(org.osid.provisioning.Distributor[] distributors) {
        super.putDistributors(distributors);
        return;
    }


    /**
     *  Makes collection of distributors available in this session.
     *
     *  @param distributors
     *  @throws org.osid.NullArgumentException {@code distributor{@code 
     *          is {@code null}
     */

    @Override
    public void putDistributors(java.util.Collection<? extends org.osid.provisioning.Distributor> distributors) {
        super.putDistributors(distributors);
        return;
    }


    /**
     *  Removes a Distributor from this session.
     *
     *  @param distributorId the {@code Id} of the distributor
     *  @throws org.osid.NullArgumentException {@code distributorId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDistributor(org.osid.id.Id distributorId) {
        super.removeDistributor(distributorId);
        return;
    }    
}
