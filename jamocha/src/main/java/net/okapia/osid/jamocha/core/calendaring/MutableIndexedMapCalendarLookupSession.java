//
// MutableIndexedMapCalendarLookupSession
//
//    Implements a Calendar lookup service backed by a collection of
//    calendars indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a Calendar lookup service backed by a collection of
 *  calendars. The calendars are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some calendars may be compatible
 *  with more types than are indicated through these calendar
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of calendars can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCalendarLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractIndexedMapCalendarLookupSession
    implements org.osid.calendaring.CalendarLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCalendarLookupSession} with no
     *  calendars.
     */

    public MutableIndexedMapCalendarLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCalendarLookupSession} with a
     *  single calendar.
     *  
     *  @param  calendar a single calendar
     *  @throws org.osid.NullArgumentException {@code calendar}
     *          is {@code null}
     */

    public MutableIndexedMapCalendarLookupSession(org.osid.calendaring.Calendar calendar) {
        putCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCalendarLookupSession} using an
     *  array of calendars.
     *
     *  @param  calendars an array of calendars
     *  @throws org.osid.NullArgumentException {@code calendars}
     *          is {@code null}
     */

    public MutableIndexedMapCalendarLookupSession(org.osid.calendaring.Calendar[] calendars) {
        putCalendars(calendars);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCalendarLookupSession} using a
     *  collection of calendars.
     *
     *  @param  calendars a collection of calendars
     *  @throws org.osid.NullArgumentException {@code calendars} is
     *          {@code null}
     */

    public MutableIndexedMapCalendarLookupSession(java.util.Collection<? extends org.osid.calendaring.Calendar> calendars) {
        putCalendars(calendars);
        return;
    }
    

    /**
     *  Makes a {@code Calendar} available in this session.
     *
     *  @param  calendar a calendar
     *  @throws org.osid.NullArgumentException {@code calendar{@code  is
     *          {@code null}
     */

    @Override
    public void putCalendar(org.osid.calendaring.Calendar calendar) {
        super.putCalendar(calendar);
        return;
    }


    /**
     *  Makes an array of calendars available in this session.
     *
     *  @param  calendars an array of calendars
     *  @throws org.osid.NullArgumentException {@code calendars{@code 
     *          is {@code null}
     */

    @Override
    public void putCalendars(org.osid.calendaring.Calendar[] calendars) {
        super.putCalendars(calendars);
        return;
    }


    /**
     *  Makes collection of calendars available in this session.
     *
     *  @param  calendars a collection of calendars
     *  @throws org.osid.NullArgumentException {@code calendar{@code  is
     *          {@code null}
     */

    @Override
    public void putCalendars(java.util.Collection<? extends org.osid.calendaring.Calendar> calendars) {
        super.putCalendars(calendars);
        return;
    }


    /**
     *  Removes a Calendar from this session.
     *
     *  @param calendarId the {@code Id} of the calendar
     *  @throws org.osid.NullArgumentException {@code calendarId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCalendar(org.osid.id.Id calendarId) {
        super.removeCalendar(calendarId);
        return;
    }    
}
