//
// ProgramElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.program.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ProgramElements
    extends net.okapia.osid.jamocha.spi.OperableOsidObjectElements {


    /**
     *  Gets the ProgramElement Id.
     *
     *  @return the program element Id
     */

    public static org.osid.id.Id getProgramEntityId() {
        return (makeEntityId("osid.course.program.Program"));
    }


    /**
     *  Gets the Title element Id.
     *
     *  @return the Title element Id
     */

    public static org.osid.id.Id getTitle() {
        return (makeElementId("osid.course.program.program.Title"));
    }


    /**
     *  Gets the Number element Id.
     *
     *  @return the Number element Id
     */

    public static org.osid.id.Id getNumber() {
        return (makeElementId("osid.course.program.program.Number"));
    }


    /**
     *  Gets the SponsorIds element Id.
     *
     *  @return the SponsorIds element Id
     */

    public static org.osid.id.Id getSponsorIds() {
        return (makeElementId("osid.course.program.program.SponsorIds"));
    }


    /**
     *  Gets the Sponsors element Id.
     *
     *  @return the Sponsors element Id
     */

    public static org.osid.id.Id getSponsors() {
        return (makeElementId("osid.course.program.program.Sponsors"));
    }


    /**
     *  Gets the CompletionRequirementsInfo element Id.
     *
     *  @return the CompletionRequirementsInfo element Id
     */

    public static org.osid.id.Id getCompletionRequirementsInfo() {
        return (makeElementId("osid.course.program.program.CompletionRequirementsInfo"));
    }


    /**
     *  Gets the CompletionRequirementIds element Id.
     *
     *  @return the CompletionRequirementIds element Id
     */

    public static org.osid.id.Id getCompletionRequirementIds() {
        return (makeElementId("osid.course.program.program.CompletionRequirementIds"));
    }


    /**
     *  Gets the CompletionRequirements element Id.
     *
     *  @return the CompletionRequirements element Id
     */

    public static org.osid.id.Id getCompletionRequirements() {
        return (makeElementId("osid.course.program.program.CompletionRequirements"));
    }


    /**
     *  Gets the CredentialIds element Id.
     *
     *  @return the CredentialIds element Id
     */

    public static org.osid.id.Id getCredentialIds() {
        return (makeElementId("osid.course.program.program.CredentialIds"));
    }


    /**
     *  Gets the Credentials element Id.
     *
     *  @return the Credentials element Id
     */

    public static org.osid.id.Id getCredentials() {
        return (makeElementId("osid.course.program.program.Credentials"));
    }


    /**
     *  Gets the ProgramOfferingId element Id.
     *
     *  @return the ProgramOfferingId element Id
     */

    public static org.osid.id.Id getProgramOfferingId() {
        return (makeQueryElementId("osid.course.program.program.ProgramOfferingId"));
    }


    /**
     *  Gets the ProgramOffering element Id.
     *
     *  @return the ProgramOffering element Id
     */

    public static org.osid.id.Id getProgramOffering() {
        return (makeQueryElementId("osid.course.program.program.ProgramOffering"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.program.program.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.program.program.CourseCatalog"));
    }
}
