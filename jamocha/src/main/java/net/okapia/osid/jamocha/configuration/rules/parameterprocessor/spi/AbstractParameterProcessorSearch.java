//
// AbstractParameterProcessorSearch.java
//
//     A template for making a ParameterProcessor Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.parameterprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing parameter processor searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractParameterProcessorSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.configuration.rules.ParameterProcessorSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.configuration.rules.records.ParameterProcessorSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.configuration.rules.ParameterProcessorSearchOrder parameterProcessorSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of parameter processors. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  parameterProcessorIds list of parameter processors
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongParameterProcessors(org.osid.id.IdList parameterProcessorIds) {
        while (parameterProcessorIds.hasNext()) {
            try {
                this.ids.add(parameterProcessorIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongParameterProcessors</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of parameter processor Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getParameterProcessorIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  parameterProcessorSearchOrder parameter processor search order 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>parameterProcessorSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderParameterProcessorResults(org.osid.configuration.rules.ParameterProcessorSearchOrder parameterProcessorSearchOrder) {
	this.parameterProcessorSearchOrder = parameterProcessorSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.configuration.rules.ParameterProcessorSearchOrder getParameterProcessorSearchOrder() {
	return (this.parameterProcessorSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given parameter processor search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a parameter processor implementing the requested record.
     *
     *  @param parameterProcessorSearchRecordType a parameter processor search record
     *         type
     *  @return the parameter processor search record
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterProcessorSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.rules.records.ParameterProcessorSearchRecord getParameterProcessorSearchRecord(org.osid.type.Type parameterProcessorSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.configuration.rules.records.ParameterProcessorSearchRecord record : this.records) {
            if (record.implementsRecordType(parameterProcessorSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter processor search. 
     *
     *  @param parameterProcessorSearchRecord parameter processor search record
     *  @param parameterProcessorSearchRecordType parameterProcessor search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addParameterProcessorSearchRecord(org.osid.configuration.rules.records.ParameterProcessorSearchRecord parameterProcessorSearchRecord, 
                                           org.osid.type.Type parameterProcessorSearchRecordType) {

        addRecordType(parameterProcessorSearchRecordType);
        this.records.add(parameterProcessorSearchRecord);        
        return;
    }
}
