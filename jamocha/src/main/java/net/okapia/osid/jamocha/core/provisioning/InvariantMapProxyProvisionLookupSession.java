//
// InvariantMapProxyProvisionLookupSession
//
//    Implements a Provision lookup service backed by a fixed
//    collection of provisions. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a Provision lookup service backed by a fixed
 *  collection of provisions. The provisions are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyProvisionLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapProvisionLookupSession
    implements org.osid.provisioning.ProvisionLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProvisionLookupSession} with no
     *  provisions.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyProvisionLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyProvisionLookupSession} with a single
     *  provision.
     *
     *  @param distributor the distributor
     *  @param provision a single provision
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provision} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProvisionLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.Provision provision, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putProvision(provision);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyProvisionLookupSession} using
     *  an array of provisions.
     *
     *  @param distributor the distributor
     *  @param provisions an array of provisions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provisions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProvisionLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.provisioning.Provision[] provisions, org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putProvisions(provisions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyProvisionLookupSession} using a
     *  collection of provisions.
     *
     *  @param distributor the distributor
     *  @param provisions a collection of provisions
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code provisions} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyProvisionLookupSession(org.osid.provisioning.Distributor distributor,
                                                  java.util.Collection<? extends org.osid.provisioning.Provision> provisions,
                                                  org.osid.proxy.Proxy proxy) {

        this(distributor, proxy);
        putProvisions(provisions);
        return;
    }
}
