//
// AbstractAssemblyObstacleEnablerQuery.java
//
//     An ObstacleEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.mapping.path.rules.obstacleenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ObstacleEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyObstacleEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.mapping.path.rules.ObstacleEnablerQuery,
               org.osid.mapping.path.rules.ObstacleEnablerQueryInspector,
               org.osid.mapping.path.rules.ObstacleEnablerSearchOrder {

    private final java.util.Collection<org.osid.mapping.path.rules.records.ObstacleEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.rules.records.ObstacleEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.path.rules.records.ObstacleEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyObstacleEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyObstacleEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to an obstacle. 
     *
     *  @param  obstacleId the obstacle <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> obstacleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledObstacleId(org.osid.id.Id obstacleId, boolean match) {
        getAssembler().addIdTerm(getRuledObstacleIdColumn(), obstacleId, match);
        return;
    }


    /**
     *  Clears the obstacle <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledObstacleIdTerms() {
        getAssembler().clearTerms(getRuledObstacleIdColumn());
        return;
    }


    /**
     *  Gets the obstacle <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledObstacleIdTerms() {
        return (getAssembler().getIdTerms(getRuledObstacleIdColumn()));
    }


    /**
     *  Gets the RuledObstacleId column name.
     *
     * @return the column name
     */

    protected String getRuledObstacleIdColumn() {
        return ("ruled_obstacle_id");
    }


    /**
     *  Tests if an <code> ObstacleQuery </code> is available. 
     *
     *  @return <code> true </code> if an obstacle query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledObstacleQuery() {
        return (false);
    }


    /**
     *  Gets the query for an obstacle. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the obstacle query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledObstacleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQuery getRuledObstacleQuery() {
        throw new org.osid.UnimplementedException("supportsRuledObstacleQuery() is false");
    }


    /**
     *  Matches rules mapped to any obstacle. 
     *
     *  @param  match <code> true </code> for rules mapped to any obstacle, 
     *          <code> false </code> to match rules mapped to no obstacles 
     */

    @OSID @Override
    public void matchAnyRuledObstacle(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledObstacleColumn(), match);
        return;
    }


    /**
     *  Clears the obstacle query terms. 
     */

    @OSID @Override
    public void clearRuledObstacleTerms() {
        getAssembler().clearTerms(getRuledObstacleColumn());
        return;
    }


    /**
     *  Gets the obstacle query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQueryInspector[] getRuledObstacleTerms() {
        return (new org.osid.mapping.path.ObstacleQueryInspector[0]);
    }


    /**
     *  Gets the RuledObstacle column name.
     *
     * @return the column name
     */

    protected String getRuledObstacleColumn() {
        return ("ruled_obstacle");
    }


    /**
     *  Matches enablers mapped to an map. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        getAssembler().addIdTerm(getMapIdColumn(), mapId, match);
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        getAssembler().clearTerms(getMapIdColumn());
        return;
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (getAssembler().getIdTerms(getMapIdColumn()));
    }


    /**
     *  Gets the MapId column name.
     *
     * @return the column name
     */

    protected String getMapIdColumn() {
        return ("map_id");
    }


    /**
     *  Tests if an <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if an map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for an map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        getAssembler().clearTerms(getMapColumn());
        return;
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }


    /**
     *  Gets the Map column name.
     *
     * @return the column name
     */

    protected String getMapColumn() {
        return ("map");
    }


    /**
     *  Tests if this obstacleEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  obstacleEnablerRecordType an obstacle enabler record type 
     *  @return <code>true</code> if the obstacleEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type obstacleEnablerRecordType) {
        for (org.osid.mapping.path.rules.records.ObstacleEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(obstacleEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  obstacleEnablerRecordType the obstacle enabler record type 
     *  @return the obstacle enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.ObstacleEnablerQueryRecord getObstacleEnablerQueryRecord(org.osid.type.Type obstacleEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.ObstacleEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(obstacleEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  obstacleEnablerRecordType the obstacle enabler record type 
     *  @return the obstacle enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.ObstacleEnablerQueryInspectorRecord getObstacleEnablerQueryInspectorRecord(org.osid.type.Type obstacleEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.ObstacleEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(obstacleEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param obstacleEnablerRecordType the obstacle enabler record type
     *  @return the obstacle enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.ObstacleEnablerSearchOrderRecord getObstacleEnablerSearchOrderRecord(org.osid.type.Type obstacleEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.ObstacleEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(obstacleEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this obstacle enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param obstacleEnablerQueryRecord the obstacle enabler query record
     *  @param obstacleEnablerQueryInspectorRecord the obstacle enabler query inspector
     *         record
     *  @param obstacleEnablerSearchOrderRecord the obstacle enabler search order record
     *  @param obstacleEnablerRecordType obstacle enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerQueryRecord</code>,
     *          <code>obstacleEnablerQueryInspectorRecord</code>,
     *          <code>obstacleEnablerSearchOrderRecord</code> or
     *          <code>obstacleEnablerRecordTypeobstacleEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addObstacleEnablerRecords(org.osid.mapping.path.rules.records.ObstacleEnablerQueryRecord obstacleEnablerQueryRecord, 
                                      org.osid.mapping.path.rules.records.ObstacleEnablerQueryInspectorRecord obstacleEnablerQueryInspectorRecord, 
                                      org.osid.mapping.path.rules.records.ObstacleEnablerSearchOrderRecord obstacleEnablerSearchOrderRecord, 
                                      org.osid.type.Type obstacleEnablerRecordType) {

        addRecordType(obstacleEnablerRecordType);

        nullarg(obstacleEnablerQueryRecord, "obstacle enabler query record");
        nullarg(obstacleEnablerQueryInspectorRecord, "obstacle enabler query inspector record");
        nullarg(obstacleEnablerSearchOrderRecord, "obstacle enabler search odrer record");

        this.queryRecords.add(obstacleEnablerQueryRecord);
        this.queryInspectorRecords.add(obstacleEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(obstacleEnablerSearchOrderRecord);
        
        return;
    }
}
