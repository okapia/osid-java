//
// AbstractMapCourseOfferingLookupSession
//
//    A simple framework for providing a CourseOffering lookup service
//    backed by a fixed collection of course offerings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CourseOffering lookup service backed by a
 *  fixed collection of course offerings. The course offerings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CourseOfferings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCourseOfferingLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractCourseOfferingLookupSession
    implements org.osid.course.CourseOfferingLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.CourseOffering> courseOfferings = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.CourseOffering>());


    /**
     *  Makes a <code>CourseOffering</code> available in this session.
     *
     *  @param  courseOffering a course offering
     *  @throws org.osid.NullArgumentException <code>courseOffering<code>
     *          is <code>null</code>
     */

    protected void putCourseOffering(org.osid.course.CourseOffering courseOffering) {
        this.courseOfferings.put(courseOffering.getId(), courseOffering);
        return;
    }


    /**
     *  Makes an array of course offerings available in this session.
     *
     *  @param  courseOfferings an array of course offerings
     *  @throws org.osid.NullArgumentException <code>courseOfferings<code>
     *          is <code>null</code>
     */

    protected void putCourseOfferings(org.osid.course.CourseOffering[] courseOfferings) {
        putCourseOfferings(java.util.Arrays.asList(courseOfferings));
        return;
    }


    /**
     *  Makes a collection of course offerings available in this session.
     *
     *  @param  courseOfferings a collection of course offerings
     *  @throws org.osid.NullArgumentException <code>courseOfferings<code>
     *          is <code>null</code>
     */

    protected void putCourseOfferings(java.util.Collection<? extends org.osid.course.CourseOffering> courseOfferings) {
        for (org.osid.course.CourseOffering courseOffering : courseOfferings) {
            this.courseOfferings.put(courseOffering.getId(), courseOffering);
        }

        return;
    }


    /**
     *  Removes a CourseOffering from this session.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @throws org.osid.NullArgumentException <code>courseOfferingId<code> is
     *          <code>null</code>
     */

    protected void removeCourseOffering(org.osid.id.Id courseOfferingId) {
        this.courseOfferings.remove(courseOfferingId);
        return;
    }


    /**
     *  Gets the <code>CourseOffering</code> specified by its <code>Id</code>.
     *
     *  @param  courseOfferingId <code>Id</code> of the <code>CourseOffering</code>
     *  @return the courseOffering
     *  @throws org.osid.NotFoundException <code>courseOfferingId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.CourseOffering courseOffering = this.courseOfferings.get(courseOfferingId);
        if (courseOffering == null) {
            throw new org.osid.NotFoundException("courseOffering not found: " + courseOfferingId);
        }

        return (courseOffering);
    }


    /**
     *  Gets all <code>CourseOfferings</code>. In plenary mode, the returned
     *  list contains all known courseOfferings or an error
     *  results. Otherwise, the returned list may contain only those
     *  courseOfferings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CourseOfferings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingList getCourseOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.courseoffering.ArrayCourseOfferingList(this.courseOfferings.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.courseOfferings.clear();
        super.close();
        return;
    }
}
