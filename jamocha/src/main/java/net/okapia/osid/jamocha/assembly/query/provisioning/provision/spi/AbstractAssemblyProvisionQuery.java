//
// AbstractAssemblyProvisionQuery.java
//
//     A ProvisionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProvisionQuery that stores terms.
 */

public abstract class AbstractAssemblyProvisionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.provisioning.ProvisionQuery,
               org.osid.provisioning.ProvisionQueryInspector,
               org.osid.provisioning.ProvisionSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.ProvisionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.ProvisionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.ProvisionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProvisionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProvisionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getBrokerIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        getAssembler().clearTerms(getBrokerIdColumn());
        return;
    }


    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrokerIdTerms() {
        return (getAssembler().getIdTerms(getBrokerIdColumn()));
    }


    /**
     *  Orders the results by the broker. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBroker(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBrokerColumn(), style);
        return;
    }


    /**
     *  Gets the BrokerId column name.
     *
     * @return the column name
     */

    protected String getBrokerIdColumn() {
        return ("broker_id");
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Broker. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        getAssembler().clearTerms(getBrokerColumn());
        return;
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Tests if a broker search order is available. 
     *
     *  @return <code> true </code> if a broker search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the broker search order. 
     *
     *  @return the broker search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBrokerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchOrder getBrokerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBrokerSearchOrder() is false");
    }


    /**
     *  Gets the Broker column name.
     *
     * @return the column name
     */

    protected String getBrokerColumn() {
        return ("broker");
    }


    /**
     *  Sets the provisionable <code> Id </code> for this query. 
     *
     *  @param  provisionableId the provisionable <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionableId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchProvisionableId(org.osid.id.Id provisionableId, 
                                     boolean match) {
        getAssembler().addIdTerm(getProvisionableIdColumn(), provisionableId, match);
        return;
    }


    /**
     *  Clears the provisionable <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProvisionableIdTerms() {
        getAssembler().clearTerms(getProvisionableIdColumn());
        return;
    }


    /**
     *  Gets the provisionable <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProvisionableIdTerms() {
        return (getAssembler().getIdTerms(getProvisionableIdColumn()));
    }


    /**
     *  Orders the results by the provisionable. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProvisionable(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProvisionableColumn(), style);
        return;
    }


    /**
     *  Gets the ProvisionableId column name.
     *
     * @return the column name
     */

    protected String getProvisionableIdColumn() {
        return ("provisionable_id");
    }


    /**
     *  Tests if a <code> ProvisionableQuery </code> is available. 
     *
     *  @return <code> true </code> if a provisionable query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Provisionable. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the provisionable query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQuery getProvisionableQuery() {
        throw new org.osid.UnimplementedException("supportsProvisionableQuery() is false");
    }


    /**
     *  Clears the provisionable query terms. 
     */

    @OSID @Override
    public void clearProvisionableTerms() {
        getAssembler().clearTerms(getProvisionableColumn());
        return;
    }


    /**
     *  Gets the provisionable query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQueryInspector[] getProvisionableTerms() {
        return (new org.osid.provisioning.ProvisionableQueryInspector[0]);
    }


    /**
     *  Tests if a provisionable search order is available. 
     *
     *  @return <code> true </code> if a provisionable search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableSearchOrder() {
        return (false);
    }


    /**
     *  Gets the provisionable search order. 
     *
     *  @return the provisionable search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsProvisionableSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableSearchOrder getProvisionableSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProvisionableSearchOrder() is false");
    }


    /**
     *  Gets the Provisionable column name.
     *
     * @return the column name
     */

    protected String getProvisionableColumn() {
        return ("provisionable");
    }


    /**
     *  Sets the recipient <code> Id </code> for this query. 
     *
     *  @param  resourceId the resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipientId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getRecipientIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the recipient <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRecipientIdTerms() {
        getAssembler().clearTerms(getRecipientIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipientIdTerms() {
        return (getAssembler().getIdTerms(getRecipientIdColumn()));
    }


    /**
     *  Orders the results by the recipient. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecipient(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecipientColumn(), style);
        return;
    }


    /**
     *  Gets the RecipientId column name.
     *
     * @return the column name
     */

    protected String getRecipientIdColumn() {
        return ("recipient_id");
    }


    /**
     *  Tests if a <code> RecipientQuery </code> is available. 
     *
     *  @return <code> true </code> if a recipient query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Recipient. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipientQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getRecipientQuery() {
        throw new org.osid.UnimplementedException("supportsRecipientQuery() is false");
    }


    /**
     *  Clears the recipient query terms. 
     */

    @OSID @Override
    public void clearRecipientTerms() {
        getAssembler().clearTerms(getRecipientColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRecipientTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipientSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRecipientSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getRecipientSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRecipientSearchOrder() is false");
    }


    /**
     *  Gets the Recipient column name.
     *
     * @return the column name
     */

    protected String getRecipientColumn() {
        return ("recipient");
    }


    /**
     *  Sets the request <code> Id </code> for this query. 
     *
     *  @param  requestId the request <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestId(org.osid.id.Id requestId, boolean match) {
        getAssembler().addIdTerm(getRequestIdColumn(), requestId, match);
        return;
    }


    /**
     *  Clears the request <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestIdTerms() {
        getAssembler().clearTerms(getRequestIdColumn());
        return;
    }


    /**
     *  Gets the request <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestIdTerms() {
        return (getAssembler().getIdTerms(getRequestIdColumn()));
    }


    /**
     *  Orders the results by the request. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequest(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRequestColumn(), style);
        return;
    }


    /**
     *  Gets the RequestId column name.
     *
     * @return the column name
     */

    protected String getRequestIdColumn() {
        return ("request_id");
    }


    /**
     *  Tests if a <code> RequestQuery </code> is available. 
     *
     *  @return <code> true </code> if a request query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestQuery() {
        return (false);
    }


    /**
     *  Gets the query for a request. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the request query 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuery getRequestQuery() {
        throw new org.osid.UnimplementedException("supportsRequestQuery() is false");
    }


    /**
     *  Matches provisions with any request. 
     *
     *  @param  match <code> true </code> to match provisions with a request, 
     *          <code> false </code> to match provisions with no requests 
     */

    @OSID @Override
    public void matchAnyRequest(boolean match) {
        getAssembler().addIdWildcardTerm(getRequestColumn(), match);
        return;
    }


    /**
     *  Clears the request query terms. 
     */

    @OSID @Override
    public void clearRequestTerms() {
        getAssembler().clearTerms(getRequestColumn());
        return;
    }


    /**
     *  Gets the request query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQueryInspector[] getRequestTerms() {
        return (new org.osid.provisioning.RequestQueryInspector[0]);
    }


    /**
     *  Tests if a request search order is available. 
     *
     *  @return <code> true </code> if a request search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestSearchOrder() {
        return (false);
    }


    /**
     *  Gets the request search order. 
     *
     *  @return the request search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRequestSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchOrder getRequestSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRequestSearchOrder() is false");
    }


    /**
     *  Gets the Request column name.
     *
     * @return the column name
     */

    protected String getRequestColumn() {
        return ("request");
    }


    /**
     *  Matches provisions with a provision date in the given range inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchProvisionDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getProvisionDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the provision date query terms. 
     */

    @OSID @Override
    public void clearProvisionDateTerms() {
        getAssembler().clearTerms(getProvisionDateColumn());
        return;
    }


    /**
     *  Gets the provision date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getProvisionDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getProvisionDateColumn()));
    }


    /**
     *  Orders the results by the provision date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProvisionDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProvisionDateColumn(), style);
        return;
    }


    /**
     *  Gets the ProvisionDate column name.
     *
     * @return the column name
     */

    protected String getProvisionDateColumn() {
        return ("provision_date");
    }


    /**
     *  Matches provisions that are leases. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchLeased(boolean match) {
        getAssembler().addBooleanTerm(getLeasedColumn(), match);
        return;
    }


    /**
     *  Clears the leased query terms. 
     */

    @OSID @Override
    public void clearLeasedTerms() {
        getAssembler().clearTerms(getLeasedColumn());
        return;
    }


    /**
     *  Gets the leased query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getLeasedTerms() {
        return (getAssembler().getBooleanTerms(getLeasedColumn()));
    }


    /**
     *  Orders the results by the leased flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLeased(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLeasedColumn(), style);
        return;
    }


    /**
     *  Gets the Leased column name.
     *
     * @return the column name
     */

    protected String getLeasedColumn() {
        return ("leased");
    }


    /**
     *  Matches provisions that must be returned. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMustReturn(boolean match) {
        getAssembler().addBooleanTerm(getMustReturnColumn(), match);
        return;
    }


    /**
     *  Clears the must return query terms. 
     */

    @OSID @Override
    public void clearMustReturnTerms() {
        getAssembler().clearTerms(getMustReturnColumn());
        return;
    }


    /**
     *  Gets the must return query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getMustReturnTerms() {
        return (getAssembler().getBooleanTerms(getMustReturnColumn()));
    }


    /**
     *  Orders the results by the must return flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMustReturn(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMustReturnColumn(), style);
        return;
    }


    /**
     *  Gets the MustReturn column name.
     *
     * @return the column name
     */

    protected String getMustReturnColumn() {
        return ("must_return");
    }


    /**
     *  Matches leased provisions with a due date within the given range 
     *  inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDueDate(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDueDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches provisions with any due date. 
     *
     *  @param  match <code> true </code> to match provisions with a due date, 
     *          <code> false </code> to match provisions with no due date 
     */

    @OSID @Override
    public void matchAnyDueDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDueDateColumn(), match);
        return;
    }


    /**
     *  Clears the due date query terms. 
     */

    @OSID @Override
    public void clearDueDateTerms() {
        getAssembler().clearTerms(getDueDateColumn());
        return;
    }


    /**
     *  Gets the due date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDueDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDueDateColumn()));
    }


    /**
     *  Orders the results by the due date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDueDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDueDateColumn(), style);
        return;
    }


    /**
     *  Gets the DueDate column name.
     *
     * @return the column name
     */

    protected String getDueDateColumn() {
        return ("due_date");
    }


    /**
     *  Matches leased provisions with a cost within the given range 
     *  inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchCost(org.osid.financials.Currency from, 
                          org.osid.financials.Currency to, boolean match) {
        getAssembler().addCurrencyRangeTerm(getCostColumn(), from, to, match);
        return;
    }


    /**
     *  Matches provisions with any cost. 
     *
     *  @param  match <code> true </code> to match provisions with a cost, 
     *          <code> false </code> to match provisions with no cost 
     */

    @OSID @Override
    public void matchAnyCost(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getCostColumn(), match);
        return;
    }


    /**
     *  Clears the cost query terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        getAssembler().clearTerms(getCostColumn());
        return;
    }


    /**
     *  Gets the cost query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCostTerms() {
        return (getAssembler().getCurrencyRangeTerms(getCostColumn()));
    }


    /**
     *  Orders the results by the cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCostColumn(), style);
        return;
    }


    /**
     *  Gets the Cost column name.
     *
     * @return the column name
     */

    protected String getCostColumn() {
        return ("cost");
    }


    /**
     *  Matches leased provisions with a rate amount within the given range 
     *  inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchRateAmount(org.osid.financials.Currency from, 
                                org.osid.financials.Currency to, boolean match) {
        getAssembler().addCurrencyRangeTerm(getRateAmountColumn(), from, to, match);
        return;
    }


    /**
     *  Matches provisions with any rate amount. 
     *
     *  @param  match <code> true </code> to match provisions with a rate, 
     *          <code> false </code> to match provisions with no rate 
     */

    @OSID @Override
    public void matchAnyRateAmount(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getRateAmountColumn(), match);
        return;
    }


    /**
     *  Clears the rate amount query terms. 
     */

    @OSID @Override
    public void clearRateAmountTerms() {
        getAssembler().clearTerms(getRateAmountColumn());
        return;
    }


    /**
     *  Gets the rate amount query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getRateAmountTerms() {
        return (getAssembler().getCurrencyRangeTerms(getRateAmountColumn()));
    }

    
    /**
     *  Gets the RateAmount column name.
     *
     * @return the column name
     */

    protected String getRateAmountColumn() {
        return ("rate_amount");
    }


    /**
     *  Matches leased provisions with a rate period within the given range 
     *  inclusive. 
     *
     *  @param  from the range start 
     *  @param  to the range end 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRatePeriod(org.osid.calendaring.Duration from, 
                                org.osid.calendaring.Duration to, 
                                boolean match) {
        getAssembler().addDurationRangeTerm(getRatePeriodColumn(), from, to, match);
        return;
    }


    /**
     *  Matches provisions with any rate period. 
     *
     *  @param  match <code> true </code> to match provisions with a rate 
     *          period, <code> false </code> to match provisions with no rate 
     *          period 
     */

    @OSID @Override
    public void matchAnyRatePeriod(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getRatePeriodColumn(), match);
        return;
    }


    /**
     *  Clears the rate period query terms. 
     */

    @OSID @Override
    public void clearRatePeriodTerms() {
        getAssembler().clearTerms(getRatePeriodColumn());
        return;
    }


    /**
     *  Gets the rate period query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRatePeriodTerms() {
        return (getAssembler().getDurationRangeTerms(getRatePeriodColumn()));
    }


    /**
     *  Gets the Rate Period column name.
     *
     *  @return the column name
     */

    protected String getRatePeriodColumn() {
        return ("rate_period");
    }


    /**
     *  Orders the results by the rate amount and period. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRateColumn(), style);
        return;
    }


    /**
     *  Gets the Rate Period column name.
     *
     *  @return the column name
     */

    protected String getRateColumn() {
        return ("rate");
    }


    /**
     *  Tests if a <code> ProvisionReturn </code> is available. 
     *
     *  @return <code> true </code> if a provision return query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionReturnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a provision return. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the provision return query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionReturnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnQuery getProvisionReturnQuery() {
        throw new org.osid.UnimplementedException("supportsProvisionReturnQuery() is false");
    }


    /**
     *  Matches provisions with any provision return. 
     *
     *  @param  match <code> true </code> to match provisions with a provision 
     *          return, <code> false </code> to match provisions with no 
     *          provision return 
     */

    @OSID @Override
    public void matchAnyProvisionReturn(boolean match) {
        getAssembler().addIdWildcardTerm(getProvisionReturnColumn(), match);
        return;
    }


    /**
     *  Clears the provision return query terms. 
     */

    @OSID @Override
    public void clearProvisionReturnTerms() {
        getAssembler().clearTerms(getProvisionReturnColumn());
        return;
    }


    /**
     *  Gets the provision return query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnQueryInspector[] getProvisionReturnTerms() {
        return (new org.osid.provisioning.ProvisionReturnQueryInspector[0]);
    }


    /**
     *  Orders the results by returned. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProvisionReturn(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProvisionReturnColumn(), style);
        return;
    }


    /**
     *  Tests if a provision return search order is available. 
     *
     *  @return <code> true </code> if a provision return search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionReturnSearchOrder() {
        return (false);
    }


    /**
     *  Gets the provision return search order. 
     *
     *  @return the provision return search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsProvisionReturnSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnSearchOrder getProvisionReturnSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProvisionReturnSearchOrder() is false");
    }


    /**
     *  Gets the ProvisionReturn column name.
     *
     * @return the column name
     */

    protected String getProvisionReturnColumn() {
        return ("provision_return");
    }


    /**
     *  Sets the distributor <code> Id </code> for this query to match 
     *  provisions assigned to distributors. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), distributorId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this provision supports the given record
     *  <code>Type</code>.
     *
     *  @param  provisionRecordType a provision record type 
     *  @return <code>true</code> if the provisionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionRecordType) {
        for (org.osid.provisioning.records.ProvisionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  provisionRecordType the provision record type 
     *  @return the provision query record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionQueryRecord getProvisionQueryRecord(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  provisionRecordType the provision record type 
     *  @return the provision query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionQueryInspectorRecord getProvisionQueryInspectorRecord(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param provisionRecordType the provision record type
     *  @return the provision search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionSearchOrderRecord getProvisionSearchOrderRecord(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(provisionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this provision. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionQueryRecord the provision query record
     *  @param provisionQueryInspectorRecord the provision query inspector
     *         record
     *  @param provisionSearchOrderRecord the provision search order record
     *  @param provisionRecordType provision record type
     *  @throws org.osid.NullArgumentException
     *          <code>provisionQueryRecord</code>,
     *          <code>provisionQueryInspectorRecord</code>,
     *          <code>provisionSearchOrderRecord</code> or
     *          <code>provisionRecordTypeprovision</code> is
     *          <code>null</code>
     */
            
    protected void addProvisionRecords(org.osid.provisioning.records.ProvisionQueryRecord provisionQueryRecord, 
                                      org.osid.provisioning.records.ProvisionQueryInspectorRecord provisionQueryInspectorRecord, 
                                      org.osid.provisioning.records.ProvisionSearchOrderRecord provisionSearchOrderRecord, 
                                      org.osid.type.Type provisionRecordType) {

        addRecordType(provisionRecordType);

        nullarg(provisionQueryRecord, "provision query record");
        nullarg(provisionQueryInspectorRecord, "provision query inspector record");
        nullarg(provisionSearchOrderRecord, "provision search odrer record");

        this.queryRecords.add(provisionQueryRecord);
        this.queryInspectorRecords.add(provisionQueryInspectorRecord);
        this.searchOrderRecords.add(provisionSearchOrderRecord);
        
        return;
    }
}
