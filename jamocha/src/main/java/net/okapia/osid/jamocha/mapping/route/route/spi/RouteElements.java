//
// RouteElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.route.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class RouteElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the RouteElement Id.
     *
     *  @return the route element Id
     */

    public static org.osid.id.Id getRouteEntityId() {
        return (makeEntityId("osid.mapping.route.Route"));
    }


    /**
     *  Gets the StartingLocationId element Id.
     *
     *  @return the StartingLocationId element Id
     */

    public static org.osid.id.Id getStartingLocationId() {
        return (makeElementId("osid.mapping.route.route.StartingLocationId"));
    }


    /**
     *  Gets the StartingLocation element Id.
     *
     *  @return the StartingLocation element Id
     */

    public static org.osid.id.Id getStartingLocation() {
        return (makeElementId("osid.mapping.route.route.StartingLocation"));
    }


    /**
     *  Gets the EndingLocationId element Id.
     *
     *  @return the EndingLocationId element Id
     */

    public static org.osid.id.Id getEndingLocationId() {
        return (makeElementId("osid.mapping.route.route.EndingLocationId"));
    }


    /**
     *  Gets the EndingLocation element Id.
     *
     *  @return the EndingLocation element Id
     */

    public static org.osid.id.Id getEndingLocation() {
        return (makeElementId("osid.mapping.route.route.EndingLocation"));
    }


    /**
     *  Gets the Distance element Id.
     *
     *  @return the Distance element Id
     */

    public static org.osid.id.Id getDistance() {
        return (makeElementId("osid.mapping.route.route.Distance"));
    }


    /**
     *  Gets the ETA element Id.
     *
     *  @return the ETA element Id
     */

    public static org.osid.id.Id getETA() {
        return (makeElementId("osid.mapping.route.route.ETA"));
    }


    /**
     *  Gets the SegmentIds element Id.
     *
     *  @return the SegmentIds element Id
     */

    public static org.osid.id.Id getSegmentIds() {
        return (makeElementId("osid.mapping.route.route.SegmentIds"));
    }


    /**
     *  Gets the Segments element Id.
     *
     *  @return the Segments element Id
     */

    public static org.osid.id.Id getSegments() {
        return (makeElementId("osid.mapping.route.route.Segments"));
    }


    /**
     *  Gets the AlongLocationIds element Id.
     *
     *  @return the AlongLocationIds element Id
     */

    public static org.osid.id.Id getAlongLocationIds() {
        return (makeQueryElementId("osid.mapping.route.route.AlongLocationIds"));
    }


    /**
     *  Gets the LocationId element Id.
     *
     *  @return the LocationId element Id
     */

    public static org.osid.id.Id getLocationId() {
        return (makeQueryElementId("osid.mapping.route.route.LocationId"));
    }


    /**
     *  Gets the Location element Id.
     *
     *  @return the Location element Id
     */

    public static org.osid.id.Id getLocation() {
        return (makeQueryElementId("osid.mapping.route.route.Location"));
    }


    /**
     *  Gets the PathId element Id.
     *
     *  @return the PathId element Id
     */

    public static org.osid.id.Id getPathId() {
        return (makeQueryElementId("osid.mapping.route.route.PathId"));
    }


    /**
     *  Gets the Path element Id.
     *
     *  @return the Path element Id
     */

    public static org.osid.id.Id getPath() {
        return (makeQueryElementId("osid.mapping.route.route.Path"));
    }


    /**
     *  Gets the MapId element Id.
     *
     *  @return the MapId element Id
     */

    public static org.osid.id.Id getMapId() {
        return (makeQueryElementId("osid.mapping.route.route.MapId"));
    }


    /**
     *  Gets the Map element Id.
     *
     *  @return the Map element Id
     */

    public static org.osid.id.Id getMap() {
        return (makeQueryElementId("osid.mapping.route.route.Map"));
    }
}
