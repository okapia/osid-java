//
// AbstractInstallationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractInstallationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.installation.InstallationManager,
               org.osid.installation.InstallationProxyManager {

    private final Types installationRecordTypes            = new TypeRefSet();
    private final Types installationSearchRecordTypes      = new TypeRefSet();

    private final Types siteRecordTypes                    = new TypeRefSet();
    private final Types packageRecordTypes                 = new TypeRefSet();
    private final Types packageSearchRecordTypes           = new TypeRefSet();

    private final Types contentRecordTypes                 = new TypeRefSet();

    private final Types depotRecordTypes                   = new TypeRefSet();
    private final Types depotSearchRecordTypes             = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractInstallationManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractInstallationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if an installation lookup service is supported. 
     *
     *  @return true if installation lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationLookup() {
        return (false);
    }


    /**
     *  Tests if an installation query service is supported. 
     *
     *  @return true if installation query is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationQuery() {
        return (false);
    }


    /**
     *  Tests if an installation search service is supported. 
     *
     *  @return <code> true </code> if installation search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationSearch() {
        return (false);
    }


    /**
     *  Tests if an installation management service is supported. 
     *
     *  @return <code> true </code> if package management is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationManagement() {
        return (false);
    }


    /**
     *  Tests if an installation update service is supported. 
     *
     *  @return <code> true </code> if package update is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationUpdate() {
        return (false);
    }


    /**
     *  Tests if installation notification is supported. Messages may be sent 
     *  when installations are installed or removed. 
     *
     *  @return <code> true </code> if installation notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationNotification() {
        return (false);
    }


    /**
     *  Tests if a site lookup service is supported. 
     *
     *  @return true if site lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsSiteLookup() {
        return (false);
    }


    /**
     *  Tests if a package lookup service is supported. A package lookup 
     *  service defines methods to access packages. 
     *
     *  @return true if package lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsPackageLookup() {
        return (false);
    }


    /**
     *  Tests if querying packages is supported. 
     *
     *  @return <code> true </code> if packages query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageQuery() {
        return (false);
    }


    /**
     *  Tests if a package search service is supported. 
     *
     *  @return <code> true </code> if package search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageSearch() {
        return (false);
    }


    /**
     *  Tests if a package administrative service is supported. 
     *
     *  @return <code> true </code> if package admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageAdmin() {
        return (false);
    }


    /**
     *  Tests if package notification is supported. Messages may be sent when 
     *  packages are created, modified, or deleted. 
     *
     *  @return <code> true </code> if package notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageNotification() {
        return (false);
    }


    /**
     *  Tests if a package to depot lookup session is available. 
     *
     *  @return <code> true </code> if package depot lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageDepot() {
        return (false);
    }


    /**
     *  Tests if a package to depot assignment session is available. 
     *
     *  @return <code> true </code> if package depot assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageDepotAssignment() {
        return (false);
    }


    /**
     *  Tests if package smart depots are available. 
     *
     *  @return <code> true </code> if package smart depots are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPackageSmartDepot() {
        return (false);
    }


    /**
     *  Tests if a depot lookup service is supported. 
     *
     *  @return <code> true </code> if depot lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotLookup() {
        return (false);
    }


    /**
     *  Tests if a depot query service is supported. 
     *
     *  @return <code> true </code> if depot query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotQuery() {
        return (false);
    }


    /**
     *  Tests if a depot search service is supported. 
     *
     *  @return <code> true </code> if depot search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotSearch() {
        return (false);
    }


    /**
     *  Tests if a depot administrative service is supported. 
     *
     *  @return <code> true </code> if depot admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotAdmin() {
        return (false);
    }


    /**
     *  Tests if depot notification is supported. Messages may be sent when 
     *  depots are created, modified, or deleted. 
     *
     *  @return <code> true </code> if depot notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotNotification() {
        return (false);
    }


    /**
     *  Tests if a depot hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a depot hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotHierarchy() {
        return (false);
    }


    /**
     *  Tests if depot hierarchy design is supported. 
     *
     *  @return <code> true </code> if a depot hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDepotHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if an installation batch service is supported. 
     *
     *  @return <code> true </code> if an installation batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Installation </code> record types. 
     *
     *  @return a list containing the supported <code> Installation </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstallationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.installationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Installation </code> record type is 
     *  supported. 
     *
     *  @param  installationRecordType a <code> Type </code> indicating an 
     *          <code> Installation </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> installationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstallationRecordType(org.osid.type.Type installationRecordType) {
        return (this.installationRecordTypes.contains(installationRecordType));
    }


    /**
     *  Adds support for an installation record type.
     *
     *  @param installationRecordType an installation record type
     *  @throws org.osid.NullArgumentException
     *  <code>installationRecordType</code> is <code>null</code>
     */

    protected void addInstallationRecordType(org.osid.type.Type installationRecordType) {
        this.installationRecordTypes.add(installationRecordType);
        return;
    }


    /**
     *  Removes support for an installation record type.
     *
     *  @param installationRecordType an installation record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>installationRecordType</code> is <code>null</code>
     */

    protected void removeInstallationRecordType(org.osid.type.Type installationRecordType) {
        this.installationRecordTypes.remove(installationRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Installation </code> search record types. 
     *
     *  @return a list containing the supported <code> Installation </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstallationSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.installationSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Installation </code> search record type is 
     *  supported. 
     *
     *  @param  installationSearchRecordType a <code> Type </code> indicating 
     *          an <code> Installation </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          installationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInstallationSearchRecordType(org.osid.type.Type installationSearchRecordType) {
        return (this.installationSearchRecordTypes.contains(installationSearchRecordType));
    }


    /**
     *  Adds support for an installation search record type.
     *
     *  @param installationSearchRecordType an installation search record type
     *  @throws org.osid.NullArgumentException
     *  <code>installationSearchRecordType</code> is <code>null</code>
     */

    protected void addInstallationSearchRecordType(org.osid.type.Type installationSearchRecordType) {
        this.installationSearchRecordTypes.add(installationSearchRecordType);
        return;
    }


    /**
     *  Removes support for an installation search record type.
     *
     *  @param installationSearchRecordType an installation search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>installationSearchRecordType</code> is <code>null</code>
     */

    protected void removeInstallationSearchRecordType(org.osid.type.Type installationSearchRecordType) {
        this.installationSearchRecordTypes.remove(installationSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Site </code> record types. 
     *
     *  @return a list containing the supported <code> Site </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSiteRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.siteRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Site </code> record type is supported. 
     *
     *  @param  siteRecordType a <code> Type </code> indicating a <code> Site 
     *          </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> siteRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSiteRecordType(org.osid.type.Type siteRecordType) {
        return (this.siteRecordTypes.contains(siteRecordType));
    }


    /**
     *  Adds support for a site record type.
     *
     *  @param siteRecordType a site record type
     *  @throws org.osid.NullArgumentException
     *  <code>siteRecordType</code> is <code>null</code>
     */

    protected void addSiteRecordType(org.osid.type.Type siteRecordType) {
        this.siteRecordTypes.add(siteRecordType);
        return;
    }


    /**
     *  Removes support for a site record type.
     *
     *  @param siteRecordType a site record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>siteRecordType</code> is <code>null</code>
     */

    protected void removeSiteRecordType(org.osid.type.Type siteRecordType) {
        this.siteRecordTypes.remove(siteRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Package </code> record types. 
     *
     *  @return a list containing the supported <code> Package </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPackageRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.packageRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Package </code> record type is supported. 
     *
     *  @param  packageRecordType a <code> Type </code> indicating a <code> 
     *          Package </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> packageRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPackageRecordType(org.osid.type.Type packageRecordType) {
        return (this.packageRecordTypes.contains(packageRecordType));
    }


    /**
     *  Adds support for a package record type.
     *
     *  @param packageRecordType a package record type
     *  @throws org.osid.NullArgumentException
     *  <code>packageRecordType</code> is <code>null</code>
     */

    protected void addPackageRecordType(org.osid.type.Type packageRecordType) {
        this.packageRecordTypes.add(packageRecordType);
        return;
    }


    /**
     *  Removes support for a package record type.
     *
     *  @param packageRecordType a package record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>packageRecordType</code> is <code>null</code>
     */

    protected void removePackageRecordType(org.osid.type.Type packageRecordType) {
        this.packageRecordTypes.remove(packageRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Package </code> search record types. 
     *
     *  @return a list containing the supported <code> Package </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPackageSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.packageSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Package </code> search record type is 
     *  supported. 
     *
     *  @param  packageSearchRecordType a <code> Type </code> indicating a 
     *          <code> Package </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> packageSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPackageSearchRecordType(org.osid.type.Type packageSearchRecordType) {
        return (this.packageSearchRecordTypes.contains(packageSearchRecordType));
    }


    /**
     *  Adds support for a package search record type.
     *
     *  @param packageSearchRecordType a package search record type
     *  @throws org.osid.NullArgumentException
     *  <code>packageSearchRecordType</code> is <code>null</code>
     */

    protected void addPackageSearchRecordType(org.osid.type.Type packageSearchRecordType) {
        this.packageSearchRecordTypes.add(packageSearchRecordType);
        return;
    }


    /**
     *  Removes support for a package search record type.
     *
     *  @param packageSearchRecordType a package search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>packageSearchRecordType</code> is <code>null</code>
     */

    protected void removePackageSearchRecordType(org.osid.type.Type packageSearchRecordType) {
        this.packageSearchRecordTypes.remove(packageSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> InstallationContent </code> record types. 
     *
     *  @return a list containing the supported <code> InstallationContent </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInstallationContentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.contentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> InstallationContent </code> record type is supported. 
     *
     *  @param installationContentRecordType a <code> Type </code>
     *          indicating a <code> InstallationContent </code> record
     *          type
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException
     *          <code>installationContentRecordType </code> is <code>
     *          null </code>
     */

    @OSID @Override
    public boolean supportsInstallationContentRecordType(org.osid.type.Type installationContentRecordType) {
        return (this.contentRecordTypes.contains(installationContentRecordType));
    }


    /**
     *  Adds support for an installation content record type.
     *
     *  @param installationContentRecordType an installation
     *         contentrecord type
     *  @throws org.osid.NullArgumentException
     *  <code>installationContentRecordType</code> is <code>null</code>
     */

    protected void addInstallationContentRecordType(org.osid.type.Type installationContentRecordType) {
        this.contentRecordTypes.add(installationContentRecordType);
        return;
    }


    /**
     *  Removes support for a installation content record type.
     *
     *  @param installationContentRecordType a installation content record type
     *
     *  @throws org.osid.NullArgumentException
     *          <code>installationContentRecordType</code> is
     *          <code>null</code>
     */

    protected void removeInstallationContentRecordType(org.osid.type.Type installationContentRecordType) {
        this.contentRecordTypes.remove(installationContentRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Depot </code> record types. 
     *
     *  @return a list containing the supported <code> Depot </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDepotRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.depotRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Depot </code> record type is supported. 
     *
     *  @param  depotRecordType a <code> Type </code> indicating a <code> 
     *          Depot </code> type 
     *  @return <code> true </code> if the given depot record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> depotRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDepotRecordType(org.osid.type.Type depotRecordType) {
        return (this.depotRecordTypes.contains(depotRecordType));
    }


    /**
     *  Adds support for a depot record type.
     *
     *  @param depotRecordType a depot record type
     *  @throws org.osid.NullArgumentException
     *  <code>depotRecordType</code> is <code>null</code>
     */

    protected void addDepotRecordType(org.osid.type.Type depotRecordType) {
        this.depotRecordTypes.add(depotRecordType);
        return;
    }


    /**
     *  Removes support for a depot record type.
     *
     *  @param depotRecordType a depot record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>depotRecordType</code> is <code>null</code>
     */

    protected void removeDepotRecordType(org.osid.type.Type depotRecordType) {
        this.depotRecordTypes.remove(depotRecordType);
        return;
    }


    /**
     *  Gets the supported depot search record types. 
     *
     *  @return a list containing the supported <code> Depot </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDepotSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.depotSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given depot search record type is supported. 
     *
     *  @param  depotSearchRecordType a <code> Type </code> indicating a 
     *          <code> Depot </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> depotSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDepotSearchRecordType(org.osid.type.Type depotSearchRecordType) {
        return (this.depotSearchRecordTypes.contains(depotSearchRecordType));
    }


    /**
     *  Adds support for a depot search record type.
     *
     *  @param depotSearchRecordType a depot search record type
     *  @throws org.osid.NullArgumentException
     *  <code>depotSearchRecordType</code> is <code>null</code>
     */

    protected void addDepotSearchRecordType(org.osid.type.Type depotSearchRecordType) {
        this.depotSearchRecordTypes.add(depotSearchRecordType);
        return;
    }


    /**
     *  Removes support for a depot search record type.
     *
     *  @param depotSearchRecordType a depot search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>depotSearchRecordType</code> is <code>null</code>
     */

    protected void removeDepotSearchRecordType(org.osid.type.Type depotSearchRecordType) {
        this.depotSearchRecordTypes.remove(depotSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  lookup service. 
     *
     *  @return an <code> InstallationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationLookupSession getInstallationLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationLookupSession getInstallationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  lookup service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @return <code> an InstallationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationLookupSession getInstallationLookupSessionForSite(org.osid.id.Id siteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationLookupSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  lookup service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationLookupSession getInstallationLookupSessionForSite(org.osid.id.Id siteId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationLookupSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  query service. 
     *
     *  @return an <code> InstallationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationQuerySession[] getInstallationQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationQuerySession[] getInstallationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  query service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @return <code> an InstallationQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationQuerySession[] getInstallationQuerySessionForSite(org.osid.id.Id siteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationQuerySessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  query service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationQuerySession[] getInstallationQuerySessionForSite(org.osid.id.Id siteId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationQuerySessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  search service. 
     *
     *  @return an <code> InstallationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationSearchSession getInstallationSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationSearchSession getInstallationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  search service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @return <code> an InstallationSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationSearchSession getInstallationSearchSessionForSite(org.osid.id.Id siteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationSearchSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  search service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationSearchSession getInstallationSearchSessionForSite(org.osid.id.Id siteId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationSearchSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  management service. 
     *
     *  @return an <code> InstallationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationManagement() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationManagementSession getInstallationManagementSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationManagementSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  management service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationManagement() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationManagementSession getInstallationManagementSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationManagementSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  management service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @return <code> an InstallationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationManagement() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationManagementSession getInstallationManagementSessionForSite(org.osid.id.Id siteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationManagementSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  management service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationManagement() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationManagementSession getInstallationManagementSessionForSite(org.osid.id.Id siteId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationManagementSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  update service. 
     *
     *  @return an <code> InstallationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationUpdate() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationUpdateSession getInstallationUpdateSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationUpdateSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  update service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InstallationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationUpdate() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationUpdateSession getInstallationUpdateSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationUpdateSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  update service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @return <code> an InstallationUpdateSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationUpdateSession getInstallationUpdateSessionForSite(org.osid.id.Id siteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationUpdateSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  update service for the given site. 
     *
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationUpdateSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> siteId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationUpdate() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationUpdateSession getInstallationUpdateSessionForSite(org.osid.id.Id siteId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationUpdateSessionForSite not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  installation changes. 
     *
     *  @param  installationReceiver the installation receiver 
     *  @return an <code> InstallationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> installationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationNotificationSession getInstallationNotificationSession(org.osid.installation.InstallationReceiver installationReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  installation changes. 
     *
     *  @param  installationReceiver the installation receiver 
     *  @param  proxy a proxy 
     *  @return an <code> InstallationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> installationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationNotificationSession getInstallationNotificationSession(org.osid.installation.InstallationReceiver installationReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  notification service for the given site. 
     *
     *  @param  installationReceiver the installation receiver 
     *  @param  siteId the <code> Id </code> of the site 
     *  @return <code> an InstallationNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> installationReceiver 
     *          </code> or <code> siteId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationNotificationSession getInstallationNotificationSessionForSite(org.osid.installation.InstallationReceiver installationReceiver, 
                                                                                                           org.osid.id.Id siteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationNotificationSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the installation 
     *  notification service for the given site. 
     *
     *  @param  installationReceiver the installation receiver 
     *  @param  siteId the <code> Id </code> of the site 
     *  @param  proxy a proxy 
     *  @return <code> an InstallationNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> siteId </code> not found 
     *  @throws org.osid.NullArgumentException <code> installationReceiver, 
     *          siteId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationNotificationSession getInstallationNotificationSessionForSite(org.osid.installation.InstallationReceiver installationReceiver, 
                                                                                                           org.osid.id.Id siteId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationNotificationSessionForSite not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the site lookup 
     *  service. 
     *
     *  @return a <code> SiteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSiteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteLookupSession getSiteLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getSiteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the site lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SiteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSiteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteLookupSession getSiteLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getSiteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package lookup 
     *  service. 
     *
     *  @return a <code> PackageLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageLookupSession getPackageLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageLookupSession getPackageLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package lookup 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @return <code> a PackageLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageLookupSession getPackageLookupSessionForDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageLookupSessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package lookup 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return <code> a PackageLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageLookupSession getPackageLookupSessionForDepot(org.osid.id.Id depotId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageLookupSessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package query 
     *  service. 
     *
     *  @return a <code> PackageQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuerySession getPackageQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuerySession getPackageQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package query 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the <code> Depot </code> 
     *  @return a <code> PackageQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Depot </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuerySession getPackageQuerySessionForDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageQuerySessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package query 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the <code> Depot </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PackageQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Depot </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageQuerySession getPackageQuerySessionForDepot(org.osid.id.Id depotId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageQuerySessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package search 
     *  service. 
     *
     *  @return a <code> PackageSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSearchSession getPackageSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSearchSession getPackageSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package search 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @return <code> a PackageSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSearchSession getPackageSearchSessionForDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageSearchSessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package search 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return <code> a PackageSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSearchSession getPackageSearchSessionForDepot(org.osid.id.Id depotId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageSearchSessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package 
     *  administration service. 
     *
     *  @return a <code> PackageAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageAdminSession getPackageAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageAdminSession getPackageAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package admin 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @return <code> a PackageAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageAdminSession getPackageAdminSessionForDepot(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageAdminSessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package admin 
     *  service for the given depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return <code> a PackageAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsPackageAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageAdminSession getPackageAdminSessionForDepot(org.osid.id.Id depotId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageAdminSessionForDepot not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to package 
     *  changes. 
     *
     *  @param  packageReceiver the package receiver 
     *  @return a <code> PackageNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> packageReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageNotificationSession getPackageNotificationSession(org.osid.installation.PackageReceiver packageReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to package 
     *  changes. 
     *
     *  @param  packageReceiver the package receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PackageNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> packageReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageNotificationSession getPackageNotificationSession(org.osid.installation.PackageReceiver packageReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package 
     *  notification service for the given depot. 
     *
     *  @param  packageReceiver the package receiver 
     *  @param  depotId the <code> Id </code> of the depot 
     *  @return <code> a PackageNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> packageReceiver </code> 
     *          or <code> depotId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageNotificationSession getPackageNotificationSessionForDepot(org.osid.installation.PackageReceiver packageReceiver, 
                                                                                                  org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageNotificationSessionForDepot not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the package 
     *  notification service for the given depot. 
     *
     *  @param  packageReceiver the package receiver 
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return <code> a PackageNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> packageReceiver, depotId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageNotificationSession getPackageNotificationSessionForDepot(org.osid.installation.PackageReceiver packageReceiver, 
                                                                                                  org.osid.id.Id depotId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageNotificationSessionForDepot not implemented");
    }


    /**
     *  Gets the session for retrieving package to depot mappings. 
     *
     *  @return a <code> PackageDepotSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageDepot() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageDepotSession getPackageDepotSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageDepotSession not implemented");
    }


    /**
     *  Gets the session for retrieving package to depot mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageDepotSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPackageDepot() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageDepotSession getPackageDepotSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageDepotSession not implemented");
    }


    /**
     *  Gets the session for assigning package to depot mappings. 
     *
     *  @return a <code> PackageDepotAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageDepotAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageDepotSession getPackageDepotAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageDepotAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning package to depot mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PackageDepotAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageDepotAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageDepotSession getPackageDepotAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageDepotAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic package depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @return a <code> PackageSmartDepotSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageSmartDepot() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSmartDepotSession getPackageSmartDepotSession(org.osid.id.Id depotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getPackageSmartDepotSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic package depot. 
     *
     *  @param  depotId the <code> Id </code> of the depot 
     *  @param  proxy a proxy 
     *  @return a <code> PackageSmartDepotSession </code> 
     *  @throws org.osid.NotFoundException <code> depotId </code> not found 
     *  @throws org.osid.NullArgumentException <code> depotId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPackageSmartDepot() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.PackageSmartDepotSession getPackageSmartDepotSession(org.osid.id.Id depotId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getPackageSmartDepotSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the depot lookup service. 
     *
     *  @return a <code> DepotLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotLookupSession getDepotLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getDepotLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the depot lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotLookup() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotLookupSession getDepotLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getDepotLookupSession not implemented");
    }


    /**
     *  Gets the depot query session. 
     *
     *  @return a <code> DepotQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuerySession getDepotQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getDepotQuerySession not implemented");
    }


    /**
     *  Gets the depot query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotQuerySession getDepotQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getDepotQuerySession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the depot search service. 
     *
     *  @return a <code> DepotSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotSearchSession getDepotSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getDepotSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the depot search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotSearch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotSearchSession getDepotSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getDepotSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the depot administration service. 
     *
     *  @return a <code> DepotAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotAdmin() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotAdminSession getDepotAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getDepotAdminSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the depot administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDepotAdmin() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotAdminSession getDepotAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getDepotAdminSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to depot 
     *  service changes. 
     *
     *  @param  depotReceiver the depot receiver 
     *  @return a <code> DepotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> depotReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotNotificationSession getDepotNotificationSession(org.osid.installation.DepotReceiver depotReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getDepotNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to depot 
     *  service changes. 
     *
     *  @param  depotReceiver the depot receiver 
     *  @param  proxy a proxy 
     *  @return a <code> DepotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> depotReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotNotificationSession getDepotNotificationSession(org.osid.installation.DepotReceiver depotReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getDepotNotificationSession not implemented");
    }


    /**
     *  Gets the session traversing depot hierarchies. 
     *
     *  @return a <code> DepotHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotHierarchySession getDepotHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getDepotHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing depot hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotHierarchySession getDepotHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getDepotHierarchySession not implemented");
    }


    /**
     *  Gets the session designing depot hierarchies. 
     *
     *  @return a <code> DepotHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotHierarchyDesignSession getDepotHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getDepotHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing depot hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DepotHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDepotHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.DepotHierarchyDesignSession getDepotHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getDepotHierarchyDesignSession not implemented");
    }


    /**
     *  Gets an <code> InstallationBatchManager. </code> 
     *
     *  @return an <code> InstallationBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationBatch() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.InstallationBatchManager getInstallationBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationManager.getInstallationBatchManager not implemented");
    }


    /**
     *  Gets an <code> InstallationBatchProxyManager. </code> 
     *
     *  @return an <code> InstallationBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationBatch() is false </code> 
     */

    @OSID @Override
    public org.osid.installation.batch.InstallationBatchProxyManager getInstallationBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.installation.InstallationProxyManager.getInstallationBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.installationRecordTypes.clear();
        this.installationRecordTypes.clear();

        this.installationSearchRecordTypes.clear();
        this.installationSearchRecordTypes.clear();

        this.siteRecordTypes.clear();
        this.siteRecordTypes.clear();

        this.packageRecordTypes.clear();
        this.packageRecordTypes.clear();

        this.packageSearchRecordTypes.clear();
        this.packageSearchRecordTypes.clear();

        this.depotRecordTypes.clear();
        this.depotRecordTypes.clear();

        this.depotSearchRecordTypes.clear();
        this.depotSearchRecordTypes.clear();

        return;
    }
}
