//
// MutableMapProxyAssessmentTakenLookupSession
//
//    Implements an AssessmentTaken lookup service backed by a collection of
//    assessmentsTaken that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements an AssessmentTaken lookup service backed by a collection of
 *  assessmentsTaken. The assessmentsTaken are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of assessments taken can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractMapAssessmentTakenLookupSession
    implements org.osid.assessment.AssessmentTakenLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyAssessmentTakenLookupSession}
     *  with no assessments taken.
     *
     *  @param bank the bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                  org.osid.proxy.Proxy proxy) {
        setBank(bank);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyAssessmentTakenLookupSession} with a
     *  single assessment taken.
     *
     *  @param bank the bank
     *  @param assessmentTaken an assessment taken
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentTaken}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                org.osid.assessment.AssessmentTaken assessmentTaken, org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putAssessmentTaken(assessmentTaken);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAssessmentTakenLookupSession} using an
     *  array of assessments taken.
     *
     *  @param bank the bank
     *  @param assessmentsTaken an array of assessments taken
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentsTaken}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                org.osid.assessment.AssessmentTaken[] assessmentsTaken, org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putAssessmentsTaken(assessmentsTaken);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAssessmentTakenLookupSession} using a
     *  collection of assessments taken.
     *
     *  @param bank the bank
     *  @param assessmentsTaken a collection of assessments taken
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentsTaken}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                java.util.Collection<? extends org.osid.assessment.AssessmentTaken> assessmentsTaken,
                                                org.osid.proxy.Proxy proxy) {
   
        this(bank, proxy);
        setSessionProxy(proxy);
        putAssessmentsTaken(assessmentsTaken);
        return;
    }

    
    /**
     *  Makes a {@code AssessmentTaken} available in this session.
     *
     *  @param assessmentTaken an assessment taken
     *  @throws org.osid.NullArgumentException {@code assessmentTaken{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentTaken(org.osid.assessment.AssessmentTaken assessmentTaken) {
        super.putAssessmentTaken(assessmentTaken);
        return;
    }


    /**
     *  Makes an array of assessmentsTaken available in this session.
     *
     *  @param assessmentsTaken an array of assessments taken
     *  @throws org.osid.NullArgumentException {@code assessmentsTaken{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentsTaken(org.osid.assessment.AssessmentTaken[] assessmentsTaken) {
        super.putAssessmentsTaken(assessmentsTaken);
        return;
    }


    /**
     *  Makes collection of assessments taken available in this session.
     *
     *  @param assessmentsTaken
     *  @throws org.osid.NullArgumentException {@code assessmentTaken{@code 
     *          is {@code null}
     */

    @Override
    public void putAssessmentsTaken(java.util.Collection<? extends org.osid.assessment.AssessmentTaken> assessmentsTaken) {
        super.putAssessmentsTaken(assessmentsTaken);
        return;
    }


    /**
     *  Removes a AssessmentTaken from this session.
     *
     *  @param assessmentTakenId the {@code Id} of the assessment taken
     *  @throws org.osid.NullArgumentException {@code assessmentTakenId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAssessmentTaken(org.osid.id.Id assessmentTakenId) {
        super.removeAssessmentTaken(assessmentTakenId);
        return;
    }    
}
