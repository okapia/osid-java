//
// AbstractTodoLookupSession.java
//
//    A starter implementation framework for providing a Todo
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Todo
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getTodos(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractTodoLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.checklist.TodoLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private boolean sequestered   = false;
    private org.osid.checklist.Checklist checklist = new net.okapia.osid.jamocha.nil.checklist.checklist.UnknownChecklist();
    

    /**
     *  Gets the <code>Checklist/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Checklist Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.checklist.getId());
    }


    /**
     *  Gets the <code>Checklist</code> associated with this 
     *  session.
     *
     *  @return the <code>Checklist</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.checklist);
    }


    /**
     *  Sets the <code>Checklist</code>.
     *
     *  @param  checklist the checklist for this session
     *  @throws org.osid.NullArgumentException <code>checklist</code>
     *          is <code>null</code>
     */

    protected void setChecklist(org.osid.checklist.Checklist checklist) {
        nullarg(checklist, "checklist");
        this.checklist = checklist;
        return;
    }

    /**
     *  Tests if this user can perform <code>Todo</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupTodos() {
        return (true);
    }


    /**
     *  A complete view of the <code>Todo</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTodoView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Todo</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTodoView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todos in checklists which are children of
     *  this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this checklist only.
     */

    @OSID @Override
    public void useIsolatedChecklistView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only todos whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveTodoView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All todos of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveTodoView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

    
    /**
     *  The returns from the lookup methods omit sequestered todos.
     */

    @OSID @Override
    public void useSequesteredTodoView() {
        this.sequestered = true;
        return;
    }


    /**
     *  All todos are returned including sequestered todos.
     */

    @OSID @Override
    public void useUnsequesteredTodoView() {
        this.sequestered = false;
        return;
    }


    /**
     *  Tests if a sequestered or unsequestered view is set.
     *
     *  @return <code>true</code> if sequestered</code>,
     *          <code>false</code> if unsequestered
     */

    protected boolean isSequestered() {
        return (this.sequestered);
    }


    /**
     *  Gets the <code>Todo</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Todo</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Todo</code> and
     *  retained for compatibility.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @param  todoId <code>Id</code> of the
     *          <code>Todo</code>
     *  @return the todo
     *  @throws org.osid.NotFoundException <code>todoId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>todoId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Todo getTodo(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.checklist.TodoList todos = getTodos()) {
            while (todos.hasNext()) {
                org.osid.checklist.Todo todo = todos.getNextTodo();
                if (todo.getId().equals(todoId)) {
                    return (todo);
                }
            }
        } 

        throw new org.osid.NotFoundException(todoId + " not found");
    }


    /**
     *  Gets a <code>TodoList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  todos specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Todos</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, todos are returned that are currently effective.
     *  In any effective mode, effective todos and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getTodos()</code>.
     *
     *  @param  todoIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>todoIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByIds(org.osid.id.IdList todoIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.checklist.Todo> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = todoIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getTodo(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("todo " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.checklist.todo.LinkedTodoList(ret));
    }


    /**
     *  Gets a <code>TodoList</code> corresponding to the given
     *  todo genus <code>Type</code> which does not include
     *  todos of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently effective.
     *  In any effective mode, effective todos and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getTodos()</code>.
     *
     *  @param  todoGenusType a todo genus type 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByGenusType(org.osid.type.Type todoGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.todo.TodoGenusFilterList(getTodos(), todoGenusType));
    }


    /**
     *  Gets a <code>TodoList</code> corresponding to the given
     *  todo genus <code>Type</code> and include any additional
     *  todos with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTodos()</code>.
     *
     *  @param  todoGenusType a todo genus type 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByParentGenusType(org.osid.type.Type todoGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTodosByGenusType(todoGenusType));
    }


    /**
     *  Gets a <code>TodoList</code> containing the given
     *  todo record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, [hobjects are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getTodos()</code>.
     *
     *  @param  todoRecordType a todo record type 
     *  @return the returned <code>Todo</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByRecordType(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.todo.TodoRecordFilterList(getTodos(), todoRecordType));
    }


    /**
     *  Gets a <code>TodoList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible
     *  through this session.
     *  
     *  In active mode, todos are returned that are currently
     *  active. In any status mode, active and inactive todos
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Todo</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.checklist.TodoList getTodosOnDate(org.osid.calendaring.DateTime from, 
                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.todo.TemporalTodoFilterList(getTodos(), from, to));
    }
        

    /**
     *  Gets a <code>TodoList</code> at the given priority
     *  <code>Type</code> or higher.
     *  
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *  
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *
     *  This method doesn't understand ordering of priority types.
     *
     *  @param  priorityType a priority type 
     *  @return the returned <code>Todo</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>priorityType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByPriority(org.osid.type.Type priorityType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.todo.TodoFilterList(new PriorityFilter(priorityType), getTodos()));
    }


    /**
     *  Gets a <code>TodoList</code> with a due date within the given
     *  date range inclusive.
     *  
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *  
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered todos are returned. In
     *  unsequestered mode, all todos are returned.
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned <code>Todo</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByDueDate(org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.checklist.todo.TodoFilterList(new DueDateFilter(from, to), getTodos()));
    }


    /**
     *  Gets a <code>TodoList</code> of todos dependent upon the given
     *  todo.
     *  
     *  In plenary mode, the returned list contains all known todos or
     *  an error results. Otherwise, the returned list may contain
     *  only those todos that are accessible through this session.
     *  
     *  In effective mode, todos are returned that are currently
     *  effective. In any effective mode, effective todos and those
     *  currently expired are returned.
     *  
     *  In sequestered mode, no sequestered todos are returned. In
     *  unsequestered mode, all todos are returned.
     *
     *  @param  dependencyTodoId a todo 
     *  @return the returned <code>Todo</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>dependencyTodoId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByDependency(org.osid.id.Id dependencyTodoId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        java.util.Collection<org.osid.checklist.Todo> ret = new java.util.ArrayList<>();

        try (org.osid.checklist.TodoList todos = getTodos()) {
            while (todos.hasNext()) {
                org.osid.checklist.Todo todo = todos.getNextTodo();                
                try (org.osid.id.IdList ids = todo.getDependencyIds()) {
                    while (ids.hasNext()) {
                        if (dependencyTodoId.equals(ids.getNextId())) {
                            ret.add(todo);
                        }
                    }
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.checklist.todo.LinkedTodoList(ret));
    }

    
    /**
     *  Gets all <code>Todos</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  todos or an error results. Otherwise, the returned list
     *  may contain only those todos that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, todos are returned that are currently
     *  effective.  In any effective mode, effective todos and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Todos</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.checklist.TodoList getTodos()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the todo list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of todos
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.checklist.TodoList filterTodosOnViews(org.osid.checklist.TodoList list)
        throws org.osid.OperationFailedException {
            
        org.osid.checklist.TodoList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.checklist.todo.EffectiveTodoFilterList(ret);
        }

        if (isSequestered()) {
            ret = new net.okapia.osid.jamocha.inline.filter.checklist.todo.SequesteredTodoFilterList(ret);
        }

        return (ret);
    }


    public static class DueDateFilter
        implements net.okapia.osid.jamocha.inline.filter.checklist.todo.TodoFilter {

        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;


        /**
         *  Constructs a new <code>DueDateFilter</code>.
         *
         *  @param from start of range
         *  @param to end of range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public DueDateFilter(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to) {
            nullarg(from, "from date");
            nullarg(to, "to date");

            this.from = from;
            this.to = to;

            return;
        }


        /**
         *  Used by the TodoFilterList to filter the todo
         *  list based on due date..
         *
         *  @param todo the todo
         *  @return <code>true</code> to pass the todo,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.checklist.Todo todo) {
            if (todo.getDueDate().isLess(this.from)) {
                return (false);
            }

            if (todo.getDueDate().isGreater(this.to)) {
                return (false);
            }

            return (true);
        }
    }        


    public static class PriorityFilter
        implements net.okapia.osid.jamocha.inline.filter.checklist.todo.TodoFilter {

        private final org.osid.type.Type priority;


        /**
         *  Constructs a new <code>PriorityFilter</code>.
         *
         *  @param priorityType a priority type
         *  @throws org.osid.NullArgumentException
         *          <code>priorityType</code> is <code>null</code>
         */

        public PriorityFilter(org.osid.type.Type priorityType) {
            nullarg(priorityType, "priority type");
            this.priority = priorityType;

            return;
        }


        /**
         *  Used by the TodoilterList to filter the todo list based on
         *  priority type.
         *
         *  @param todo the todo
         *  @return <code>true</code> to pass the todo,
         *          <code>false</code> to filter it
         */
       
        @Override
        public boolean pass(org.osid.checklist.Todo todo) {
            return (todo.getPriority().equals(this.priority));
        }
    }        
}
