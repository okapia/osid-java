//
// AbstractParameterProcessorEnablerNotificationSession.java
//
//     A template for making ParameterProcessorEnablerNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code ParameterProcessorEnabler} objects. This
 *  session is intended for consumers needing to synchronize their
 *  state with this service without the use of polling. Notifications
 *  are cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code ParameterProcessorEnabler} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for parameter processor enabler entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractParameterProcessorEnablerNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.configuration.rules.ParameterProcessorEnablerNotificationSession {

    private boolean federated = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();


    /**
     *  Gets the {@code Configuration} {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Configuration Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }

    
    /**
     *  Gets the {@code Configuration} associated with this session.
     *
     *  @return the {@code Configuration} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the {@code Configuration}.
     *
     *  @param configuration the configuration for this session
     *  @throws org.osid.NullArgumentException {@code configuration}
     *          is {@code null}
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can register for {@code
     *  ParameterProcessorEnabler} notifications.  A return of true
     *  does not guarantee successful authorization. A return of false
     *  indicates that it is known all methods in this session will
     *  result in a {@code PERMISSION_DENIED}. This is intended as a
     *  hint to an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForParameterProcessorEnablerNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeParameterProcessorEnablerNotification() </code>.
     */

    @OSID @Override
    public void reliableParameterProcessorEnablerNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableParameterProcessorEnablerNotifications() {
        return;
    }


    /**
     *  Acknowledge a parameter processor enabler notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeParameterProcessorEnablerNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for parameter processor
     *  enablers in configurations which are children of this
     *  configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new parameter processor
     *  enablers. {@code
     *  ParameterProcessorEnablerReceiver.newParameterProcessorEnabler()}
     *  is invoked when a new {@code ParameterProcessorEnabler} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewParameterProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated parameter processor
     *  enablers. {@code
     *  ParameterProcessorEnablerReceiver.changedParameterProcessorEnabler()}
     *  is invoked when a parameter processor enabler is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedParameterProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated parameter processor
     *  enabler. {@code
     *  ParameterProcessorEnablerReceiver.changedParameterProcessorEnabler()}
     *  is invoked when the specified parameter processor enabler is
     *  changed.
     *
     *  @param parameterProcessorEnablerId the {@code Id} of the {@code ParameterProcessorEnabler} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code parameterProcessorEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedParameterProcessorEnabler(org.osid.id.Id parameterProcessorEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted parameter processor
     *  enablers. {@code
     *  ParameterProcessorEnablerReceiver.deletedParameterProcessorEnabler()}
     *  is invoked when a parameter processor enabler is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedParameterProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted parameter processor
     *  enabler. {@code
     *  ParameterProcessorEnablerReceiver.deletedParameterProcessorEnabler()}
     *  is invoked when the specified parameter processor enabler is
     *  deleted.
     *
     *  @param parameterProcessorEnablerId the {@code Id} of the
     *          {@code ParameterProcessorEnabler} to monitor
     *  @throws org.osid.NullArgumentException {@code parameterProcessorEnablerId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedParameterProcessorEnabler(org.osid.id.Id parameterProcessorEnablerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
