//
// AbstractFederatingSequenceRuleLookupSession.java
//
//     An abstract federating adapter for a SequenceRuleLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SequenceRuleLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSequenceRuleLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.assessment.authoring.SequenceRuleLookupSession>
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {

    private boolean parallel = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();


    /**
     *  Constructs a new <code>AbstractFederatingSequenceRuleLookupSession</code>.
     */

    protected AbstractFederatingSequenceRuleLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.assessment.authoring.SequenceRuleLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }


    /**
     *  Tests if this user can perform <code>SequenceRule</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSequenceRules() {
        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            if (session.canLookupSequenceRules()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>SequenceRule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSequenceRuleView() {
        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            session.useComparativeSequenceRuleView();
        }

        return;
    }


    /**
     *  A complete view of the <code>SequenceRule</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySequenceRuleView() {
        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            session.usePlenarySequenceRuleView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include sequence rules in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            session.useFederatedBankView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            session.useIsolatedBankView();
        }

        return;
    }


    /**
     *  Only active sequence rules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSequenceRuleView() {
        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            session.useActiveSequenceRuleView();
        }

        return;
    }


    /**
     *  Active and inactive sequence rules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSequenceRuleView() {
        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            session.useAnyStatusSequenceRuleView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>SequenceRule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SequenceRule</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SequenceRule</code> and
     *  retained for compatibility.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleId <code>Id</code> of the
     *          <code>SequenceRule</code>
     *  @return the sequence rule
     *  @throws org.osid.NotFoundException <code>sequenceRuleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>sequenceRuleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRule getSequenceRule(org.osid.id.Id sequenceRuleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            try {
                return (session.getSequenceRule(sequenceRuleId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(sequenceRuleId + " not found");
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  sequenceRules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SequenceRules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByIds(org.osid.id.IdList sequenceRuleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.assessment.authoring.sequencerule.MutableSequenceRuleList ret = new net.okapia.osid.jamocha.assessment.authoring.sequencerule.MutableSequenceRuleList();

        try (org.osid.id.IdList ids = sequenceRuleIds) {
            while (ids.hasNext()) {
                ret.addSequenceRule(getSequenceRule(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  sequence rule genus <code>Type</code> which does not include
     *  sequence rules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleGenusType a sequenceRule genus type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList ret = getSequenceRuleList();

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            ret.addSequenceRuleList(session.getSequenceRulesByGenusType(sequenceRuleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  sequence rule genus <code>Type</code> and include any additional
     *  sequence rules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleGenusType a sequenceRule genus type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByParentGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList ret = getSequenceRuleList();

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            ret.addSequenceRuleList(session.getSequenceRulesByParentGenusType(sequenceRuleGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleList</code> containing the given
     *  sequence rule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleRecordType a sequenceRule record type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByRecordType(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList ret = getSequenceRuleList();

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            ret.addSequenceRuleList(session.getSequenceRulesByRecordType(sequenceRuleRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleList</code> for the given source
     *  assessment part.
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentPartId an assessment part <code> Id </code>
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessmentPart(org.osid.id.Id assessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList ret = getSequenceRuleList();

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            ret.addSequenceRuleList(session.getSequenceRulesForAssessmentPart(assessmentPartId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleList</code> for the given target
     *  assessment part.
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  nextAssessmentPartId an assessment part <code> Id </code> 
     *  @return the returned <code>SequenceRule</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>nextAssessmentPartId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForNextAssessmentPart(org.osid.id.Id nextAssessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList ret = getSequenceRuleList();

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            ret.addSequenceRuleList(session.getSequenceRulesForNextAssessmentPart(nextAssessmentPartId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleList</code> for the given source and
     *  target assessment parts.
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentPartId source assessment part <code>Id</code> 
     *  @param  nextAssessmentPartId target assessment part <code>Id</code> 
     *  @return the returned <code>SequenceRule</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartId</code> or
     *          <code>nextAssessmentPartId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessmentParts(org.osid.id.Id assessmentPartId, 
                                                                                             org.osid.id.Id nextAssessmentPartId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList ret = getSequenceRuleList();

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            ret.addSequenceRuleList(session.getSequenceRulesForAssessmentParts(assessmentPartId, nextAssessmentPartId));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets a <code>SequenceRuleList</code> for an entire assessment. 
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule or an error results. Otherwise, the returned list may
     *  contain only those sequence rule that are accessible through
     *  this session.
     *  
     *  In active mode, sequence rules are returned that are currently
     *  active.  In any status mode, active and inactive sequence
     *  rules are returned.
     *
     *  @param  assessmentId an assessment <code>Id</code> 
     *  @return the returned <code>SequenceRule</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList ret = getSequenceRuleList();

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            ret.addSequenceRuleList(session.getSequenceRulesForAssessment(assessmentId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>SequenceRules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @return a list of <code>SequenceRules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList ret = getSequenceRuleList();

        for (org.osid.assessment.authoring.SequenceRuleLookupSession session : getSessions()) {
            ret.addSequenceRuleList(session.getSequenceRules());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.FederatingSequenceRuleList getSequenceRuleList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.ParallelSequenceRuleList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequencerule.CompositeSequenceRuleList());
        }
    }
}
