//
// InstructionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.instruction.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class InstructionElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the InstructionElement Id.
     *
     *  @return the instruction element Id
     */

    public static org.osid.id.Id getInstructionEntityId() {
        return (makeEntityId("osid.rules.check.Instruction"));
    }


    /**
     *  Gets the AgendaId element Id.
     *
     *  @return the AgendaId element Id
     */

    public static org.osid.id.Id getAgendaId() {
        return (makeElementId("osid.rules.check.instruction.AgendaId"));
    }


    /**
     *  Gets the Agenda element Id.
     *
     *  @return the Agenda element Id
     */

    public static org.osid.id.Id getAgenda() {
        return (makeElementId("osid.rules.check.instruction.Agenda"));
    }


    /**
     *  Gets the CheckId element Id.
     *
     *  @return the CheckId element Id
     */

    public static org.osid.id.Id getCheckId() {
        return (makeElementId("osid.rules.check.instruction.CheckId"));
    }


    /**
     *  Gets the Check element Id.
     *
     *  @return the Check element Id
     */

    public static org.osid.id.Id getCheck() {
        return (makeElementId("osid.rules.check.instruction.Check"));
    }


    /**
     *  Gets the Message element Id.
     *
     *  @return the Message element Id
     */

    public static org.osid.id.Id getMessage() {
        return (makeElementId("osid.rules.check.instruction.Message"));
    }


    /**
     *  Gets the Warning element Id.
     *
     *  @return the Warning element Id
     */

    public static org.osid.id.Id getWarning() {
        return (makeElementId("osid.rules.check.instruction.Warning"));
    }


    /**
     *  Gets the ContinueOnFail element Id.
     *
     *  @return the ContinueOnFail element Id
     */

    public static org.osid.id.Id getContinueOnFail() {
        return (makeElementId("osid.rules.check.instruction.ContinueOnFail"));
    }


    /**
     *  Gets the EngineId element Id.
     *
     *  @return the EngineId element Id
     */

    public static org.osid.id.Id getEngineId() {
        return (makeQueryElementId("osid.rules.check.instruction.EngineId"));
    }


    /**
     *  Gets the Engine element Id.
     *
     *  @return the Engine element Id
     */

    public static org.osid.id.Id getEngine() {
        return (makeQueryElementId("osid.rules.check.instruction.Engine"));
    }
}
