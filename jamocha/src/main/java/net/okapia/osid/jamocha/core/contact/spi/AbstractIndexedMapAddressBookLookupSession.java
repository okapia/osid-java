//
// AbstractIndexedMapAddressBookLookupSession.java
//
//    A simple framework for providing an AddressBook lookup service
//    backed by a fixed collection of address books with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.contact.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AddressBook lookup service backed by a
 *  fixed collection of address books. The address books are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some address books may be compatible
 *  with more types than are indicated through these address book
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AddressBooks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAddressBookLookupSession
    extends AbstractMapAddressBookLookupSession
    implements org.osid.contact.AddressBookLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.contact.AddressBook> addressBooksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.contact.AddressBook>());
    private final MultiMap<org.osid.type.Type, org.osid.contact.AddressBook> addressBooksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.contact.AddressBook>());


    /**
     *  Makes an <code>AddressBook</code> available in this session.
     *
     *  @param  addressBook an address book
     *  @throws org.osid.NullArgumentException <code>addressBook<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAddressBook(org.osid.contact.AddressBook addressBook) {
        super.putAddressBook(addressBook);

        this.addressBooksByGenus.put(addressBook.getGenusType(), addressBook);
        
        try (org.osid.type.TypeList types = addressBook.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.addressBooksByRecord.put(types.getNextType(), addressBook);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an address book from this session.
     *
     *  @param addressBookId the <code>Id</code> of the address book
     *  @throws org.osid.NullArgumentException <code>addressBookId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAddressBook(org.osid.id.Id addressBookId) {
        org.osid.contact.AddressBook addressBook;
        try {
            addressBook = getAddressBook(addressBookId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.addressBooksByGenus.remove(addressBook.getGenusType());

        try (org.osid.type.TypeList types = addressBook.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.addressBooksByRecord.remove(types.getNextType(), addressBook);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAddressBook(addressBookId);
        return;
    }


    /**
     *  Gets an <code>AddressBookList</code> corresponding to the given
     *  address book genus <code>Type</code> which does not include
     *  address books of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known address books or an error results. Otherwise,
     *  the returned list may contain only those address books that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  addressBookGenusType an address book genus type 
     *  @return the returned <code>AddressBook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByGenusType(org.osid.type.Type addressBookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.addressbook.ArrayAddressBookList(this.addressBooksByGenus.get(addressBookGenusType)));
    }


    /**
     *  Gets an <code>AddressBookList</code> containing the given
     *  address book record <code>Type</code>. In plenary mode, the
     *  returned list contains all known address books or an error
     *  results. Otherwise, the returned list may contain only those
     *  address books that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  addressBookRecordType an address book record type 
     *  @return the returned <code>addressBook</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooksByRecordType(org.osid.type.Type addressBookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.contact.addressbook.ArrayAddressBookList(this.addressBooksByRecord.get(addressBookRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.addressBooksByGenus.clear();
        this.addressBooksByRecord.clear();

        super.close();

        return;
    }
}
