//
// AbstractFederatingOntologyLookupSession.java
//
//     An abstract federating adapter for an OntologyLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  OntologyLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingOntologyLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.ontology.OntologyLookupSession>
    implements org.osid.ontology.OntologyLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingOntologyLookupSession</code>.
     */

    protected AbstractFederatingOntologyLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.ontology.OntologyLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Ontology</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOntologies() {
        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            if (session.canLookupOntologies()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Ontology</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOntologyView() {
        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            session.useComparativeOntologyView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Ontology</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOntologyView() {
        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            session.usePlenaryOntologyView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Ontology</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Ontology</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Ontology</code> and
     *  retained for compatibility.
     *
     *  @param  ontologyId <code>Id</code> of the
     *          <code>Ontology</code>
     *  @return the ontology
     *  @throws org.osid.NotFoundException <code>ontologyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ontologyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            try {
                return (session.getOntology(ontologyId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(ontologyId + " not found");
    }


    /**
     *  Gets an <code>OntologyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ontologies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Ontologies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  ontologyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByIds(org.osid.id.IdList ontologyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.ontology.ontology.MutableOntologyList ret = new net.okapia.osid.jamocha.ontology.ontology.MutableOntologyList();

        try (org.osid.id.IdList ids = ontologyIds) {
            while (ids.hasNext()) {
                ret.addOntology(getOntology(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>OntologyList</code> corresponding to the given
     *  ontology genus <code>Type</code> which does not include
     *  ontologies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyGenusType an ontology genus type 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByGenusType(org.osid.type.Type ontologyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.ontology.FederatingOntologyList ret = getOntologyList();

        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            ret.addOntologyList(session.getOntologiesByGenusType(ontologyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>OntologyList</code> corresponding to the given
     *  ontology genus <code>Type</code> and include any additional
     *  ontologies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyGenusType an ontology genus type 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByParentGenusType(org.osid.type.Type ontologyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.ontology.FederatingOntologyList ret = getOntologyList();

        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            ret.addOntologyList(session.getOntologiesByParentGenusType(ontologyGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>OntologyList</code> containing the given
     *  ontology record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyRecordType an ontology record type 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByRecordType(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.ontology.FederatingOntologyList ret = getOntologyList();

        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            ret.addOntologyList(session.getOntologiesByRecordType(ontologyRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>OntologyList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known ontologies or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  ontologies that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Ontology</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.ontology.ontology.FederatingOntologyList ret = getOntologyList();

        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            ret.addOntologyList(session.getOntologiesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Ontologies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Ontologies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.ontology.ontology.FederatingOntologyList ret = getOntologyList();

        for (org.osid.ontology.OntologyLookupSession session : getSessions()) {
            ret.addOntologyList(session.getOntologies());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.ontology.ontology.FederatingOntologyList getOntologyList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.ontology.ontology.ParallelOntologyList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.ontology.ontology.CompositeOntologyList());
        }
    }
}
