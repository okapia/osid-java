//
// AbstractAgencyLookupSession.java
//
//    A starter implementation framework for providing an Agency
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing an Agency
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAgencies(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAgencyLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.authentication.AgencyLookupSession {

    private boolean pedantic = false;
    private org.osid.authentication.Agency agency = new net.okapia.osid.jamocha.nil.authentication.agency.UnknownAgency();
    

    /**
     *  Tests if this user can perform <code>Agency</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAgencies() {
        return (true);
    }


    /**
     *  A complete view of the <code>Agency</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAgencyView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Agency</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAgencyView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Agency</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code>Agency</code> may have a different <code> Id </code>
     *  than requested, such as the case where a duplicate <code> Id
     *  </code> was assigned to a <code>Agency</code> and retained
     *  for compatibility.
     *
     *  @param  agencyId <code>Id</code> of the
     *          <code>Agency</code>
     *  @return the agency
     *  @throws org.osid.NotFoundException <code>agencyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>agencyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.authentication.AgencyList agencies = getAgencies()) {
            while (agencies.hasNext()) {
                org.osid.authentication.Agency agency = agencies.getNextAgency();
                if (agency.getId().equals(agencyId)) {
                    return (agency);
                }
            }
        } 

        throw new org.osid.NotFoundException(agencyId + " not found");
    }


    /**
     *  Gets a <code>AgencyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  agencies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Agencies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAgencies()</code>.
     *
     *  @param  agencyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>agencyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByIds(org.osid.id.IdList agencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.authentication.Agency> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = agencyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAgency(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("agency " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.authentication.agency.LinkedAgencyList(ret));
    }


    /**
     *  Gets a <code>AgencyList</code> corresponding to the given
     *  agency genus <code>Type</code> which does not include
     *  agencies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAgencies()</code>.
     *
     *  @param  agencyGenusType an agency genus type 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByGenusType(org.osid.type.Type agencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authentication.agency.AgencyGenusFilterList(getAgencies(), agencyGenusType));
    }


    /**
     *  Gets a <code>AgencyList</code> corresponding to the given
     *  agency genus <code>Type</code> and include any additional
     *  agencies with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAgencies()</code>.
     *
     *  @param  agencyGenusType an agency genus type 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agencyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByParentGenusType(org.osid.type.Type agencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAgenciesByGenusType(agencyGenusType));
    }


    /**
     *  Gets a <code>AgencyList</code> containing the given
     *  agency record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAgencies()</code>.
     *
     *  @param  agencyRecordType an agency record type 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByRecordType(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.authentication.agency.AgencyRecordFilterList(getAgencies(), agencyRecordType));
    }


    /**
     *  Gets an <code>AgencyList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known agencies
     *  or an error results. Otherwise, the returned list may contain
     *  only those agencies that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Agency</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.authentication.agency.AgencyProviderFilterList(getAgencies(), resourceId));
    }


    /**
     *  Gets all <code>Agencies</code>. 
     *
     *  In plenary mode, the returned list contains all known agencies
     *  or an error results. Otherwise, the returned list may contain
     *  only those agencies that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Agencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.authentication.AgencyList getAgencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the agency list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of agencies
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.authentication.AgencyList filterAgenciesOnViews(org.osid.authentication.AgencyList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }
}
