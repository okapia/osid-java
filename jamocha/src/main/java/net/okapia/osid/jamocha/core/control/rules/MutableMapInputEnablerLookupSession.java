//
// MutableMapInputEnablerLookupSession
//
//    Implements an InputEnabler lookup service backed by a collection of
//    inputEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules;


/**
 *  Implements an InputEnabler lookup service backed by a collection of
 *  input enablers. The input enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of input enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapInputEnablerLookupSession
    extends net.okapia.osid.jamocha.core.control.rules.spi.AbstractMapInputEnablerLookupSession
    implements org.osid.control.rules.InputEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapInputEnablerLookupSession}
     *  with no input enablers.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system} is
     *          {@code null}
     */

      public MutableMapInputEnablerLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInputEnablerLookupSession} with a
     *  single inputEnabler.
     *
     *  @param system the system  
     *  @param inputEnabler an input enabler
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code inputEnabler} is {@code null}
     */

    public MutableMapInputEnablerLookupSession(org.osid.control.System system,
                                           org.osid.control.rules.InputEnabler inputEnabler) {
        this(system);
        putInputEnabler(inputEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInputEnablerLookupSession}
     *  using an array of input enablers.
     *
     *  @param system the system
     *  @param inputEnablers an array of input enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code inputEnablers} is {@code null}
     */

    public MutableMapInputEnablerLookupSession(org.osid.control.System system,
                                           org.osid.control.rules.InputEnabler[] inputEnablers) {
        this(system);
        putInputEnablers(inputEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInputEnablerLookupSession}
     *  using a collection of input enablers.
     *
     *  @param system the system
     *  @param inputEnablers a collection of input enablers
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code inputEnablers} is {@code null}
     */

    public MutableMapInputEnablerLookupSession(org.osid.control.System system,
                                           java.util.Collection<? extends org.osid.control.rules.InputEnabler> inputEnablers) {

        this(system);
        putInputEnablers(inputEnablers);
        return;
    }

    
    /**
     *  Makes an {@code InputEnabler} available in this session.
     *
     *  @param inputEnabler an input enabler
     *  @throws org.osid.NullArgumentException {@code inputEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putInputEnabler(org.osid.control.rules.InputEnabler inputEnabler) {
        super.putInputEnabler(inputEnabler);
        return;
    }


    /**
     *  Makes an array of input enablers available in this session.
     *
     *  @param inputEnablers an array of input enablers
     *  @throws org.osid.NullArgumentException {@code inputEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putInputEnablers(org.osid.control.rules.InputEnabler[] inputEnablers) {
        super.putInputEnablers(inputEnablers);
        return;
    }


    /**
     *  Makes collection of input enablers available in this session.
     *
     *  @param inputEnablers a collection of input enablers
     *  @throws org.osid.NullArgumentException {@code inputEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putInputEnablers(java.util.Collection<? extends org.osid.control.rules.InputEnabler> inputEnablers) {
        super.putInputEnablers(inputEnablers);
        return;
    }


    /**
     *  Removes an InputEnabler from this session.
     *
     *  @param inputEnablerId the {@code Id} of the input enabler
     *  @throws org.osid.NullArgumentException {@code inputEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeInputEnabler(org.osid.id.Id inputEnablerId) {
        super.removeInputEnabler(inputEnablerId);
        return;
    }    
}
