//
// AbstractMeteringBatchManager.java
//
//     An adapter for a MeteringBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.metering.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MeteringBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMeteringBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.metering.batch.MeteringBatchManager>
    implements org.osid.metering.batch.MeteringBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterMeteringBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMeteringBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMeteringBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMeteringBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of meters is available. 
     *
     *  @return <code> true </code> if a meter bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterBatchAdmin() {
        return (getAdapteeManager().supportsMeterBatchAdmin());
    }


    /**
     *  Tests if bulk administration of utilities is available. 
     *
     *  @return <code> true </code> if a utility bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityBatchAdmin() {
        return (getAdapteeManager().supportsUtilityBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk meter 
     *  administration service. 
     *
     *  @return a <code> MeterBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.batch.MeterBatchAdminSession getMeterBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk meter 
     *  administration service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.batch.MeterBatchAdminSession getMeterBatchAdminSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterBatchAdminSessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk utility 
     *  administration service. 
     *
     *  @return a <code> UtilityBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.batch.UtilityBatchAdminSession getUtilityBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUtilityBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
