//
// BudgetElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budget.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BudgetElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the BudgetElement Id.
     *
     *  @return the budget element Id
     */

    public static org.osid.id.Id getBudgetEntityId() {
        return (makeEntityId("osid.financials.budgeting.Budget"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeElementId("osid.financials.budgeting.budget.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeElementId("osid.financials.budgeting.budget.Activity"));
    }


    /**
     *  Gets the FiscalPeriodId element Id.
     *
     *  @return the FiscalPeriodId element Id
     */

    public static org.osid.id.Id getFiscalPeriodId() {
        return (makeElementId("osid.financials.budgeting.budget.FiscalPeriodId"));
    }


    /**
     *  Gets the FiscalPeriod element Id.
     *
     *  @return the FiscalPeriod element Id
     */

    public static org.osid.id.Id getFiscalPeriod() {
        return (makeElementId("osid.financials.budgeting.budget.FiscalPeriod"));
    }


    /**
     *  Gets the BudgetEntryId element Id.
     *
     *  @return the BudgetEntryId element Id
     */

    public static org.osid.id.Id getBudgetEntryId() {
        return (makeQueryElementId("osid.financials.budgeting.budget.BudgetEntryId"));
    }


    /**
     *  Gets the BudgetEntry element Id.
     *
     *  @return the BudgetEntry element Id
     */

    public static org.osid.id.Id getBudgetEntry() {
        return (makeQueryElementId("osid.financials.budgeting.budget.BudgetEntry"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.financials.budgeting.budget.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.financials.budgeting.budget.Business"));
    }
}
