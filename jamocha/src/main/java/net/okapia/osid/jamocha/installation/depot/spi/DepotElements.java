//
// DepotElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.depot.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class DepotElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the DepotElement Id.
     *
     *  @return the depot element Id
     */

    public static org.osid.id.Id getDepotEntityId() {
        return (makeEntityId("osid.installation.Depot"));
    }


    /**
     *  Gets the PackageId element Id.
     *
     *  @return the PackageId element Id
     */

    public static org.osid.id.Id getPackageId() {
        return (makeQueryElementId("osid.installation.depot.PackageId"));
    }


    /**
     *  Gets the Package element Id.
     *
     *  @return the Package element Id
     */

    public static org.osid.id.Id getPackage() {
        return (makeQueryElementId("osid.installation.depot.Package"));
    }


    /**
     *  Gets the AncestorDepotId element Id.
     *
     *  @return the AncestorDepotId element Id
     */

    public static org.osid.id.Id getAncestorDepotId() {
        return (makeQueryElementId("osid.installation.depot.AncestorDepotId"));
    }


    /**
     *  Gets the AncestorDepot element Id.
     *
     *  @return the AncestorDepot element Id
     */

    public static org.osid.id.Id getAncestorDepot() {
        return (makeQueryElementId("osid.installation.depot.AncestorDepot"));
    }


    /**
     *  Gets the DescendantDepotId element Id.
     *
     *  @return the DescendantDepotId element Id
     */

    public static org.osid.id.Id getDescendantDepotId() {
        return (makeQueryElementId("osid.installation.depot.DescendantDepotId"));
    }


    /**
     *  Gets the DescendantDepot element Id.
     *
     *  @return the DescendantDepot element Id
     */

    public static org.osid.id.Id getDescendantDepot() {
        return (makeQueryElementId("osid.installation.depot.DescendantDepot"));
    }
}
