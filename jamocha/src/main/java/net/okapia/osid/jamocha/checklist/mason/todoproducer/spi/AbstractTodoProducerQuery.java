//
// AbstractTodoProducerQuery.java
//
//     A template for making a TodoProducer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.mason.todoproducer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for todo producers.
 */

public abstract class AbstractTodoProducerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.checklist.mason.TodoProducerQuery {

    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches producers that create todos. 
     *
     *  @param  match <code> true </code> to match creation rules, <code> 
     *          false </code> to match destruction rules 
     */

    @OSID @Override
    public void matchCreationRule(boolean match) {
        return;
    }


    /**
     *  Clears the creation rule query terms. 
     */

    @OSID @Override
    public void clearCreationRuleTerms() {
        return;
    }


    /**
     *  Matches producers based on a cyclic event. 
     *
     *  @param  cyclicEventId the cyclic event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cyclicEventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCyclicEventId(org.osid.id.Id cyclicEventId, boolean match) {
        return;
    }


    /**
     *  Clears the cyclic event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCyclicEventIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CyclicEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a cyclic event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cyclic event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the cyclic event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuery getCyclicEventQuery() {
        throw new org.osid.UnimplementedException("supportsCyclicEventQuery() is false");
    }


    /**
     *  Matches producers based on any cyclic event. 
     *
     *  @param  match <code> true </code> for producers based on any cyclic 
     *          event, <code> false </code> to match producers based on no 
     *          cyclic event 
     */

    @OSID @Override
    public void matchAnyCyclicEvent(boolean match) {
        return;
    }


    /**
     *  Clears the cyclic event query terms. 
     */

    @OSID @Override
    public void clearCyclicEventTerms() {
        return;
    }


    /**
     *  Matches producers based on a stock. 
     *
     *  @param  stockId the stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        return;
    }


    /**
     *  Clears the stock <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Matches producers based on any stock. 
     *
     *  @param  match <code> true </code> for producers based on any stock, 
     *          <code> false </code> to match producers based on no stock 
     */

    @OSID @Override
    public void matchAnyStock(boolean match) {
        return;
    }


    /**
     *  Clears the stock query terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        return;
    }


    /**
     *  Matches producers with a stock level between the given range 
     *  inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchStockLevel(long from, long to, boolean match) {
        return;
    }


    /**
     *  Matches producers with any stock level. 
     *
     *  @param  match <code> true </code> for producers with any stock, <code> 
     *          false </code> to match producers with no stock 
     */

    @OSID @Override
    public void matchAnyStockLevel(boolean match) {
        return;
    }


    /**
     *  Clears the stock level query terms. 
     */

    @OSID @Override
    public void clearStockLevelTerms() {
        return;
    }


    /**
     *  Matches producers producing to the todo. 
     *
     *  @param  todoId the todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProducedTodoId(org.osid.id.Id todoId, boolean match) {
        return;
    }


    /**
     *  Clears the produced todo <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProducedTodoIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TodoBookQuery </code> is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProducedTodoQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the todo query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProducedTodoQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getProducedTodoQuery() {
        throw new org.osid.UnimplementedException("supportsProducedTodoQuery() is false");
    }


    /**
     *  Matches producers producing to any todo. 
     *
     *  @param  match <code> true </code> for producers producing any todo, 
     *          <code> false </code> to match producers producing no todo 
     */

    @OSID @Override
    public void matchAnyProducedTodo(boolean match) {
        return;
    }


    /**
     *  Clears the todo query terms. 
     */

    @OSID @Override
    public void clearProducedTodoTerms() {
        return;
    }


    /**
     *  Matches producers mapped to the checklist. 
     *
     *  @param  checklistId the checklist <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchChecklistId(org.osid.id.Id checklistId, boolean match) {
        return;
    }


    /**
     *  Clears the checklist <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearChecklistIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ChecklistQuery </code> is available. 
     *
     *  @return <code> true </code> if a checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistQuery() {
        return (false);
    }


    /**
     *  Gets the query for a checklist. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuery getChecklistQuery() {
        throw new org.osid.UnimplementedException("supportsChecklistQuery() is false");
    }


    /**
     *  Clears the checklist query terms. 
     */

    @OSID @Override
    public void clearChecklistTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given todo producer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a todo producer implementing the requested record.
     *
     *  @param todoProducerRecordType a todo producer record type
     *  @return the todo producer query record
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoProducerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerQueryRecord getTodoProducerQueryRecord(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.mason.records.TodoProducerQueryRecord record : this.records) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoProducerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this todo producer query. 
     *
     *  @param todoProducerQueryRecord todo producer query record
     *  @param todoProducerRecordType todoProducer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTodoProducerQueryRecord(org.osid.checklist.mason.records.TodoProducerQueryRecord todoProducerQueryRecord, 
                                          org.osid.type.Type todoProducerRecordType) {

        addRecordType(todoProducerRecordType);
        nullarg(todoProducerQueryRecord, "todo producer query record");
        this.records.add(todoProducerQueryRecord);        
        return;
    }
}
