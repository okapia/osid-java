//
// AbstractDistanceMetadata.java
//
//     Defines a distance Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a distance Metadata.
 */

public abstract class AbstractDistanceMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private org.osid.mapping.DistanceResolution resolution;
    private org.osid.mapping.Distance minimum = new net.okapia.osid.primordium.mapping.Distance(0);
    private org.osid.mapping.Distance maximum = new net.okapia.osid.primordium.mapping.Distance(1, org.osid.mapping.DistanceResolution.LIGHTYEAR);

    private final java.util.Collection<org.osid.mapping.Distance> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.Distance> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.Distance> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractDistanceMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractDistanceMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.DISTANCE, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractDistanceMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractDistanceMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.DISTANCE, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the smallest resolution of the distance value. 
     *
     *  @return the resolution 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DISTANCE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.DistanceResolution getDistanceResolution() {
        return (this.resolution);
    }


    /**
     *  Sets the smallest resolution accepted.
     *
     *  @param resolution the smallest distance resolution
     *  @throws org.osid.NullArgumentException {@code resolution} is
     *          {@code null}
     */

    protected void setDistanceResolution(org.osid.mapping.DistanceResolution resolution) {
        nullarg(resolution, "resolution");
        this.resolution = resolution;
        return;
    }


    /**
     *  Gets the minimum distance value. 
     *
     *  @return the minimum distance 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DISTANCE </code>
     */

    @OSID @Override
    public org.osid.mapping.Distance getMinimumDistance() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum distance value. 
     *
     *  @return the maximum distance 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DISTANCE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance getMaximumDistance() {
        return (this.maximum);
    }

    
    /**
     *  Sets the distance range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     */

    protected void setDistanceRange(org.osid.mapping.Distance min, org.osid.mapping.Distance max) {
        nullarg(min, "min");
        nullarg(max, "max");

        if (min.compareTo(max) > 0) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.minimum = min;
        this.maximum = max;

        return;
    }


    /**
     *  Gets the set of acceptable distance values. 
     *
     *  @return a set of distances or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DISTANCE </code>
     */

    @OSID @Override
    public org.osid.mapping.Distance[] getDistanceSet() {
        return (this.set.toArray(new org.osid.mapping.Distance[this.set.size()]));
    }

    
    /**
     *  Sets the distance set.
     *
     *  @param values a collection of accepted distance values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDistanceSet(java.util.Collection<org.osid.mapping.Distance> values) {
        this.set.clear();
        addToDistanceSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the distance set.
     *
     *  @param values a collection of accepted distance values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToDistanceSet(java.util.Collection<org.osid.mapping.Distance> values) {
        nullarg(values, "distance set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the distance set.
     *
     *  @param value a distance value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addToDistanceSet(org.osid.mapping.Distance value) {
        nullarg(value, "distance value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the distance set.
     *
     *  @param value a distance value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeFromDistanceSet(org.osid.mapping.Distance value) {
        nullarg(value, "distance value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the distance set.
     */

    protected void clearDistanceSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default distance values. These are the values used if
     *  the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default distance values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          DISTANCE </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.mapping.Distance[] getDefaultDistanceValues() {
        return (this.defvals.toArray(new org.osid.mapping.Distance[this.defvals.size()]));
    }


    /**
     *  Sets the default distance set.
     *
     *  @param values a collection of default distance values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultDistanceValues(java.util.Collection<org.osid.mapping.Distance> values) {
        clearDefaultDistanceValues();
        addDefaultDistanceValues(values);
        return;
    }


    /**
     *  Adds a collection of default distance values.
     *
     *  @param values a collection of default distance values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultDistanceValues(java.util.Collection<org.osid.mapping.Distance> values) {
        nullarg(values, "default distance values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultDistanceValue(org.osid.mapping.Distance value) {
        nullarg(value, "default distance value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultDistanceValue(org.osid.mapping.Distance value) {
        nullarg(value, "default distance value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default distance values.
     */

    protected void clearDefaultDistanceValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing distance values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing distance values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          DISTANCE </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance[] getExistingDistanceValues() {
        return (this.existing.toArray(new org.osid.mapping.Distance[this.existing.size()]));
    }


    /**
     *  Sets the existing distance set.
     *
     *  @param values a collection of existing distance values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingDistanceValues(java.util.Collection<org.osid.mapping.Distance> values) {
        clearExistingDistanceValues();
        addExistingDistanceValues(values);
        return;
    }


    /**
     *  Adds a collection of existing distance values.
     *
     *  @param values a collection of existing distance values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingDistanceValues(java.util.Collection<org.osid.mapping.Distance> values) {
        nullarg(values, "existing distance values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingDistanceValue(org.osid.mapping.Distance value) {
        nullarg(value, "existing distance value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing distance value.
     *
     *  @param value a distance value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingDistanceValue(org.osid.mapping.Distance value) {
        nullarg(value, "existing distance value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing distance values.
     */

    protected void clearExistingDistanceValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }    
}