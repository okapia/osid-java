//
// AbstractRecurringEventEnabler.java
//
//     Defines a RecurringEventEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.rules.recurringeventenabler.spi;


/**
 *  Defines a <code>RecurringEventEnabler</code> builder.
 */

public abstract class AbstractRecurringEventEnablerBuilder<T extends AbstractRecurringEventEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.rules.recurringeventenabler.RecurringEventEnablerMiter recurringEventEnabler;


    /**
     *  Constructs a new <code>AbstractRecurringEventEnablerBuilder</code>.
     *
     *  @param recurringEventEnabler the recurring event enabler
     *         to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRecurringEventEnablerBuilder(net.okapia.osid.jamocha.builder.calendaring.rules.recurringeventenabler.RecurringEventEnablerMiter recurringEventEnabler) {
        super(recurringEventEnabler);
        this.recurringEventEnabler = recurringEventEnabler;
        return;
    }


    /**
     *  Builds the recurring event enabler.
     *
     *  @return the new recurring event enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.rules.RecurringEventEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.rules.recurringeventenabler.RecurringEventEnablerValidator(getValidations())).validate(this.recurringEventEnabler);
        return (new net.okapia.osid.jamocha.builder.calendaring.rules.recurringeventenabler.ImmutableRecurringEventEnabler(this.recurringEventEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the recurring event enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.rules.recurringeventenabler.RecurringEventEnablerMiter getMiter() {
        return (this.recurringEventEnabler);
    }


    /**
     *  Adds a RecurringEventEnabler record.
     *
     *  @param record a recurring event enabler record
     *  @param recordType the type of recurring event enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.rules.records.RecurringEventEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addRecurringEventEnablerRecord(record, recordType);
        return (self());
    }
}       


