//
// AbstractAuthenticationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAuthenticationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.authentication.AuthenticationManager,
               org.osid.authentication.AuthenticationProxyManager {

    private final Types agentRecordTypes                   = new TypeRefSet();
    private final Types agentSearchRecordTypes             = new TypeRefSet();

    private final Types agencyRecordTypes                  = new TypeRefSet();
    private final Types agencySearchRecordTypes            = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractAuthenticationManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAuthenticationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests is authentication acquisition is supported. Authentication 
     *  acquisition is responsible for acquiring client side authentication 
     *  credentials. 
     *
     *  @return <code> true </code> if authentication acquisiiton is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationAcquisition() {
        return (false);
    }


    /**
     *  Tests if authentication validation is supported. Authentication 
     *  validation verifies given authentication credentials and maps to an 
     *  agent identity. 
     *
     *  @return <code> true </code> if authentication validation is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationValidation() {
        return (false);
    }


    /**
     *  Tests if an agent lookup service is supported. An agent lookup service 
     *  defines methods to access agents. 
     *
     *  @return <code> true </code> if agent lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentLookup() {
        return (false);
    }


    /**
     *  Tests if an agent query service is supported. 
     *
     *  @return <code> true </code> if agent query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Tests if an agent search service is supported. 
     *
     *  @return <code> true </code> if agent search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearch() {
        return (false);
    }


    /**
     *  Tests if an agent administrative service is supported. 
     *
     *  @return <code> true </code> if agent admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentAdmin() {
        return (false);
    }


    /**
     *  Tests if agent notification is supported. Messages may be sent when 
     *  agents are created, modified, or deleted. 
     *
     *  @return <code> true </code> if agent notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentNotification() {
        return (false);
    }


    /**
     *  Tests if retrieving mappings of agents and agencies is supported. 
     *
     *  @return <code> true </code> if agent agency mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentAgency() {
        return (false);
    }


    /**
     *  Tests if managing mappings of agents and agencies is supported. 
     *
     *  @return <code> true </code> if agent agency assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentAgencyAssignment() {
        return (false);
    }


    /**
     *  Tests if agent smart agency is available. 
     *
     *  @return <code> true </code> if agent smart agency is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSmartAgency() {
        return (false);
    }


    /**
     *  Tests if an agency lookup service is supported. An agency lookup 
     *  service defines methods to access agencies. 
     *
     *  @return <code> true </code> if agency lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyLookup() {
        return (false);
    }


    /**
     *  Tests if an agency query service is supported. 
     *
     *  @return <code> true </code> if agency query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyQuery() {
        return (false);
    }


    /**
     *  Tests if an agency search service is supported. 
     *
     *  @return <code> true </code> if agency search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencySearch() {
        return (false);
    }


    /**
     *  Tests if an agency administrative service is supported. 
     *
     *  @return <code> true </code> if agency admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyAdmin() {
        return (false);
    }


    /**
     *  Tests if agency notification is supported. Messages may be sent when 
     *  agencies are created, modified, or deleted. 
     *
     *  @return <code> true </code> if agency notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyNotification() {
        return (false);
    }


    /**
     *  Tests if an agency hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if an agency hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyHierarchy() {
        return (false);
    }


    /**
     *  Tests if an agency hierarchy design is supported. 
     *
     *  @return <code> true </code> if an agency hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if an authentication key service is available. 
     *
     *  @return <code> true </code> if an authentication key service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationKeys() {
        return (false);
    }


    /**
     *  Tests if an authentication process service is available. 
     *
     *  @return <code> true </code> if an authentication process service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationProcess() {
        return (false);
    }


    /**
     *  Gets the supported <code> Agent </code> record types. 
     *
     *  @return a list containing the supported <code> Agent </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAgentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.agentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Agent </code> record type is supported. 
     *
     *  @param  agentRecordType a <code> Type </code> indicating an <code> 
     *          Agent </code> record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> agentRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAgentRecordType(org.osid.type.Type agentRecordType) {
        return (this.agentRecordTypes.contains(agentRecordType));
    }


    /**
     *  Adds support for an agent record type.
     *
     *  @param agentRecordType an agent record type
     *  @throws org.osid.NullArgumentException
     *  <code>agentRecordType</code> is <code>null</code>
     */

    protected void addAgentRecordType(org.osid.type.Type agentRecordType) {
        this.agentRecordTypes.add(agentRecordType);
        return;
    }


    /**
     *  Removes support for an agent record type.
     *
     *  @param agentRecordType an agent record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>agentRecordType</code> is <code>null</code>
     */

    protected void removeAgentRecordType(org.osid.type.Type agentRecordType) {
        this.agentRecordTypes.remove(agentRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Agent </code> search record types. 
     *
     *  @return a list containing the supported <code> Agent </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAgentSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.agentSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Agent </code> search record type is 
     *  supported. 
     *
     *  @param  agentSearchRecordType a <code> Type </code> indicating an 
     *          <code> Agent </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> agentSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAgentSearchRecordType(org.osid.type.Type agentSearchRecordType) {
        return (this.agentSearchRecordTypes.contains(agentSearchRecordType));
    }


    /**
     *  Adds support for an agent search record type.
     *
     *  @param agentSearchRecordType an agent search record type
     *  @throws org.osid.NullArgumentException
     *  <code>agentSearchRecordType</code> is <code>null</code>
     */

    protected void addAgentSearchRecordType(org.osid.type.Type agentSearchRecordType) {
        this.agentSearchRecordTypes.add(agentSearchRecordType);
        return;
    }


    /**
     *  Removes support for an agent search record type.
     *
     *  @param agentSearchRecordType an agent search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>agentSearchRecordType</code> is <code>null</code>
     */

    protected void removeAgentSearchRecordType(org.osid.type.Type agentSearchRecordType) {
        this.agentSearchRecordTypes.remove(agentSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Agency </code> record types. 
     *
     *  @return a list containing the supported <code> Agency </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAgencyRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.agencyRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Agency </code> record type is supported. 
     *
     *  @param  agencyRecordType a <code> Type </code> indicating an <code> 
     *          Agency </code> record type 
     *  @return <code> true </code> if the given record Type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> agencyRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAgencyRecordType(org.osid.type.Type agencyRecordType) {
        return (this.agencyRecordTypes.contains(agencyRecordType));
    }


    /**
     *  Adds support for an agency record type.
     *
     *  @param agencyRecordType an agency record type
     *  @throws org.osid.NullArgumentException
     *  <code>agencyRecordType</code> is <code>null</code>
     */

    protected void addAgencyRecordType(org.osid.type.Type agencyRecordType) {
        this.agencyRecordTypes.add(agencyRecordType);
        return;
    }


    /**
     *  Removes support for an agency record type.
     *
     *  @param agencyRecordType an agency record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>agencyRecordType</code> is <code>null</code>
     */

    protected void removeAgencyRecordType(org.osid.type.Type agencyRecordType) {
        this.agencyRecordTypes.remove(agencyRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Agency </code> search record types. 
     *
     *  @return a list containing the supported <code> Agency </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAgencySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.agencySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Agency </code> search record type is 
     *  supported. 
     *
     *  @param  agencySearchRecordType a <code> Type </code> indicating an 
     *          <code> Agency </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> agencySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAgencySearchRecordType(org.osid.type.Type agencySearchRecordType) {
        return (this.agencySearchRecordTypes.contains(agencySearchRecordType));
    }


    /**
     *  Adds support for an agency search record type.
     *
     *  @param agencySearchRecordType an agency search record type
     *  @throws org.osid.NullArgumentException
     *  <code>agencySearchRecordType</code> is <code>null</code>
     */

    protected void addAgencySearchRecordType(org.osid.type.Type agencySearchRecordType) {
        this.agencySearchRecordTypes.add(agencySearchRecordType);
        return;
    }


    /**
     *  Removes support for an agency search record type.
     *
     *  @param agencySearchRecordType an agency search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>agencySearchRecordType</code> is <code>null</code>
     */

    protected void removeAgencySearchRecordType(org.osid.type.Type agencySearchRecordType) {
        this.agencySearchRecordTypes.remove(agencySearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent lookup 
     *  service. 
     *
     *  @return an <code> AgentLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentLookupSession getAgentLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentLookupSession getAgentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent lookup 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> an AgentLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentLookupSession getAgentLookupSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentLookupSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent lookup 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> an AgentLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentLookupSession getAgentLookupSessionForAgency(org.osid.id.Id agencyId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentLookupSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent query 
     *  service. 
     *
     *  @return an <code> AgentQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuerySession getAgentQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuerySession getAgentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent query 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> an AgentQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuerySession getAgentQuerySessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentQuerySessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent query 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return an <code> AgentQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuerySession getAgentQuerySessionForAgency(org.osid.id.Id agencyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentQuerySessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent search 
     *  service. 
     *
     *  @return an <code> AgentSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchSession getAgentSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchSession getAgentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent search 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> an AgentSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchSession getAgentSearchSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentSearchSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent search 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> an AgentSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchSession getAgentSearchSessionForAgency(org.osid.id.Id agencyId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentSearchSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent 
     *  administration service. 
     *
     *  @return an <code> AgentAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentAdminSession getAgentAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentAdminSession getAgentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent admin 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> an AgentAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentAdminSession getAgentAdminSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentAdminSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent admin 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> an AgentAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentAdminSession getAgentAdminSessionForAgency(org.osid.id.Id agencyId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentAdminSessionForAgency not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to service 
     *  changes. 
     *
     *  @param  agentReceiver the agent receiver 
     *  @return an <code> AgentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> agentReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentNotificationSession getAgentNotificationSession(org.osid.authentication.AgentReceiver agentReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentNotificationSession not implemented");
    }


    /**
     *  Gets the messaging receiver session for notifications pertaining to 
     *  agent changes. 
     *
     *  @param  agentReceiver the agent receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AgentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          agentReceiver </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentNotificationSession getAgentNotificationSession(org.osid.authentication.AgentReceiver agentReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent 
     *  notification service for the given agency. 
     *
     *  @param  agentReceiver the agent receiver 
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return <code> an AgentNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agentReceiver </code> or 
     *          <code> agencyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentNotificationSession getAgentNotificationSessionForAgency(org.osid.authentication.AgentReceiver agentReceiver, 
                                                                                                 org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentNotificationSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent 
     *  notification service for the given agency. 
     *
     *  @param  agentReceiver the agent receiver 
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return <code> an AgentNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agentReceiver, agencyId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentNotificationSession getAgentNotificationSessionForAgency(org.osid.authentication.AgentReceiver agentReceiver, 
                                                                                                 org.osid.id.Id agencyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentNotificationSessionForAgency not implemented");
    }


    /**
     *  Gets the session for retrieving agent to agency mappings. 
     *
     *  @return an <code> AgentAgencySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentAgency() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentAgencySession getAgentAgencySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentAgencySession not implemented");
    }


    /**
     *  Gets the session for retrieving agent to agency mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgentAgencySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgentAgency() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentAgencySession getAgentAgencySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentAgencySession not implemented");
    }


    /**
     *  Gets the session for assigning agent to agency mappings. 
     *
     *  @return a <code> AgentAgencyAsignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentAgencyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentAgencyAssignmentSession getAgentAgencyAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentAgencyAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning agent to agency mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgentAgencyAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentAgencyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentAgencyAssignmentSession getAgentAgencyAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentAgencyAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent smart 
     *  agency service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return an <code> AgentSmartAgencySession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSmartAgency() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSmartAgencySession getAgentSmartAgencySession(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgentSmartAgencySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agent smart 
     *  agency service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AgentSmartAgencySession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSmartAgency() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSmartAgencySession getAgentSmartAgencySession(org.osid.id.Id agencyId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgentSmartAgencySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agency lookup 
     *  service. 
     *
     *  @return an <code> AgencyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgencyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyLookupSession getAgencyLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgencyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agency lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgencyLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgencyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyLookupSession getAgencyLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgencyLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agency search 
     *  service. 
     *
     *  @return an <code> AgencySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgencySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencySearchSession getAgencySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgencySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agency search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgencySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgencySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencySearchSession getAgencySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgencySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agency 
     *  administration service. 
     *
     *  @return an <code> AgencyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgencyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyAdminSession getAgencyAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgencyAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the agency 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgencyAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAgencyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyAdminSession getAgencyAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgencyAdminSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to agency 
     *  service changes. 
     *
     *  @param  agencyReceiver the agency receiver 
     *  @return an <code> AgencyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> agencyReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyNotificationSession getAgencyNotificationSession(org.osid.authentication.AgencyReceiver agencyReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgencyNotificationSession not implemented");
    }


    /**
     *  Gets the messaging receiver session for notifications pertaining to 
     *  agency changes. 
     *
     *  @param  agencyReceiver the agency receiver 
     *  @param  proxy a proxy 
     *  @return an <code> AgencyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> agencyReceiver </code> 
     *          or <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyNotificationSession getAgencyNotificationSession(org.osid.authentication.AgencyReceiver agencyReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgencyNotificationSession not implemented");
    }


    /**
     *  Gets the session traversing agency hierarchies. 
     *
     *  @return an <code> AgencyHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyHierarchySession getAgencyHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgencyHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing agency hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgencyHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyHierarchySession getAgencyHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgencyHierarchySession not implemented");
    }


    /**
     *  Gets the session designing agency hierarchies. 
     *
     *  @return an <code> AgencyHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyHierarchyDesignSession getAgencyHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAgencyHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing agency hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AgencyHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgencyHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyHierarchyDesignSession getAgencyHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAgencyHierarchyDesignSession not implemented");
    }


    /**
     *  Gets an <code> AuthenticationBatchManager. </code> 
     *
     *  @return an <code> AuthenticationBatchManager. </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AuthenticationBatchManager getAuthenticationBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAuthenticationBatchManager not implemented");
    }


    /**
     *  Gets an <code> AuthenticationBatchProxyManager. </code> 
     *
     *  @return an <code> AuthenticationBatchProxyManager. </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.batch.AuthenticationBatchProxyManager getAuthenticationBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAuthenticationBatchProxyManager not implemented");
    }


    /**
     *  Gets an <code> AuthenticationKeysManager. </code> 
     *
     *  @return an <code> AuthenticationKeysManager. </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationKeys() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.AuthenticationKeysManager getAuthenticationKeysManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAuthenticationKeysManager not implemented");
    }


    /**
     *  Gets an <code> AuthenticationKeysProxyManager. </code> 
     *
     *  @return an <code> AuthenticationKeysProxyManager. </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationKeys() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.keys.AuthenticationKeysProxyManager getAuthenticationKeysProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAuthenticationKeysProxyManager not implemented");
    }


    /**
     *  Gets an <code> AuthenticationProcessManager. </code> 
     *
     *  @return an <code> AuthenticationProcessManager. </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationProcess() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.AuthenticationProcessManager getAuthenticationProcessManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationManager.getAuthenticationProcessManager not implemented");
    }


    /**
     *  Gets an <code> AuthenticationProcessProxyManager. </code> 
     *
     *  @return an <code> AuthenticationProcessproxyManager. </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationProcess() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.AuthenticationProcessProxyManager getAuthenticationProcessProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.AuthenticationProxyManager.getAuthenticationProcessProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.agentRecordTypes.clear();
        this.agentRecordTypes.clear();

        this.agentSearchRecordTypes.clear();
        this.agentSearchRecordTypes.clear();

        this.agencyRecordTypes.clear();
        this.agencyRecordTypes.clear();

        this.agencySearchRecordTypes.clear();
        this.agencySearchRecordTypes.clear();

        return;
    }
}
