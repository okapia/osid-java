//
// AbstractProfileEntryBatchFormList
//
//     Implements a filter for a ProfileEntryBatchFormList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.profile.batch.profileentrybatchform.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a ProfileEntryBatchFormList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedProfileEntryBatchFormList
 *  to improve performance.
 */

public abstract class AbstractProfileEntryBatchFormFilterList
    extends net.okapia.osid.jamocha.profile.batch.profileentrybatchform.spi.AbstractProfileEntryBatchFormList
    implements org.osid.profile.batch.ProfileEntryBatchFormList,
               net.okapia.osid.jamocha.inline.filter.profile.batch.profileentrybatchform.ProfileEntryBatchFormFilter {

    private org.osid.profile.batch.ProfileEntryBatchForm profileEntryBatchForm;
    private final org.osid.profile.batch.ProfileEntryBatchFormList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractProfileEntryBatchFormFilterList</code>.
     *
     *  @param profileEntryBatchFormList a <code>ProfileEntryBatchFormList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryBatchFormList</code> is <code>null</code>
     */

    protected AbstractProfileEntryBatchFormFilterList(org.osid.profile.batch.ProfileEntryBatchFormList profileEntryBatchFormList) {
        nullarg(profileEntryBatchFormList, "profile entry batch form list");
        this.list = profileEntryBatchFormList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.profileEntryBatchForm == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ProfileEntryBatchForm </code> in this list. 
     *
     *  @return the next <code> ProfileEntryBatchForm </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ProfileEntryBatchForm </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.profile.batch.ProfileEntryBatchForm getNextProfileEntryBatchForm()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.profile.batch.ProfileEntryBatchForm profileEntryBatchForm = this.profileEntryBatchForm;
            this.profileEntryBatchForm = null;
            return (profileEntryBatchForm);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in profile entry batch form list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.profileEntryBatchForm = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ProfileEntryBatchForms.
     *
     *  @param profileEntryBatchForm the profile entry batch form to filter
     *  @return <code>true</code> if the profile entry batch form passes the filter,
     *          <code>false</code> if the profile entry batch form should be filtered
     */

    public abstract boolean pass(org.osid.profile.batch.ProfileEntryBatchForm profileEntryBatchForm);


    protected void prime() {
        if (this.profileEntryBatchForm != null) {
            return;
        }

        org.osid.profile.batch.ProfileEntryBatchForm profileEntryBatchForm = null;

        while (this.list.hasNext()) {
            try {
                profileEntryBatchForm = this.list.getNextProfileEntryBatchForm();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(profileEntryBatchForm)) {
                this.profileEntryBatchForm = profileEntryBatchForm;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
