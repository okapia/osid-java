//
// AbstractAssemblyCourseCatalogQuery.java
//
//     A CourseCatalogQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.coursecatalog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CourseCatalogQuery that stores terms.
 */

public abstract class AbstractAssemblyCourseCatalogQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.course.CourseCatalogQuery,
               org.osid.course.CourseCatalogQueryInspector,
               org.osid.course.CourseCatalogSearchOrder {

    private final java.util.Collection<org.osid.course.records.CourseCatalogQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseCatalogQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.CourseCatalogSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCourseCatalogQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCourseCatalogQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the course <code> Id </code> for this query to match courses that 
     *  have a related course. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Matches course catalogs that have any course. 
     *
     *  @param  match <code> true </code> to match courses with any course, 
     *          <code> false </code> to match courses with no course 
     */

    @OSID @Override
    public void matchAnyCourse(boolean match) {
        getAssembler().addIdWildcardTerm(getCourseColumn(), match);
        return;
    }


    /**
     *  Clears the course query terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course query terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Sets the activity unit <code> Id </code> for this query. 
     *
     *  @param  activityUnitId an activity unit <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityUnitId(org.osid.id.Id activityUnitId, 
                                    boolean match) {
        getAssembler().addIdTerm(getActivityUnitIdColumn(), activityUnitId, match);
        return;
    }


    /**
     *  Clears the activity unit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActivityUnitIdTerms() {
        getAssembler().clearTerms(getActivityUnitIdColumn());
        return;
    }


    /**
     *  Gets the activity unit <code> Id </code> query terms. 
     *
     *  @return the activity unit <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityUnitIdTerms() {
        return (getAssembler().getIdTerms(getActivityUnitIdColumn()));
    }


    /**
     *  Gets the ActivityUnitId column name.
     *
     * @return the column name
     */

    protected String getActivityUnitIdColumn() {
        return ("activity_unit_id");
    }


    /**
     *  Tests if an <code> ActivityUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the activity unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuery getActivityUnitQuery() {
        throw new org.osid.UnimplementedException("supportsActivityUnitQuery() is false");
    }


    /**
     *  Matches course catalogs that have any activity unit. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          activity unit, <code> false </code> to match course catalogs 
     *          with no activity units 
     */

    @OSID @Override
    public void matchAnyActivityUnit(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityUnitColumn(), match);
        return;
    }


    /**
     *  Clears the activity unit query terms. 
     */

    @OSID @Override
    public void clearActivityUnitTerms() {
        getAssembler().clearTerms(getActivityUnitColumn());
        return;
    }


    /**
     *  Gets the activity unit query terms. 
     *
     *  @return the activity unit query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQueryInspector[] getActivityUnitTerms() {
        return (new org.osid.course.ActivityUnitQueryInspector[0]);
    }


    /**
     *  Gets the ActivityUnit column name.
     *
     * @return the column name
     */

    protected String getActivityUnitColumn() {
        return ("activity_unit");
    }


    /**
     *  Sets the catalog <code> Id </code> for this query. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        getAssembler().addIdTerm(getCourseOfferingIdColumn(), courseOfferingId, match);
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        getAssembler().clearTerms(getCourseOfferingIdColumn());
        return;
    }


    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (getAssembler().getIdTerms(getCourseOfferingIdColumn()));
    }


    /**
     *  Gets the CourseOfferingId column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingIdColumn() {
        return ("course_offering_id");
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Matches course catalogs that have any course offering. 
     *
     *  @param  match <code> true </code> to match courses with any course 
     *          offering, <code> false </code> to match courses with no course 
     *          offering 
     */

    @OSID @Override
    public void matchAnyCourseOffering(boolean match) {
        getAssembler().addIdWildcardTerm(getCourseOfferingColumn(), match);
        return;
    }


    /**
     *  Clears the course offering query terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        getAssembler().clearTerms(getCourseOfferingColumn());
        return;
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Gets the CourseOffering column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingColumn() {
        return ("course_offering");
    }


    /**
     *  Sets the activity <code> Id </code> for this query. 
     *
     *  @param  activityUnitId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityUnitId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityUnitId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches course catalogs that have any activity. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          activity, <code> false </code> to match course catalogs with 
     *          no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity query terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Sets the term <code> Id </code> for this query to match catalogs 
     *  containing terms. 
     *
     *  @param  termId the term <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        getAssembler().addIdTerm(getTermIdColumn(), termId, match);
        return;
    }


    /**
     *  Clears the term <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        getAssembler().clearTerms(getTermIdColumn());
        return;
    }


    /**
     *  Gets the term <code> Id </code> query terms. 
     *
     *  @return the term <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTermIdTerms() {
        return (getAssembler().getIdTerms(getTermIdColumn()));
    }


    /**
     *  Gets the TermId column name.
     *
     * @return the column name
     */

    protected String getTermIdColumn() {
        return ("term_id");
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Matches course catalogs that have any term. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          term, <code> false </code> to match courses with no term 
     */

    @OSID @Override
    public void matchAnyTerm(boolean match) {
        getAssembler().addIdWildcardTerm(getTermColumn(), match);
        return;
    }


    /**
     *  Clears the term query terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        getAssembler().clearTerms(getTermColumn());
        return;
    }


    /**
     *  Gets the term query terms. 
     *
     *  @return the term query terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Gets the Term column name.
     *
     * @return the column name
     */

    protected String getTermColumn() {
        return ("term");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  course catalogs that have the specified course catalog as an ancestor. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                             boolean match) {
        getAssembler().addIdTerm(getAncestorCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the ancestor course catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorCourseCatalogIdTerms() {
        getAssembler().clearTerms(getAncestorCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the ancestor course catalog <code> Id </code> query terms. 
     *
     *  @return the ancestor course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getAncestorCourseCatalogIdColumn()));
    }


    /**
     *  Gets the AncestorCourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getAncestorCourseCatalogIdColumn() {
        return ("ancestor_course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCourseCatalogQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getAncestorCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCourseCatalogQuery() is false");
    }


    /**
     *  Matches course catalogs with any course catalog ancestor. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          ancestor, <code> false </code> to match root course catalogs 
     */

    @OSID @Override
    public void matchAnyAncestorCourseCatalog(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorCourseCatalogColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor course catalog query terms. 
     */

    @OSID @Override
    public void clearAncestorCourseCatalogTerms() {
        getAssembler().clearTerms(getAncestorCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the ancestor course catalog query terms. 
     *
     *  @return the ancestor course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getAncestorCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the AncestorCourseCatalog column name.
     *
     * @return the column name
     */

    protected String getAncestorCourseCatalogColumn() {
        return ("ancestor_course_catalog");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  course catalogs that have the specified course catalog as an 
     *  descendant. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                               boolean match) {
        getAssembler().addIdTerm(getDescendantCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the descendant course catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantCourseCatalogIdTerms() {
        getAssembler().clearTerms(getDescendantCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the descendant course catalog <code> Id </code> query terms. 
     *
     *  @return the descendant course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getDescendantCourseCatalogIdColumn()));
    }


    /**
     *  Gets the DescendantCourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getDescendantCourseCatalogIdColumn() {
        return ("descendant_course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCourseCatalogQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getDescendantCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCourseCatalogQuery() is false");
    }


    /**
     *  Matches course catalogs with any descendant course catalog. 
     *
     *  @param  match <code> true </code> to match course catalogs with any 
     *          descendant, <code> false </code> to match leaf course catalogs 
     */

    @OSID @Override
    public void matchAnyDescendantCourseCatalog(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantCourseCatalogColumn(), match);
        return;
    }


    /**
     *  Clears the descendant course catalog query terms. 
     */

    @OSID @Override
    public void clearDescendantCourseCatalogTerms() {
        getAssembler().clearTerms(getDescendantCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the descendant course catalog query terms. 
     *
     *  @return the descendant course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getDescendantCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the DescendantCourseCatalog column name.
     *
     * @return the column name
     */

    protected String getDescendantCourseCatalogColumn() {
        return ("descendant_course_catalog");
    }


    /**
     *  Tests if this courseCatalog supports the given record
     *  <code>Type</code>.
     *
     *  @param  courseCatalogRecordType a course catalog record type 
     *  @return <code>true</code> if the courseCatalogRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type courseCatalogRecordType) {
        for (org.osid.course.records.CourseCatalogQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(courseCatalogRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  courseCatalogRecordType the course catalog record type 
     *  @return the course catalog query record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseCatalogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseCatalogQueryRecord getCourseCatalogQueryRecord(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseCatalogQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(courseCatalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseCatalogRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  courseCatalogRecordType the course catalog record type 
     *  @return the course catalog query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseCatalogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseCatalogQueryInspectorRecord getCourseCatalogQueryInspectorRecord(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseCatalogQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(courseCatalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseCatalogRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param courseCatalogRecordType the course catalog record type
     *  @return the course catalog search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseCatalogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.CourseCatalogSearchOrderRecord getCourseCatalogSearchOrderRecord(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.CourseCatalogSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(courseCatalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseCatalogRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this course catalog. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param courseCatalogQueryRecord the course catalog query record
     *  @param courseCatalogQueryInspectorRecord the course catalog query inspector
     *         record
     *  @param courseCatalogSearchOrderRecord the course catalog search order record
     *  @param courseCatalogRecordType course catalog record type
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogQueryRecord</code>,
     *          <code>courseCatalogQueryInspectorRecord</code>,
     *          <code>courseCatalogSearchOrderRecord</code> or
     *          <code>courseCatalogRecordTypecourseCatalog</code> is
     *          <code>null</code>
     */
            
    protected void addCourseCatalogRecords(org.osid.course.records.CourseCatalogQueryRecord courseCatalogQueryRecord, 
                                      org.osid.course.records.CourseCatalogQueryInspectorRecord courseCatalogQueryInspectorRecord, 
                                      org.osid.course.records.CourseCatalogSearchOrderRecord courseCatalogSearchOrderRecord, 
                                      org.osid.type.Type courseCatalogRecordType) {

        addRecordType(courseCatalogRecordType);

        nullarg(courseCatalogQueryRecord, "course catalog query record");
        nullarg(courseCatalogQueryInspectorRecord, "course catalog query inspector record");
        nullarg(courseCatalogSearchOrderRecord, "course catalog search odrer record");

        this.queryRecords.add(courseCatalogQueryRecord);
        this.queryInspectorRecords.add(courseCatalogQueryInspectorRecord);
        this.searchOrderRecords.add(courseCatalogSearchOrderRecord);
        
        return;
    }
}
