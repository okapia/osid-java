//
// AbstractMapOfferingLookupSession
//
//    A simple framework for providing an Offering lookup service
//    backed by a fixed collection of offerings.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Offering lookup service backed by a
 *  fixed collection of offerings. The offerings are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Offerings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapOfferingLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractOfferingLookupSession
    implements org.osid.offering.OfferingLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.Offering> offerings = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.Offering>());


    /**
     *  Makes an <code>Offering</code> available in this session.
     *
     *  @param  offering an offering
     *  @throws org.osid.NullArgumentException <code>offering<code>
     *          is <code>null</code>
     */

    protected void putOffering(org.osid.offering.Offering offering) {
        this.offerings.put(offering.getId(), offering);
        return;
    }


    /**
     *  Makes an array of offerings available in this session.
     *
     *  @param  offerings an array of offerings
     *  @throws org.osid.NullArgumentException <code>offerings<code>
     *          is <code>null</code>
     */

    protected void putOfferings(org.osid.offering.Offering[] offerings) {
        putOfferings(java.util.Arrays.asList(offerings));
        return;
    }


    /**
     *  Makes a collection of offerings available in this session.
     *
     *  @param  offerings a collection of offerings
     *  @throws org.osid.NullArgumentException <code>offerings<code>
     *          is <code>null</code>
     */

    protected void putOfferings(java.util.Collection<? extends org.osid.offering.Offering> offerings) {
        for (org.osid.offering.Offering offering : offerings) {
            this.offerings.put(offering.getId(), offering);
        }

        return;
    }


    /**
     *  Removes an Offering from this session.
     *
     *  @param  offeringId the <code>Id</code> of the offering
     *  @throws org.osid.NullArgumentException <code>offeringId<code> is
     *          <code>null</code>
     */

    protected void removeOffering(org.osid.id.Id offeringId) {
        this.offerings.remove(offeringId);
        return;
    }


    /**
     *  Gets the <code>Offering</code> specified by its <code>Id</code>.
     *
     *  @param  offeringId <code>Id</code> of the <code>Offering</code>
     *  @return the offering
     *  @throws org.osid.NotFoundException <code>offeringId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>offeringId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Offering getOffering(org.osid.id.Id offeringId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.Offering offering = this.offerings.get(offeringId);
        if (offering == null) {
            throw new org.osid.NotFoundException("offering not found: " + offeringId);
        }

        return (offering);
    }


    /**
     *  Gets all <code>Offerings</code>. In plenary mode, the returned
     *  list contains all known offerings or an error
     *  results. Otherwise, the returned list may contain only those
     *  offerings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Offerings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.OfferingList getOfferings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.offering.ArrayOfferingList(this.offerings.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.offerings.clear();
        super.close();
        return;
    }
}
