//
// MutableIndexedMapCommitmentLookupSession
//
//    Implements a Commitment lookup service backed by a collection of
//    commitments indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a Commitment lookup service backed by a collection of
 *  commitments. The commitments are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some commitments may be compatible
 *  with more types than are indicated through these commitment
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of commitments can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCommitmentLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractIndexedMapCommitmentLookupSession
    implements org.osid.calendaring.CommitmentLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCommitmentLookupSession} with no commitments.
     *
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumentException {@code calendar}
     *          is {@code null}
     */

      public MutableIndexedMapCommitmentLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCommitmentLookupSession} with a
     *  single commitment.
     *  
     *  @param calendar the calendar
     *  @param  commitment a single commitment
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code commitment} is {@code null}
     */

    public MutableIndexedMapCommitmentLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.Commitment commitment) {
        this(calendar);
        putCommitment(commitment);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCommitmentLookupSession} using an
     *  array of commitments.
     *
     *  @param calendar the calendar
     *  @param  commitments an array of commitments
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code commitments} is {@code null}
     */

    public MutableIndexedMapCommitmentLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.Commitment[] commitments) {
        this(calendar);
        putCommitments(commitments);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCommitmentLookupSession} using a
     *  collection of commitments.
     *
     *  @param calendar the calendar
     *  @param  commitments a collection of commitments
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code commitments} is {@code null}
     */

    public MutableIndexedMapCommitmentLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.Commitment> commitments) {

        this(calendar);
        putCommitments(commitments);
        return;
    }
    

    /**
     *  Makes a {@code Commitment} available in this session.
     *
     *  @param  commitment a commitment
     *  @throws org.osid.NullArgumentException {@code commitment{@code  is
     *          {@code null}
     */

    @Override
    public void putCommitment(org.osid.calendaring.Commitment commitment) {
        super.putCommitment(commitment);
        return;
    }


    /**
     *  Makes an array of commitments available in this session.
     *
     *  @param  commitments an array of commitments
     *  @throws org.osid.NullArgumentException {@code commitments{@code 
     *          is {@code null}
     */

    @Override
    public void putCommitments(org.osid.calendaring.Commitment[] commitments) {
        super.putCommitments(commitments);
        return;
    }


    /**
     *  Makes collection of commitments available in this session.
     *
     *  @param  commitments a collection of commitments
     *  @throws org.osid.NullArgumentException {@code commitment{@code  is
     *          {@code null}
     */

    @Override
    public void putCommitments(java.util.Collection<? extends org.osid.calendaring.Commitment> commitments) {
        super.putCommitments(commitments);
        return;
    }


    /**
     *  Removes a Commitment from this session.
     *
     *  @param commitmentId the {@code Id} of the commitment
     *  @throws org.osid.NullArgumentException {@code commitmentId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCommitment(org.osid.id.Id commitmentId) {
        super.removeCommitment(commitmentId);
        return;
    }    
}
