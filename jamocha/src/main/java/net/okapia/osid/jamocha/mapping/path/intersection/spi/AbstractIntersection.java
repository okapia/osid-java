//
// AbstractIntersection.java
//
//     Defines an Intersection.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.intersection.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Intersection</code>.
 */

public abstract class AbstractIntersection
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.mapping.path.Intersection {

    private org.osid.mapping.Coordinate coordinate;
    private final java.util.Collection<org.osid.mapping.path.Path> paths = new java.util.LinkedHashSet<>();

    private boolean rotary = false;
    private boolean fork   = false;

    private final java.util.Collection<org.osid.mapping.path.records.IntersectionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets a single corrdinate to represent the intersection. 
     *
     *  @return the coordinate 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getCoordinate() {
        return (this.coordinate);
    }


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate a coordinate
     *  @throws org.osid.NullArgumentException
     *          <code>coordinate</code> is <code>null</code>
     */

    protected void setCoordinate(org.osid.mapping.Coordinate coordinate) {
        nullarg(coordinate, "coordinate");
        this.coordinate = coordinate;
        return;
    }


    /**
     *  Gets the intersecting path <code> Ids. </code> 
     *
     *  @return the path <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPathIds() {
        try {
            org.osid.mapping.path.PathList paths = getPaths();
            return (new net.okapia.osid.jamocha.adapter.converter.mapping.path.path.PathToIdList(paths));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the intersecting paths. 
     *
     *  @return the paths 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPaths()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.mapping.path.path.ArrayPathList(this.paths));
    }


    /**
     *  Adds a path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException
     *          <code>path</code> is <code>null</code>
     */

    protected void addPath(org.osid.mapping.path.Path path) {
        nullarg(path, "path");
        this.paths.add(path);
        return;
    }


    /**
     *  Sets all the paths.
     *
     *  @param paths a collection of paths
     *  @throws org.osid.NullArgumentException
     *          <code>paths</code> is <code>null</code>
     */

    protected void setPaths(java.util.Collection<org.osid.mapping.path.Path> paths) {
        nullarg(paths, "paths");
        this.paths.clear();
        this.paths.addAll(paths);
        return;
    }


    /**
     *  Tests if this intersection is a rotary. 
     *
     *  @return <code> true </code> if this intersection is a rotary, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isRotary() {
        return (this.rotary);
    }


    /**
     *  Sets the rotary flag.
     *
     *  @param rotary <code> true </code> if this intersection is a
     *          rotary, <code> false </code> otherwise
     */

    protected void setRotary(boolean rotary) {
        this.rotary = rotary;
        return;
    }


    /**
     *  Tests if this intersection is a fork or exit. 
     *
     *  @return <code> true </code> if this intersection is a fork, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isFork() {
        return (this.fork);
    }


    /**
     *  Sets the fork flag.
     *
     *  @param fork <code> true </code> if this intersection is a
     *          fork, <code> false </code> otherwise
     */

    protected void setFork(boolean fork) {
        this.fork = fork;
        return;
    }


    /**
     *  Tests if this intersection supports the given record
     *  <code>Type</code>.
     *
     *  @param  intersectionRecordType an intersection record type 
     *  @return <code>true</code> if the intersectionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type intersectionRecordType) {
        for (org.osid.mapping.path.records.IntersectionRecord record : this.records) {
            if (record.implementsRecordType(intersectionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Intersection</code> record <code>Type</code>.
     *
     *  @param  intersectionRecordType the intersection record type 
     *  @return the intersection record 
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(intersectionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.IntersectionRecord getIntersectionRecord(org.osid.type.Type intersectionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.IntersectionRecord record : this.records) {
            if (record.implementsRecordType(intersectionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(intersectionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this intersection. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param intersectionRecord the intersection record
     *  @param intersectionRecordType intersection record type
     *  @throws org.osid.NullArgumentException
     *          <code>intersectionRecord</code> or
     *          <code>intersectionRecordTypeintersection</code> is
     *          <code>null</code>
     */
            
    protected void addIntersectionRecord(org.osid.mapping.path.records.IntersectionRecord intersectionRecord, 
                                         org.osid.type.Type intersectionRecordType) {
        
        nullarg(intersectionRecord, "intersection record");
        addRecordType(intersectionRecordType);
        this.records.add(intersectionRecord);
        
        return;
    }
}
