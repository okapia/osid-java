//
// AbstractWorkflowRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractWorkflowRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.workflow.rules.WorkflowRulesManager,
               org.osid.workflow.rules.WorkflowRulesProxyManager {

    private final Types stepConstrainerRecordTypes         = new TypeRefSet();
    private final Types stepConstrainerSearchRecordTypes   = new TypeRefSet();

    private final Types stepConstrainerEnablerRecordTypes  = new TypeRefSet();
    private final Types stepConstrainerEnablerSearchRecordTypes= new TypeRefSet();

    private final Types stepProcessorRecordTypes           = new TypeRefSet();
    private final Types stepProcessorSearchRecordTypes     = new TypeRefSet();

    private final Types stepProcessorEnablerRecordTypes    = new TypeRefSet();
    private final Types stepProcessorEnablerSearchRecordTypes= new TypeRefSet();

    private final Types processEnablerRecordTypes          = new TypeRefSet();
    private final Types processEnablerSearchRecordTypes    = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractWorkflowRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractWorkflowRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up step constrainer is supported. 
     *
     *  @return <code> true </code> if step constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerLookup() {
        return (false);
    }


    /**
     *  Tests if querying step constrainer is supported. 
     *
     *  @return <code> true </code> if step constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerQuery() {
        return (false);
    }


    /**
     *  Tests if searching step constrainer is supported. 
     *
     *  @return <code> true </code> if step constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerSearch() {
        return (false);
    }


    /**
     *  Tests if a step constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if step constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerAdmin() {
        return (false);
    }


    /**
     *  Tests if a step constrainer notification service is supported. 
     *
     *  @return <code> true </code> if step constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerNotification() {
        return (false);
    }


    /**
     *  Tests if a step constrainer office lookup service is supported. 
     *
     *  @return <code> true </code> if a step constrainer office lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerOffice() {
        return (false);
    }


    /**
     *  Tests if a step constrainer office service is supported. 
     *
     *  @return <code> true </code> if step constrainer office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a step constrainer office lookup service is supported. 
     *
     *  @return <code> true </code> if a step constrainer office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerSmartOffice() {
        return (false);
    }


    /**
     *  Tests if a step constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a step constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a step constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a step constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up step constrainer enablers is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying step constrainer enablers is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching step constrainer enablers is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a step constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if step constrainer enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a step constrainer enabler notification service is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a step constrainer enabler office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a step constrainer enabler office 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerOffice() {
        return (false);
    }


    /**
     *  Tests if a step constrainer enabler office service is supported. 
     *
     *  @return <code> true </code> if step constrainer enabler office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a step constrainer enabler office lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a step constrainer enabler office 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerSmartOffice() {
        return (false);
    }


    /**
     *  Tests if a step constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a step constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a step constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if step constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up step processor is supported. 
     *
     *  @return <code> true </code> if step processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorLookup() {
        return (false);
    }


    /**
     *  Tests if querying step processor is supported. 
     *
     *  @return <code> true </code> if step processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorQuery() {
        return (false);
    }


    /**
     *  Tests if searching step processor is supported. 
     *
     *  @return <code> true </code> if step processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorSearch() {
        return (false);
    }


    /**
     *  Tests if a step processor administrative service is supported. 
     *
     *  @return <code> true </code> if step processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorAdmin() {
        return (false);
    }


    /**
     *  Tests if a step processor notification service is supported. 
     *
     *  @return <code> true </code> if step processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorNotification() {
        return (false);
    }


    /**
     *  Tests if a step processor office lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor office lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorOffice() {
        return (false);
    }


    /**
     *  Tests if a step processor office service is supported. 
     *
     *  @return <code> true </code> if step processor office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a step processor office lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorSmartOffice() {
        return (false);
    }


    /**
     *  Tests if a step processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a step processor rule application service is supported. 
     *
     *  @return <code> true </code> if step processor rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up step processor enablers is supported. 
     *
     *  @return <code> true </code> if step processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying step processor enablers is supported. 
     *
     *  @return <code> true </code> if step processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching step processor enablers is supported. 
     *
     *  @return <code> true </code> if step processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a step processor enabler administrative service is supported. 
     *
     *  @return <code> true </code> if step processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a step processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if step processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a step processor enabler office lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor enabler office lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerOffice() {
        return (false);
    }


    /**
     *  Tests if a step processor enabler office service is supported. 
     *
     *  @return <code> true </code> if step processor enabler office 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a step processor enabler office lookup service is supported. 
     *
     *  @return <code> true </code> if a step processor enabler office service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerSmartOffice() {
        return (false);
    }


    /**
     *  Tests if a step processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a step processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if step processor enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up process enabler is supported. 
     *
     *  @return <code> true </code> if process enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying process enabler is supported. 
     *
     *  @return <code> true </code> if process enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching process enabler is supported. 
     *
     *  @return <code> true </code> if process enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a process enabler administrative service is supported. 
     *
     *  @return <code> true </code> if process enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a process enabler notification service is supported. 
     *
     *  @return <code> true </code> if process enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a process enabler office lookup service is supported. 
     *
     *  @return <code> true </code> if a process enabler office lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerOffice() {
        return (false);
    }


    /**
     *  Tests if a process enabler office service is supported. 
     *
     *  @return <code> true </code> if process enabler office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a process enabler office lookup service is supported. 
     *
     *  @return <code> true </code> if a process enabler office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerSmartOffice() {
        return (false);
    }


    /**
     *  Tests if a process enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a process enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a process enabler rule application service is supported. 
     *
     *  @return <code> true </code> if a process enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> StepConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> StepConstrainer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepConstrainerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepConstrainerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> StepConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  stepConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> StepConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepConstrainerRecordType(org.osid.type.Type stepConstrainerRecordType) {
        return (this.stepConstrainerRecordTypes.contains(stepConstrainerRecordType));
    }


    /**
     *  Adds support for a step constrainer record type.
     *
     *  @param stepConstrainerRecordType a step constrainer record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepConstrainerRecordType</code> is <code>null</code>
     */

    protected void addStepConstrainerRecordType(org.osid.type.Type stepConstrainerRecordType) {
        this.stepConstrainerRecordTypes.add(stepConstrainerRecordType);
        return;
    }


    /**
     *  Removes support for a step constrainer record type.
     *
     *  @param stepConstrainerRecordType a step constrainer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepConstrainerRecordType</code> is <code>null</code>
     */

    protected void removeStepConstrainerRecordType(org.osid.type.Type stepConstrainerRecordType) {
        this.stepConstrainerRecordTypes.remove(stepConstrainerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> StepConstrainer </code> search record types. 
     *
     *  @return a list containing the supported <code> StepConstrainer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepConstrainerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepConstrainerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> StepConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  stepConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> StepConstrainer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepConstrainerSearchRecordType(org.osid.type.Type stepConstrainerSearchRecordType) {
        return (this.stepConstrainerSearchRecordTypes.contains(stepConstrainerSearchRecordType));
    }


    /**
     *  Adds support for a step constrainer search record type.
     *
     *  @param stepConstrainerSearchRecordType a step constrainer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void addStepConstrainerSearchRecordType(org.osid.type.Type stepConstrainerSearchRecordType) {
        this.stepConstrainerSearchRecordTypes.add(stepConstrainerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a step constrainer search record type.
     *
     *  @param stepConstrainerSearchRecordType a step constrainer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void removeStepConstrainerSearchRecordType(org.osid.type.Type stepConstrainerSearchRecordType) {
        this.stepConstrainerSearchRecordTypes.remove(stepConstrainerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> StepConstrainerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> StepConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepConstrainerEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepConstrainerEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> StepConstrainerEnabler </code> record type 
     *  is supported. 
     *
     *  @param  stepConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> StepConstrainerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerRecordType(org.osid.type.Type stepConstrainerEnablerRecordType) {
        return (this.stepConstrainerEnablerRecordTypes.contains(stepConstrainerEnablerRecordType));
    }


    /**
     *  Adds support for a step constrainer enabler record type.
     *
     *  @param stepConstrainerEnablerRecordType a step constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void addStepConstrainerEnablerRecordType(org.osid.type.Type stepConstrainerEnablerRecordType) {
        this.stepConstrainerEnablerRecordTypes.add(stepConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a step constrainer enabler record type.
     *
     *  @param stepConstrainerEnablerRecordType a step constrainer enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void removeStepConstrainerEnablerRecordType(org.osid.type.Type stepConstrainerEnablerRecordType) {
        this.stepConstrainerEnablerRecordTypes.remove(stepConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> StepConstrainerEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> StepConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepConstrainerEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepConstrainerEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> StepConstrainerEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  stepConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> StepConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsStepConstrainerEnablerSearchRecordType(org.osid.type.Type stepConstrainerEnablerSearchRecordType) {
        return (this.stepConstrainerEnablerSearchRecordTypes.contains(stepConstrainerEnablerSearchRecordType));
    }


    /**
     *  Adds support for a step constrainer enabler search record type.
     *
     *  @param stepConstrainerEnablerSearchRecordType a step constrainer enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addStepConstrainerEnablerSearchRecordType(org.osid.type.Type stepConstrainerEnablerSearchRecordType) {
        this.stepConstrainerEnablerSearchRecordTypes.add(stepConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a step constrainer enabler search record type.
     *
     *  @param stepConstrainerEnablerSearchRecordType a step constrainer enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeStepConstrainerEnablerSearchRecordType(org.osid.type.Type stepConstrainerEnablerSearchRecordType) {
        this.stepConstrainerEnablerSearchRecordTypes.remove(stepConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> StepProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> StepProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepProcessorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepProcessorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> StepProcessor </code> record type is 
     *  supported. 
     *
     *  @param  stepProcessorRecordType a <code> Type </code> indicating a 
     *          <code> StepProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> stepProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepProcessorRecordType(org.osid.type.Type stepProcessorRecordType) {
        return (this.stepProcessorRecordTypes.contains(stepProcessorRecordType));
    }


    /**
     *  Adds support for a step processor record type.
     *
     *  @param stepProcessorRecordType a step processor record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepProcessorRecordType</code> is <code>null</code>
     */

    protected void addStepProcessorRecordType(org.osid.type.Type stepProcessorRecordType) {
        this.stepProcessorRecordTypes.add(stepProcessorRecordType);
        return;
    }


    /**
     *  Removes support for a step processor record type.
     *
     *  @param stepProcessorRecordType a step processor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepProcessorRecordType</code> is <code>null</code>
     */

    protected void removeStepProcessorRecordType(org.osid.type.Type stepProcessorRecordType) {
        this.stepProcessorRecordTypes.remove(stepProcessorRecordType);
        return;
    }


    /**
     *  Gets the supported <code> StepProcessor </code> search record types. 
     *
     *  @return a list containing the supported <code> StepProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepProcessorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepProcessorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> StepProcessor </code> search record type is 
     *  supported. 
     *
     *  @param  stepProcessorSearchRecordType a <code> Type </code> indicating 
     *          a <code> StepProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepProcessorSearchRecordType(org.osid.type.Type stepProcessorSearchRecordType) {
        return (this.stepProcessorSearchRecordTypes.contains(stepProcessorSearchRecordType));
    }


    /**
     *  Adds support for a step processor search record type.
     *
     *  @param stepProcessorSearchRecordType a step processor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void addStepProcessorSearchRecordType(org.osid.type.Type stepProcessorSearchRecordType) {
        this.stepProcessorSearchRecordTypes.add(stepProcessorSearchRecordType);
        return;
    }


    /**
     *  Removes support for a step processor search record type.
     *
     *  @param stepProcessorSearchRecordType a step processor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void removeStepProcessorSearchRecordType(org.osid.type.Type stepProcessorSearchRecordType) {
        this.stepProcessorSearchRecordTypes.remove(stepProcessorSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> StepProcessorEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> StepProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepProcessorEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepProcessorEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> StepProcessorEnabler </code> record type is 
     *  supported. 
     *
     *  @param  stepProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> StepProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerRecordType(org.osid.type.Type stepProcessorEnablerRecordType) {
        return (this.stepProcessorEnablerRecordTypes.contains(stepProcessorEnablerRecordType));
    }


    /**
     *  Adds support for a step processor enabler record type.
     *
     *  @param stepProcessorEnablerRecordType a step processor enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void addStepProcessorEnablerRecordType(org.osid.type.Type stepProcessorEnablerRecordType) {
        this.stepProcessorEnablerRecordTypes.add(stepProcessorEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a step processor enabler record type.
     *
     *  @param stepProcessorEnablerRecordType a step processor enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void removeStepProcessorEnablerRecordType(org.osid.type.Type stepProcessorEnablerRecordType) {
        this.stepProcessorEnablerRecordTypes.remove(stepProcessorEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> StepProcessorEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> StepProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStepProcessorEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.stepProcessorEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> StepProcessorEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  stepProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> StepProcessorEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsStepProcessorEnablerSearchRecordType(org.osid.type.Type stepProcessorEnablerSearchRecordType) {
        return (this.stepProcessorEnablerSearchRecordTypes.contains(stepProcessorEnablerSearchRecordType));
    }


    /**
     *  Adds support for a step processor enabler search record type.
     *
     *  @param stepProcessorEnablerSearchRecordType a step processor enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>stepProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addStepProcessorEnablerSearchRecordType(org.osid.type.Type stepProcessorEnablerSearchRecordType) {
        this.stepProcessorEnablerSearchRecordTypes.add(stepProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a step processor enabler search record type.
     *
     *  @param stepProcessorEnablerSearchRecordType a step processor enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>stepProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeStepProcessorEnablerSearchRecordType(org.osid.type.Type stepProcessorEnablerSearchRecordType) {
        this.stepProcessorEnablerSearchRecordTypes.remove(stepProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProcessEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ProcessEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.processEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProcessEnabler </code> record type is 
     *  supported. 
     *
     *  @param  processEnablerRecordType a <code> Type </code> indicating a 
     *          <code> ProcessEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> processEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessEnablerRecordType(org.osid.type.Type processEnablerRecordType) {
        return (this.processEnablerRecordTypes.contains(processEnablerRecordType));
    }


    /**
     *  Adds support for a process enabler record type.
     *
     *  @param processEnablerRecordType a process enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>processEnablerRecordType</code> is <code>null</code>
     */

    protected void addProcessEnablerRecordType(org.osid.type.Type processEnablerRecordType) {
        this.processEnablerRecordTypes.add(processEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a process enabler record type.
     *
     *  @param processEnablerRecordType a process enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>processEnablerRecordType</code> is <code>null</code>
     */

    protected void removeProcessEnablerRecordType(org.osid.type.Type processEnablerRecordType) {
        this.processEnablerRecordTypes.remove(processEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProcessEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ProcessEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcessEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.processEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProcessEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  processEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> ProcessEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          processEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcessEnablerSearchRecordType(org.osid.type.Type processEnablerSearchRecordType) {
        return (this.processEnablerSearchRecordTypes.contains(processEnablerSearchRecordType));
    }


    /**
     *  Adds support for a process enabler search record type.
     *
     *  @param processEnablerSearchRecordType a process enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>processEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addProcessEnablerSearchRecordType(org.osid.type.Type processEnablerSearchRecordType) {
        this.processEnablerSearchRecordTypes.add(processEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a process enabler search record type.
     *
     *  @param processEnablerSearchRecordType a process enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>processEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeProcessEnablerSearchRecordType(org.osid.type.Type processEnablerSearchRecordType) {
        this.processEnablerSearchRecordTypes.remove(processEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer lookup service. 
     *
     *  @return a <code> StepConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerLookupSession getStepConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerLookupSession getStepConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerLookupSession getStepConstrainerLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerLookupSession getStepConstrainerLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer query service. 
     *
     *  @return a <code> StepConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQuerySession getStepConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQuerySession getStepConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQuerySession getStepConstrainerQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQuerySession getStepConstrainerQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer search service. 
     *
     *  @return a <code> StepConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSearchSession getStepConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSearchSession getStepConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSearchSession getStepConstrainerSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSearchSession getStepConstrainerSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer administration service. 
     *
     *  @return a <code> StepConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerAdminSession getStepConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerAdminSession getStepConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerAdminSession getStepConstrainerAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerAdminSession getStepConstrainerAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer notification service. 
     *
     *  @param  stepConstrainerReceiver the notification callback 
     *  @return a <code> StepConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepConstrainerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerNotificationSession getStepConstrainerNotificationSession(org.osid.workflow.rules.StepConstrainerReceiver stepConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer notification service. 
     *
     *  @param  stepConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepConstrainerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerNotificationSession getStepConstrainerNotificationSession(org.osid.workflow.rules.StepConstrainerReceiver stepConstrainerReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer notification service for the given office. 
     *
     *  @param  stepConstrainerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepConstrainerReceiver 
     *          </code> or <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerNotificationSession getStepConstrainerNotificationSessionForOffice(org.osid.workflow.rules.StepConstrainerReceiver stepConstrainerReceiver, 
                                                                                                                     org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer notification service for the given office. 
     *
     *  @param  stepConstrainerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepConstrainerReceiver, 
     *          officeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerNotificationSession getStepConstrainerNotificationSessionForOffice(org.osid.workflow.rules.StepConstrainerReceiver stepConstrainerReceiver, 
                                                                                                                     org.osid.id.Id officeId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step constrainer/office 
     *  mappings for step constrainers. 
     *
     *  @return a <code> StepConstrainerOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerOfficeSession getStepConstrainerOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step constrainer/office 
     *  mappings for step constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerOfficeSession getStepConstrainerOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  constrainer to office. 
     *
     *  @return a <code> StepConstrainerOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerOfficeAssignmentSession getStepConstrainerOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  constrainer to office. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerOfficeAssignmentSession getStepConstrainerOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step constrainer smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSmartOfficeSession getStepConstrainerSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step constrainer smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerSmartOfficeSession getStepConstrainerSmartOfficeSession(org.osid.id.Id officeId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a step. 
     *
     *  @return a <code> StepConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleLookupSession getStepConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  ta step. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleLookupSession getStepConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer mapping lookup service for the given office for looking up 
     *  rules applied to a step. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleLookupSession getStepConstrainerRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer mapping lookup service for the given office for looking up 
     *  rules applied to a qeue. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleLookupSession getStepConstrainerRuleLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer assignment service to apply to steps. 
     *
     *  @return a <code> StepConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleApplicationSession getStepConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer assignment service to apply to steps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleApplicationSession getStepConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer assignment service for the given office to apply to steps. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleApplicationSession getStepConstrainerRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer assignment service for the given office to apply to steps. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerRuleApplicationSession getStepConstrainerRuleApplicationSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler lookup service. 
     *
     *  @return a <code> StepConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerLookupSession getStepConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerLookupSession getStepConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerLookupSession getStepConstrainerEnablerLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerLookupSession getStepConstrainerEnablerLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler query service. 
     *
     *  @return a <code> StepConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerQuerySession getStepConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerQuerySession getStepConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerQuerySession getStepConstrainerEnablerQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerQuerySession getStepConstrainerEnablerQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler search service. 
     *
     *  @return a <code> StepConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSearchSession getStepConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSearchSession getStepConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enablers earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSearchSession getStepConstrainerEnablerSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enablers earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSearchSession getStepConstrainerEnablerSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler administration service. 
     *
     *  @return a <code> StepConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerAdminSession getStepConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerAdminSession getStepConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerAdminSession getStepConstrainerEnablerAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerAdminSession getStepConstrainerEnablerAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler notification service. 
     *
     *  @param  stepConstrainerEnablerReceiver the notification callback 
     *  @return a <code> StepConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerNotificationSession getStepConstrainerEnablerNotificationSession(org.osid.workflow.rules.StepConstrainerEnablerReceiver stepConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler notification service. 
     *
     *  @param  stepConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerNotificationSession getStepConstrainerEnablerNotificationSession(org.osid.workflow.rules.StepConstrainerEnablerReceiver stepConstrainerEnablerReceiver, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler notification service for the given office. 
     *
     *  @param  stepConstrainerEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerReceiver </code> or <code> officeId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerNotificationSession getStepConstrainerEnablerNotificationSessionForOffice(org.osid.workflow.rules.StepConstrainerEnablerReceiver stepConstrainerEnablerReceiver, 
                                                                                                                                   org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler notification service for the given office. 
     *
     *  @param  stepConstrainerEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepConstrainerEnablerReceiver, officeId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerNotificationSession getStepConstrainerEnablerNotificationSessionForOffice(org.osid.workflow.rules.StepConstrainerEnablerReceiver stepConstrainerEnablerReceiver, 
                                                                                                                                   org.osid.id.Id officeId, 
                                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step constrainer 
     *  enabler/office mappings for step constrainer enablers. 
     *
     *  @return a <code> StepConstrainerEnablerOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerOfficeSession getStepConstrainerEnablerOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step constrainer 
     *  enabler/office mappings for step constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerOfficeSession getStepConstrainerEnablerOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  constrainer enablers to office. 
     *
     *  @return a <code> StepConstrainerEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerOfficeAssignmentSession getStepConstrainerEnablerOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  constrainer enablers to office. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerOfficeAssignmentSession getStepConstrainerEnablerOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step constrainer enabler 
     *  smart office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSmartOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSmartOfficeSession getStepConstrainerEnablerSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step constrainer enabler 
     *  smart office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerSmartOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerSmartOfficeSession getStepConstrainerEnablerSmartOfficeSession(org.osid.id.Id officeId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return a <code> StepConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleLookupSession getStepConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleLookupSession getStepConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler mapping lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleLookupSession getStepConstrainerEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler mapping lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleLookupSession getStepConstrainerEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler assignment service. 
     *
     *  @return a <code> StepConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleApplicationSession getStepConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleApplicationSession getStepConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler assignment service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleApplicationSession getStepConstrainerEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepConstrainerEnablerRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step 
     *  constrainer enabler assignment service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerRuleApplicationSession getStepConstrainerEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepConstrainerEnablerRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  lookup service. 
     *
     *  @return a <code> StepProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorLookupSession getStepProcessorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorLookupSession getStepProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorLookupSession getStepProcessorLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorLookupSession getStepProcessorLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  query service. 
     *
     *  @return a <code> StepProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQuerySession getStepProcessorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQuerySession getStepProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQuerySession getStepProcessorQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorQuerySession getStepProcessorQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  search service. 
     *
     *  @return a <code> StepProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSearchSession getStepProcessorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSearchSession getStepProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSearchSession getStepProcessorSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSearchSession getStepProcessorSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  administration service. 
     *
     *  @return a <code> StepProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorAdminSession getStepProcessorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorAdminSession getStepProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorAdminSession getStepProcessorAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorAdminSession getStepProcessorAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  notification service. 
     *
     *  @param  stepProcessorReceiver the notification callback 
     *  @return a <code> StepProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorNotificationSession getStepProcessorNotificationSession(org.osid.workflow.rules.StepProcessorReceiver stepProcessorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  notification service. 
     *
     *  @param  stepProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> stepProcessorReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorNotificationSession getStepProcessorNotificationSession(org.osid.workflow.rules.StepProcessorReceiver stepProcessorReceiver, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  notification service for the given office. 
     *
     *  @param  stepProcessorReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepProcessorReceiver 
     *          </code> or <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorNotificationSession getStepProcessorNotificationSessionForOffice(org.osid.workflow.rules.StepProcessorReceiver stepProcessorReceiver, 
                                                                                                                 org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  notification service for the given office. 
     *
     *  @param  stepProcessorReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> stepProcessorReceiver, 
     *          officeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorNotificationSession getStepProcessorNotificationSessionForOffice(org.osid.workflow.rules.StepProcessorReceiver stepProcessorReceiver, 
                                                                                                                 org.osid.id.Id officeId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step processor/office 
     *  mappings for step processors. 
     *
     *  @return a <code> StepProcessorOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorOfficeSession getStepProcessorOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step processor/office 
     *  mappings for step processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorOfficeSession getStepProcessorOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  processor to office. 
     *
     *  @return a <code> StepProcessorOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorOfficeAssignmentSession getStepProcessorOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  processor to office. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorOfficeAssignmentSession getStepProcessorOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step processor smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSmartOfficeSession getStepProcessorSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step processor smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorSmartOfficeSession getStepProcessorSmartOfficeSession(org.osid.id.Id officeId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  mapping lookup service for looking up the rules applied to a step. 
     *
     *  @return a <code> StepProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleLookupSession getStepProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  mapping lookup service for looking up the rules applied to a step 
     *  processor. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleLookupSession getStepProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  mapping lookup service for the given office for looking up rules 
     *  applied to a step. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleLookupSession getStepProcessorRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  mapping lookup service for the given office for looking up rules 
     *  applied to a step. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleLookupSession getStepProcessorRuleLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  assignment service. 
     *
     *  @return a <code> StepProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleApplicationSession getStepProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  assignment service to apply to steps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleApplicationSession getStepProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  assignment service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleApplicationSession getStepProcessorRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  assignment service for the given office to apply to steps. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorRuleApplicationSession getStepProcessorRuleApplicationSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler lookup service. 
     *
     *  @return a <code> StepProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerLookupSession getStepProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerLookupSession getStepProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerLookupSession getStepProcessorEnablerLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerLookupSession getStepProcessorEnablerLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler query service. 
     *
     *  @return a <code> StepProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerQuerySession getStepProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerQuerySession getStepProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerQuerySession getStepProcessorEnablerQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerQuerySession getStepProcessorEnablerQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler search service. 
     *
     *  @return a <code> StepProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSearchSession getStepProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSearchSession getStepProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enablers earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSearchSession getStepProcessorEnablerSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enablers earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSearchSession getStepProcessorEnablerSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler administration service. 
     *
     *  @return a <code> StepProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerAdminSession getStepProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerAdminSession getStepProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerAdminSession getStepProcessorEnablerAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerAdminSession getStepProcessorEnablerAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler notification service. 
     *
     *  @param  stepProcessorEnablerReceiver the notification callback 
     *  @return a <code> StepProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerNotificationSession getStepProcessorEnablerNotificationSession(org.osid.workflow.rules.StepProcessorEnablerReceiver stepProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler notification service. 
     *
     *  @param  stepProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerNotificationSession getStepProcessorEnablerNotificationSession(org.osid.workflow.rules.StepProcessorEnablerReceiver stepProcessorEnablerReceiver, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler notification service for the given office. 
     *
     *  @param  stepProcessorEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerReceiver </code> or <code> officeId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerNotificationSession getStepProcessorEnablerNotificationSessionForOffice(org.osid.workflow.rules.StepProcessorEnablerReceiver stepProcessorEnablerReceiver, 
                                                                                                                               org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler notification service for the given office. 
     *
     *  @param  stepProcessorEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          stepProcessorEnablerReceiver, officeId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerNotificationSession getStepProcessorEnablerNotificationSessionForOffice(org.osid.workflow.rules.StepProcessorEnablerReceiver stepProcessorEnablerReceiver, 
                                                                                                                               org.osid.id.Id officeId, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step processor 
     *  enabler/office mappings for step processor enablers. 
     *
     *  @return a <code> StepProcessorEnablerOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerOfficeSession getStepProcessorEnablerOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup step processor 
     *  enabler/office mappings for step processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerOfficeSession getStepProcessorEnablerOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  processor enablers to office. 
     *
     *  @return a <code> StepProcessorEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerOfficeAssignmentSession getStepProcessorEnablerOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning step 
     *  processor enablers to step processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerOfficeAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerOfficeAssignmentSession getStepProcessorEnablerOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step processor enabler 
     *  smart office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSmartOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSmartOfficeSession getStepProcessorEnablerSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage step processor enabler 
     *  smart office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerSmartOffice() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerSmartOfficeSession getStepProcessorEnablerSmartOfficeSession(org.osid.id.Id officeId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> StepProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleLookupSession getStepProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleLookupSession getStepProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler mapping lookup service. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleLookupSession getStepProcessorEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler mapping lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleLookupSession getStepProcessorEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler assignment service. 
     *
     *  @return a <code> StepProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleApplicationSession getStepProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleApplicationSession getStepProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler assignment service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> StepProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleApplicationSession getStepProcessorEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getStepProcessorEnablerRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the step processor 
     *  enabler assignment service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> StepProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStepProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerRuleApplicationSession getStepProcessorEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getStepProcessorEnablerRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler lookup service. 
     *
     *  @return a <code> ProcessEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerLookupSession getProcessEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerLookupSession getProcessEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerLookupSession getProcessEnablerLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler lookup service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerLookupSession getProcessEnablerLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler query service. 
     *
     *  @return a <code> ProcessEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerQuerySession getProcessEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerQuerySession getProcessEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerQuerySession getProcessEnablerQuerySessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler query service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerQuerySession getProcessEnablerQuerySessionForOffice(org.osid.id.Id officeId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerQuerySessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler search service. 
     *
     *  @return a <code> ProcessEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSearchSession getProcessEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException a <code> 
     *          ProcessEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSearchSession getProcessEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSearchSession getProcessEnablerSearchSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler earch service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSearchSession getProcessEnablerSearchSessionForOffice(org.osid.id.Id officeId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerSearchSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler administration service. 
     *
     *  @return a <code> ProcessEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerAdminSession getProcessEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerAdminSession getProcessEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerAdminSession getProcessEnablerAdminSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler administration service for the given office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerAdminSession getProcessEnablerAdminSessionForOffice(org.osid.id.Id officeId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerAdminSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler notification service. 
     *
     *  @param  processEnablerReceiver the notification callback 
     *  @return a <code> ProcessEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerNotificationSession getProcessEnablerNotificationSession(org.osid.workflow.rules.ProcessEnablerReceiver processEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler notification service. 
     *
     *  @param  processEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> processEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerNotificationSession getProcessEnablerNotificationSession(org.osid.workflow.rules.ProcessEnablerReceiver processEnablerReceiver, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler notification service for the given office. 
     *
     *  @param  processEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> processEnablerReceiver 
     *          </code> or <code> officeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerNotificationSession getProcessEnablerNotificationSessionForOffice(org.osid.workflow.rules.ProcessEnablerReceiver processEnablerReceiver, 
                                                                                                                   org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler notification service for the given office. 
     *
     *  @param  processEnablerReceiver the notification callback 
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no office found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> processEnablerReceiver, 
     *          officeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerNotificationSession getProcessEnablerNotificationSessionForOffice(org.osid.workflow.rules.ProcessEnablerReceiver processEnablerReceiver, 
                                                                                                                   org.osid.id.Id officeId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerNotificationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup process enabler/office 
     *  mappings for process enablers. 
     *
     *  @return a <code> ProcessEnablerOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerOfficeSession getProcessEnablerOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup process enabler/office 
     *  mappings for process enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerOfficeSession getProcessEnablerOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning process 
     *  enabler to office. 
     *
     *  @return a <code> ProcessEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerOfficeAssignmentSession getProcessEnablerOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning process 
     *  enabler to office. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerOfficeAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerOfficeAssignmentSession getProcessEnablerOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage process enabler smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSmartOfficeSession getProcessEnablerSmartOfficeSession(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage process enabler smart 
     *  office. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerSmartOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerSmartOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerSmartOfficeSession getProcessEnablerSmartOfficeSession(org.osid.id.Id officeId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerSmartOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  process. 
     *
     *  @return a <code> ProcessEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleLookupSession getProcessEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler mapping lookup service for looking up the rules applied to a 
     *  process. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleLookupSession getProcessEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler mapping lookup service for the given office for looking up 
     *  rules applied to a process. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleLookupSession getProcessEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler mapping lookup service for the given office for looking up 
     *  rules applied to a process. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleLookupSession getProcessEnablerRuleLookupSessionForOffice(org.osid.id.Id officeId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerRuleLookupSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler assignment service to apply to processs. 
     *
     *  @return a <code> ProcessEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleApplicationSession getProcessEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler assignment service to apply to processs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleApplicationSession getProcessEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler assignment service for the given office to apply to processs. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @return a <code> ProcessEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Office </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleApplicationSession getProcessEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesManager.getProcessEnablerRuleApplicationSessionForOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the process 
     *  enabler assignment service for the given office to apply to processs. 
     *
     *  @param  officeId the <code> Id </code> of the <code> Office </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcessEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException <code> officeId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerRuleApplicationSession getProcessEnablerRuleApplicationSessionForOffice(org.osid.id.Id officeId, 
                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.workflow.rules.WorkflowRulesProxyManager.getProcessEnablerRuleApplicationSessionForOffice not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.stepConstrainerRecordTypes.clear();
        this.stepConstrainerRecordTypes.clear();

        this.stepConstrainerSearchRecordTypes.clear();
        this.stepConstrainerSearchRecordTypes.clear();

        this.stepConstrainerEnablerRecordTypes.clear();
        this.stepConstrainerEnablerRecordTypes.clear();

        this.stepConstrainerEnablerSearchRecordTypes.clear();
        this.stepConstrainerEnablerSearchRecordTypes.clear();

        this.stepProcessorRecordTypes.clear();
        this.stepProcessorRecordTypes.clear();

        this.stepProcessorSearchRecordTypes.clear();
        this.stepProcessorSearchRecordTypes.clear();

        this.stepProcessorEnablerRecordTypes.clear();
        this.stepProcessorEnablerRecordTypes.clear();

        this.stepProcessorEnablerSearchRecordTypes.clear();
        this.stepProcessorEnablerSearchRecordTypes.clear();

        this.processEnablerRecordTypes.clear();
        this.processEnablerRecordTypes.clear();

        this.processEnablerSearchRecordTypes.clear();
        this.processEnablerSearchRecordTypes.clear();

        return;
    }
}
