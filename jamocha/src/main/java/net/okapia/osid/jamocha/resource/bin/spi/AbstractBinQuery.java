//
// AbstractBinQuery.java
//
//     A template for making a Bin Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.bin.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for bins.
 */

public abstract class AbstractBinQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.resource.BinQuery {

    private final java.util.Collection<org.osid.resource.records.BinQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches bins with any resource. 
     *
     *  @param  match <code> true </code> to match bins with any resource, 
     *          <code> false </code> to match bins with no resources 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        return;
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the bin <code> Id </code> for this query to match bins that have 
     *  the specified bin as an ancestor. 
     *
     *  @param  binid a bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorBinId(org.osid.id.Id binid, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBinIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBinQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getAncestorBinQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBinQuery() is false");
    }


    /**
     *  Matches bins with any ancestor. 
     *
     *  @param  match <code> true </code> to match bins with any ancestor, 
     *          <code> false </code> to match root bins 
     */

    @OSID @Override
    public void matchAnyAncestorBin(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor bin terms. 
     */

    @OSID @Override
    public void clearAncestorBinTerms() {
        return;
    }


    /**
     *  Sets the bin <code> Id </code> for this query to match bins that have 
     *  the specified bin as a descendant. 
     *
     *  @param  binid a bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantBinId(org.osid.id.Id binid, boolean match) {
        return;
    }


    /**
     *  Clears the descendant bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBinIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBinQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getDescendantBinQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBinQuery() is false");
    }


    /**
     *  Matches bins with any descendant. 
     *
     *  @param  match <code> true </code> to match bins with any descendant, 
     *          <code> false </code> to match leaf bins 
     */

    @OSID @Override
    public void matchAnyDescendantBin(boolean match) {
        return;
    }


    /**
     *  Clears the descendant bin terms. 
     */

    @OSID @Override
    public void clearDescendantBinTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given bin query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a bin implementing the requested record.
     *
     *  @param binRecordType a bin record type
     *  @return the bin query record
     *  @throws org.osid.NullArgumentException
     *          <code>binRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(binRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.BinQueryRecord getBinQueryRecord(org.osid.type.Type binRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.BinQueryRecord record : this.records) {
            if (record.implementsRecordType(binRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(binRecordType + " is not supported");
    }


    /**
     *  Adds a record to this bin query. 
     *
     *  @param binQueryRecord bin query record
     *  @param binRecordType bin record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBinQueryRecord(org.osid.resource.records.BinQueryRecord binQueryRecord, 
                                          org.osid.type.Type binRecordType) {

        addRecordType(binRecordType);
        nullarg(binQueryRecord, "bin query record");
        this.records.add(binQueryRecord);        
        return;
    }
}
