//
// AbstractImmutableAliasResponse.java
//
//     Wraps a mutable AliasResponse to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transaction.batch.aliasresponse.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AliasResponse</code> to hide modifiers. This
 *  wrapper provides an immutized AliasResponse from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying aliasResponse whose state changes are visible.
 */

public abstract class AbstractImmutableAliasResponse
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutable
    implements org.osid.transaction.batch.AliasResponse {

    private final org.osid.transaction.batch.AliasResponse aliasResponse;


    /**
     *  Constructs a new <code>AbstractImmutableAliasResponse</code>.
     *
     *  @param aliasResponse the alias response to immutablize
     *  @throws org.osid.NullArgumentException <code>aliasResponse</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAliasResponse(org.osid.transaction.batch.AliasResponse aliasResponse) {
        super(aliasResponse);
        this.aliasResponse = aliasResponse;
        return;
    }


    /**
     *  Gets the reference <code> Id. </code> 
     *
     *  @return the reference <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.aliasResponse.getReferenceId());
    }


    /**
     *  Gets the alias <code> Id. </code> 
     *
     *  @return the alias <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAliasId() {
        return (this.aliasResponse.getAliasId());
    }


    /**
     *  Tests if this item within the create operation was successful. 
     *
     *  @return <code> true </code> if the alias operation was successful, 
     *          <code> false </code> if it was not successful 
     */

    @OSID @Override
    public boolean isSuccessful() {
        return (this.aliasResponse.isSuccessful());
    }


    /**
     *  Gets the error message for an unsuccessful item within the create 
     *  operation. 
     *
     *  @return the message 
     *  @throws org.osid.IllegalStateException <code> isSuccessful() </code> 
     *          is <code> true </code> 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getErrorMessage() {
        return (this.aliasResponse.getErrorMessage());
    }
}

