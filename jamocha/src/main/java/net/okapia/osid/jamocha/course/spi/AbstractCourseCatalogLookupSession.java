//
// AbstractCourseCatalogLookupSession.java
//
//    A starter implementation framework for providing a CourseCatalog
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a CourseCatalog
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCourseCatalogs(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCourseCatalogLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.CourseCatalogLookupSession {

    private boolean pedantic = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Tests if this user can perform <code>CourseCatalog</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourseCatalogs() {
        return (true);
    }


    /**
     *  A complete view of the <code>CourseCatalog</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseCatalogView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CourseCatalog</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseCatalogView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>CourseCatalog</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseCatalog</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CourseCatalog</code> and
     *  retained for compatibility.
     *
     *  @param  courseCatalogId <code>Id</code> of the
     *          <code>CourseCatalog</code>
     *  @return the course catalog
     *  @throws org.osid.NotFoundException <code>courseCatalogId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseCatalogId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.CourseCatalogList courseCatalogs = getCourseCatalogs()) {
            while (courseCatalogs.hasNext()) {
                org.osid.course.CourseCatalog courseCatalog = courseCatalogs.getNextCourseCatalog();
                if (courseCatalog.getId().equals(courseCatalogId)) {
                    return (courseCatalog);
                }
            }
        } 

        throw new org.osid.NotFoundException(courseCatalogId + " not found");
    }


    /**
     *  Gets a <code>CourseCatalogList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courseCatalogs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CourseCatalogs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCourseCatalogs()</code>.
     *
     *  @param  courseCatalogIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByIds(org.osid.id.IdList courseCatalogIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.CourseCatalog> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = courseCatalogIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCourseCatalog(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("course catalog " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.coursecatalog.LinkedCourseCatalogList(ret));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> corresponding to the given
     *  course catalog genus <code>Type</code> which does not include
     *  course catalogs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCourseCatalogs()</code>.
     *
     *  @param  courseCatalogGenusType a courseCatalog genus type 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByGenusType(org.osid.type.Type courseCatalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.coursecatalog.CourseCatalogGenusFilterList(getCourseCatalogs(), courseCatalogGenusType));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> corresponding to the given
     *  course catalog genus <code>Type</code> and include any additional
     *  course catalogs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourseCatalogs()</code>.
     *
     *  @param  courseCatalogGenusType a courseCatalog genus type 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByParentGenusType(org.osid.type.Type courseCatalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCourseCatalogsByGenusType(courseCatalogGenusType));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> containing the given
     *  course catalog record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourseCatalogs()</code>.
     *
     *  @param  courseCatalogRecordType a courseCatalog record type 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByRecordType(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.coursecatalog.CourseCatalogRecordFilterList(getCourseCatalogs(), courseCatalogRecordType));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known course catalogs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  course catalogs that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>CourseCatalog</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.course.coursecatalog.CourseCatalogProviderFilterList(getCourseCatalogs(), resourceId));
    }


    /**
     *  Gets all <code>CourseCatalogs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>CourseCatalogs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.CourseCatalogList getCourseCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the course catalog list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of course catalogs
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.CourseCatalogList filterCourseCatalogsOnViews(org.osid.course.CourseCatalogList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
