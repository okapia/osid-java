//
// AbstractProduct.java
//
//     Defines a Product builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.product.spi;


/**
 *  Defines a <code>Product</code> builder.
 */

public abstract class AbstractProductBuilder<T extends AbstractProductBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ordering.product.ProductMiter product;


    /**
     *  Constructs a new <code>AbstractProductBuilder</code>.
     *
     *  @param product the product to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProductBuilder(net.okapia.osid.jamocha.builder.ordering.product.ProductMiter product) {
        super(product);
        this.product = product;
        return;
    }


    /**
     *  Builds the product.
     *
     *  @return the new product
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ordering.Product build() {
        (new net.okapia.osid.jamocha.builder.validator.ordering.product.ProductValidator(getValidations())).validate(this.product);
        return (new net.okapia.osid.jamocha.builder.ordering.product.ImmutableProduct(this.product));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the product miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ordering.product.ProductMiter getMiter() {
        return (this.product);
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    public T code(String code) {
        getMiter().setCode(code);
        return (self());
    }


    /**
     *  Adds a price schedule.
     *
     *  @param schedule a price schedule
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    public T priceSchedule(org.osid.ordering.PriceSchedule schedule) {
        getMiter().addPriceSchedule(schedule);
        return (self());
    }


    /**
     *  Sets all the price schedules.
     *
     *  @param schedules a collection of price schedules
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          is <code>null</code>
     */

    public T priceSchedules(java.util.Collection<org.osid.ordering.PriceSchedule> schedules) {
        getMiter().setPriceSchedules(schedules);
        return (self());
    }


    /**
     *  Sets the availability.
     *
     *  @param availability an availability
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>availability</code> is negative
     */

    public T availability(long availability) {
        getMiter().setAvailability(availability);
        return (self());
    }


    /**
     *  Adds a Product record.
     *
     *  @param record a product record
     *  @param recordType the type of product record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.ordering.records.ProductRecord record, org.osid.type.Type recordType) {
        getMiter().addProductRecord(record, recordType);
        return (self());
    }
}       


