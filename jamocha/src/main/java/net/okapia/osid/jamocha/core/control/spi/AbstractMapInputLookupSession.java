//
// AbstractMapInputLookupSession
//
//    A simple framework for providing an Input lookup service
//    backed by a fixed collection of inputs.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Input lookup service backed by a
 *  fixed collection of inputs. The inputs are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Inputs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapInputLookupSession
    extends net.okapia.osid.jamocha.control.spi.AbstractInputLookupSession
    implements org.osid.control.InputLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.control.Input> inputs = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.control.Input>());


    /**
     *  Makes an <code>Input</code> available in this session.
     *
     *  @param  input an input
     *  @throws org.osid.NullArgumentException <code>input<code>
     *          is <code>null</code>
     */

    protected void putInput(org.osid.control.Input input) {
        this.inputs.put(input.getId(), input);
        return;
    }


    /**
     *  Makes an array of inputs available in this session.
     *
     *  @param  inputs an array of inputs
     *  @throws org.osid.NullArgumentException <code>inputs<code>
     *          is <code>null</code>
     */

    protected void putInputs(org.osid.control.Input[] inputs) {
        putInputs(java.util.Arrays.asList(inputs));
        return;
    }


    /**
     *  Makes a collection of inputs available in this session.
     *
     *  @param  inputs a collection of inputs
     *  @throws org.osid.NullArgumentException <code>inputs<code>
     *          is <code>null</code>
     */

    protected void putInputs(java.util.Collection<? extends org.osid.control.Input> inputs) {
        for (org.osid.control.Input input : inputs) {
            this.inputs.put(input.getId(), input);
        }

        return;
    }


    /**
     *  Removes an Input from this session.
     *
     *  @param  inputId the <code>Id</code> of the input
     *  @throws org.osid.NullArgumentException <code>inputId<code> is
     *          <code>null</code>
     */

    protected void removeInput(org.osid.id.Id inputId) {
        this.inputs.remove(inputId);
        return;
    }


    /**
     *  Gets the <code>Input</code> specified by its <code>Id</code>.
     *
     *  @param  inputId <code>Id</code> of the <code>Input</code>
     *  @return the input
     *  @throws org.osid.NotFoundException <code>inputId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>inputId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Input getInput(org.osid.id.Id inputId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.control.Input input = this.inputs.get(inputId);
        if (input == null) {
            throw new org.osid.NotFoundException("input not found: " + inputId);
        }

        return (input);
    }


    /**
     *  Gets all <code>Inputs</code>. In plenary mode, the returned
     *  list contains all known inputs or an error
     *  results. Otherwise, the returned list may contain only those
     *  inputs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Inputs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.InputList getInputs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.input.ArrayInputList(this.inputs.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.inputs.clear();
        super.close();
        return;
    }
}
