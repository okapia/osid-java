//
// StringQueryTerm.java
//
//     Formats SQL for a string query term.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc.mysql;


/**
 *  Formats SQL for a string query term.
 */

public class StringQueryTerm 
    extends net.okapia.osid.jamocha.assembly.query.jdbc.StringQueryTerm
    implements net.okapia.osid.jamocha.assembly.query.QueryTerm {

    private final MysqlStringMatchTypes stringMatch;


    /**
     *  Constructs a new <code>StringQueryTerm</code>.
     *
     *  @param column name of query column
     *  @param value <code>true</code> or <code>false</code>
     *  @throws org.osid.NullArgumentException is <code>null</code>
     *          argument provided
     *  @throws org.osid.UnsupportedException
     *          <code>stringMatchType</code> is unsupported
     */

    public StringQueryTerm(String column, String value, 
                           org.osid.type.Type stringMatchType, boolean match) {

        super(column, value, stringMatchType, match);
        this.stringMatch = QueryAssembler.getStringMatchType(stringMatchType);
        return;
    }


    /**
     *  Gets the query string for this term.
     *
     *  @return the query string
     */

    @Override
    public String toString() {
        switch (this.stringMatch) {
        case IGNORECASE:
        case WORDIGNORECASE:
            return ("UPPER(" + getColumn() + ") " + getOperator() + getExpression());
        default:
            return (getColumn() + getOperator() + "'" + getExpression() + "'");
        }
    }


    /**
     *  Gets the query operator for this term.
     *
     *  @return the query operator
     */

    @Override
    protected String getOperator() {
        
        if (isPositive()) {
            switch(this.stringMatch) {
            case WORD:
            case WILDCARD:
                return (" LIKE ");
            case REGEX:
                return (" REGEX ");
            case SOUND:
                return (" SOUNDS LIKE ");
            default:
                return ("=");
            }
        } else {
            switch(this.stringMatch) {
            case WORD:
            case WILDCARD:
                return (" NOT LIKE ");
            case REGEX:
                return (" NOT REGEX ");
            case SOUND:
                return (" NOT SOUNDS LIKE ");
            default:
                return ("!=");
            }
        }
    }
    

    /**
     *  Gets the query expression for this term.
     *
     *  @return the query expression
     */

    protected String getExpression() {
        switch (this.stringMatch) {
        case IGNORECASE:
            return ("UPPER('" + getString() + "')");
        case WORD:
            return ("[:space:]" + getString() + "[:space:]");
        case WORDIGNORECASE:
            return ("[:space:] UPPER('" + getString() + "')[:space:]");
        case WILDCARD:
            return (getString().replace("*", "%").replace("?", "_"));
        default:        
            return (getString());
        }
    }
}
