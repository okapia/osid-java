//
// AbstractGradingManager.java
//
//     An adapter for a GradingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a GradingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterGradingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.grading.GradingManager>
    implements org.osid.grading.GradingManager {


    /**
     *  Constructs a new {@code AbstractAdapterGradingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterGradingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterGradingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterGradingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a grade system lookup service is supported. 
     *
     *  @return true if grade system lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemLookup() {
        return (getAdapteeManager().supportsGradeSystemLookup());
    }


    /**
     *  Tests if a grade system query service is supported. 
     *
     *  @return <code> true </code> if grade system query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (getAdapteeManager().supportsGradeSystemQuery());
    }


    /**
     *  Tests if a grade system search service is supported. 
     *
     *  @return <code> true </code> if grade system search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearch() {
        return (getAdapteeManager().supportsGradeSystemSearch());
    }


    /**
     *  Tests if a grade system administrative service is supported. 
     *
     *  @return <code> true </code> if grade system admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemAdmin() {
        return (getAdapteeManager().supportsGradeSystemAdmin());
    }


    /**
     *  Tests if grade system notification is supported. Messages may be sent 
     *  when grade entries are created, modified, or deleted. 
     *
     *  @return <code> true </code> if grade system notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemNotification() {
        return (getAdapteeManager().supportsGradeSystemNotification());
    }


    /**
     *  Tests if a grade system to gradebook lookup session is available. 
     *
     *  @return <code> true </code> if grade system gradebook lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemGradebook() {
        return (getAdapteeManager().supportsGradeSystemGradebook());
    }


    /**
     *  Tests if a grade system to gradebook assignment session is available. 
     *
     *  @return <code> true </code> if grade system gradebook assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemGradebookAssignment() {
        return (getAdapteeManager().supportsGradeSystemGradebookAssignment());
    }


    /**
     *  Tests if a grade system smart gradebook session is available. 
     *
     *  @return <code> true </code> if grade system smart gradebook is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSmartGradebook() {
        return (getAdapteeManager().supportsGradeSystemSmartGradebook());
    }


    /**
     *  Tests if a grade entry lookup service is supported. 
     *
     *  @return true if grade entry lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryLookup() {
        return (getAdapteeManager().supportsGradeEntryLookup());
    }


    /**
     *  Tests if a grade entry query service is supported. 
     *
     *  @return true if grade entry query is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryQuery() {
        return (getAdapteeManager().supportsGradeEntryQuery());
    }


    /**
     *  Tests if a grade entry search service is supported. 
     *
     *  @return <code> true </code> if grade entry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntrySearch() {
        return (getAdapteeManager().supportsGradeEntrySearch());
    }


    /**
     *  Tests if a grade entry administrative service is supported. 
     *
     *  @return <code> true </code> if grade entry admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryAdmin() {
        return (getAdapteeManager().supportsGradeEntryAdmin());
    }


    /**
     *  Tests if grade entry notification is supported. 
     *
     *  @return <code> true </code> if grade entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryNotification() {
        return (getAdapteeManager().supportsGradeEntryNotification());
    }


    /**
     *  Tests if a gradebook column lookup service is supported. 
     *
     *  @return true if gradebook column lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnLookup() {
        return (getAdapteeManager().supportsGradebookColumnLookup());
    }


    /**
     *  Tests if a gradebook column query service is supported. 
     *
     *  @return <code> true </code> if grade system query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (getAdapteeManager().supportsGradebookColumnQuery());
    }


    /**
     *  Tests if a gradebook column search service is supported. 
     *
     *  @return <code> true </code> if grade system search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSearch() {
        return (getAdapteeManager().supportsGradebookColumnSearch());
    }


    /**
     *  Tests if a gradebook column administrative service is supported. 
     *
     *  @return <code> true </code> if gradebook column admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnAdmin() {
        return (getAdapteeManager().supportsGradebookColumnAdmin());
    }


    /**
     *  Tests if gradebook column notification is supported. Messages may be 
     *  sent when grade entries are created, modified, or deleted. 
     *
     *  @return <code> true </code> if gradebook column notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnNotification() {
        return (getAdapteeManager().supportsGradebookColumnNotification());
    }


    /**
     *  Tests if a gradebook column to gradebook lookup session is available. 
     *
     *  @return <code> true </code> if gradebook column gradebook lookup 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnGradebook() {
        return (getAdapteeManager().supportsGradebookColumnGradebook());
    }


    /**
     *  Tests if a gradebook column to gradebook assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if gradebook column gradebook assignment 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnGradebookAssignment() {
        return (getAdapteeManager().supportsGradebookColumnGradebookAssignment());
    }


    /**
     *  Tests if a gradebook column smart gradebookt session is available. 
     *
     *  @return <code> true </code> if gradebook column amsrt gradebook is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSmartGradebook() {
        return (getAdapteeManager().supportsGradebookColumnSmartGradebook());
    }


    /**
     *  Tests if a gradebook lookup service is supported. 
     *
     *  @return <code> true </code> if gradebook lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookLookup() {
        return (getAdapteeManager().supportsGradebookLookup());
    }


    /**
     *  Tests if a gradebook query service is supported. 
     *
     *  @return <code> true </code> if gradebook query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (getAdapteeManager().supportsGradebookQuery());
    }


    /**
     *  Tests if a gradebook search service is supported. 
     *
     *  @return <code> true </code> if gradebook search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookSearch() {
        return (getAdapteeManager().supportsGradebookSearch());
    }


    /**
     *  Tests if a gradebook administrative service is supported. 
     *
     *  @return <code> true </code> if gradebook admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookAdmin() {
        return (getAdapteeManager().supportsGradebookAdmin());
    }


    /**
     *  Tests if gradebook notification is supported. Messages may be sent 
     *  when gradebooks are created, modified, or deleted. 
     *
     *  @return <code> true </code> if gradebook notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookNotification() {
        return (getAdapteeManager().supportsGradebookNotification());
    }


    /**
     *  Tests if a gradebook hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a gradebook hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookHierarchy() {
        return (getAdapteeManager().supportsGradebookHierarchy());
    }


    /**
     *  Tests if gradebook hierarchy design is supported. 
     *
     *  @return <code> true </code> if a gradebook hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookHierarchyDesign() {
        return (getAdapteeManager().supportsGradebookHierarchyDesign());
    }


    /**
     *  Tests if a grading batch service is supported. 
     *
     *  @return <code> true </code> if a grading batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingBatch() {
        return (getAdapteeManager().supportsGradingBatch());
    }


    /**
     *  Tests if a grading calculation service is supported. 
     *
     *  @return <code> true </code> if a grading calculation service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingCalculation() {
        return (getAdapteeManager().supportsGradingCalculation());
    }


    /**
     *  Tests if a grade system transform service is supported. 
     *
     *  @return <code> true </code> if a grading transform service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingTransform() {
        return (getAdapteeManager().supportsGradingTransform());
    }


    /**
     *  Gets the supported <code> Grade </code> record types. 
     *
     *  @return a list containing the supported <code> Grade </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeRecordTypes() {
        return (getAdapteeManager().getGradeRecordTypes());
    }


    /**
     *  Tests if the given <code> Grade </code> record type is supported. 
     *
     *  @param  gradeRecordType a <code> Type </code> indicating a <code> 
     *          Grade </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> gradeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeRecordType(org.osid.type.Type gradeRecordType) {
        return (getAdapteeManager().supportsGradeRecordType(gradeRecordType));
    }


    /**
     *  Gets the supported <code> GradeSystem </code> record types. 
     *
     *  @return a list containing the supported <code> GradeSystem </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeSystemRecordTypes() {
        return (getAdapteeManager().getGradeSystemRecordTypes());
    }


    /**
     *  Tests if the given <code> GradeSystem </code> record type is 
     *  supported. 
     *
     *  @param  gradeSystemRecordType a <code> Type </code> indicating a 
     *          <code> GradeSystem </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> gradeSystemRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeSystemRecordType(org.osid.type.Type gradeSystemRecordType) {
        return (getAdapteeManager().supportsGradeSystemRecordType(gradeSystemRecordType));
    }


    /**
     *  Gets the supported <code> GradeSystem </code> search record types. 
     *
     *  @return a list containing the supported <code> GradeSystem </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeSystemSearchRecordTypes() {
        return (getAdapteeManager().getGradeSystemSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> GradeSystem </code> search record type is 
     *  supported. 
     *
     *  @param  gradeSystemSearchRecordType a <code> Type </code> indicating a 
     *          <code> GradeSystem </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradeSystemSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearchRecordType(org.osid.type.Type gradeSystemSearchRecordType) {
        return (getAdapteeManager().supportsGradeSystemSearchRecordType(gradeSystemSearchRecordType));
    }


    /**
     *  Gets the supported <code> GradeEntry </code> record types. 
     *
     *  @return a list containing the supported <code> GradeEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeEntryRecordTypes() {
        return (getAdapteeManager().getGradeEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> GradeEntry </code> record type is supported. 
     *
     *  @param  gradeEntryRecordType a <code> Type </code> indicating a <code> 
     *          GradeEntry </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> gradeEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeEntryRecordType(org.osid.type.Type gradeEntryRecordType) {
        return (getAdapteeManager().supportsGradeEntryRecordType(gradeEntryRecordType));
    }


    /**
     *  Gets the supported <code> GradeEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> GradeEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradeEntrySearchRecordTypes() {
        return (getAdapteeManager().getGradeEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> GradeEntry </code> search record type is 
     *  supported. 
     *
     *  @param  gradeEntrySearchRecordType a <code> Type </code> indicating a 
     *          <code> GradeEntry </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradeEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradeEntrySearchRecordType(org.osid.type.Type gradeEntrySearchRecordType) {
        return (getAdapteeManager().supportsGradeEntrySearchRecordType(gradeEntrySearchRecordType));
    }


    /**
     *  Gets the supported <code> GradebookColumn </code> record types. 
     *
     *  @return a list containing the supported <code> GradebookColumn </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookColumnRecordTypes() {
        return (getAdapteeManager().getGradebookColumnRecordTypes());
    }


    /**
     *  Tests if the given <code> GradebookColumn </code> record type is 
     *  supported. 
     *
     *  @param  gradebookColumnRecordType a <code> Type </code> indicating a 
     *          <code> GradebookColumn </code> type 
     *  @return <code> true </code> if the given gradebook column record 
     *          <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookColumnRecordType(org.osid.type.Type gradebookColumnRecordType) {
        return (getAdapteeManager().supportsGradebookColumnRecordType(gradebookColumnRecordType));
    }


    /**
     *  Gets the supported gradebook column search record types. 
     *
     *  @return a list containing the supported <code> GradebookColumn </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookColumnSearchRecordTypes() {
        return (getAdapteeManager().getGradebookColumnSearchRecordTypes());
    }


    /**
     *  Tests if the given gradebook column search record type is supported. 
     *
     *  @param  gradebookColumnSearchRecordType a <code> Type </code> 
     *          indicating a <code> GradebookColumn </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSearchRecordType(org.osid.type.Type gradebookColumnSearchRecordType) {
        return (getAdapteeManager().supportsGradebookColumnSearchRecordType(gradebookColumnSearchRecordType));
    }


    /**
     *  Gets the supported <code> GradebookColumnSummary </code> record types. 
     *
     *  @return a list containing the supported <code> GradebookColumnSummary 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookColumnSummaryRecordTypes() {
        return (getAdapteeManager().getGradebookColumnSummaryRecordTypes());
    }


    /**
     *  Tests if the given <code> GradebookColumnSummary </code> record type 
     *  is supported. 
     *
     *  @param  gradebookColumnSummaryRecordType a <code> Type </code> 
     *          indicating a <code> GradebookColumnSummary </code> type 
     *  @return <code> true </code> if the given gradebook column summary 
     *          record <code> Type </code> is supported, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookColumnSummaryRecordType(org.osid.type.Type gradebookColumnSummaryRecordType) {
        return (getAdapteeManager().supportsGradebookColumnSummaryRecordType(gradebookColumnSummaryRecordType));
    }


    /**
     *  Gets the supported <code> Gradebook </code> record types. 
     *
     *  @return a list containing the supported <code> Gradebook </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookRecordTypes() {
        return (getAdapteeManager().getGradebookRecordTypes());
    }


    /**
     *  Tests if the given <code> Gradebook </code> record type is supported. 
     *
     *  @param  gradebookRecordType a <code> Type </code> indicating a <code> 
     *          Gradebook </code> type 
     *  @return <code> true </code> if the given gradebook record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> gradebookRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookRecordType(org.osid.type.Type gradebookRecordType) {
        return (getAdapteeManager().supportsGradebookRecordType(gradebookRecordType));
    }


    /**
     *  Gets the supported gradebook search record types. 
     *
     *  @return a list containing the supported <code> Gradebook </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getGradebookSearchRecordTypes() {
        return (getAdapteeManager().getGradebookSearchRecordTypes());
    }


    /**
     *  Tests if the given gradebook search record type is supported. 
     *
     *  @param  gradebookSearchRecordType a <code> Type </code> indicating a 
     *          <code> Gradebook </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsGradebookSearchRecordType(org.osid.type.Type gradebookSearchRecordType) {
        return (getAdapteeManager().supportsGradebookSearchRecordType(gradebookSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  lookup service. 
     *
     *  @return a <code> GradeSystemLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemLookupSession getGradeSystemLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemLookupSession getGradeSystemLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemLookupSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  query service. 
     *
     *  @return a <code> GradeSystemQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuerySession getGradeSystemQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuerySession getGradeSystemQuerySessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemQuerySessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  search service. 
     *
     *  @return a <code> GradeSystemSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchSession getGradeSystemSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchSession getGradeSystemSearchSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemSearchSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  administration service. 
     *
     *  @return a <code> GradeSystemAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemAdminSession getGradeSystemAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemAdminSession getGradeSystemAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemAdminSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  system changes. 
     *
     *  @param  gradeSystemReceiver the grade system receiver 
     *  @return a <code> GradeSystemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemNotificationSession getGradeSystemNotificationSession(org.osid.grading.GradeSystemReceiver gradeSystemReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemNotificationSession(gradeSystemReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade system 
     *  notification service for the given gradebook. 
     *
     *  @param  gradeSystemReceiver the grade system receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeSystemNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradeSystemReceiver 
     *          </code> or <code> gradebookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemNotificationSession getGradeSystemNotificationSessionForGradebook(org.osid.grading.GradeSystemReceiver gradeSystemReceiver, 
                                                                                                         org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemNotificationSessionForGradebook(gradeSystemReceiver, gradebookId));
    }


    /**
     *  Gets the session for retrieving grade system to gradebook mappings. 
     *
     *  @return a <code> GradeSystemGradebookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemGradebook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemGradebookSession getGradeSystemGradebookSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemGradebookSession());
    }


    /**
     *  Gets the session for assigning grade system to gradebook mappings. 
     *
     *  @return a <code> GradeSystemGradebookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemGradebookAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemGradebookSession getGradeSystemGradebookAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemGradebookAssignmentSession());
    }


    /**
     *  Gets the session for managing smart gradebooks of grade systems. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return a <code> GradeSystemSmartGradebookSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSmartGradebook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSmartGradebookSession getGradeSystemSmartGradebookSession(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemSmartGradebookSession(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  lookup service. 
     *
     *  @return a <code> GradeEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryLookupSession getGradeEntryLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryLookupSession getGradeEntryLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryLookupSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  query service. 
     *
     *  @return a <code> GradeEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuerySession getGradeEntryQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuerySession getGradeEntryQuerySessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryQuerySessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  search service. 
     *
     *  @return a <code> GradeEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntrySearchSession getGradeEntrySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntrySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntrySearchSession getGradeEntrySearchSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntrySearchSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  administration service. 
     *
     *  @return a <code> GradeEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryAdminSession getGradeEntryAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryAdminSession getGradeEntryAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryAdminSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the notification session for notifications pertaining to grade 
     *  entry changes. 
     *
     *  @param  receiver the grade entry receiver 
     *  @return a <code> GradeEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryNotificationSession getGradeEntryNotificationSession(org.osid.grading.GradeEntryReceiver receiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryNotificationSession(receiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the grade entry 
     *  notification service for the given gradebook. 
     *
     *  @param  receiver the grade entry receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradeEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> receiver </code> or 
     *          <code> gradebookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryNotificationSession getGradeEntryNotificationSessionForGradebook(org.osid.grading.GradeEntryReceiver receiver, 
                                                                                                       org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryNotificationSessionForGradebook(receiver, gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column lookup service. 
     *
     *  @return a <code> GradebookColumnLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnLookupSession getGradebookColumnLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column lookup service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnLookupSession getGradebookColumnLookupSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnLookupSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column query service. 
     *
     *  @return a <code> GradebookColumnQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuerySession getGradebookColumnQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column query service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuerySession getGradebookColumnQuerySessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnQuerySessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column search service. 
     *
     *  @return a <code> GradebookColumnSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSearchSession getGradebookColumnSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column search service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSearchSession getGradebookColumnSearchSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnSearchSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column administration service. 
     *
     *  @return a <code> GradebookColumnAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnAdminSession getGradebookColumnAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column admin service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnAdminSession getGradebookColumnAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnAdminSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  gradebook column changes. 
     *
     *  @param  gradebookColumnReceiver the grade system receiver 
     *  @return a <code> GradebookColumnNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnNotificationSession getGradebookColumnNotificationSession(org.osid.grading.GradebookColumnReceiver gradebookColumnReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnNotificationSession(gradebookColumnReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the gradebook 
     *  column notification service for the given gradebook. 
     *
     *  @param  gradebookColumnReceiver the gradebook column receiver 
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return <code> a GradebookColumnNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnReceiver 
     *          </code> or <code> gradebookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnNotificationSession getGradebookColumnNotificationSessionForGradebook(org.osid.grading.GradebookColumnReceiver gradebookColumnReceiver, 
                                                                                                                 org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnNotificationSessionForGradebook(gradebookColumnReceiver, gradebookId));
    }


    /**
     *  Gets the session for retrieving gradebook column to gradebook 
     *  mappings. 
     *
     *  @return a <code> GradebookColumnGradebookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnGradebook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnGradebookSession getGradebookColumnGradebookSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnGradebookSession());
    }


    /**
     *  Gets the session for assigning gradebook column to gradebook mappings. 
     *
     *  @return a <code> GradebookColumnGradebookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnGradebookAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnGradebookAssignmentSession getGradebookColumnGradebookAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnGradebookAssignmentSession());
    }


    /**
     *  Gets the session for managing smart gradebooks of gradebook columns. 
     *
     *  @param  gradebookId the <code> Id </code> of the gradebook 
     *  @return a <code> GradebookColumnSmartGradebookSession </code> 
     *  @throws org.osid.NotFoundException <code> gradebookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnSmartGradebook() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSmartGradebookSession getGradebookColumnSmartGradebookSession(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnSmartGradebookSession(gradebookId));
    }


    /**
     *  Gets the OsidSession associated with the gradebook lookup service. 
     *
     *  @return a <code> GradebookLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookLookupSession getGradebookLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookLookupSession());
    }


    /**
     *  Gets the OsidSession associated with the gradebook query service. 
     *
     *  @return a <code> GradebookQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuerySession getGradebookQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookQuerySession());
    }


    /**
     *  Gets the OsidSession associated with the gradebook search service. 
     *
     *  @return a <code> GradebookSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookSearchSession getGradebookSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookSearchSession());
    }


    /**
     *  Gets the OsidSession associated with the gradebook administration 
     *  service. 
     *
     *  @return a <code> GradebookAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookAdmin() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookAdminSession getGradebookAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookAdminSession());
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  gradebook service changes. 
     *
     *  @param  gradebookReceiver the gradebook receiver 
     *  @return a <code> GradebookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookNotificationSession getGradebookNotificationSession(org.osid.grading.GradebookReceiver gradebookReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookNotificationSession(gradebookReceiver));
    }


    /**
     *  Gets the session traversing gradebook hierarchies. 
     *
     *  @return a <code> GradebookHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookHierarchySession getGradebookHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookHierarchySession());
    }


    /**
     *  Gets the session designing gradebook hierarchies. 
     *
     *  @return a <code> GradebookHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookHierarchyDesignSession getGradebookHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookHierarchyDesignSession());
    }


    /**
     *  Gets the <code> GradingBatchManager. </code> 
     *
     *  @return a <code> GradingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsGradingBatch() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradingBatchManager getGradingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradingBatchManager());
    }


    /**
     *  Gets the <code> GradingCalculationManager. </code> 
     *
     *  @return a <code> GradingCalculationManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingCalculation() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradingCalculationManager getGradingCalculationManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradingCalculationManager());
    }


    /**
     *  Gets the <code> GradingTransformManager. </code> 
     *
     *  @return a <code> GradingTransformManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingTransform() is false </code> 
     */

    @OSID @Override
    public org.osid.grading.transform.GradingTransformManager getGradingTransformManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradingTransformManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
