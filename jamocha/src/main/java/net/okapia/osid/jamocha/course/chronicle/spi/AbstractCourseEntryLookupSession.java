//
// AbstractCourseEntryLookupSession.java
//
//    A starter implementation framework for providing a CourseEntry
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a CourseEntry
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCourseEntries(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCourseEntryLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.chronicle.CourseEntryLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>CourseEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourseEntries() {
        return (true);
    }


    /**
     *  A complete view of the <code>CourseEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCourseEntryView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>CourseEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCourseEntryView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include course entries in course catalogs which are
     *  children of this course catalog in the course catalog
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only course entries whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveCourseEntryView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All course entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveCourseEntryView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>CourseEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CourseEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @param  courseEntryId <code>Id</code> of the
     *          <code>CourseEntry</code>
     *  @return the course entry
     *  @throws org.osid.NotFoundException <code>courseEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntry getCourseEntry(org.osid.id.Id courseEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.chronicle.CourseEntryList courseEntries = getCourseEntries()) {
            while (courseEntries.hasNext()) {
                org.osid.course.chronicle.CourseEntry courseEntry = courseEntries.getNextCourseEntry();
                if (courseEntry.getId().equals(courseEntryId)) {
                    return (courseEntry);
                }
            }
        } 

        throw new org.osid.NotFoundException(courseEntryId + " not found");
    }


    /**
     *  Gets a <code>CourseEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courseEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CourseEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, course entries are returned that are currently effective.
     *  In any effective mode, effective course entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCourseEntries()</code>.
     *
     *  @param  courseEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CourseEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByIds(org.osid.id.IdList courseEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.chronicle.CourseEntry> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = courseEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCourseEntry(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("course entry " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.chronicle.courseentry.LinkedCourseEntryList(ret));
    }


    /**
     *  Gets a <code>CourseEntryList</code> corresponding to the given
     *  course entry genus <code>Type</code> which does not include
     *  course entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course entries are returned that are currently effective.
     *  In any effective mode, effective course entries and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCourseEntries()</code>.
     *
     *  @param  courseEntryGenusType a courseEntry genus type 
     *  @return the returned <code>CourseEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByGenusType(org.osid.type.Type courseEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.CourseEntryGenusFilterList(getCourseEntries(), courseEntryGenusType));
    }


    /**
     *  Gets a <code>CourseEntryList</code> corresponding to the given
     *  course entry genus <code>Type</code> and include any additional
     *  course entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourseEntries()</code>.
     *
     *  @param  courseEntryGenusType a courseEntry genus type 
     *  @return the returned <code>CourseEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByParentGenusType(org.osid.type.Type courseEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCourseEntriesByGenusType(courseEntryGenusType));
    }


    /**
     *  Gets a <code>CourseEntryList</code> containing the given
     *  course entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCourseEntries()</code>.
     *
     *  @param  courseEntryRecordType a courseEntry record type 
     *  @return the returned <code>CourseEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesByRecordType(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.CourseEntryRecordFilterList(getCourseEntries(), courseEntryRecordType));
    }


    /**
     *  Gets a <code>CourseEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *  
     *  In active mode, course entries are returned that are currently
     *  active. In any status mode, active and inactive course entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>CourseEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.TemporalCourseEntryFilterList(getCourseEntries(), from, to));
    }
        

    /**
     *  Gets a list of course entries corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>CourseEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.CourseEntryList getCourseEntriesForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.CourseEntryFilterList(new StudentFilter(resourceId), getCourseEntries()));
    }


    /**
     *  Gets a list of course entries corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CourseEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.TemporalCourseEntryFilterList(getCourseEntriesForStudent(resourceId), from, to));
    }


    /**
     *  Gets a list of course entries corresponding to a course
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  courseId the <code>Id</code> of the course
     *  @return the returned <code>CourseEntryList</code>
     *  @throws org.osid.NullArgumentException <code>courseId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.CourseEntryList getCourseEntriesForCourse(org.osid.id.Id courseId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.CourseEntryFilterList(new CourseFilter(courseId), getCourseEntries()));
    }


    /**
     *  Gets a list of course entries corresponding to a course
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  courseId the <code>Id</code> of the course
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CourseEntryList</code>
     *  @throws org.osid.NullArgumentException <code>courseId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForCourseOnDate(org.osid.id.Id courseId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.TemporalCourseEntryFilterList(getCourseEntriesForCourse(courseId), from, to));
    }


    /**
     *  Gets a list of course entries corresponding to student and course
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  courseId the <code>Id</code> of the course
     *  @return the returned <code>CourseEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>courseId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForStudentAndCourse(org.osid.id.Id resourceId,
                                                                                         org.osid.id.Id courseId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.CourseEntryFilterList(new CourseFilter(courseId), getCourseEntriesForStudent(resourceId)));
    }


    /**
     *  Gets a list of course entries corresponding to student and course
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible
     *  through this session.
     *
     *  In effective mode, course entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  course entries and those currently expired are returned.
     *
     *  @param  courseId the <code>Id</code> of the course
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>CourseEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>courseId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntriesForStudentAndCourseOnDate(org.osid.id.Id resourceId,
                                                                                               org.osid.id.Id courseId,
                                                                                               org.osid.calendaring.DateTime from,
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.TemporalCourseEntryFilterList(getCourseEntriesForStudentAndCourse(resourceId, courseId), from, to));
    }


    /**
     *  Gets all <code>CourseEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  course entries or an error results. Otherwise, the returned list
     *  may contain only those course entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, course entries are returned that are currently
     *  effective.  In any effective mode, effective course entries and
     *  those currently expired are returned.
     *
     *  @return a list of <code>CourseEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.chronicle.CourseEntryList getCourseEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the course entry list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of course entries
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.chronicle.CourseEntryList filterCourseEntriesOnViews(org.osid.course.chronicle.CourseEntryList list)
        throws org.osid.OperationFailedException {

        org.osid.course.chronicle.CourseEntryList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.EffectiveCourseEntryFilterList(ret);
        }

        return (ret);
    }

    public static class StudentFilter
        implements net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.CourseEntryFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>StudentFilter</code>.
         *
         *  @param resourceId the student to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public StudentFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "student Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the CourseEntryFilterList to filter the 
         *  course entry list based on student.
         *
         *  @param courseEntry the course entry
         *  @return <code>true</code> to pass the course entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.chronicle.CourseEntry courseEntry) {
            return (courseEntry.getStudentId().equals(this.resourceId));
        }
    }


    public static class CourseFilter
        implements net.okapia.osid.jamocha.inline.filter.course.chronicle.courseentry.CourseEntryFilter {
         
        private final org.osid.id.Id courseId;
         
         
        /**
         *  Constructs a new <code>CourseFilter</code>.
         *
         *  @param courseId the course to filter
         *  @throws org.osid.NullArgumentException
         *          <code>courseId</code> is <code>null</code>
         */
        
        public CourseFilter(org.osid.id.Id courseId) {
            nullarg(courseId, "course Id");
            this.courseId = courseId;
            return;
        }

         
        /**
         *  Used by the CourseEntryFilterList to filter the 
         *  course entry list based on course.
         *
         *  @param courseEntry the course entry
         *  @return <code>true</code> to pass the course entry,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.chronicle.CourseEntry courseEntry) {
            return (courseEntry.getCourseId().equals(this.courseId));
        }
    }
}
