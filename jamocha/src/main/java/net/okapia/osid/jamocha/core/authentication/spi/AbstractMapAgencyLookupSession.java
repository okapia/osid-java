//
// AbstractMapAgencyLookupSession
//
//    A simple framework for providing an Agency lookup service
//    backed by a fixed collection of agencies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Agency lookup service backed by a
 *  fixed collection of agencies. The agencies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Agencies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAgencyLookupSession
    extends net.okapia.osid.jamocha.authentication.spi.AbstractAgencyLookupSession
    implements org.osid.authentication.AgencyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authentication.Agency> agencies = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authentication.Agency>());


    /**
     *  Makes an <code>Agency</code> available in this session.
     *
     *  @param  agency an agency
     *  @throws org.osid.NullArgumentException <code>agency<code>
     *          is <code>null</code>
     */

    protected void putAgency(org.osid.authentication.Agency agency) {
        this.agencies.put(agency.getId(), agency);
        return;
    }


    /**
     *  Makes an array of agencies available in this session.
     *
     *  @param  agencies an array of agencies
     *  @throws org.osid.NullArgumentException <code>agencies<code>
     *          is <code>null</code>
     */

    protected void putAgencies(org.osid.authentication.Agency[] agencies) {
        putAgencies(java.util.Arrays.asList(agencies));
        return;
    }


    /**
     *  Makes a collection of agencies available in this session.
     *
     *  @param  agencies a collection of agencies
     *  @throws org.osid.NullArgumentException <code>agencies<code>
     *          is <code>null</code>
     */

    protected void putAgencies(java.util.Collection<? extends org.osid.authentication.Agency> agencies) {
        for (org.osid.authentication.Agency agency : agencies) {
            this.agencies.put(agency.getId(), agency);
        }

        return;
    }


    /**
     *  Removes an Agency from this session.
     *
     *  @param  agencyId the <code>Id</code> of the agency
     *  @throws org.osid.NullArgumentException <code>agencyId<code> is
     *          <code>null</code>
     */

    protected void removeAgency(org.osid.id.Id agencyId) {
        this.agencies.remove(agencyId);
        return;
    }


    /**
     *  Gets the <code>Agency</code> specified by its <code>Id</code>.
     *
     *  @param  agencyId <code>Id</code> of the <code>Agency</code>
     *  @return the agency
     *  @throws org.osid.NotFoundException <code>agencyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>agencyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authentication.Agency agency = this.agencies.get(agencyId);
        if (agency == null) {
            throw new org.osid.NotFoundException("agency not found: " + agencyId);
        }

        return (agency);
    }


    /**
     *  Gets all <code>Agencies</code>. In plenary mode, the returned
     *  list contains all known agencies or an error
     *  results. Otherwise, the returned list may contain only those
     *  agencies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Agencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authentication.agency.ArrayAgencyList(this.agencies.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.agencies.clear();
        super.close();
        return;
    }
}
