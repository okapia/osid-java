//
// AbstractFederatingEndpointLookupSession.java
//
//     An abstract federating adapter for an EndpointLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.transport.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  EndpointLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingEndpointLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.transport.EndpointLookupSession>
    implements org.osid.transport.EndpointLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingEndpointLookupSession</code>.
     */

    protected AbstractFederatingEndpointLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.transport.EndpointLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Endpoint</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupEndpoints() {
        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            if (session.canLookupEndpoints()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Endpoint</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEndpointView() {
        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            session.useComparativeEndpointView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Endpoint</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEndpointView() {
        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            session.usePlenaryEndpointView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Endpoint</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Endpoint</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Endpoint</code> and
     *  retained for compatibility.
     *
     *  @param  endpointId <code>Id</code> of the
     *          <code>Endpoint</code>
     *  @return the endpoint
     *  @throws org.osid.NotFoundException <code>endpointId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>endpointId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.Endpoint getEndpoint(org.osid.id.Id endpointId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            try {
                return (session.getEndpoint(endpointId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(endpointId + " not found");
    }


    /**
     *  Gets an <code>EndpointList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  endpoints specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Endpoints</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  endpointIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>endpointIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByIds(org.osid.id.IdList endpointIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.transport.endpoint.MutableEndpointList ret = new net.okapia.osid.jamocha.transport.endpoint.MutableEndpointList();

        try (org.osid.id.IdList ids = endpointIds) {
            while (ids.hasNext()) {
                ret.addEndpoint(getEndpoint(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>EndpointList</code> corresponding to the given
     *  endpoint genus <code>Type</code> which does not include
     *  endpoints of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  endpoints or an error results. Otherwise, the returned list
     *  may contain only those endpoints that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  endpointGenusType an endpoint genus type 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>endpointGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByGenusType(org.osid.type.Type endpointGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.transport.endpoint.FederatingEndpointList ret = getEndpointList();

        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            ret.addEndpointList(session.getEndpointsByGenusType(endpointGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EndpointList</code> corresponding to the given
     *  endpoint genus <code>Type</code> and include any additional
     *  endpoints with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  endpoints or an error results. Otherwise, the returned list
     *  may contain only those endpoints that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  endpointGenusType an endpoint genus type 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>endpointGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByParentGenusType(org.osid.type.Type endpointGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.transport.endpoint.FederatingEndpointList ret = getEndpointList();

        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            ret.addEndpointList(session.getEndpointsByParentGenusType(endpointGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EndpointList</code> containing the given
     *  endpoint record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  endpoints or an error results. Otherwise, the returned list
     *  may contain only those endpoints that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  endpointRecordType an endpoint record type 
     *  @return the returned <code>Endpoint</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>endpointRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByRecordType(org.osid.type.Type endpointRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.transport.endpoint.FederatingEndpointList ret = getEndpointList();

        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            ret.addEndpointList(session.getEndpointsByRecordType(endpointRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>EndpointList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known endpoints or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  endpoints that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Endpoint</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpointsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.transport.endpoint.FederatingEndpointList ret = getEndpointList();

        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            ret.addEndpointList(session.getEndpointsByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Endpoints</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  endpoints or an error results. Otherwise, the returned list
     *  may contain only those endpoints that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Endpoints</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.transport.EndpointList getEndpoints()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.transport.endpoint.FederatingEndpointList ret = getEndpointList();

        for (org.osid.transport.EndpointLookupSession session : getSessions()) {
            ret.addEndpointList(session.getEndpoints());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.transport.endpoint.FederatingEndpointList getEndpointList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.transport.endpoint.ParallelEndpointList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.transport.endpoint.CompositeEndpointList());
        }
    }
}
