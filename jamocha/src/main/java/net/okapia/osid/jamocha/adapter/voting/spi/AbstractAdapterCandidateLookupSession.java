//
// AbstractAdapterCandidateLookupSession.java
//
//    A Candidate lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Candidate lookup session adapter.
 */

public abstract class AbstractAdapterCandidateLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.CandidateLookupSession {

    private final org.osid.voting.CandidateLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCandidateLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCandidateLookupSession(org.osid.voting.CandidateLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code Candidate} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCandidates() {
        return (this.session.canLookupCandidates());
    }


    /**
     *  A complete view of the {@code Candidate} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCandidateView() {
        this.session.useComparativeCandidateView();
        return;
    }


    /**
     *  A complete view of the {@code Candidate} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCandidateView() {
        this.session.usePlenaryCandidateView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include candidates in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only candidates whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveCandidateView() {
        this.session.useEffectiveCandidateView();
        return;
    }
    

    /**
     *  All candidates of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveCandidateView() {
        this.session.useAnyEffectiveCandidateView();
        return;
    }

     
    /**
     *  Gets the {@code Candidate} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Candidate} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Candidate} and
     *  retained for compatibility.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param candidateId {@code Id} of the {@code Candidate}
     *  @return the candidate
     *  @throws org.osid.NotFoundException {@code candidateId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code candidateId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Candidate getCandidate(org.osid.id.Id candidateId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidate(candidateId));
    }


    /**
     *  Gets a {@code CandidateList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  candidates specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Candidates} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Candidate} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code candidateIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByIds(org.osid.id.IdList candidateIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesByIds(candidateIds));
    }


    /**
     *  Gets a {@code CandidateList} corresponding to the given
     *  candidate genus {@code Type} which does not include
     *  candidates of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateGenusType a candidate genus type 
     *  @return the returned {@code Candidate} list
     *  @throws org.osid.NullArgumentException
     *          {@code candidateGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByGenusType(org.osid.type.Type candidateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesByGenusType(candidateGenusType));
    }


    /**
     *  Gets a {@code CandidateList} corresponding to the given
     *  candidate genus {@code Type} and include any additional
     *  candidates with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateGenusType a candidate genus type 
     *  @return the returned {@code Candidate} list
     *  @throws org.osid.NullArgumentException
     *          {@code candidateGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByParentGenusType(org.osid.type.Type candidateGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesByParentGenusType(candidateGenusType));
    }


    /**
     *  Gets a {@code CandidateList} containing the given
     *  candidate record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  candidateRecordType a candidate record type 
     *  @return the returned {@code Candidate} list
     *  @throws org.osid.NullArgumentException
     *          {@code candidateRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesByRecordType(org.osid.type.Type candidateRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesByRecordType(candidateRecordType));
    }


    /**
     *  Gets a {@code CandidateList} effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session.
     *  
     *  In active mode, candidates are returned that are currently
     *  active. In any status mode, active and inactive candidates are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Candidate} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesOnDate(from, to));
    }
        

    /**
     *  Gets a list of candidates corresponding to a race
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  raceId the {@code Id} of the race
     *  @return the returned {@code CandidateList}
     *  @throws org.osid.NullArgumentException {@code raceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRace(org.osid.id.Id raceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesForRace(raceId));
    }


    /**
     *  Gets a list of candidates corresponding to a race {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  raceId the {@code Id} of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CandidateList}
     *  @throws org.osid.NullArgumentException {@code raceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceOnDate(org.osid.id.Id raceId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesForRaceOnDate(raceId, from, to));
    }


    /**
     *  Gets a list of candidates corresponding to a resource {@code
     *  Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code CandidateList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesForResource(resourceId));
    }


    /**
     *  Gets a list of candidates corresponding to a resource {@code
     *  Id} and effective during the entire given date range inclusive
     *  but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CandidateList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForResourceOnDate(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of candidates corresponding to race and resource
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session.
     *
     *  In effective mode, candidates are returned that are
     *  currently effective.  In any effective mode, effective
     *  candidates and those currently expired are returned.
     *
     *  @param  raceId the {@code Id} of the race
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code CandidateList}
     *  @throws org.osid.NullArgumentException {@code raceId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceAndResource(org.osid.id.Id raceId,
                                                                         org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesForRaceAndResource(raceId, resourceId));
    }


    /**
     *  Gets a list of candidates corresponding to race and resource
     *  {@code Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective. In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code CandidateList}
     *  @throws org.osid.NullArgumentException {@code raceId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidatesForRaceAndResourceOnDate(org.osid.id.Id raceId,
                                                                               org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidatesForRaceAndResourceOnDate(raceId, resourceId, from, to));
    }


    /**
     *  Gets all {@code Candidates}. 
     *
     *  In plenary mode, the returned list contains all known
     *  candidates or an error results. Otherwise, the returned list
     *  may contain only those candidates that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, candidates are returned that are currently
     *  effective.  In any effective mode, effective candidates and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Candidates} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.CandidateList getCandidates()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCandidates());
    }
}
