//
// AbstractAvailabilityEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAvailabilityEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.rules.AvailabilityEnablerSearchResults {

    private org.osid.resourcing.rules.AvailabilityEnablerList availabilityEnablers;
    private final org.osid.resourcing.rules.AvailabilityEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.rules.records.AvailabilityEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAvailabilityEnablerSearchResults.
     *
     *  @param availabilityEnablers the result set
     *  @param availabilityEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>availabilityEnablers</code>
     *          or <code>availabilityEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAvailabilityEnablerSearchResults(org.osid.resourcing.rules.AvailabilityEnablerList availabilityEnablers,
                                            org.osid.resourcing.rules.AvailabilityEnablerQueryInspector availabilityEnablerQueryInspector) {
        nullarg(availabilityEnablers, "availability enablers");
        nullarg(availabilityEnablerQueryInspector, "availability enabler query inspectpr");

        this.availabilityEnablers = availabilityEnablers;
        this.inspector = availabilityEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the availability enabler list resulting from a search.
     *
     *  @return an availability enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablers() {
        if (this.availabilityEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.rules.AvailabilityEnablerList availabilityEnablers = this.availabilityEnablers;
        this.availabilityEnablers = null;
	return (availabilityEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.rules.AvailabilityEnablerQueryInspector getAvailabilityEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  availability enabler search record <code> Type. </code> This method must
     *  be used to retrieve an availabilityEnabler implementing the requested
     *  record.
     *
     *  @param availabilityEnablerSearchRecordType an availabilityEnabler search 
     *         record type 
     *  @return the availability enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(availabilityEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.AvailabilityEnablerSearchResultsRecord getAvailabilityEnablerSearchResultsRecord(org.osid.type.Type availabilityEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.rules.records.AvailabilityEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(availabilityEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(availabilityEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record availability enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAvailabilityEnablerRecord(org.osid.resourcing.rules.records.AvailabilityEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "availability enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
