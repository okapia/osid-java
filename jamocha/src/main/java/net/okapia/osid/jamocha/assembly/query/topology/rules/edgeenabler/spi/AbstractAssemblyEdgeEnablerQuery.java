//
// AbstractAssemblyEdgeEnablerQuery.java
//
//     An EdgeEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.topology.rules.edgeenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EdgeEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyEdgeEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.topology.rules.EdgeEnablerQuery,
               org.osid.topology.rules.EdgeEnablerQueryInspector,
               org.osid.topology.rules.EdgeEnablerSearchOrder {

    private final java.util.Collection<org.osid.topology.rules.records.EdgeEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.topology.rules.records.EdgeEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.topology.rules.records.EdgeEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEdgeEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEdgeEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the edge. 
     *
     *  @param  edgeId the edge <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> edgeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledEdgeId(org.osid.id.Id edgeId, boolean match) {
        getAssembler().addIdTerm(getRuledEdgeIdColumn(), edgeId, match);
        return;
    }


    /**
     *  Clears the edge <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledEdgeIdTerms() {
        getAssembler().clearTerms(getRuledEdgeIdColumn());
        return;
    }


    /**
     *  Gets the ruled edge <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledEdgeIdTerms() {
        return (getAssembler().getIdTerms(getRuledEdgeIdColumn()));
    }


    /**
     *  Gets the RuledEdgeId column name.
     *
     * @return the column name
     */

    protected String getRuledEdgeIdColumn() {
        return ("ruled_edge_id");
    }


    /**
     *  Tests if an <code> EdgeQuery </code> is available. 
     *
     *  @return <code> true </code> if an edge query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledEdgeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an edge. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the edge query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledEdgeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuery getRuledEdgeQuery() {
        throw new org.osid.UnimplementedException("supportsRuledEdgeQuery() is false");
    }


    /**
     *  Matches enablers mapped to any edge. 
     *
     *  @param  match <code> true </code> for enablers mapped to any edge, 
     *          <code> false </code> to match enablers mapped to no edge 
     */

    @OSID @Override
    public void matchAnyRuledEdge(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledEdgeColumn(), match);
        return;
    }


    /**
     *  Clears the edge query terms. 
     */

    @OSID @Override
    public void clearRuledEdgeTerms() {
        getAssembler().clearTerms(getRuledEdgeColumn());
        return;
    }


    /**
     *  Gets the ruled edge query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.EdgeQueryInspector[] getRuledEdgeTerms() {
        return (new org.osid.topology.EdgeQueryInspector[0]);
    }


    /**
     *  Gets the RuledEdge column name.
     *
     * @return the column name
     */

    protected String getRuledEdgeColumn() {
        return ("ruled_edge");
    }


    /**
     *  Matches enablers mapped to the graph. 
     *
     *  @param  graphId the graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraphId(org.osid.id.Id graphId, boolean match) {
        getAssembler().addIdTerm(getGraphIdColumn(), graphId, match);
        return;
    }


    /**
     *  Clears the graph <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearGraphIdTerms() {
        getAssembler().clearTerms(getGraphIdColumn());
        return;
    }


    /**
     *  Gets the graph <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraphIdTerms() {
        return (getAssembler().getIdTerms(getGraphIdColumn()));
    }


    /**
     *  Gets the GraphId column name.
     *
     * @return the column name
     */

    protected String getGraphIdColumn() {
        return ("graph_id");
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getGraphQuery() {
        throw new org.osid.UnimplementedException("supportsGraphQuery() is false");
    }


    /**
     *  Clears the graph query terms. 
     */

    @OSID @Override
    public void clearGraphTerms() {
        getAssembler().clearTerms(getGraphColumn());
        return;
    }


    /**
     *  Gets the graph query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }


    /**
     *  Gets the Graph column name.
     *
     * @return the column name
     */

    protected String getGraphColumn() {
        return ("graph");
    }


    /**
     *  Tests if this edgeEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  edgeEnablerRecordType an edge enabler record type 
     *  @return <code>true</code> if the edgeEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type edgeEnablerRecordType) {
        for (org.osid.topology.rules.records.EdgeEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(edgeEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  edgeEnablerRecordType the edge enabler record type 
     *  @return the edge enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.rules.records.EdgeEnablerQueryRecord getEdgeEnablerQueryRecord(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.rules.records.EdgeEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(edgeEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  edgeEnablerRecordType the edge enabler record type 
     *  @return the edge enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.rules.records.EdgeEnablerQueryInspectorRecord getEdgeEnablerQueryInspectorRecord(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.rules.records.EdgeEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(edgeEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param edgeEnablerRecordType the edge enabler record type
     *  @return the edge enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.rules.records.EdgeEnablerSearchOrderRecord getEdgeEnablerSearchOrderRecord(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.rules.records.EdgeEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(edgeEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this edge enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param edgeEnablerQueryRecord the edge enabler query record
     *  @param edgeEnablerQueryInspectorRecord the edge enabler query inspector
     *         record
     *  @param edgeEnablerSearchOrderRecord the edge enabler search order record
     *  @param edgeEnablerRecordType edge enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerQueryRecord</code>,
     *          <code>edgeEnablerQueryInspectorRecord</code>,
     *          <code>edgeEnablerSearchOrderRecord</code> or
     *          <code>edgeEnablerRecordTypeedgeEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addEdgeEnablerRecords(org.osid.topology.rules.records.EdgeEnablerQueryRecord edgeEnablerQueryRecord, 
                                      org.osid.topology.rules.records.EdgeEnablerQueryInspectorRecord edgeEnablerQueryInspectorRecord, 
                                      org.osid.topology.rules.records.EdgeEnablerSearchOrderRecord edgeEnablerSearchOrderRecord, 
                                      org.osid.type.Type edgeEnablerRecordType) {

        addRecordType(edgeEnablerRecordType);

        nullarg(edgeEnablerQueryRecord, "edge enabler query record");
        nullarg(edgeEnablerQueryInspectorRecord, "edge enabler query inspector record");
        nullarg(edgeEnablerSearchOrderRecord, "edge enabler search odrer record");

        this.queryRecords.add(edgeEnablerQueryRecord);
        this.queryInspectorRecords.add(edgeEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(edgeEnablerSearchOrderRecord);
        
        return;
    }
}
