//
// AbstractFiscalPeriodQueryInspector.java
//
//     A template for making a FiscalPeriodQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.fiscalperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for fiscal periods.
 */

public abstract class AbstractFiscalPeriodQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.financials.FiscalPeriodQueryInspector {

    private final java.util.Collection<org.osid.financials.records.FiscalPeriodQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the display label query terms. 
     *
     *  @return the display label terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayLabelTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the fiscal year query terms. 
     *
     *  @return the fiscal year terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getFiscalYearTerms() {
        return (new org.osid.search.terms.IntegerRangeTerm[0]);
    }


    /**
     *  Gets the start date query terms. 
     *
     *  @return the start date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the end date query terms. 
     *
     *  @return the end date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the duration query terms. 
     *
     *  @return the duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the budget deadline query terms. 
     *
     *  @return the budget deadline query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getBudgetDeadlineTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the posting deadline query terms. 
     *
     *  @return the posting deadline query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getPostingDeadlineTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the closing query terms. 
     *
     *  @return the closing query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClosingTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given fiscal period query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a fiscal period implementing the requested record.
     *
     *  @param fiscalPeriodRecordType a fiscal period record type
     *  @return the fiscal period query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>fiscalPeriodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fiscalPeriodRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.FiscalPeriodQueryInspectorRecord getFiscalPeriodQueryInspectorRecord(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.FiscalPeriodQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(fiscalPeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fiscalPeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this fiscal period query. 
     *
     *  @param fiscalPeriodQueryInspectorRecord fiscal period query inspector
     *         record
     *  @param fiscalPeriodRecordType fiscalPeriod record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFiscalPeriodQueryInspectorRecord(org.osid.financials.records.FiscalPeriodQueryInspectorRecord fiscalPeriodQueryInspectorRecord, 
                                                   org.osid.type.Type fiscalPeriodRecordType) {

        addRecordType(fiscalPeriodRecordType);
        nullarg(fiscalPeriodRecordType, "fiscal period record type");
        this.records.add(fiscalPeriodQueryInspectorRecord);        
        return;
    }
}
