//
// AbstractJobConstrainerEnablerQuery.java
//
//     A template for making a JobConstrainerEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for job constrainer enablers.
 */

public abstract class AbstractJobConstrainerEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.resourcing.rules.JobConstrainerEnablerQuery {

    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the job constrainer. 
     *
     *  @param  jobConstrainerId the job constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> jobConstrainerId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledJobConstrainerId(org.osid.id.Id jobConstrainerId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the job constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledJobConstrainerIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> JobConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a job constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledJobConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a job constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the job constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledJobConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobConstrainerQuery getRuledJobConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledJobConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any job constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any job 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no job constrainers 
     */

    @OSID @Override
    public void matchAnyRuledJobConstrainer(boolean match) {
        return;
    }


    /**
     *  Clears the job constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledJobConstrainerTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given job constrainer enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a job constrainer enabler implementing the requested record.
     *
     *  @param jobConstrainerEnablerRecordType a job constrainer enabler record type
     *  @return the job constrainer enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord getJobConstrainerEnablerQueryRecord(org.osid.type.Type jobConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(jobConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job constrainer enabler query. 
     *
     *  @param jobConstrainerEnablerQueryRecord job constrainer enabler query record
     *  @param jobConstrainerEnablerRecordType jobConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobConstrainerEnablerQueryRecord(org.osid.resourcing.rules.records.JobConstrainerEnablerQueryRecord jobConstrainerEnablerQueryRecord, 
                                          org.osid.type.Type jobConstrainerEnablerRecordType) {

        addRecordType(jobConstrainerEnablerRecordType);
        nullarg(jobConstrainerEnablerQueryRecord, "job constrainer enabler query record");
        this.records.add(jobConstrainerEnablerQueryRecord);        
        return;
    }
}
