//
// AbstractGradeSystemQuery.java
//
//     A template for making a GradeSystem Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradesystem.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for grade systems.
 */

public abstract class AbstractGradeSystemQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.grading.GradeSystemQuery {

    private final java.util.Collection<org.osid.grading.records.GradeSystemQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches grade systems based on grades. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     */

    @OSID @Override
    public void matchBasedOnGrades(boolean match) {
        return;
    }


    /**
     *  Clears the grade <code> based </code> terms. 
     */

    @OSID @Override
    public void clearBasedOnGradesTerms() {
        return;
    }


    /**
     *  Sets the grade <code> Id </code> for this query. 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeId(org.osid.id.Id gradeId, boolean match) {
        return;
    }


    /**
     *  Clears the grade <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available for querying grades. 
     *
     *  @return <code> true </code> if a grade query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade query 
     *  @throws org.osid.UnimplementedException <code> supportsGradeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getGradeQuery() {
        throw new org.osid.UnimplementedException("supportsGradeQuery() is false");
    }


    /**
     *  Matches grade systems with any grade. 
     *
     *  @param  match <code> true </code> to match grade systems with any 
     *          grade, <code> false </code> to match systems with no grade 
     */

    @OSID @Override
    public void matchAnyGrade(boolean match) {
        return;
    }


    /**
     *  Clears the grade terms. 
     */

    @OSID @Override
    public void clearGradeTerms() {
        return;
    }


    /**
     *  Matches grade systems whose low end score falls in the specified range 
     *  inclusive. 
     *
     *  @param  start low end of range 
     *  @param  end high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLowestNumericScore(java.math.BigDecimal start, 
                                        java.math.BigDecimal end, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the lowest numeric score terms. 
     */

    @OSID @Override
    public void clearLowestNumericScoreTerms() {
        return;
    }


    /**
     *  Matches grade systems numeric score increment is between the specified 
     *  range inclusive. 
     *
     *  @param  start low end of range 
     *  @param  end high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchNumericScoreIncrement(java.math.BigDecimal start, 
                                           java.math.BigDecimal end, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the numeric score increment terms. 
     */

    @OSID @Override
    public void clearNumericScoreIncrementTerms() {
        return;
    }


    /**
     *  Matches grade systems whose high end score falls in the specified 
     *  range inclusive. 
     *
     *  @param  start low end of range 
     *  @param  end high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchHighestNumericScore(java.math.BigDecimal start, 
                                         java.math.BigDecimal end, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the highest numeric score terms. 
     */

    @OSID @Override
    public void clearHighestNumericScoreTerms() {
        return;
    }


    /**
     *  Sets the gradebook column <code> Id </code> for this query. 
     *
     *  @param  gradebookColumnId a gradebook column <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookColumnId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookColumnId(org.osid.id.Id gradebookColumnId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the gradebook column <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookColumnIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookColumnQuery </code> is available. 
     *
     *  @return <code> true </code> if a gradebook column query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook column. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the gradebook column query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnQuery getGradebookColumnQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookColumnQuery() is false");
    }


    /**
     *  Matches grade systems assigned to any gradebook column. 
     *
     *  @param  match <code> true </code> to match grade systems mapped to any 
     *          column, <code> false </code> to match systems mapped to no 
     *          columns 
     */

    @OSID @Override
    public void matchAnyGradebookColumn(boolean match) {
        return;
    }


    /**
     *  Clears the gradebook column terms. 
     */

    @OSID @Override
    public void clearGradebookColumnTerms() {
        return;
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available . 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given grade system query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a grade system implementing the requested record.
     *
     *  @param gradeSystemRecordType a grade system record type
     *  @return the grade system query record
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeSystemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemQueryRecord getGradeSystemQueryRecord(org.osid.type.Type gradeSystemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSystemQueryRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade system query. 
     *
     *  @param gradeSystemQueryRecord grade system query record
     *  @param gradeSystemRecordType gradeSystem record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradeSystemQueryRecord(org.osid.grading.records.GradeSystemQueryRecord gradeSystemQueryRecord, 
                                          org.osid.type.Type gradeSystemRecordType) {

        addRecordType(gradeSystemRecordType);
        nullarg(gradeSystemQueryRecord, "grade system query record");
        this.records.add(gradeSystemQueryRecord);        
        return;
    }
}
