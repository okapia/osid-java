//
// AbstractSyllabusSearchOdrer.java
//
//     Defines a SyllabusSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.syllabus.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code SyllabusSearchOrder}.
 */

public abstract class AbstractSyllabusSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.course.syllabus.SyllabusSearchOrder {

    private final java.util.Collection<org.osid.course.syllabus.records.SyllabusSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the course. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourse(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a course order is available. 
     *
     *  @return <code> true </code> if a course order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course order. 
     *
     *  @return the course search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchOrder getCourseSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  syllabusRecordType a syllabus record type 
     *  @return {@code true} if the syllabusRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code syllabusRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type syllabusRecordType) {
        for (org.osid.course.syllabus.records.SyllabusSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  syllabusRecordType the syllabus record type 
     *  @return the syllabus search order record
     *  @throws org.osid.NullArgumentException
     *          {@code syllabusRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(syllabusRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusSearchOrderRecord getSyllabusSearchOrderRecord(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.syllabus.records.SyllabusSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(syllabusRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(syllabusRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this syllabus. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param syllabusRecord the syllabus search odrer record
     *  @param syllabusRecordType syllabus record type
     *  @throws org.osid.NullArgumentException
     *          {@code syllabusRecord} or
     *          {@code syllabusRecordTypesyllabus} is
     *          {@code null}
     */
            
    protected void addSyllabusRecord(org.osid.course.syllabus.records.SyllabusSearchOrderRecord syllabusSearchOrderRecord, 
                                     org.osid.type.Type syllabusRecordType) {

        addRecordType(syllabusRecordType);
        this.records.add(syllabusSearchOrderRecord);
        
        return;
    }
}
