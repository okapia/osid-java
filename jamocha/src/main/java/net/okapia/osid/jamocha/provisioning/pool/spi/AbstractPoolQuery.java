//
// AbstractPoolQuery.java
//
//     A template for making a Pool Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.pool.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for pools.
 */

public abstract class AbstractPoolQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.provisioning.PoolQuery {

    private final java.util.Collection<org.osid.provisioning.records.PoolQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        return;
    }


    /**
     *  Sets the provisionable <code> Id </code> for this query. 
     *
     *  @param  provisionableId the provisionable <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> provisionableId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchProvisionableId(org.osid.id.Id provisionableId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the provisionable <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProvisionableIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ProvisionableQuery </code> is available. 
     *
     *  @return <code> true </code> if a provisionable query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> PoolQntry. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the provisionable query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableQuery getProvisionableQuery() {
        throw new org.osid.UnimplementedException("supportsProvisionableQuery() is false");
    }


    /**
     *  Matches pools that have any provisionable. 
     *
     *  @param  match <code> true </code> to match pools with any 
     *          provisionable, <code> false </code> to match pools with no 
     *          provisionable 
     */

    @OSID @Override
    public void matchAnyProvisionable(boolean match) {
        return;
    }


    /**
     *  Clears the provisionable query terms. 
     */

    @OSID @Override
    public void clearProvisionableTerms() {
        return;
    }


    /**
     *  Matches pools of the given size inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchSize(long start, long end, boolean match) {
        return;
    }


    /**
     *  Matches pools with any known size. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySize(boolean match) {
        return;
    }


    /**
     *  Clears the size query terms. 
     */

    @OSID @Override
    public void clearSizeTerms() {
        return;
    }


    /**
     *  Sets the broker <code> Id </code> for this query to match queues 
     *  assigned to brokers. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given pool query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a pool implementing the requested record.
     *
     *  @param poolRecordType a pool record type
     *  @return the pool query record
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.PoolQueryRecord getPoolQueryRecord(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.PoolQueryRecord record : this.records) {
            if (record.implementsRecordType(poolRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool query. 
     *
     *  @param poolQueryRecord pool query record
     *  @param poolRecordType pool record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolQueryRecord(org.osid.provisioning.records.PoolQueryRecord poolQueryRecord, 
                                          org.osid.type.Type poolRecordType) {

        addRecordType(poolRecordType);
        nullarg(poolQueryRecord, "pool query record");
        this.records.add(poolQueryRecord);        
        return;
    }
}
