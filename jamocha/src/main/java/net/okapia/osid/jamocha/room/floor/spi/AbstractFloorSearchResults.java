//
// AbstractFloorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.floor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractFloorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.room.FloorSearchResults {

    private org.osid.room.FloorList floors;
    private final org.osid.room.FloorQueryInspector inspector;
    private final java.util.Collection<org.osid.room.records.FloorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractFloorSearchResults.
     *
     *  @param floors the result set
     *  @param floorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>floors</code>
     *          or <code>floorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractFloorSearchResults(org.osid.room.FloorList floors,
                                            org.osid.room.FloorQueryInspector floorQueryInspector) {
        nullarg(floors, "floors");
        nullarg(floorQueryInspector, "floor query inspectpr");

        this.floors = floors;
        this.inspector = floorQueryInspector;

        return;
    }


    /**
     *  Gets the floor list resulting from a search.
     *
     *  @return a floor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.room.FloorList getFloors() {
        if (this.floors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.room.FloorList floors = this.floors;
        this.floors = null;
	return (floors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.room.FloorQueryInspector getFloorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  floor search record <code> Type. </code> This method must
     *  be used to retrieve a floor implementing the requested
     *  record.
     *
     *  @param floorSearchRecordType a floor search 
     *         record type 
     *  @return the floor search
     *  @throws org.osid.NullArgumentException
     *          <code>floorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(floorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.FloorSearchResultsRecord getFloorSearchResultsRecord(org.osid.type.Type floorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.room.records.FloorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(floorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(floorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record floor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addFloorRecord(org.osid.room.records.FloorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "floor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
