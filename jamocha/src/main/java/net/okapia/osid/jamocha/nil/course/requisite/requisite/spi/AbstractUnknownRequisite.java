//
// AbstractUnknownRequisite.java
//
//     Defines an unknown Requisite.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.course.requisite.requisite.spi;


/**
 *  Defines an unknown <code>Requisite</code>.
 */

public abstract class AbstractUnknownRequisite
    extends net.okapia.osid.jamocha.course.requisite.requisite.spi.AbstractRequisite
    implements org.osid.course.requisite.Requisite {

    protected static final String OBJECT = "osid.course.requisite.Requisite";


    /**
     *  Constructs a new <code>AbstractUnknownRequisite</code>.
     */

    public AbstractUnknownRequisite() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownRequisite</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownRequisite(boolean optional) {
        this();

        setRule(new net.okapia.osid.jamocha.nil.rules.rule.UnknownRule());
        addRequisiteOption(new net.okapia.osid.jamocha.nil.course.requisite.requisite.UnknownRequisite());
        addCourseRequirement(new net.okapia.osid.jamocha.nil.course.requisite.courserequirement.UnknownCourseRequirement());
        addProgramRequirement(new net.okapia.osid.jamocha.nil.course.requisite.programrequirement.UnknownProgramRequirement());
        addCredentialRequirement(new net.okapia.osid.jamocha.nil.course.requisite.credentialrequirement.UnknownCredentialRequirement());
        addLearningObjectiveRequirement(new net.okapia.osid.jamocha.nil.course.requisite.learningobjectiverequirement.UnknownLearningObjectiveRequirement());
        addAssessmentRequirement(new net.okapia.osid.jamocha.nil.course.requisite.assessmentrequirement.UnknownAssessmentRequirement());
        addAwardRequirement(new net.okapia.osid.jamocha.nil.course.requisite.awardrequirement.UnknownAwardRequirement());

        return;
    }
}
