//
// AbstractMapConfigurationLookupSession
//
//    A simple framework for providing a Configuration lookup service
//    backed by a fixed collection of configurations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Configuration lookup service backed by a
 *  fixed collection of configurations. The configurations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Configurations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapConfigurationLookupSession
    extends net.okapia.osid.jamocha.configuration.spi.AbstractConfigurationLookupSession
    implements org.osid.configuration.ConfigurationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.configuration.Configuration> configurations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.configuration.Configuration>());


    /**
     *  Makes a <code>Configuration</code> available in this session.
     *
     *  @param  configuration a configuration
     *  @throws org.osid.NullArgumentException <code>configuration<code>
     *          is <code>null</code>
     */

    protected void putConfiguration(org.osid.configuration.Configuration configuration) {
        this.configurations.put(configuration.getId(), configuration);
        return;
    }


    /**
     *  Makes an array of configurations available in this session.
     *
     *  @param  configurations an array of configurations
     *  @throws org.osid.NullArgumentException <code>configurations<code>
     *          is <code>null</code>
     */

    protected void putConfigurations(org.osid.configuration.Configuration[] configurations) {
        putConfigurations(java.util.Arrays.asList(configurations));
        return;
    }


    /**
     *  Makes a collection of configurations available in this session.
     *
     *  @param  configurations a collection of configurations
     *  @throws org.osid.NullArgumentException <code>configurations<code>
     *          is <code>null</code>
     */

    protected void putConfigurations(java.util.Collection<? extends org.osid.configuration.Configuration> configurations) {
        for (org.osid.configuration.Configuration configuration : configurations) {
            this.configurations.put(configuration.getId(), configuration);
        }

        return;
    }


    /**
     *  Removes a Configuration from this session.
     *
     *  @param  configurationId the <code>Id</code> of the configuration
     *  @throws org.osid.NullArgumentException <code>configurationId<code> is
     *          <code>null</code>
     */

    protected void removeConfiguration(org.osid.id.Id configurationId) {
        this.configurations.remove(configurationId);
        return;
    }


    /**
     *  Gets the <code>Configuration</code> specified by its <code>Id</code>.
     *
     *  @param  configurationId <code>Id</code> of the <code>Configuration</code>
     *  @return the configuration
     *  @throws org.osid.NotFoundException <code>configurationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>configurationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration(org.osid.id.Id configurationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.configuration.Configuration configuration = this.configurations.get(configurationId);
        if (configuration == null) {
            throw new org.osid.NotFoundException("configuration not found: " + configurationId);
        }

        return (configuration);
    }


    /**
     *  Gets all <code>Configurations</code>. In plenary mode, the returned
     *  list contains all known configurations or an error
     *  results. Otherwise, the returned list may contain only those
     *  configurations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Configurations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationList getConfigurations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.configuration.ArrayConfigurationList(this.configurations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.configurations.clear();
        super.close();
        return;
    }
}
