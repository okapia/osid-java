//
// AbstractResourceRelationshipQuery.java
//
//     A template for making a ResourceRelationship Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resourcerelationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for resource relationships.
 */

public abstract class AbstractResourceRelationshipQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.resource.ResourceRelationshipQuery {

    private final java.util.Collection<org.osid.resource.records.ResourceRelationshipQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSourceResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceResourceQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSourceResourceQuery() {
        throw new org.osid.UnimplementedException("supportsSourceResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearSourceResourceTerms() {
        return;
    }


    /**
     *  Sets the peer resource <code> Id </code> for this query. 
     *
     *  @param  peerResourceId a peer resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> peerResourceId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchDestinationResourceId(org.osid.id.Id peerResourceId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the peer resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDestinationResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDestinationResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDestinationResourceQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getDestinationResourceQuery() {
        throw new org.osid.UnimplementedException("supportsDestinationResourceQuery() is false");
    }


    /**
     *  Clears the peer resource terms. 
     */

    @OSID @Override
    public void clearDestinationResourceTerms() {
        return;
    }


    /**
     *  Matches relationships where the peer resources are the same. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSameResource(boolean match) {
        return;
    }


    /**
     *  Clears the same resource terms. 
     */

    @OSID @Override
    public void clearSameResourceTerms() {
        return;
    }


    /**
     *  Sets the bin <code> Id </code> for this query to match terms assigned 
     *  to bins. 
     *
     *  @param  binId the bin <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBinId(org.osid.id.Id binId, boolean match) {
        return;
    }


    /**
     *  Clears the bin <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBinIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BinQuery </code> is available. 
     *
     *  @return <code> true </code> if a bin query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinQuery() {
        return (false);
    }


    /**
     *  Gets the query for a bin. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the bin query 
     *  @throws org.osid.UnimplementedException <code> supportsBinQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.BinQuery getBinQuery() {
        throw new org.osid.UnimplementedException("supportsBinQuery() is false");
    }


    /**
     *  Clears the bin terms. 
     */

    @OSID @Override
    public void clearBinTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given resource relationship query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a resource relationship implementing the requested record.
     *
     *  @param resourceRelationshipRecordType a resource relationship record type
     *  @return the resource relationship query record
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRelationshipRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipQueryRecord getResourceRelationshipQueryRecord(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceRelationshipQueryRecord record : this.records) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRelationshipRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource relationship query. 
     *
     *  @param resourceRelationshipQueryRecord resource relationship query record
     *  @param resourceRelationshipRecordType resourceRelationship record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addResourceRelationshipQueryRecord(org.osid.resource.records.ResourceRelationshipQueryRecord resourceRelationshipQueryRecord, 
                                          org.osid.type.Type resourceRelationshipRecordType) {

        addRecordType(resourceRelationshipRecordType);
        nullarg(resourceRelationshipQueryRecord, "resource relationship query record");
        this.records.add(resourceRelationshipQueryRecord);        
        return;
    }
}
