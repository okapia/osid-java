//
// AbstractAdapterTemporal.java
//
//     Defines a Temporal wrapper.
//
//
// Tom Coppeto
// Okapia
// 20 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a Temporal wrapper.
 */

public abstract class AbstractAdapterTemporal
    implements org.osid.Temporal {

    private final org.osid.Temporal temporal;


    /**
     *  Creates a new <code>AbstractAdapterTemporal</code>.
     *
     *  @param temporal the <code>Temporal</code> for this object
     *  @throws org.osid.NullArgumentException <code>temporal</code>
     *          is <code>null</code>
     */

    protected AbstractAdapterTemporal(org.osid.Temporal temporal) {
        this.temporal = temporal;
        return;
    }

    
    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.temporal.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.temporal.getStartDate());
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.temporal.getEndDate());
    }
}
