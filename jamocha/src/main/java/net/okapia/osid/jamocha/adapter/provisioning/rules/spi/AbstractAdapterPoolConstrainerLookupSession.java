//
// AbstractAdapterPoolConstrainerLookupSession.java
//
//    A PoolConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A PoolConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterPoolConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {

    private final org.osid.provisioning.rules.PoolConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPoolConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPoolConstrainerLookupSession(org.osid.provisioning.rules.PoolConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Distributor/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Distributor Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the {@code Distributor} associated with this session.
     *
     *  @return the {@code Distributor} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform {@code PoolConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPoolConstrainers() {
        return (this.session.canLookupPoolConstrainers());
    }


    /**
     *  A complete view of the {@code PoolConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolConstrainerView() {
        this.session.useComparativePoolConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code PoolConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolConstrainerView() {
        this.session.usePlenaryPoolConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool constrainers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active pool constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePoolConstrainerView() {
        this.session.useActivePoolConstrainerView();
        return;
    }


    /**
     *  Active and inactive pool constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolConstrainerView() {
        this.session.useAnyStatusPoolConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code PoolConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code PoolConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code PoolConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @param poolConstrainerId {@code Id} of the {@code PoolConstrainer}
     *  @return the pool constrainer
     *  @throws org.osid.NotFoundException {@code poolConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code poolConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainer getPoolConstrainer(org.osid.id.Id poolConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainer(poolConstrainerId));
    }


    /**
     *  Gets a {@code PoolConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  poolConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code PoolConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @param  poolConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code PoolConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code poolConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByIds(org.osid.id.IdList poolConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainersByIds(poolConstrainerIds));
    }


    /**
     *  Gets a {@code PoolConstrainerList} corresponding to the given
     *  pool constrainer genus {@code Type} which does not include
     *  pool constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @param  poolConstrainerGenusType a poolConstrainer genus type 
     *  @return the returned {@code PoolConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainersByGenusType(poolConstrainerGenusType));
    }


    /**
     *  Gets a {@code PoolConstrainerList} corresponding to the given
     *  pool constrainer genus {@code Type} and include any additional
     *  pool constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @param  poolConstrainerGenusType a poolConstrainer genus type 
     *  @return the returned {@code PoolConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByParentGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainersByParentGenusType(poolConstrainerGenusType));
    }


    /**
     *  Gets a {@code PoolConstrainerList} containing the given
     *  pool constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  pool constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @param  poolConstrainerRecordType a poolConstrainer record type 
     *  @return the returned {@code PoolConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code poolConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByRecordType(org.osid.type.Type poolConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainersByRecordType(poolConstrainerRecordType));
    }


    /**
     *  Gets all {@code PoolConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @return a list of {@code PoolConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPoolConstrainers());
    }
}
