//
// InvariantIndexedMapInquiryEnablerLookupSession
//
//    Implements an InquiryEnabler lookup service backed by a fixed
//    collection of inquiryEnablers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules;


/**
 *  Implements an InquiryEnabler lookup service backed by a fixed
 *  collection of inquiry enablers. The inquiry enablers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some inquiry enablers may be compatible
 *  with more types than are indicated through these inquiry enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapInquiryEnablerLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.rules.spi.AbstractIndexedMapInquiryEnablerLookupSession
    implements org.osid.inquiry.rules.InquiryEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapInquiryEnablerLookupSession} using an
     *  array of inquiryEnablers.
     *
     *  @param inquest the inquest
     *  @param inquiryEnablers an array of inquiry enablers
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiryEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                    org.osid.inquiry.rules.InquiryEnabler[] inquiryEnablers) {

        setInquest(inquest);
        putInquiryEnablers(inquiryEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapInquiryEnablerLookupSession} using a
     *  collection of inquiry enablers.
     *
     *  @param inquest the inquest
     *  @param inquiryEnablers a collection of inquiry enablers
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiryEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapInquiryEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                    java.util.Collection<? extends org.osid.inquiry.rules.InquiryEnabler> inquiryEnablers) {

        setInquest(inquest);
        putInquiryEnablers(inquiryEnablers);
        return;
    }
}
