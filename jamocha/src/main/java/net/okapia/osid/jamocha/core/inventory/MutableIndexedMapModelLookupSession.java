//
// MutableIndexedMapModelLookupSession
//
//    Implements a Model lookup service backed by a collection of
//    models indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory;


/**
 *  Implements a Model lookup service backed by a collection of
 *  models. The models are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some models may be compatible
 *  with more types than are indicated through these model
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of models can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapModelLookupSession
    extends net.okapia.osid.jamocha.core.inventory.spi.AbstractIndexedMapModelLookupSession
    implements org.osid.inventory.ModelLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapModelLookupSession} with no models.
     *
     *  @param warehouse the warehouse
     *  @throws org.osid.NullArgumentException {@code warehouse}
     *          is {@code null}
     */

      public MutableIndexedMapModelLookupSession(org.osid.inventory.Warehouse warehouse) {
        setWarehouse(warehouse);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapModelLookupSession} with a
     *  single model.
     *  
     *  @param warehouse the warehouse
     *  @param  model a single model
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code model} is {@code null}
     */

    public MutableIndexedMapModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.inventory.Model model) {
        this(warehouse);
        putModel(model);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapModelLookupSession} using an
     *  array of models.
     *
     *  @param warehouse the warehouse
     *  @param  models an array of models
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code models} is {@code null}
     */

    public MutableIndexedMapModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  org.osid.inventory.Model[] models) {
        this(warehouse);
        putModels(models);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapModelLookupSession} using a
     *  collection of models.
     *
     *  @param warehouse the warehouse
     *  @param  models a collection of models
     *  @throws org.osid.NullArgumentException {@code warehouse} or
     *          {@code models} is {@code null}
     */

    public MutableIndexedMapModelLookupSession(org.osid.inventory.Warehouse warehouse,
                                                  java.util.Collection<? extends org.osid.inventory.Model> models) {

        this(warehouse);
        putModels(models);
        return;
    }
    

    /**
     *  Makes a {@code Model} available in this session.
     *
     *  @param  model a model
     *  @throws org.osid.NullArgumentException {@code model{@code  is
     *          {@code null}
     */

    @Override
    public void putModel(org.osid.inventory.Model model) {
        super.putModel(model);
        return;
    }


    /**
     *  Makes an array of models available in this session.
     *
     *  @param  models an array of models
     *  @throws org.osid.NullArgumentException {@code models{@code 
     *          is {@code null}
     */

    @Override
    public void putModels(org.osid.inventory.Model[] models) {
        super.putModels(models);
        return;
    }


    /**
     *  Makes collection of models available in this session.
     *
     *  @param  models a collection of models
     *  @throws org.osid.NullArgumentException {@code model{@code  is
     *          {@code null}
     */

    @Override
    public void putModels(java.util.Collection<? extends org.osid.inventory.Model> models) {
        super.putModels(models);
        return;
    }


    /**
     *  Removes a Model from this session.
     *
     *  @param modelId the {@code Id} of the model
     *  @throws org.osid.NullArgumentException {@code modelId{@code  is
     *          {@code null}
     */

    @Override
    public void removeModel(org.osid.id.Id modelId) {
        super.removeModel(modelId);
        return;
    }    
}
