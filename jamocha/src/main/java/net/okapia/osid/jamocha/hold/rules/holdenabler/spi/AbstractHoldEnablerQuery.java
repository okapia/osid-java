//
// AbstractHoldEnablerQuery.java
//
//     A template for making a HoldEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.rules.holdenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for hold enablers.
 */

public abstract class AbstractHoldEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.hold.rules.HoldEnablerQuery {

    private final java.util.Collection<org.osid.hold.rules.records.HoldEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the hold. 
     *
     *  @param  holdId the hold <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> holdId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRuledHoldId(org.osid.id.Id holdId, boolean match) {
        return;
    }


    /**
     *  Clears the hold <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledHoldIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> HoldQuery </code> is available. 
     *
     *  @return <code> true </code> if a hold query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledHoldQuery() {
        return (false);
    }


    /**
     *  Gets the query for a hold. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the hold query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledHoldQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuery getRuledHoldQuery() {
        throw new org.osid.UnimplementedException("supportsRuledHoldQuery() is false");
    }


    /**
     *  Matches enablers mapped to any hold. 
     *
     *  @param  match <code> true </code> for enablers mapped to any hold, 
     *          <code> false </code> to match enablers mapped to no hold 
     */

    @OSID @Override
    public void matchAnyRuledHold(boolean match) {
        return;
    }


    /**
     *  Clears the hold query terms. 
     */

    @OSID @Override
    public void clearRuledHoldTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the oubliette. 
     *
     *  @param  oublietteId the oubliette <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOublietteId(org.osid.id.Id oublietteId, boolean match) {
        return;
    }


    /**
     *  Clears the oubliette <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOublietteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OublietteQuery </code> is available. 
     *
     *  @return <code> true </code> if an oubliette query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (false);
    }


    /**
     *  Gets the query for an oubliette. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the oubliette query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuery getOublietteQuery() {
        throw new org.osid.UnimplementedException("supportsOublietteQuery() is false");
    }


    /**
     *  Clears the oubliette query terms. 
     */

    @OSID @Override
    public void clearOublietteTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given hold enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a hold enabler implementing the requested record.
     *
     *  @param holdEnablerRecordType a hold enabler record type
     *  @return the hold enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(holdEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.rules.records.HoldEnablerQueryRecord getHoldEnablerQueryRecord(org.osid.type.Type holdEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.hold.rules.records.HoldEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(holdEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(holdEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this hold enabler query. 
     *
     *  @param holdEnablerQueryRecord hold enabler query record
     *  @param holdEnablerRecordType holdEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addHoldEnablerQueryRecord(org.osid.hold.rules.records.HoldEnablerQueryRecord holdEnablerQueryRecord, 
                                          org.osid.type.Type holdEnablerRecordType) {

        addRecordType(holdEnablerRecordType);
        nullarg(holdEnablerQueryRecord, "hold enabler query record");
        this.records.add(holdEnablerQueryRecord);        
        return;
    }
}
