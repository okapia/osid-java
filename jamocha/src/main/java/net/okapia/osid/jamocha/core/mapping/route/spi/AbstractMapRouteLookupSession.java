//
// AbstractMapRouteLookupSession
//
//    A simple framework for providing a Route lookup service
//    backed by a fixed collection of routes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.route.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Route lookup service backed by a
 *  fixed collection of routes. The routes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Routes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRouteLookupSession
    extends net.okapia.osid.jamocha.mapping.route.spi.AbstractRouteLookupSession
    implements org.osid.mapping.route.RouteLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.mapping.route.Route> routes = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.mapping.route.Route>());


    /**
     *  Makes a <code>Route</code> available in this session.
     *
     *  @param  route a route
     *  @throws org.osid.NullArgumentException <code>route<code>
     *          is <code>null</code>
     */

    protected void putRoute(org.osid.mapping.route.Route route) {
        this.routes.put(route.getId(), route);
        return;
    }


    /**
     *  Makes an array of routes available in this session.
     *
     *  @param  routes an array of routes
     *  @throws org.osid.NullArgumentException <code>routes<code>
     *          is <code>null</code>
     */

    protected void putRoutes(org.osid.mapping.route.Route[] routes) {
        putRoutes(java.util.Arrays.asList(routes));
        return;
    }


    /**
     *  Makes a collection of routes available in this session.
     *
     *  @param  routes a collection of routes
     *  @throws org.osid.NullArgumentException <code>routes<code>
     *          is <code>null</code>
     */

    protected void putRoutes(java.util.Collection<? extends org.osid.mapping.route.Route> routes) {
        for (org.osid.mapping.route.Route route : routes) {
            this.routes.put(route.getId(), route);
        }

        return;
    }


    /**
     *  Removes a Route from this session.
     *
     *  @param  routeId the <code>Id</code> of the route
     *  @throws org.osid.NullArgumentException <code>routeId<code> is
     *          <code>null</code>
     */

    protected void removeRoute(org.osid.id.Id routeId) {
        this.routes.remove(routeId);
        return;
    }


    /**
     *  Gets the <code>Route</code> specified by its <code>Id</code>.
     *
     *  @param  routeId <code>Id</code> of the <code>Route</code>
     *  @return the route
     *  @throws org.osid.NotFoundException <code>routeId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>routeId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.Route getRoute(org.osid.id.Id routeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.mapping.route.Route route = this.routes.get(routeId);
        if (route == null) {
            throw new org.osid.NotFoundException("route not found: " + routeId);
        }

        return (route);
    }


    /**
     *  Gets all <code>Routes</code>. In plenary mode, the returned
     *  list contains all known routes or an error
     *  results. Otherwise, the returned list may contain only those
     *  routes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Routes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteList getRoutes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.route.route.ArrayRouteList(this.routes.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.routes.clear();
        super.close();
        return;
    }
}
