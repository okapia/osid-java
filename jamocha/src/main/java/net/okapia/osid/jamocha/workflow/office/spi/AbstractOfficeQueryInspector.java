//
// AbstractOfficeQueryInspector.java
//
//     A template for making an OfficeQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.office.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for offices.
 */

public abstract class AbstractOfficeQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.workflow.OfficeQueryInspector {

    private final java.util.Collection<org.osid.workflow.records.OfficeQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the process <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the process query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQueryInspector[] getProcessTerms() {
        return (new org.osid.workflow.ProcessQueryInspector[0]);
    }


    /**
     *  Gets the step <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStepIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the step query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.StepQueryInspector[] getStepTerms() {
        return (new org.osid.workflow.StepQueryInspector[0]);
    }


    /**
     *  Gets the work <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWorkIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the work query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.WorkQueryInspector[] getWorkTerms() {
        return (new org.osid.workflow.WorkQueryInspector[0]);
    }


    /**
     *  Gets the ancestor office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getAncestorOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the descendant office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getDescendantOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given office query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an office implementing the requested record.
     *
     *  @param officeRecordType an office record type
     *  @return the office query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>officeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(officeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.OfficeQueryInspectorRecord getOfficeQueryInspectorRecord(org.osid.type.Type officeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.records.OfficeQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(officeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(officeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this office query. 
     *
     *  @param officeQueryInspectorRecord office query inspector
     *         record
     *  @param officeRecordType office record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOfficeQueryInspectorRecord(org.osid.workflow.records.OfficeQueryInspectorRecord officeQueryInspectorRecord, 
                                                   org.osid.type.Type officeRecordType) {

        addRecordType(officeRecordType);
        nullarg(officeRecordType, "office record type");
        this.records.add(officeQueryInspectorRecord);        
        return;
    }
}
