//
// AbstractAssemblyScheduleSlotQuery.java
//
//     A ScheduleSlotQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ScheduleSlotQuery that stores terms.
 */

public abstract class AbstractAssemblyScheduleSlotQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyContainableOsidObjectQuery
    implements org.osid.calendaring.ScheduleSlotQuery,
               org.osid.calendaring.ScheduleSlotQueryInspector,
               org.osid.calendaring.ScheduleSlotSearchOrder {

    private final java.util.Collection<org.osid.calendaring.records.ScheduleSlotQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.ScheduleSlotQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.ScheduleSlotSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyScheduleSlotQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyScheduleSlotQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the schedule <code> Id </code> for this query for matching nested 
     *  schedule slots. 
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleSlotId(org.osid.id.Id scheduleSlotId, 
                                    boolean match) {
        getAssembler().addIdTerm(getScheduleSlotIdColumn(), scheduleSlotId, match);
        return;
    }


    /**
     *  Clears the schedule slot <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleSlotIdTerms() {
        getAssembler().clearTerms(getScheduleSlotIdColumn());
        return;
    }


    /**
     *  Gets the schedule slot <code> Id </code> terms. 
     *
     *  @return the schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getScheduleSlotIdTerms() {
        return (getAssembler().getIdTerms(getScheduleSlotIdColumn()));
    }


    /**
     *  Gets the ScheduleSlotId column name.
     *
     * @return the column name
     */

    protected String getScheduleSlotIdColumn() {
        return ("schedule_slot_id");
    }


    /**
     *  Tests if a <code> ScheduleSlotQuery </code> is available for querying 
     *  sechedule slots. 
     *
     *  @return <code> true </code> if a schedule slot query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a schedul slot. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the schedule slot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuery getScheduleSlotQuery() {
        throw new org.osid.UnimplementedException("supportsScheduleSlotQuery() is false");
    }


    /**
     *  Matches a schedule that has any schedule slot assigned. 
     *
     *  @param  match <code> true </code> to match schedule with any schedule 
     *          slots, <code> false </code> to match schedules with no 
     *          schedule slots 
     */

    @OSID @Override
    public void matchAnyScheduleSlot(boolean match) {
        getAssembler().addIdWildcardTerm(getScheduleSlotColumn(), match);
        return;
    }


    /**
     *  Clears the schedule slot terms. 
     */

    @OSID @Override
    public void clearScheduleSlotTerms() {
        getAssembler().clearTerms(getScheduleSlotColumn());
        return;
    }


    /**
     *  Gets the schedule slot terms. 
     *
     *  @return the schedule slot terms 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQueryInspector[] getScheduleSlotTerms() {
        return (new org.osid.calendaring.ScheduleSlotQueryInspector[0]);
    }


    /**
     *  Gets the ScheduleSlot column name.
     *
     * @return the column name
     */

    protected String getScheduleSlotColumn() {
        return ("schedule_slot");
    }


    /**
     *  Matches schedules that have the given weekday. 
     *
     *  @param  weekday a weekday 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchWeekday(long weekday, boolean match) {
        getAssembler().addCardinalTerm(getWeekdayColumn(), weekday, match);
        return;
    }


    /**
     *  Matches schedules with any weekday set. 
     *
     *  @param  match <code> true </code> to match schedules with any weekday, 
     *          <code> false </code> to match schedules with no weekday 
     */

    @OSID @Override
    public void matchAnyWeekday(boolean match) {
        getAssembler().addCardinalWildcardTerm(getWeekdayColumn(), match);
        return;
    }


    /**
     *  Clears the weekday terms. 
     */

    @OSID @Override
    public void clearWeekdayTerms() {
        getAssembler().clearTerms(getWeekdayColumn());
        return;
    }


    /**
     *  Gets the weekday terms. 
     *
     *  @return the weekday terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalTerm[] getWeekdayTerms() {
        return (getAssembler().getCardinalTerms(getWeekdayColumn()));
    }


    /**
     *  Gets the Weekday column name.
     *
     * @return the column name
     */

    protected String getWeekdayColumn() {
        return ("weekday");
    }


    /**
     *  Matches schedules that have the given weekly interval in the given 
     *  range inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchWeeklyInterval(long from, long to, boolean match) {
        getAssembler().addIntegerRangeTerm(getWeeklyIntervalColumn(), from, to, match);
        return;
    }


    /**
     *  Matches schedules with any weekly interval set. 
     *
     *  @param  match <code> true </code> to match schedules with any weekly 
     *          interval, <code> false </code> to match schedules with no 
     *          weekly interval 
     */

    @OSID @Override
    public void matchAnyWeeklyInterval(boolean match) {
        getAssembler().addIntegerRangeWildcardTerm(getWeeklyIntervalColumn(), match);
        return;
    }


    /**
     *  Clears the weekly interval terms. 
     */

    @OSID @Override
    public void clearWeeklyIntervalTerms() {
        getAssembler().clearTerms(getWeeklyIntervalColumn());
        return;
    }


    /**
     *  Gets the weekly interval terms. 
     *
     *  @return the weekly interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerTerm[] getWeeklyIntervalTerms() {
        return (getAssembler().getIntegerTerms(getWeeklyIntervalColumn()));
    }


    /**
     *  Specified a preference for ordering results by the weekly interval. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeeklyInterval(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeeklyIntervalColumn(), style);
        return;
    }


    /**
     *  Gets the WeeklyInterval column name.
     *
     * @return the column name
     */

    protected String getWeeklyIntervalColumn() {
        return ("weekly_interval");
    }


    /**
     *  Matches schedules that have a week of month in the given range 
     *  inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchWeekOfMonth(long from, long to, boolean match) {
        getAssembler().addIntegerRangeTerm(getWeekOfMonthColumn(), from, to, match);
        return;
    }


    /**
     *  Matches schedules with any month week set. 
     *
     *  @param  match <code> true </code> to match schedules with any week of 
     *          month, <code> false </code> to match schedules with no month 
     *          week 
     */

    @OSID @Override
    public void matchAnyWeekOfMonth(boolean match) {
        getAssembler().addIntegerRangeWildcardTerm(getWeekOfMonthColumn(), match);
        return;
    }


    /**
     *  Clears the week of month terms. 
     */

    @OSID @Override
    public void clearWeekOfMonthTerms() {
        getAssembler().clearTerms(getWeekOfMonthColumn());
        return;
    }


    /**
     *  Gets the week of month terms. 
     *
     *  @return the week of month terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerTerm[] getWeekOfMonthTerms() {
        return (getAssembler().getIntegerTerms(getWeekOfMonthColumn()));
    }


    /**
     *  Specified a preference for ordering results by the week of the month. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeekOfMonth(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeekOfMonthColumn(), style);
        return;
    }


    /**
     *  Gets the WeekOfMonth column name.
     *
     * @return the column name
     */

    protected String getWeekOfMonthColumn() {
        return ("week_of_month");
    }


    /**
     *  Matches schedules that have a weekday time in the given range 
     *  inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchWeekdayTime(org.osid.calendaring.Time from, 
                                 org.osid.calendaring.Time to, boolean match) {
        getAssembler().addTimeRangeTerm(getWeekdayTimeColumn(), from, to, match);
        return;
    }


    /**
     *  Matches schedules with any weekday time. 
     *
     *  @param  match <code> true </code> to match schedules with any weekday 
     *          time, <code> false </code> to match schedules with no weekday 
     *          time 
     */

    @OSID @Override
    public void matchAnyWeekdayTime(boolean match) {
        getAssembler().addTimeRangeWildcardTerm(getWeekdayTimeColumn(), match);
        return;
    }


    /**
     *  Clears the weekday time terms. 
     */

    @OSID @Override
    public void clearWeekdayTimeTerms() {
        getAssembler().clearTerms(getWeekdayTimeColumn());
        return;
    }


    /**
     *  Gets the weekday time terms. 
     *
     *  @return the fixed interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.TimeRangeTerm[] getWeekdayTimeTerms() {
        return (getAssembler().getTimeRangeTerms(getWeekdayTimeColumn()));
    }


    /**
     *  Specified a preference for ordering results by the weekday time. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeekdayTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeekdayTimeColumn(), style);
        return;
    }


    /**
     *  Gets the WeekdayTime column name.
     *
     * @return the column name
     */

    protected String getWeekdayTimeColumn() {
        return ("weekday_time");
    }


    /**
     *  Specified a preference for ordering results by the weekday start. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWeekdayStart(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWeekdayStartColumn(), style);
        return;
    }


    /**
     *  Gets the WeekdayStart column name.
     *
     * @return the column name
     */

    protected String getWeekdayStartColumn() {
        return ("weekday_start");
    }


    /**
     *  Matches schedules that have the given fixed interval in the given 
     *  range inclusive. 
     *
     *  @param  from start range 
     *  @param  to end range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> <code> null </code> 
     */

    @OSID @Override
    public void matchFixedInterval(org.osid.calendaring.Duration from, 
                                   org.osid.calendaring.Duration to, 
                                   boolean match) {
        getAssembler().addDurationRangeTerm(getFixedIntervalColumn(), from, to, match);
        return;
    }


    /**
     *  Matches schedules with any fixed interval. 
     *
     *  @param  match <code> true </code> to match schedules with any fixed 
     *          interval, <code> false </code> to match schedules with no 
     *          fixed interval 
     */

    @OSID @Override
    public void matchAnyFixedInterval(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getFixedIntervalColumn(), match);
        return;
    }


    /**
     *  Clears the fixed interval terms. 
     */

    @OSID @Override
    public void clearFixedIntervalTerms() {
        getAssembler().clearTerms(getFixedIntervalColumn());
        return;
    }


    /**
     *  Gets the fixed interval terms. 
     *
     *  @return the fixed interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getFixedIntervalTerms() {
        return (getAssembler().getDurationRangeTerms(getFixedIntervalColumn()));
    }


    /**
     *  Specified a preference for ordering results by the fixed interval. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFixedInterval(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFixedIntervalColumn(), style);
        return;
    }


    /**
     *  Gets the FixedInterval column name.
     *
     * @return the column name
     */

    protected String getFixedIntervalColumn() {
        return ("fixed_interval");
    }


    /**
     *  Matches the duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        getAssembler().addDurationRangeTerm(getDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a schedule slot that has any duration. 
     *
     *  @param  match <code> true </code> to match schedules with any 
     *          duration, <code> false </code> to match schedules with no 
     *          start time 
     */

    @OSID @Override
    public void matchAnyDuration(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getDurationColumn(), match);
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        getAssembler().clearTerms(getDurationColumn());
        return;
    }


    /**
     *  Gets the duration terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationTerm[] getDurationTerms() {
        return (getAssembler().getDurationTerms(getDurationColumn()));
    }


    /**
     *  Specified a preference for ordering results by the duration. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDurationColumn(), style);
        return;
    }


    /**
     *  Gets the Duration column name.
     *
     * @return the column name
     */

    protected String getDurationColumn() {
        return ("duration");
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        getAssembler().addIdTerm(getCalendarIdColumn(), calendarId, match);
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        getAssembler().clearTerms(getCalendarIdColumn());
        return;
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (getAssembler().getIdTerms(getCalendarIdColumn()));
    }


    /**
     *  Gets the CalendarId column name.
     *
     * @return the column name
     */

    protected String getCalendarIdColumn() {
        return ("calendar_id");
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        getAssembler().clearTerms(getCalendarColumn());
        return;
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }


    /**
     *  Gets the Calendar column name.
     *
     * @return the column name
     */

    protected String getCalendarColumn() {
        return ("calendar");
    }


    /**
     *  Tests if this scheduleSlot supports the given record
     *  <code>Type</code>.
     *
     *  @param  scheduleSlotRecordType a schedule slot record type 
     *  @return <code>true</code> if the scheduleSlotRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type scheduleSlotRecordType) {
        for (org.osid.calendaring.records.ScheduleSlotQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(scheduleSlotRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  scheduleSlotRecordType the schedule slot record type 
     *  @return the schedule slot query record 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleSlotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotQueryRecord getScheduleSlotQueryRecord(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleSlotQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(scheduleSlotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleSlotRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  scheduleSlotRecordType the schedule slot record type 
     *  @return the schedule slot query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleSlotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotQueryInspectorRecord getScheduleSlotQueryInspectorRecord(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleSlotQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(scheduleSlotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleSlotRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param scheduleSlotRecordType the schedule slot record type
     *  @return the schedule slot search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(scheduleSlotRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleSlotSearchOrderRecord getScheduleSlotSearchOrderRecord(org.osid.type.Type scheduleSlotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.ScheduleSlotSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(scheduleSlotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(scheduleSlotRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this schedule slot. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param scheduleSlotQueryRecord the schedule slot query record
     *  @param scheduleSlotQueryInspectorRecord the schedule slot query inspector
     *         record
     *  @param scheduleSlotSearchOrderRecord the schedule slot search order record
     *  @param scheduleSlotRecordType schedule slot record type
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotQueryRecord</code>,
     *          <code>scheduleSlotQueryInspectorRecord</code>,
     *          <code>scheduleSlotSearchOrderRecord</code> or
     *          <code>scheduleSlotRecordTypescheduleSlot</code> is
     *          <code>null</code>
     */
            
    protected void addScheduleSlotRecords(org.osid.calendaring.records.ScheduleSlotQueryRecord scheduleSlotQueryRecord, 
                                      org.osid.calendaring.records.ScheduleSlotQueryInspectorRecord scheduleSlotQueryInspectorRecord, 
                                      org.osid.calendaring.records.ScheduleSlotSearchOrderRecord scheduleSlotSearchOrderRecord, 
                                      org.osid.type.Type scheduleSlotRecordType) {

        addRecordType(scheduleSlotRecordType);

        nullarg(scheduleSlotQueryRecord, "schedule slot query record");
        nullarg(scheduleSlotQueryInspectorRecord, "schedule slot query inspector record");
        nullarg(scheduleSlotSearchOrderRecord, "schedule slot search odrer record");

        this.queryRecords.add(scheduleSlotQueryRecord);
        this.queryInspectorRecords.add(scheduleSlotQueryInspectorRecord);
        this.searchOrderRecords.add(scheduleSlotSearchOrderRecord);
        
        return;
    }
}
