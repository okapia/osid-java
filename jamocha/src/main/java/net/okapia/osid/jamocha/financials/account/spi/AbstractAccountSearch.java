//
// AbstractAccountSearch.java
//
//     A template for making an Account Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.account.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing account searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAccountSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.financials.AccountSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.financials.records.AccountSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.financials.AccountSearchOrder accountSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of accounts. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  accountIds list of accounts
     *  @throws org.osid.NullArgumentException
     *          <code>accountIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAccounts(org.osid.id.IdList accountIds) {
        while (accountIds.hasNext()) {
            try {
                this.ids.add(accountIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAccounts</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of account Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAccountIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  accountSearchOrder account search order 
     *  @throws org.osid.NullArgumentException
     *          <code>accountSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>accountSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAccountResults(org.osid.financials.AccountSearchOrder accountSearchOrder) {
	this.accountSearchOrder = accountSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.financials.AccountSearchOrder getAccountSearchOrder() {
	return (this.accountSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given account search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an account implementing the requested record.
     *
     *  @param accountSearchRecordType an account search record
     *         type
     *  @return the account search record
     *  @throws org.osid.NullArgumentException
     *          <code>accountSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(accountSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.AccountSearchRecord getAccountSearchRecord(org.osid.type.Type accountSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.financials.records.AccountSearchRecord record : this.records) {
            if (record.implementsRecordType(accountSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(accountSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this account search. 
     *
     *  @param accountSearchRecord account search record
     *  @param accountSearchRecordType account search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAccountSearchRecord(org.osid.financials.records.AccountSearchRecord accountSearchRecord, 
                                           org.osid.type.Type accountSearchRecordType) {

        addRecordType(accountSearchRecordType);
        this.records.add(accountSearchRecord);        
        return;
    }
}
