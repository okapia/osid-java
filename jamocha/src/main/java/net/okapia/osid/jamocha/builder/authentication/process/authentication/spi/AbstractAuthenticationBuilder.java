//
// AbstractAuthentication.java
//
//     Defines an Authentication builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.process.authentication.spi;


/**
 *  Defines an <code>Authentication</code> builder.
 */

public abstract class AbstractAuthenticationBuilder<T extends AbstractAuthenticationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.authentication.process.authentication.AuthenticationMiter authentication;


    /**
     *  Constructs a new <code>AbstractAuthenticationBuilder</code>.
     *
     *  @param authentication the authentication to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAuthenticationBuilder(net.okapia.osid.jamocha.builder.authentication.process.authentication.AuthenticationMiter authentication) {
        super(authentication);
        this.authentication = authentication;
        return;
    }


    /**
     *  Builds the authentication.
     *
     *  @return the new authentication
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.authentication.process.Authentication build() {
        (new net.okapia.osid.jamocha.builder.validator.authentication.process.authentication.AuthenticationValidator(getValidations())).validate(this.authentication);
        return (new net.okapia.osid.jamocha.builder.authentication.process.authentication.ImmutableAuthentication(this.authentication));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the authentication miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.authentication.process.authentication.AuthenticationMiter getMiter() {
        return (this.authentication);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent the authenticated agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setAgent(agent);
        return (self());
    }


    /**
     *  Sets the authentication as valid.
     */

    public T valid() {
        getMiter().setValid(true);
        return (self());
    }


    /**
     *  Sets the authentication as invalid.
     */

    public T invalid() {
        getMiter().setValid(false);
        return (self());
    }


    /**
     *  Sets the expiration.
     *
     *  @param date the authenticated expiration
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T expiration(java.util.Date date) {
        getMiter().setExpiration(date);
        return (self());
    }



    /**
     *  Adds a credential.
     *
     *  @param credentialType the tyep of credential
     *  @param credential the authenticated credential
     *  @throws org.osid.NullArgumentException
     *          <code>credentialType</code> or <code>credential</code>
     *          is <code>null</code>
     */

    public T credential(org.osid.type.Type credentialType, java.lang.Object credential) {
        getMiter().addCredential(credentialType, credential);
        return (self());
    }


    /**
     *  Adds an Authentication record.
     *
     *  @param record an authentication record
     *  @param recordType the type of authentication record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.authentication.process.records.AuthenticationRecord record, org.osid.type.Type recordType) {
        getMiter().addAuthenticationRecord(record, recordType);
        return (self());
    }
}       


