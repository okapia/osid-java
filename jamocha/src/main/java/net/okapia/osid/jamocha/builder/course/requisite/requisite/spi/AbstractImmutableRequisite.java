//
// AbstractImmutableRequisite.java
//
//     Wraps a mutable Requisite to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Requisite</code> to hide modifiers. This
 *  wrapper provides an immutized Requisite from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying requisite whose state changes are visible.
 */

public abstract class AbstractImmutableRequisite
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.course.requisite.Requisite {

    private final org.osid.course.requisite.Requisite requisite;


    /**
     *  Constructs a new <code>AbstractImmutableRequisite</code>.
     *
     *  @param requisite the requisite to immutablize
     *  @throws org.osid.NullArgumentException <code>requisite</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRequisite(org.osid.course.requisite.Requisite requisite) {
        super(requisite);
        this.requisite = requisite;
        return;
    }


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  requisite.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.requisite.isSequestered());
    }


    /**
     *  Tests if any requisite options are defined. If no requisite options 
     *  are defined, this requisite term evaluates to <code> true. </code> If 
     *  requisite options are defined, then at least one <code> Requisite 
     *  </code> must evaluate to <code> true </code> for the requisite option 
     *  term to be <code> true. </code> 
     *
     *  @return <code> true </code> if any requisite options are defined, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasRequisiteOptions() {
        return (this.requisite.hasRequisiteOptions());
    }


    /**
     *  Gets the requisite options for the <code> Requisite </code> options 
     *  term. If any active requisite options are available, meeting the 
     *  requirements of any one of these requisite options evaluates this 
     *  requisite options term as <code> true. </code> If no active requisite 
     *  options apply then this term evaluates to <code> false. </code> 
     *
     *  @return the requsites 
     *  @throws org.osid.IllegalStateException <code> hasRequisiteOptions() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getRequisiteOptions() {
        return (this.requisite.getRequisiteOptions());
    }


    /**
     *  Tests if any course requirements are defined. If no course 
     *  requirements are defined, this course requirements term evaluates to 
     *  <code> true. </code> If course requirements are defined, then at least 
     *  one <code> CourseRequirement </code> must evaluate to <code> true 
     *  </code> for the course requirement term to be <code> true. </code> 
     *
     *  @return <code> true </code> if any course requirements are defined, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCourseRequirements() {
        return (this.requisite.hasCourseRequirements());
    }


    /**
     *  Gets the course requirements term. If any <code> CourseRequirements 
     *  </code> are available, meeting the requirements of any one of the 
     *  <code> CourseRequirements </code> evaluates this course requirements 
     *  term as <code> true. </code> If no active course requirements apply 
     *  then this course requirements term is <code> false. </code> 
     *
     *  @return the course requirements 
     *  @throws org.osid.IllegalStateException <code> hasCourseRequirements() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequirement[] getCourseRequirements() {
        return (this.requisite.getCourseRequirements());
    }


    /**
     *  Tests if any program requirements are defined. If no program 
     *  requirements are defined, this program requirements term evaluates to 
     *  <code> true. </code> If program requirements are defined, then at 
     *  least one <code> ProgramRequirement </code> must evaluate to <code> 
     *  true </code> for the program requirement term to be <code> true. 
     *  </code> 
     *
     *  @return <code> true </code> if any program requirements are defined, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasProgramRequirements() {
        return (this.requisite.hasProgramRequirements());
    }


    /**
     *  Gets the program requirements term. If any <code> ProgramRequirements 
     *  </code> are available, meeting the requirements of any one of the 
     *  <code> ProgramRequirements </code> evaluates this program requirements 
     *  term as <code> true. </code> If no program requirements apply then 
     *  this program requirements term evaluates to <code> false. </code> 
     *
     *  @return the program requirements 
     *  @throws org.osid.IllegalStateException <code> hasProgramRequirements() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.ProgramRequirement[] getProgramRequirements() {
        return (this.requisite.getProgramRequirements());
    }


    /**
     *  Tests if any credential requirements are defined. If no credential 
     *  requirements are defined, this credential requirements term evaluates 
     *  to <code> true. </code> If credential requirements are defined, then 
     *  at least one <code> CredentialRequirement </code> must evaluate to 
     *  <code> true </code> for the credential requirement term to be <code> 
     *  true. </code> 
     *
     *  @return <code> true </code> if any credential requirements are 
     *          defined, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCredentialRequirements() {
        return (this.requisite.hasCredentialRequirements());
    }


    /**
     *  Gets the credential requirements term. If any <code> 
     *  CredentialRequirements </code> are available, meeting the requirements 
     *  of any one of the <code> CredentialRequirements </code> evaluates this 
     *  credential requirements term as <code> true. </code> If no credential 
     *  requirements are available, this credential requirements term 
     *  evaluates to <code> false. </code> 
     *
     *  @return the credential requirements 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasCredentialRequirements() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.CredentialRequirement[] getCredentialRequirements() {
        return (this.requisite.getCredentialRequirements());
    }


    /**
     *  Tests if any learning objective requirements are defined. If no 
     *  learning objective requirements are defined, this learning objective 
     *  requirements term evaluates to <code> true. </code> If learning 
     *  objective requirements are defined, then at least one <code> 
     *  LearningObjectiveRequirement </code> must evaluate to <code> true 
     *  </code> for the learning objective requirement term to be <code> true. 
     *  </code> 
     *
     *  @return <code> true </code> if any learning objective requirements are 
     *          defined, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLearningObjectiveRequirements() {
        return (this.requisite.hasLearningObjectiveRequirements());
    }


    /**
     *  Gets the learning objective requirements term. If any <code> 
     *  LearningObjectiveRequirements </code> are available, meeting the 
     *  requirements of any one of the <code> LearningObjectiveRequirements 
     *  </code> evaluates this learning objective requirements term as <code> 
     *  true. </code> If no learning objective requirements applythen this 
     *  learning objective requirements term evaluates to <code> false. 
     *  </code> 
     *
     *  @return the learning objective requirements 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasLearningObjectiveRequirements() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.LearningObjectiveRequirement[] getLearningObjectiveRequirements() {
        return (this.requisite.getLearningObjectiveRequirements());
    }


    /**
     *  Tests if any assessment requirements are defined. If no assessment 
     *  requirements are defined, this assessment requirements term evaluates 
     *  to <code> true. </code> If assessment requirements are defined, then 
     *  at least one <code> AssessmentRequirement </code> must evaluate to 
     *  <code> true </code> for the assessment requirement term to be <code> 
     *  true. </code> 
     *
     *  @return <code> true </code> if any assessment requirements are 
     *          defined, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAssessmentRequirements() {
        return (this.requisite.hasAssessmentRequirements());
    }


    /**
     *  Gets the assessment requirements term. If any <code> 
     *  AssessmentRequirements </code> are available, meeting the requirements 
     *  of any one of the <code> AssessmentRequirements </code> evaluates this 
     *  assessment requirements term as <code> true. </code> If no assessment 
     *  requirements are available, this assessment requirements term 
     *  evaluates to <code> false. </code> 
     *
     *  @return the assessment requirements 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasAssessmentRequirements() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.AssessmentRequirement[] getAssessmentRequirements() {
        return (this.requisite.getAssessmentRequirements());
    }


    /**
     *  Tests if any award requirements are defined. If no award requirements 
     *  are defined, this award requirements term evaluates to <code> true. 
     *  </code> If award requirements are defined, then at least one <code> 
     *  AwardRequirement </code> must evaluate to <code> true </code> for the 
     *  award requirement term to be <code> true. </code> 
     *
     *  @return <code> true </code> if any award requirements are defined, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAwardRequirements() {
        return (this.requisite.hasAwardRequirements());
    }


    /**
     *  Gets the award requirements term. If any <code> AwardRequirements 
     *  </code> are available, meeting the requirements of any one of the 
     *  <code> AwardRequirements </code> evaluates this award requirements 
     *  term as <code> true. </code> If no award requirements are available, 
     *  this award requirements term evaluates to <code> false. </code> 
     *
     *  @return the award requirements 
     *  @throws org.osid.IllegalStateException <code> hasAwardRequirements() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.AwardRequirement[] getAwardRequirements() {
        return (this.requisite.getAwardRequirements());
    }


    /**
     *  Gets the requisite record corresponding to the given <code> Requisite 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  requisiteRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(requisiteRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  requisiteRecordType the type of requisite record to retrieve 
     *  @return the requisite record 
     *  @throws org.osid.NullArgumentException <code> requisiteRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(requisiteRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteRecord getRequisiteRecord(org.osid.type.Type requisiteRecordType)
        throws org.osid.OperationFailedException {

        return (this.requisite.getRequisiteRecord(requisiteRecordType));
    }
}

