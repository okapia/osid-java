//
// AbstractPostSearchOdrer.java
//
//     Defines a PostSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.post.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PostSearchOrder}.
 */

public abstract class AbstractPostSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.forum.PostSearchOrder {

    private final java.util.Collection<org.osid.forum.records.PostSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the timestamp. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimestamp(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the poster. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPoster(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource order interface is available. 
     *
     *  @return <code> true </code> if a poster order interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPosterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the poster order interface. 
     *
     *  @return the poster search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPosterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getPosterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPosterSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the posting 
     *  agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPostingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a posting agent order interface is available. 
     *
     *  @return <code> true </code> if a posting agent order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the posting agent search order interface. 
     *
     *  @return the posting agent search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getPostingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPostingAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the subject. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubjectLine(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  postRecordType a post record type 
     *  @return {@code true} if the postRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code postRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type postRecordType) {
        for (org.osid.forum.records.PostSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(postRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  postRecordType the post record type 
     *  @return the post search order record
     *  @throws org.osid.NullArgumentException
     *          {@code postRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(postRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.forum.records.PostSearchOrderRecord getPostSearchOrderRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.forum.records.PostSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this post. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param postRecord the post search odrer record
     *  @param postRecordType post record type
     *  @throws org.osid.NullArgumentException
     *          {@code postRecord} or
     *          {@code postRecordTypepost} is
     *          {@code null}
     */
            
    protected void addPostRecord(org.osid.forum.records.PostSearchOrderRecord postSearchOrderRecord, 
                                     org.osid.type.Type postRecordType) {

        addRecordType(postRecordType);
        this.records.add(postSearchOrderRecord);
        
        return;
    }
}
