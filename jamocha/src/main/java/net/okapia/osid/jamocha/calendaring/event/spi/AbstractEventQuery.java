//
// AbstractEventQuery.java
//
//     A template for making an Event Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.event.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for events.
 */

public abstract class AbstractEventQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.calendaring.EventQuery {

    private final java.util.Collection<org.osid.calendaring.records.EventQueryRecord> records = new java.util.ArrayList<>();
    private final OsidContainableQuery containableQuery = new OsidContainableQuery();

    
    /**
     *  Matches an event that is implicitly generated. 
     *
     *  @param  match <code> true </code> to match events implicitly 
     *          generated, <code> false </code> to match events explicitly 
     *          defined 
     */

    @OSID @Override
    public void matchImplicit(boolean match) {
        return;
    }


    /**
     *  Clears the implcit terms. 
     */

    @OSID @Override
    public void clearImplicitTerms() {
        return;
    }


    /**
     *  Matches the event duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        return;
    }


    /**
     *  Matches an event that has any duration. 
     *
     *  @param  match <code> true </code> to match events with any duration, 
     *          <code> false </code> to match events with no start time 
     */

    @OSID @Override
    public void matchAnyDuration(boolean match) {
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        return;
    }


    /**
     *  Matches events that related to the recurring event. 
     *
     *  @param  recurringEventId an <code> Id </code> for a recurring event 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recurringEventId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRecurringEventId(org.osid.id.Id recurringEventId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the recurring event terms. 
     */

    @OSID @Override
    public void clearRecurringEventIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RecurringEventQuery </code> is available for 
     *  querying recurring events. 
     *
     *  @return <code> true </code> if a recurring event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recurring event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the recurring event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuery getRecurringEventQuery() {
        throw new org.osid.UnimplementedException("supportsRecurringEventQuery() is false");
    }


    /**
     *  Matches an event that is part of any recurring event. 
     *
     *  @param  match <code> true </code> to match events part of any 
     *          recurring event, <code> false </code> to match only standalone 
     *          events 
     */

    @OSID @Override
    public void matchAnyRecurringEvent(boolean match) {
        return;
    }


    /**
     *  Clears the recurring event terms. 
     */

    @OSID @Override
    public void clearRecurringEventTerms() {
        return;
    }


    /**
     *  Matches events that relate to the superseding event. 
     *
     *  @param  supersedingEventId an <code> Id </code> for a superseding 
     *          event 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSupersedingEventId(org.osid.id.Id supersedingEventId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the superseding events type terms. 
     */

    @OSID @Override
    public void clearSupersedingEventIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SupersedingEventQuery </code> is available for 
     *  querying offset events. 
     *
     *  @return <code> true </code> if a superseding event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a superseding event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the superseding event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuery getSupersedingEventQuery() {
        throw new org.osid.UnimplementedException("supportsSupersedingEventQuery() is false");
    }


    /**
     *  Matches any superseding event. 
     *
     *  @param  match <code> true </code> to match any superseding events, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAnySupersedingEvent(boolean match) {
        return;
    }


    /**
     *  Clears the superseding event terms. 
     */

    @OSID @Override
    public void clearSupersedingEventTerms() {
        return;
    }


    /**
     *  Matches events that relates to the offset event. 
     *
     *  @param  offsetEventId an <code> Id </code> for an offset event 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOffsetEventId(org.osid.id.Id offsetEventId, boolean match) {
        return;
    }


    /**
     *  Clears the recurring events type terms. 
     */

    @OSID @Override
    public void clearOffsetEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OffsetEventQuery </code> is available for querying 
     *  offset events. 
     *
     *  @return <code> true </code> if an offset event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an offset event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the offset event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuery getOffsetEventQuery() {
        throw new org.osid.UnimplementedException("supportsOffsetEventQuery() is false");
    }


    /**
     *  Matches any offset event. 
     *
     *  @param  match <code> true </code> to match any offset events, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public void matchAnyOffsetEvent(boolean match) {
        return;
    }


    /**
     *  Clears the offset event terms. 
     */

    @OSID @Override
    public void clearOffsetEventTerms() {
        return;
    }


    /**
     *  Matches the location description string. 
     *
     *  @param  location location string 
     *  @param  stringMatchType string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> location </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> location </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLocationDescription(String location, 
                                         org.osid.type.Type stringMatchType, 
                                         boolean match) {
        return;
    }


    /**
     *  Matches an event that has any location description assigned. 
     *
     *  @param  match <code> true </code> to match events with any location 
     *          description, <code> false </code> to match events with no 
     *          location description 
     */

    @OSID @Override
    public void matchAnyLocationDescription(boolean match) {
        return;
    }


    /**
     *  Clears the location description terms. 
     */

    @OSID @Override
    public void clearLocationDescriptionTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  locations. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches an event that has any location assigned. 
     *
     *  @param  match <code> true </code> to match events with any location, 
     *          <code> false </code> to match events with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Sets the sponsor <code> Id </code> for this query. 
     *
     *  @param  sponsorId a sponsor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sponsorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSponsorId(org.osid.id.Id sponsorId, boolean match) {
        return;
    }


    /**
     *  Clears the sponsor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSponsorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for querying 
     *  sponsors. 
     *
     *  @return <code> true </code> if a sponsor query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSponsorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a sponsor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the sponsor query 
     *  @throws org.osid.UnimplementedException <code> supportsSponsorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getSponsorQuery() {
        throw new org.osid.UnimplementedException("supportsSponsorQuery() is false");
    }


    /**
     *  Clears the sponsor terms. 
     */

    @OSID @Override
    public void clearSponsorTerms() {
        return;
    }


    /**
     *  Matches events whose locations contain the given coordinate. 
     *
     *  @param  coordinate a coordinate 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> coordinate </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCoordinate(org.osid.mapping.Coordinate coordinate, 
                                boolean match) {
        return;
    }


    /**
     *  Clears the cooordinate terms. 
     */

    @OSID @Override
    public void clearCoordinateTerms() {
        return;
    }


    /**
     *  Matches events whose locations fall within the given spatial unit. 
     *
     *  @param  spatialUnit a spatial unit 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> spatialUnit </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSpatialUnit(org.osid.mapping.SpatialUnit spatialUnit, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the spatial unit terms. 
     */

    @OSID @Override
    public void clearSpatialUnitTerms() {
        return;
    }


    /**
     *  Sets the commitment <code> Id </code> for this query. 
     *
     *  @param  commitmentId a commitment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commitmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommitmentId(org.osid.id.Id commitmentId, boolean match) {
        return;
    }


    /**
     *  Clears the commitment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommitmentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CommitmentQuery </code> is available for querying 
     *  recurring event terms. 
     *
     *  @return <code> true </code> if a commitment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commitment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commitment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuery getCommitmentQuery() {
        throw new org.osid.UnimplementedException("supportsCommitmentQuery() is false");
    }


    /**
     *  Matches an event that has any commitment. 
     *
     *  @param  match <code> true </code> to match events with any commitment, 
     *          <code> false </code> to match events with no commitments 
     */

    @OSID @Override
    public void matchAnyCommitment(boolean match) {
        return;
    }


    /**
     *  Clears the commitment terms. 
     */

    @OSID @Override
    public void clearCommitmentTerms() {
        return;
    }


    /**
     *  Sets the event <code> Id </code> for this query to match events that 
     *  have the specified event as an ancestor. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContainingEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the containing event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContainingEventIdTerms() {
        return;
    }


    /**
     *  Tests if a containing event query is available. 
     *
     *  @return <code> true </code> if a containing event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContainingEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a containing event. 
     *
     *  @return the containing event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContainingEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getContainingEventQuery() {
        throw new org.osid.UnimplementedException("supportsContainingEventQuery() is false");
    }


    /**
     *  Matches events with any ancestor event. 
     *
     *  @param  match <code> true </code> to match events with any ancestor 
     *          event, <code> false </code> to match events with no ancestor 
     *          events 
     */

    @OSID @Override
    public void matchAnyContainingEvent(boolean match) {
        return;
    }


    /**
     *  Clears the containing event terms. 
     */

    @OSID @Override
    public void clearContainingEventTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  calendars. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }


    /**
     *  Match containables that are sequestered.
     *
     *  @param  match <code> true </code> to match any sequestered
     *          containables, <code> false </code> to match non-sequestered
     *          containables
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.containableQuery.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms.
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.containableQuery.clearSequesteredTerms();
        return;
    }


    /**
     *  Gets the record corresponding to the given event query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an event implementing the requested record.
     *
     *  @param eventRecordType an event record type
     *  @return the event query record
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(eventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.EventQueryRecord getEventQueryRecord(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.EventQueryRecord record : this.records) {
            if (record.implementsRecordType(eventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(eventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this event query. 
     *
     *  @param eventQueryRecord event query record
     *  @param eventRecordType event record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEventQueryRecord(org.osid.calendaring.records.EventQueryRecord eventQueryRecord, 
                                          org.osid.type.Type eventRecordType) {

        addRecordType(eventRecordType);
        nullarg(eventQueryRecord, "event query record");
        this.records.add(eventQueryRecord);        
        return;
    }


    protected class OsidContainableQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQuery
        implements org.osid.OsidContainableQuery {

    }
}
