//
// AbstractAdapterDistributorLookupSession.java
//
//    A Distributor lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Distributor lookup session adapter.
 */

public abstract class AbstractAdapterDistributorLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.provisioning.DistributorLookupSession {

    private final org.osid.provisioning.DistributorLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterDistributorLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterDistributorLookupSession(org.osid.provisioning.DistributorLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Distributor} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupDistributors() {
        return (this.session.canLookupDistributors());
    }


    /**
     *  A complete view of the {@code Distributor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDistributorView() {
        this.session.useComparativeDistributorView();
        return;
    }


    /**
     *  A complete view of the {@code Distributor} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDistributorView() {
        this.session.usePlenaryDistributorView();
        return;
    }

     
    /**
     *  Gets the {@code Distributor} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Distributor} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Distributor} and
     *  retained for compatibility.
     *
     *  @param distributorId {@code Id} of the {@code Distributor}
     *  @return the distributor
     *  @throws org.osid.NotFoundException {@code distributorId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code distributorId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDistributor(distributorId));
    }


    /**
     *  Gets a {@code DistributorList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  distributors specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Distributors} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  distributorIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Distributor} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code distributorIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByIds(org.osid.id.IdList distributorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDistributorsByIds(distributorIds));
    }


    /**
     *  Gets a {@code DistributorList} corresponding to the given
     *  distributor genus {@code Type} which does not include
     *  distributors of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  distributorGenusType a distributor genus type 
     *  @return the returned {@code Distributor} list
     *  @throws org.osid.NullArgumentException
     *          {@code distributorGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByGenusType(org.osid.type.Type distributorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDistributorsByGenusType(distributorGenusType));
    }


    /**
     *  Gets a {@code DistributorList} corresponding to the given
     *  distributor genus {@code Type} and include any additional
     *  distributors with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  distributorGenusType a distributor genus type 
     *  @return the returned {@code Distributor} list
     *  @throws org.osid.NullArgumentException
     *          {@code distributorGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByParentGenusType(org.osid.type.Type distributorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDistributorsByParentGenusType(distributorGenusType));
    }


    /**
     *  Gets a {@code DistributorList} containing the given
     *  distributor record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  distributorRecordType a distributor record type 
     *  @return the returned {@code Distributor} list
     *  @throws org.osid.NullArgumentException
     *          {@code distributorRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByRecordType(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDistributorsByRecordType(distributorRecordType));
    }


    /**
     *  Gets a {@code DistributorList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Distributor} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDistributorsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Distributors}. 
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Distributors} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getDistributors());
    }
}
