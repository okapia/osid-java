//
// AbstractMapBallotLookupSession
//
//    A simple framework for providing a Ballot lookup service
//    backed by a fixed collection of ballots.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Ballot lookup service backed by a
 *  fixed collection of ballots. The ballots are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Ballots</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBallotLookupSession
    extends net.okapia.osid.jamocha.voting.spi.AbstractBallotLookupSession
    implements org.osid.voting.BallotLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.voting.Ballot> ballots = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.voting.Ballot>());


    /**
     *  Makes a <code>Ballot</code> available in this session.
     *
     *  @param  ballot a ballot
     *  @throws org.osid.NullArgumentException <code>ballot<code>
     *          is <code>null</code>
     */

    protected void putBallot(org.osid.voting.Ballot ballot) {
        this.ballots.put(ballot.getId(), ballot);
        return;
    }


    /**
     *  Makes an array of ballots available in this session.
     *
     *  @param  ballots an array of ballots
     *  @throws org.osid.NullArgumentException <code>ballots<code>
     *          is <code>null</code>
     */

    protected void putBallots(org.osid.voting.Ballot[] ballots) {
        putBallots(java.util.Arrays.asList(ballots));
        return;
    }


    /**
     *  Makes a collection of ballots available in this session.
     *
     *  @param  ballots a collection of ballots
     *  @throws org.osid.NullArgumentException <code>ballots<code>
     *          is <code>null</code>
     */

    protected void putBallots(java.util.Collection<? extends org.osid.voting.Ballot> ballots) {
        for (org.osid.voting.Ballot ballot : ballots) {
            this.ballots.put(ballot.getId(), ballot);
        }

        return;
    }


    /**
     *  Removes a Ballot from this session.
     *
     *  @param  ballotId the <code>Id</code> of the ballot
     *  @throws org.osid.NullArgumentException <code>ballotId<code> is
     *          <code>null</code>
     */

    protected void removeBallot(org.osid.id.Id ballotId) {
        this.ballots.remove(ballotId);
        return;
    }


    /**
     *  Gets the <code>Ballot</code> specified by its <code>Id</code>.
     *
     *  @param  ballotId <code>Id</code> of the <code>Ballot</code>
     *  @return the ballot
     *  @throws org.osid.NotFoundException <code>ballotId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>ballotId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Ballot getBallot(org.osid.id.Id ballotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.voting.Ballot ballot = this.ballots.get(ballotId);
        if (ballot == null) {
            throw new org.osid.NotFoundException("ballot not found: " + ballotId);
        }

        return (ballot);
    }


    /**
     *  Gets all <code>Ballots</code>. In plenary mode, the returned
     *  list contains all known ballots or an error
     *  results. Otherwise, the returned list may contain only those
     *  ballots that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Ballots</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.voting.ballot.ArrayBallotList(this.ballots.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.ballots.clear();
        super.close();
        return;
    }
}
