//
// AntimatroidElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.antimatroid;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AntimatroidElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the AntimatroidElement Id.
     *
     *  @return the antimatroid element Id
     */

    public static org.osid.id.Id getAntimatroidEntityId() {
        return (makeEntityId("osid.sequencing.Antimatroid"));
    }


    /**
     *  Gets the ChainId element Id.
     *
     *  @return the ChainId element Id
     */

    public static org.osid.id.Id getChainId() {
        return (makeQueryElementId("osid.sequencing.antimatroid.ChainId"));
    }


    /**
     *  Gets the Chain element Id.
     *
     *  @return the Chain element Id
     */

    public static org.osid.id.Id getChain() {
        return (makeQueryElementId("osid.sequencing.antimatroid.Chain"));
    }


    /**
     *  Gets the AncestorAntimatroidId element Id.
     *
     *  @return the AncestorAntimatroidId element Id
     */

    public static org.osid.id.Id getAncestorAntimatroidId() {
        return (makeQueryElementId("osid.sequencing.antimatroid.AncestorAntimatroidId"));
    }


    /**
     *  Gets the AncestorAntimatroid element Id.
     *
     *  @return the AncestorAntimatroid element Id
     */

    public static org.osid.id.Id getAncestorAntimatroid() {
        return (makeQueryElementId("osid.sequencing.antimatroid.AncestorAntimatroid"));
    }


    /**
     *  Gets the DescendantAntimatroidId element Id.
     *
     *  @return the DescendantAntimatroidId element Id
     */

    public static org.osid.id.Id getDescendantAntimatroidId() {
        return (makeQueryElementId("osid.sequencing.antimatroid.DescendantAntimatroidId"));
    }


    /**
     *  Gets the DescendantAntimatroid element Id.
     *
     *  @return the DescendantAntimatroid element Id
     */

    public static org.osid.id.Id getDescendantAntimatroid() {
        return (makeQueryElementId("osid.sequencing.antimatroid.DescendantAntimatroid"));
    }
}
