//
// AbstractMutableGradeEntry.java
//
//     Defines a mutable GradeEntry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>GradeEntry</code>.
 */

public abstract class AbstractMutableGradeEntry
    extends net.okapia.osid.jamocha.grading.gradeentry.spi.AbstractGradeEntry
    implements org.osid.grading.GradeEntry,
               net.okapia.osid.jamocha.builder.grading.gradeentry.GradeEntryMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this grade entry. 
     *
     *  @param record grade entry record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addGradeEntryRecord(org.osid.grading.records.GradeEntryRecord record, org.osid.type.Type recordType) {
        super.addGradeEntryRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this grade entry is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this grade entry ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this grade entry.
     *
     *  @param displayName the name for this grade entry
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this grade entry.
     *
     *  @param description the description of this grade entry
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this grade entry
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    @Override
    public void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        super.setGradebookColumn(gradebookColumn);
        return;
    }


    /**
     *  Sets the key resource.
     *
     *  @param resource a key resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    @Override
    public void setKeyResource(org.osid.resource.Resource resource) {
        super.setKeyResource(resource);
        return;
    }


    /**
     *  Sets the overridden calculated entry.
     *
     *  @param entry an overridden calculated entry
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    @Override
    public void setOverriddenCalculatedEntry(org.osid.grading.GradeEntry entry) {
        super.setOverriddenCalculatedEntry(entry);
        return;
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    @Override
    public void setGrade(org.osid.grading.Grade grade) {
        super.setGrade(grade);
        return;
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    @Override
    public void setScore(java.math.BigDecimal score) {
        super.setScore(score);
        return;
    }


    /**
     *  Sets the time graded.
     *
     *  @param graded a time graded
     *  @throws org.osid.NullArgumentException <code>graded</code> is
     *          <code>null</code>
     */

    @Override
    public void setTimeGraded(org.osid.calendaring.DateTime graded) {
        super.setTimeGraded(graded);
        return;
    }


    /**
     *  Sets the grader.
     *
     *  @param grader a grader
     *  @throws org.osid.NullArgumentException
     *          <code>grader</code> is <code>null</code>
     */

    @Override
    public void setGrader(org.osid.resource.Resource grader) {
        super.setGrader(grader);
        return;
    }


    /**
     *  Sets the grading agent.
     *
     *  @param agent a grading agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    @Override
    public void setGradingAgent(org.osid.authentication.Agent agent) {
        super.setGradingAgent(agent);
        return;
    }
}

