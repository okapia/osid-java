//
// AbstractCost.java
//
//     Defines a Cost builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.cost.spi;


/**
 *  Defines a <code>Cost</code> builder.
 */

public abstract class AbstractCostBuilder<T extends AbstractCostBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ordering.cost.CostMiter cost;


    /**
     *  Constructs a new <code>AbstractCostBuilder</code>.
     *
     *  @param cost the cost to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCostBuilder(net.okapia.osid.jamocha.builder.ordering.cost.CostMiter cost) {
        this.cost = cost;
        return;
    }


    /**
     *  Builds the cost.
     *
     *  @return the new cost
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ordering.Cost build() {
        (new net.okapia.osid.jamocha.builder.validator.ordering.cost.CostValidator(getValidations())).validate(this.cost);
        return (new net.okapia.osid.jamocha.builder.ordering.cost.ImmutableCost(this.cost));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the cost miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ordering.cost.CostMiter getMiter() {
        return (this.cost);
    }


    /** 
     *  Sets the schedule.
     *
     *  @param schedule the schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    public T priceSchedule(org.osid.ordering.PriceSchedule schedule) {
        getMiter().setPriceSchedule(schedule);
        return(self());
    }

    
    /** 
     *  Sets the amount.
     *
     *  @param amount the amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T amount(org.osid.financials.Currency amount) {
        getMiter().setAmount(amount);
        return(self());
    }
}


