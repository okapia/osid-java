//
// AbstractCommentSearch.java
//
//     A template for making a Comment Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.comment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing comment searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractCommentSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.commenting.CommentSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.commenting.records.CommentSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.commenting.CommentSearchOrder commentSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of comments. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  commentIds list of comments
     *  @throws org.osid.NullArgumentException
     *          <code>commentIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongComments(org.osid.id.IdList commentIds) {
        while (commentIds.hasNext()) {
            try {
                this.ids.add(commentIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongComments</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of comment Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getCommentIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  commentSearchOrder comment search order 
     *  @throws org.osid.NullArgumentException
     *          <code>commentSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>commentSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderCommentResults(org.osid.commenting.CommentSearchOrder commentSearchOrder) {
	this.commentSearchOrder = commentSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.commenting.CommentSearchOrder getCommentSearchOrder() {
	return (this.commentSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given comment search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a comment implementing the requested record.
     *
     *  @param commentSearchRecordType a comment search record
     *         type
     *  @return the comment search record
     *  @throws org.osid.NullArgumentException
     *          <code>commentSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commentSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.CommentSearchRecord getCommentSearchRecord(org.osid.type.Type commentSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.commenting.records.CommentSearchRecord record : this.records) {
            if (record.implementsRecordType(commentSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commentSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this comment search. 
     *
     *  @param commentSearchRecord comment search record
     *  @param commentSearchRecordType comment search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCommentSearchRecord(org.osid.commenting.records.CommentSearchRecord commentSearchRecord, 
                                           org.osid.type.Type commentSearchRecordType) {

        addRecordType(commentSearchRecordType);
        this.records.add(commentSearchRecord);        
        return;
    }
}
