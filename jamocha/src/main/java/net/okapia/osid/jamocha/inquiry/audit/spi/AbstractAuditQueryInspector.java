//
// AbstractAuditQueryInspector.java
//
//     A template for making an AuditQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.audit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for audits.
 */

public abstract class AbstractAuditQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQueryInspector
    implements org.osid.inquiry.AuditQueryInspector {

    private final java.util.Collection<org.osid.inquiry.records.AuditQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the inquiry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquiryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the inquiry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryQueryInspector[] getInquiryTerms() {
        return (new org.osid.inquiry.InquiryQueryInspector[0]);
    }


    /**
     *  Gets the inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquestIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given audit query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an audit implementing the requested record.
     *
     *  @param auditRecordType an audit record type
     *  @return the audit query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auditRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.AuditQueryInspectorRecord getAuditQueryInspectorRecord(org.osid.type.Type auditRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.AuditQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(auditRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auditRecordType + " is not supported");
    }


    /**
     *  Adds a record to this audit query. 
     *
     *  @param auditQueryInspectorRecord audit query inspector
     *         record
     *  @param auditRecordType audit record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuditQueryInspectorRecord(org.osid.inquiry.records.AuditQueryInspectorRecord auditQueryInspectorRecord, 
                                                   org.osid.type.Type auditRecordType) {

        addRecordType(auditRecordType);
        nullarg(auditRecordType, "audit record type");
        this.records.add(auditQueryInspectorRecord);        
        return;
    }
}
