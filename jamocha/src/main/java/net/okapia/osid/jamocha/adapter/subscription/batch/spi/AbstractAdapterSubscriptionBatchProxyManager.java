//
// AbstractSubscriptionBatchProxyManager.java
//
//     An adapter for a SubscriptionBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.subscription.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a SubscriptionBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterSubscriptionBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.subscription.batch.SubscriptionBatchProxyManager>
    implements org.osid.subscription.batch.SubscriptionBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterSubscriptionBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterSubscriptionBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterSubscriptionBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterSubscriptionBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of subscriptions is available. 
     *
     *  @return <code> true </code> if a subscription bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionBatchAdmin() {
        return (getAdapteeManager().supportsSubscriptionBatchAdmin());
    }


    /**
     *  Tests if bulk administration of dispatches is available. 
     *
     *  @return <code> true </code> if a dispatch bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDispatchBatchAdmin() {
        return (getAdapteeManager().supportsDispatchBatchAdmin());
    }


    /**
     *  Tests if bulk administration of publishers is available. 
     *
     *  @return <code> true </code> if an publisher bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherBatchAdmin() {
        return (getAdapteeManager().supportsPublisherBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk work 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DispatchBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.batch.DispatchBatchAdminSession getSubscriptionBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk 
     *  subscription administration service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SubscriptionBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.batch.SubscriptionBatchAdminSession getSubscriptionBatchAdminSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubscriptionBatchAdminSessionForPublisher(publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk dispatch 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DispatchBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.batch.DispatchBatchAdminSession getDispatchBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk dispatch 
     *  administration service for the given publisher. 
     *
     *  @param  publisherId the <code> Id </code> of the <code> Publisher 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DispatchBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Publisher </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDispatchBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.batch.DispatchBatchAdminSession getDispatchBatchAdminSessionForPublisher(org.osid.id.Id publisherId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDispatchBatchAdminSessionForPublisher(publisherId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk publisher 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PublisherBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.batch.PublisherBatchAdminSession getPublisherBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPublisherBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
