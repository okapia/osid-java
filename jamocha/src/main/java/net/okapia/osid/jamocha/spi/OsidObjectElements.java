//
// OsidObjectElements.java
//
//     Id definitions for OsidForm and OsidQuery fields.
//
//
// Tom Coppeto
// Okapia
// 15 May 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;


/**
 *  Ids for OsidForm and OsidQuery elements. These are used in
 *  Metadata and may also be used as a key for relational mapping.
 */

public class OsidObjectElements
    extends BrowsableElements {


    /**
     *  Gets the display name element Id.
     *
     *  @return the display name element Id
     */

    public static org.osid.id.Id getDisplayName() {
        return (makeElementId("osid.OsidObject.DisplayName"));
    }


    /**
     *  Gets the description element Id.
     *
     *  @return the description element Id
     */

    public static org.osid.id.Id getDescription() {
        return (makeElementId("osid.OsidObject.Description"));
    }


    /**
     *  Gets the genus type element Id.
     *
     *  @return the genus type element Id
     */

    public static org.osid.id.Id getGenusType() {
        return (makeElementId("osid.OsidObject.GenusType"));
    }


    /**
     *  Gets the parent genus type element Id.
     *
     *  @return the parent genus type element Id
     */

    public static org.osid.id.Id getParentGenusType() {
        return (makeElementId("osid.OsidObject.ParentGenusType"));
    }


    /**
     *  Gets the subject Id element Id.
     *
     *  @return the subject Id element Id
     */

    public static org.osid.id.Id getSubjectId() {
        return (makeQueryElementId("osid.OsidObjectQuery.SubjectId"));
    }


    /**
     *  Gets the subject element Id.
     *
     *  @return the subject element Id
     */

    public static org.osid.id.Id getSubject() {
        return (makeQueryElementId("osid.OsidObjectQuery.Subject"));
    }


    /**
     *  Gets the subject relevancy element Id.
     *
     *  @return the subject relevancy element Id
     */

    public static org.osid.id.Id getSubjectRelevancy() {
        return (makeQueryElementId("osid.OsidObjectQuery.SubjectRelevancy"));
    }


    /**
     *  Gets the state Id element Id.
     *
     *  @return the state Id element Id
     */

    public static org.osid.id.Id getStateId() {
        return (makeQueryElementId("osid.OsidObjectQuery.StateId"));
    }


    /**
     *  Gets the state element Id.
     *
     *  @return the state element Id
     */

    public static org.osid.id.Id getState() {
        return (makeQueryElementId("osid.OsidObjectQuery.State"));
    }


    /**
     *  Gets the comment Id element Id.
     *
     *  @return the comment Id element Id
     */

    public static org.osid.id.Id getCommentId() {
        return (makeQueryElementId("osid.OsidObjectQuery.CommentId"));
    }


    /**
     *  Gets the comment element Id.
     *
     *  @return the comment element Id
     */

    public static org.osid.id.Id getComment() {
        return (makeQueryElementId("osid.OsidObjectQuery.Comment"));
    }


    /**
     *  Gets the cumulative rating element Id.
     *
     *  @return the cumulative rating element Id
     */

    public static org.osid.id.Id getCumulativeRating() {
        return (makeSearchOrderElementId("osid.OsidObjectQuery.CumulativeRating"));
    }

    
    /**
     *  Gets the journal entry Id element Id.
     *
     *  @return the journal entry Id element Id
     */

    public static org.osid.id.Id getJournalEntryId() {
        return (makeQueryElementId("osid.OsidObjectQuery.JournalEntryId"));
    }


    /**
     *  Gets the journal entry element Id.
     *
     *  @return the journal entry element Id
     */

    public static org.osid.id.Id getJournalEntry() {
        return (makeQueryElementId("osid.OsidObjectQuery.JournalEntry"));
    }


    /**
     *  Gets the statistic element Id.
     *
     *  @return the statistic element Id
     */

    public static org.osid.id.Id getStatistic() {
        return (makeQueryElementId("osid.OsidObjectQuery.Statistic"));
    }


    /**
     *  Gets the credit Id element Id.
     *
     *  @return the credit Id element Id
     */

    public static org.osid.id.Id getCreditId() {
        return (makeQueryElementId("osid.OsidObjectQuery.CreditId"));
    }


    /**
     *  Gets the credit element Id.
     *
     *  @return the credit element Id
     */

    public static org.osid.id.Id getCredit() {
        return (makeQueryElementId("osid.OsidObjectQuery.Credit"));
    }
    

    /**
     *  Gets the relationship Id element Id.
     *
     *  @return the relationship Id element Id
     */

    public static org.osid.id.Id getRelationshipId() {
        return (makeQueryElementId("osid.OsidObjectQuery.RelationshipId"));
    }


    /**
     *  Gets the relationship element Id.
     *
     *  @return the relationship element Id
     */

    public static org.osid.id.Id getRelationship() {
        return (makeQueryElementId("osid.OsidObjectQuery.Relationship"));
    }

    
    /**
     *  Gets the relationship peer Id element Id.
     *
     *  @return the relationship peer Id element Id
     */

    public static org.osid.id.Id getRelationshipPeerId() {
        return (makeQueryElementId("osid.OsidObjectQuery.RelationshipPeerId"));
    }


    /**
     *  Gets the create time element Id.
     *
     *  @return the create time element Id
     */

    public static org.osid.id.Id getCreateTime() {
        return (makeSearchOrderElementId("osid.OsidObjectSearchOrder.CreateTime"));
    }


    /**
     *  Gets the last modified time element Id.
     *
     *  @return the last modified time element Id
     */

    public static org.osid.id.Id getLastModifiedTime() {
        return (makeSearchOrderElementId("osid.OsidObjectSearchOrder.LastModifiedTime"));
    }
}

