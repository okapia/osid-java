//
// Property
//
//     Implements a property.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.property;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements a Property.
 */

public final class Property
    extends net.okapia.osid.jamocha.property.spi.AbstractProperty
    implements org.osid.Property {


    /**
     *  Constructs a new <code>Property</code> with no value.
     *
     *  @param displayName the property display name
     *  @param displayLabel a property display label
     *  @param description the property description
     *  @throws org.osid.NullArgumentException
     *          <code>displayName</code>, <code>displayLabel</code> or
     *          <code>description</code> is <code>null</code>
     */

    public Property(org.osid.locale.DisplayText displayName, 
                    org.osid.locale.DisplayText displayLabel, 
                    org.osid.locale.DisplayText description) {

        super(displayName, displayLabel, description);
        return;
    }


    /**
     *  Constructs a new <code>Property</code>.
     *
     *  @param displayName the property display name
     *  @param displayLabel a property display label
     *  @param description the property description
     *  @param value the property value
     *  @throws org.osid.NullArgumentException
     *          <code>displayName</code>, <code>displayLabel</code>,
     *          <code>description</code> or <code>value</code> is
     *          <code>null</code>
     */

    public Property(org.osid.locale.DisplayText displayName, 
                    org.osid.locale.DisplayText displayLabel, 
                    org.osid.locale.DisplayText description, 
                    String value) {

        super(displayName, displayLabel, description, value);
        return;
    }


    /**
     *  Constructs a new <code>Property</code>.
     *
     *  @param property
     *  @param value the property value
     *  @throws org.osid.NullArgumentException <code>property</code>
     *          or <code>value</code> is
     *          <code>null</code>
     */

    public Property(org.osid.Property property, String value) {
        super(property, value);
        return;
    }
}
