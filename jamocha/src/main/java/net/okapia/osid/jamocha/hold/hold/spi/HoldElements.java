//
// HoldElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.hold.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class HoldElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the HoldElement Id.
     *
     *  @return the hold element Id
     */

    public static org.osid.id.Id getHoldEntityId() {
        return (makeEntityId("osid.hold.Hold"));
    }


    /**
     *  Gets the IssueId element Id.
     *
     *  @return the IssueId element Id
     */

    public static org.osid.id.Id getIssueId() {
        return (makeElementId("osid.hold.hold.IssueId"));
    }


    /**
     *  Gets the Issue element Id.
     *
     *  @return the Issue element Id
     */

    public static org.osid.id.Id getIssue() {
        return (makeElementId("osid.hold.hold.Issue"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.hold.hold.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.hold.hold.Resource"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeElementId("osid.hold.hold.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeElementId("osid.hold.hold.Agent"));
    }


    /**
     *  Gets the OublietteId element Id.
     *
     *  @return the OublietteId element Id
     */

    public static org.osid.id.Id getOublietteId() {
        return (makeQueryElementId("osid.hold.hold.OublietteId"));
    }


    /**
     *  Gets the Oubliette element Id.
     *
     *  @return the Oubliette element Id
     */

    public static org.osid.id.Id getOubliette() {
        return (makeQueryElementId("osid.hold.hold.Oubliette"));
    }
}
