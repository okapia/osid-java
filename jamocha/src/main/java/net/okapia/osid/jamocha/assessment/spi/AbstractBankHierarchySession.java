//
// AbstractBankHierarchySession.java
//
//     Defines a BankHierarchySession template.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>BankHierarchySession</code> template.
 */

public abstract class AbstractBankHierarchySession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.assessment.BankHierarchySession {

    private org.osid.hierarchy.Hierarchy hierarchy = new net.okapia.osid.jamocha.nil.hierarchy.hierarchy.UnknownHierarchy();
    private boolean comparative = false;


    /**
     *  Gets the hierarchy <code> Id </code> associated with this
     *  session.
     *
     *  @return the hierarchy <code> Id </code> associated with this
     *          session
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.Id getBankHierarchyId() {
        return (this.hierarchy.getId());
    }


    /**
     *  Gets the hierarchy associated with this session. 
     *
     *  @return the hierarchy associated with this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Hierarchy getBankHierarchy()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.hierarchy);
    }


    /**
     *  Sets the hierarchy associated with this session. 
     *
     *  @param hierarchy
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          is <code>null</code>
     */

    protected void setHierarchy(org.osid.hierarchy.Hierarchy hierarchy) {
        nullarg(hierarchy, "hierarchy");
        this.hierarchy = hierarchy;
        return;
    }

    
    /**
     *  Tests if this user can perform hierarchy queries. A return of
     *  true does not guarantee successful authorization. A return of
     *  false indicates that it is known all methods in this session
     *  will result in a <code> PERMISSION_DENIED. </code> This is
     *  intended as a hint to an application that may opt not to offer
     *  lookup operations.
     *
     *  @return <code> false </code> if hierarchy traversal methods
     *          are not authorized, <code> true </code> otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canAccessBankHierarchy() {
        return (true);
    }


    /**
     *  The returns from the bin methods may omit or translate
     *  elements based on this session, such as authorization, and not
     *  result in an error. This view is used when greater
     *  interoperability is desired at the expense of precision.
     */

    @OSID @Override
    public void useComparativeBankView() {
        this.comparative = true;
        return;
    }


    /**
     *  A complete view of the <code> Bank </code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBankView() {
        this.comparative = false;
        return;
    }


    /**
     *  Tests if comparative view is set.
     *
     *  @return <code>true</code> if comparative, <code>false</code>
     *          otherwise
     */

    protected boolean isComparative() {
        return (this.comparative);
    }
}
