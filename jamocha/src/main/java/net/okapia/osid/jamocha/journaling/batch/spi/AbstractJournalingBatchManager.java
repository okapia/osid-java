//
// AbstractJournalingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractJournalingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.journaling.batch.JournalingBatchManager,
               org.osid.journaling.batch.JournalingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractJournalingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractJournalingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of journal entries is available. 
     *
     *  @return <code> true </code> if a journal entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of branches is available. 
     *
     *  @return <code> true </code> if a branch bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of journals is available. 
     *
     *  @return <code> true </code> if a journal bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  entry administration service. 
     *
     *  @return a <code> JournalEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalEntryBatchAdminSession getJournalEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchManager.getJournalEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  entry administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalEntryBatchAdminSession getJournalEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchProxyManager.getJournalEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  entry administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> JournalEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalEntryBatchAdminSession getJournalEntryBatchAdminSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchManager.getJournalEntryBatchAdminSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  entry administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalEntryBatchAdminSession getJournalEntryBatchAdminSessionForJournal(org.osid.id.Id journalId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchProxyManager.getJournalEntryBatchAdminSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk branch 
     *  administration service. 
     *
     *  @return a <code> BranchBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.BranchBatchAdminSession getBranchBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchManager.getBranchBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk branch 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BranchBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.BranchBatchAdminSession getBranchBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchProxyManager.getBranchBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk branch 
     *  administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> BranchBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.BranchBatchAdminSession getBranchBatchAdminSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchManager.getBranchBatchAdminSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk branch 
     *  administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BranchBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.BranchBatchAdminSession getBranchBatchAdminSessionForJournal(org.osid.id.Id journalId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchProxyManager.getBranchBatchAdminSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  administration service. 
     *
     *  @return a <code> JournalBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalBatchAdminSession getJournalBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchManager.getJournalBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk journal 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalBatchAdminSession getJournalBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.batch.JournalingBatchProxyManager.getJournalBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
