//
// AbstractAssemblyCredentialQuery.java
//
//     A CredentialQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.program.credential.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CredentialQuery that stores terms.
 */

public abstract class AbstractAssemblyCredentialQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.course.program.CredentialQuery,
               org.osid.course.program.CredentialQueryInspector,
               org.osid.course.program.CredentialSearchOrder {

    private final java.util.Collection<org.osid.course.program.records.CredentialQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.CredentialQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.program.records.CredentialSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCredentialQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCredentialQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches lifetimes between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchLifetime(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        getAssembler().addDurationRangeTerm(getLifetimeColumn(), low, high, match);
        return;
    }


    /**
     *  Matches credentials that have any lifetime. 
     *
     *  @param  match <code> true </code> to match credentials with any 
     *          lifetime, <code> false </code> to match credentials with no 
     *          lifetime 
     */

    @OSID @Override
    public void matchAnyLifetime(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getLifetimeColumn(), match);
        return;
    }


    /**
     *  Clears the lifetime <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLifetimeTerms() {
        getAssembler().clearTerms(getLifetimeColumn());
        return;
    }


    /**
     *  Gets the lifetime query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getLifetimeTerms() {
        return (getAssembler().getDurationRangeTerms(getLifetimeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by lifetime. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLifetime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLifetimeColumn(), style);
        return;
    }


    /**
     *  Gets the Lifetime column name.
     *
     * @return the column name
     */

    protected String getLifetimeColumn() {
        return ("lifetime");
    }


    /**
     *  Sets the program <code> Id </code> for this query to match credentials 
     *  that have a related program. 
     *
     *  @param  programId a program <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> programId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProgramId(org.osid.id.Id programId, boolean match) {
        getAssembler().addIdTerm(getProgramIdColumn(), programId, match);
        return;
    }


    /**
     *  Clears the program <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProgramIdTerms() {
        getAssembler().clearTerms(getProgramIdColumn());
        return;
    }


    /**
     *  Gets the program <code> Id </code> query terms. 
     *
     *  @return the program <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramIdTerms() {
        return (getAssembler().getIdTerms(getProgramIdColumn()));
    }


    /**
     *  Gets the ProgramId column name.
     *
     * @return the column name
     */

    protected String getProgramIdColumn() {
        return ("program_id");
    }


    /**
     *  Tests if a <code> ProgramQuery </code> is available. 
     *
     *  @return <code> true </code> if a program query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Gets the query for a program. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the program query 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuery getProgramQuery() {
        throw new org.osid.UnimplementedException("supportsProgramQuery() is false");
    }


    /**
     *  Matches credentials that have any program. 
     *
     *  @param  match <code> true </code> to match credentials with any 
     *          related program, <code> false </code> to match credentials 
     *          with no programs 
     */

    @OSID @Override
    public void matchAnyProgram(boolean match) {
        getAssembler().addIdWildcardTerm(getProgramColumn(), match);
        return;
    }


    /**
     *  Clears the program terms. 
     */

    @OSID @Override
    public void clearProgramTerms() {
        getAssembler().clearTerms(getProgramColumn());
        return;
    }


    /**
     *  Gets the program query terms. 
     *
     *  @return the program query terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQueryInspector[] getProgramTerms() {
        return (new org.osid.course.program.ProgramQueryInspector[0]);
    }


    /**
     *  Gets the Program column name.
     *
     * @return the column name
     */

    protected String getProgramColumn() {
        return ("program");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  credentials assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this credential supports the given record
     *  <code>Type</code>.
     *
     *  @param  credentialRecordType a credential record type 
     *  @return <code>true</code> if the credentialRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type credentialRecordType) {
        for (org.osid.course.program.records.CredentialQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(credentialRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  credentialRecordType the credential record type 
     *  @return the credential query record 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.CredentialQueryRecord getCredentialQueryRecord(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.CredentialQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(credentialRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  credentialRecordType the credential record type 
     *  @return the credential query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.CredentialQueryInspectorRecord getCredentialQueryInspectorRecord(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.CredentialQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(credentialRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param credentialRecordType the credential record type
     *  @return the credential search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>credentialRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(credentialRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.CredentialSearchOrderRecord getCredentialSearchOrderRecord(org.osid.type.Type credentialRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.CredentialSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(credentialRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(credentialRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this credential. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param credentialQueryRecord the credential query record
     *  @param credentialQueryInspectorRecord the credential query inspector
     *         record
     *  @param credentialSearchOrderRecord the credential search order record
     *  @param credentialRecordType credential record type
     *  @throws org.osid.NullArgumentException
     *          <code>credentialQueryRecord</code>,
     *          <code>credentialQueryInspectorRecord</code>,
     *          <code>credentialSearchOrderRecord</code> or
     *          <code>credentialRecordTypecredential</code> is
     *          <code>null</code>
     */
            
    protected void addCredentialRecords(org.osid.course.program.records.CredentialQueryRecord credentialQueryRecord, 
                                      org.osid.course.program.records.CredentialQueryInspectorRecord credentialQueryInspectorRecord, 
                                      org.osid.course.program.records.CredentialSearchOrderRecord credentialSearchOrderRecord, 
                                      org.osid.type.Type credentialRecordType) {

        addRecordType(credentialRecordType);

        nullarg(credentialQueryRecord, "credential query record");
        nullarg(credentialQueryInspectorRecord, "credential query inspector record");
        nullarg(credentialSearchOrderRecord, "credential search odrer record");

        this.queryRecords.add(credentialQueryRecord);
        this.queryInspectorRecords.add(credentialQueryInspectorRecord);
        this.searchOrderRecords.add(credentialSearchOrderRecord);
        
        return;
    }
}
