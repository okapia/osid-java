//
// AbstractFederatingActivityLookupSession.java
//
//     An abstract federating adapter for an ActivityLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  ActivityLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingActivityLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.ActivityLookupSession>
    implements org.osid.course.ActivityLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingActivityLookupSession</code>.
     */

    protected AbstractFederatingActivityLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.ActivityLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Activity</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivities() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            if (session.canLookupActivities()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Activity</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityView() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            session.useComparativeActivityView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Activity</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityView() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            session.usePlenaryActivityView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only activities whose effective dates are current are returned
     *  by methods in this session.
     */

    @OSID @Override
    public void useEffectiveActivityView() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            session.useEffectiveActivityView();
        }

        return;
    }


    /**
     *  All activities of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveActivityView() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            session.useAnyEffectiveActivityView();
        }

        return;
    }


    /**
     *  A normalized view uses a single <code>Activity</code> to represent a
     *  set of recurring activities.
     */

    @OSID @Override
    public void useNormalizedActivityView() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            session.useNormalizedActivityView();
        }

        return;
    }


    /**
     *  A denormalized view expands activities into a series of <code>
     *  Activities.</code>
     */

    @OSID @Override
    public void useDenormalizedActivityView() {
        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            session.useDenormalizedActivityView();
        }

        return;
    }


    /**
     *  Gets the <code>Activity</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Activity</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Activity</code> and
     *  retained for compatibility.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityId <code>Id</code> of the
     *          <code>Activity</code>
     *  @return the activity
     *  @throws org.osid.NotFoundException <code>activityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            try {
                return (session.getActivity(activityId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(activityId + " not found");
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>activityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.activity.MutableActivityList ret = new net.okapia.osid.jamocha.course.activity.MutableActivityList();

        try (org.osid.id.IdList ids = activityIds) {
            while (ids.hasNext()) {
                ret.addActivity(getActivity(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> which does not include
     *  activities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesByGenusType(activityGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> and include any additional
     *  activities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesByParentGenusType(activityGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActivityList</code> containing the given
     *  activity record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesByRecordType(activityRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ActivityList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *  
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesOnDate(org.osid.calendaring.DateTime from, 
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Activities</code> associated with a given
     *  <code>ActivityUnit</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityUnitId an activity unit <code>Id</code>
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesForActivityUnit(activityUnitId));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ActivityList</code> for an activity unit effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *  
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityUnitId an activity unit <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitOnDate(org.osid.id.Id activityUnitId, 
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesForActivityUnitOnDate(activityUnitId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Activities</code> associated with a given
     *  <code>Term</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  termId an activity unit <code>Id</code>
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>termId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForTerm(org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesForTerm(termId));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ActivityList</code> for an term effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *  
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  termId an term <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>termId</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForTermOnDate(org.osid.id.Id termId, 
                                                                           org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesForTermOnDate(termId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Activities</code> associated with a given
     *  <code>ActivityUnit</code> for a term. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityUnitId an activity unit <code>Id</code>
     *  @param  termId a term <code>Id</code>
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code> or <code>termId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitAndTerm(org.osid.id.Id activityUnitId,
                                                                            org.osid.id.Id termId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesForActivityUnitAndTerm(activityUnitId, termId));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ActivityList</code> for an activity unit, term,
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *  
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityUnitId an activity unit <code>Id</code>
     *  @param termId a term <code>Id</code>
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitId</code>, <code>termId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForActivityUnitAndTermOnDate(org.osid.id.Id activityUnitId, 
                                                                                  org.osid.id.Id termId, 
                                                                                  org.osid.calendaring.DateTime from, 
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesForActivityUnitAndTermOnDate(activityUnitId, termId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Activities</code> associated with a given
     *  <code>CourseOffering</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  courseOfferingId a course <code>Id</code>
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivitiesForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivitiesForCourseOffering(courseOfferingId));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Activities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Activities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList ret = getActivityList();

        for (org.osid.course.ActivityLookupSession session : getSessions()) {
            ret.addActivityList(session.getActivities());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.activity.FederatingActivityList getActivityList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.activity.ParallelActivityList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.activity.CompositeActivityList());
        }
    }
}
