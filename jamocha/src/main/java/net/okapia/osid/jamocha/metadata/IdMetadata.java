//
// IdMetadata.java
//
//     Defines an Id Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines an Id Metadata.
 */

public final class IdMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractIdMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unblinked {@code IdMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public IdMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code IdMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public IdMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code IdMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public IdMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
        return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }

    
    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }

    
    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code false} if
     *         can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }

    
    /**
     *  Sets the Id set.
     *
     *  @param values a collection of accepted Id values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setIdSet(java.util.Collection<org.osid.id.Id> values) {
        super.setIdSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the Id set.
     *
     *  @param values a collection of accepted Id values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToIdSet(java.util.Collection<org.osid.id.Id> values) {
        super.addToIdSet(values);
        return;
    }


    /**
     *  Adds a value to the Id set.
     *
     *  @param value an Id value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void addToIdSet(org.osid.id.Id value) {
        super.addToIdSet(value);
        return;
    }


    /**
     *  Removes a value from the Id set.
     *
     *  @param value an Id value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    public void removeFromIdSet(org.osid.id.Id value) {
        super.removeFromIdSet(value);
        return;
    }


    /**
     *  Clears the Id set.
     */

    public void clearIdSet() {
        super.clearIdSet();
        return;
    }


    /**
     *  Sets the default Id set.
     *
     *  @param values a collection of default Id values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultIdValues(java.util.Collection<org.osid.id.Id> values) {
        super.setDefaultIdValues(values);
        return;
    }


    /**
     *  Adds a collection of default Id values.
     *
     *  @param values a collection of default Id values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultIdValues(java.util.Collection<org.osid.id.Id> values) {
        super.addDefaultIdValues(values);
        return;
    }


    /**
     *  Adds a default Id value.
     *
     *  @param value an Id value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultIdValue(org.osid.id.Id value) {
        super.addDefaultIdValue(value);
        return;
    }


    /**
     *  Removes a default Id value.
     *
     *  @param value an Id value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultIdValue(org.osid.id.Id value) {
        super.removeDefaultIdValue(value);
        return;
    }


    /**
     *  Clears the default Id values.
     */

    public void clearDefaultIdValues() {
        super.clearDefaultIdValues();
        return;
    }


    /**
     *  Sets the existing Id set.
     *
     *  @param values a collection of existing Id values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingIdValues(java.util.Collection<org.osid.id.Id> values) {
        super.setExistingIdValues(values);
        return;
    }


    /**
     *  Adds a collection of existing Id values.
     *
     *  @param values a collection of existing Id values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingIdValues(java.util.Collection<org.osid.id.Id> values) {
        super.addExistingIdValues(values);
        return;
    }


    /**
     *  Adds a existing Id value.
     *
     *  @param value an Id value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingIdValue(org.osid.id.Id value) {
        super.addExistingIdValue(value);
        return;
    }


    /**
     *  Removes a existing Id value.
     *
     *  @param value an Id value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingIdValue(org.osid.id.Id value) {
        super.removeExistingIdValue(value);
        return;
    }


    /**
     *  Clears the existing Id values.
     */

    public void clearExistingIdValues() {
        super.clearExistingIdValues();
        return;
    }        
}
