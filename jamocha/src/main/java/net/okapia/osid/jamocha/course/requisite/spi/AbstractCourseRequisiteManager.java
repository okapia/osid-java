//
// AbstractCourseRequisiteManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCourseRequisiteManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.course.requisite.CourseRequisiteManager,
               org.osid.course.requisite.CourseRequisiteProxyManager {

    private final Types requisiteRecordTypes               = new TypeRefSet();
    private final Types requisiteSearchRecordTypes         = new TypeRefSet();

    private final Types courseRequirementRecordTypes       = new TypeRefSet();
    private final Types programRequirementRecordTypes      = new TypeRefSet();
    private final Types credentialRequirementRecordTypes   = new TypeRefSet();
    private final Types learningObjectiveRequirementRecordTypes= new TypeRefSet();
    private final Types assessmentRequirementRecordTypes   = new TypeRefSet();
    private final Types awardRequirementRecordTypes        = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCourseRequisiteManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCourseRequisiteManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up requisites is supported. 
     *
     *  @return <code> true </code> if requisite lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteLookup() {
        return (false);
    }


    /**
     *  Tests if querying requisites is supported. 
     *
     *  @return <code> true </code> if requisite query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteQuery() {
        return (false);
    }


    /**
     *  Tests if searching requisites is supported. 
     *
     *  @return <code> true </code> if requisite search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteSearch() {
        return (false);
    }


    /**
     *  Tests if requisite administrative service is supported. 
     *
     *  @return <code> true </code> if requisite administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteAdmin() {
        return (false);
    }


    /**
     *  Tests if a requisite <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if requisite notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteNotification() {
        return (false);
    }


    /**
     *  Tests if a requisite cataloging service is supported. 
     *
     *  @return <code> true </code> if requisite cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps requisites to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a requisite smart course catalog session is available. 
     *
     *  @return <code> true </code> if a requisite smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequisiteSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Gets the supported <code> Requisite </code> record types. 
     *
     *  @return a list containing the supported <code> Requisite </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequisiteRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.requisiteRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Requisite </code> record type is supported. 
     *
     *  @param  requisiteRecordType a <code> Type </code> indicating a <code> 
     *          Requisite </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> requisiteRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequisiteRecordType(org.osid.type.Type requisiteRecordType) {
        return (this.requisiteRecordTypes.contains(requisiteRecordType));
    }


    /**
     *  Adds support for a requisite record type.
     *
     *  @param requisiteRecordType a requisite record type
     *  @throws org.osid.NullArgumentException
     *  <code>requisiteRecordType</code> is <code>null</code>
     */

    protected void addRequisiteRecordType(org.osid.type.Type requisiteRecordType) {
        this.requisiteRecordTypes.add(requisiteRecordType);
        return;
    }


    /**
     *  Removes support for a requisite record type.
     *
     *  @param requisiteRecordType a requisite record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>requisiteRecordType</code> is <code>null</code>
     */

    protected void removeRequisiteRecordType(org.osid.type.Type requisiteRecordType) {
        this.requisiteRecordTypes.remove(requisiteRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Requisite </code> search record types. 
     *
     *  @return a list containing the supported <code> Requisite </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequisiteSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.requisiteSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Requisite </code> search record type is 
     *  supported. 
     *
     *  @param  requisiteSearchRecordType a <code> Type </code> indicating a 
     *          <code> Requisite </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          requisiteSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequisiteSearchRecordType(org.osid.type.Type requisiteSearchRecordType) {
        return (this.requisiteSearchRecordTypes.contains(requisiteSearchRecordType));
    }


    /**
     *  Adds support for a requisite search record type.
     *
     *  @param requisiteSearchRecordType a requisite search record type
     *  @throws org.osid.NullArgumentException
     *  <code>requisiteSearchRecordType</code> is <code>null</code>
     */

    protected void addRequisiteSearchRecordType(org.osid.type.Type requisiteSearchRecordType) {
        this.requisiteSearchRecordTypes.add(requisiteSearchRecordType);
        return;
    }


    /**
     *  Removes support for a requisite search record type.
     *
     *  @param requisiteSearchRecordType a requisite search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>requisiteSearchRecordType</code> is <code>null</code>
     */

    protected void removeRequisiteSearchRecordType(org.osid.type.Type requisiteSearchRecordType) {
        this.requisiteSearchRecordTypes.remove(requisiteSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CourseRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> CourseRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseRequirementRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.courseRequirementRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CourseRequirement </code> record type is 
     *  supported. 
     *
     *  @param  courseRequirementRecordType a <code> Type </code> indicating a 
     *          <code> CourseRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseRequirementRecordType(org.osid.type.Type courseRequirementRecordType) {
        return (this.courseRequirementRecordTypes.contains(courseRequirementRecordType));
    }


    /**
     *  Adds support for a course requirement record type.
     *
     *  @param courseRequirementRecordType a course requirement record type
     *  @throws org.osid.NullArgumentException
     *  <code>courseRequirementRecordType</code> is <code>null</code>
     */

    protected void addCourseRequirementRecordType(org.osid.type.Type courseRequirementRecordType) {
        this.courseRequirementRecordTypes.add(courseRequirementRecordType);
        return;
    }


    /**
     *  Removes support for a course requirement record type.
     *
     *  @param courseRequirementRecordType a course requirement record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>courseRequirementRecordType</code> is <code>null</code>
     */

    protected void removeCourseRequirementRecordType(org.osid.type.Type courseRequirementRecordType) {
        this.courseRequirementRecordTypes.remove(courseRequirementRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProgramRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> ProgramRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramRequirementRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.programRequirementRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProgramRequirement </code> record type is 
     *  supported. 
     *
     *  @param  programRequirementRecordType a <code> Type </code> indicating 
     *          a <code> ProgramRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          programRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramRequirementRecordType(org.osid.type.Type programRequirementRecordType) {
        return (this.programRequirementRecordTypes.contains(programRequirementRecordType));
    }


    /**
     *  Adds support for a program requirement record type.
     *
     *  @param programRequirementRecordType a program requirement record type
     *  @throws org.osid.NullArgumentException
     *  <code>programRequirementRecordType</code> is <code>null</code>
     */

    protected void addProgramRequirementRecordType(org.osid.type.Type programRequirementRecordType) {
        this.programRequirementRecordTypes.add(programRequirementRecordType);
        return;
    }


    /**
     *  Removes support for a program requirement record type.
     *
     *  @param programRequirementRecordType a program requirement record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>programRequirementRecordType</code> is <code>null</code>
     */

    protected void removeProgramRequirementRecordType(org.osid.type.Type programRequirementRecordType) {
        this.programRequirementRecordTypes.remove(programRequirementRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CredentialRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> CredentialRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialRequirementRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.credentialRequirementRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CredentialRequirement </code> record type is 
     *  supported. 
     *
     *  @param  credentialRequirementRecordType a <code> Type </code> 
     *          indicating a <code> CredentialRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialRequirementRecordType(org.osid.type.Type credentialRequirementRecordType) {
        return (this.credentialRequirementRecordTypes.contains(credentialRequirementRecordType));
    }


    /**
     *  Adds support for a credential requirement record type.
     *
     *  @param credentialRequirementRecordType a credential requirement record type
     *  @throws org.osid.NullArgumentException
     *  <code>credentialRequirementRecordType</code> is <code>null</code>
     */

    protected void addCredentialRequirementRecordType(org.osid.type.Type credentialRequirementRecordType) {
        this.credentialRequirementRecordTypes.add(credentialRequirementRecordType);
        return;
    }


    /**
     *  Removes support for a credential requirement record type.
     *
     *  @param credentialRequirementRecordType a credential requirement record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>credentialRequirementRecordType</code> is <code>null</code>
     */

    protected void removeCredentialRequirementRecordType(org.osid.type.Type credentialRequirementRecordType) {
        this.credentialRequirementRecordTypes.remove(credentialRequirementRecordType);
        return;
    }


    /**
     *  Gets the supported <code> LearningObjectiveRequirement </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          LearningObjectiveRequirement </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLearningObjectiveRequirementRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.learningObjectiveRequirementRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> LearningObjectiveRequirement </code> record 
     *  type is supported. 
     *
     *  @param  learningObjectiveRequirementRecordType a <code> Type </code> 
     *          indicating a <code> LearningObjectivelRequirement </code> 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          learningObjectiveRequirementRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsLearningObjectiveRequirementRecordType(org.osid.type.Type learningObjectiveRequirementRecordType) {
        return (this.learningObjectiveRequirementRecordTypes.contains(learningObjectiveRequirementRecordType));
    }


    /**
     *  Adds support for a learning objective requirement record type.
     *
     *  @param learningObjectiveRequirementRecordType a learning objective requirement record type
     *  @throws org.osid.NullArgumentException
     *  <code>learningObjectiveRequirementRecordType</code> is <code>null</code>
     */

    protected void addLearningObjectiveRequirementRecordType(org.osid.type.Type learningObjectiveRequirementRecordType) {
        this.learningObjectiveRequirementRecordTypes.add(learningObjectiveRequirementRecordType);
        return;
    }


    /**
     *  Removes support for a learning objective requirement record type.
     *
     *  @param learningObjectiveRequirementRecordType a learning objective requirement record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>learningObjectiveRequirementRecordType</code> is <code>null</code>
     */

    protected void removeLearningObjectiveRequirementRecordType(org.osid.type.Type learningObjectiveRequirementRecordType) {
        this.learningObjectiveRequirementRecordTypes.remove(learningObjectiveRequirementRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AsessmentRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentRequirementRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.assessmentRequirementRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AsessmentRequirement </code> record type is 
     *  supported. 
     *
     *  @param  assessmentRequirementRecordType a <code> Type </code> 
     *          indicating an <code> AssessmentRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentRequirementRecordType(org.osid.type.Type assessmentRequirementRecordType) {
        return (this.assessmentRequirementRecordTypes.contains(assessmentRequirementRecordType));
    }


    /**
     *  Adds support for an assessment requirement record type.
     *
     *  @param assessmentRequirementRecordType an assessment requirement record type
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentRequirementRecordType</code> is <code>null</code>
     */

    protected void addAssessmentRequirementRecordType(org.osid.type.Type assessmentRequirementRecordType) {
        this.assessmentRequirementRecordTypes.add(assessmentRequirementRecordType);
        return;
    }


    /**
     *  Removes support for an assessment requirement record type.
     *
     *  @param assessmentRequirementRecordType an assessment requirement record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentRequirementRecordType</code> is <code>null</code>
     */

    protected void removeAssessmentRequirementRecordType(org.osid.type.Type assessmentRequirementRecordType) {
        this.assessmentRequirementRecordTypes.remove(assessmentRequirementRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AsessmentRequirement </code> record types. 
     *
     *  @return a list containing the supported <code> AwardRequirement 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardRequirementRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.awardRequirementRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AsessmentRequirement </code> record type is 
     *  supported. 
     *
     *  @param  awardRequirementRecordType a <code> Type </code> indicating an 
     *          <code> AwardRequirement </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          awardRequirementRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardRequirementRecordType(org.osid.type.Type awardRequirementRecordType) {
        return (this.awardRequirementRecordTypes.contains(awardRequirementRecordType));
    }


    /**
     *  Adds support for an award requirement record type.
     *
     *  @param awardRequirementRecordType an award requirement record type
     *  @throws org.osid.NullArgumentException
     *  <code>awardRequirementRecordType</code> is <code>null</code>
     */

    protected void addAwardRequirementRecordType(org.osid.type.Type awardRequirementRecordType) {
        this.awardRequirementRecordTypes.add(awardRequirementRecordType);
        return;
    }


    /**
     *  Removes support for an award requirement record type.
     *
     *  @param awardRequirementRecordType an award requirement record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>awardRequirementRecordType</code> is <code>null</code>
     */

    protected void removeAwardRequirementRecordType(org.osid.type.Type awardRequirementRecordType) {
        this.awardRequirementRecordTypes.remove(awardRequirementRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  lookup service. 
     *
     *  @return a <code> RequisiteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteLookupSession getRequisiteLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteLookupSession getRequisiteLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> RequisiteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteLookupSession getRequisiteLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteLookupSession getRequisiteLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  query service. 
     *
     *  @return a <code> RequisiteQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuerySession getRequisiteQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuerySession getRequisiteQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RequisiteQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuerySession getRequisiteQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQuerySession getRequisiteQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  search service. 
     *
     *  @return a <code> RequisiteSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSearchSession getRequisiteSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSearchSession getRequisiteSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RequisiteSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSearchSession getRequisiteSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSearchSession getRequisiteSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  administration service. 
     *
     *  @return a <code> RequisiteAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteAdminSession getRequisiteAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteAdminSession getRequisiteAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RequisiteAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteAdminSession getRequisiteAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteAdminSession getRequisiteAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  notification service. 
     *
     *  @param  requisiteReceiver the notification callback 
     *  @return a <code> RequisiteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> requisiteReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteNotificationSession getRequisiteNotificationSession(org.osid.course.requisite.RequisiteReceiver requisiteReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  notification service. 
     *
     *  @param  requisiteReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> requisiteReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteNotificationSession getRequisiteNotificationSession(org.osid.course.requisite.RequisiteReceiver requisiteReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  requisiteReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RequisiteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> requisiteReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteNotificationSession getRequisiteNotificationSessionForCourseCatalog(org.osid.course.requisite.RequisiteReceiver requisiteReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  notification service for the given course catalog. 
     *
     *  @param  requisiteReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> requisiteReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteNotificationSession getRequisiteNotificationSessionForCourseCatalog(org.osid.course.requisite.RequisiteReceiver requisiteReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup requisite/catalog 
     *  mappings. 
     *
     *  @return a <code> RequisiteCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteCourseCatalogSession getRequisiteCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup requisite/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteCourseCatalogSession getRequisiteCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  requisites to course catalogs. 
     *
     *  @return a <code> RequisiteCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteCourseCatalogAssignmentSession getRequisiteCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  requisites to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> RequisiteCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteCourseCatalogAssignmentSession getRequisiteCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> RequisiteSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSmartCourseCatalogSession getRequisiteSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteManager.getRequisiteSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the requisite 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> RequisiteSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequisiteSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteSmartCourseCatalogSession getRequisiteSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.requisite.CourseRequisiteProxyManager.getRequisiteSmartCourseCatalogSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.requisiteRecordTypes.clear();
        this.requisiteRecordTypes.clear();

        this.requisiteSearchRecordTypes.clear();
        this.requisiteSearchRecordTypes.clear();

        this.courseRequirementRecordTypes.clear();
        this.courseRequirementRecordTypes.clear();

        this.programRequirementRecordTypes.clear();
        this.programRequirementRecordTypes.clear();

        this.credentialRequirementRecordTypes.clear();
        this.credentialRequirementRecordTypes.clear();

        this.learningObjectiveRequirementRecordTypes.clear();
        this.learningObjectiveRequirementRecordTypes.clear();

        this.assessmentRequirementRecordTypes.clear();
        this.assessmentRequirementRecordTypes.clear();

        this.awardRequirementRecordTypes.clear();
        this.awardRequirementRecordTypes.clear();

        return;
    }
}
