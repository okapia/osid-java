//
// AbstractIndexedMapIssueLookupSession.java
//
//    A simple framework for providing an Issue lookup service
//    backed by a fixed collection of issues with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Issue lookup service backed by a
 *  fixed collection of issues. The issues are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some issues may be compatible
 *  with more types than are indicated through these issue
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Issues</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapIssueLookupSession
    extends AbstractMapIssueLookupSession
    implements org.osid.tracking.IssueLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.tracking.Issue> issuesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.tracking.Issue>());
    private final MultiMap<org.osid.type.Type, org.osid.tracking.Issue> issuesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.tracking.Issue>());


    /**
     *  Makes an <code>Issue</code> available in this session.
     *
     *  @param  issue an issue
     *  @throws org.osid.NullArgumentException <code>issue<code> is
     *          <code>null</code>
     */

    @Override
    protected void putIssue(org.osid.tracking.Issue issue) {
        super.putIssue(issue);

        this.issuesByGenus.put(issue.getGenusType(), issue);
        
        try (org.osid.type.TypeList types = issue.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.issuesByRecord.put(types.getNextType(), issue);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an issue from this session.
     *
     *  @param issueId the <code>Id</code> of the issue
     *  @throws org.osid.NullArgumentException <code>issueId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeIssue(org.osid.id.Id issueId) {
        org.osid.tracking.Issue issue;
        try {
            issue = getIssue(issueId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.issuesByGenus.remove(issue.getGenusType());

        try (org.osid.type.TypeList types = issue.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.issuesByRecord.remove(types.getNextType(), issue);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeIssue(issueId);
        return;
    }


    /**
     *  Gets an <code>IssueList</code> corresponding to the given
     *  issue genus <code>Type</code> which does not include
     *  issues of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known issues or an error results. Otherwise,
     *  the returned list may contain only those issues that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  issueGenusType an issue genus type 
     *  @return the returned <code>Issue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>issueGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.issue.ArrayIssueList(this.issuesByGenus.get(issueGenusType)));
    }


    /**
     *  Gets an <code>IssueList</code> containing the given
     *  issue record <code>Type</code>. In plenary mode, the
     *  returned list contains all known issues or an error
     *  results. Otherwise, the returned list may contain only those
     *  issues that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  issueRecordType an issue record type 
     *  @return the returned <code>issue</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>issueRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.IssueList getIssuesByRecordType(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.issue.ArrayIssueList(this.issuesByRecord.get(issueRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.issuesByGenus.clear();
        this.issuesByRecord.clear();

        super.close();

        return;
    }
}
