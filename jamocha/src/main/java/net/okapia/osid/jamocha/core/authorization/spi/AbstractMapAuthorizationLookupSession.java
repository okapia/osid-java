//
// AbstractMapAuthorizationLookupSession
//
//    A simple framework for providing an Authorization lookup service
//    backed by a fixed collection of authorizations.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Authorization lookup service backed by a
 *  fixed collection of authorizations. The authorizations are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Authorizations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAuthorizationLookupSession
    extends net.okapia.osid.jamocha.authorization.spi.AbstractAuthorizationLookupSession
    implements org.osid.authorization.AuthorizationLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.authorization.Authorization> authorizations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.authorization.Authorization>());
    private final java.util.Map<org.osid.id.Id, org.osid.id.Id> explicitAuthorizations = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.id.Id>());


    /**
     *  Makes an <code>Authorization</code> available in this session.
     *
     *  @param  authorization an authorization
     *  @throws org.osid.NullArgumentException <code>authorization<code>
     *          is <code>null</code>
     */

    protected void putAuthorization(org.osid.authorization.Authorization authorization) {
        this.authorizations.put(authorization.getId(), authorization);
        return;
    }


    /**
     *  Makes an array of authorizations available in this session.
     *
     *  @param  authorizations an array of authorizations
     *  @throws org.osid.NullArgumentException <code>authorizations<code>
     *          is <code>null</code>
     */

    protected void putAuthorizations(org.osid.authorization.Authorization[] authorizations) {
        putAuthorizations(java.util.Arrays.asList(authorizations));
        return;
    }


    /**
     *  Makes a collection of authorizations available in this session.
     *
     *  @param  authorizations a collection of authorizations
     *  @throws org.osid.NullArgumentException <code>authorizations<code>
     *          is <code>null</code>
     */

    protected void putAuthorizations(java.util.Collection<? extends org.osid.authorization.Authorization> authorizations) {
        for (org.osid.authorization.Authorization authorization : authorizations) {
            this.authorizations.put(authorization.getId(), authorization);
        }

        return;
    }


    /**
     *  Removes an Authorization from this session.
     *
     *  @param  authorizationId the <code>Id</code> of the authorization
     *  @throws org.osid.NullArgumentException <code>authorizationId<code> is
     *          <code>null</code>
     */

    protected void removeAuthorization(org.osid.id.Id authorizationId) {
        this.authorizations.remove(authorizationId);
        return;
    }


    /**
     *  Sets the explicit authorization for an implicit authorization.
     *  Only one explicit authorization per customer. This method does
     *  not check for the existence of the Ids or wether they are
     *  really explicit or implicit.
     *
     *  @param  explicitAuthorizationId the explicit authorization Id
     *  @param  implicitAuthorizationId the implicit authorization Id
     *  @throws org.osid.NullArgumentException
     *          <code>explicitAuthorizationId<code> or
     *          <code>implicitAuthorizationId<code> is
     *          <code>null</code>
     */

    protected void setExplicitAuthorization(org.osid.id.Id explicitAuthorizationId, 
                                           org.osid.id.Id implicitAuthorizationId) {

        this.explicitAuthorizations.put(implicitAuthorizationId, explicitAuthorizationId);
        return;
    }


    /**
     *  Unsets the explicit authorization for an implicit
     *  authorization.
     *
     *  @param  implicitAuthorizationId the implicit authorization Id
     *  @throws org.osid.NullArgumentException
     *          <code>implicitAuthorizationId<code> is
     *          <code>null</code>
     */

    protected void removeExplicitAuthorization(org.osid.id.Id implicitAuthorizationId) {
        this.explicitAuthorizations.remove(implicitAuthorizationId);
        return;
    }


    /**
     *  Gets the <code>Authorization</code> specified by its <code>Id</code>.
     *
     *  @param  authorizationId <code>Id</code> of the <code>Authorization</code>
     *  @return the authorization
     *  @throws org.osid.NotFoundException <code>authorizationId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>authorizationId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.Authorization getAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.authorization.Authorization authorization = this.authorizations.get(authorizationId);
        if (authorization == null) {
            throw new org.osid.NotFoundException("authorization not found: " + authorizationId);
        }

        return (authorization);
    }


    /**
     *  Gets the explicit <code>Authorization</code> that generated
     *  the given implicit authorization. If the given
     *  <code>Authorization</code> is explicit, then the same
     *  <code>Authorization</code> is returned.
     *
     *  @param  authorizationId an authorization
     *  @return the explicit <code>Authorization</code>
     *  @throws org.osid.NotFoundException
     *          <code>authorizationId</code> is not found
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.authorization.Authorization getExplicitAuthorization(org.osid.id.Id authorizationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.id.Id id = this.explicitAuthorizations.get(authorizationId);
        org.osid.authorization.Authorization entry;

        if (id == null) {
            entry = getAuthorization(authorizationId);
            if (entry.isImplicit()) {
                throw new org.osid.NotFoundException("no implicit authorization found for " + authorizationId);
            }
        } else {
            entry = getAuthorization(id);
            if (entry.isImplicit()) {
                throw new org.osid.OperationFailedException("an implicit authorization was found for " + authorizationId);
            }
        }

        return (entry);
    }


    /**
     *  Gets all <code>Authorizations</code>. In plenary mode, the returned
     *  list contains all known authorizations or an error
     *  results. Otherwise, the returned list may contain only those
     *  authorizations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Authorizations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationList getAuthorizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.authorization.ArrayAuthorizationList(this.authorizations.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.authorizations.clear();
        super.close();
        return;
    }
}
