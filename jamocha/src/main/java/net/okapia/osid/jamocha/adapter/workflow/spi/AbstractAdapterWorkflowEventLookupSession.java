//
// AbstractAdapterWorkflowEventLookupSession.java
//
//    A WorkflowEvent lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A WorkflowEvent lookup session adapter.
 */

public abstract class AbstractAdapterWorkflowEventLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.WorkflowEventLookupSession {

    private final org.osid.workflow.WorkflowEventLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterWorkflowEventLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterWorkflowEventLookupSession(org.osid.workflow.WorkflowEventLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Office/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Office Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the {@code Office} associated with this session.
     *
     *  @return the {@code Office} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform {@code WorkflowEvent} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupWorkflowEvents() {
        return (this.session.canLookupWorkflowEvents());
    }


    /**
     *  A complete view of the {@code WorkflowEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWorkflowEventView() {
        this.session.useComparativeWorkflowEventView();
        return;
    }


    /**
     *  A complete view of the {@code WorkflowEvent} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWorkflowEventView() {
        this.session.usePlenaryWorkflowEventView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include workflow events in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    
     
    /**
     *  Gets the {@code WorkflowEvent} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code WorkflowEvent} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code WorkflowEvent} and
     *  retained for compatibility.
     *
     *  @param workflowEventId {@code Id} of the {@code WorkflowEvent}
     *  @return the workflow event
     *  @throws org.osid.NotFoundException {@code workflowEventId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code workflowEventId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEvent getWorkflowEvent(org.osid.id.Id workflowEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEvent(workflowEventId));
    }


    /**
     *  Gets a {@code WorkflowEventList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  workflowEvents specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code WorkflowEvents} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  workflowEventIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code WorkflowEvent} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code workflowEventIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByIds(org.osid.id.IdList workflowEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByIds(workflowEventIds));
    }


    /**
     *  Gets a {@code WorkflowEventList} corresponding to the given
     *  workflow event genus {@code Type} which does not include
     *  workflow events of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  workflow events or an error results. Otherwise, the returned list
     *  may contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workflowEventGenusType a workflowEvent genus type 
     *  @return the returned {@code WorkflowEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code workflowEventGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByGenusType(org.osid.type.Type workflowEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByGenusType(workflowEventGenusType));
    }


    /**
     *  Gets a {@code WorkflowEventList} corresponding to the given
     *  workflow event genus {@code Type} and include any additional
     *  workflow events with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  workflow events or an error results. Otherwise, the returned list
     *  may contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workflowEventGenusType a workflowEvent genus type 
     *  @return the returned {@code WorkflowEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code workflowEventGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByParentGenusType(org.osid.type.Type workflowEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByParentGenusType(workflowEventGenusType));
    }


    /**
     *  Gets a {@code WorkflowEventList} containing the given
     *  workflow event record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  workflow events or an error results. Otherwise, the returned list
     *  may contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workflowEventRecordType a workflowEvent record type 
     *  @return the returned {@code WorkflowEvent} list
     *  @throws org.osid.NullArgumentException
     *          {@code workflowEventRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByRecordType(org.osid.type.Type workflowEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByRecordType(workflowEventRecordType));
    }


    /**
     *  Gets the entire workflow log within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  start start range 
     *  @param  end end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code start} or {@code 
     *          end} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDate(org.osid.calendaring.DateTime start, 
                                                                       org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByDate(start, end));
    }


    /**
     *  Gets the entire workflow log for a process. In plenary mode,
     *  the returned list contains all known workflow events or an
     *  error results.  Otherwise, the returned list may contain only
     *  those workflow events that are accessible through this
     *  session.
     *
     *  @param  processId a process {@code Id} 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException {@code processId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForProcess(org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsForProcess(processId));
    }

    
    /**
     *  Gets the entire workflow log for this process within the given date 
     *  range inclusive. In plenary mode, the returned list contains all known 
     *  workflow events or an error results. Otherwise, the returned list may 
     *  contain only those workflow events that are accessible through this 
     *  session. 
     *
     *  @param  processId a process {@code Id} 
     *  @param  start start range 
     *  @param  end end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code processId, start, 
     *         } or {@code end} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForProcess(org.osid.id.Id processId, 
                                                                                 org.osid.calendaring.DateTime start, 
                                                                                 org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByDateForProcess(processId, start, end));
    }


    /**
     *  Gets the workflow log for a step. In plenary mode, the
     *  returned list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  stepId a step {@code Id} 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException {@code stepId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForStep(org.osid.id.Id stepId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsForStep(stepId));
    }

    
    /**
     *  Gets the workflow log for a step within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  stepId a step {@code Id} 
     *  @param  start start range 
     *  @param  end end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code stepId, start} or 
     *          {@code end} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForStep(org.osid.id.Id stepId, 
                                                                              org.osid.calendaring.DateTime start, 
                                                                              org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByDateForStep(stepId, start, end));
    }


    /**
     *  Gets the workflow log for a work. In plenary mode, the returned list 
     *  contains all known workflow events or an error results. Otherwise, the 
     *  returned list may contain only those workflow events that are 
     *  accessible through this session. 
     *
     *  @param  workId a work {@code Id} 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException {@code workId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWork(org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsForWork(workId));
    }

    
    /**
     *  Gets the workflow log for a work within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  workId a work {@code Id} 
     *  @param  start start range 
     *  @param  end end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code workId, start} or 
     *          {@code end} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWork(org.osid.id.Id workId, 
                                                                              org.osid.calendaring.DateTime start, 
                                                                              org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByDateForWork(workId, start, end));
    }


    /**
     *  Gets the workflow log for a work in a process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results.  Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  processId a process {@code Id} 
     *  @param  workId a work {@code Id} 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException {@code workId} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorkAndProcess(org.osid.id.Id processId, 
                                                                                  org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsForWorkAndProcess(processId, workId));
    }


    /**
     *  Gets the workflow log for a work in a process within the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  processId a process {@code Id} 
     *  @param  workId a work {@code Id} 
     *  @param  start start range 
     *  @param  end end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code workId, start} or 
     *          {@code end} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorkAndProcess(org.osid.id.Id processId, 
                                                                                        org.osid.id.Id workId, 
                                                                                        org.osid.calendaring.DateTime start, 
                                                                                        org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByDateForWorkAndProcess(processId, workId, start, end));
    }


    /**
     *  Gets the workflow log for a work in this process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results.  Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  stepId a step {@code Id} 
     *  @param  workId a work {@code Id} 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException {@code stepId} or {@code 
     *          workId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForStepAndWork(org.osid.id.Id stepId, 
                                                                               org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsForStepAndWork(stepId, workId));
    }


    /**
     *  Gets the workflow log for a work in this process within the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  stepId a step {@code Id} 
     *  @param  workId a work {@code Id} 
     *  @param  start start range 
     *  @param  end end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code stepId, workId,
     *          start} or {@code end} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForStepAndWork(org.osid.id.Id stepId, 
                                                                                     org.osid.id.Id workId, 
                                                                                     org.osid.calendaring.DateTime start, 
                                                                                     org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByDateForStepAndWork(stepId, workId, start, end));
    }

    
    /**
     *  Gets the workflow log by an agent in this process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results. Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsForWorker(resourceId));
    }


    /**
     *  Gets the workflow log by the resource in this process within
     *  the given date range inclusive. In plenary mode, the returned
     *  list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  start start range 
     *  @param  end end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          start}, or {@code end} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorker(org.osid.id.Id resourceId, 
                                                                                org.osid.calendaring.DateTime start, 
                                                                                org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByDateForWorker(resourceId, start, end));
    }


    /**
     *  Gets the workflow log by an agent in this process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results. Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  processId a process {@code Id} 
     *  @return the workflow events 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code processId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorkerAndProcess(org.osid.id.Id resourceId, 
                                                                                    org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsForWorkerAndProcess(resourceId, processId));
    }


    /**
     *  Gets the workflow log by the resource in this process within
     *  the given date range inclusive. In plenary mode, the returned
     *  list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  processId a process {@code Id} 
     *  @param  start start range 
     *  @param  end end range 
     *  @return the workflow events 
     *  @throws org.osid.InvalidArgumentException {@code start} is 
     *          greater than {@code end} 
     *  @throws org.osid.NullArgumentException {@code resourceId,
     *          processId, start} or {@code end} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorkerAndProcess(org.osid.id.Id resourceId, 
                                                                                          org.osid.id.Id processId, 
                                                                                          org.osid.calendaring.DateTime start, 
                                                                                          org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEventsByDateForWorkerAndProcess(resourceId, processId, start, end));
    }

    
    /**
     *  Gets all {@code WorkflowEvents}. 
     *
     *  In plenary mode, the returned list contains all known
     *  workflow events or an error results. Otherwise, the returned list
     *  may contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code WorkflowEvents} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getWorkflowEvents());
    }
}
