//
// AbstractAdapterIssueLookupSession.java
//
//    An Issue lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Issue lookup session adapter.
 */

public abstract class AbstractAdapterIssueLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.hold.IssueLookupSession {

    private final org.osid.hold.IssueLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterIssueLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterIssueLookupSession(org.osid.hold.IssueLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Oubliette/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Oubliette Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.session.getOublietteId());
    }


    /**
     *  Gets the {@code Oubliette} associated with this session.
     *
     *  @return the {@code Oubliette} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOubliette());
    }


    /**
     *  Tests if this user can perform {@code Issue} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupIssues() {
        return (this.session.canLookupIssues());
    }


    /**
     *  A complete view of the {@code Issue} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeIssueView() {
        this.session.useComparativeIssueView();
        return;
    }


    /**
     *  A complete view of the {@code Issue} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryIssueView() {
        this.session.usePlenaryIssueView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include issues in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.session.useFederatedOublietteView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        this.session.useIsolatedOublietteView();
        return;
    }
    
     
    /**
     *  Gets the {@code Issue} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Issue} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Issue} and
     *  retained for compatibility.
     *
     *  @param issueId {@code Id} of the {@code Issue}
     *  @return the issue
     *  @throws org.osid.NotFoundException {@code issueId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code issueId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Issue getIssue(org.osid.id.Id issueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssue(issueId));
    }


    /**
     *  Gets an {@code IssueList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  issues specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Issues} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  issueIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Issue} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code issueIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.IssueList getIssuesByIds(org.osid.id.IdList issueIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesByIds(issueIds));
    }


    /**
     *  Gets an {@code IssueList} corresponding to the given
     *  issue genus {@code Type} which does not include
     *  issues of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  issueGenusType an issue genus type 
     *  @return the returned {@code Issue} list
     *  @throws org.osid.NullArgumentException
     *          {@code issueGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.IssueList getIssuesByGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesByGenusType(issueGenusType));
    }


    /**
     *  Gets an {@code IssueList} corresponding to the given
     *  issue genus {@code Type} and include any additional
     *  issues with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  issueGenusType an issue genus type 
     *  @return the returned {@code Issue} list
     *  @throws org.osid.NullArgumentException
     *          {@code issueGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.IssueList getIssuesByParentGenusType(org.osid.type.Type issueGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesByParentGenusType(issueGenusType));
    }


    /**
     *  Gets an {@code IssueList} containing the given
     *  issue record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  issueRecordType an issue record type 
     *  @return the returned {@code Issue} list
     *  @throws org.osid.NullArgumentException
     *          {@code issueRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.IssueList getIssuesByRecordType(org.osid.type.Type issueRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssuesByRecordType(issueRecordType));
    }


    /**
     *  Gets all {@code Issues}. 
     *
     *  In plenary mode, the returned list contains all known
     *  issues or an error results. Otherwise, the returned list
     *  may contain only those issues that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Issues} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.IssueList getIssues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getIssues());
    }
}
