//
// AbstractIndexedMapSignalLookupSession.java
//
//    A simple framework for providing a Signal lookup service
//    backed by a fixed collection of signals with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Signal lookup service backed by a
 *  fixed collection of signals. The signals are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some signals may be compatible
 *  with more types than are indicated through these signal
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Signals</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSignalLookupSession
    extends AbstractMapSignalLookupSession
    implements org.osid.mapping.path.SignalLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.Signal> signalsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.Signal>());
    private final MultiMap<org.osid.type.Type, org.osid.mapping.path.Signal> signalsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.mapping.path.Signal>());


    /**
     *  Makes a <code>Signal</code> available in this session.
     *
     *  @param  signal a signal
     *  @throws org.osid.NullArgumentException <code>signal<code> is
     *          <code>null</code>
     */

    @Override
    protected void putSignal(org.osid.mapping.path.Signal signal) {
        super.putSignal(signal);

        this.signalsByGenus.put(signal.getGenusType(), signal);
        
        try (org.osid.type.TypeList types = signal.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.signalsByRecord.put(types.getNextType(), signal);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a signal from this session.
     *
     *  @param signalId the <code>Id</code> of the signal
     *  @throws org.osid.NullArgumentException <code>signalId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeSignal(org.osid.id.Id signalId) {
        org.osid.mapping.path.Signal signal;
        try {
            signal = getSignal(signalId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.signalsByGenus.remove(signal.getGenusType());

        try (org.osid.type.TypeList types = signal.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.signalsByRecord.remove(types.getNextType(), signal);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeSignal(signalId);
        return;
    }


    /**
     *  Gets a <code>SignalList</code> corresponding to the given
     *  signal genus <code>Type</code> which does not include
     *  signals of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known signals or an error results. Otherwise,
     *  the returned list may contain only those signals that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  signalGenusType a signal genus type 
     *  @return the returned <code>Signal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByGenusType(org.osid.type.Type signalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.signal.ArraySignalList(this.signalsByGenus.get(signalGenusType)));
    }


    /**
     *  Gets a <code>SignalList</code> containing the given
     *  signal record <code>Type</code>. In plenary mode, the
     *  returned list contains all known signals or an error
     *  results. Otherwise, the returned list may contain only those
     *  signals that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  signalRecordType a signal record type 
     *  @return the returned <code>signal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>signalRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalList getSignalsByRecordType(org.osid.type.Type signalRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.mapping.path.signal.ArraySignalList(this.signalsByRecord.get(signalRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.signalsByGenus.clear();
        this.signalsByRecord.clear();

        super.close();

        return;
    }
}
