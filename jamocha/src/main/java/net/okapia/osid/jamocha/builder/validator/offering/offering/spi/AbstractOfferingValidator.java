//
// AbstractOfferingValidator.java
//
//     Validates an Offering.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.offering.offering.spi;


/**
 *  Validates an Offering.
 */

public abstract class AbstractOfferingValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractOfferingValidator</code>.
     */

    protected AbstractOfferingValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractOfferingValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractOfferingValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an Offering.
     *
     *  @param offering an offering to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>offering</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.offering.Offering offering) {
        super.validate(offering);

        testNestedObject(offering, "getCanonicalUnit");
        testNestedObject(offering, "getTimePeriod");
        test(offering.getTitle(), "getTitle()");
        test(offering.getCode(), "getCode()");

        testConditionalMethod(offering, "getResultOptionIds", offering.hasResults(), "hasResults()");
        testConditionalMethod(offering, "getResultOptions", offering.hasResults(), "hasResults()");
        if (offering.hasResults()) {
            testNestedObjects(offering, "getResultOptionIds", "getResultOptions");
        }

        testConditionalMethod(offering, "getSponsorIds", offering.hasSponsors(), "hasSponsors()");
        testConditionalMethod(offering, "getSponsors", offering.hasSponsors(), "hasSponsors()");
        if (offering.hasSponsors()) {
            testNestedObjects(offering, "getSponsorIds", "getSponsors");
        }

        testNestedObjects(offering, "getScheduleIds", "getSchedules");

        return;
    }
}
