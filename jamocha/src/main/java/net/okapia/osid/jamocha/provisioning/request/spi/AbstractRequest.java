//
// AbstractRequest.java
//
//     Defines a Request.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Request</code>.
 */

public abstract class AbstractRequest
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.provisioning.Request {

    private org.osid.provisioning.RequestTransaction requestTransaction;
    private org.osid.provisioning.Queue queue;
    private org.osid.calendaring.DateTime requestDate;
    private org.osid.resource.Resource requester;
    private org.osid.authentication.Agent requestingAgent;
    private org.osid.provisioning.Pool pool;

    private final java.util.Collection<org.osid.provisioning.Provisionable> requestedProvisionables = new java.util.LinkedHashSet<>();

    private org.osid.provisioning.Provision exchangeProvision;
    private org.osid.provisioning.Provision originProvision;
    private long position;
    private org.osid.calendaring.Duration ewa;

    private final java.util.Collection<org.osid.provisioning.records.RequestRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the transaction in which this request is 
     *  a part. Requests can be made individually or as part of a transaction 
     *  group to provide an atomic compound request. 
     *
     *  @return the request transaction <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRequestTransactionId() {
        return (this.requestTransaction.getId());
    }


    /**
     *  Gets the <code> Id </code> of the transaction in which this request is 
     *  a part. Requests can be made individually or as part of a transaction 
     *  group to provide an atomic compound request. 
     *
     *  @return the request transaction 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransaction getRequestTransaction()
        throws org.osid.OperationFailedException {

        return (this.requestTransaction);
    }


    /**
     *  Sets the request transaction.
     *
     *  @param requestTransaction a request transaction
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransaction</code> is <code>null</code>
     */

    protected void setRequestTransaction(org.osid.provisioning.RequestTransaction requestTransaction) {
        nullarg(requestTransaction, "request transaction");
        this.requestTransaction = requestTransaction;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the queue. 
     *
     *  @return the queue <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQueueId() {
        return (this.queue.getId());
    }


    /**
     *  Gets the queue. 
     *
     *  @return the queue 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Queue getQueue()
        throws org.osid.OperationFailedException {

        return (this.queue);
    }


    /**
     *  Sets the queue.
     *
     *  @param queue a queue
     *  @throws org.osid.NullArgumentException
     *          <code>queue</code> is <code>null</code>
     */

    protected void setQueue(org.osid.provisioning.Queue queue) {
        nullarg(queue, "queue");
        this.queue = queue;
        return;
    }


    /**
     *  Gets the date of the request. 
     *
     *  @return the request date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getRequestDate() {
        return (this.requestDate);
    }


    /**
     *  Sets the request date.
     *
     *  @param date a request date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setRequestDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "request date");
        this.requestDate = date;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the queued resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRequesterId() {
        return (this.requester.getId());
    }


    /**
     *  Gets the queued resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getRequester()
        throws org.osid.OperationFailedException {

        return (this.requester);
    }


    /**
     *  Sets the requester.
     *
     *  @param requester a requester
     *  @throws org.osid.NullArgumentException
     *          <code>requester</code> is <code>null</code>
     */

    protected void setRequester(org.osid.resource.Resource requester) {
        nullarg(requester, "requester");
        this.requester = requester;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the requesting agent. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRequestingAgentId() {
        return (this.requestingAgent.getId());
    }


    /**
     *  Gets the requesting agent. 
     *
     *  @return the requesting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getRequestingAgent()
        throws org.osid.OperationFailedException {

        return (this.requestingAgent);
    }


    /**
     *  Sets the requesting agent.
     *
     *  @param agent a requesting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setRequestingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "requesting agent");
        this.requestingAgent = agent;
        return;
    }


    /**
     *  Tests if this request is qualified by a pool. An unqualified
     *  request may be allocated out of any pool in the broker.
     *
     *  @return <code>true</code> if this request has a pool,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean hasPool() {
        return (this.pool != null);
    }


    /**
     *  Gets the <code> Id </code> of the pool. 
     *
     *  @return the pool <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasPool() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPoolId() {
        if (!hasPool()) {
            throw new org.osid.IllegalStateException("hasPool() is false");
        }

        return (this.pool.getId());
    }


    /**
     *  Gets the pool. 
     *
     *  @return the pool 
     *  @throws org.osid.IllegalStateException <code> hasPool() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Pool getPool()
        throws org.osid.OperationFailedException {

        if (!hasPool()) {
            throw new org.osid.IllegalStateException("hasPool() is false");
        }

        return (this.pool);
    }


    /**
     *  Sets the pool.
     *
     *  @param pool a pool
     *  @throws org.osid.NullArgumentException
     *          <code>pool</code> is <code>null</code>
     */

    protected void setPool(org.osid.provisioning.Pool pool) {
        nullarg(pool, "pool");
        this.pool = pool;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the requested provisionables. 
     *
     *  @return the requested provisionable <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getRequestedProvisionableIds() {
        try {
            org.osid.provisioning.ProvisionableList requestedProvisionables = getRequestedProvisionables();
            return (new net.okapia.osid.jamocha.adapter.converter.provisioning.provisionable.ProvisionableToIdList(requestedProvisionables));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the requested provisionables. 
     *
     *  @return the requested provisionables 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getRequestedProvisionables()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.provisioning.provisionable.ArrayProvisionableList(this.requestedProvisionables));
    }


    /**
     *  Adds a requested provisionable.
     *
     *  @param provisionable a requested provisionable
     *  @throws org.osid.NullArgumentException
     *          <code>provisionable</code> is <code>null</code>
     */

    protected void addRequestedProvisionable(org.osid.provisioning.Provisionable provisionable) {
        nullarg(provisionable, "requested provisionable");
        this.requestedProvisionables.add(provisionable);
        return;
    }


    /**
     *  Sets all the requested provisionables.
     *
     *  @param provisionables a collection of requested provisionables
     *  @throws org.osid.NullArgumentException
     *          <code>provisionables</code> is <code>null</code>
     */

    protected void setRequestedProvisionables(java.util.Collection<org.osid.provisioning.Provisionable> provisionables) {
        nullarg(provisionables, "requested provisionables");
        this.requestedProvisionables.clear();
        this.requestedProvisionables.addAll(provisionables);
        return;
    }


    /**
     *  Tests if this request was created to exchange a provision. 
     *
     *  @return <code>true</code> if this request is an exchange
     *          request, <code>false</code> otherwise
     */

    @OSID @Override
    public boolean isExchange() {
        return (this.exchangeProvision != null);
    }


    /**
     *  Gets the <code> Id </code> of the provision to be exchanged if this 
     *  request is provisioned. 
     *
     *  @return the exchange provision <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isExchange() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getExchangeProvisionId() {
        if (!isExchange()) {
            throw new org.osid.IllegalStateException("isExchange() is false");
        }

        return (this.exchangeProvision.getId());
    }


    /**
     *  Gets the provision to be exchanged if this request is provisioned. 
     *
     *  @return the exchange provision 
     *  @throws org.osid.IllegalStateException <code> isExchange()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getExchangeProvision()
        throws org.osid.OperationFailedException {

        if (!isExchange()) {
            throw new org.osid.IllegalStateException("isExchange() is false");
        }

        return (this.exchangeProvision);
    }


    /**
     *  Sets the exchange provision.
     *
     *  @param provision an exchange provision
     *  @throws org.osid.NullArgumentException <code>provision</code>
     *          is <code>null</code>
     */

    protected void setExchangeProvision(org.osid.provisioning.Provision provision) {
        nullarg(provision, "exchange provision");
        this.exchangeProvision = exchangeProvision;
        return;
    }


    /**
     *  Tests if this request was created as a result of another
     *  provision.
     *
     *  @return <code>true</code> if this request is a provision
     *          result, <code>false</code> otherwise
     */

    @OSID @Override
    public boolean isProvisionResult() {
        return (this.originProvision != null);
    }


    /**
     *  Gets the <code> Id </code> of the provision that resulted in this 
     *  request. 
     *
     *  @return the origin provision <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isProvisionResult() 
     *          </code> is f <code> alse </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOriginProvisionId() {
        if (!isProvisionResult()) {
            throw new org.osid.IllegalStateException("isProvisionResult() is false");
        }

        return (this.originProvision.getId());
    }


    /**
     *  Gets the provision that resulted in this request. 
     *
     *  @return the origin provision 
     *  @throws org.osid.IllegalStateException <code> isProvisionResult() 
     *          </code> is <code>false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getOriginProvision()
        throws org.osid.OperationFailedException {

        if (!isProvisionResult()) {
            throw new org.osid.IllegalStateException("isProvisionResult() is false");
        }

        return (this.originProvision);
    }


    /**
     *  Sets the origin provision.
     *
     *  @param provision an origin provision
     *  @throws org.osid.NullArgumentException
     *          <code>provision</code> is <code>null</code>
     */

    protected void setOriginProvision(org.osid.provisioning.Provision provision) {
        nullarg(provision, "origin provision");
        this.originProvision = provision;
        return;
    }


    /**
     *  Tests if this request has a position in the queue. A position may be 
     *  indicate the rank of the request among waiting requests for the same 
     *  pool. 
     *
     *  @return true if this request has a position, false otherwise 
     */

    @OSID @Override
    public boolean hasPosition() {
        if (this.position < 0) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the position of this request in the queue. A position may
     *  be indicate the rank of the request among waiting request for
     *  the same pool.
     *
     *  @return the position 
     *  @throws org.osid.IllegalStateException <code> hasPosition() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getPosition() {
        if (!hasPosition()) {
            throw new org.osid.IllegalStateException("hasPosition() is false");
        }

        return (this.position);
    }


    /**
     *  Sets the position.
     *
     *  @param position a position
     *  @throws org.osid.InvalidArgumentException
     *          <code>position</code> is negative
     */

    protected void setPosition(long position) {
        cardinalarg(position, "position");
        this.position = position;
        return;
    }


    /**
     *  Tests if there is an estimated waiting time for a provision
     *  out of the pool.
     *
     *  @return true if this request has an estimated time, false otherwise 
     */

    @OSID @Override
    public boolean hasEWA() {
        return (this.ewa != null);
    }


    /**
     *  Gets the estimated waiting time for a provision out of the pool. 
     *
     *  @return the estimated waiting time 
     *  @throws org.osid.IllegalStateException <code> hasEWA() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getEWA() {
        if (!hasEWA()) {
            throw new org.osid.IllegalStateException("hasEWA() is false");
        }

        return (this.ewa);
    }


    /**
     *  Sets the estimated wait time.
     *
     *  @param ewa the estimated wait time
     *  @throws org.osid.NullArgumentException <code>ewa</code> is
     *          <code>null</code>
     */

    protected void setEWA(org.osid.calendaring.Duration ewa) {
        nullarg(ewa, "estimated wait time");
        this.ewa = ewa;
        return;
    }


    /**
     *  Tests if this request supports the given record
     *  <code>Type</code>.
     *
     *  @param  requestRecordType a request record type 
     *  @return <code>true</code> if the requestRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type requestRecordType) {
        for (org.osid.provisioning.records.RequestRecord record : this.records) {
            if (record.implementsRecordType(requestRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Requst</code>
     *  record <code>Type</code>.
     *
     *  @param  requestRecordType the request record type 
     *  @return the request record 
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestRecord getRequestRecord(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestRecord record : this.records) {
            if (record.implementsRecordType(requestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestRecordType + " is not supported");
    }


    /**
     *  Adds a record to this request. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param requestRecord the request record
     *  @param requestRecordType request record type
     *  @throws org.osid.NullArgumentException
     *          <code>requestRecord</code> or
     *          <code>requestRecordTyperequest</code> is
     *          <code>null</code>
     */
            
    protected void addRequestRecord(org.osid.provisioning.records.RequestRecord requestRecord, 
                                    org.osid.type.Type requestRecordType) {
        
        nullarg(requestRecord, "request record");
        addRecordType(requestRecordType);
        this.records.add(requestRecord);
        
        return;
    }
}
