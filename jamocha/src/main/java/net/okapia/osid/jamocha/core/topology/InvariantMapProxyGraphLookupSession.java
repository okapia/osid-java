//
// InvariantMapProxyGraphLookupSession
//
//    Implements a Graph lookup service backed by a fixed
//    collection of graphs. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology;


/**
 *  Implements a Graph lookup service backed by a fixed
 *  collection of graphs. The graphs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyGraphLookupSession
    extends net.okapia.osid.jamocha.core.topology.spi.AbstractMapGraphLookupSession
    implements org.osid.topology.GraphLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyGraphLookupSession} with no
     *  graphs.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyGraphLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyGraphLookupSession} with a
     *  single graph.
     *
     *  @param graph a single graph
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyGraphLookupSession(org.osid.topology.Graph graph, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGraph(graph);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyGraphLookupSession} using
     *  an array of graphs.
     *
     *  @param graphs an array of graphs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graphs} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyGraphLookupSession(org.osid.topology.Graph[] graphs, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGraphs(graphs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyGraphLookupSession} using a
     *  collection of graphs.
     *
     *  @param graphs a collection of graphs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graphs} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyGraphLookupSession(java.util.Collection<? extends org.osid.topology.Graph> graphs,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGraphs(graphs);
        return;
    }
}
