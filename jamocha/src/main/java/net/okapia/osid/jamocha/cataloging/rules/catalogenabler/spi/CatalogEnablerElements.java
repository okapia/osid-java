//
// CatalogEnablerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.rules.catalogenabler.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CatalogEnablerElements
    extends net.okapia.osid.jamocha.spi.OsidEnablerElements {


    /**
     *  Gets the CatalogEnablerElement Id.
     *
     *  @return the catalog enabler element Id
     */

    public static org.osid.id.Id getCatalogEnablerEntityId() {
        return (makeEntityId("osid.cataloging.rules.CatalogEnabler"));
    }


    /**
     *  Gets the RuledCatalogId element Id.
     *
     *  @return the RuledCatalogId element Id
     */

    public static org.osid.id.Id getRuledCatalogId() {
        return (makeQueryElementId("osid.cataloging.rules.catalogenabler.RuledCatalogId"));
    }


    /**
     *  Gets the RuledCatalog element Id.
     *
     *  @return the RuledCatalog element Id
     */

    public static org.osid.id.Id getRuledCatalog() {
        return (makeQueryElementId("osid.cataloging.rules.catalogenabler.RuledCatalog"));
    }


    /**
     *  Gets the CatalogId element Id.
     *
     *  @return the CatalogId element Id
     */

    public static org.osid.id.Id getCatalogId() {
        return (makeQueryElementId("osid.cataloging.rules.catalogenabler.CatalogId"));
    }


    /**
     *  Gets the Catalog element Id.
     *
     *  @return the Catalog element Id
     */

    public static org.osid.id.Id getCatalog() {
        return (makeQueryElementId("osid.cataloging.rules.catalogenabler.Catalog"));
    }
}
