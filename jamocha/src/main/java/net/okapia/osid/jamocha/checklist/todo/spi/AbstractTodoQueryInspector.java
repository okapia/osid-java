//
// AbstractTodoQueryInspector.java
//
//     A template for making a TodoQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.todo.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for todos.
 */

public abstract class AbstractTodoQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.checklist.TodoQueryInspector {

    private final java.util.Collection<org.osid.checklist.records.TodoQueryInspectorRecord> records = new java.util.ArrayList<>();

    private final OsidContainableQueryInspector inspector = new OsidContainableQueryInspector();


    /**
     *  Gets the sequestered query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSequesteredTerms() {
        return (this.inspector.getSequesteredTerms());
    }

    
    /**
     *  Gets the complete terms. 
     *
     *  @return the completed query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the priority terms. 
     *
     *  @return the priority query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getPriorityTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the minimum priority terms. 
     *
     *  @return the priority query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getMinimumPriorityTerms() {
        return (new org.osid.search.terms.TypeTerm[0]);
    }


    /**
     *  Gets the due date terms. 
     *
     *  @return the date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDueDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the dependency <code> Id </code> terms. 
     *
     *  @return the todo query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDependencyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the dependency terms. 
     *
     *  @return the todo query terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getDependencyTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the ancestor todo <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorTodoIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor todo query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getAncestorTodoTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the descendant todo <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantTodoIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant todo query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getDescendantTodoTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the checklist <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getChecklistIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the checklist query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQueryInspector[] getChecklistTerms() {
        return (new org.osid.checklist.ChecklistQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given todo query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a todo implementing the requested record.
     *
     *  @param todoRecordType a todo record type
     *  @return the todo query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.TodoQueryInspectorRecord getTodoQueryInspectorRecord(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.TodoQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(todoRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoRecordType + " is not supported");
    }


    /**
     *  Adds a record to this todo query. 
     *
     *  @param todoQueryInspectorRecord todo query inspector
     *         record
     *  @param todoRecordType todo record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTodoQueryInspectorRecord(org.osid.checklist.records.TodoQueryInspectorRecord todoQueryInspectorRecord, 
                                                   org.osid.type.Type todoRecordType) {

        addRecordType(todoRecordType);
        nullarg(todoRecordType, "todo record type");
        this.records.add(todoQueryInspectorRecord);        
        return;
    }


    protected class OsidContainableQueryInspector
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQueryInspector
        implements org.osid.OsidContainableQueryInspector {
    }
}
