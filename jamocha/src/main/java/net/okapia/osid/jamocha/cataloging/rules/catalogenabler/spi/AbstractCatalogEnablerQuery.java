//
// AbstractCatalogEnablerQuery.java
//
//     A template for making a CatalogEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.rules.catalogenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for catalog enablers.
 */

public abstract class AbstractCatalogEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.cataloging.rules.CatalogEnablerQuery {

    private final java.util.Collection<org.osid.cataloging.rules.records.CatalogEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the catalog. 
     *
     *  @param  catalogId the catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCatalogId(org.osid.id.Id catalogId, boolean match) {
        return;
    }


    /**
     *  Clears the catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalog. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuery getRuledCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCatalogQuery() is false");
    }


    /**
     *  Matches enablers mapped to any catalog. 
     *
     *  @param  match <code> true </code> for enablers mapped to any catalog, 
     *          <code> false </code> to match enablers mapped to no catalog 
     */

    @OSID @Override
    public void matchAnyRuledCatalog(boolean match) {
        return;
    }


    /**
     *  Clears the catalog query terms. 
     */

    @OSID @Override
    public void clearRuledCatalogTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the catalog. 
     *
     *  @param  catalogId the catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogId(org.osid.id.Id catalogId, boolean match) {
        return;
    }


    /**
     *  Clears the catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalog. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalog query 
     *  @throws org.osid.UnimplementedException <code> supportsCatalogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuery getCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogQuery() is false");
    }


    /**
     *  Clears the catalog query terms. 
     */

    @OSID @Override
    public void clearCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given catalog enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a catalog enabler implementing the requested record.
     *
     *  @param catalogEnablerRecordType a catalog enabler record type
     *  @return the catalog enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.rules.records.CatalogEnablerQueryRecord getCatalogEnablerQueryRecord(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.rules.records.CatalogEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(catalogEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this catalog enabler query. 
     *
     *  @param catalogEnablerQueryRecord catalog enabler query record
     *  @param catalogEnablerRecordType catalogEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCatalogEnablerQueryRecord(org.osid.cataloging.rules.records.CatalogEnablerQueryRecord catalogEnablerQueryRecord, 
                                          org.osid.type.Type catalogEnablerRecordType) {

        addRecordType(catalogEnablerRecordType);
        nullarg(catalogEnablerQueryRecord, "catalog enabler query record");
        this.records.add(catalogEnablerQueryRecord);        
        return;
    }
}
