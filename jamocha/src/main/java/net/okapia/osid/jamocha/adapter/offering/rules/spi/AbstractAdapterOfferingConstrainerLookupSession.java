//
// AbstractAdapterOfferingConstrainerLookupSession.java
//
//    An OfferingConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OfferingConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterOfferingConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.offering.rules.OfferingConstrainerLookupSession {

    private final org.osid.offering.rules.OfferingConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOfferingConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOfferingConstrainerLookupSession(org.osid.offering.rules.OfferingConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Catalogue/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Catalogue Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.session.getCatalogueId());
    }


    /**
     *  Gets the {@code Catalogue} associated with this session.
     *
     *  @return the {@code Catalogue} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCatalogue());
    }


    /**
     *  Tests if this user can perform {@code OfferingConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOfferingConstrainers() {
        return (this.session.canLookupOfferingConstrainers());
    }


    /**
     *  A complete view of the {@code OfferingConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOfferingConstrainerView() {
        this.session.useComparativeOfferingConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code OfferingConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOfferingConstrainerView() {
        this.session.usePlenaryOfferingConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offering constrainers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.session.useFederatedCatalogueView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.session.useIsolatedCatalogueView();
        return;
    }
    

    /**
     *  Only active offering constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveOfferingConstrainerView() {
        this.session.useActiveOfferingConstrainerView();
        return;
    }


    /**
     *  Active and inactive offering constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusOfferingConstrainerView() {
        this.session.useAnyStatusOfferingConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code OfferingConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code OfferingConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code OfferingConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  @param offeringConstrainerId {@code Id} of the {@code OfferingConstrainer}
     *  @return the offering constrainer
     *  @throws org.osid.NotFoundException {@code offeringConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code offeringConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainer getOfferingConstrainer(org.osid.id.Id offeringConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainer(offeringConstrainerId));
    }


    /**
     *  Gets an {@code OfferingConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offeringConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code OfferingConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  @param  offeringConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code OfferingConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByIds(org.osid.id.IdList offeringConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainersByIds(offeringConstrainerIds));
    }


    /**
     *  Gets an {@code OfferingConstrainerList} corresponding to the given
     *  offering constrainer genus {@code Type} which does not include
     *  offering constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  @param  offeringConstrainerGenusType an offeringConstrainer genus type 
     *  @return the returned {@code OfferingConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByGenusType(org.osid.type.Type offeringConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainersByGenusType(offeringConstrainerGenusType));
    }


    /**
     *  Gets an {@code OfferingConstrainerList} corresponding to the given
     *  offering constrainer genus {@code Type} and include any additional
     *  offering constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  @param  offeringConstrainerGenusType an offeringConstrainer genus type 
     *  @return the returned {@code OfferingConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByParentGenusType(org.osid.type.Type offeringConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainersByParentGenusType(offeringConstrainerGenusType));
    }


    /**
     *  Gets an {@code OfferingConstrainerList} containing the given
     *  offering constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offering constrainers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  @param  offeringConstrainerRecordType an offeringConstrainer record type 
     *  @return the returned {@code OfferingConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code offeringConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByRecordType(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainersByRecordType(offeringConstrainerRecordType));
    }


    /**
     *  Gets all {@code OfferingConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  @return a list of {@code OfferingConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOfferingConstrainers());
    }
}
