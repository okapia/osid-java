//
// AbstractAssemblyQueueConstrainerEnablerQuery.java
//
//     A QueueConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.tracking.rules.queueconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A QueueConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyQueueConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.tracking.rules.QueueConstrainerEnablerQuery,
               org.osid.tracking.rules.QueueConstrainerEnablerQueryInspector,
               org.osid.tracking.rules.QueueConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.tracking.rules.records.QueueConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.rules.records.QueueConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.rules.records.QueueConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyQueueConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyQueueConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the queue constrainer. 
     *
     *  @param  queueConstrainerId the queue constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledQueueConstrainerId(org.osid.id.Id queueConstrainerId, 
                                             boolean match) {
        getAssembler().addIdTerm(getRuledQueueConstrainerIdColumn(), queueConstrainerId, match);
        return;
    }


    /**
     *  Clears the queue constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledQueueConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledQueueConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the queue constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledQueueConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledQueueConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledQueueConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledQueueConstrainerIdColumn() {
        return ("ruled_queue_constrainer_id");
    }


    /**
     *  Tests if a <code> QueueConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledQueueConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the queue constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledQueueConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQuery getRuledQueueConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledQueueConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any queue constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any queue 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no queue constrainers 
     */

    @OSID @Override
    public void matchAnyRuledQueueConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledQueueConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the queue constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledQueueConstrainerTerms() {
        getAssembler().clearTerms(getRuledQueueConstrainerColumn());
        return;
    }


    /**
     *  Gets the queue constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueConstrainerQueryInspector[] getRuledQueueConstrainerTerms() {
        return (new org.osid.tracking.rules.QueueConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledQueueConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledQueueConstrainerColumn() {
        return ("ruled_queue_constrainer");
    }


    /**
     *  Matches enablers mapped to the front office. 
     *
     *  @param  frontOfficeId the front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFrontOfficeId(org.osid.id.Id frontOfficeId, boolean match) {
        getAssembler().addIdTerm(getFrontOfficeIdColumn(), frontOfficeId, match);
        return;
    }


    /**
     *  Clears the front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeIdTerms() {
        getAssembler().clearTerms(getFrontOfficeIdColumn());
        return;
    }


    /**
     *  Gets the front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFrontOfficeIdTerms() {
        return (getAssembler().getIdTerms(getFrontOfficeIdColumn()));
    }


    /**
     *  Gets the FrontOfficeId column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeIdColumn() {
        return ("front_office_id");
    }


    /**
     *  Tests if an <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a front office. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsFrontOfficeQuery() is false");
    }


    /**
     *  Clears the front office query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeTerms() {
        getAssembler().clearTerms(getFrontOfficeColumn());
        return;
    }


    /**
     *  Gets the front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }


    /**
     *  Gets the FrontOffice column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeColumn() {
        return ("front_office");
    }


    /**
     *  Tests if this queueConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  queueConstrainerEnablerRecordType a queue constrainer enabler record type 
     *  @return <code>true</code> if the queueConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type queueConstrainerEnablerRecordType) {
        for (org.osid.tracking.rules.records.QueueConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  queueConstrainerEnablerRecordType the queue constrainer enabler record type 
     *  @return the queue constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueConstrainerEnablerQueryRecord getQueueConstrainerEnablerQueryRecord(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  queueConstrainerEnablerRecordType the queue constrainer enabler record type 
     *  @return the queue constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueConstrainerEnablerQueryInspectorRecord getQueueConstrainerEnablerQueryInspectorRecord(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(queueConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param queueConstrainerEnablerRecordType the queue constrainer enabler record type
     *  @return the queue constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueConstrainerEnablerSearchOrderRecord getQueueConstrainerEnablerSearchOrderRecord(org.osid.type.Type queueConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(queueConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this queue constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param queueConstrainerEnablerQueryRecord the queue constrainer enabler query record
     *  @param queueConstrainerEnablerQueryInspectorRecord the queue constrainer enabler query inspector
     *         record
     *  @param queueConstrainerEnablerSearchOrderRecord the queue constrainer enabler search order record
     *  @param queueConstrainerEnablerRecordType queue constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerEnablerQueryRecord</code>,
     *          <code>queueConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>queueConstrainerEnablerSearchOrderRecord</code> or
     *          <code>queueConstrainerEnablerRecordTypequeueConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addQueueConstrainerEnablerRecords(org.osid.tracking.rules.records.QueueConstrainerEnablerQueryRecord queueConstrainerEnablerQueryRecord, 
                                      org.osid.tracking.rules.records.QueueConstrainerEnablerQueryInspectorRecord queueConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.tracking.rules.records.QueueConstrainerEnablerSearchOrderRecord queueConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type queueConstrainerEnablerRecordType) {

        addRecordType(queueConstrainerEnablerRecordType);

        nullarg(queueConstrainerEnablerQueryRecord, "queue constrainer enabler query record");
        nullarg(queueConstrainerEnablerQueryInspectorRecord, "queue constrainer enabler query inspector record");
        nullarg(queueConstrainerEnablerSearchOrderRecord, "queue constrainer enabler search odrer record");

        this.queryRecords.add(queueConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(queueConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(queueConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
