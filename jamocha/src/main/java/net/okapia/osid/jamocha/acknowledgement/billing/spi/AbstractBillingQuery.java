//
// AbstractBillingQuery.java
//
//     A template for making a Billing Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for billings.
 */

public abstract class AbstractBillingQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.acknowledgement.BillingQuery {

    private final java.util.Collection<org.osid.acknowledgement.records.BillingQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the credit <code> Id </code> for this query to match credits 
     *  assigned to billings. 
     *
     *  @param  creditId a credit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> creditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCreditId(org.osid.id.Id creditId, boolean match) {
        return;
    }


    /**
     *  Clears all credit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCreditIdTerms() {
        return;
    }


    /**
     *  Tests if a credit query is available. 
     *
     *  @return <code> true </code> if a credit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing. 
     *
     *  @return the credit query 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuery getCreditQuery() {
        throw new org.osid.UnimplementedException("supportsCreditQuery() is false");
    }


    /**
     *  Matches billings with any credit. 
     *
     *  @param  match <code> true </code> to match billings with any credit, 
     *          <code> false </code> to match billings with no credits 
     */

    @OSID @Override
    public void matchAnyCredit(boolean match) {
        return;
    }


    /**
     *  Clears all credit terms. 
     */

    @OSID @Override
    public void clearCreditTerms() {
        return;
    }


    /**
     *  Sets the billing <code> Id </code> for this query to match billings 
     *  that have the specified billing as an ancestor. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorBillingId(org.osid.id.Id billingId, boolean match) {
        return;
    }


    /**
     *  Clears all ancestor billing <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBillingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BillingQuery </code> is available. 
     *
     *  @return <code> true </code> if a billing query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBillingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the billing query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBillingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuery getAncestorBillingQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBillingQuery() is false");
    }


    /**
     *  Matches billings with any ancestor. 
     *
     *  @param  match <code> true </code> to match billings with any ancestor, 
     *          <code> false </code> to match root billings 
     */

    @OSID @Override
    public void matchAnyAncestorBilling(boolean match) {
        return;
    }


    /**
     *  Clears all ancestor billing terms. 
     */

    @OSID @Override
    public void clearAncestorBillingTerms() {
        return;
    }


    /**
     *  Sets the billing <code> Id </code> for this query to match billings 
     *  that have the specified billing as a descendant. 
     *
     *  @param  billingId a billing <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> billingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantBillingId(org.osid.id.Id billingId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears all descendant billing <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBillingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BillingQuery </code> is available. 
     *
     *  @return <code> true </code> if a billing query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBillingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the billing query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBillingQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingQuery getDescendantBillingQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBillingQuery() is false");
    }


    /**
     *  Matches billings with any descendant. 
     *
     *  @param  match <code> true </code> to match billings with any 
     *          descendant, <code> false </code> to match leaf billings 
     */

    @OSID @Override
    public void matchAnyDescendantBilling(boolean match) {
        return;
    }


    /**
     *  Clears all descendant billing terms. 
     */

    @OSID @Override
    public void clearDescendantBillingTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given billing query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a billing implementing the requested record.
     *
     *  @param billingRecordType a billing record type
     *  @return the billing query record
     *  @throws org.osid.NullArgumentException
     *          <code>billingRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(billingRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.BillingQueryRecord getBillingQueryRecord(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.acknowledgement.records.BillingQueryRecord record : this.records) {
            if (record.implementsRecordType(billingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(billingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this billing query. 
     *
     *  @param billingQueryRecord billing query record
     *  @param billingRecordType billing record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBillingQueryRecord(org.osid.acknowledgement.records.BillingQueryRecord billingQueryRecord, 
                                          org.osid.type.Type billingRecordType) {

        addRecordType(billingRecordType);
        nullarg(billingQueryRecord, "billing query record");
        this.records.add(billingQueryRecord);        
        return;
    }
}
