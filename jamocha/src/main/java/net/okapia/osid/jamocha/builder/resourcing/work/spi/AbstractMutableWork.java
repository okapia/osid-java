//
// AbstractMutableWork.java
//
//     Defines a mutable Work.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.work.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Work</code>.
 */

public abstract class AbstractMutableWork
    extends net.okapia.osid.jamocha.resourcing.work.spi.AbstractWork
    implements org.osid.resourcing.Work,
               net.okapia.osid.jamocha.builder.resourcing.work.WorkMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this work. 
     *
     *  @param record work record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addWorkRecord(org.osid.resourcing.records.WorkRecord record, org.osid.type.Type recordType) {
        super.addWorkRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this work.
     *
     *  @param displayName the name for this work
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this work.
     *
     *  @param description the description of this work
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the job.
     *
     *  @param job a job
     *  @throws org.osid.NullArgumentException <code>job</code> is
     *          <code>null</code>
     */

    @Override
    public void setJob(org.osid.resourcing.Job job) {
        super.setJob(job);
        return;
    }


    /**
     *  Adds a competency.
     *
     *  @param competency a competency
     *  @throws org.osid.NullArgumentException <code>competency</code>
     *          is <code>null</code>
     */

    @Override
    public void addCompetency(org.osid.resourcing.Competency competency) {
        super.addCompetency(competency);
        return;
    }


    /**
     *  Sets all the competencies.
     *
     *  @param competencies a collection of competencies
     *  @throws org.osid.NullArgumentException
     *          <code>competencies</code> is <code>null</code>
     */

    @Override
    public void setCompetencies(java.util.Collection<org.osid.resourcing.Competency> competencies) {
        super.setCompetencies(competencies);
        return;
    }


    /**
     *  Sets the created date.
     *
     *  @param date a created date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setCreatedDate(org.osid.calendaring.DateTime date) {
        super.setCreatedDate(date);
        return;
    }


    /**
     *  Sets the completion date.
     *
     *  @param date a completion date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setCompletionDate(org.osid.calendaring.DateTime date) {
        super.setCompletionDate(date);
        return;
    }
}

