//
// AbstractAdapterAvailabilityEnablerLookupSession.java
//
//    An AvailabilityEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AvailabilityEnabler lookup session adapter.
 */

public abstract class AbstractAdapterAvailabilityEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.rules.AvailabilityEnablerLookupSession {

    private final org.osid.resourcing.rules.AvailabilityEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAvailabilityEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAvailabilityEnablerLookupSession(org.osid.resourcing.rules.AvailabilityEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code AvailabilityEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAvailabilityEnablers() {
        return (this.session.canLookupAvailabilityEnablers());
    }


    /**
     *  A complete view of the {@code AvailabilityEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAvailabilityEnablerView() {
        this.session.useComparativeAvailabilityEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code AvailabilityEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAvailabilityEnablerView() {
        this.session.usePlenaryAvailabilityEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include availability enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active availability enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAvailabilityEnablerView() {
        this.session.useActiveAvailabilityEnablerView();
        return;
    }


    /**
     *  Active and inactive availability enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAvailabilityEnablerView() {
        this.session.useAnyStatusAvailabilityEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code AvailabilityEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AvailabilityEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AvailabilityEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param availabilityEnablerId {@code Id} of the {@code AvailabilityEnabler}
     *  @return the availability enabler
     *  @throws org.osid.NotFoundException {@code availabilityEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code availabilityEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnabler getAvailabilityEnabler(org.osid.id.Id availabilityEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilityEnabler(availabilityEnablerId));
    }


    /**
     *  Gets an {@code AvailabilityEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  availabilityEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AvailabilityEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  availabilityEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AvailabilityEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByIds(org.osid.id.IdList availabilityEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilityEnablersByIds(availabilityEnablerIds));
    }


    /**
     *  Gets an {@code AvailabilityEnablerList} corresponding to the given
     *  availability enabler genus {@code Type} which does not include
     *  availability enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the returned list
     *  may contain only those availability enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  availabilityEnablerGenusType an availabilityEnabler genus type 
     *  @return the returned {@code AvailabilityEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByGenusType(org.osid.type.Type availabilityEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilityEnablersByGenusType(availabilityEnablerGenusType));
    }


    /**
     *  Gets an {@code AvailabilityEnablerList} corresponding to the given
     *  availability enabler genus {@code Type} and include any additional
     *  availability enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the returned list
     *  may contain only those availability enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  availabilityEnablerGenusType an availabilityEnabler genus type 
     *  @return the returned {@code AvailabilityEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByParentGenusType(org.osid.type.Type availabilityEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilityEnablersByParentGenusType(availabilityEnablerGenusType));
    }


    /**
     *  Gets an {@code AvailabilityEnablerList} containing the given
     *  availability enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the returned list
     *  may contain only those availability enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  availabilityEnablerRecordType an availabilityEnabler record type 
     *  @return the returned {@code AvailabilityEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code availabilityEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByRecordType(org.osid.type.Type availabilityEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilityEnablersByRecordType(availabilityEnablerRecordType));
    }


    /**
     *  Gets an {@code AvailabilityEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the returned list
     *  may contain only those availability enablers that are accessible
     *  through this session.
     *  
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code AvailabilityEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilityEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code AvailabilityEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the returned list
     *  may contain only those availability enablers that are accessible
     *  through this session.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code AvailabilityEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getAvailabilityEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code AvailabilityEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the returned list
     *  may contain only those availability enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @return a list of {@code AvailabilityEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAvailabilityEnablers());
    }
}
