//
// AbstractOsidEnablerBuilder.java
//
//     Defines a builder for an OSID Enabler.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      enablers:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;


/**
 *  Defines the OsidEnabler builder.
 */

public abstract class AbstractOsidEnablerBuilder<T extends AbstractOsidEnablerBuilder<? extends T>>
    extends AbstractOsidRuleBuilder<T> {

    private final OsidEnablerMiter enabler;


    /**
     *  Creates a new <code>AbstractOsidEnablerBuilder</code>.
     *
     *  @param enabler an enabler miter interface
     *  @throws org.osid.NullArgumentException <code>enabler</code> is
     *          <code>null</code>
     */

    protected AbstractOsidEnablerBuilder(OsidEnablerMiter enabler) {
        super(enabler);
        this.enabler = enabler;
        return;
    }


    /**
     *  Sets the schedule.
     *
     *  @param schedule a schedule
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */
    
    public T startSchedule(org.osid.calendaring.Schedule schedule) {
        this.enabler.setSchedule(schedule);
        return (self());
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public T endDate(org.osid.calendaring.DateTime date) {
        this.enabler.setEndDate(date);
        return (self());
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    public T startDate(org.osid.calendaring.DateTime date) {
        this.enabler.setStartDate(date);
        return (self());
    }


    /**
     *  Sets the event and makes this enabler effective by event.
     *
     *  @param event the event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */
    
    public T event(org.osid.calendaring.Event event) {
        this.enabler.setEvent(event);
        return (self());
    }


    /**
     *  Sets the cyclic event and makes this enabler effective by
     *  cyclic event.
     *
     *  @param cyclicEvent the cyclic event
     *  @throws org.osid.NullArgumentException <code>cyclicEvent</code> is
     *          <code>null</code>
     */
    
    public T cyclicEvent(org.osid.calendaring.cycle.CyclicEvent cyclicEvent) {
        this.enabler.setCyclicEvent(cyclicEvent);
        return (self());
    }


    /**
     *  Sets the demographic.
     *
     *  @param resource the demographic resource
     *  @throws org.osid.NullArgumentException <code>resource</code> is
     *          <code>null</code>
     */
    
    public T demographic(org.osid.resource.Resource resource) {
        this.enabler.setDemographic(resource);
        return (self());
    }
}
