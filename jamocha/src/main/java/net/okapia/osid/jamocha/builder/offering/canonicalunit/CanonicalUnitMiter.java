//
// CanonicalUnitMiter.java
//
//     Defines a CanonicalUnit miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.offering.canonicalunit;


/**
 *  Defines a <code>CanonicalUnit</code> miter for use with the builders.
 */

public interface CanonicalUnitMiter
    extends net.okapia.osid.jamocha.builder.spi.OperableOsidObjectMiter,
            org.osid.offering.CanonicalUnit {


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    public void setTitle(org.osid.locale.DisplayText title);


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    public void setCode(String code);


    /**
     *  Adds an offered cyclic time period.
     *
     *  @param offeredCyclicTimePeriod an offered cyclic time period
     *  @throws org.osid.NullArgumentException
     *          <code>offeredCyclicTimePeriod</code> is <code>null</code>
     */

    public void addOfferedCyclicTimePeriod(org.osid.calendaring.cycle.CyclicTimePeriod offeredCyclicTimePeriod);


    /**
     *  Sets all the offered cyclic time periods.
     *
     *  @param offeredCyclicTimePeriods a collection of offered cyclic time periods
     *  @throws org.osid.NullArgumentException
     *          <code>offeredCyclicTimePeriods</code> is <code>null</code>
     */

    public void setOfferedCyclicTimePeriods(java.util.Collection<org.osid.calendaring.cycle.CyclicTimePeriod> offeredCyclicTimePeriods);


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    public void addResultOption(org.osid.grading.GradeSystem resultOption);


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    public void setResultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Adds a CanonicalUnit record.
     *
     *  @param record a canonicalUnit record
     *  @param recordType the type of canonicalUnit record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCanonicalUnitRecord(org.osid.offering.records.CanonicalUnitRecord record, org.osid.type.Type recordType);
}       


