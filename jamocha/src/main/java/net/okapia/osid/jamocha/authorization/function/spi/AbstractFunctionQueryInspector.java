//
// AbstractFunctionQueryInspector.java
//
//     A template for making a FunctionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.function.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for functions.
 */

public abstract class AbstractFunctionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.authorization.FunctionQueryInspector {

    private final java.util.Collection<org.osid.authorization.records.FunctionQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the qualifier hierarchy <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQualifierHierarchyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the qualifier hierarchy query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyQueryInspector[] getQualifierHierarchyTerms() {
        return (new org.osid.hierarchy.HierarchyQueryInspector[0]);
    }


    /**
     *  Gets the authorization <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAuthorizationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the authorization query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationQueryInspector[] getAuthorizationTerms() {
        return (new org.osid.authorization.AuthorizationQueryInspector[0]);
    }


    /**
     *  Gets the vault <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVaultIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the vault query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authorization.VaultQueryInspector[] getVaultTerms() {
        return (new org.osid.authorization.VaultQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given function query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a function implementing the requested record.
     *
     *  @param functionRecordType a function record type
     *  @return the function query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>functionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(functionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.FunctionQueryInspectorRecord getFunctionQueryInspectorRecord(org.osid.type.Type functionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.FunctionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(functionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(functionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this function query. 
     *
     *  @param functionQueryInspectorRecord function query inspector
     *         record
     *  @param functionRecordType function record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFunctionQueryInspectorRecord(org.osid.authorization.records.FunctionQueryInspectorRecord functionQueryInspectorRecord, 
                                                   org.osid.type.Type functionRecordType) {

        addRecordType(functionRecordType);
        nullarg(functionRecordType, "function record type");
        this.records.add(functionQueryInspectorRecord);        
        return;
    }
}
