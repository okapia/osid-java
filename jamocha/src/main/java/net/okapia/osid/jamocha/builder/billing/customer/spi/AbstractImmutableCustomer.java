//
// AbstractImmutableCustomer.java
//
//     Wraps a mutable Customer to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.customer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Customer</code> to hide modifiers. This
 *  wrapper provides an immutized Customer from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying customer whose state changes are visible.
 */

public abstract class AbstractImmutableCustomer
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.billing.Customer {

    private final org.osid.billing.Customer customer;


    /**
     *  Constructs a new <code>AbstractImmutableCustomer</code>.
     *
     *  @param customer the customer to immutablize
     *  @throws org.osid.NullArgumentException <code>customer</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCustomer(org.osid.billing.Customer customer) {
        super(customer);
        this.customer = customer;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> representing the customer. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.customer.getResourceId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.customer.getResource());
    }


    /**
     *  Gets the customer account number. 
     *
     *  @return the customer number 
     */

    @OSID @Override
    public String getCustomerNumber() {
        return (this.customer.getCustomerNumber());
    }


    /**
     *  Tests if this customer is linked to a financial activity, 
     *
     *  @return <code> true </code> if this customer has an activity, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasActivity() {
        return (this.customer.hasActivity());
    }


    /**
     *  Gets the activity <code> Id. </code> 
     *
     *  @return the activity <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasActivity() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.customer.getActivityId());
    }


    /**
     *  Gets the activity. 
     *
     *  @return the activity 
     *  @throws org.osid.IllegalStateException <code> hasActivity() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.customer.getActivity());
    }


    /**
     *  Gets the customer record corresponding to the given <code> Customer 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  customerRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(customerRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  customerRecordType the type of customer record to retrieve 
     *  @return the customer record 
     *  @throws org.osid.NullArgumentException <code> customerRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(customerRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.records.CustomerRecord getCustomerRecord(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException {

        return (this.customer.getCustomerRecord(customerRecordType));
    }
}

