//
// AbstractFederatingSequenceRuleEnablerLookupSession.java
//
//     An abstract federating adapter for a SequenceRuleEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SequenceRuleEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSequenceRuleEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.assessment.authoring.SequenceRuleEnablerLookupSession>
    implements org.osid.assessment.authoring.SequenceRuleEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();


    /**
     *  Constructs a new <code>AbstractFederatingSequenceRuleEnablerLookupSession</code>.
     */

    protected AbstractFederatingSequenceRuleEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }


    /**
     *  Tests if this user can perform <code>SequenceRuleEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSequenceRuleEnablers() {
        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            if (session.canLookupSequenceRuleEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>SequenceRuleEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSequenceRuleEnablerView() {
        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            session.useComparativeSequenceRuleEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>SequenceRuleEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySequenceRuleEnablerView() {
        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            session.usePlenarySequenceRuleEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include sequence rule enablers in banks which are
     *  children of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            session.useFederatedBankView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            session.useIsolatedBankView();
        }

        return;
    }


    /**
     *  Only active sequence rule enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSequenceRuleEnablerView() {
        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            session.useActiveSequenceRuleEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive sequence rule enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSequenceRuleEnablerView() {
        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            session.useAnyStatusSequenceRuleEnablerView();
        }

        return;
    }

     
    /**
     *  Gets the <code>SequenceRuleEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SequenceRuleEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SequenceRuleEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, sequence rule enablers are returned that are currently
     *  active. In any status mode, active and inactive sequence rule enablers
     *  are returned.
     *
     *  @param  sequenceRuleEnablerId <code>Id</code> of the
     *          <code>SequenceRuleEnabler</code>
     *  @return the sequence rule enabler
     *  @throws org.osid.NotFoundException <code>sequenceRuleEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>sequenceRuleEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnabler getSequenceRuleEnabler(org.osid.id.Id sequenceRuleEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            try {
                return (session.getSequenceRuleEnabler(sequenceRuleEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(sequenceRuleEnablerId + " not found");
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  sequenceRuleEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>SequenceRuleEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  sequenceRuleEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByIds(org.osid.id.IdList sequenceRuleEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.MutableSequenceRuleEnablerList ret = new net.okapia.osid.jamocha.assessment.authoring.sequenceruleenabler.MutableSequenceRuleEnablerList();

        try (org.osid.id.IdList ids = sequenceRuleEnablerIds) {
            while (ids.hasNext()) {
                ret.addSequenceRuleEnabler(getSequenceRuleEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> corresponding to
     *  the given sequence rule enabler genus <code>Type</code> which
     *  does not include sequence rule enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  sequenceRuleEnablerGenusType a sequenceRuleEnabler genus type 
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByGenusType(org.osid.type.Type sequenceRuleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();

        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablersByGenusType(sequenceRuleEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> corresponding to
     *  the given sequence rule enabler genus <code>Type</code> and
     *  include any additional sequence rule enablers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  sequenceRuleEnablerGenusType a sequenceRuleEnabler genus type 
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByParentGenusType(org.osid.type.Type sequenceRuleEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();

        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablersByParentGenusType(sequenceRuleEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> containing the given
     *  sequence rule enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  sequenceRuleEnablerRecordType a sequenceRuleEnabler record type 
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersByRecordType(org.osid.type.Type sequenceRuleEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();

        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablersByRecordType(sequenceRuleEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SequenceRuleEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session.
     *  
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>SequenceRuleEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();

        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>SequenceRuleEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>SequenceRuleEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                                        org.osid.calendaring.DateTime from,
                                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();

        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>SequenceRuleEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known sequence
     *  rule enablers or an error results. Otherwise, the returned
     *  list may contain only those sequence rule enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, sequence rule enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  sequence rule enablers are returned.
     *
     *  @return a list of <code>SequenceRuleEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerList getSequenceRuleEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList ret = getSequenceRuleEnablerList();

        for (org.osid.assessment.authoring.SequenceRuleEnablerLookupSession session : getSessions()) {
            ret.addSequenceRuleEnablerList(session.getSequenceRuleEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.FederatingSequenceRuleEnablerList getSequenceRuleEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.ParallelSequenceRuleEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.authoring.sequenceruleenabler.CompositeSequenceRuleEnablerList());
        }
    }
}
