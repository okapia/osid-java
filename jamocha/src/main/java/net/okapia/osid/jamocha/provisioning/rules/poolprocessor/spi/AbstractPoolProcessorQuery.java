//
// AbstractPoolProcessorQuery.java
//
//     A template for making a PoolProcessor Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for pool processors.
 */

public abstract class AbstractPoolProcessorQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQuery
    implements org.osid.provisioning.rules.PoolProcessorQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches pools that allocate by least use. 
     *
     *  @param  match <code> true </code> to match pool allocators that 
     *          allocate by least use, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllocatesByLeastUse(boolean match) {
        return;
    }


    /**
     *  Clears the allocate by least use query terms. 
     */

    @OSID @Override
    public void clearAllocatesByLeastUseTerms() {
        return;
    }


    /**
     *  Matches pools that allocate by most use. 
     *
     *  @param  match <code> true </code> to match pool allocators that 
     *          allocate by most use, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllocatesByMostUse(boolean match) {
        return;
    }


    /**
     *  Clears the allocate by most use query terms. 
     */

    @OSID @Override
    public void clearAllocatesByMostUseTerms() {
        return;
    }


    /**
     *  Matches pools that allocate by least cost. 
     *
     *  @param  match <code> true </code> to match pool allocators that 
     *          allocate by least cost, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllocatesByLeastCost(boolean match) {
        return;
    }


    /**
     *  Clears the allocate by most cost query terms. 
     */

    @OSID @Override
    public void clearAllocatesByLeastCostTerms() {
        return;
    }


    /**
     *  Matches pools that allocate by most cost. 
     *
     *  @param  match <code> true </code> to match pool allocators that 
     *          allocate by most cost, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchAllocatesByMostCost(boolean match) {
        return;
    }


    /**
     *  Clears the allocate by least cost query terms. 
     */

    @OSID @Override
    public void clearAllocatesByMostCostTerms() {
        return;
    }


    /**
     *  Matches mapped to the pool. 
     *
     *  @param  distributorId the pool <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledPoolId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the pool <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledPoolIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PoolQuery </code> is available. 
     *
     *  @return <code> true </code> if a pool query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledPoolQuery() {
        return (false);
    }


    /**
     *  Gets the query for a pool. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the pool query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledPoolQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuery getRuledPoolQuery() {
        throw new org.osid.UnimplementedException("supportsRuledPoolQuery() is false");
    }


    /**
     *  Matches mapped to any pool. 
     *
     *  @param  match <code> true </code> for mapped to any pool, <code> false 
     *          </code> to match mapped to no pool 
     */

    @OSID @Override
    public void matchAnyRuledPool(boolean match) {
        return;
    }


    /**
     *  Clears the pool query terms. 
     */

    @OSID @Override
    public void clearRuledPoolTerms() {
        return;
    }


    /**
     *  Matches mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given pool processor query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a pool processor implementing the requested record.
     *
     *  @param poolProcessorRecordType a pool processor record type
     *  @return the pool processor query record
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorQueryRecord getPoolProcessorQueryRecord(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorQueryRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool processor query. 
     *
     *  @param poolProcessorQueryRecord pool processor query record
     *  @param poolProcessorRecordType poolProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPoolProcessorQueryRecord(org.osid.provisioning.rules.records.PoolProcessorQueryRecord poolProcessorQueryRecord, 
                                          org.osid.type.Type poolProcessorRecordType) {

        addRecordType(poolProcessorRecordType);
        nullarg(poolProcessorQueryRecord, "pool processor query record");
        this.records.add(poolProcessorQueryRecord);        
        return;
    }
}
