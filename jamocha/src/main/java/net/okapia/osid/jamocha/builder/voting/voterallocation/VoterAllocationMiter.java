//
// VoterAllocationMiter.java
//
//     Defines a VoterAllocation miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.voting.voterallocation;


/**
 *  Defines a <code>VoterAllocation</code> miter for use with the builders.
 */

public interface VoterAllocationMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.voting.VoterAllocation {


    /**
     *  Sets the race.
     *
     *  @param race a race
     *  @throws org.osid.NullArgumentException <code>race</code> is
     *          <code>null</code>
     */

    public void setRace(org.osid.voting.Race race);


    /**
     *  Sets the voter.
     *
     *  @param voter a voter
     *  @throws org.osid.NullArgumentException <code>voter</code> is
     *          <code>null</code>
     */

    public void setVoter(org.osid.resource.Resource voter);


    /**
     *  Sets the total votes.
     *
     *  @param votes the total votes
     *  @throws org.osid.InvalidArgumentException <code>votes</code>
     *          is negative
     */

    public void setTotalVotes(long votes);


    /**
     *  Sets the max votes per candidate.
     *
     *  @param votes the max votes per candidate
     *  @throws org.osid.InvalidArgumentException <code>votes</code>
     *          is negative
     */

    public void setMaxVotesPerCandidate(long votes);


    /**
     *  Sets the max candidates.
     *
     *  @param candidates the max candidates
     *  @throws org.osid.InvalidArgumentException
     *          <code>candidates</code> is negative
     */

    public void setMaxCandidates(long candidates);


    /**
     *  Sets the reallocates votes when candidate drops.
     *
     *  @param reallocate <code> true </code> if votes are returned to
     *         the resource when the candidate drops from the race,
     *         <code> false </code> otherwise
     */

    public void setReallocatesVotesWhenCandidateDrops(boolean reallocate);


    /**
     *  Adds a VoterAllocation record.
     *
     *  @param record a voterAllocation record
     *  @param recordType the type of voterAllocation record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addVoterAllocationRecord(org.osid.voting.records.VoterAllocationRecord record, org.osid.type.Type recordType);
}       


