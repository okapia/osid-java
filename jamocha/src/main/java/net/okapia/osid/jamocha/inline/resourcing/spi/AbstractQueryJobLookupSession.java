//
// AbstractQueryJobLookupSession.java
//
//    An inline adapter that maps a JobLookupSession to
//    a JobQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a JobLookupSession to
 *  a JobQuerySession.
 */

public abstract class AbstractQueryJobLookupSession
    extends net.okapia.osid.jamocha.resourcing.spi.AbstractJobLookupSession
    implements org.osid.resourcing.JobLookupSession {

    private boolean activeonly    = false;
    private final org.osid.resourcing.JobQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryJobLookupSession.
     *
     *  @param querySession the underlying job query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryJobLookupSession(org.osid.resourcing.JobQuerySession querySession) {
        nullarg(querySession, "job query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Foundry</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform <code>Job</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJobs() {
        return (this.session.canSearchJobs());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include jobs in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active jobs are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive jobs are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Job</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Job</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Job</code> and
     *  retained for compatibility.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobId <code>Id</code> of the
     *          <code>Job</code>
     *  @return the job
     *  @throws org.osid.NotFoundException <code>jobId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>jobId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Job getJob(org.osid.id.Id jobId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.JobQuery query = getQuery();
        query.matchId(jobId, true);
        org.osid.resourcing.JobList jobs = this.session.getJobsByQuery(query);
        if (jobs.hasNext()) {
            return (jobs.getNextJob());
        } 
        
        throw new org.osid.NotFoundException(jobId + " not found");
    }


    /**
     *  Gets a <code>JobList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Jobs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>jobIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByIds(org.osid.id.IdList jobIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.JobQuery query = getQuery();

        try (org.osid.id.IdList ids = jobIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getJobsByQuery(query));
    }


    /**
     *  Gets a <code>JobList</code> corresponding to the given
     *  job genus <code>Type</code> which does not include
     *  jobs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobGenusType a job genus type 
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByGenusType(org.osid.type.Type jobGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.JobQuery query = getQuery();
        query.matchGenusType(jobGenusType, true);
        return (this.session.getJobsByQuery(query));
    }


    /**
     *  Gets a <code>JobList</code> corresponding to the given
     *  job genus <code>Type</code> and include any additional
     *  jobs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobGenusType a job genus type 
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByParentGenusType(org.osid.type.Type jobGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.JobQuery query = getQuery();
        query.matchParentGenusType(jobGenusType, true);
        return (this.session.getJobsByQuery(query));
    }


    /**
     *  Gets a <code>JobList</code> containing the given
     *  job record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  jobRecordType a job record type 
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByRecordType(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.JobQuery query = getQuery();
        query.matchRecordType(jobRecordType, true);
        return (this.session.getJobsByQuery(query));
    }


    /**
     *  Gets a <code>JobList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known jobs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  jobs that are accessible through this session. 
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Job</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.JobQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getJobsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Jobs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  jobs or an error results. Otherwise, the returned list
     *  may contain only those jobs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs
     *  are returned.
     *
     *  @return a list of <code>Jobs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.resourcing.JobQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getJobsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.resourcing.JobQuery getQuery() {
        org.osid.resourcing.JobQuery query = this.session.getJobQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
