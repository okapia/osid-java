//
// AbstractOfferingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOfferingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.offering.OfferingManager,
               org.osid.offering.OfferingProxyManager {

    private final Types canonicalUnitRecordTypes           = new TypeRefSet();
    private final Types canonicalUnitSearchRecordTypes     = new TypeRefSet();

    private final Types offeringRecordTypes                = new TypeRefSet();
    private final Types offeringSearchRecordTypes          = new TypeRefSet();

    private final Types participantRecordTypes             = new TypeRefSet();
    private final Types participantSearchRecordTypes       = new TypeRefSet();

    private final Types resultRecordTypes                  = new TypeRefSet();
    private final Types resultSearchRecordTypes            = new TypeRefSet();

    private final Types catalogueRecordTypes               = new TypeRefSet();
    private final Types catalogueSearchRecordTypes         = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractOfferingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOfferingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any catalogue federation is exposed. Federation is exposed 
     *  when a specific catalogue may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of catalogues appears as a single catalogue. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a canonical unit lookup service. 
     *
     *  @return <code> true </code> if canonical unit lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a canonical unit query service. 
     *
     *  @return <code> true </code> if canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitQuery() {
        return (false);
    }


    /**
     *  Tests if searching for canonical units is available. 
     *
     *  @return <code> true </code> if canonical unit search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitSearch() {
        return (false);
    }


    /**
     *  Tests if managing for canonical units is available. 
     *
     *  @return <code> true </code> if a canonical unit adminstrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitAdmin() {
        return (false);
    }


    /**
     *  Tests if canonical unit notification is available. 
     *
     *  @return <code> true </code> if canonical unit notification is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitNotification() {
        return (false);
    }


    /**
     *  Tests if a canonical unit to catalogue lookup session is available. 
     *
     *  @return <code> true </code> if canonical unit catalogue lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitCatalogue() {
        return (false);
    }


    /**
     *  Tests if a canonical unit to catalogue assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if canonical unit catalogue assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if a canonical unit smart catalogue session is available. 
     *
     *  @return <code> true </code> if canonical unit smart catalogue is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests for the availability of an offering lookup service. 
     *
     *  @return <code> true </code> if offering lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of an offering query service. 
     *
     *  @return <code> true </code> if offering query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingQuery() {
        return (false);
    }


    /**
     *  Tests if searching for offerings is available. 
     *
     *  @return <code> true </code> if offering search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingSearch() {
        return (false);
    }


    /**
     *  Tests if managing for offerings is available. 
     *
     *  @return <code> true </code> if an offering adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingAdmin() {
        return (false);
    }


    /**
     *  Tests if offering notification is available. 
     *
     *  @return <code> true </code> if offering notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingNotification() {
        return (false);
    }


    /**
     *  Tests if an offering to catalogue lookup session is available. 
     *
     *  @return <code> true </code> if offering catalogue lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingCatalogue() {
        return (false);
    }


    /**
     *  Tests if an offering to catalogue assignment session is available. 
     *
     *  @return <code> true </code> if offering catalogue assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if an offering smart catalogue session is available. 
     *
     *  @return <code> true </code> if offering smart catalogue is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests for the availability of a participant lookup service. 
     *
     *  @return <code> true </code> if participant lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a participant query service. 
     *
     *  @return <code> true </code> if participant query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantQuery() {
        return (false);
    }


    /**
     *  Tests if searching for participants is available. 
     *
     *  @return <code> true </code> if participant search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantSearch() {
        return (false);
    }


    /**
     *  Tests if managing for participants is available. 
     *
     *  @return <code> true </code> if a participant adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantAdmin() {
        return (false);
    }


    /**
     *  Tests if participant notification is available. 
     *
     *  @return <code> true </code> if participant notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantNotification() {
        return (false);
    }


    /**
     *  Tests if a participant to catalogue lookup session is available. 
     *
     *  @return <code> true </code> if participant catalogue lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantCatalogue() {
        return (false);
    }


    /**
     *  Tests if a participant to catalogue assignment session is available. 
     *
     *  @return <code> true </code> if participant catalogue assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if a participant smart catalogue session is available. 
     *
     *  @return <code> true </code> if participant smart catalogue is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParticipantSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests for the availability of an result lookup service. 
     *
     *  @return <code> true </code> if result lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of an result query service. 
     *
     *  @return <code> true </code> if result query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultQuery() {
        return (false);
    }


    /**
     *  Tests if searching for results is available. 
     *
     *  @return <code> true </code> if result search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultSearch() {
        return (false);
    }


    /**
     *  Tests if managing for results is available. 
     *
     *  @return <code> true </code> if an result adminstrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultAdmin() {
        return (false);
    }


    /**
     *  Tests if result notification is available. 
     *
     *  @return <code> true </code> if result notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultNotification() {
        return (false);
    }


    /**
     *  Tests if an result to catalogue lookup session is available. 
     *
     *  @return <code> true </code> if result catalogue lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultCatalogue() {
        return (false);
    }


    /**
     *  Tests if an result to catalogue assignment session is available. 
     *
     *  @return <code> true </code> if result catalogue assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if an result smart catalogue session is available. 
     *
     *  @return <code> true </code> if result smart catalogue is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResultSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests for the availability of an catalogue lookup service. 
     *
     *  @return <code> true </code> if catalogue lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueLookup() {
        return (false);
    }


    /**
     *  Tests if querying catalogues is available. 
     *
     *  @return <code> true </code> if catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Tests if searching for catalogues is available. 
     *
     *  @return <code> true </code> if catalogue search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalogue administrative service for 
     *  creating and deleting catalogues. 
     *
     *  @return <code> true </code> if catalogue administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalogue notification service. 
     *
     *  @return <code> true </code> if catalogue notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalogue hierarchy traversal service. 
     *
     *  @return <code> true </code> if catalogue hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a catalogue hierarchy design service. 
     *
     *  @return <code> true </code> if catalogue hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a offering batch service. 
     *
     *  @return <code> true </code> if a offering batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a offering rules service. 
     *
     *  @return <code> true </code> if a offering rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> CanonicalUnit </code> record types. 
     *
     *  @return a list containing the supported canonical unit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.canonicalUnitRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CanonicalUnit </code> record type is 
     *  supported. 
     *
     *  @param  canonicalUnitRecordType a <code> Type </code> indicating a 
     *          <code> CanonicalUnit </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitRecordType(org.osid.type.Type canonicalUnitRecordType) {
        return (this.canonicalUnitRecordTypes.contains(canonicalUnitRecordType));
    }


    /**
     *  Adds support for a canonical unit record type.
     *
     *  @param canonicalUnitRecordType a canonical unit record type
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitRecordType</code> is <code>null</code>
     */

    protected void addCanonicalUnitRecordType(org.osid.type.Type canonicalUnitRecordType) {
        this.canonicalUnitRecordTypes.add(canonicalUnitRecordType);
        return;
    }


    /**
     *  Removes support for a canonical unit record type.
     *
     *  @param canonicalUnitRecordType a canonical unit record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitRecordType</code> is <code>null</code>
     */

    protected void removeCanonicalUnitRecordType(org.osid.type.Type canonicalUnitRecordType) {
        this.canonicalUnitRecordTypes.remove(canonicalUnitRecordType);
        return;
    }


    /**
     *  Gets the supported canonical unit search record types. 
     *
     *  @return a list containing the supported canonical unit search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.canonicalUnitSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given canonical unit search record type is supported. 
     *
     *  @param  canonicalUnitSearchRecordType a <code> Type </code> indicating 
     *          a canonical unit record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitSearchRecordType(org.osid.type.Type canonicalUnitSearchRecordType) {
        return (this.canonicalUnitSearchRecordTypes.contains(canonicalUnitSearchRecordType));
    }


    /**
     *  Adds support for a canonical unit search record type.
     *
     *  @param canonicalUnitSearchRecordType a canonical unit search record type
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitSearchRecordType</code> is <code>null</code>
     */

    protected void addCanonicalUnitSearchRecordType(org.osid.type.Type canonicalUnitSearchRecordType) {
        this.canonicalUnitSearchRecordTypes.add(canonicalUnitSearchRecordType);
        return;
    }


    /**
     *  Removes support for a canonical unit search record type.
     *
     *  @param canonicalUnitSearchRecordType a canonical unit search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitSearchRecordType</code> is <code>null</code>
     */

    protected void removeCanonicalUnitSearchRecordType(org.osid.type.Type canonicalUnitSearchRecordType) {
        this.canonicalUnitSearchRecordTypes.remove(canonicalUnitSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Offering </code> record types. 
     *
     *  @return a list containing the supported offering record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offeringRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Offering </code> record type is supported. 
     *
     *  @param  offeringRecordType a <code> Type </code> indicating an <code> 
     *          Offering </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> offeringRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfferingRecordType(org.osid.type.Type offeringRecordType) {
        return (this.offeringRecordTypes.contains(offeringRecordType));
    }


    /**
     *  Adds support for an offering record type.
     *
     *  @param offeringRecordType an offering record type
     *  @throws org.osid.NullArgumentException
     *  <code>offeringRecordType</code> is <code>null</code>
     */

    protected void addOfferingRecordType(org.osid.type.Type offeringRecordType) {
        this.offeringRecordTypes.add(offeringRecordType);
        return;
    }


    /**
     *  Removes support for an offering record type.
     *
     *  @param offeringRecordType an offering record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offeringRecordType</code> is <code>null</code>
     */

    protected void removeOfferingRecordType(org.osid.type.Type offeringRecordType) {
        this.offeringRecordTypes.remove(offeringRecordType);
        return;
    }


    /**
     *  Gets the supported offering search record types. 
     *
     *  @return a list containing the supported offering search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offeringSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given offering search record type is supported. 
     *
     *  @param  offeringSearchRecordType a <code> Type </code> indicating an 
     *          offering record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> offeringSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfferingSearchRecordType(org.osid.type.Type offeringSearchRecordType) {
        return (this.offeringSearchRecordTypes.contains(offeringSearchRecordType));
    }


    /**
     *  Adds support for an offering search record type.
     *
     *  @param offeringSearchRecordType an offering search record type
     *  @throws org.osid.NullArgumentException
     *  <code>offeringSearchRecordType</code> is <code>null</code>
     */

    protected void addOfferingSearchRecordType(org.osid.type.Type offeringSearchRecordType) {
        this.offeringSearchRecordTypes.add(offeringSearchRecordType);
        return;
    }


    /**
     *  Removes support for an offering search record type.
     *
     *  @param offeringSearchRecordType an offering search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offeringSearchRecordType</code> is <code>null</code>
     */

    protected void removeOfferingSearchRecordType(org.osid.type.Type offeringSearchRecordType) {
        this.offeringSearchRecordTypes.remove(offeringSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Participant </code> record types. 
     *
     *  @return a list containing the supported participant record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParticipantRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.participantRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Participant </code> record type is 
     *  supported. 
     *
     *  @param  participantRecordType a <code> Type </code> indicating a 
     *          <code> Participant </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> participantRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParticipantRecordType(org.osid.type.Type participantRecordType) {
        return (this.participantRecordTypes.contains(participantRecordType));
    }


    /**
     *  Adds support for a participant record type.
     *
     *  @param participantRecordType a participant record type
     *  @throws org.osid.NullArgumentException
     *  <code>participantRecordType</code> is <code>null</code>
     */

    protected void addParticipantRecordType(org.osid.type.Type participantRecordType) {
        this.participantRecordTypes.add(participantRecordType);
        return;
    }


    /**
     *  Removes support for a participant record type.
     *
     *  @param participantRecordType a participant record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>participantRecordType</code> is <code>null</code>
     */

    protected void removeParticipantRecordType(org.osid.type.Type participantRecordType) {
        this.participantRecordTypes.remove(participantRecordType);
        return;
    }


    /**
     *  Gets the supported participant search record types. 
     *
     *  @return a list containing the supported participant search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParticipantSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.participantSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given participant search record type is supported. 
     *
     *  @param  participantSearchRecordType a <code> Type </code> indicating a 
     *          participant record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          participantSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParticipantSearchRecordType(org.osid.type.Type participantSearchRecordType) {
        return (this.participantSearchRecordTypes.contains(participantSearchRecordType));
    }


    /**
     *  Adds support for a participant search record type.
     *
     *  @param participantSearchRecordType a participant search record type
     *  @throws org.osid.NullArgumentException
     *  <code>participantSearchRecordType</code> is <code>null</code>
     */

    protected void addParticipantSearchRecordType(org.osid.type.Type participantSearchRecordType) {
        this.participantSearchRecordTypes.add(participantSearchRecordType);
        return;
    }


    /**
     *  Removes support for a participant search record type.
     *
     *  @param participantSearchRecordType a participant search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>participantSearchRecordType</code> is <code>null</code>
     */

    protected void removeParticipantSearchRecordType(org.osid.type.Type participantSearchRecordType) {
        this.participantSearchRecordTypes.remove(participantSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Result </code> record types. 
     *
     *  @return a list containing the supported result record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResultRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.resultRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Result </code> record type is supported. 
     *
     *  @param  resultRecordType a <code> Type </code> indicating an <code> 
     *          Result </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resultRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResultRecordType(org.osid.type.Type resultRecordType) {
        return (this.resultRecordTypes.contains(resultRecordType));
    }


    /**
     *  Adds support for a result record type.
     *
     *  @param resultRecordType a result record type
     *  @throws org.osid.NullArgumentException
     *  <code>resultRecordType</code> is <code>null</code>
     */

    protected void addResultRecordType(org.osid.type.Type resultRecordType) {
        this.resultRecordTypes.add(resultRecordType);
        return;
    }


    /**
     *  Removes support for a result record type.
     *
     *  @param resultRecordType a result record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>resultRecordType</code> is <code>null</code>
     */

    protected void removeResultRecordType(org.osid.type.Type resultRecordType) {
        this.resultRecordTypes.remove(resultRecordType);
        return;
    }


    /**
     *  Gets the supported result search record types. 
     *
     *  @return a list containing the supported result search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResultSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.resultSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given result search record type is supported. 
     *
     *  @param  resultSearchRecordType a <code> Type </code> indicating an 
     *          result record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> resultSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResultSearchRecordType(org.osid.type.Type resultSearchRecordType) {
        return (this.resultSearchRecordTypes.contains(resultSearchRecordType));
    }


    /**
     *  Adds support for a result search record type.
     *
     *  @param resultSearchRecordType a result search record type
     *  @throws org.osid.NullArgumentException
     *  <code>resultSearchRecordType</code> is <code>null</code>
     */

    protected void addResultSearchRecordType(org.osid.type.Type resultSearchRecordType) {
        this.resultSearchRecordTypes.add(resultSearchRecordType);
        return;
    }


    /**
     *  Removes support for a result search record type.
     *
     *  @param resultSearchRecordType a result search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>resultSearchRecordType</code> is <code>null</code>
     */

    protected void removeResultSearchRecordType(org.osid.type.Type resultSearchRecordType) {
        this.resultSearchRecordTypes.remove(resultSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Catalogue </code> record types. 
     *
     *  @return a list containing the supported catalogue record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogueRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.catalogueRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Catalogue </code> record type is supported. 
     *
     *  @param  catalogueRecordType a <code> Type </code> indicating a <code> 
     *          Catalogue </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> catalogueRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogueRecordType(org.osid.type.Type catalogueRecordType) {
        return (this.catalogueRecordTypes.contains(catalogueRecordType));
    }


    /**
     *  Adds support for a catalogue record type.
     *
     *  @param catalogueRecordType a catalogue record type
     *  @throws org.osid.NullArgumentException
     *  <code>catalogueRecordType</code> is <code>null</code>
     */

    protected void addCatalogueRecordType(org.osid.type.Type catalogueRecordType) {
        this.catalogueRecordTypes.add(catalogueRecordType);
        return;
    }


    /**
     *  Removes support for a catalogue record type.
     *
     *  @param catalogueRecordType a catalogue record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>catalogueRecordType</code> is <code>null</code>
     */

    protected void removeCatalogueRecordType(org.osid.type.Type catalogueRecordType) {
        this.catalogueRecordTypes.remove(catalogueRecordType);
        return;
    }


    /**
     *  Gets the supported catalogue search record types. 
     *
     *  @return a list containing the supported catalogue search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCatalogueSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.catalogueSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given catalogue search record type is supported. 
     *
     *  @param  catalogueSearchRecordType a <code> Type </code> indicating a 
     *          catalogue record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          catalogueSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCatalogueSearchRecordType(org.osid.type.Type catalogueSearchRecordType) {
        return (this.catalogueSearchRecordTypes.contains(catalogueSearchRecordType));
    }


    /**
     *  Adds support for a catalogue search record type.
     *
     *  @param catalogueSearchRecordType a catalogue search record type
     *  @throws org.osid.NullArgumentException
     *  <code>catalogueSearchRecordType</code> is <code>null</code>
     */

    protected void addCatalogueSearchRecordType(org.osid.type.Type catalogueSearchRecordType) {
        this.catalogueSearchRecordTypes.add(catalogueSearchRecordType);
        return;
    }


    /**
     *  Removes support for a catalogue search record type.
     *
     *  @param catalogueSearchRecordType a catalogue search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>catalogueSearchRecordType</code> is <code>null</code>
     */

    protected void removeCatalogueSearchRecordType(org.osid.type.Type catalogueSearchRecordType) {
        this.catalogueSearchRecordTypes.remove(catalogueSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  lookup service. 
     *
     *  @return a <code> CanonicalUnitLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitLookupSession getCanonicalUnitLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitLookupSession getCanonicalUnitLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitLookupSession getCanonicalUnitLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitLookupSession getCanonicalUnitLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  query service. 
     *
     *  @return a <code> CanonicalUnitQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuerySession getCanonicalUnitQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuerySession getCanonicalUnitQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuerySession getCanonicalUnitQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuerySession getCanonicalUnitQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  search service. 
     *
     *  @return a <code> CanonicalUnitSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSearchSession getCanonicalUnitSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSearchSession getCanonicalUnitSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSearchSession getCanonicalUnitSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSearchSession getCanonicalUnitSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  administration service. 
     *
     *  @return a <code> CanonicalUnitAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitAdminSession getCanonicalUnitAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitAdminSession getCanonicalUnitAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitAdminSession getCanonicalUnitAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitAdminSession getCanonicalUnitAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  notification service. 
     *
     *  @param  canonicalUnitReceiver the receiver 
     *  @return a <code> CanonicalUnitNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitNotificationSession getCanonicalUnitNotificationSession(org.osid.offering.CanonicalUnitReceiver canonicalUnitReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  notification service. 
     *
     *  @param  canonicalUnitReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitNotificationSession getCanonicalUnitNotificationSession(org.osid.offering.CanonicalUnitReceiver canonicalUnitReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  notification service for the given catalogue. 
     *
     *  @param  canonicalUnitReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitReceiver 
     *          </code> or <code> catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitNotificationSession getCanonicalUnitNotificationSessionForCatalogue(org.osid.offering.CanonicalUnitReceiver canonicalUnitReceiver, 
                                                                                                              org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  notification service for the given catalogue. 
     *
     *  @param  canonicalUnitReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitReceiver, 
     *          catalogueId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitNotificationSession getCanonicalUnitNotificationSessionForCatalogue(org.osid.offering.CanonicalUnitReceiver canonicalUnitReceiver, 
                                                                                                              org.osid.id.Id catalogueId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the session for retrieving canonical unit to catalogue mappings. 
     *
     *  @return a <code> CanonicalUnitCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitCatalogueSession getCanonicalUnitCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitCatalogueSession not implemented");
    }


    /**
     *  Gets the session for retrieving canonical unit to catalogue mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitCatalogueSession getCanonicalUnitCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitCatalogueSession not implemented");
    }


    /**
     *  Gets the session for assigning canonical unit to catalogue mappings. 
     *
     *  @return a <code> CanonicalUnitCatalogueAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitCatalogueAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitCatalogueAssignmentSession getCanonicalUnitCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning canonical unit to catalogue mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitCatalogueAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitCatalogueAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitCatalogueAssignmentSession getCanonicalUnitCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the canonical unit smart catalogue 
     *  for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the catalogue 
     *  @return a <code> CanonicalUnitSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSmartCatalogueSession getCanonicalUnitSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCanonicalUnitSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic canonical unit catalogues for 
     *  the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of a catalogue 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitSmartCatalogueSession getCanonicalUnitSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCanonicalUnitSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  lookup service. 
     *
     *  @return an <code> OfferingLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingLookupSession getOfferingLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingLookupSession getOfferingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingLookupSession getOfferingLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingLookupSession getOfferingLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering query 
     *  service. 
     *
     *  @return an <code> OfferingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuerySession getOfferingQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuerySession getOfferingQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering query 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuerySession getOfferingQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering query 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingQuerySession getOfferingQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  search service. 
     *
     *  @return an <code> OfferingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSearchSession getOfferingSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSearchSession getOfferingSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSearchSession getOfferingSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSearchSession getOfferingSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  administration service. 
     *
     *  @return an <code> OfferingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingAdminSession getOfferingAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingAdminSession getOfferingAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingAdminSession getOfferingAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingAdminSession getOfferingAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  notification service. 
     *
     *  @param  offeringReceiver the receiver 
     *  @return an <code> OfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> offeringReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingNotificationSession getOfferingNotificationSession(org.osid.offering.OfferingReceiver offeringReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  notification service. 
     *
     *  @param  offeringReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> offeringReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingNotificationSession getOfferingNotificationSession(org.osid.offering.OfferingReceiver offeringReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  notification service for the given catalogue. 
     *
     *  @param  offeringReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> offeringReceiver </code> 
     *          or <code> catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingNotificationSession getOfferingNotificationSessionForCatalogue(org.osid.offering.OfferingReceiver offeringReceiver, 
                                                                                                    org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  notification service for the given catalogue. 
     *
     *  @param  offeringReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> offeringReceiver, 
     *          catalogueId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingNotificationSession getOfferingNotificationSessionForCatalogue(org.osid.offering.OfferingReceiver offeringReceiver, 
                                                                                                    org.osid.id.Id catalogueId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the session for retrieving offering to catalogue mappings. 
     *
     *  @return an <code> OfferingCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingCatalogueSession getOfferingCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingCatalogueSession not implemented");
    }


    /**
     *  Gets the session for retrieving offering to catalogue mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingCatalogueSession getOfferingCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingCatalogueSession not implemented");
    }


    /**
     *  Gets the session for assigning offering to catalogue mappings. 
     *
     *  @return an <code> OfferingCatalogueAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingCatalogueAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingCatalogueAssignmentSession getOfferingCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning offering to catalogue mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingCatalogueAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingCatalogueAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingCatalogueAssignmentSession getOfferingCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the offering smart catalogue for the 
     *  given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the catalogue 
     *  @return an <code> OfferingSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSmartCatalogueSession getOfferingSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic offering catalogues for the 
     *  given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of a catalogue 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingSmartCatalogueSession getOfferingSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  lookup service. 
     *
     *  @return a <code> ParticipantLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantLookupSession getParticipantLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantLookupSession getParticipantLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantLookupSession getParticipantLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantLookupSession getParticipantLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  query service. 
     *
     *  @return a <code> ParticipantQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuerySession getParticipantQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuerySession getParticipantQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuerySession getParticipantQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantQuerySession getParticipantQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  search service. 
     *
     *  @return a <code> ParticipantSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSearchSession getParticipantSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSearchSession getParticipantSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSearchSession getParticipantSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  search service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSearchSession getParticipantSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  administration service. 
     *
     *  @return a <code> ParticipantAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantAdminSession getParticipantAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantAdminSession getParticipantAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantAdminSession getParticipantAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantAdminSession getParticipantAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  notification service. 
     *
     *  @param  participantReceiver the receiver 
     *  @return a <code> ParticipantNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> participantReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantNotificationSession getParticipantNotificationSession(org.osid.offering.ParticipantReceiver participantReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  notification service. 
     *
     *  @param  participantReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> participantReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantNotificationSession getParticipantNotificationSession(org.osid.offering.ParticipantReceiver participantReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  notification service for the given catalogue. 
     *
     *  @param  participantReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> ParticipantNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> participantReceiver 
     *          </code> or <code> catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantNotificationSession getParticipantNotificationSessionForCatalogue(org.osid.offering.ParticipantReceiver participantReceiver, 
                                                                                                          org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the participant 
     *  notification service for the given catalogue. 
     *
     *  @param  participantReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> participantReceiver, 
     *          catalogueId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantNotificationSession getParticipantNotificationSessionForCatalogue(org.osid.offering.ParticipantReceiver participantReceiver, 
                                                                                                          org.osid.id.Id catalogueId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the session for retrieving participant to catalogue mappings. 
     *
     *  @return a <code> ParticipantCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantCatalogueSession getParticipantCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantCatalogueSession not implemented");
    }


    /**
     *  Gets the session for retrieving participant to catalogue mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantCatalogueSession getParticipantCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantCatalogueSession not implemented");
    }


    /**
     *  Gets the session for assigning participant to catalogue mappings. 
     *
     *  @return a <code> ParticipantCatalogueAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantCatalogueAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantCatalogueAssignmentSession getParticipantCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning participant to catalogue mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantCatalogueAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantCatalogueAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantCatalogueAssignmentSession getParticipantCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the participant smart catalogue for 
     *  the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the catalogue 
     *  @return a <code> ParticipantSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSmartCatalogueSession getParticipantSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getParticipantSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic participant catalogues for the 
     *  given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of a catalogue 
     *  @param  proxy a proxy 
     *  @return a <code> ParticipantSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParticipantSmartCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ParticipantSmartCatalogueSession getParticipantSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getParticipantSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result lookup 
     *  service. 
     *
     *  @return an <code> ResultLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultLookupSession getResultLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ResultLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultLookupSession getResultLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result lookup 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultLookupSession getResultLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result lookup 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ResultLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultLookupSession getResultLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result query 
     *  service. 
     *
     *  @return an <code> ResultQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultQuerySession getResultQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ResultQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultQuerySession getResultQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result query 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultQuerySession getResultQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result query 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ResultQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultQuerySession getResultQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result search 
     *  service. 
     *
     *  @return an <code> ResultSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSearchSession getResultSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ResultSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSearchSession getResultSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result search 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSearchSession getResultSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result search 
     *  service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ResultSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSearchSession getResultSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  administration service. 
     *
     *  @return an <code> ResultAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultAdminSession getResultAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ResultAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultAdminSession getResultAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultAdminSession getResultAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ResultAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResultAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultAdminSession getResultAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  notification service. 
     *
     *  @param  resultReceiver the receiver 
     *  @return an <code> ResultNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resultReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultNotificationSession getResultNotificationSession(org.osid.offering.ResultReceiver resultReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  notification service. 
     *
     *  @param  resultReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return an <code> ResultNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resultReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultNotificationSession getResultNotificationSession(org.osid.offering.ResultReceiver resultReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  notification service for the given catalogue. 
     *
     *  @param  resultReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> ResultNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> resultReceiver </code> 
     *          or <code> catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultNotificationSession getResultNotificationSessionForCatalogue(org.osid.offering.ResultReceiver resultReceiver, 
                                                                                                org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the result 
     *  notification service for the given catalogue. 
     *
     *  @param  resultReceiver the receiver 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ResultNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> resultReceiver, 
     *          catalogueId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultNotificationSession getResultNotificationSessionForCatalogue(org.osid.offering.ResultReceiver resultReceiver, 
                                                                                                org.osid.id.Id catalogueId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the session for retrieving result to catalogue mappings. 
     *
     *  @return an <code> ResultCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultCatalogueSession getResultCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultCatalogueSession not implemented");
    }


    /**
     *  Gets the session for retrieving result to catalogue mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ResultCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultCatalogueSession getResultCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultCatalogueSession not implemented");
    }


    /**
     *  Gets the session for assigning result to catalogue mappings. 
     *
     *  @return an <code> ResultCatalogueAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultCatalogueAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultCatalogueAssignmentSession getResultCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning result to catalogue mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ResultCatalogueAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultCatalogueAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultCatalogueAssignmentSession getResultCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the result smart catalogue for the 
     *  given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the catalogue 
     *  @return an <code> ResultSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultSmartCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSmartCatalogueSession getResultSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getResultSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic result catalogues for the given 
     *  catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of a catalogue 
     *  @param  proxy a proxy 
     *  @return an <code> ResultSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException <code> catalogueId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResultSmartCatalogue() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.ResultSmartCatalogueSession getResultSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getResultSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  lookup service. 
     *
     *  @return a <code> CatalogueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueLookupSession getCatalogueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCatalogueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueLookupSession getCatalogueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCatalogueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  query service. 
     *
     *  @return a <code> CatalogueQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuerySession getCatalogueQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCatalogueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuerySession getCatalogueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCatalogueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  search service. 
     *
     *  @return a <code> CatalogueSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueSearchSession getCatalogueSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCatalogueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueSearchSession getCatalogueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCatalogueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  administrative service. 
     *
     *  @return a <code> CatalogueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueAdminSession getCatalogueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCatalogueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueAdminSession getCatalogueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCatalogueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  notification service. 
     *
     *  @param  catalogueReceiver the receiver 
     *  @return a <code> CatalogueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueNotificationSession getCatalogueNotificationSession(org.osid.offering.CatalogueReceiver catalogueReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCatalogueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  notification service. 
     *
     *  @param  catalogueReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CatalogueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueNotificationSession getCatalogueNotificationSession(org.osid.offering.CatalogueReceiver catalogueReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCatalogueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  hierarchy service. 
     *
     *  @return a <code> CatalogueHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueHierarchySession getCatalogueHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCatalogueHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogueHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueHierarchySession getCatalogueHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCatalogueHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  hierarchy design service. 
     *
     *  @return a <code> CatalogueHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueHierarchyDesignSession getCatalogueHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getCatalogueHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the catalogue 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CatalogueHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueHierarchyDesignSession getCatalogueHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getCatalogueHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> OfferingBatchManager. </code> 
     *
     *  @return a <code> OfferingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.OfferingBatchManager getOfferingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingBatchManager not implemented");
    }


    /**
     *  Gets a <code> OfferingBatchProxyManager. </code> 
     *
     *  @return a <code> OfferingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.batch.OfferingBatchProxyManager getOfferingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> OfferingRulesManager. </code> 
     *
     *  @return a <code> OfferingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingRulesManager getOfferingRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingManager.getOfferingRulesManager not implemented");
    }


    /**
     *  Gets a <code> OfferingRulesProxyManager. </code> 
     *
     *  @return a <code> OfferingRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsOfferingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingRulesProxyManager getOfferingRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.OfferingProxyManager.getOfferingRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.canonicalUnitRecordTypes.clear();
        this.canonicalUnitRecordTypes.clear();

        this.canonicalUnitSearchRecordTypes.clear();
        this.canonicalUnitSearchRecordTypes.clear();

        this.offeringRecordTypes.clear();
        this.offeringRecordTypes.clear();

        this.offeringSearchRecordTypes.clear();
        this.offeringSearchRecordTypes.clear();

        this.participantRecordTypes.clear();
        this.participantRecordTypes.clear();

        this.participantSearchRecordTypes.clear();
        this.participantSearchRecordTypes.clear();

        this.resultRecordTypes.clear();
        this.resultRecordTypes.clear();

        this.resultSearchRecordTypes.clear();
        this.resultSearchRecordTypes.clear();

        this.catalogueRecordTypes.clear();
        this.catalogueRecordTypes.clear();

        this.catalogueSearchRecordTypes.clear();
        this.catalogueSearchRecordTypes.clear();

        return;
    }
}
