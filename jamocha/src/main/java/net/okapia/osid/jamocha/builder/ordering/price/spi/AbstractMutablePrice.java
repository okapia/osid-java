//
// AbstractMutablePrice.java
//
//     Defines a mutable Price.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.price.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Price</code>.
 */

public abstract class AbstractMutablePrice
    extends net.okapia.osid.jamocha.ordering.price.spi.AbstractPrice
    implements org.osid.ordering.Price,
               net.okapia.osid.jamocha.builder.ordering.price.PriceMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this price. 
     *
     *  @param record price record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addPriceRecord(org.osid.ordering.records.PriceRecord record, org.osid.type.Type recordType) {
        super.addPriceRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Enables this price. Enabling an operable overrides any
     *  enabling rule that may exist.
     *  
     *  @param enabled <code>true</code> if enabled, <code>false<code>
     *         otherwise
     */
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        return;
    }


    /**
     *  Disables this price. Disabling an operable overrides any
     *  enabling rule that may exist.
     *
     *  @param disabled <code> true </code> if this object is
     *         disabled, <code> false </code> otherwise
     */
    
    @Override
    public void setDisabled(boolean disabled) {
        super.setDisabled(disabled);
        return;
    }


    /**
     *  Sets the operational flag.
     *
     *  @param operational <code>true</code>if operational,
     *         <code>false</code> if not operational
     */
    
    @Override
    public void setOperational(boolean operational) {
        super.setOperational(operational);
        return;
    }


    /**
     *  Sets the display name for this price.
     *
     *  @param displayName the name for this price
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this price.
     *
     *  @param description the description of this price
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the rule.
     *
     *  @param rule the rule
     *  @throws org.osid.NullArgumentException <code>rule</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setRule(org.osid.rules.Rule rule) {
        super.setRule(rule);
        return;
    }


    /**
     *  Sets the price schedule.
     *
     *  @param schedule a price schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    @Override
    public void setPriceSchedule(org.osid.ordering.PriceSchedule schedule) {
        super.setPriceSchedule(schedule);
        return;
    }


    /**
     *  Sets the minimum quantity.
     *
     *  @param quantity a minimum quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    @Override
    public void setMinimumQuantity(long quantity) {
        super.setMinimumQuantity(quantity);
        return;
    }


    /**
     *  Sets the maximum quantity.
     *
     *  @param quantity a maximum quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    @Override
    public void setMaximumQuantity(long quantity) {
        super.setMaximumQuantity(quantity);
        return;
    }


    /**
     *  Sets the demographic.
     *
     *  @param demographic a demographic
     *  @throws org.osid.NullArgumentException
     *          <code>demographic</code> is <code>null</code>
     */

    @Override
    public void setDemographic(org.osid.resource.Resource demographic) {
        super.setDemographic(demographic);
        return;
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    @Override
    public void setAmount(org.osid.financials.Currency amount) {
        super.setAmount(amount);
        return;
    }


    /**
     *  Sets the recurring interval.
     *
     *  @param interval a recurring interval
     *  @throws org.osid.NullArgumentException
     *          <code>interval</code> is <code>null</code>
     */

    @Override
    public void setRecurringInterval(org.osid.calendaring.Duration interval) {
        super.setRecurringInterval(interval);
        return;
    }
}

