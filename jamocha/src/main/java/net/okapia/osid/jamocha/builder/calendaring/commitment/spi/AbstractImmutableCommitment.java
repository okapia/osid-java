//
// AbstractImmutableCommitment.java
//
//     Wraps a mutable Commitment to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.commitment.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Commitment</code> to hide modifiers. This
 *  wrapper provides an immutized Commitment from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying commitment whose state changes are visible.
 */

public abstract class AbstractImmutableCommitment
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.calendaring.Commitment {

    private final org.osid.calendaring.Commitment commitment;


    /**
     *  Constructs a new <code>AbstractImmutableCommitment</code>.
     *
     *  @param commitment the commitment to immutablize
     *  @throws org.osid.NullArgumentException <code>commitment</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCommitment(org.osid.calendaring.Commitment commitment) {
        super(commitment);
        this.commitment = commitment;
        return;
    }


    /**
     *  Gets the event <code> Id. </code> 
     *
     *  @return the event <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEventId() {
        return (this.commitment.getEventId());
    }


    /**
     *  Gets the <code> Event. </code> 
     *
     *  @return the event 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getEvent()
        throws org.osid.OperationFailedException {

        return (this.commitment.getEvent());
    }


    /**
     *  Gets the resource <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.commitment.getResourceId());
    }


    /**
     *  Gets the <code> Resource. </code> 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.commitment.getResource());
    }


    /**
     *  Gets the record corresponding to the given <code> Commitment </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> commitmentRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(commitmentRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  commitmentRecordType the type of the record to retrieve 
     *  @return the commitment record 
     *  @throws org.osid.NullArgumentException <code> commitmentRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(commitmentRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.records.CommitmentRecord getCommitmentRecord(org.osid.type.Type commitmentRecordType)
        throws org.osid.OperationFailedException {

        return (this.commitment.getCommitmentRecord(commitmentRecordType));
    }
}

