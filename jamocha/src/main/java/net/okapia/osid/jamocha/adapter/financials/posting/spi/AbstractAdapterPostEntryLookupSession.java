//
// AbstractAdapterPostEntryLookupSession.java
//
//    A PostEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.posting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A PostEntry lookup session adapter.
 */

public abstract class AbstractAdapterPostEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.financials.posting.PostEntryLookupSession {

    private final org.osid.financials.posting.PostEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPostEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPostEntryLookupSession(org.osid.financials.posting.PostEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code PostEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPostEntries() {
        return (this.session.canLookupPostEntries());
    }


    /**
     *  A complete view of the {@code PostEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePostEntryView() {
        this.session.useComparativePostEntryView();
        return;
    }


    /**
     *  A complete view of the {@code PostEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPostEntryView() {
        this.session.usePlenaryPostEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include post entries in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the {@code PostEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code PostEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code PostEntry} and
     *  retained for compatibility.
     *
     *  @param postEntryId {@code Id} of the {@code PostEntry}
     *  @return the post entry
     *  @throws org.osid.NotFoundException {@code postEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code postEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntry getPostEntry(org.osid.id.Id postEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntry(postEntryId));
    }


    /**
     *  Gets a {@code PostEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  postEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code PostEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  postEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code PostEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code postEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByIds(org.osid.id.IdList postEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByIds(postEntryIds));
    }


    /**
     *  Gets a {@code PostEntryList} corresponding to the given
     *  post entry genus {@code Type} which does not include
     *  post entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryGenusType a postEntry genus type 
     *  @return the returned {@code PostEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code postEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByGenusType(postEntryGenusType));
    }


    /**
     *  Gets a {@code PostEntryList} corresponding to the given
     *  post entry genus {@code Type} and include any additional
     *  post entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryGenusType a postEntry genus type 
     *  @return the returned {@code PostEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code postEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByParentGenusType(org.osid.type.Type postEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByParentGenusType(postEntryGenusType));
    }


    /**
     *  Gets a {@code PostEntryList} containing the given
     *  post entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postEntryRecordType a postEntry record type 
     *  @return the returned {@code PostEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code postEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByRecordType(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByRecordType(postEntryRecordType));
    }


    /**
     *  Gets a {@code PostEntryList} for the given post.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  postId a post {@code Id} 
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.NullArgumentException {@code postId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesForPost(org.osid.id.Id postId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPostEntriesForPost(postId));
    }


    /**
     *  Gets a {@code PostEntryList} in the given fiscal period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  fiscalPeriodId a fiscal period {@code Id} 
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodId} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByFiscalPeriod(fiscalPeriodId));
    }


    /**
     *  Gets a {@code PostEntryList} posted within given date range
     *  inclusive.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code from} or {@code 
     *          to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByDate(from, to));
    }


    /**
     *  Gets a {@code PostEntryList} for the given account. 
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account {@code Id} 
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.NullArgumentException {@code accountId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccount(org.osid.id.Id accountId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByAccount(accountId));
    }


    /**
     *  Gets a {@code PostEntryList} for the given account in a fiscal
     *  period. 
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account {@code Id} 
     *  @param  fiscalPeriodId a fiscal period {@code Id} 
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.NullArgumentException {@code accountId} or 
     *          {@code fiscalPeriodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndFiscalPeriod(org.osid.id.Id accountId, 
                                                                                            org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByAccountAndFiscalPeriod(accountId, fiscalPeriodId));
    }


    /**
     *  Gets a {@code PostEntryList} for the given activity.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param activityId an activity {@code Id}
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.NullArgumentException {@code activityId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByActivity(activityId));
    }


    /**
     *  Gets a {@code PostEntryList} for the given activity in a
     *  fiscal period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  activityId an activity {@code Id} 
     *  @param  fiscalPeriodId a fiscal period {@code Id} 
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.NullArgumentException {@code activityId} or 
     *          {@code fiscalPeriodId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByActivityAndFiscalPeriod(org.osid.id.Id activityId, 
                                                                                             org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByActivityAndFiscalPeriod(activityId, fiscalPeriodId));
    }


    /**
     *  Gets a {@code PostEntryList} for the given activity and 
     *  account. 
     *  
     *   In plenary mode, the returned list contains all known post
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account {@code Id} 
     *  @param  activityId an activity {@code Id} 
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.NullArgumentException {@code accountId} or 
     *          {@code activityId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndActivity(org.osid.id.Id accountId, 
                                                                                        org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByAccountAndActivity(accountId, activityId));
    }

    
    /**
     *  Gets a {@code PostEntryList} for the given account in a fiscal
     *  period.
     *  
     *  In plenary mode, the returned list contains all known post
     *  entriesq or an error results. Otherwise, the returned list may
     *  contain only those post entries that are accessible through
     *  this session.
     *
     *  @param  accountId an account {@code Id} 
     *  @param  activityId an activity {@code Id} 
     *  @param  fiscalPeriodId a fiscal period {@code Id} 
     *  @return the returned {@code PostEntry} list 
     *  @throws org.osid.NullArgumentException {@code accountId,
     *         activityId}, or {@code fiscalPeriodId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntriesByAccountAndActivityAndFiscalPeriod(org.osid.id.Id accountId, 
                                                                                                       org.osid.id.Id activityId, 
                                                                                                       org.osid.id.Id fiscalPeriodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntriesByAccountAndActivityAndFiscalPeriod(accountId, activityId, fiscalPeriodId));
    }
    

    /**
     *  Gets all {@code PostEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  post entries or an error results. Otherwise, the returned list
     *  may contain only those post entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code PostEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPostEntries());
    }
}
