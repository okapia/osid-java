//
// AbstractSignalEnablerQueryInspector.java
//
//     A template for making a SignalEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.signalenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for signal enablers.
 */

public abstract class AbstractSignalEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.mapping.path.rules.SignalEnablerQueryInspector {

    private final java.util.Collection<org.osid.mapping.path.rules.records.SignalEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the signal <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSignalIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the signal query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.SignalQueryInspector[] getRuledSignalTerms() {
        return (new org.osid.mapping.path.SignalQueryInspector[0]);
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given signal enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a signal enabler implementing the requested record.
     *
     *  @param signalEnablerRecordType a signal enabler record type
     *  @return the signal enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>signalEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(signalEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.SignalEnablerQueryInspectorRecord getSignalEnablerQueryInspectorRecord(org.osid.type.Type signalEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.SignalEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(signalEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(signalEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this signal enabler query. 
     *
     *  @param signalEnablerQueryInspectorRecord signal enabler query inspector
     *         record
     *  @param signalEnablerRecordType signalEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSignalEnablerQueryInspectorRecord(org.osid.mapping.path.rules.records.SignalEnablerQueryInspectorRecord signalEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type signalEnablerRecordType) {

        addRecordType(signalEnablerRecordType);
        nullarg(signalEnablerRecordType, "signal enabler record type");
        this.records.add(signalEnablerQueryInspectorRecord);        
        return;
    }
}
