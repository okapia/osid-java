//
// AbstractForumBatchManager.java
//
//     An adapter for a ForumBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.forum.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ForumBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterForumBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.forum.batch.ForumBatchManager>
    implements org.osid.forum.batch.ForumBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterForumBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterForumBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterForumBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterForumBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of posts is available. 
     *
     *  @return <code> true </code> if a post bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostBatchAdmin() {
        return (getAdapteeManager().supportsPostBatchAdmin());
    }


    /**
     *  Tests if bulk administration of replies is available. 
     *
     *  @return <code> true </code> if a reply bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReplyBatchAdmin() {
        return (getAdapteeManager().supportsReplyBatchAdmin());
    }


    /**
     *  Tests if bulk administration of forums is available. 
     *
     *  @return <code> true </code> if a forum bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumBatchAdmin() {
        return (getAdapteeManager().supportsForumBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk post 
     *  administration service. 
     *
     *  @return a <code> PostBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.batch.PostBatchAdminSession getPostBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPostBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk post 
     *  administration service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> PostBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.batch.PostBatchAdminSession getPostBatchAdminSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPostBatchAdminSessionForForum(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk reply 
     *  administration service. 
     *
     *  @return a <code> ReplyBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReplyBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.batch.ReplyBatchAdminSession getReplyBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReplyBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk reply 
     *  administration service for the given forum. 
     *
     *  @param  forumId the <code> Id </code> of the <code> Forum </code> 
     *  @return a <code> ReplyBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Forum </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> forumId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReplyBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.batch.ReplyBatchAdminSession getReplyBatchAdminSessionForForum(org.osid.id.Id forumId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReplyBatchAdminSessionForForum(forumId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk forum 
     *  administration service. 
     *
     *  @return a <code> ForumBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.batch.ForumBatchAdminSession getForumBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getForumBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
