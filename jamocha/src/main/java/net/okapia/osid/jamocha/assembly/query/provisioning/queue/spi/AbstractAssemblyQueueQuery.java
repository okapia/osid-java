//
// AbstractAssemblyQueueQuery.java
//
//     A QueueQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.provisioning.queue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A QueueQuery that stores terms.
 */

public abstract class AbstractAssemblyQueueQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.provisioning.QueueQuery,
               org.osid.provisioning.QueueQueryInspector,
               org.osid.provisioning.QueueSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.QueueQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.QueueQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.provisioning.records.QueueSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyQueueQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyQueueQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getBrokerIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        getAssembler().clearTerms(getBrokerIdColumn());
        return;
    }


    /**
     *  Gets the broker <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrokerIdTerms() {
        return (getAssembler().getIdTerms(getBrokerIdColumn()));
    }


    /**
     *  Orders the results by broker. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBroker(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBrokerColumn(), style);
        return;
    }


    /**
     *  Gets the BrokerId column name.
     *
     * @return the column name
     */

    protected String getBrokerIdColumn() {
        return ("broker_id");
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        getAssembler().clearTerms(getBrokerColumn());
        return;
    }


    /**
     *  Gets the broker query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQueryInspector[] getBrokerTerms() {
        return (new org.osid.provisioning.BrokerQueryInspector[0]);
    }


    /**
     *  Tests if a broker search order is available. 
     *
     *  @return <code> true </code> if a broker search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the broker search order. 
     *
     *  @return the broker search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBrokerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchOrder getBrokerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBrokerSearchOrder() is false");
    }


    /**
     *  Gets the Broker column name.
     *
     * @return the column name
     */

    protected String getBrokerColumn() {
        return ("broker");
    }


    /**
     *  Matches queues of the given size inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchSize(long start, long end, boolean match) {
        getAssembler().addCardinalRangeTerm(getSizeColumn(), start, end, match);
        return;
    }


    /**
     *  Matches queues with any known size. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySize(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getSizeColumn(), match);
        return;
    }


    /**
     *  Clears the size query terms. 
     */

    @OSID @Override
    public void clearSizeTerms() {
        getAssembler().clearTerms(getSizeColumn());
        return;
    }


    /**
     *  Gets the size query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getSizeTerms() {
        return (getAssembler().getCardinalRangeTerms(getSizeColumn()));
    }


    /**
     *  Orders the results by queue size. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySize(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSizeColumn(), style);
        return;
    }


    /**
     *  Gets the Size column name.
     *
     * @return the column name
     */

    protected String getSizeColumn() {
        return ("size");
    }


    /**
     *  Matches queues whose estimated waiting time is in the given range 
     *  inclusive,. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEWA(org.osid.calendaring.Duration start, 
                         org.osid.calendaring.Duration end, boolean match) {
        getAssembler().addDurationRangeTerm(getEWAColumn(), start, end, match);
        return;
    }


    /**
     *  Matches queues with any estimated wiating time. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyEWA(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getEWAColumn(), match);
        return;
    }


    /**
     *  Clears the estimated waiting time query terms. 
     */

    @OSID @Override
    public void clearEWATerms() {
        getAssembler().clearTerms(getEWAColumn());
        return;
    }


    /**
     *  Gets the ewa query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getEWATerms() {
        return (getAssembler().getDurationRangeTerms(getEWAColumn()));
    }


    /**
     *  Orders the results by estimated waiting time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEWA(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEWAColumn(), style);
        return;
    }


    /**
     *  Gets the EWA column name.
     *
     * @return the column name
     */

    protected String getEWAColumn() {
        return ("ewa");
    }


    /**
     *  Matches queues that permit requests for specific provisionables. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchCanSpecifyProvisionable(boolean match) {
        getAssembler().addBooleanTerm(getCanSpecifyProvisionableColumn(), match);
        return;
    }


    /**
     *  Clears the can request provisionable terms. 
     */

    @OSID @Override
    public void clearCanSpecifyProvisionableTerms() {
        getAssembler().clearTerms(getCanSpecifyProvisionableColumn());
        return;
    }


    /**
     *  Gets the can request provisionable query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCanSpecifyProvisionableTerms() {
        return (getAssembler().getBooleanTerms(getCanSpecifyProvisionableColumn()));
    }


    /**
     *  Orders the results by the can request provisionable flag.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByCanSpecifyProvisionable(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCanSpecifyProvisionableColumn(), style);
        return;
    }


    /**
     *  Gets the CanSpecifyProvisionable column name.
     *
     * @return the column name
     */

    protected String getCanSpecifyProvisionableColumn() {
        return ("can_specify_provisionable");
    }


    /**
     *  Sets the request <code> Id </code> for this query. 
     *
     *  @param  requestId the request <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestId(org.osid.id.Id requestId, boolean match) {
        getAssembler().addIdTerm(getRequestIdColumn(), requestId, match);
        return;
    }


    /**
     *  Clears the request <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestIdTerms() {
        getAssembler().clearTerms(getRequestIdColumn());
        return;
    }


    /**
     *  Gets the request <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRequestIdTerms() {
        return (getAssembler().getIdTerms(getRequestIdColumn()));
    }


    /**
     *  Gets the RequestId column name.
     *
     * @return the column name
     */

    protected String getRequestIdColumn() {
        return ("request_id");
    }


    /**
     *  Tests if a <code> RequestQuery </code> is available. 
     *
     *  @return <code> true </code> if a request query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Request. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the request query 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuery getRequestQuery() {
        throw new org.osid.UnimplementedException("supportsRequestQuery() is false");
    }


    /**
     *  Matches queues that have any request. 
     *
     *  @param  match <code> true </code> to match queues with any request, 
     *          <code> false </code> to match queues with no request 
     */

    @OSID @Override
    public void matchAnyRequest(boolean match) {
        getAssembler().addIdWildcardTerm(getRequestColumn(), match);
        return;
    }


    /**
     *  Clears the request query terms. 
     */

    @OSID @Override
    public void clearRequestTerms() {
        getAssembler().clearTerms(getRequestColumn());
        return;
    }


    /**
     *  Gets the request query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQueryInspector[] getRequestTerms() {
        return (new org.osid.provisioning.RequestQueryInspector[0]);
    }


    /**
     *  Gets the Request column name.
     *
     * @return the column name
     */

    protected String getRequestColumn() {
        return ("request");
    }


    /**
     *  Sets the broker <code> Id </code> for this query to match queues 
     *  assigned to brokers. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id brokerId, boolean match) {
        getAssembler().addIdTerm(getDistributorIdColumn(), brokerId, match);
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        getAssembler().clearTerms(getDistributorIdColumn());
        return;
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (getAssembler().getIdTerms(getDistributorIdColumn()));
    }


    /**
     *  Gets the DistributorId column name.
     *
     * @return the column name
     */

    protected String getDistributorIdColumn() {
        return ("distributor_id");
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        getAssembler().clearTerms(getDistributorColumn());
        return;
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }


    /**
     *  Gets the Distributor column name.
     *
     * @return the column name
     */

    protected String getDistributorColumn() {
        return ("distributor");
    }


    /**
     *  Tests if this queue supports the given record
     *  <code>Type</code>.
     *
     *  @param  queueRecordType a queue record type 
     *  @return <code>true</code> if the queueRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type queueRecordType) {
        for (org.osid.provisioning.records.QueueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  queueRecordType the queue record type 
     *  @return the queue query record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.QueueQueryRecord getQueueQueryRecord(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.QueueQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  queueRecordType the queue record type 
     *  @return the queue query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.QueueQueryInspectorRecord getQueueQueryInspectorRecord(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.QueueQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(queueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param queueRecordType the queue record type
     *  @return the queue search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.QueueSearchOrderRecord getQueueSearchOrderRecord(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.QueueSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(queueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this queue. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param queueQueryRecord the queue query record
     *  @param queueQueryInspectorRecord the queue query inspector
     *         record
     *  @param queueSearchOrderRecord the queue search order record
     *  @param queueRecordType queue record type
     *  @throws org.osid.NullArgumentException
     *          <code>queueQueryRecord</code>,
     *          <code>queueQueryInspectorRecord</code>,
     *          <code>queueSearchOrderRecord</code> or
     *          <code>queueRecordTypequeue</code> is
     *          <code>null</code>
     */
            
    protected void addQueueRecords(org.osid.provisioning.records.QueueQueryRecord queueQueryRecord, 
                                      org.osid.provisioning.records.QueueQueryInspectorRecord queueQueryInspectorRecord, 
                                      org.osid.provisioning.records.QueueSearchOrderRecord queueSearchOrderRecord, 
                                      org.osid.type.Type queueRecordType) {

        addRecordType(queueRecordType);

        nullarg(queueQueryRecord, "queue query record");
        nullarg(queueQueryInspectorRecord, "queue query inspector record");
        nullarg(queueSearchOrderRecord, "queue search odrer record");

        this.queryRecords.add(queueQueryRecord);
        this.queryInspectorRecords.add(queueQueryInspectorRecord);
        this.searchOrderRecords.add(queueSearchOrderRecord);
        
        return;
    }
}
