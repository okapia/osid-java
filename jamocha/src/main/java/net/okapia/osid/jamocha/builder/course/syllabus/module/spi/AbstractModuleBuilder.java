//
// AbstractModule.java
//
//     Defines a Module builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.syllabus.module.spi;


/**
 *  Defines a <code>Module</code> builder.
 */

public abstract class AbstractModuleBuilder<T extends AbstractModuleBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidGovernatorBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.syllabus.module.ModuleMiter module;


    /**
     *  Constructs a new <code>AbstractModuleBuilder</code>.
     *
     *  @param module the module to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractModuleBuilder(net.okapia.osid.jamocha.builder.course.syllabus.module.ModuleMiter module) {
        super(module);
        this.module = module;
        return;
    }


    /**
     *  Builds the module.
     *
     *  @return the new module
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.syllabus.Module build() {
        (new net.okapia.osid.jamocha.builder.validator.course.syllabus.module.ModuleValidator(getValidations())).validate(this.module);
        return (new net.okapia.osid.jamocha.builder.course.syllabus.module.ImmutableModule(this.module));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the module miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.syllabus.module.ModuleMiter getMiter() {
        return (this.module);
    }


    /**
     *  Sets the syllabus.
     *
     *  @param syllabus a syllabus
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>syllabus</code> is <code>null</code>
     */

    public T syllabus(org.osid.course.syllabus.Syllabus syllabus) {
        getMiter().setSyllabus(syllabus);
        return (self());
    }


    /**
     *  Adds a Module record.
     *
     *  @param record a module record
     *  @param recordType the type of module record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.syllabus.records.ModuleRecord record, org.osid.type.Type recordType) {
        getMiter().addModuleRecord(record, recordType);
        return (self());
    }
}       


