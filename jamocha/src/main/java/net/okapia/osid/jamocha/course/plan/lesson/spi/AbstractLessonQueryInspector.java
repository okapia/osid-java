//
// AbstractLessonQueryInspector.java
//
//     A template for making a LessonQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.lesson.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for lessons.
 */

public abstract class AbstractLessonQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.plan.LessonQueryInspector {

    private final java.util.Collection<org.osid.course.plan.records.LessonQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the plan <code> Id </code> terms. 
     *
     *  @return the plan <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPlanIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the plan terms. 
     *
     *  @return the plan terms 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQueryInspector[] getPlanTerms() {
        return (new org.osid.course.plan.PlanQueryInspector[0]);
    }


    /**
     *  Gets the docet <code> Id </code> terms. 
     *
     *  @return the docet <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDocetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the docet terms. 
     *
     *  @return the docet terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetQueryInspector[] getDocetTerms() {
        return (new org.osid.course.syllabus.DocetQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the planned start time query terms. 
     *
     *  @return the time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getPlannedStartTimeTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets actual start time query terms. 
     *
     *  @return the terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getActualStartTimeTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the starting activity <code> Id </code> terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActualStartingActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the starting activity terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActualStartingActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets completed query terms. 
     *
     *  @return the terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCompleteTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets skipped query terms. 
     *
     *  @return the terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getSkippedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the actual end time query terms. 
     *
     *  @return the time time terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getActualEndTimeTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the ending activity <code> Id </code> terms. 
     *
     *  @return the activity <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActualEndingActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ending activity terms. 
     *
     *  @return the activity terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActualEndingActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the actual time spent terms. 
     *
     *  @return the duration terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getActualTimeSpentTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given lesson query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a lesson implementing the requested record.
     *
     *  @param lessonRecordType a lesson record type
     *  @return the lesson query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>lessonRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(lessonRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.LessonQueryInspectorRecord getLessonQueryInspectorRecord(org.osid.type.Type lessonRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.LessonQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(lessonRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(lessonRecordType + " is not supported");
    }


    /**
     *  Adds a record to this lesson query. 
     *
     *  @param lessonQueryInspectorRecord lesson query inspector
     *         record
     *  @param lessonRecordType lesson record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addLessonQueryInspectorRecord(org.osid.course.plan.records.LessonQueryInspectorRecord lessonQueryInspectorRecord, 
                                                   org.osid.type.Type lessonRecordType) {

        addRecordType(lessonRecordType);
        nullarg(lessonRecordType, "lesson record type");
        this.records.add(lessonQueryInspectorRecord);        
        return;
    }
}
