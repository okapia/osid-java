//
// InvariantMapDepotLookupSession
//
//    Implements a Depot lookup service backed by a fixed collection of
//    depots.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.installation;


/**
 *  Implements a Depot lookup service backed by a fixed
 *  collection of depots. The depots are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapDepotLookupSession
    extends net.okapia.osid.jamocha.core.installation.spi.AbstractMapDepotLookupSession
    implements org.osid.installation.DepotLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapDepotLookupSession</code> with no
     *  depots.
     */

    public InvariantMapDepotLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDepotLookupSession</code> with a single
     *  depot.
     *  
     *  @throws org.osid.NullArgumentException {@code depot}
     *          is <code>null</code>
     */

    public InvariantMapDepotLookupSession(org.osid.installation.Depot depot) {
        putDepot(depot);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDepotLookupSession</code> using an array
     *  of depots.
     *  
     *  @throws org.osid.NullArgumentException {@code depots}
     *          is <code>null</code>
     */

    public InvariantMapDepotLookupSession(org.osid.installation.Depot[] depots) {
        putDepots(depots);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDepotLookupSession</code> using a
     *  collection of depots.
     *
     *  @throws org.osid.NullArgumentException {@code depots}
     *          is <code>null</code>
     */

    public InvariantMapDepotLookupSession(java.util.Collection<? extends org.osid.installation.Depot> depots) {
        putDepots(depots);
        return;
    }
}
