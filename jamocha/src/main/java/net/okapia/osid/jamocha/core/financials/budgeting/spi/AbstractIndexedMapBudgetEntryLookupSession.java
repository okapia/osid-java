//
// AbstractIndexedMapBudgetEntryLookupSession.java
//
//    A simple framework for providing a BudgetEntry lookup service
//    backed by a fixed collection of budget entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.budgeting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a BudgetEntry lookup service backed by a
 *  fixed collection of budget entries. The budget entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some budget entries may be compatible
 *  with more types than are indicated through these budget entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BudgetEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBudgetEntryLookupSession
    extends AbstractMapBudgetEntryLookupSession
    implements org.osid.financials.budgeting.BudgetEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.financials.budgeting.BudgetEntry> budgetEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.budgeting.BudgetEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.financials.budgeting.BudgetEntry> budgetEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.financials.budgeting.BudgetEntry>());


    /**
     *  Makes a <code>BudgetEntry</code> available in this session.
     *
     *  @param  budgetEntry a budget entry
     *  @throws org.osid.NullArgumentException <code>budgetEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBudgetEntry(org.osid.financials.budgeting.BudgetEntry budgetEntry) {
        super.putBudgetEntry(budgetEntry);

        this.budgetEntriesByGenus.put(budgetEntry.getGenusType(), budgetEntry);
        
        try (org.osid.type.TypeList types = budgetEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.budgetEntriesByRecord.put(types.getNextType(), budgetEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a budget entry from this session.
     *
     *  @param budgetEntryId the <code>Id</code> of the budget entry
     *  @throws org.osid.NullArgumentException <code>budgetEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBudgetEntry(org.osid.id.Id budgetEntryId) {
        org.osid.financials.budgeting.BudgetEntry budgetEntry;
        try {
            budgetEntry = getBudgetEntry(budgetEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.budgetEntriesByGenus.remove(budgetEntry.getGenusType());

        try (org.osid.type.TypeList types = budgetEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.budgetEntriesByRecord.remove(types.getNextType(), budgetEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBudgetEntry(budgetEntryId);
        return;
    }


    /**
     *  Gets a <code>BudgetEntryList</code> corresponding to the given
     *  budget entry genus <code>Type</code> which does not include
     *  budget entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known budget entries or an error results. Otherwise,
     *  the returned list may contain only those budget entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  budgetEntryGenusType a budget entry genus type 
     *  @return the returned <code>BudgetEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByGenusType(org.osid.type.Type budgetEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.budgeting.budgetentry.ArrayBudgetEntryList(this.budgetEntriesByGenus.get(budgetEntryGenusType)));
    }


    /**
     *  Gets a <code>BudgetEntryList</code> containing the given
     *  budget entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known budget entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  budget entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  budgetEntryRecordType a budget entry record type 
     *  @return the returned <code>budgetEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetEntryList getBudgetEntriesByRecordType(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.financials.budgeting.budgetentry.ArrayBudgetEntryList(this.budgetEntriesByRecord.get(budgetEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.budgetEntriesByGenus.clear();
        this.budgetEntriesByRecord.clear();

        super.close();

        return;
    }
}
