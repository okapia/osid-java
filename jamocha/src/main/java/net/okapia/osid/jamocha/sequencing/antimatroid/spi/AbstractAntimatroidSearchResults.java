//
// AbstractAntimatroidSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.antimatroid.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAntimatroidSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.sequencing.AntimatroidSearchResults {

    private org.osid.sequencing.AntimatroidList antimatroids;
    private final org.osid.sequencing.AntimatroidQueryInspector inspector;
    private final java.util.Collection<org.osid.sequencing.records.AntimatroidSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAntimatroidSearchResults.
     *
     *  @param antimatroids the result set
     *  @param antimatroidQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>antimatroids</code>
     *          or <code>antimatroidQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAntimatroidSearchResults(org.osid.sequencing.AntimatroidList antimatroids,
                                            org.osid.sequencing.AntimatroidQueryInspector antimatroidQueryInspector) {
        nullarg(antimatroids, "antimatroids");
        nullarg(antimatroidQueryInspector, "antimatroid query inspectpr");

        this.antimatroids = antimatroids;
        this.inspector = antimatroidQueryInspector;

        return;
    }


    /**
     *  Gets the antimatroid list resulting from a search.
     *
     *  @return an antimatroid list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidList getAntimatroids() {
        if (this.antimatroids == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.sequencing.AntimatroidList antimatroids = this.antimatroids;
        this.antimatroids = null;
	return (antimatroids);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.sequencing.AntimatroidQueryInspector getAntimatroidQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  antimatroid search record <code> Type. </code> This method must
     *  be used to retrieve an antimatroid implementing the requested
     *  record.
     *
     *  @param antimatroidSearchRecordType an antimatroid search 
     *         record type 
     *  @return the antimatroid search
     *  @throws org.osid.NullArgumentException
     *          <code>antimatroidSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(antimatroidSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.AntimatroidSearchResultsRecord getAntimatroidSearchResultsRecord(org.osid.type.Type antimatroidSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.sequencing.records.AntimatroidSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(antimatroidSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(antimatroidSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record antimatroid search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAntimatroidRecord(org.osid.sequencing.records.AntimatroidSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "antimatroid record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
