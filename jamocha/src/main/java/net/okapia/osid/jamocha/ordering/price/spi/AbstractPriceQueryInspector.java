//
// AbstractPriceQueryInspector.java
//
//     A template for making a PriceQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.price.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for prices.
 */

public abstract class AbstractPriceQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.ordering.PriceQueryInspector {

    private final java.util.Collection<org.osid.ordering.records.PriceQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the price schedule <code> Id </code> terms. 
     *
     *  @return the price schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPriceScheduleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the price schedule terms. 
     *
     *  @return the price schedule terms 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQueryInspector[] getPriceScheduleTerms() {
        return (new org.osid.ordering.PriceScheduleQueryInspector[0]);
    }


    /**
     *  Gets the minimum quantity terms. 
     *
     *  @return the minimum quantity terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMinimumQuantityTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the maximum quantity terms. 
     *
     *  @return the maximum quantity terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getMaximumQuantityTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the demographic <code> Id </code> terms. 
     *
     *  @return the demographic <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDemographicIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the demographic terms. 
     *
     *  @return the demographic terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getDemographicTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the amount terms. 
     *
     *  @return the amount terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the minimum amount terms. 
     *
     *  @return the minimum amount terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyTerm[] getMinimumAmountTerms() {
        return (new org.osid.search.terms.CurrencyTerm[0]);
    }


    /**
     *  Gets the recurring interval terms. 
     *
     *  @return the recurring interval terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRecurringIntervalTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the item <code> Id </code> terms. 
     *
     *  @return the item <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the item terms. 
     *
     *  @return the item terms 
     */

    @OSID @Override
    public org.osid.ordering.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.ordering.ItemQueryInspector[0]);
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given price query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a price implementing the requested record.
     *
     *  @param priceRecordType a price record type
     *  @return the price query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>priceRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(priceRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.PriceQueryInspectorRecord getPriceQueryInspectorRecord(org.osid.type.Type priceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.PriceQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(priceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(priceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this price query. 
     *
     *  @param priceQueryInspectorRecord price query inspector
     *         record
     *  @param priceRecordType price record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPriceQueryInspectorRecord(org.osid.ordering.records.PriceQueryInspectorRecord priceQueryInspectorRecord, 
                                                   org.osid.type.Type priceRecordType) {

        addRecordType(priceRecordType);
        nullarg(priceRecordType, "price record type");
        this.records.add(priceQueryInspectorRecord);        
        return;
    }
}
