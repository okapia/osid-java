//
// UnknownRace.java
//
//     Defines an unknown Race.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.voting.race;


/**
 *  Defines an unknown <code>Race</code>.
 */

public final class UnknownRace
    extends net.okapia.osid.jamocha.nil.voting.race.spi.AbstractUnknownRace
    implements org.osid.voting.Race {


    /**
     *  Constructs a new <code>UnknownRace</code>.
     */

    public UnknownRace() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownRace</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownRace(boolean optional) {
        super(optional);
        addRaceRecord(new RaceRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Race.
     *
     *  @return an unknown Race
     */

    public static org.osid.voting.Race create() {
        return (net.okapia.osid.jamocha.builder.validator.voting.race.RaceValidator.validateRace(new UnknownRace()));
    }


    public class RaceRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.voting.records.RaceRecord {

        
        protected RaceRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
