//
// AbstractActionGroup.java
//
//     Defines an ActionGroup.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.actiongroup.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>ActionGroup</code>.
 */

public abstract class AbstractActionGroup
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.control.ActionGroup {

    private final java.util.Collection<org.osid.control.Action> actions = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.ActionGroupRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the action <code> Ids. </code> 
     *
     *  @return the action <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getActionIds() {
        try (org.osid.control.ActionList actions = getActions()) {
            return (new net.okapia.osid.jamocha.adapter.converter.control.action.ActionToIdList(actions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the actions. 
     *
     *  @return the actions 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.control.ActionList getActions()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.control.action.ArrayActionList(this.actions));
    }


    /**
     *  Adds an action.
     *
     *  @param action an action
     *  @throws org.osid.NullArgumentException
     *          <code>action</code> is <code>null</code>
     */

    protected void addAction(org.osid.control.Action action) {
        nullarg(action, "action");
        this.actions.add(action);
        return;
    }


    /**
     *  Sets all the actions.
     *
     *  @param actions a collection of actions
     *  @throws org.osid.NullArgumentException <code>actions</code> is
     *          <code>null</code>
     */

    protected void setActions(java.util.Collection<org.osid.control.Action> actions) {
        nullarg(actions, "actions");
        this.actions.clear();
        this.actions.addAll(actions);
        return;
    }


    /**
     *  Tests if this actionGroup supports the given record
     *  <code>Type</code>.
     *
     *  @param  actionGroupRecordType an action group record type 
     *  @return <code>true</code> if the actionGroupRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type actionGroupRecordType) {
        for (org.osid.control.records.ActionGroupRecord record : this.records) {
            if (record.implementsRecordType(actionGroupRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  actionGroupRecordType the action group record type 
     *  @return the action group record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionGroupRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionGroupRecord getActionGroupRecord(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionGroupRecord record : this.records) {
            if (record.implementsRecordType(actionGroupRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionGroupRecordType + " is not supported");
    }


    /**
     *  Adds a record to this action group. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param actionGroupRecord the action group record
     *  @param actionGroupRecordType action group record type
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecord</code> or
     *          <code>actionGroupRecordTypeactionGroup</code> is
     *          <code>null</code>
     */
            
    protected void addActionGroupRecord(org.osid.control.records.ActionGroupRecord actionGroupRecord, 
                                     org.osid.type.Type actionGroupRecordType) {

        addRecordType(actionGroupRecordType);
        this.records.add(actionGroupRecord);
        
        return;
    }
}
