//
// InlineActivityUnitNotifier.java
//
//     A callback interface for performing activity unit
//     notifications.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.spi;


/**
 *  A callback interface for performing activity unit
 *  notifications.
 */

public interface InlineActivityUnitNotifier {



    /**
     *  Notifies the creation of a new activity unit.
     *
     *  @param activityUnitId the {@code Id} of the new 
     *         activity unit
     *  @param peer1Id the {@code Id} of the peer1
     *  @throws org.osid.NullArgumentException {@code [objectId]}
     *          or {@code peer1Id} is {@code null}
     */

    public void newActivityUnit(org.osid.id.Id activityUnitId, org.osid.id.Id peer1Id);


    /**
     *  Notifies the change of an updated activity unit.
     *
     *  @param activityUnitId the {@code Id} of the changed
     *         activity unit
     *  @param peer1Id the {@code Id} of the peer1
     *  @throws org.osid.NullArgumentException {@code [objectId]} or
     *          {@code peer1Id} is {@code null}
     */

    public void changedActivityUnit(org.osid.id.Id activityUnitId, org.osid.id.Id peer1Id);


    /**
     *  Notifies a deleted activity unit.
     *
     *  @param activityUnitId the {@code Id} of the deleted
     *         activity unit
     *  @param peer1Id the {@code Id} of the peer1
     *  @throws org.osid.NullArgumentException {@code [objectId]} or
     *          {@code peer1Id} is {@code null}
     */

    public void deletedActivityUnit(org.osid.id.Id activityUnitId, org.osid.id.Id peer1Id);

}
