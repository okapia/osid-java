//
// MutableMapAuctionConstrainerEnablerLookupSession
//
//    Implements an AuctionConstrainerEnabler lookup service backed by a collection of
//    auctionConstrainerEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules;


/**
 *  Implements an AuctionConstrainerEnabler lookup service backed by a collection of
 *  auction constrainer enablers. The auction constrainer enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of auction constrainer enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAuctionConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.bidding.rules.spi.AbstractMapAuctionConstrainerEnablerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapAuctionConstrainerEnablerLookupSession}
     *  with no auction constrainer enablers.
     *
     *  @param auctionHouse the auction house
     *  @throws org.osid.NullArgumentException {@code auctionHouse} is
     *          {@code null}
     */

      public MutableMapAuctionConstrainerEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse) {
        setAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionConstrainerEnablerLookupSession} with a
     *  single auctionConstrainerEnabler.
     *
     *  @param auctionHouse the auction house  
     *  @param auctionConstrainerEnabler an auction constrainer enabler
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainerEnabler} is {@code null}
     */

    public MutableMapAuctionConstrainerEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                           org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler) {
        this(auctionHouse);
        putAuctionConstrainerEnabler(auctionConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionConstrainerEnablerLookupSession}
     *  using an array of auction constrainer enablers.
     *
     *  @param auctionHouse the auction house
     *  @param auctionConstrainerEnablers an array of auction constrainer enablers
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainerEnablers} is {@code null}
     */

    public MutableMapAuctionConstrainerEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                           org.osid.bidding.rules.AuctionConstrainerEnabler[] auctionConstrainerEnablers) {
        this(auctionHouse);
        putAuctionConstrainerEnablers(auctionConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAuctionConstrainerEnablerLookupSession}
     *  using a collection of auction constrainer enablers.
     *
     *  @param auctionHouse the auction house
     *  @param auctionConstrainerEnablers a collection of auction constrainer enablers
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code auctionConstrainerEnablers} is {@code null}
     */

    public MutableMapAuctionConstrainerEnablerLookupSession(org.osid.bidding.AuctionHouse auctionHouse,
                                           java.util.Collection<? extends org.osid.bidding.rules.AuctionConstrainerEnabler> auctionConstrainerEnablers) {

        this(auctionHouse);
        putAuctionConstrainerEnablers(auctionConstrainerEnablers);
        return;
    }

    
    /**
     *  Makes an {@code AuctionConstrainerEnabler} available in this session.
     *
     *  @param auctionConstrainerEnabler an auction constrainer enabler
     *  @throws org.osid.NullArgumentException {@code auctionConstrainerEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionConstrainerEnabler(org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler) {
        super.putAuctionConstrainerEnabler(auctionConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of auction constrainer enablers available in this session.
     *
     *  @param auctionConstrainerEnablers an array of auction constrainer enablers
     *  @throws org.osid.NullArgumentException {@code auctionConstrainerEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putAuctionConstrainerEnablers(org.osid.bidding.rules.AuctionConstrainerEnabler[] auctionConstrainerEnablers) {
        super.putAuctionConstrainerEnablers(auctionConstrainerEnablers);
        return;
    }


    /**
     *  Makes collection of auction constrainer enablers available in this session.
     *
     *  @param auctionConstrainerEnablers a collection of auction constrainer enablers
     *  @throws org.osid.NullArgumentException {@code auctionConstrainerEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putAuctionConstrainerEnablers(java.util.Collection<? extends org.osid.bidding.rules.AuctionConstrainerEnabler> auctionConstrainerEnablers) {
        super.putAuctionConstrainerEnablers(auctionConstrainerEnablers);
        return;
    }


    /**
     *  Removes an AuctionConstrainerEnabler from this session.
     *
     *  @param auctionConstrainerEnablerId the {@code Id} of the auction constrainer enabler
     *  @throws org.osid.NullArgumentException {@code auctionConstrainerEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAuctionConstrainerEnabler(org.osid.id.Id auctionConstrainerEnablerId) {
        super.removeAuctionConstrainerEnabler(auctionConstrainerEnablerId);
        return;
    }    
}
