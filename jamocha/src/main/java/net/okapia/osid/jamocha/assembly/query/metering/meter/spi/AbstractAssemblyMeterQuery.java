//
// AbstractAssemblyMeterQuery.java
//
//     A MeterQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.metering.meter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A MeterQuery that stores terms.
 */

public abstract class AbstractAssemblyMeterQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.metering.MeterQuery,
               org.osid.metering.MeterQueryInspector,
               org.osid.metering.MeterSearchOrder {

    private final java.util.Collection<org.osid.metering.records.MeterQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.metering.records.MeterQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.metering.records.MeterSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyMeterQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyMeterQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the utility <code> Id </code> for this query. 
     *
     *  @param  utilityId the utility <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchUtilityId(org.osid.id.Id utilityId, boolean match) {
        getAssembler().addIdTerm(getUtilityIdColumn(), utilityId, match);
        return;
    }


    /**
     *  Clears the utility <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearUtilityIdTerms() {
        getAssembler().clearTerms(getUtilityIdColumn());
        return;
    }


    /**
     *  Gets the utility <code> Id </code> terms. 
     *
     *  @return the utility <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getUtilityIdTerms() {
        return (getAssembler().getIdTerms(getUtilityIdColumn()));
    }


    /**
     *  Gets the UtilityId column name.
     *
     * @return the column name
     */

    protected String getUtilityIdColumn() {
        return ("utility_id");
    }


    /**
     *  Tests if a <code> UtilityQuery </code> is available. 
     *
     *  @return <code> true </code> if a utility query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a utility. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the utility query 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuery getUtilityQuery() {
        throw new org.osid.UnimplementedException("supportsUtilityQuery() is false");
    }


    /**
     *  Clears the utility query terms. 
     */

    @OSID @Override
    public void clearUtilityTerms() {
        getAssembler().clearTerms(getUtilityColumn());
        return;
    }


    /**
     *  Gets the utility terms. 
     *
     *  @return the utility terms 
     */

    @OSID @Override
    public org.osid.metering.UtilityQueryInspector[] getUtilityTerms() {
        return (new org.osid.metering.UtilityQueryInspector[0]);
    }


    /**
     *  Gets the Utility column name.
     *
     * @return the column name
     */

    protected String getUtilityColumn() {
        return ("utility");
    }


    /**
     *  Tests if this meter supports the given record
     *  <code>Type</code>.
     *
     *  @param  meterRecordType a meter record type 
     *  @return <code>true</code> if the meterRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type meterRecordType) {
        for (org.osid.metering.records.MeterQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(meterRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  meterRecordType the meter record type 
     *  @return the meter query record 
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(meterRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.MeterQueryRecord getMeterQueryRecord(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.MeterQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(meterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(meterRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  meterRecordType the meter record type 
     *  @return the meter query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(meterRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.MeterQueryInspectorRecord getMeterQueryInspectorRecord(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.MeterQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(meterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(meterRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param meterRecordType the meter record type
     *  @return the meter search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>meterRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(meterRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.MeterSearchOrderRecord getMeterSearchOrderRecord(org.osid.type.Type meterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.MeterSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(meterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(meterRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this meter. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param meterQueryRecord the meter query record
     *  @param meterQueryInspectorRecord the meter query inspector
     *         record
     *  @param meterSearchOrderRecord the meter search order record
     *  @param meterRecordType meter record type
     *  @throws org.osid.NullArgumentException
     *          <code>meterQueryRecord</code>,
     *          <code>meterQueryInspectorRecord</code>,
     *          <code>meterSearchOrderRecord</code> or
     *          <code>meterRecordTypemeter</code> is
     *          <code>null</code>
     */
            
    protected void addMeterRecords(org.osid.metering.records.MeterQueryRecord meterQueryRecord, 
                                      org.osid.metering.records.MeterQueryInspectorRecord meterQueryInspectorRecord, 
                                      org.osid.metering.records.MeterSearchOrderRecord meterSearchOrderRecord, 
                                      org.osid.type.Type meterRecordType) {

        addRecordType(meterRecordType);

        nullarg(meterQueryRecord, "meter query record");
        nullarg(meterQueryInspectorRecord, "meter query inspector record");
        nullarg(meterSearchOrderRecord, "meter search odrer record");

        this.queryRecords.add(meterQueryRecord);
        this.queryInspectorRecords.add(meterQueryInspectorRecord);
        this.searchOrderRecords.add(meterSearchOrderRecord);
        
        return;
    }
}
