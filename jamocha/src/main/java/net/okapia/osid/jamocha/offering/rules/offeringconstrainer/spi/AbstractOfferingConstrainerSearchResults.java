//
// AbstractOfferingConstrainerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractOfferingConstrainerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.rules.OfferingConstrainerSearchResults {

    private org.osid.offering.rules.OfferingConstrainerList offeringConstrainers;
    private final org.osid.offering.rules.OfferingConstrainerQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractOfferingConstrainerSearchResults.
     *
     *  @param offeringConstrainers the result set
     *  @param offeringConstrainerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>offeringConstrainers</code>
     *          or <code>offeringConstrainerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractOfferingConstrainerSearchResults(org.osid.offering.rules.OfferingConstrainerList offeringConstrainers,
                                            org.osid.offering.rules.OfferingConstrainerQueryInspector offeringConstrainerQueryInspector) {
        nullarg(offeringConstrainers, "offering constrainers");
        nullarg(offeringConstrainerQueryInspector, "offering constrainer query inspectpr");

        this.offeringConstrainers = offeringConstrainers;
        this.inspector = offeringConstrainerQueryInspector;

        return;
    }


    /**
     *  Gets the offering constrainer list resulting from a search.
     *
     *  @return an offering constrainer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainers() {
        if (this.offeringConstrainers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.rules.OfferingConstrainerList offeringConstrainers = this.offeringConstrainers;
        this.offeringConstrainers = null;
	return (offeringConstrainers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.rules.OfferingConstrainerQueryInspector getOfferingConstrainerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  offering constrainer search record <code> Type. </code> This method must
     *  be used to retrieve an offeringConstrainer implementing the requested
     *  record.
     *
     *  @param offeringConstrainerSearchRecordType an offeringConstrainer search 
     *         record type 
     *  @return the offering constrainer search
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(offeringConstrainerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerSearchResultsRecord getOfferingConstrainerSearchResultsRecord(org.osid.type.Type offeringConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.rules.records.OfferingConstrainerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(offeringConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record offering constrainer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addOfferingConstrainerRecord(org.osid.offering.rules.records.OfferingConstrainerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "offering constrainer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
