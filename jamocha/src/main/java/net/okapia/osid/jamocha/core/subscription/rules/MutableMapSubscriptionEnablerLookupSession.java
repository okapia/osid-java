//
// MutableMapSubscriptionEnablerLookupSession
//
//    Implements a SubscriptionEnabler lookup service backed by a collection of
//    subscriptionEnablers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.rules;


/**
 *  Implements a SubscriptionEnabler lookup service backed by a collection of
 *  subscription enablers. The subscription enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of subscription enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapSubscriptionEnablerLookupSession
    extends net.okapia.osid.jamocha.core.subscription.rules.spi.AbstractMapSubscriptionEnablerLookupSession
    implements org.osid.subscription.rules.SubscriptionEnablerLookupSession {


    /**
     *  Constructs a new {@code MutableMapSubscriptionEnablerLookupSession}
     *  with no subscription enablers.
     *
     *  @param publisher the publisher
     *  @throws org.osid.NullArgumentException {@code publisher} is
     *          {@code null}
     */

      public MutableMapSubscriptionEnablerLookupSession(org.osid.subscription.Publisher publisher) {
        setPublisher(publisher);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSubscriptionEnablerLookupSession} with a
     *  single subscriptionEnabler.
     *
     *  @param publisher the publisher  
     *  @param subscriptionEnabler a subscription enabler
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code subscriptionEnabler} is {@code null}
     */

    public MutableMapSubscriptionEnablerLookupSession(org.osid.subscription.Publisher publisher,
                                           org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler) {
        this(publisher);
        putSubscriptionEnabler(subscriptionEnabler);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSubscriptionEnablerLookupSession}
     *  using an array of subscription enablers.
     *
     *  @param publisher the publisher
     *  @param subscriptionEnablers an array of subscription enablers
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code subscriptionEnablers} is {@code null}
     */

    public MutableMapSubscriptionEnablerLookupSession(org.osid.subscription.Publisher publisher,
                                           org.osid.subscription.rules.SubscriptionEnabler[] subscriptionEnablers) {
        this(publisher);
        putSubscriptionEnablers(subscriptionEnablers);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapSubscriptionEnablerLookupSession}
     *  using a collection of subscription enablers.
     *
     *  @param publisher the publisher
     *  @param subscriptionEnablers a collection of subscription enablers
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code subscriptionEnablers} is {@code null}
     */

    public MutableMapSubscriptionEnablerLookupSession(org.osid.subscription.Publisher publisher,
                                           java.util.Collection<? extends org.osid.subscription.rules.SubscriptionEnabler> subscriptionEnablers) {

        this(publisher);
        putSubscriptionEnablers(subscriptionEnablers);
        return;
    }

    
    /**
     *  Makes a {@code SubscriptionEnabler} available in this session.
     *
     *  @param subscriptionEnabler a subscription enabler
     *  @throws org.osid.NullArgumentException {@code subscriptionEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putSubscriptionEnabler(org.osid.subscription.rules.SubscriptionEnabler subscriptionEnabler) {
        super.putSubscriptionEnabler(subscriptionEnabler);
        return;
    }


    /**
     *  Makes an array of subscription enablers available in this session.
     *
     *  @param subscriptionEnablers an array of subscription enablers
     *  @throws org.osid.NullArgumentException {@code subscriptionEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putSubscriptionEnablers(org.osid.subscription.rules.SubscriptionEnabler[] subscriptionEnablers) {
        super.putSubscriptionEnablers(subscriptionEnablers);
        return;
    }


    /**
     *  Makes collection of subscription enablers available in this session.
     *
     *  @param subscriptionEnablers a collection of subscription enablers
     *  @throws org.osid.NullArgumentException {@code subscriptionEnablers{@code  is
     *          {@code null}
     */

    @Override
    public void putSubscriptionEnablers(java.util.Collection<? extends org.osid.subscription.rules.SubscriptionEnabler> subscriptionEnablers) {
        super.putSubscriptionEnablers(subscriptionEnablers);
        return;
    }


    /**
     *  Removes a SubscriptionEnabler from this session.
     *
     *  @param subscriptionEnablerId the {@code Id} of the subscription enabler
     *  @throws org.osid.NullArgumentException {@code subscriptionEnablerId{@code 
     *          is {@code null}
     */

    @Override
    public void removeSubscriptionEnabler(org.osid.id.Id subscriptionEnablerId) {
        super.removeSubscriptionEnabler(subscriptionEnablerId);
        return;
    }    
}
