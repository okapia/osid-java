//
// MutableIndexedMapCompositionLookupSession
//
//    Implements a Composition lookup service backed by a collection of
//    compositions indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements a Composition lookup service backed by a collection of
 *  compositions. The compositions are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some compositions may be compatible
 *  with more types than are indicated through these composition
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of compositions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCompositionLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractIndexedMapCompositionLookupSession
    implements org.osid.repository.CompositionLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCompositionLookupSession} with no compositions.
     *
     *  @param repository the repository
     *  @throws org.osid.NullArgumentException {@code repository}
     *          is {@code null}
     */

      public MutableIndexedMapCompositionLookupSession(org.osid.repository.Repository repository) {
        setRepository(repository);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompositionLookupSession} with a
     *  single composition.
     *  
     *  @param repository the repository
     *  @param  composition a single composition
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code composition} is {@code null}
     */

    public MutableIndexedMapCompositionLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.repository.Composition composition) {
        this(repository);
        putComposition(composition);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompositionLookupSession} using an
     *  array of compositions.
     *
     *  @param repository the repository
     *  @param  compositions an array of compositions
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositions} is {@code null}
     */

    public MutableIndexedMapCompositionLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.repository.Composition[] compositions) {
        this(repository);
        putCompositions(compositions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCompositionLookupSession} using a
     *  collection of compositions.
     *
     *  @param repository the repository
     *  @param  compositions a collection of compositions
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code compositions} is {@code null}
     */

    public MutableIndexedMapCompositionLookupSession(org.osid.repository.Repository repository,
                                                  java.util.Collection<? extends org.osid.repository.Composition> compositions) {

        this(repository);
        putCompositions(compositions);
        return;
    }
    

    /**
     *  Makes a {@code Composition} available in this session.
     *
     *  @param  composition a composition
     *  @throws org.osid.NullArgumentException {@code composition{@code  is
     *          {@code null}
     */

    @Override
    public void putComposition(org.osid.repository.Composition composition) {
        super.putComposition(composition);
        return;
    }


    /**
     *  Makes an array of compositions available in this session.
     *
     *  @param  compositions an array of compositions
     *  @throws org.osid.NullArgumentException {@code compositions{@code 
     *          is {@code null}
     */

    @Override
    public void putCompositions(org.osid.repository.Composition[] compositions) {
        super.putCompositions(compositions);
        return;
    }


    /**
     *  Makes collection of compositions available in this session.
     *
     *  @param  compositions a collection of compositions
     *  @throws org.osid.NullArgumentException {@code composition{@code  is
     *          {@code null}
     */

    @Override
    public void putCompositions(java.util.Collection<? extends org.osid.repository.Composition> compositions) {
        super.putCompositions(compositions);
        return;
    }


    /**
     *  Removes a Composition from this session.
     *
     *  @param compositionId the {@code Id} of the composition
     *  @throws org.osid.NullArgumentException {@code compositionId{@code  is
     *          {@code null}
     */

    @Override
    public void removeComposition(org.osid.id.Id compositionId) {
        super.removeComposition(compositionId);
        return;
    }    
}
