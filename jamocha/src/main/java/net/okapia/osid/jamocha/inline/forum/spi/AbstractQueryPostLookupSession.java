//
// AbstractQueryPostLookupSession.java
//
//    An inline adapter that maps a PostLookupSession to
//    a PostQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PostLookupSession to
 *  a PostQuerySession.
 */

public abstract class AbstractQueryPostLookupSession
    extends net.okapia.osid.jamocha.forum.spi.AbstractPostLookupSession
    implements org.osid.forum.PostLookupSession {

    private final org.osid.forum.PostQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPostLookupSession.
     *
     *  @param querySession the underlying post query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPostLookupSession(org.osid.forum.PostQuerySession querySession) {
        nullarg(querySession, "post query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Forum</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Forum Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getForumId() {
        return (this.session.getForumId());
    }


    /**
     *  Gets the <code>Forum</code> associated with this 
     *  session.
     *
     *  @return the <code>Forum</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Forum getForum()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getForum());
    }


    /**
     *  Tests if this user can perform <code>Post</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPosts() {
        return (this.session.canSearchPosts());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include posts in forums which are children
     *  of this forum in the forum hierarchy.
     */

    @OSID @Override
    public void useFederatedForumView() {
        this.session.useFederatedForumView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this forum only.
     */

    @OSID @Override
    public void useIsolatedForumView() {
        this.session.useIsolatedForumView();
        return;
    }
    
     
    /**
     *  Gets the <code>Post</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Post</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Post</code> and
     *  retained for compatibility.
     *
     *  @param  postId <code>Id</code> of the
     *          <code>Post</code>
     *  @return the post
     *  @throws org.osid.NotFoundException <code>postId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>postId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.Post getPost(org.osid.id.Id postId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();
        query.matchId(postId, true);
        org.osid.forum.PostList posts = this.session.getPostsByQuery(query);
        if (posts.hasNext()) {
            return (posts.getNextPost());
        } 
        
        throw new org.osid.NotFoundException(postId + " not found");
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  posts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Posts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  postIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>postIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByIds(org.osid.id.IdList postIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();

        try (org.osid.id.IdList ids = postIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> which does not include
     *  posts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();
        query.matchGenusType(postGenusType, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code>PostList</code> corresponding to the given
     *  post genus <code>Type</code> and include any additional
     *  posts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postGenusType a post genus type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByParentGenusType(org.osid.type.Type postGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();
        query.matchParentGenusType(postGenusType, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code>PostList</code> containing the given
     *  post record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  postRecordType a post record type 
     *  @return the returned <code>Post</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByRecordType(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();
        query.matchRecordType(postRecordType, true);
        return (this.session.getPostsByQuery(query));
    }

    
    /**
     *  Gets a <code> PostList </code> in the given date range
     *  inclusive.  <code> </code> In plenary mode, the returned list
     *  contains all known posts or an error results. Otherwise, the
     *  returned list may contain only those posts that are accessible
     *  through this session.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Post </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or
     *          <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByDate(org.osid.calendaring.DateTime from, 
                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();
        query.matchTimestamp(from, to, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code> PostList </code> for the given poster. <code>
     *  </code> In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list may
     *  contain only those posts that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> Post </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsForPoster(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();
        query.matchPosterId(resourceId, true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets a <code> PostList </code> by the given poster and in the
     *  given date range inclusive. <code> </code> In plenary mode,
     *  the returned list contains all known posts or an error
     *  results. Otherwise, the returned list may contain only those
     *  posts that are accessible through this session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Post </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.forum.PostList getPostsByDateForPoster(org.osid.id.Id resourceId, 
                                                           org.osid.calendaring.DateTime from, 
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();
        query.matchPosterId(resourceId,true);
        query.matchTimestamp(from, to, true);
        return (this.session.getPostsByQuery(query));
    }

    
    /**
     *  Gets all <code>Posts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  posts or an error results. Otherwise, the returned list
     *  may contain only those posts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Posts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.PostList getPosts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.forum.PostQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPostsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.forum.PostQuery getQuery() {
        org.osid.forum.PostQuery query = this.session.getPostQuery();
        return (query);
    }
}
