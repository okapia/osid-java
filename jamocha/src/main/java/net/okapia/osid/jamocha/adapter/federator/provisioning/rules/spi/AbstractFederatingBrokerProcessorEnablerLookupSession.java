//
// AbstractFederatingBrokerProcessorEnablerLookupSession.java
//
//     An abstract federating adapter for a BrokerProcessorEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BrokerProcessorEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBrokerProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession>
    implements org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new <code>AbstractFederatingBrokerProcessorEnablerLookupSession</code>.
     */

    protected AbstractFederatingBrokerProcessorEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>BrokerProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBrokerProcessorEnablers() {
        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            if (session.canLookupBrokerProcessorEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>BrokerProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBrokerProcessorEnablerView() {
        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            session.useComparativeBrokerProcessorEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>BrokerProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBrokerProcessorEnablerView() {
        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            session.usePlenaryBrokerProcessorEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include broker processor enablers in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only active broker processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveBrokerProcessorEnablerView() {
        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            session.useActiveBrokerProcessorEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive broker processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBrokerProcessorEnablerView() {
        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            session.useAnyStatusBrokerProcessorEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>BrokerProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BrokerProcessorEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>BrokerProcessorEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerId <code>Id</code> of the
     *          <code>BrokerProcessorEnabler</code>
     *  @return the broker processor enabler
     *  @throws org.osid.NotFoundException <code>brokerProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnabler getBrokerProcessorEnabler(org.osid.id.Id brokerProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            try {
                return (session.getBrokerProcessorEnabler(brokerProcessorEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(brokerProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  brokerProcessorEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>BrokerProcessorEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByIds(org.osid.id.IdList brokerProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.rules.brokerprocessorenabler.MutableBrokerProcessorEnablerList ret = new net.okapia.osid.jamocha.provisioning.rules.brokerprocessorenabler.MutableBrokerProcessorEnablerList();

        try (org.osid.id.IdList ids = brokerProcessorEnablerIds) {
            while (ids.hasNext()) {
                ret.addBrokerProcessorEnabler(getBrokerProcessorEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> corresponding
     *  to the given broker processor enabler genus <code>Type</code>
     *  which does not include broker processor enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an errorq results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerGenusType a brokerProcessorEnabler genus type 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByGenusType(org.osid.type.Type brokerProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.FederatingBrokerProcessorEnablerList ret = getBrokerProcessorEnablerList();

        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            ret.addBrokerProcessorEnablerList(session.getBrokerProcessorEnablersByGenusType(brokerProcessorEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> corresponding
     *  to the given broker processor enabler genus <code>Type</code>
     *  and include any additional broker processor enablers with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, broker processor enablers are returned that are currently
     *  active. In any status mode, active and inactive broker processor enablers
     *  are returned.
     *
     *  @param  brokerProcessorEnablerGenusType a brokerProcessorEnabler genus type 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByParentGenusType(org.osid.type.Type brokerProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.FederatingBrokerProcessorEnablerList ret = getBrokerProcessorEnablerList();

        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            ret.addBrokerProcessorEnablerList(session.getBrokerProcessorEnablersByParentGenusType(brokerProcessorEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> containing the
     *  given broker processor enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  brokerProcessorEnablerRecordType a brokerProcessorEnabler record type 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByRecordType(org.osid.type.Type brokerProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.FederatingBrokerProcessorEnablerList ret = getBrokerProcessorEnablerList();

        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            ret.addBrokerProcessorEnablerList(session.getBrokerProcessorEnablersByRecordType(brokerProcessorEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session.
     *  
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>BrokerProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.FederatingBrokerProcessorEnablerList ret = getBrokerProcessorEnablerList();

        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            ret.addBrokerProcessorEnablerList(session.getBrokerProcessorEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>BrokerProcessorEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.FederatingBrokerProcessorEnablerList ret = getBrokerProcessorEnablerList();

        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            ret.addBrokerProcessorEnablerList(session.getBrokerProcessorEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>BrokerProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known broker
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those broker processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, broker processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  broker processor enablers are returned.
     *
     *  @return a list of <code>BrokerProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.FederatingBrokerProcessorEnablerList ret = getBrokerProcessorEnablerList();

        for (org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession session : getSessions()) {
            ret.addBrokerProcessorEnablerList(session.getBrokerProcessorEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.FederatingBrokerProcessorEnablerList getBrokerProcessorEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.ParallelBrokerProcessorEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.brokerprocessorenabler.CompositeBrokerProcessorEnablerList());
        }
    }
}
