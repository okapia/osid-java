//
// InvariantIndexedMapDemographicEnablerLookupSession
//
//    Implements a DemographicEnabler lookup service backed by a fixed
//    collection of demographicEnablers indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.demographic;


/**
 *  Implements a DemographicEnabler lookup service backed by a fixed
 *  collection of demographic enablers. The demographic enablers are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some demographic enablers may be compatible
 *  with more types than are indicated through these demographic enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapDemographicEnablerLookupSession
    extends net.okapia.osid.jamocha.core.resource.demographic.spi.AbstractIndexedMapDemographicEnablerLookupSession
    implements org.osid.resource.demographic.DemographicEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapDemographicEnablerLookupSession} using an
     *  array of demographicEnablers.
     *
     *  @param bin the bin
     *  @param demographicEnablers an array of demographic enablers
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code demographicEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                    org.osid.resource.demographic.DemographicEnabler[] demographicEnablers) {

        setBin(bin);
        putDemographicEnablers(demographicEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapDemographicEnablerLookupSession} using a
     *  collection of demographic enablers.
     *
     *  @param bin the bin
     *  @param demographicEnablers a collection of demographic enablers
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code demographicEnablers} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapDemographicEnablerLookupSession(org.osid.resource.Bin bin,
                                                    java.util.Collection<? extends org.osid.resource.demographic.DemographicEnabler> demographicEnablers) {

        setBin(bin);
        putDemographicEnablers(demographicEnablers);
        return;
    }
}
