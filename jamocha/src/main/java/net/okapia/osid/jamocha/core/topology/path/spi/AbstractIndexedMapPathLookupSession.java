//
// AbstractIndexedMapPathLookupSession.java
//
//    A simple framework for providing a Path lookup service
//    backed by a fixed collection of paths with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Path lookup service backed by a
 *  fixed collection of paths. The paths are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some paths may be compatible
 *  with more types than are indicated through these path
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Paths</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapPathLookupSession
    extends AbstractMapPathLookupSession
    implements org.osid.topology.path.PathLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.topology.path.Path> pathsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.path.Path>());
    private final MultiMap<org.osid.type.Type, org.osid.topology.path.Path> pathsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.path.Path>());


    /**
     *  Makes a <code>Path</code> available in this session.
     *
     *  @param  path a path
     *  @throws org.osid.NullArgumentException <code>path<code> is
     *          <code>null</code>
     */

    @Override
    protected void putPath(org.osid.topology.path.Path path) {
        super.putPath(path);

        this.pathsByGenus.put(path.getGenusType(), path);
        
        try (org.osid.type.TypeList types = path.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.pathsByRecord.put(types.getNextType(), path);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a path from this session.
     *
     *  @param pathId the <code>Id</code> of the path
     *  @throws org.osid.NullArgumentException <code>pathId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removePath(org.osid.id.Id pathId) {
        org.osid.topology.path.Path path;
        try {
            path = getPath(pathId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.pathsByGenus.remove(path.getGenusType());

        try (org.osid.type.TypeList types = path.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.pathsByRecord.remove(types.getNextType(), path);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removePath(pathId);
        return;
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> which does not include
     *  paths of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known paths or an error results. Otherwise,
     *  the returned list may contain only those paths that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.path.path.ArrayPathList(this.pathsByGenus.get(pathGenusType)));
    }


    /**
     *  Gets a <code>PathList</code> containing the given
     *  path record <code>Type</code>. In plenary mode, the
     *  returned list contains all known paths or an error
     *  results. Otherwise, the returned list may contain only those
     *  paths that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned <code>path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.path.path.ArrayPathList(this.pathsByRecord.get(pathRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.pathsByGenus.clear();
        this.pathsByRecord.clear();

        super.close();

        return;
    }
}
