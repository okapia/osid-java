//
// AbstractIntersection.java
//
//     Defines an Intersection builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.intersection.spi;


/**
 *  Defines an <code>Intersection</code> builder.
 */

public abstract class AbstractIntersectionBuilder<T extends AbstractIntersectionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.mapping.path.intersection.IntersectionMiter intersection;


    /**
     *  Constructs a new <code>AbstractIntersectionBuilder</code>.
     *
     *  @param intersection the intersection to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractIntersectionBuilder(net.okapia.osid.jamocha.builder.mapping.path.intersection.IntersectionMiter intersection) {
        super(intersection);
        this.intersection = intersection;
        return;
    }


    /**
     *  Builds the intersection.
     *
     *  @return the new intersection
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.mapping.path.Intersection build() {
        (new net.okapia.osid.jamocha.builder.validator.mapping.path.intersection.IntersectionValidator(getValidations())).validate(this.intersection);
        return (new net.okapia.osid.jamocha.builder.mapping.path.intersection.ImmutableIntersection(this.intersection));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the intersection miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.mapping.path.intersection.IntersectionMiter getMiter() {
        return (this.intersection);
    }


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate a coordinate
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public T coordinate(org.osid.mapping.Coordinate coordinate) {
        getMiter().setCoordinate(coordinate);
        return (self());
    }


    /**
     *  Adds a path.
     *
     *  @param path a path
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public T path(org.osid.mapping.path.Path path) {
        getMiter().addPath(path);
        return (self());
    }


    /**
     *  Sets all the paths.
     *
     *  @param paths a collection of paths
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>paths</code> is
     *          <code>null</code>
     */

    public T paths(java.util.Collection<org.osid.mapping.path.Path> paths) {
        getMiter().setPaths(paths);
        return (self());
    }


    /**
     *  Sets the rotary flag.
     *
     *  @return the builder
     */

    public T rotary() {
        getMiter().setRotary(true);
        return (self());
    }


    /**
     *  Unsets the rotary flag.
     *
     *  @return the builder
     */

    public T notRotary() {
        getMiter().setRotary(false);
        return (self());
    }


    /**
     *  Sets the fork flag.
     *
     *  @return the builder
     */

    public T fork() {
        getMiter().setFork(true);
        return (self());
    }


    /**
     *  Unsets the fork flag.
     *
     *  @return the builder
     */

    public T spoon() {
        getMiter().setFork(false);
        return (self());
    }


    /**
     *  Adds an Intersection record.
     *
     *  @param record an intersection record
     *  @param recordType the type of intersection record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.mapping.path.records.IntersectionRecord record, org.osid.type.Type recordType) {
        getMiter().addIntersectionRecord(record, recordType);
        return (self());
    }
}       


