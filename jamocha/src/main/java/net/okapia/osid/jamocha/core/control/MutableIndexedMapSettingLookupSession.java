//
// MutableIndexedMapSettingLookupSession
//
//    Implements a Setting lookup service backed by a collection of
//    settings indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements a Setting lookup service backed by a collection of
 *  settings. The settings are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some settings may be compatible
 *  with more types than are indicated through these setting
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of settings can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapSettingLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractIndexedMapSettingLookupSession
    implements org.osid.control.SettingLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapSettingLookupSession} with no settings.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system}
     *          is {@code null}
     */

      public MutableIndexedMapSettingLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSettingLookupSession} with a
     *  single setting.
     *  
     *  @param system the system
     *  @param  setting a single setting
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code setting} is {@code null}
     */

    public MutableIndexedMapSettingLookupSession(org.osid.control.System system,
                                                  org.osid.control.Setting setting) {
        this(system);
        putSetting(setting);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSettingLookupSession} using an
     *  array of settings.
     *
     *  @param system the system
     *  @param  settings an array of settings
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code settings} is {@code null}
     */

    public MutableIndexedMapSettingLookupSession(org.osid.control.System system,
                                                  org.osid.control.Setting[] settings) {
        this(system);
        putSettings(settings);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapSettingLookupSession} using a
     *  collection of settings.
     *
     *  @param system the system
     *  @param  settings a collection of settings
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code settings} is {@code null}
     */

    public MutableIndexedMapSettingLookupSession(org.osid.control.System system,
                                                  java.util.Collection<? extends org.osid.control.Setting> settings) {

        this(system);
        putSettings(settings);
        return;
    }
    

    /**
     *  Makes a {@code Setting} available in this session.
     *
     *  @param  setting a setting
     *  @throws org.osid.NullArgumentException {@code setting{@code  is
     *          {@code null}
     */

    @Override
    public void putSetting(org.osid.control.Setting setting) {
        super.putSetting(setting);
        return;
    }


    /**
     *  Makes an array of settings available in this session.
     *
     *  @param  settings an array of settings
     *  @throws org.osid.NullArgumentException {@code settings{@code 
     *          is {@code null}
     */

    @Override
    public void putSettings(org.osid.control.Setting[] settings) {
        super.putSettings(settings);
        return;
    }


    /**
     *  Makes collection of settings available in this session.
     *
     *  @param  settings a collection of settings
     *  @throws org.osid.NullArgumentException {@code setting{@code  is
     *          {@code null}
     */

    @Override
    public void putSettings(java.util.Collection<? extends org.osid.control.Setting> settings) {
        super.putSettings(settings);
        return;
    }


    /**
     *  Removes a Setting from this session.
     *
     *  @param settingId the {@code Id} of the setting
     *  @throws org.osid.NullArgumentException {@code settingId{@code  is
     *          {@code null}
     */

    @Override
    public void removeSetting(org.osid.id.Id settingId) {
        super.removeSetting(settingId);
        return;
    }    
}
