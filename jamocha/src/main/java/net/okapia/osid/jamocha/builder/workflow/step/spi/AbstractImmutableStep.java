//
// AbstractImmutableStep.java
//
//     Wraps a mutable Step to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.step.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Step</code> to hide modifiers. This
 *  wrapper provides an immutized Step from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying step whose state changes are visible.
 */

public abstract class AbstractImmutableStep
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.workflow.Step {

    private final org.osid.workflow.Step step;


    /**
     *  Constructs a new <code>AbstractImmutableStep</code>.
     *
     *  @param step the step to immutablize
     *  @throws org.osid.NullArgumentException <code>step</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableStep(org.osid.workflow.Step step) {
        super(step);
        this.step = step;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the process. 
     *
     *  @return the process <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProcessId() {
        return (this.step.getProcessId());
    }


    /**
     *  Gets the process. 
     *
     *  @return the process 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.workflow.Process getProcess()
        throws org.osid.OperationFailedException {

        return (this.step.getProcess());
    }


    /**
     *  Gets the <code> Ids </code> of the resources working in this step. 
     *
     *  @return the resource <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getResourceIds() {
        return (this.step.getResourceIds());
    }


    /**
     *  Gets the resources working in this step. 
     *
     *  @return the resources 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getResources()
        throws org.osid.OperationFailedException {

        return (this.step.getResources());
    }


    /**
     *  Gets the <code> Ids </code> of the valid states entering this step.
     *
     *  @return the state <code> Ids </code>
     */

    @OSID @Override
    public org.osid.id.IdList getInputStateIds() {
        return (this.step.getInputStateIds());
    }


    /**
     *  Gets the valid states to enter this step.
     *
     *  @return the states
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.process.StateList getInputStates()
        throws org.osid.OperationFailedException {

        return (this.step.getInputStates());
    }

    /**
     *  Gets the <code> Id </code> of the state of the work upon completing 
     *  this step. 
     *
     *  @return the state <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getNextStateId() {
        return (this.step.getNextStateId());
    }


    /**
     *  Gets the state of the work upon completing this step. 
     *
     *  @return the state 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.State getNextState()
        throws org.osid.OperationFailedException {

        return (this.step.getNextState());
    }


    /**
     *  Gets the step record corresponding to the given <code> Step </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> stepRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(stepRecordType) </code> is <code> true </code> . 
     *
     *  @param  stepRecordType the type of step record to retrieve 
     *  @return the step record 
     *  @throws org.osid.NullArgumentException <code> stepRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(stepRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.records.StepRecord getStepRecord(org.osid.type.Type stepRecordType)
        throws org.osid.OperationFailedException {

        return (this.step.getStepRecord(stepRecordType));
    }
}

