//
// AbstractAdapterObjectiveBankLookupSession.java
//
//    An ObjectiveBank lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.learning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An ObjectiveBank lookup session adapter.
 */

public abstract class AbstractAdapterObjectiveBankLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.learning.ObjectiveBankLookupSession {

    private final org.osid.learning.ObjectiveBankLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterObjectiveBankLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterObjectiveBankLookupSession(org.osid.learning.ObjectiveBankLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code ObjectiveBank} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupObjectiveBanks() {
        return (this.session.canLookupObjectiveBanks());
    }


    /**
     *  A complete view of the {@code ObjectiveBank} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeObjectiveBankView() {
        this.session.useComparativeObjectiveBankView();
        return;
    }


    /**
     *  A complete view of the {@code ObjectiveBank} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryObjectiveBankView() {
        this.session.usePlenaryObjectiveBankView();
        return;
    }

     
    /**
     *  Gets the {@code ObjectiveBank} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ObjectiveBank} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ObjectiveBank} and
     *  retained for compatibility.
     *
     *  @param objectiveBankId {@code Id} of the {@code ObjectiveBank}
     *  @return the objective bank
     *  @throws org.osid.NotFoundException {@code objectiveBankId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code objectiveBankId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectiveBank(objectiveBankId));
    }


    /**
     *  Gets an {@code ObjectiveBankList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  objectiveBanks specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ObjectiveBanks} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  objectiveBankIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ObjectiveBank} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveBankIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByIds(org.osid.id.IdList objectiveBankIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectiveBanksByIds(objectiveBankIds));
    }


    /**
     *  Gets an {@code ObjectiveBankList} corresponding to the given
     *  objective bank genus {@code Type} which does not include
     *  objective banks of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveBankGenusType an objectiveBank genus type 
     *  @return the returned {@code ObjectiveBank} list
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveBankGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByGenusType(org.osid.type.Type objectiveBankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectiveBanksByGenusType(objectiveBankGenusType));
    }


    /**
     *  Gets an {@code ObjectiveBankList} corresponding to the given
     *  objective bank genus {@code Type} and include any additional
     *  objective banks with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveBankGenusType an objectiveBank genus type 
     *  @return the returned {@code ObjectiveBank} list
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveBankGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByParentGenusType(org.osid.type.Type objectiveBankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectiveBanksByParentGenusType(objectiveBankGenusType));
    }


    /**
     *  Gets an {@code ObjectiveBankList} containing the given
     *  objective bank record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveBankRecordType an objectiveBank record type 
     *  @return the returned {@code ObjectiveBank} list
     *  @throws org.osid.NullArgumentException
     *          {@code objectiveBankRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByRecordType(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectiveBanksByRecordType(objectiveBankRecordType));
    }


    /**
     *  Gets an {@code ObjectiveBankList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code ObjectiveBank} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectiveBanksByProvider(resourceId));
    }


    /**
     *  Gets all {@code ObjectiveBanks}. 
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code ObjectiveBanks} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getObjectiveBanks());
    }
}
