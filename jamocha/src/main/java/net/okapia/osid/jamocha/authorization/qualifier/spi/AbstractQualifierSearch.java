//
// AbstractQualifierSearch.java
//
//     A template for making a Qualifier Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.qualifier.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing qualifier searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractQualifierSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.authorization.QualifierSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.authorization.records.QualifierSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.authorization.QualifierSearchOrder qualifierSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of qualifiers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  qualifierIds list of qualifiers
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongQualifiers(org.osid.id.IdList qualifierIds) {
        while (qualifierIds.hasNext()) {
            try {
                this.ids.add(qualifierIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongQualifiers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of qualifier Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getQualifierIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  qualifierSearchOrder qualifier search order 
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>qualifierSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderQualifierResults(org.osid.authorization.QualifierSearchOrder qualifierSearchOrder) {
	this.qualifierSearchOrder = qualifierSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.authorization.QualifierSearchOrder getQualifierSearchOrder() {
	return (this.qualifierSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given qualifier search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a qualifier implementing the requested record.
     *
     *  @param qualifierSearchRecordType a qualifier search record
     *         type
     *  @return the qualifier search record
     *  @throws org.osid.NullArgumentException
     *          <code>qualifierSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(qualifierSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authorization.records.QualifierSearchRecord getQualifierSearchRecord(org.osid.type.Type qualifierSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.authorization.records.QualifierSearchRecord record : this.records) {
            if (record.implementsRecordType(qualifierSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(qualifierSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this qualifier search. 
     *
     *  @param qualifierSearchRecord qualifier search record
     *  @param qualifierSearchRecordType qualifier search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQualifierSearchRecord(org.osid.authorization.records.QualifierSearchRecord qualifierSearchRecord, 
                                           org.osid.type.Type qualifierSearchRecordType) {

        addRecordType(qualifierSearchRecordType);
        this.records.add(qualifierSearchRecord);        
        return;
    }
}
