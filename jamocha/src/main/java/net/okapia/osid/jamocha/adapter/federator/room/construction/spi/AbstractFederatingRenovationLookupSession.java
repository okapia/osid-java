//
// AbstractFederatingRenovationLookupSession.java
//
//     An abstract federating adapter for a RenovationLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.room.construction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RenovationLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRenovationLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.room.construction.RenovationLookupSession>
    implements org.osid.room.construction.RenovationLookupSession {

    private boolean parallel = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();


    /**
     *  Constructs a new <code>AbstractFederatingRenovationLookupSession</code>.
     */

    protected AbstractFederatingRenovationLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.room.construction.RenovationLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Renovation</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRenovations() {
        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            if (session.canLookupRenovations()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Renovation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRenovationView() {
        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            session.useComparativeRenovationView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Renovation</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRenovationView() {
        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            session.usePlenaryRenovationView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include renovations in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            session.useFederatedCampusView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            session.useIsolatedCampusView();
        }

        return;
    }


    /**
     *  Only renovations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveRenovationView() {
        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            session.useEffectiveRenovationView();
        }

        return;
    }


    /**
     *  All renovations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveRenovationView() {
        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            session.useAnyEffectiveRenovationView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Renovation</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Renovation</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Renovation</code> and
     *  retained for compatibility.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationId <code>Id</code> of the
     *          <code>Renovation</code>
     *  @return the renovation
     *  @throws org.osid.NotFoundException <code>renovationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>renovationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.Renovation getRenovation(org.osid.id.Id renovationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            try {
                return (session.getRenovation(renovationId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(renovationId + " not found");
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  renovations specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Renovations</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>renovationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByIds(org.osid.id.IdList renovationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.room.construction.renovation.MutableRenovationList ret = new net.okapia.osid.jamocha.room.construction.renovation.MutableRenovationList();

        try (org.osid.id.IdList ids = renovationIds) {
            while (ids.hasNext()) {
                ret.addRenovation(getRenovation(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  renovation genus <code>Type</code> which does not include
     *  renovations of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByGenusType(renovationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RenovationList</code> corresponding to the given
     *  renovation genus <code>Type</code> and include any additional
     *  renovations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByParentGenusType(org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByParentGenusType(renovationGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RenovationList</code> containing the given
     *  renovation record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationRecordType a renovation record type 
     *  @return the returned <code>Renovation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>renovationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByRecordType(org.osid.type.Type renovationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByRecordType(renovationRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RenovationList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible
     *  through this session.
     *  
     *  In active mode, renovations are returned that are currently
     *  active. In any status mode, active and inactive renovations
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Renovation</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsOnDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of all renovations of a genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  renovationGenusType a renovation genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *  greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>renovationGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeOnDate(org.osid.type.Type renovationGenusType,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByGenusTypeOnDate(renovationGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations corresponding to a room
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForRoom(org.osid.id.Id roomId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsForRoom(roomId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a room and of a renovation
     *  genus type.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code>
     *  @param  renovationGenusType a renovation genus type
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code> or
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForRoom(org.osid.id.Id roomId,
                                                                                      org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByGenusTypeForRoom(roomId, renovationGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a room with effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForRoomOnDate(org.osid.id.Id roomId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsForRoomOnDate(roomId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a room and of a renovation
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code>
     *  @param  renovationGenusType a renovation genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>renovationGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForRoomOnDate(org.osid.id.Id roomId,
                                                                                            org.osid.type.Type renovationGenusType,
                                                                                            org.osid.calendaring.DateTime from,
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByGenusTypeForRoomOnDate(roomId, renovationGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations corresponding to a floor
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId the <code>Id</code> of the floor
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.NullArgumentException <code>floorId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForFloor(org.osid.id.Id floorId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsForFloor(floorId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a floor and of a renovation
     *  genus type.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code>Id</code>
     *  @param  renovationGenusType a renovation genus type
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>floorId</code> or
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForFloor(org.osid.id.Id floorId,
                                                                                      org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByGenusTypeForFloor(floorId, renovationGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a floor with effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>floorId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForFloorOnDate(org.osid.id.Id floorId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsForFloorOnDate(floorId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a floor and of a renovation
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  floorId a floor <code>Id</code>
     *  @param  renovationGenusType a renovation genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>floorId</code>,
     *          <code>renovationGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForFloorOnDate(org.osid.id.Id floorId,
                                                                                             org.osid.type.Type renovationGenusType,
                                                                                             org.osid.calendaring.DateTime from,
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByGenusTypeForFloorOnDate(floorId, renovationGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations corresponding to a building
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId the <code>Id</code> of the building
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForBuilding(org.osid.id.Id buildingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsForBuilding(buildingId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a building and of a renovation
     *  genus type.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code>
     *  @param  renovationGenusType a renovation genus type
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code> or
     *          <code>renovationGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForBuilding(org.osid.id.Id buildingId,
                                                                                          org.osid.type.Type renovationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByGenusTypeForBuilding(buildingId, renovationGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a building with effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code>
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsForBuildingOnDate(org.osid.id.Id buildingId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsForBuildingOnDate(buildingId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of all renovations for a building and of a renovation
     *  genus type effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective. In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @param  buildingId a building <code>Id</code>
     *  @param  renovationGenusType a renovation genus type
     *  @param  from start of date range
     *  @param  to end of date range
     *  @return the returned <code>RenovationList</code>
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>buildingId</code>,
     *          <code>renovationGenusType</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovationsByGenusTypeForBuildingOnDate(org.osid.id.Id buildingId,
                                                                                                org.osid.type.Type renovationGenusType,
                                                                                                org.osid.calendaring.DateTime from,
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovationsByGenusTypeForBuildingOnDate(buildingId, renovationGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets all <code>Renovations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  renovations or an error results. Otherwise, the returned list
     *  may contain only those renovations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, renovations are returned that are currently
     *  effective.  In any effective mode, effective renovations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Renovations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.construction.RenovationList getRenovations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList ret = getRenovationList();

        for (org.osid.room.construction.RenovationLookupSession session : getSessions()) {
            ret.addRenovationList(session.getRenovations());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.FederatingRenovationList getRenovationList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.ParallelRenovationList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.room.construction.renovation.CompositeRenovationList());
        }
    }
}
