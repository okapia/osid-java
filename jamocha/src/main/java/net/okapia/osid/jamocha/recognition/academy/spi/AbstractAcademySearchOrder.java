//
// AbstractAcademySearchOdrer.java
//
//     Defines an AcademySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.academy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AcademySearchOrder}.
 */

public abstract class AbstractAcademySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogSearchOrder
    implements org.osid.recognition.AcademySearchOrder {

    private final java.util.Collection<org.osid.recognition.records.AcademySearchOrderRecord> records = new java.util.LinkedHashSet<>();



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  academyRecordType an academy record type 
     *  @return {@code true} if the academyRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code academyRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type academyRecordType) {
        for (org.osid.recognition.records.AcademySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(academyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  academyRecordType the academy record type 
     *  @return the academy search order record
     *  @throws org.osid.NullArgumentException
     *          {@code academyRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(academyRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.recognition.records.AcademySearchOrderRecord getAcademySearchOrderRecord(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.AcademySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(academyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(academyRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this academy. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param academyRecord the academy search odrer record
     *  @param academyRecordType academy record type
     *  @throws org.osid.NullArgumentException
     *          {@code academyRecord} or
     *          {@code academyRecordTypeacademy} is
     *          {@code null}
     */
            
    protected void addAcademyRecord(org.osid.recognition.records.AcademySearchOrderRecord academySearchOrderRecord, 
                                     org.osid.type.Type academyRecordType) {

        addRecordType(academyRecordType);
        this.records.add(academySearchOrderRecord);
        
        return;
    }
}
