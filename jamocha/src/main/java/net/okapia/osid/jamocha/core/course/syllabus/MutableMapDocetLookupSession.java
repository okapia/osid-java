//
// MutableMapDocetLookupSession
//
//    Implements a Docet lookup service backed by a collection of
//    docets that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus;


/**
 *  Implements a Docet lookup service backed by a collection of
 *  docets. The docets are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of docets can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapDocetLookupSession
    extends net.okapia.osid.jamocha.core.course.syllabus.spi.AbstractMapDocetLookupSession
    implements org.osid.course.syllabus.DocetLookupSession {


    /**
     *  Constructs a new {@code MutableMapDocetLookupSession}
     *  with no docets.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog} is
     *          {@code null}
     */

      public MutableMapDocetLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDocetLookupSession} with a
     *  single docet.
     *
     *  @param courseCatalog the course catalog  
     *  @param docet a docet
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code docet} is {@code null}
     */

    public MutableMapDocetLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.syllabus.Docet docet) {
        this(courseCatalog);
        putDocet(docet);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDocetLookupSession}
     *  using an array of docets.
     *
     *  @param courseCatalog the course catalog
     *  @param docets an array of docets
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code docets} is {@code null}
     */

    public MutableMapDocetLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.syllabus.Docet[] docets) {
        this(courseCatalog);
        putDocets(docets);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapDocetLookupSession}
     *  using a collection of docets.
     *
     *  @param courseCatalog the course catalog
     *  @param docets a collection of docets
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code docets} is {@code null}
     */

    public MutableMapDocetLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           java.util.Collection<? extends org.osid.course.syllabus.Docet> docets) {

        this(courseCatalog);
        putDocets(docets);
        return;
    }

    
    /**
     *  Makes a {@code Docet} available in this session.
     *
     *  @param docet a docet
     *  @throws org.osid.NullArgumentException {@code docet{@code  is
     *          {@code null}
     */

    @Override
    public void putDocet(org.osid.course.syllabus.Docet docet) {
        super.putDocet(docet);
        return;
    }


    /**
     *  Makes an array of docets available in this session.
     *
     *  @param docets an array of docets
     *  @throws org.osid.NullArgumentException {@code docets{@code 
     *          is {@code null}
     */

    @Override
    public void putDocets(org.osid.course.syllabus.Docet[] docets) {
        super.putDocets(docets);
        return;
    }


    /**
     *  Makes collection of docets available in this session.
     *
     *  @param docets a collection of docets
     *  @throws org.osid.NullArgumentException {@code docets{@code  is
     *          {@code null}
     */

    @Override
    public void putDocets(java.util.Collection<? extends org.osid.course.syllabus.Docet> docets) {
        super.putDocets(docets);
        return;
    }


    /**
     *  Removes a Docet from this session.
     *
     *  @param docetId the {@code Id} of the docet
     *  @throws org.osid.NullArgumentException {@code docetId{@code 
     *          is {@code null}
     */

    @Override
    public void removeDocet(org.osid.id.Id docetId) {
        super.removeDocet(docetId);
        return;
    }    
}
