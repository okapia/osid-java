//
// MutableIndexedMapProxyAssessmentPartLookupSession
//
//    Implements an AssessmentPart lookup service backed by a collection of
//    assessmentParts indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements an AssessmentPart lookup service backed by a collection of
 *  assessmentParts. The assessment parts are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some assessmentParts may be compatible
 *  with more types than are indicated through these assessmentPart
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of assessment parts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyAssessmentPartLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractIndexedMapAssessmentPartLookupSession
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAssessmentPartLookupSession} with
     *  no assessment part.
     *
     *  @param bank the bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                       org.osid.proxy.Proxy proxy) {
        setBank(bank);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAssessmentPartLookupSession} with
     *  a single assessment part.
     *
     *  @param bank the bank
     *  @param  assessmentPart an assessment part
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentPart}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                       org.osid.assessment.authoring.AssessmentPart assessmentPart, org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putAssessmentPart(assessmentPart);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAssessmentPartLookupSession} using
     *  an array of assessment parts.
     *
     *  @param bank the bank
     *  @param  assessmentParts an array of assessment parts
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentParts}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                       org.osid.assessment.authoring.AssessmentPart[] assessmentParts, org.osid.proxy.Proxy proxy) {

        this(bank, proxy);
        putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyAssessmentPartLookupSession} using
     *  a collection of assessment parts.
     *
     *  @param bank the bank
     *  @param  assessmentParts a collection of assessment parts
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentParts}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyAssessmentPartLookupSession(org.osid.assessment.Bank bank,
                                                       java.util.Collection<? extends org.osid.assessment.authoring.AssessmentPart> assessmentParts,
                                                       org.osid.proxy.Proxy proxy) {
        this(bank, proxy);
        putAssessmentParts(assessmentParts);
        return;
    }

    
    /**
     *  Makes an {@code AssessmentPart} available in this session.
     *
     *  @param  assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException {@code assessmentPart}
     *          is {@code null}
     */

    @Override
    public void putAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        super.putAssessmentPart(assessmentPart);
        return;
    }


    /**
     *  Makes an array of assessment parts available in this session.
     *
     *  @param  assessmentParts an array of assessment parts
     *  @throws org.osid.NullArgumentException {@code assessmentParts}
     *          is {@code null}
     */

    @Override
    public void putAssessmentParts(org.osid.assessment.authoring.AssessmentPart[] assessmentParts) {
        super.putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Makes collection of assessment parts available in this session.
     *
     *  @param  assessmentParts a collection of assessment parts
     *  @throws org.osid.NullArgumentException {@code assessmentPart}
     *          is {@code null}
     */

    @Override
    public void putAssessmentParts(java.util.Collection<? extends org.osid.assessment.authoring.AssessmentPart> assessmentParts) {
        super.putAssessmentParts(assessmentParts);
        return;
    }


    /**
     *  Removes an AssessmentPart from this session.
     *
     *  @param assessmentPartId the {@code Id} of the assessment part
     *  @throws org.osid.NullArgumentException {@code assessmentPartId}  is
     *          {@code null}
     */

    @Override
    public void removeAssessmentPart(org.osid.id.Id assessmentPartId) {
        super.removeAssessmentPart(assessmentPartId);
        return;
    }    
}
