//
// AbstractRequisiteSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRequisiteSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.requisite.RequisiteSearchResults {

    private org.osid.course.requisite.RequisiteList requisites;
    private final org.osid.course.requisite.RequisiteQueryInspector inspector;
    private final java.util.Collection<org.osid.course.requisite.records.RequisiteSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRequisiteSearchResults.
     *
     *  @param requisites the result set
     *  @param requisiteQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>requisites</code>
     *          or <code>requisiteQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRequisiteSearchResults(org.osid.course.requisite.RequisiteList requisites,
                                            org.osid.course.requisite.RequisiteQueryInspector requisiteQueryInspector) {
        nullarg(requisites, "requisites");
        nullarg(requisiteQueryInspector, "requisite query inspectpr");

        this.requisites = requisites;
        this.inspector = requisiteQueryInspector;

        return;
    }


    /**
     *  Gets the requisite list resulting from a search.
     *
     *  @return a requisite list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisites() {
        if (this.requisites == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.requisite.RequisiteList requisites = this.requisites;
        this.requisites = null;
	return (requisites);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.requisite.RequisiteQueryInspector getRequisiteQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  requisite search record <code> Type. </code> This method must
     *  be used to retrieve a requisite implementing the requested
     *  record.
     *
     *  @param requisiteSearchRecordType a requisite search 
     *         record type 
     *  @return the requisite search
     *  @throws org.osid.NullArgumentException
     *          <code>requisiteSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(requisiteSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.RequisiteSearchResultsRecord getRequisiteSearchResultsRecord(org.osid.type.Type requisiteSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.requisite.records.RequisiteSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(requisiteSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(requisiteSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record requisite search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRequisiteRecord(org.osid.course.requisite.records.RequisiteSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "requisite record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
