//
// InvariantMapAssessmentEntryLookupSession
//
//    Implements an AssessmentEntry lookup service backed by a fixed collection of
//    assessmentEntries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements an AssessmentEntry lookup service backed by a fixed
 *  collection of assessment entries. The assessment entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAssessmentEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractMapAssessmentEntryLookupSession
    implements org.osid.course.chronicle.AssessmentEntryLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentEntryLookupSession</code> with no
     *  assessment entries.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapAssessmentEntryLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentEntryLookupSession</code> with a single
     *  assessment entry.
     *  
     *  @param courseCatalog the course catalog
     *  @param assessmentEntry an single assessment entry
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code assessmentEntry} is <code>null</code>
     */

      public InvariantMapAssessmentEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.chronicle.AssessmentEntry assessmentEntry) {
        this(courseCatalog);
        putAssessmentEntry(assessmentEntry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentEntryLookupSession</code> using an array
     *  of assessment entries.
     *  
     *  @param courseCatalog the course catalog
     *  @param assessmentEntries an array of assessment entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code assessmentEntries} is <code>null</code>
     */

      public InvariantMapAssessmentEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.chronicle.AssessmentEntry[] assessmentEntries) {
        this(courseCatalog);
        putAssessmentEntries(assessmentEntries);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentEntryLookupSession</code> using a
     *  collection of assessment entries.
     *
     *  @param courseCatalog the course catalog
     *  @param assessmentEntries a collection of assessment entries
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code assessmentEntries} is <code>null</code>
     */

      public InvariantMapAssessmentEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.chronicle.AssessmentEntry> assessmentEntries) {
        this(courseCatalog);
        putAssessmentEntries(assessmentEntries);
        return;
    }
}
