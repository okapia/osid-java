//
// AwardElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.award.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AwardElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the AwardElement Id.
     *
     *  @return the award element Id
     */

    public static org.osid.id.Id getAwardEntityId() {
        return (makeEntityId("osid.recognition.Award"));
    }


    /**
     *  Gets the ConferralId element Id.
     *
     *  @return the ConferralId element Id
     */

    public static org.osid.id.Id getConferralId() {
        return (makeQueryElementId("osid.recognition.award.ConferralId"));
    }


    /**
     *  Gets the Conferral element Id.
     *
     *  @return the Conferral element Id
     */

    public static org.osid.id.Id getConferral() {
        return (makeQueryElementId("osid.recognition.award.Conferral"));
    }


    /**
     *  Gets the AcademyId element Id.
     *
     *  @return the AcademyId element Id
     */

    public static org.osid.id.Id getAcademyId() {
        return (makeQueryElementId("osid.recognition.award.AcademyId"));
    }


    /**
     *  Gets the Academy element Id.
     *
     *  @return the Academy element Id
     */

    public static org.osid.id.Id getAcademy() {
        return (makeQueryElementId("osid.recognition.award.Academy"));
    }
}
