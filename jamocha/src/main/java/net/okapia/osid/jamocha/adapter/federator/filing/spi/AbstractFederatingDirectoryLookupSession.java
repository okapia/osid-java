//
// AbstractFederatingDirectoryLookupSession.java
//
//     An abstract federating adapter for a DirectoryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.filing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  DirectoryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingDirectoryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.filing.DirectoryLookupSession>
    implements org.osid.filing.DirectoryLookupSession {

    private boolean parallel = false;
    private org.osid.filing.Directory directory = new net.okapia.osid.jamocha.nil.filing.directory.UnknownDirectory();


    /**
     *  Constructs a new <code>AbstractFederatingDirectoryLookupSession</code>.
     */

    protected AbstractFederatingDirectoryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.filing.DirectoryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Directory/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Directory Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDirectoryId() {
        return (this.directory.getId());
    }


    /**
     *  Gets the <code>Directory</code> associated with this 
     *  session.
     *
     *  @return the <code>Directory</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.directory);
    }


    /**
     *  Sets the <code>Directory</code>.
     *
     *  @param  directory the directory for this session
     *  @throws org.osid.NullArgumentException <code>directory</code>
     *          is <code>null</code>
     */

    protected void setDirectory(org.osid.filing.Directory directory) {
        nullarg(directory, "directory");
        this.directory = directory;
        return;
    }


    /**
     *  Tests if this user can perform <code>Directory</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDirectories() {
        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            if (session.canLookupDirectories()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Directory</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDirectoryView() {
        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            session.useComparativeDirectoryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Directory</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDirectoryView() {
        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            session.usePlenaryDirectoryView();
        }

        return;
    }

     
    /**
     *  Federates the view for methods in this session. A federated
     *  view will include files in directories which are children
     *  of this directory in the directory hierarchy.
     */

    @OSID @Override
    public void useFederatedDirectoryView() {
        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            session.useFederatedDirectoryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this directory only.
     */

    @OSID @Override
    public void useIsolatedDirectoryView() {
        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            session.useIsolatedDirectoryView();
        }

        return;
    }


    /**
     *  Gets the <code>Directory</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Directory</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Directory</code> and
     *  retained for compatibility.
     *
     *  @param  directoryId <code>Id</code> of the
     *          <code>Directory</code>
     *  @return the directory
     *  @throws org.osid.NotFoundException <code>directoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>directoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            try {
                return (session.getDirectory(directoryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(directoryId + " not found");
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  directories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Directories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getDirectories()</code>.
     *
     *  @param  directoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>directoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByIds(org.osid.id.IdList directoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.filing.directory.MutableDirectoryList ret = new net.okapia.osid.jamocha.filing.directory.MutableDirectoryList();

        try (org.osid.id.IdList ids = directoryIds) {
            while (ids.hasNext()) {
                ret.addDirectory(getDirectory(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  directory genus <code>Type</code> which does not include
     *  directories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getDirectories()</code>.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.filing.directory.FederatingDirectoryList ret = getDirectoryList();

        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            ret.addDirectoryList(session.getDirectoriesByGenusType(directoryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DirectoryList</code> corresponding to the given
     *  directory genus <code>Type</code> and include any additional
     *  directories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDirectories()</code>.
     *
     *  @param  directoryGenusType a directory genus type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByParentGenusType(org.osid.type.Type directoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.filing.directory.FederatingDirectoryList ret = getDirectoryList();

        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            ret.addDirectoryList(session.getDirectoriesByParentGenusType(directoryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DirectoryList</code> containing the given
     *  directory record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDirectories()</code>.
     *
     *  @param  directoryRecordType a directory record type 
     *  @return the returned <code>Directory</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>directoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByRecordType(org.osid.type.Type directoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.filing.directory.FederatingDirectoryList ret = getDirectoryList();

        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            ret.addDirectoryList(session.getDirectoriesByRecordType(directoryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DirectoryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known directories or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  directories that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Directory</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectoriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.filing.directory.FederatingDirectoryList ret = getDirectoryList();

        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            ret.addDirectoryList(session.getDirectoriesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Directories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  directories or an error results. Otherwise, the returned list
     *  may contain only those directories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Directories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.filing.directory.FederatingDirectoryList ret = getDirectoryList();

        for (org.osid.filing.DirectoryLookupSession session : getSessions()) {
            ret.addDirectoryList(session.getDirectories());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.filing.directory.FederatingDirectoryList getDirectoryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.filing.directory.ParallelDirectoryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.filing.directory.CompositeDirectoryList());
        }
    }
}
