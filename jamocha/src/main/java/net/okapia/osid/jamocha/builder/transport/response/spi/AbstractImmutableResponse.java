//
// AbstractImmutableResponse.java
//
//     Wraps a mutable Response to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transport.response.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Response</code> to hide modifiers. This
 *  wrapper provides an immutized Response from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying response whose state changes are visible.
 */

public abstract class AbstractImmutableResponse
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.transport.Response {

    private final org.osid.transport.Response response;


    /**
     *  Constructs a new <code>AbstractImmutableResponse</code>.
     *
     *  @param response the response to immutablize
     *  @throws org.osid.NullArgumentException <code>response</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableResponse(org.osid.transport.Response response) {
        super(response);
        this.response = response;
        return;
    }


    /**
     *  Tests if the request was successful. 
     *
     *  @return <code> true </code> if the request was successful, <code> 
     *          false </code> otherrwise 
     */

    @OSID @Override
    public boolean ok() {
        return (this.response.ok());
    }


    /**
     *  Gets a status message. The returned string may be empty or may contain 
     *  an error message is an error occurred. 
     *
     *  @return an error message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getStatusMessage() {
        return (this.response.getStatusMessage());
    }


    /**
     *  Gets the response record. <code> </code> This method is used to 
     *  retrieve an object implementing the requested message. 
     *
     *  @param  responseRecordType a response record type 
     *  @return the response record 
     *  @throws org.osid.NullArgumentException <code> responseMessageType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(responseRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.transport.records.ResponseRecord getResponseRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        return (this.response.getResponseRecord(responseRecordType));
    }
}

