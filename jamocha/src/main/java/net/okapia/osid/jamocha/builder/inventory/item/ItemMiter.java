//
// ItemMiter.java
//
//     Defines an Item miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.item;


/**
 *  Defines an <code>Item</code> miter for use with the builders.
 */

public interface ItemMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.inventory.Item {


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @throws org.osid.NullArgumentException <code>stock</code> is
     *          <code>null</code>
     */

    public void setStock(org.osid.inventory.Stock stock);


    /**
     *  Sets the property tag.
     *
     *  @param tag a property tag
     *  @throws org.osid.NullArgumentException <code>tag</code> is
     *          <code>null</code>
     */

    public void setPropertyTag(String tag);


    /**
     *  Sets the serial number.
     *
     *  @param number a serial number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setSerialNumber(String number);


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    public void setLocationDescription(org.osid.locale.DisplayText description);


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public void setLocation(org.osid.mapping.Location location);


    /**
     *  Sets the parent item.
     *
     *  @param item the parent item
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    public void setParentItem(org.osid.inventory.Item item);


    /**
     *  Adds an Item record.
     *
     *  @param record an item record
     *  @param recordType the type of item record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addItemRecord(org.osid.inventory.records.ItemRecord record, org.osid.type.Type recordType);
}       


