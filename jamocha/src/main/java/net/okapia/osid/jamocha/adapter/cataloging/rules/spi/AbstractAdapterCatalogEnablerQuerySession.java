//
// AbstractQueryCatalogEnablerLookupSession.java
//
//    A CatalogEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.cataloging.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CatalogEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterCatalogEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.cataloging.rules.CatalogEnablerQuerySession {

    private final org.osid.cataloging.rules.CatalogEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterCatalogEnablerQuerySession.
     *
     *  @param session the underlying catalog enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCatalogEnablerQuerySession(org.osid.cataloging.rules.CatalogEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeCatalog</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogId() {
        return (this.session.getCatalogId());
    }


    /**
     *  Gets the {@codeCatalog</code> associated with this 
     *  session.
     *
     *  @return the {@codeCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.Catalog getCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCatalog());
    }


    /**
     *  Tests if this user can perform {@codeCatalogEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchCatalogEnablers() {
        return (this.session.canSearchCatalogEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include catalog enablers in catalogs which are children
     *  of this catalog in the catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogView() {
        this.session.useFederatedCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this catalog only.
     */
    
    @OSID @Override
    public void useIsolatedCatalogView() {
        this.session.useIsolatedCatalogView();
        return;
    }
    
      
    /**
     *  Gets a catalog enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the catalog enabler query 
     */
      
    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerQuery getCatalogEnablerQuery() {
        return (this.session.getCatalogEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  catalogEnablerQuery the catalog enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code catalogEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code catalogEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablersByQuery(org.osid.cataloging.rules.CatalogEnablerQuery catalogEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getCatalogEnablersByQuery(catalogEnablerQuery));
    }
}
