//
// AbstractGradeSystemTransform.java
//
//     Defines a GradeSystemTransform builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.transform.gradesystemtransform.spi;


/**
 *  Defines a <code>GradeSystemTransform</code> builder.
 */

public abstract class AbstractGradeSystemTransformBuilder<T extends AbstractGradeSystemTransformBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.transform.gradesystemtransform.GradeSystemTransformMiter gradeSystemTransform;


    /**
     *  Constructs a new <code>AbstractGradeSystemTransformBuilder</code>.
     *
     *  @param gradeSystemTransform the grade system transform to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradeSystemTransformBuilder(net.okapia.osid.jamocha.builder.grading.transform.gradesystemtransform.GradeSystemTransformMiter gradeSystemTransform) {
        super(gradeSystemTransform);
        this.gradeSystemTransform = gradeSystemTransform;
        return;
    }


    /**
     *  Builds the grade system transform.
     *
     *  @return the new grade system transform
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.transform.GradeSystemTransform build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.transform.gradesystemtransform.GradeSystemTransformValidator(getValidations())).validate(this.gradeSystemTransform);
        return (new net.okapia.osid.jamocha.builder.grading.transform.gradesystemtransform.ImmutableGradeSystemTransform(this.gradeSystemTransform));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the grade system transform miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.transform.gradesystemtransform.GradeSystemTransformMiter getMiter() {
        return (this.gradeSystemTransform);
    }


    /**
     *  Sets the source grade system.
     *
     *  @param system a source grade system
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public T sourceGradeSystem(org.osid.grading.GradeSystem system) {
        getMiter().setSourceGradeSystem(system);
        return (self());
    }


    /**
     *  Sets the target grade system.
     *
     *  @param system a target grade system
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    public T targetGradeSystem(org.osid.grading.GradeSystem system) {
        getMiter().setTargetGradeSystem(system);
        return (self());
    }


    /**
     *  Normalizes input scores.
     *
     *  @return the builder
     */

    public T normalizedInputScores(boolean normalizes) {
        getMiter().setNormalizesInputScores(true);
        return (self());
    }


    /**
     *  Does not normalize input scores.
     *
     *  @return the builder
     */

    public T noNormalizedInputScores(boolean normalizes) {
        getMiter().setNormalizesInputScores(false);
        return (self());
    }


    /**
     *  Adds a grade map.
     *
     *  @param gradeMap a grade map
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>gradeMap</code> is <code>null</code>
     */

    public T gradeMap(org.osid.grading.transform.GradeMap gradeMap) {
        getMiter().addGradeMap(gradeMap);
        return (self());
    }


    /**
     *  Sets all the grade maps.
     *
     *  @param gradeMaps a collection of grade maps
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>gradeMaps</code> is <code>null</code>
     */

    public T gradeMaps(java.util.Collection<org.osid.grading.transform.GradeMap> gradeMaps) {
        getMiter().setGradeMaps(gradeMaps);
        return (self());
    }


    /**
     *  Adds a GradeSystemTransform record.
     *
     *  @param record a grade system transform record
     *  @param recordType the type of grade system transform record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.grading.transform.records.GradeSystemTransformRecord record, org.osid.type.Type recordType) {
        getMiter().addGradeSystemTransformRecord(record, recordType);
        return (self());
    }
}       


