//
// AbstractAppointment.java
//
//     Defines an Appointment builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.appointment.spi;


/**
 *  Defines an <code>Appointment</code> builder.
 */

public abstract class AbstractAppointmentBuilder<T extends AbstractAppointmentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.personnel.appointment.AppointmentMiter appointment;


    /**
     *  Constructs a new <code>AbstractAppointmentBuilder</code>.
     *
     *  @param appointment the appointment to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAppointmentBuilder(net.okapia.osid.jamocha.builder.personnel.appointment.AppointmentMiter appointment) {
        super(appointment);
        this.appointment = appointment;
        return;
    }


    /**
     *  Builds the appointment.
     *
     *  @return the new appointment
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.personnel.Appointment build() {
        (new net.okapia.osid.jamocha.builder.validator.personnel.appointment.AppointmentValidator(getValidations())).validate(this.appointment);
        return (new net.okapia.osid.jamocha.builder.personnel.appointment.ImmutableAppointment(this.appointment));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the appointment miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.personnel.appointment.AppointmentMiter getMiter() {
        return (this.appointment);
    }


    /**
     *  Sets the person.
     *
     *  @param person a person
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>person</code> is <code>null</code>
     */

    public T person(org.osid.personnel.Person person) {
        getMiter().setPerson(person);
        return (self());
    }


    /**
     *  Sets the position.
     *
     *  @param position a position
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>position</code>
     *          is <code>null</code>
     */

    public T position(org.osid.personnel.Position position) {
        getMiter().setPosition(position);
        return (self());
    }


    /**
     *  Sets the commitment.
     *
     *  @param percentage a commitment
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    public T commitment(long percentage) {
        getMiter().setCommitment(percentage);
        return (self());
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public T title(org.osid.locale.DisplayText title) {
        getMiter().setTitle(title);
        return (self());
    }


    /**
     *  Sets the salary.
     *
     *  @param salary a salary
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    public T salary(org.osid.financials.Currency salary) {
        getMiter().setSalary(salary);
        return (self());
    }


    /**
     *  Sets the salary basis.
     *
     *  @param basis a salary basis
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>basis</code> is
     *          <code>null</code>
     */

    public T salaryBasis(long basis) {
        getMiter().setSalaryBasis(basis);
        return (self());
    }


    /**
     *  Adds an Appointment record.
     *
     *  @param record an appointment record
     *  @param recordType the type of appointment record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.personnel.records.AppointmentRecord record, org.osid.type.Type recordType) {
        getMiter().addAppointmentRecord(record, recordType);
        return (self());
    }
}       


