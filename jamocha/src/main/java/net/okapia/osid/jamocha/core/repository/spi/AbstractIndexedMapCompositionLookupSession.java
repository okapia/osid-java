//
// AbstractIndexedMapCompositionLookupSession.java
//
//    A simple framework for providing a Composition lookup service
//    backed by a fixed collection of compositions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Composition lookup service backed by a
 *  fixed collection of compositions. The compositions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some compositions may be compatible
 *  with more types than are indicated through these composition
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Compositions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCompositionLookupSession
    extends AbstractMapCompositionLookupSession
    implements org.osid.repository.CompositionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.repository.Composition> compositionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.repository.Composition>());
    private final MultiMap<org.osid.type.Type, org.osid.repository.Composition> compositionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.repository.Composition>());


    /**
     *  Makes a <code>Composition</code> available in this session.
     *
     *  @param  composition a composition
     *  @throws org.osid.NullArgumentException <code>composition<code> is
     *          <code>null</code>
     */

    @Override
    protected void putComposition(org.osid.repository.Composition composition) {
        super.putComposition(composition);

        this.compositionsByGenus.put(composition.getGenusType(), composition);
        
        try (org.osid.type.TypeList types = composition.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.compositionsByRecord.put(types.getNextType(), composition);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a composition from this session.
     *
     *  @param compositionId the <code>Id</code> of the composition
     *  @throws org.osid.NullArgumentException <code>compositionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeComposition(org.osid.id.Id compositionId) {
        org.osid.repository.Composition composition;
        try {
            composition = getComposition(compositionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.compositionsByGenus.remove(composition.getGenusType());

        try (org.osid.type.TypeList types = composition.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.compositionsByRecord.remove(types.getNextType(), composition);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeComposition(compositionId);
        return;
    }


    /**
     *  Gets a <code>CompositionList</code> corresponding to the given
     *  composition genus <code>Type</code> which does not include
     *  compositions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known compositions or an error results. Otherwise,
     *  the returned list may contain only those compositions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  compositionGenusType a composition genus type 
     *  @return the returned <code>Composition</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByGenusType(org.osid.type.Type compositionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.composition.ArrayCompositionList(this.compositionsByGenus.get(compositionGenusType)));
    }


    /**
     *  Gets a <code>CompositionList</code> containing the given
     *  composition record <code>Type</code>. In plenary mode, the
     *  returned list contains all known compositions or an error
     *  results. Otherwise, the returned list may contain only those
     *  compositions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  compositionRecordType a composition record type 
     *  @return the returned <code>composition</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>compositionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.CompositionList getCompositionsByRecordType(org.osid.type.Type compositionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.composition.ArrayCompositionList(this.compositionsByRecord.get(compositionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.compositionsByGenus.clear();
        this.compositionsByRecord.clear();

        super.close();

        return;
    }
}
