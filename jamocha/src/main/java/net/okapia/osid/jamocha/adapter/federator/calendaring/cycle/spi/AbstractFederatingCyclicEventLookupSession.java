//
// AbstractFederatingCyclicEventLookupSession.java
//
//     An abstract federating adapter for a CyclicEventLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  CyclicEventLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingCyclicEventLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.cycle.CyclicEventLookupSession>
    implements org.osid.calendaring.cycle.CyclicEventLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingCyclicEventLookupSession</code>.
     */

    protected AbstractFederatingCyclicEventLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.cycle.CyclicEventLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>CyclicEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCyclicEvents() {
        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            if (session.canLookupCyclicEvents()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>CyclicEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCyclicEventView() {
        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            session.useComparativeCyclicEventView();
        }

        return;
    }


    /**
     *  A complete view of the <code>CyclicEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCyclicEventView() {
        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            session.usePlenaryCyclicEventView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic events in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }

     
    /**
     *  Gets the <code>CyclicEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CyclicEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CyclicEvent</code> and
     *  retained for compatibility.
     *
     *  @param  cyclicEventId <code>Id</code> of the
     *          <code>CyclicEvent</code>
     *  @return the cyclic event
     *  @throws org.osid.NotFoundException <code>cyclicEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>cyclicEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent(org.osid.id.Id cyclicEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            try {
                return (session.getCyclicEvent(cyclicEventId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(cyclicEventId + " not found");
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cyclicEvents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CyclicEvents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  cyclicEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByIds(org.osid.id.IdList cyclicEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.MutableCyclicEventList ret = new net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.MutableCyclicEventList();

        try (org.osid.id.IdList ids = cyclicEventIds) {
            while (ids.hasNext()) {
                ret.addCyclicEvent(getCyclicEvent(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  cyclic event genus <code>Type</code> which does not include
     *  cyclic events of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventGenusType a cyclicEvent genus type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclicevent.FederatingCyclicEventList ret = getCyclicEventList();

        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            ret.addCyclicEventList(session.getCyclicEventsByGenusType(cyclicEventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CyclicEventList</code> corresponding to the given
     *  cyclic event genus <code>Type</code> and include any additional
     *  cyclic events with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventGenusType a cyclicEvent genus type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByParentGenusType(org.osid.type.Type cyclicEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclicevent.FederatingCyclicEventList ret = getCyclicEventList();

        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            ret.addCyclicEventList(session.getCyclicEventsByParentGenusType(cyclicEventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>CyclicEventList</code> containing the given
     *  cyclic event record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicEventRecordType a cyclicEvent record type 
     *  @return the returned <code>CyclicEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEventsByRecordType(org.osid.type.Type cyclicEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclicevent.FederatingCyclicEventList ret = getCyclicEventList();

        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            ret.addCyclicEventList(session.getCyclicEventsByRecordType(cyclicEventRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>CyclicEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic events or an error results. Otherwise, the returned list
     *  may contain only those cyclic events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>CyclicEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclicevent.FederatingCyclicEventList ret = getCyclicEventList();

        for (org.osid.calendaring.cycle.CyclicEventLookupSession session : getSessions()) {
            ret.addCyclicEventList(session.getCyclicEvents());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclicevent.FederatingCyclicEventList getCyclicEventList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclicevent.ParallelCyclicEventList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.cycle.cyclicevent.CompositeCyclicEventList());
        }
    }
}
