//
// AbstractOntologyQueryInspector.java
//
//     A template for making an OntologyQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for ontologies.
 */

public abstract class AbstractOntologyQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.ontology.OntologyQueryInspector {

    private final java.util.Collection<org.osid.ontology.records.OntologyQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the subject <code> Id </code> terms. 
     *
     *  @return the subject <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubjectIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the subject terms. 
     *
     *  @return the subject terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the relevancy <code> Id </code> terms. 
     *
     *  @return the relevancy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelevancyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the relevancy terms. 
     *
     *  @return the relevancy terms 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getRelevancyTerms() {
        return (new org.osid.ontology.RelevancyQueryInspector[0]);
    }


    /**
     *  Gets the ancestor ontology <code> Id </code> terms. 
     *
     *  @return the ancestor ontology <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorOntologyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor ontology terms. 
     *
     *  @return the ancestor ontology terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getAncestorOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }


    /**
     *  Gets the descendant ontology <code> Id </code> terms. 
     *
     *  @return the descendant ontology <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantOntologyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant ontology terms. 
     *
     *  @return the descendant ontology terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getDescendantOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given ontology query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an ontology implementing the requested record.
     *
     *  @param ontologyRecordType an ontology record type
     *  @return the ontology query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ontologyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.OntologyQueryInspectorRecord getOntologyQueryInspectorRecord(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.OntologyQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(ontologyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ontologyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ontology query. 
     *
     *  @param ontologyQueryInspectorRecord ontology query inspector
     *         record
     *  @param ontologyRecordType ontology record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOntologyQueryInspectorRecord(org.osid.ontology.records.OntologyQueryInspectorRecord ontologyQueryInspectorRecord, 
                                                   org.osid.type.Type ontologyRecordType) {

        addRecordType(ontologyRecordType);
        nullarg(ontologyRecordType, "ontology record type");
        this.records.add(ontologyQueryInspectorRecord);        
        return;
    }
}
