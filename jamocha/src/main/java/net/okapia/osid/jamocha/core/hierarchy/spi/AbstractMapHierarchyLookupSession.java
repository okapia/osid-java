//
// AbstractMapHierarchyLookupSession
//
//    A simple framework for providing a Hierarchy lookup service
//    backed by a fixed collection of hierarchies.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Hierarchy lookup service backed by a
 *  fixed collection of hierarchies. The hierarchies are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Hierarchies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapHierarchyLookupSession
    extends net.okapia.osid.jamocha.hierarchy.spi.AbstractHierarchyLookupSession
    implements org.osid.hierarchy.HierarchyLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.hierarchy.Hierarchy> hierarchies = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.hierarchy.Hierarchy>());


    /**
     *  Makes a <code>Hierarchy</code> available in this session.
     *
     *  @param  hierarchy a hierarchy
     *  @throws org.osid.NullArgumentException <code>hierarchy<code>
     *          is <code>null</code>
     */

    protected void putHierarchy(org.osid.hierarchy.Hierarchy hierarchy) {
        this.hierarchies.put(hierarchy.getId(), hierarchy);
        return;
    }


    /**
     *  Makes an array of hierarchies available in this session.
     *
     *  @param  hierarchies an array of hierarchies
     *  @throws org.osid.NullArgumentException <code>hierarchies<code>
     *          is <code>null</code>
     */

    protected void putHierarchies(org.osid.hierarchy.Hierarchy[] hierarchies) {
        putHierarchies(java.util.Arrays.asList(hierarchies));
        return;
    }


    /**
     *  Makes a collection of hierarchies available in this session.
     *
     *  @param  hierarchies a collection of hierarchies
     *  @throws org.osid.NullArgumentException <code>hierarchies<code>
     *          is <code>null</code>
     */

    protected void putHierarchies(java.util.Collection<? extends org.osid.hierarchy.Hierarchy> hierarchies) {
        for (org.osid.hierarchy.Hierarchy hierarchy : hierarchies) {
            this.hierarchies.put(hierarchy.getId(), hierarchy);
        }

        return;
    }


    /**
     *  Removes a Hierarchy from this session.
     *
     *  @param  hierarchyId the <code>Id</code> of the hierarchy
     *  @throws org.osid.NullArgumentException <code>hierarchyId<code> is
     *          <code>null</code>
     */

    protected void removeHierarchy(org.osid.id.Id hierarchyId) {
        this.hierarchies.remove(hierarchyId);
        return;
    }


    /**
     *  Gets the <code>Hierarchy</code> specified by its <code>Id</code>.
     *
     *  @param  hierarchyId <code>Id</code> of the <code>Hierarchy</code>
     *  @return the hierarchy
     *  @throws org.osid.NotFoundException <code>hierarchyId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>hierarchyId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Hierarchy getHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.hierarchy.Hierarchy hierarchy = this.hierarchies.get(hierarchyId);
        if (hierarchy == null) {
            throw new org.osid.NotFoundException("hierarchy not found: " + hierarchyId);
        }

        return (hierarchy);
    }


    /**
     *  Gets all <code>Hierarchies</code>. In plenary mode, the returned
     *  list contains all known hierarchies or an error
     *  results. Otherwise, the returned list may contain only those
     *  hierarchies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Hierarchies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyList getHierarchies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hierarchy.hierarchy.ArrayHierarchyList(this.hierarchies.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.hierarchies.clear();
        super.close();
        return;
    }
}
