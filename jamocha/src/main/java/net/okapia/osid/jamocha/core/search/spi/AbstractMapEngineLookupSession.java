//
// AbstractMapEngineLookupSession
//
//    A simple framework for providing an Engine lookup service
//    backed by a fixed collection of engines.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.search.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Engine lookup service backed by a
 *  fixed collection of engines. The engines are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Engines</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapEngineLookupSession
    extends net.okapia.osid.jamocha.search.spi.AbstractEngineLookupSession
    implements org.osid.search.EngineLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.search.Engine> engines = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.search.Engine>());


    /**
     *  Makes an <code>Engine</code> available in this session.
     *
     *  @param  engine an engine
     *  @throws org.osid.NullArgumentException <code>engine<code>
     *          is <code>null</code>
     */

    protected void putEngine(org.osid.search.Engine engine) {
        this.engines.put(engine.getId(), engine);
        return;
    }


    /**
     *  Makes an array of engines available in this session.
     *
     *  @param  engines an array of engines
     *  @throws org.osid.NullArgumentException <code>engines<code>
     *          is <code>null</code>
     */

    protected void putEngines(org.osid.search.Engine[] engines) {
        putEngines(java.util.Arrays.asList(engines));
        return;
    }


    /**
     *  Makes a collection of engines available in this session.
     *
     *  @param  engines a collection of engines
     *  @throws org.osid.NullArgumentException <code>engines<code>
     *          is <code>null</code>
     */

    protected void putEngines(java.util.Collection<? extends org.osid.search.Engine> engines) {
        for (org.osid.search.Engine engine : engines) {
            this.engines.put(engine.getId(), engine);
        }

        return;
    }


    /**
     *  Removes an Engine from this session.
     *
     *  @param  engineId the <code>Id</code> of the engine
     *  @throws org.osid.NullArgumentException <code>engineId<code> is
     *          <code>null</code>
     */

    protected void removeEngine(org.osid.id.Id engineId) {
        this.engines.remove(engineId);
        return;
    }


    /**
     *  Gets the <code>Engine</code> specified by its <code>Id</code>.
     *
     *  @param  engineId <code>Id</code> of the <code>Engine</code>
     *  @return the engine
     *  @throws org.osid.NotFoundException <code>engineId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>engineId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.Engine getEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.search.Engine engine = this.engines.get(engineId);
        if (engine == null) {
            throw new org.osid.NotFoundException("engine not found: " + engineId);
        }

        return (engine);
    }


    /**
     *  Gets all <code>Engines</code>. In plenary mode, the returned
     *  list contains all known engines or an error
     *  results. Otherwise, the returned list may contain only those
     *  engines that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Engines</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.search.EngineList getEngines()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.search.engine.ArrayEngineList(this.engines.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.engines.clear();
        super.close();
        return;
    }
}
