//
// AbstractIndexedMapActionEnablerLookupSession.java
//
//    A simple framework for providing an ActionEnabler lookup service
//    backed by a fixed collection of action enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an ActionEnabler lookup service backed by a
 *  fixed collection of action enablers. The action enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some action enablers may be compatible
 *  with more types than are indicated through these action enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActionEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapActionEnablerLookupSession
    extends AbstractMapActionEnablerLookupSession
    implements org.osid.control.rules.ActionEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.rules.ActionEnabler> actionEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.rules.ActionEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.control.rules.ActionEnabler> actionEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.rules.ActionEnabler>());


    /**
     *  Makes an <code>ActionEnabler</code> available in this session.
     *
     *  @param  actionEnabler an action enabler
     *  @throws org.osid.NullArgumentException <code>actionEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putActionEnabler(org.osid.control.rules.ActionEnabler actionEnabler) {
        super.putActionEnabler(actionEnabler);

        this.actionEnablersByGenus.put(actionEnabler.getGenusType(), actionEnabler);
        
        try (org.osid.type.TypeList types = actionEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.actionEnablersByRecord.put(types.getNextType(), actionEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of action enablers available in this session.
     *
     *  @param  actionEnablers an array of action enablers
     *  @throws org.osid.NullArgumentException <code>actionEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putActionEnablers(org.osid.control.rules.ActionEnabler[] actionEnablers) {
        for (org.osid.control.rules.ActionEnabler actionEnabler : actionEnablers) {
            putActionEnabler(actionEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of action enablers available in this session.
     *
     *  @param  actionEnablers a collection of action enablers
     *  @throws org.osid.NullArgumentException <code>actionEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putActionEnablers(java.util.Collection<? extends org.osid.control.rules.ActionEnabler> actionEnablers) {
        for (org.osid.control.rules.ActionEnabler actionEnabler : actionEnablers) {
            putActionEnabler(actionEnabler);
        }

        return;
    }


    /**
     *  Removes an action enabler from this session.
     *
     *  @param actionEnablerId the <code>Id</code> of the action enabler
     *  @throws org.osid.NullArgumentException <code>actionEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeActionEnabler(org.osid.id.Id actionEnablerId) {
        org.osid.control.rules.ActionEnabler actionEnabler;
        try {
            actionEnabler = getActionEnabler(actionEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.actionEnablersByGenus.remove(actionEnabler.getGenusType());

        try (org.osid.type.TypeList types = actionEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.actionEnablersByRecord.remove(types.getNextType(), actionEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeActionEnabler(actionEnablerId);
        return;
    }


    /**
     *  Gets an <code>ActionEnablerList</code> corresponding to the given
     *  action enabler genus <code>Type</code> which does not include
     *  action enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known action enablers or an error results. Otherwise,
     *  the returned list may contain only those action enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  actionEnablerGenusType an action enabler genus type 
     *  @return the returned <code>ActionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByGenusType(org.osid.type.Type actionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.actionenabler.ArrayActionEnablerList(this.actionEnablersByGenus.get(actionEnablerGenusType)));
    }


    /**
     *  Gets an <code>ActionEnablerList</code> containing the given
     *  action enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known action enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  action enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  actionEnablerRecordType an action enabler record type 
     *  @return the returned <code>actionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByRecordType(org.osid.type.Type actionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.actionenabler.ArrayActionEnablerList(this.actionEnablersByRecord.get(actionEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.actionEnablersByGenus.clear();
        this.actionEnablersByRecord.clear();

        super.close();

        return;
    }
}
