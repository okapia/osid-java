//
// InvariantMapBankLookupSession
//
//    Implements a Bank lookup service backed by a fixed collection of
//    banks.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements a Bank lookup service backed by a fixed
 *  collection of banks. The banks are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapBankLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractMapBankLookupSession
    implements org.osid.assessment.BankLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapBankLookupSession</code> with no
     *  banks.
     */

    public InvariantMapBankLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBankLookupSession</code> with a single
     *  bank.
     *  
     *  @throws org.osid.NullArgumentException {@code bank}
     *          is <code>null</code>
     */

    public InvariantMapBankLookupSession(org.osid.assessment.Bank bank) {
        putBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBankLookupSession</code> using an array
     *  of banks.
     *  
     *  @throws org.osid.NullArgumentException {@code banks}
     *          is <code>null</code>
     */

    public InvariantMapBankLookupSession(org.osid.assessment.Bank[] banks) {
        putBanks(banks);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapBankLookupSession</code> using a
     *  collection of banks.
     *
     *  @throws org.osid.NullArgumentException {@code banks}
     *          is <code>null</code>
     */

    public InvariantMapBankLookupSession(java.util.Collection<? extends org.osid.assessment.Bank> banks) {
        putBanks(banks);
        return;
    }
}
