//
// AbstractAddressBookSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.addressbook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAddressBookSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.contact.AddressBookSearchResults {

    private org.osid.contact.AddressBookList addressBooks;
    private final org.osid.contact.AddressBookQueryInspector inspector;
    private final java.util.Collection<org.osid.contact.records.AddressBookSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAddressBookSearchResults.
     *
     *  @param addressBooks the result set
     *  @param addressBookQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>addressBooks</code>
     *          or <code>addressBookQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAddressBookSearchResults(org.osid.contact.AddressBookList addressBooks,
                                            org.osid.contact.AddressBookQueryInspector addressBookQueryInspector) {
        nullarg(addressBooks, "address books");
        nullarg(addressBookQueryInspector, "address book query inspectpr");

        this.addressBooks = addressBooks;
        this.inspector = addressBookQueryInspector;

        return;
    }


    /**
     *  Gets the address book list resulting from a search.
     *
     *  @return an address book list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.contact.AddressBookList getAddressBooks() {
        if (this.addressBooks == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.contact.AddressBookList addressBooks = this.addressBooks;
        this.addressBooks = null;
	return (addressBooks);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.contact.AddressBookQueryInspector getAddressBookQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  address book search record <code> Type. </code> This method must
     *  be used to retrieve an addressBook implementing the requested
     *  record.
     *
     *  @param addressBookSearchRecordType an addressBook search 
     *         record type 
     *  @return the address book search
     *  @throws org.osid.NullArgumentException
     *          <code>addressBookSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(addressBookSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressBookSearchResultsRecord getAddressBookSearchResultsRecord(org.osid.type.Type addressBookSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.contact.records.AddressBookSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(addressBookSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(addressBookSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record address book search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAddressBookRecord(org.osid.contact.records.AddressBookSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "address book record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
