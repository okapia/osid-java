//
// AbstractTemporalOsidObjectQuery.java
//
//     A temporal OsidObjectQuery with stored terms.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A temporal OsidObjectQuery with stored terms.
 */

public abstract class AbstractTemporalOsidObjectQuery
    extends AbstractOsidObjectQuery
    implements org.osid.OsidTemporalQuery,
               org.osid.OsidObjectQuery {

    private final OsidTemporalQuery query;


    /**
     *  Constructs a new <code>AbstractTemporalOsidObjectQuery</code>.
     *
     *  @param factory the term factory
     *  @throws org.osid.NullArgumentException <code>factory</code> is
     *          <code>null</code>
     */

    protected AbstractTemporalOsidObjectQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
        super(factory);
        this.query = new OsidTemporalQuery(factory);
        return;
    }


    /**
     *  Match effective objects where the current date falls within
     *  the start and end dates inclusive.
     *
     *  @param  match <code> true </code> to match any effective, <code> false 
     *          </code> to match ineffective 
     */

    @OSID @Override
    public void matchEffective(boolean match) {
        this.query.matchEffective(match);
        return;
    }


    /**
     *  Clears the effective query terms. 
     */

    @OSID @Override
    public void clearEffectiveTerms() {
        this.query.clearEffectiveTerms();
        return;
    }


    /**
     *  Gets all the effective query terms.
     *
     *  @return a collection of the effective query terms
     */

    protected java.util.Collection<org.osid.search.terms.BooleanTerm> getEffectiveTerms() {
        return (this.query.getEffectiveTerms());
    }


    /**
     *  Matches temporals whose start date falls in between the given dates 
     *  inclusive. 
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                               boolean match) {
        this.query.matchStartDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any start date set. 
     *
     *  @param  match <code> true </code> to match any start date, <code> 
     *          false </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        this.query.matchAnyStartDate(match);
        return;
    }


    /**
     *  Clears the start date query terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        this.query.clearStartDateTerms();
        return;
    }


    /**
     *  Gets all the start date query terms.
     *
     *  @return a collection of the start date query terms
     */

    protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getStartDateTerms() {
        return (this.getStartDateTerms());
    }


    /**
     *  Matches temporals whose end date falls in between the given dates 
     *  inclusive. 
     *
     *  @param  start the start of date range 
     *  @param  end the end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> end </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                               boolean match) {
        this.query.matchEndDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any end date set. 
     *
     *  @param  match <code> true </code> to match any end date, <code> 
     *          false </code> to match no end date 
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        this.query.matchAnyEndDate(match);
        return;
    }


    /**
     *  Clears the end date query terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        this.query.clearEndDateTerms();
        return;
    }


    /**
     *  Gets all the end date query terms.
     *
     *  @return a collection of the end date query terms
     */

    protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getEndDateTerms() {
        return (this.query.getEndDateTerms());
    }


    /**
     *  Matches temporals where the given date is included within the start 
     *  and end dates inclusive. 
     *
     *  @param from start date
     *  @param to end date
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException
     *          <code>from</code> is greater than or
     *          <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code>
     *          or <code>to</code> is <code>null</code>
     */
        
    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from,
                          org.osid.calendaring.DateTime to,
                          boolean match) {
        this.query.matchDate(from, to, match);
        return;
    }


    /**
     *  Clears the date query terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        this.query.clearDateTerms();
        return;
    }


    /**
     *  Gets all the date query terms.
     *
     *  @return a collection of the date query terms
     */

    protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getDateTerms() {
        return (this.query.getDateTerms());
    }

    
    protected class OsidTemporalQuery
        extends AbstractOsidTemporalQuery
        implements org.osid.OsidTemporalQuery {


        /**
         *  Constructs a new <code>OsidTemporalQuery</code>.
         *
         *  @param factory the term factory
         *  @throws org.osid.NullArgumentException <code>factory</code> is
         *          <code>null</code>
         */

        protected OsidTemporalQuery(net.okapia.osid.jamocha.query.TermFactory factory) {
            super(factory);
            return;
        }


        /**
         *  Match effective objects where the current date falls within
         *  the start and end dates inclusive.
         *
         *  @param  match <code> true </code> to match any effective, <code> false 
         *          </code> to match ineffective 
         */

        @OSID @Override
        public void matchEffective(boolean match) {
            super.matchEffective(match);
            return;
        }


        /**
         *  Clears the effective query terms. 
         */

        @OSID @Override
        public void clearEffectiveTerms() {
            super.clearEffectiveTerms();
            return;
        }


        /**
         *  Gets all the effective query terms.
         *
         *  @return a collection of the effective query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.BooleanTerm> getEffectiveTerms() {
            return (super.getEffectiveTerms());
        }


        /**
         *  Matches temporals whose start date falls in between the given dates 
         *  inclusive. 
         *
         *  @param  start start of date range 
         *  @param  end end of date range 
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
         *          than <code> end </code> 
         *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
         *          end </code> is <code> null </code> 
         */

        @OSID @Override
        public void matchStartDate(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                   boolean match) {
            super.matchStartDate(start, end, match);
            return;
        }


        /**
         *  Matches temporals with any start date set. 
         *
         *  @param  match <code> true </code> to match any start date, <code> 
         *          false </code> to match no start date 
         */

        @OSID @Override
        public void matchAnyStartDate(boolean match) {
            super.matchAnyStartDate(match);
            return;
        }


        /**
         *  Clears the start date query terms. 
         */

        @OSID @Override
        public void clearStartDateTerms() {
            super.clearStartDateTerms();
            return;
        }


        /**
         *  Gets all the start date query terms.
         *
         *  @return a collection of the start date query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getStartDateTerms() {
            return (this.getStartDateTerms());
        }


        /**
         *  Matches temporals whose end date falls in between the given dates 
         *  inclusive. 
         *
         *  @param  start the start of date range 
         *  @param  end the end of date range 
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
         *          than <code> end </code> 
         *  @throws org.osid.NullArgumentException <code> end </code> or <code> 
         *          end </code> is <code> null </code> 
         */

        @OSID @Override
        public void matchEndDate(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                 boolean match) {
            super.matchEndDate(start, end, match);
            return;
        }


        /**
         *  Matches temporals with any end date set. 
         *
         *  @param  match <code> true </code> to match any end date, <code> 
         *          false </code> to match no end date 
         */

        @OSID @Override
        public void matchAnyEndDate(boolean match) {
            super.matchAnyEndDate(match);
            return;
        }


        /**
         *  Clears the end date query terms. 
         */

        @OSID @Override
        public void clearEndDateTerms() {
            super.clearEndDateTerms();
            return;
        }


        /**
         *  Gets all the end date query terms.
         *
         *  @return a collection of the end date query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getEndDateTerms() {
            return (super.getEndDateTerms());
        }


        /**
         *  Matches temporals where the given date is included within the start 
         *  and end dates inclusive. 
         *
         *  @param from start date
         *  @param to end date
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException
         *          <code>from</code> is greater than or
         *          <code>to</code>
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        @OSID @Override
        public void matchDate(org.osid.calendaring.DateTime from,
                              org.osid.calendaring.DateTime to,
                              boolean match) {
            super.matchDate(from, to, match);
            return;
        }


        /**
         *  Clears the date query terms. 
         */

        @OSID @Override
        public void clearDateTerms() {
            super.clearDateTerms();
            return;
        }


        /**
         *  Gets all the date query terms.
         *
         *  @return a collection of the date query terms
         */

        @Override
        protected java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> getDateTerms() {
            return (super.getDateTerms());
        }
    }
}
