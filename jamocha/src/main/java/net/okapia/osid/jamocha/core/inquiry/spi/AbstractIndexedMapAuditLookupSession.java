//
// AbstractIndexedMapAuditLookupSession.java
//
//    A simple framework for providing an Audit lookup service
//    backed by a fixed collection of audits with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Audit lookup service backed by a
 *  fixed collection of audits. The audits are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some audits may be compatible
 *  with more types than are indicated through these audit
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Audits</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuditLookupSession
    extends AbstractMapAuditLookupSession
    implements org.osid.inquiry.AuditLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.inquiry.Audit> auditsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.Audit>());
    private final MultiMap<org.osid.type.Type, org.osid.inquiry.Audit> auditsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.inquiry.Audit>());


    /**
     *  Makes an <code>Audit</code> available in this session.
     *
     *  @param  audit an audit
     *  @throws org.osid.NullArgumentException <code>audit<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAudit(org.osid.inquiry.Audit audit) {
        super.putAudit(audit);

        this.auditsByGenus.put(audit.getGenusType(), audit);
        
        try (org.osid.type.TypeList types = audit.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auditsByRecord.put(types.getNextType(), audit);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of audits available in this session.
     *
     *  @param  audits an array of audits
     *  @throws org.osid.NullArgumentException <code>audits<code>
     *          is <code>null</code>
     */

    @Override
    protected void putAudits(org.osid.inquiry.Audit[] audits) {
        for (org.osid.inquiry.Audit audit : audits) {
            putAudit(audit);
        }

        return;
    }


    /**
     *  Makes a collection of audits available in this session.
     *
     *  @param  audits a collection of audits
     *  @throws org.osid.NullArgumentException <code>audits<code>
     *          is <code>null</code>
     */

    @Override
    protected void putAudits(java.util.Collection<? extends org.osid.inquiry.Audit> audits) {
        for (org.osid.inquiry.Audit audit : audits) {
            putAudit(audit);
        }

        return;
    }


    /**
     *  Removes an audit from this session.
     *
     *  @param auditId the <code>Id</code> of the audit
     *  @throws org.osid.NullArgumentException <code>auditId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAudit(org.osid.id.Id auditId) {
        org.osid.inquiry.Audit audit;
        try {
            audit = getAudit(auditId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.auditsByGenus.remove(audit.getGenusType());

        try (org.osid.type.TypeList types = audit.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auditsByRecord.remove(types.getNextType(), audit);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAudit(auditId);
        return;
    }


    /**
     *  Gets an <code>AuditList</code> corresponding to the given
     *  audit genus <code>Type</code> which does not include
     *  audits of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known audits or an error results. Otherwise,
     *  the returned list may contain only those audits that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  auditGenusType an audit genus type 
     *  @return the returned <code>Audit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auditGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByGenusType(org.osid.type.Type auditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.audit.ArrayAuditList(this.auditsByGenus.get(auditGenusType)));
    }


    /**
     *  Gets an <code>AuditList</code> containing the given
     *  audit record <code>Type</code>. In plenary mode, the
     *  returned list contains all known audits or an error
     *  results. Otherwise, the returned list may contain only those
     *  audits that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  auditRecordType an audit record type 
     *  @return the returned <code>audit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auditRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inquiry.AuditList getAuditsByRecordType(org.osid.type.Type auditRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inquiry.audit.ArrayAuditList(this.auditsByRecord.get(auditRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auditsByGenus.clear();
        this.auditsByRecord.clear();

        super.close();

        return;
    }
}
