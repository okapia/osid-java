//
// AbstractAdapterSupersedingEventEnablerLookupSession.java
//
//    A SupersedingEventEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SupersedingEventEnabler lookup session adapter.
 */

public abstract class AbstractAdapterSupersedingEventEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.rules.SupersedingEventEnablerLookupSession {

    private final org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterSupersedingEventEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterSupersedingEventEnablerLookupSession(org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code SupersedingEventEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupSupersedingEventEnablers() {
        return (this.session.canLookupSupersedingEventEnablers());
    }


    /**
     *  A complete view of the {@code SupersedingEventEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSupersedingEventEnablerView() {
        this.session.useComparativeSupersedingEventEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code SupersedingEventEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySupersedingEventEnablerView() {
        this.session.usePlenarySupersedingEventEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include superseding event enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active superseding event enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSupersedingEventEnablerView() {
        this.session.useActiveSupersedingEventEnablerView();
        return;
    }


    /**
     *  Active and inactive superseding event enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSupersedingEventEnablerView() {
        this.session.useAnyStatusSupersedingEventEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code SupersedingEventEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code SupersedingEventEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code SupersedingEventEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, superseding event enablers are returned that are currently
     *  active. In any status mode, active and inactive superseding event enablers
     *  are returned.
     *
     *  @param supersedingEventEnablerId {@code Id} of the {@code SupersedingEventEnabler}
     *  @return the superseding event enabler
     *  @throws org.osid.NotFoundException {@code supersedingEventEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code supersedingEventEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnabler getSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventEnabler(supersedingEventEnablerId));
    }


    /**
     *  Gets a {@code SupersedingEventEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  supersedingEventEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code SupersedingEventEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, superseding event enablers are returned that are currently
     *  active. In any status mode, active and inactive superseding event enablers
     *  are returned.
     *
     *  @param  supersedingEventEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code SupersedingEventEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByIds(org.osid.id.IdList supersedingEventEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventEnablersByIds(supersedingEventEnablerIds));
    }


    /**
     *  Gets a {@code SupersedingEventEnablerList} corresponding to the given
     *  superseding event enabler genus {@code Type} which does not include
     *  superseding event enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the returned list
     *  may contain only those superseding event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding event enablers are returned that are currently
     *  active. In any status mode, active and inactive superseding event enablers
     *  are returned.
     *
     *  @param  supersedingEventEnablerGenusType a supersedingEventEnabler genus type 
     *  @return the returned {@code SupersedingEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByGenusType(org.osid.type.Type supersedingEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventEnablersByGenusType(supersedingEventEnablerGenusType));
    }


    /**
     *  Gets a {@code SupersedingEventEnablerList} corresponding to the given
     *  superseding event enabler genus {@code Type} and include any additional
     *  superseding event enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the returned list
     *  may contain only those superseding event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding event enablers are returned that are currently
     *  active. In any status mode, active and inactive superseding event enablers
     *  are returned.
     *
     *  @param  supersedingEventEnablerGenusType a supersedingEventEnabler genus type 
     *  @return the returned {@code SupersedingEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByParentGenusType(org.osid.type.Type supersedingEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventEnablersByParentGenusType(supersedingEventEnablerGenusType));
    }


    /**
     *  Gets a {@code SupersedingEventEnablerList} containing the given
     *  superseding event enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the returned list
     *  may contain only those superseding event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding event enablers are returned that are currently
     *  active. In any status mode, active and inactive superseding event enablers
     *  are returned.
     *
     *  @param  supersedingEventEnablerRecordType a supersedingEventEnabler record type 
     *  @return the returned {@code SupersedingEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code supersedingEventEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByRecordType(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventEnablersByRecordType(supersedingEventEnablerRecordType));
    }


    /**
     *  Gets a {@code SupersedingEventEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the returned list
     *  may contain only those superseding event enablers that are accessible
     *  through this session.
     *  
     *  In active mode, superseding event enablers are returned that are currently
     *  active. In any status mode, active and inactive superseding event enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code SupersedingEventEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code SupersedingEventEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the returned list
     *  may contain only those superseding event enablers that are accessible
     *  through this session.
     *
     *  In active mode, superseding event enablers are returned that are currently
     *  active. In any status mode, active and inactive superseding event enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code SupersedingEventEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getSupersedingEventEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code SupersedingEventEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the returned list
     *  may contain only those superseding event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, superseding event enablers are returned that are currently
     *  active. In any status mode, active and inactive superseding event enablers
     *  are returned.
     *
     *  @return a list of {@code SupersedingEventEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getSupersedingEventEnablers());
    }
}
