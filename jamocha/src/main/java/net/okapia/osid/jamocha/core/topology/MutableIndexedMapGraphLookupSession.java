//
// MutableIndexedMapGraphLookupSession
//
//    Implements a Graph lookup service backed by a collection of
//    graphs indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology;


/**
 *  Implements a Graph lookup service backed by a collection of
 *  graphs. The graphs are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some graphs may be compatible
 *  with more types than are indicated through these graph
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of graphs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapGraphLookupSession
    extends net.okapia.osid.jamocha.core.topology.spi.AbstractIndexedMapGraphLookupSession
    implements org.osid.topology.GraphLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGraphLookupSession} with no
     *  graphs.
     */

    public MutableIndexedMapGraphLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGraphLookupSession} with a
     *  single graph.
     *  
     *  @param  graph a single graph
     *  @throws org.osid.NullArgumentException {@code graph}
     *          is {@code null}
     */

    public MutableIndexedMapGraphLookupSession(org.osid.topology.Graph graph) {
        putGraph(graph);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGraphLookupSession} using an
     *  array of graphs.
     *
     *  @param  graphs an array of graphs
     *  @throws org.osid.NullArgumentException {@code graphs}
     *          is {@code null}
     */

    public MutableIndexedMapGraphLookupSession(org.osid.topology.Graph[] graphs) {
        putGraphs(graphs);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapGraphLookupSession} using a
     *  collection of graphs.
     *
     *  @param  graphs a collection of graphs
     *  @throws org.osid.NullArgumentException {@code graphs} is
     *          {@code null}
     */

    public MutableIndexedMapGraphLookupSession(java.util.Collection<? extends org.osid.topology.Graph> graphs) {
        putGraphs(graphs);
        return;
    }
    

    /**
     *  Makes a {@code Graph} available in this session.
     *
     *  @param  graph a graph
     *  @throws org.osid.NullArgumentException {@code graph{@code  is
     *          {@code null}
     */

    @Override
    public void putGraph(org.osid.topology.Graph graph) {
        super.putGraph(graph);
        return;
    }


    /**
     *  Makes an array of graphs available in this session.
     *
     *  @param  graphs an array of graphs
     *  @throws org.osid.NullArgumentException {@code graphs{@code 
     *          is {@code null}
     */

    @Override
    public void putGraphs(org.osid.topology.Graph[] graphs) {
        super.putGraphs(graphs);
        return;
    }


    /**
     *  Makes collection of graphs available in this session.
     *
     *  @param  graphs a collection of graphs
     *  @throws org.osid.NullArgumentException {@code graph{@code  is
     *          {@code null}
     */

    @Override
    public void putGraphs(java.util.Collection<? extends org.osid.topology.Graph> graphs) {
        super.putGraphs(graphs);
        return;
    }


    /**
     *  Removes a Graph from this session.
     *
     *  @param graphId the {@code Id} of the graph
     *  @throws org.osid.NullArgumentException {@code graphId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGraph(org.osid.id.Id graphId) {
        super.removeGraph(graphId);
        return;
    }    
}
