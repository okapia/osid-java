//
// AbstractPayerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.payment.payer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPayerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.billing.payment.PayerSearchResults {

    private org.osid.billing.payment.PayerList payers;
    private final org.osid.billing.payment.PayerQueryInspector inspector;
    private final java.util.Collection<org.osid.billing.payment.records.PayerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPayerSearchResults.
     *
     *  @param payers the result set
     *  @param payerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>payers</code>
     *          or <code>payerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPayerSearchResults(org.osid.billing.payment.PayerList payers,
                                            org.osid.billing.payment.PayerQueryInspector payerQueryInspector) {
        nullarg(payers, "payers");
        nullarg(payerQueryInspector, "payer query inspectpr");

        this.payers = payers;
        this.inspector = payerQueryInspector;

        return;
    }


    /**
     *  Gets the payer list resulting from a search.
     *
     *  @return a payer list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayers() {
        if (this.payers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.billing.payment.PayerList payers = this.payers;
        this.payers = null;
	return (payers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.billing.payment.PayerQueryInspector getPayerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  payer search record <code> Type. </code> This method must
     *  be used to retrieve a payer implementing the requested
     *  record.
     *
     *  @param payerSearchRecordType a payer search 
     *         record type 
     *  @return the payer search
     *  @throws org.osid.NullArgumentException
     *          <code>payerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(payerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PayerSearchResultsRecord getPayerSearchResultsRecord(org.osid.type.Type payerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.billing.payment.records.PayerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(payerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(payerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record payer search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPayerRecord(org.osid.billing.payment.records.PayerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "payer record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
