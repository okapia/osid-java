//
// AbstractCalendarNodeToCalendarList.java
//
//     Implements an AbstractCalendarNodeToCalendarList adapter.
//
//
// Tom Coppeto
// OnTapSolutions
// 21 September 2010
//
//
// Copyright (c) 2010 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.calendaring.calendarnode.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements an AbstractCalendarList adapter to convert calendar
 *  nodes into calendars.
 */

public abstract class AbstractCalendarNodeToCalendarList
    extends net.okapia.osid.jamocha.adapter.calendaring.calendar.spi.AbstractAdapterCalendarList
    implements org.osid.calendaring.CalendarList {

    private final org.osid.calendaring.CalendarNodeList list;


    /**
     *  Constructs a new {@code AbstractCalendarNodeToCalendarList}.
     *
     *  @param list the calendar node list to convert
     *  @throws org.osid.NullArgumentException {@code list} is
     *          {@code null}
     */

    protected AbstractCalendarNodeToCalendarList(org.osid.calendaring.CalendarNodeList list) {
        super(list);
        this.list = list;
        return;
    }


    /**
     *  Gets the next {@code Calendar} in this list. 
     *
     *  @return the next {@code Calendar} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Calendar} is available before calling this
     *          method.
     *  @throws org.osid.IllegalStateException no more elements
     *          available in this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getNextCalendar()
        throws org.osid.OperationFailedException {

        return (this.list.getNextCalendarNode().getCalendar());
    }
}
