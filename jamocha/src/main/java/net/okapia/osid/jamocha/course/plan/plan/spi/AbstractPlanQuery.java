//
// AbstractPlanQuery.java
//
//     A template for making a Plan Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.plan.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for plans.
 */

public abstract class AbstractPlanQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.plan.PlanQuery {

    private final java.util.Collection<org.osid.course.plan.records.PlanQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the syllabus <code> Id </code> for this query. 
     *
     *  @param  syllabusId a syllabus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> syllabusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSyllabusId(org.osid.id.Id syllabusId, boolean match) {
        return;
    }


    /**
     *  Clears the syllabus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSyllabusIdTerms() {
        return;
    }


    /**
     *  Tests if a syllabus query is available. 
     *
     *  @return <code> true </code> if a syllabus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSyllabusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a lesson. 
     *
     *  @return the syllabus query 
     *  @throws org.osid.UnimplementedException <code> supportsSyllabusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQuery getSyllabusQuery() {
        throw new org.osid.UnimplementedException("supportsSyllabusQuery() is false");
    }


    /**
     *  Clears the syllabus terms. 
     */

    @OSID @Override
    public void clearSyllabusTerms() {
        return;
    }


    /**
     *  Sets the course offering <code> Id </code> for this query. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> syllabusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if a course offering query is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Clears the syllabus terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        return;
    }


    /**
     *  Sets a module <code> Id. </code> 
     *
     *  @param  moduleId a module <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> moduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModuleId(org.osid.id.Id moduleId, boolean match) {
        return;
    }


    /**
     *  Clears the module <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModuleIdTerms() {
        return;
    }


    /**
     *  Gets the query for a module query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the lesson query 
     *  @throws org.osid.UnimplementedException <code> supportsModuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQuery getModuleQuery() {
        throw new org.osid.UnimplementedException("supportsModuleQuery() is false");
    }


    /**
     *  Matches plans with any module. 
     *
     *  @param  match <code> true </code> to match pans with any module, 
     *          <code> false </code> to match plans with no modules 
     */

    @OSID @Override
    public void matchAnyModule(boolean match) {
        return;
    }


    /**
     *  Clears the module terms. 
     */

    @OSID @Override
    public void clearModuleTerms() {
        return;
    }


    /**
     *  Sets a lesson <code> Id. </code> 
     *
     *  @param  lessonId a lesson <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> lessonId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLessonId(org.osid.id.Id lessonId, boolean match) {
        return;
    }


    /**
     *  Clears the lesson <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearLessonIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> LessonQuery </code> is available. 
     *
     *  @return <code> true </code> if a lesson query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonQuery() {
        return (false);
    }


    /**
     *  Gets the query for a lesson query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the lesson query 
     *  @throws org.osid.UnimplementedException <code> supportsLessonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQuery getLessonQuery() {
        throw new org.osid.UnimplementedException("supportsLessonQuery() is false");
    }


    /**
     *  Matches plans with any lesson. 
     *
     *  @param  match <code> true </code> to match covocations with any 
     *          lesson, <code> false </code> to match plans with no lessons 
     */

    @OSID @Override
    public void matchAnyLesson(boolean match) {
        return;
    }


    /**
     *  Clears the lesson terms. 
     */

    @OSID @Override
    public void clearLessonTerms() {
        return;
    }


    /**
     *  Sets the lesson <code> Id </code> for this query to match plans 
     *  assigned to course catalogs. 
     *
     *  @param  courseCatalogId a course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given plan query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a plan implementing the requested record.
     *
     *  @param planRecordType a plan record type
     *  @return the plan query record
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(planRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanQueryRecord getPlanQueryRecord(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.PlanQueryRecord record : this.records) {
            if (record.implementsRecordType(planRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(planRecordType + " is not supported");
    }


    /**
     *  Adds a record to this plan query. 
     *
     *  @param planQueryRecord plan query record
     *  @param planRecordType plan record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPlanQueryRecord(org.osid.course.plan.records.PlanQueryRecord planQueryRecord, 
                                          org.osid.type.Type planRecordType) {

        addRecordType(planRecordType);
        nullarg(planQueryRecord, "plan query record");
        this.records.add(planQueryRecord);        
        return;
    }
}
