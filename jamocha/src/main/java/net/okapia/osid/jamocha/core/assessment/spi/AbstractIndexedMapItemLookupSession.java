//
// AbstractIndexedMapItemLookupSession.java
//
//    A simple framework for providing an Item lookup service
//    backed by a fixed collection of items with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Item lookup service backed by a
 *  fixed collection of items. The items are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some items may be compatible
 *  with more types than are indicated through these item
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Items</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapItemLookupSession
    extends AbstractMapItemLookupSession
    implements org.osid.assessment.ItemLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.assessment.Item> itemsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.Item>());
    private final MultiMap<org.osid.type.Type, org.osid.assessment.Item> itemsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.assessment.Item>());


    /**
     *  Makes an <code>Item</code> available in this session.
     *
     *  @param  item an item
     *  @throws org.osid.NullArgumentException <code>item<code> is
     *          <code>null</code>
     */

    @Override
    protected void putItem(org.osid.assessment.Item item) {
        super.putItem(item);

        this.itemsByGenus.put(item.getGenusType(), item);
        
        try (org.osid.type.TypeList types = item.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.itemsByRecord.put(types.getNextType(), item);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an item from this session.
     *
     *  @param itemId the <code>Id</code> of the item
     *  @throws org.osid.NullArgumentException <code>itemId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeItem(org.osid.id.Id itemId) {
        org.osid.assessment.Item item;
        try {
            item = getItem(itemId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.itemsByGenus.remove(item.getGenusType());

        try (org.osid.type.TypeList types = item.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.itemsByRecord.remove(types.getNextType(), item);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeItem(itemId);
        return;
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> which does not include
     *  items of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known items or an error results. Otherwise,
     *  the returned list may contain only those items that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.item.ArrayItemList(this.itemsByGenus.get(itemGenusType)));
    }


    /**
     *  Gets an <code>ItemList</code> containing the given
     *  item record <code>Type</code>. In plenary mode, the
     *  returned list contains all known items or an error
     *  results. Otherwise, the returned list may contain only those
     *  items that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned <code>item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.item.ArrayItemList(this.itemsByRecord.get(itemRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.itemsByGenus.clear();
        this.itemsByRecord.clear();

        super.close();

        return;
    }
}
