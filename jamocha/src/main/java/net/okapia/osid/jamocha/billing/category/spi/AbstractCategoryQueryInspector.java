//
// AbstractCategoryQueryInspector.java
//
//     A template for making a CategoryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.category.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for categories.
 */

public abstract class AbstractCategoryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.billing.CategoryQueryInspector {

    private final java.util.Collection<org.osid.billing.records.CategoryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the entry query terms 
     */

    @OSID @Override
    public org.osid.billing.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.billing.EntryQueryInspector[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given category query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a category implementing the requested record.
     *
     *  @param categoryRecordType a category record type
     *  @return the category query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>categoryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(categoryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.CategoryQueryInspectorRecord getCategoryQueryInspectorRecord(org.osid.type.Type categoryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.CategoryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(categoryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(categoryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this category query. 
     *
     *  @param categoryQueryInspectorRecord category query inspector
     *         record
     *  @param categoryRecordType category record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCategoryQueryInspectorRecord(org.osid.billing.records.CategoryQueryInspectorRecord categoryQueryInspectorRecord, 
                                                   org.osid.type.Type categoryRecordType) {

        addRecordType(categoryRecordType);
        nullarg(categoryRecordType, "category record type");
        this.records.add(categoryQueryInspectorRecord);        
        return;
    }
}
