//
// PositionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.position.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PositionElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the PositionElement Id.
     *
     *  @return the position element Id
     */

    public static org.osid.id.Id getPositionEntityId() {
        return (makeEntityId("osid.personnel.Position"));
    }


    /**
     *  Gets the OrganizationId element Id.
     *
     *  @return the OrganizationId element Id
     */

    public static org.osid.id.Id getOrganizationId() {
        return (makeElementId("osid.personnel.position.OrganizationId"));
    }


    /**
     *  Gets the Organization element Id.
     *
     *  @return the Organization element Id
     */

    public static org.osid.id.Id getOrganization() {
        return (makeElementId("osid.personnel.position.Organization"));
    }


    /**
     *  Gets the Title element Id.
     *
     *  @return the Title element Id
     */

    public static org.osid.id.Id getTitle() {
        return (makeElementId("osid.personnel.position.Title"));
    }


    /**
     *  Gets the LevelId element Id.
     *
     *  @return the LevelId element Id
     */

    public static org.osid.id.Id getLevelId() {
        return (makeElementId("osid.personnel.position.LevelId"));
    }


    /**
     *  Gets the Level element Id.
     *
     *  @return the Level element Id
     */

    public static org.osid.id.Id getLevel() {
        return (makeElementId("osid.personnel.position.Level"));
    }


    /**
     *  Gets the QualificationIds element Id.
     *
     *  @return the QualificationIds element Id
     */

    public static org.osid.id.Id getQualificationIds() {
        return (makeElementId("osid.personnel.position.QualificationIds"));
    }


    /**
     *  Gets the Qualifications element Id.
     *
     *  @return the Qualifications element Id
     */

    public static org.osid.id.Id getQualifications() {
        return (makeElementId("osid.personnel.position.Qualifications"));
    }


    /**
     *  Gets the TargetAppointments element Id.
     *
     *  @return the TargetAppointments element Id
     */

    public static org.osid.id.Id getTargetAppointments() {
        return (makeElementId("osid.personnel.position.TargetAppointments"));
    }


    /**
     *  Gets the RequiredCommitment element Id.
     *
     *  @return the RequiredCommitment element Id
     */

    public static org.osid.id.Id getRequiredCommitment() {
        return (makeElementId("osid.personnel.position.RequiredCommitment"));
    }


    /**
     *  Gets the LowSalaryRange element Id.
     *
     *  @return the LowSalaryRange element Id
     */

    public static org.osid.id.Id getLowSalaryRange() {
        return (makeElementId("osid.personnel.position.LowSalaryRange"));
    }


    /**
     *  Gets the MidpointSalaryRange element Id.
     *
     *  @return the MidpointSalaryRange element Id
     */

    public static org.osid.id.Id getMidpointSalaryRange() {
        return (makeElementId("osid.personnel.position.MidpointSalaryRange"));
    }


    /**
     *  Gets the HighSalaryRange element Id.
     *
     *  @return the HighSalaryRange element Id
     */

    public static org.osid.id.Id getHighSalaryRange() {
        return (makeElementId("osid.personnel.position.HighSalaryRange"));
    }


    /**
     *  Gets the CompensationFrequency element Id.
     *
     *  @return the CompensationFrequency element Id
     */

    public static org.osid.id.Id getCompensationFrequency() {
        return (makeElementId("osid.personnel.position.CompensationFrequency"));
    }


    /**
     *  Gets the BenefitsType element Id.
     *
     *  @return the BenefitsType element Id
     */

    public static org.osid.id.Id getBenefitsType() {
        return (makeElementId("osid.personnel.position.BenefitsType"));
    }


    /**
     *  Gets the Exempt element Id.
     *
     *  @return the Exempt element Id
     */

    public static org.osid.id.Id getExempt() {
        return (makeQueryElementId("osid.personnel.position.Exempt"));
    }


    /**
     *  Gets the RealmId element Id.
     *
     *  @return the RealmId element Id
     */

    public static org.osid.id.Id getRealmId() {
        return (makeQueryElementId("osid.personnel.position.RealmId"));
    }


    /**
     *  Gets the Realm element Id.
     *
     *  @return the Realm element Id
     */

    public static org.osid.id.Id getRealm() {
        return (makeQueryElementId("osid.personnel.position.Realm"));
    }
}
