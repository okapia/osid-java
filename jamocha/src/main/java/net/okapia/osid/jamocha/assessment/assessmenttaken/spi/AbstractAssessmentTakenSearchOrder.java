//
// AbstractAssessmentTakenSearchOdrer.java
//
//     Defines an AssessmentTakenSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmenttaken.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AssessmentTakenSearchOrder}.
 */

public abstract class AbstractAssessmentTakenSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.assessment.AssessmentTakenSearchOrder {

    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  offered. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAssessmentOffered(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment search order is available. 
     *
     *  @return <code> true </code> if an assessment offered search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment offered search order. 
     *
     *  @return an assessment offered search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedSearchOrder getAssessmentOfferedSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAssessmentOfferedSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTaker(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTakerSearchOrder() {
        return (false);
    }


    /**
     *  Gets a resource search order. 
     *
     *  @return a resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTakerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getTakerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTakerSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTakingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTakingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets an agent search order. 
     *
     *  @return an agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTakingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getTakingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTakingAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  start time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByActualStartTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the assessment 
     *  deadline. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCompletionTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the time spent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimeSpent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the grade 
     *  system. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScoreSystem(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade system search order is available. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScoreSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade system search order. 
     *
     *  @return a grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScoreSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getScoreSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsScoreSystemSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByScore(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the grade. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrade(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade search order is available. 
     *
     *  @return <code> true </code> if a grade search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSearchOrder() {
        return (false);
    }


    /**
     *  Gets a grade search order. 
     *
     *  @return a grade search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getGradeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the comments. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFeedback(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the rubric 
     *  assessment. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRubric(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an assessment taken search order is available. 
     *
     *  @return <code> true </code> if an assessment taken search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRubricSearchOrder() {
        return (false);
    }


    /**
     *  Gets an assessment taken search order. 
     *
     *  @return a rubric assessment taken search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRubricSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenSearchOrder getRubricSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRubricSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  assessmentTakenRecordType an assessment taken record type 
     *  @return {@code true} if the assessmentTakenRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentTakenRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentTakenRecordType) {
        for (org.osid.assessment.records.AssessmentTakenSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  assessmentTakenRecordType the assessment taken record type 
     *  @return the assessment taken search order record
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentTakenRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(assessmentTakenRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenSearchOrderRecord getAssessmentTakenSearchOrderRecord(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentTakenSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(assessmentTakenRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentTakenRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this assessment taken. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentTakenRecord the assessment taken search odrer record
     *  @param assessmentTakenRecordType assessment taken record type
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentTakenRecord} or
     *          {@code assessmentTakenRecordTypeassessmentTaken} is
     *          {@code null}
     */
            
    protected void addAssessmentTakenRecord(org.osid.assessment.records.AssessmentTakenSearchOrderRecord assessmentTakenSearchOrderRecord, 
                                     org.osid.type.Type assessmentTakenRecordType) {

        addRecordType(assessmentTakenRecordType);
        this.records.add(assessmentTakenSearchOrderRecord);
        
        return;
    }
}
