//
// AbstractAssemblyPostQuery.java
//
//     A PostQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.financials.posting.post.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PostQuery that stores terms.
 */

public abstract class AbstractAssemblyPostQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.financials.posting.PostQuery,
               org.osid.financials.posting.PostQueryInspector,
               org.osid.financials.posting.PostSearchOrder {

    private final java.util.Collection<org.osid.financials.posting.records.PostQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.posting.records.PostQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.financials.posting.records.PostSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPostQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPostQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the fiscal period <code> Id </code> for this query. 
     *
     *  @param  fiscalPeriodId a fiscal period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foscalPeriodId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchFiscalPeriodId(org.osid.id.Id fiscalPeriodId, 
                                    boolean match) {
        getAssembler().addIdTerm(getFiscalPeriodIdColumn(), fiscalPeriodId, match);
        return;
    }


    /**
     *  Clears the fiscal period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodIdTerms() {
        getAssembler().clearTerms(getFiscalPeriodIdColumn());
        return;
    }


    /**
     *  Gets the fiscal period <code> Id </code> query terms. 
     *
     *  @return the fiscal period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFiscalPeriodIdTerms() {
        return (getAssembler().getIdTerms(getFiscalPeriodIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by fiscal period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFiscalPeriod(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFiscalPeriodColumn(), style);
        return;
    }


    /**
     *  Gets the FiscalPeriodId column name.
     *
     * @return the column name
     */

    protected String getFiscalPeriodIdColumn() {
        return ("fiscal_period_id");
    }


    /**
     *  Tests if a <code> FiscalPeriod </code> is available. 
     *
     *  @return <code> true </code> if a fiscal period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a fiscal period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the fiscal period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQuery getFiscalPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodQuery() is false");
    }


    /**
     *  Matches any fiscal period. 
     *
     *  @param  match <code> true </code> to match posts with any fiscal 
     *          period, <code> false </code> to match posts with no fiscal 
     *          period 
     */

    @OSID @Override
    public void matchAnyFiscalPeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getFiscalPeriodColumn(), match);
        return;
    }


    /**
     *  Clears the fiscal period terms. 
     */

    @OSID @Override
    public void clearFiscalPeriodTerms() {
        getAssembler().clearTerms(getFiscalPeriodColumn());
        return;
    }


    /**
     *  Gets the fiscal period query terms. 
     *
     *  @return the fiscal period query terms 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQueryInspector[] getFiscalPeriodTerms() {
        return (new org.osid.financials.FiscalPeriodQueryInspector[0]);
    }


    /**
     *  Tests if a fiscal period search order is available. 
     *
     *  @return <code> true </code> if a fiscal period search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFiscalPeriodSearchOrder() {
        return (false);
    }


    /**
     *  Gets the fiscal period search order. 
     *
     *  @return the fiscal period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFiscalPeriodSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodSearchOrder getFiscalPeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFiscalPeriodSearchOrder() is false");
    }


    /**
     *  Gets the FiscalPeriod column name.
     *
     * @return the column name
     */

    protected String getFiscalPeriodColumn() {
        return ("fiscal_period");
    }


    /**
     *  Matches posts that have been posted. 
     *
     *  @param  match <code> true </code> to match posted posts <code> false 
     *          </code> to match unposted posts 
     */

    @OSID @Override
    public void matchPosted(boolean match) {
        getAssembler().addBooleanTerm(getPostedColumn(), match);
        return;
    }


    /**
     *  Clears the posted terms. 
     */

    @OSID @Override
    public void clearPostedTerms() {
        getAssembler().clearTerms(getPostedColumn());
        return;
    }


    /**
     *  Gets the posted query terms. 
     *
     *  @return the posted query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getPostedTerms() {
        return (getAssembler().getBooleanTerms(getPostedColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the posted 
     *  status. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPosted(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPostedColumn(), style);
        return;
    }


    /**
     *  Gets the Posted column name.
     *
     * @return the column name
     */

    protected String getPostedColumn() {
        return ("posted");
    }


    /**
     *  Matches the date between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     *  @throws org.osid.NullArgumentException <code> low </code> or <code> 
     *          high </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime low, 
                          org.osid.calendaring.DateTime high, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches items that have any date set. 
     *
     *  @param  match <code> true </code> to match items with any date, <code> 
     *          false </code> to match items with no date 
     */

    @OSID @Override
    public void matchAnyDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDateColumn(), match);
        return;
    }


    /**
     *  Clears the date terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        getAssembler().clearTerms(getDateColumn());
        return;
    }


    /**
     *  Gets the date query terms. 
     *
     *  @return the date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDateColumn(), style);
        return;
    }


    /**
     *  Gets the Date column name.
     *
     * @return the column name
     */

    protected String getDateColumn() {
        return ("date");
    }


    /**
     *  Sets the post entry <code> Id </code> for this query to match posts 
     *  that have a related post entries. 
     *
     *  @param  postEntryId a post entry <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPostEntryId(org.osid.id.Id postEntryId, boolean match) {
        getAssembler().addIdTerm(getPostEntryIdColumn(), postEntryId, match);
        return;
    }


    /**
     *  Clears the post entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPostEntryIdTerms() {
        getAssembler().clearTerms(getPostEntryIdColumn());
        return;
    }


    /**
     *  Gets the post entry <code> Id </code> query terms. 
     *
     *  @return the post entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPostEntryIdTerms() {
        return (getAssembler().getIdTerms(getPostEntryIdColumn()));
    }


    /**
     *  Gets the PostEntryId column name.
     *
     * @return the column name
     */

    protected String getPostEntryIdColumn() {
        return ("post_entry_id");
    }


    /**
     *  Tests if a <code> PostEntryQuery </code> is available for the 
     *  location. 
     *
     *  @return <code> true </code> if a post entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPostEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a post entries. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the post entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPostEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQuery getPostEntryQuery() {
        throw new org.osid.UnimplementedException("supportsPostEntryQuery() is false");
    }


    /**
     *  Matches any posts with post entries. 
     *
     *  @param  match <code> true </code> to match posts with any post entry, 
     *          <code> false </code> to match posts with no entries 
     */

    @OSID @Override
    public void matchAnyPostEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getPostEntryColumn(), match);
        return;
    }


    /**
     *  Clears the post entry terms. 
     */

    @OSID @Override
    public void clearPostEntryTerms() {
        getAssembler().clearTerms(getPostEntryColumn());
        return;
    }


    /**
     *  Gets the post entry query terms. 
     *
     *  @return the post entry query terms 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryQueryInspector[] getPostEntryTerms() {
        return (new org.osid.financials.posting.PostEntryQueryInspector[0]);
    }


    /**
     *  Gets the PostEntry column name.
     *
     * @return the column name
     */

    protected String getPostEntryColumn() {
        return ("post_entry");
    }


    /**
     *  Sets the post entry <code> Id </code> for this query to match posts 
     *  that have a corrected post. 
     *
     *  @param  postId a post <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> postId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchCorrectedPostId(org.osid.id.Id postId, boolean match) {
        getAssembler().addIdTerm(getCorrectedPostIdColumn(), postId, match);
        return;
    }


    /**
     *  Clears the corrected post <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCorrectedPostIdTerms() {
        getAssembler().clearTerms(getCorrectedPostIdColumn());
        return;
    }


    /**
     *  Gets the corrected post <code> Id </code> query terms. 
     *
     *  @return the post <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCorrectedPostIdTerms() {
        return (getAssembler().getIdTerms(getCorrectedPostIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the corrected 
     *  post. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCorrectedPost(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCorrectedPostColumn(), style);
        return;
    }


    /**
     *  Gets the CorrectedPostId column name.
     *
     * @return the column name
     */

    protected String getCorrectedPostIdColumn() {
        return ("corrected_post_id");
    }


    /**
     *  Tests if a <code> PostQuery </code> is available. 
     *
     *  @return <code> true </code> if a post query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCorrectedPostQuery() {
        return (false);
    }


    /**
     *  Gets the query for a corrected posts. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the post query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCorrectedPostQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQuery getCorrectedPostQuery() {
        throw new org.osid.UnimplementedException("supportsCorrectedPostQuery() is false");
    }


    /**
     *  Matches any posts with corrected posts. 
     *
     *  @param  match <code> true </code> to match posts with any corrected 
     *          post, <code> false </code> to match posts with no corrected 
     *          posts 
     */

    @OSID @Override
    public void matchAnyCorrectedPost(boolean match) {
        getAssembler().addIdWildcardTerm(getCorrectedPostColumn(), match);
        return;
    }


    /**
     *  Clears the corrected post terms. 
     */

    @OSID @Override
    public void clearCorrectedPostTerms() {
        getAssembler().clearTerms(getCorrectedPostColumn());
        return;
    }


    /**
     *  Gets the corrected post query terms. 
     *
     *  @return the post query terms 
     */

    @OSID @Override
    public org.osid.financials.posting.PostQueryInspector[] getCorrectedPostTerms() {
        return (new org.osid.financials.posting.PostQueryInspector[0]);
    }


    /**
     *  Tests if a post search order is available. 
     *
     *  @return <code> true </code> if a post search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCorrectedPostSearchOrder() {
        return (false);
    }


    /**
     *  Gets the post search order. 
     *
     *  @return the corrected post search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCorrectedPostSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.PostSearchOrder getCorrectedPostSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCorrectedPostSearchOrder() is false");
    }


    /**
     *  Gets the CorrectedPost column name.
     *
     * @return the column name
     */

    protected String getCorrectedPostColumn() {
        return ("corrected_post");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match posts 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this post supports the given record
     *  <code>Type</code>.
     *
     *  @param  postRecordType a post record type 
     *  @return <code>true</code> if the postRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type postRecordType) {
        for (org.osid.financials.posting.records.PostQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(postRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  postRecordType the post record type 
     *  @return the post query record 
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostQueryRecord getPostQueryRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  postRecordType the post record type 
     *  @return the post query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostQueryInspectorRecord getPostQueryInspectorRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param postRecordType the post record type
     *  @return the post search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>postRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostSearchOrderRecord getPostSearchOrderRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(postRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this post. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param postQueryRecord the post query record
     *  @param postQueryInspectorRecord the post query inspector
     *         record
     *  @param postSearchOrderRecord the post search order record
     *  @param postRecordType post record type
     *  @throws org.osid.NullArgumentException
     *          <code>postQueryRecord</code>,
     *          <code>postQueryInspectorRecord</code>,
     *          <code>postSearchOrderRecord</code> or
     *          <code>postRecordTypepost</code> is
     *          <code>null</code>
     */
            
    protected void addPostRecords(org.osid.financials.posting.records.PostQueryRecord postQueryRecord, 
                                      org.osid.financials.posting.records.PostQueryInspectorRecord postQueryInspectorRecord, 
                                      org.osid.financials.posting.records.PostSearchOrderRecord postSearchOrderRecord, 
                                      org.osid.type.Type postRecordType) {

        addRecordType(postRecordType);

        nullarg(postQueryRecord, "post query record");
        nullarg(postQueryInspectorRecord, "post query inspector record");
        nullarg(postSearchOrderRecord, "post search odrer record");

        this.queryRecords.add(postQueryRecord);
        this.queryInspectorRecords.add(postQueryInspectorRecord);
        this.searchOrderRecords.add(postSearchOrderRecord);
        
        return;
    }
}
