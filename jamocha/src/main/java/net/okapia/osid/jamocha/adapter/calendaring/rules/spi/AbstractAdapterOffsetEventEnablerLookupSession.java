//
// AbstractAdapterOffsetEventEnablerLookupSession.java
//
//    An OffsetEventEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An OffsetEventEnabler lookup session adapter.
 */

public abstract class AbstractAdapterOffsetEventEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.rules.OffsetEventEnablerLookupSession {

    private final org.osid.calendaring.rules.OffsetEventEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterOffsetEventEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterOffsetEventEnablerLookupSession(org.osid.calendaring.rules.OffsetEventEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code OffsetEventEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupOffsetEventEnablers() {
        return (this.session.canLookupOffsetEventEnablers());
    }


    /**
     *  A complete view of the {@code OffsetEventEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOffsetEventEnablerView() {
        this.session.useComparativeOffsetEventEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code OffsetEventEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOffsetEventEnablerView() {
        this.session.usePlenaryOffsetEventEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offset event enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active offset event enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveOffsetEventEnablerView() {
        this.session.useActiveOffsetEventEnablerView();
        return;
    }


    /**
     *  Active and inactive offset event enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusOffsetEventEnablerView() {
        this.session.useAnyStatusOffsetEventEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code OffsetEventEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code OffsetEventEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code OffsetEventEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, offset event enablers are returned that are currently
     *  active. In any status mode, active and inactive offset event enablers
     *  are returned.
     *
     *  @param offsetEventEnablerId {@code Id} of the {@code OffsetEventEnabler}
     *  @return the offset event enabler
     *  @throws org.osid.NotFoundException {@code offsetEventEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code offsetEventEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnabler getOffsetEventEnabler(org.osid.id.Id offsetEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventEnabler(offsetEventEnablerId));
    }


    /**
     *  Gets an {@code OffsetEventEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offsetEventEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code OffsetEventEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, offset event enablers are returned that are currently
     *  active. In any status mode, active and inactive offset event enablers
     *  are returned.
     *
     *  @param  offsetEventEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code OffsetEventEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablersByIds(org.osid.id.IdList offsetEventEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventEnablersByIds(offsetEventEnablerIds));
    }


    /**
     *  Gets an {@code OffsetEventEnablerList} corresponding to the given
     *  offset event enabler genus {@code Type} which does not include
     *  offset event enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  offset event enablers or an error results. Otherwise, the returned list
     *  may contain only those offset event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset event enablers are returned that are currently
     *  active. In any status mode, active and inactive offset event enablers
     *  are returned.
     *
     *  @param  offsetEventEnablerGenusType an offsetEventEnabler genus type 
     *  @return the returned {@code OffsetEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablersByGenusType(org.osid.type.Type offsetEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventEnablersByGenusType(offsetEventEnablerGenusType));
    }


    /**
     *  Gets an {@code OffsetEventEnablerList} corresponding to the given
     *  offset event enabler genus {@code Type} and include any additional
     *  offset event enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  offset event enablers or an error results. Otherwise, the returned list
     *  may contain only those offset event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset event enablers are returned that are currently
     *  active. In any status mode, active and inactive offset event enablers
     *  are returned.
     *
     *  @param  offsetEventEnablerGenusType an offsetEventEnabler genus type 
     *  @return the returned {@code OffsetEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablersByParentGenusType(org.osid.type.Type offsetEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventEnablersByParentGenusType(offsetEventEnablerGenusType));
    }


    /**
     *  Gets an {@code OffsetEventEnablerList} containing the given
     *  offset event enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offset event enablers or an error results. Otherwise, the returned list
     *  may contain only those offset event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset event enablers are returned that are currently
     *  active. In any status mode, active and inactive offset event enablers
     *  are returned.
     *
     *  @param  offsetEventEnablerRecordType an offsetEventEnabler record type 
     *  @return the returned {@code OffsetEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code offsetEventEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablersByRecordType(org.osid.type.Type offsetEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventEnablersByRecordType(offsetEventEnablerRecordType));
    }


    /**
     *  Gets an {@code OffsetEventEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  offset event enablers or an error results. Otherwise, the returned list
     *  may contain only those offset event enablers that are accessible
     *  through this session.
     *  
     *  In active mode, offset event enablers are returned that are currently
     *  active. In any status mode, active and inactive offset event enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code OffsetEventEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventEnablersOnDate(from, to));
    }
        

    /**
     *  Gets an {@code OffsetEventEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  offset event enablers or an error results. Otherwise, the returned list
     *  may contain only those offset event enablers that are accessible
     *  through this session.
     *
     *  In active mode, offset event enablers are returned that are currently
     *  active. In any status mode, active and inactive offset event enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code OffsetEventEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getOffsetEventEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code OffsetEventEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  offset event enablers or an error results. Otherwise, the returned list
     *  may contain only those offset event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offset event enablers are returned that are currently
     *  active. In any status mode, active and inactive offset event enablers
     *  are returned.
     *
     *  @return a list of {@code OffsetEventEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.OffsetEventEnablerList getOffsetEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getOffsetEventEnablers());
    }
}
