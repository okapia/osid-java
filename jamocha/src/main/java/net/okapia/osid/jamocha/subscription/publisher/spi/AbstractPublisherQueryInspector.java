//
// AbstractPublisherQueryInspector.java
//
//     A template for making a PublisherQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.publisher.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for publishers.
 */

public abstract class AbstractPublisherQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.subscription.PublisherQueryInspector {

    private final java.util.Collection<org.osid.subscription.records.PublisherQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the subscription <code> Id </code> terms. 
     *
     *  @return the subscription <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubscriptionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the subscription terms. 
     *
     *  @return the subscription terms 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQueryInspector[] getSubscriptionTerms() {
        return (new org.osid.subscription.SubscriptionQueryInspector[0]);
    }


    /**
     *  Gets the dispatch <code> Id </code> terms. 
     *
     *  @return the dispatch <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDispatchIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the dispatch terms. 
     *
     *  @return the dispatch terms 
     */

    @OSID @Override
    public org.osid.subscription.DispatchQueryInspector[] getDispatchTerms() {
        return (new org.osid.subscription.DispatchQueryInspector[0]);
    }


    /**
     *  Gets the ancestor publisher <code> Id </code> terms. 
     *
     *  @return the ancestor publisher <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorPublisherIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor publisher terms. 
     *
     *  @return the ancestor publisher terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getAncestorPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }


    /**
     *  Gets the descendant publisher <code> Id </code> terms. 
     *
     *  @return the descendant publisher <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantPublisherIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant publisher terms. 
     *
     *  @return the descendant publisher terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getDescendantPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given publisher query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a publisher implementing the requested record.
     *
     *  @param publisherRecordType a publisher record type
     *  @return the publisher query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(publisherRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.PublisherQueryInspectorRecord getPublisherQueryInspectorRecord(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.PublisherQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(publisherRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(publisherRecordType + " is not supported");
    }


    /**
     *  Adds a record to this publisher query. 
     *
     *  @param publisherQueryInspectorRecord publisher query inspector
     *         record
     *  @param publisherRecordType publisher record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPublisherQueryInspectorRecord(org.osid.subscription.records.PublisherQueryInspectorRecord publisherQueryInspectorRecord, 
                                                   org.osid.type.Type publisherRecordType) {

        addRecordType(publisherRecordType);
        nullarg(publisherRecordType, "publisher record type");
        this.records.add(publisherQueryInspectorRecord);        
        return;
    }
}
