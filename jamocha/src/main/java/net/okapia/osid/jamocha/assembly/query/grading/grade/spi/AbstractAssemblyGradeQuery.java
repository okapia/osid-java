//
// AbstractAssemblyGradeQuery.java
//
//     A GradeQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.grading.grade.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GradeQuery that stores terms.
 */

public abstract class AbstractAssemblyGradeQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.grading.GradeQuery,
               org.osid.grading.GradeQueryInspector,
               org.osid.grading.GradeSearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradeQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradeQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradeSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyGradeQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyGradeQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeSystemId(org.osid.id.Id gradeSystemId, boolean match) {
        getAssembler().addIdTerm(getGradeSystemIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeSystemIdTerms() {
        getAssembler().clearTerms(getGradeSystemIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeSystemIdTerms() {
        return (getAssembler().getIdTerms(getGradeSystemIdColumn()));
    }


    /**
     *  Specified a preference for ordering results by the grade system. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGradeSystem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGradeSystemColumn(), style);
        return;
    }


    /**
     *  Gets the GradeSystemId column name.
     *
     * @return the column name
     */

    protected String getGradeSystemIdColumn() {
        return ("grade_system_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available for querying 
     *  grade systems. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade system. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradeSystemQuery() {
        throw new org.osid.UnimplementedException("supportsGradeSystemQuery() is false");
    }


    /**
     *  Clears the grade system terms. 
     */

    @OSID @Override
    public void clearGradeSystemTerms() {
        getAssembler().clearTerms(getGradeSystemColumn());
        return;
    }


    /**
     *  Gets the grade system terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradeSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Tests if a <code> GradeSystemSearchOrder </code> interface is 
     *  available for grade systems. 
     *
     *  @return <code> true </code> if a grade system search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the search order for a grade system. 
     *
     *  @return the grade system search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGradeSystemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGradeSystemSearchOrder() is false");
    }


    /**
     *  Gets the GradeSystem column name.
     *
     * @return the column name
     */

    protected String getGradeSystemColumn() {
        return ("grade_system");
    }


    /**
     *  Matches grades with the start input score inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchInputScoreStartRange(java.math.BigDecimal start, 
                                          java.math.BigDecimal end, 
                                          boolean match) {
        getAssembler().addDecimalRangeTerm(getInputScoreStartRangeColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the nput score start range terms. 
     */

    @OSID @Override
    public void clearInputScoreStartRangeTerms() {
        getAssembler().clearTerms(getInputScoreStartRangeColumn());
        return;
    }


    /**
     *  Gets the input score start range terms. 
     *
     *  @return the input score start range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getInputScoreStartRangeTerms() {
        return (getAssembler().getDecimalRangeTerms(getInputScoreStartRangeColumn()));
    }


    /**
     *  Specified a preference for ordering results by start of the input 
     *  score range. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInputScoreStartRange(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInputScoreStartRangeColumn(), style);
        return;
    }


    /**
     *  Gets the InputScoreStartRange column name.
     *
     * @return the column name
     */

    protected String getInputScoreStartRangeColumn() {
        return ("input_score_start_range");
    }


    /**
     *  Matches grades with the end input score inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchInputScoreEndRange(java.math.BigDecimal start, 
                                        java.math.BigDecimal end, 
                                        boolean match) {
        getAssembler().addDecimalRangeTerm(getInputScoreEndRangeColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the nput score start range terms. 
     */

    @OSID @Override
    public void clearInputScoreEndRangeTerms() {
        getAssembler().clearTerms(getInputScoreEndRangeColumn());
        return;
    }


    /**
     *  Gets the input score end range terms. 
     *
     *  @return the input score end range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getInputScoreEndRangeTerms() {
        return (getAssembler().getDecimalRangeTerms(getInputScoreEndRangeColumn()));
    }


    /**
     *  Specified a preference for ordering results by end of the input score 
     *  range. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByInputScoreEndRange(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getInputScoreEndRangeColumn(), style);
        return;
    }


    /**
     *  Gets the InputScoreEndRange column name.
     *
     * @return the column name
     */

    protected String getInputScoreEndRangeColumn() {
        return ("input_score_end_range");
    }


    /**
     *  Matches grades with the input score range contained within the given 
     *  range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchInputScore(java.math.BigDecimal start, 
                                java.math.BigDecimal end, boolean match) {
        getAssembler().addDecimalRangeTerm(getInputScoreColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the input score start range terms. 
     */

    @OSID @Override
    public void clearInputScoreTerms() {
        getAssembler().clearTerms(getInputScoreColumn());
        return;
    }


    /**
     *  Gets the input score terms. 
     *
     *  @return the input score range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getInputScoreTerms() {
        return (getAssembler().getDecimalRangeTerms(getInputScoreColumn()));
    }


    /**
     *  Gets the InputScore column name.
     *
     * @return the column name
     */

    protected String getInputScoreColumn() {
        return ("input_score");
    }


    /**
     *  Matches grades with the output score contained within the given range 
     *  inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchOutputScore(java.math.BigDecimal start, 
                                 java.math.BigDecimal end, boolean match) {
        getAssembler().addDecimalRangeTerm(getOutputScoreColumn(), start, end, match);
        return;
    }


    /**
     *  Clears the output score terms. 
     */

    @OSID @Override
    public void clearOutputScoreTerms() {
        getAssembler().clearTerms(getOutputScoreColumn());
        return;
    }


    /**
     *  Gets the output score terms. 
     *
     *  @return the output score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getOutputScoreTerms() {
        return (getAssembler().getDecimalRangeTerms(getOutputScoreColumn()));
    }


    /**
     *  Specified a preference for ordering results by the output score. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOutputScore(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOutputScoreColumn(), style);
        return;
    }


    /**
     *  Gets the OutputScore column name.
     *
     * @return the column name
     */

    protected String getOutputScoreColumn() {
        return ("output_score");
    }


    /**
     *  Sets the grade entry <code> Id </code> for this query. 
     *
     *  @param  gradeEntryId a grade entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradeEntryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradeEntryId(org.osid.id.Id gradeEntryId, boolean match) {
        getAssembler().addIdTerm(getGradeEntryIdColumn(), gradeEntryId, match);
        return;
    }


    /**
     *  Clears the grade entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradeEntryIdTerms() {
        getAssembler().clearTerms(getGradeEntryIdColumn());
        return;
    }


    /**
     *  Gets the grade entry <code> Id </code> terms. 
     *
     *  @return the grade entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeEntryIdTerms() {
        return (getAssembler().getIdTerms(getGradeEntryIdColumn()));
    }


    /**
     *  Gets the GradeEntryId column name.
     *
     * @return the column name
     */

    protected String getGradeEntryIdColumn() {
        return ("grade_entry_id");
    }


    /**
     *  Tests if a <code> GradeEntryQuery </code> is available for querying 
     *  grade entries. 
     *
     *  @return <code> true </code> if a grade entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grade entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the grade entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQuery getGradeEntryQuery() {
        throw new org.osid.UnimplementedException("supportsGradeEntryQuery() is false");
    }


    /**
     *  Matches grades that are assigned to any grade entry. 
     *
     *  @param  match <code> true </code> to match grades used in any grade 
     *          entry, <code> false </code> to match grades that are not used 
     *          in any grade entries 
     */

    @OSID @Override
    public void matchAnyGradeEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getGradeEntryColumn(), match);
        return;
    }


    /**
     *  Clears the grade entry terms. 
     */

    @OSID @Override
    public void clearGradeEntryTerms() {
        getAssembler().clearTerms(getGradeEntryColumn());
        return;
    }


    /**
     *  Gets the grade entry terms. 
     *
     *  @return the grade entry terms 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQueryInspector[] getGradeEntryTerms() {
        return (new org.osid.grading.GradeEntryQueryInspector[0]);
    }


    /**
     *  Gets the GradeEntry column name.
     *
     * @return the column name
     */

    protected String getGradeEntryColumn() {
        return ("grade_entry");
    }


    /**
     *  Sets the gradebook <code> Id </code> for this query. 
     *
     *  @param  gradebookId a gradebook <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradebookId(org.osid.id.Id gradebookId, boolean match) {
        getAssembler().addIdTerm(getGradebookIdColumn(), gradebookId, match);
        return;
    }


    /**
     *  Clears the gradebook <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradebookIdTerms() {
        getAssembler().clearTerms(getGradebookIdColumn());
        return;
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (getAssembler().getIdTerms(getGradebookIdColumn()));
    }


    /**
     *  Gets the GradebookId column name.
     *
     * @return the column name
     */

    protected String getGradebookIdColumn() {
        return ("gradebook_id");
    }


    /**
     *  Tests if a <code> GradebookQuery </code> is available . 
     *
     *  @return <code> true </code> if a gradebook query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a gradebook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the gradebook query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradebookQuery getGradebookQuery() {
        throw new org.osid.UnimplementedException("supportsGradebookQuery() is false");
    }


    /**
     *  Clears the gradebook terms. 
     */

    @OSID @Override
    public void clearGradebookTerms() {
        getAssembler().clearTerms(getGradebookColumn());
        return;
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }


    /**
     *  Gets the Gradebook column name.
     *
     * @return the column name
     */

    protected String getGradebookColumn() {
        return ("gradebook");
    }


    /**
     *  Tests if this grade supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradeRecordType a grade record type 
     *  @return <code>true</code> if the gradeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeRecordType) {
        for (org.osid.grading.records.GradeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  gradeRecordType the grade record type 
     *  @return the grade query record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeQueryRecord getGradeQueryRecord(org.osid.type.Type gradeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  gradeRecordType the grade record type 
     *  @return the grade query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeQueryInspectorRecord getGradeQueryInspectorRecord(org.osid.type.Type gradeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param gradeRecordType the grade record type
     *  @return the grade search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSearchOrderRecord getGradeSearchOrderRecord(org.osid.type.Type gradeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this grade. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeQueryRecord the grade query record
     *  @param gradeQueryInspectorRecord the grade query inspector
     *         record
     *  @param gradeSearchOrderRecord the grade search order record
     *  @param gradeRecordType grade record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradeQueryRecord</code>,
     *          <code>gradeQueryInspectorRecord</code>,
     *          <code>gradeSearchOrderRecord</code> or
     *          <code>gradeRecordTypegrade</code> is
     *          <code>null</code>
     */
            
    protected void addGradeRecords(org.osid.grading.records.GradeQueryRecord gradeQueryRecord, 
                                      org.osid.grading.records.GradeQueryInspectorRecord gradeQueryInspectorRecord, 
                                      org.osid.grading.records.GradeSearchOrderRecord gradeSearchOrderRecord, 
                                      org.osid.type.Type gradeRecordType) {

        addRecordType(gradeRecordType);

        nullarg(gradeQueryRecord, "grade query record");
        nullarg(gradeQueryInspectorRecord, "grade query inspector record");
        nullarg(gradeSearchOrderRecord, "grade search odrer record");

        this.queryRecords.add(gradeQueryRecord);
        this.queryInspectorRecords.add(gradeQueryInspectorRecord);
        this.searchOrderRecords.add(gradeSearchOrderRecord);
        
        return;
    }
}
