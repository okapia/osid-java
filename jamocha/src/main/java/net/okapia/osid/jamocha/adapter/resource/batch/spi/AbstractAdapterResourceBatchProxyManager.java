//
// AbstractResourceBatchProxyManager.java
//
//     An adapter for a ResourceBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ResourceBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterResourceBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.resource.batch.ResourceBatchProxyManager>
    implements org.osid.resource.batch.ResourceBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterResourceBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterResourceBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterResourceBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterResourceBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of resources is available. 
     *
     *  @return <code> true </code> if a resource bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceBatchAdmin() {
        return (getAdapteeManager().supportsResourceBatchAdmin());
    }


    /**
     *  Tests if bulk administration of resource relationships is available. 
     *
     *  @return <code> true </code> if a resource relationship bulk 
     *          administrative service is available, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRelationshipBatchAdmin() {
        return (getAdapteeManager().supportsResourceRelationshipBatchAdmin());
    }


    /**
     *  Tests if bulk administration of bins is available. 
     *
     *  @return <code> true </code> if an bin bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBinBatchAdmin() {
        return (getAdapteeManager().supportsBinBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk resource 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.batch.ResourceBatchAdminSession getResourceBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk resource 
     *  administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.batch.ResourceBatchAdminSession getResourceBatchAdminSessionForBin(org.osid.id.Id binId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceBatchAdminSessionForBin(binId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk resource 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipBatchAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.batch.ResourceRelationshipBatchAdminSession getResourceRelationshipBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk resource 
     *  relationship administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ResourceRelationshipBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRelationshipBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.batch.ResourceRelationshipBatchAdminSession getResourceRelationshipBatchAdminSessionForBin(org.osid.id.Id binId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRelationshipBatchAdminSessionForBin(binId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk bin 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BinBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBinBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.batch.BinBatchAdminSession getBinBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBinBatchAdminSession(proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
