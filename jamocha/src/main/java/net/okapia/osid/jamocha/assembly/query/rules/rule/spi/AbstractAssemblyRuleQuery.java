//
// AbstractAssemblyRuleQuery.java
//
//     A RuleQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.rules.rule.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RuleQuery that stores terms.
 */

public abstract class AbstractAssemblyRuleQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.rules.RuleQuery,
               org.osid.rules.RuleQueryInspector,
               org.osid.rules.RuleSearchOrder {

    private final java.util.Collection<org.osid.rules.records.RuleQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.records.RuleQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.records.RuleSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRuleQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRuleQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the engine <code> Id </code> for this query to match rules 
     *  assigned to engines. 
     *
     *  @param  engineId an engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEngineId(org.osid.id.Id engineId, boolean match) {
        getAssembler().addIdTerm(getEngineIdColumn(), engineId, match);
        return;
    }


    /**
     *  Clears the engine <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEngineIdTerms() {
        getAssembler().clearTerms(getEngineIdColumn());
        return;
    }


    /**
     *  Gets the engine <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEngineIdTerms() {
        return (getAssembler().getIdTerms(getEngineIdColumn()));
    }


    /**
     *  Gets the EngineId column name.
     *
     * @return the column name
     */

    protected String getEngineIdColumn() {
        return ("engine_id");
    }


    /**
     *  Tests if an <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuery getEngineQuery() {
        throw new org.osid.UnimplementedException("supportsEngineQuery() is false");
    }


    /**
     *  Clears the engine query terms. 
     */

    @OSID @Override
    public void clearEngineTerms() {
        getAssembler().clearTerms(getEngineColumn());
        return;
    }


    /**
     *  Gets the engine query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.EngineQueryInspector[] getEngineTerms() {
        return (new org.osid.rules.EngineQueryInspector[0]);
    }


    /**
     *  Gets the Engine column name.
     *
     * @return the column name
     */

    protected String getEngineColumn() {
        return ("engine");
    }


    /**
     *  Tests if this rule supports the given record
     *  <code>Type</code>.
     *
     *  @param  ruleRecordType a rule record type 
     *  @return <code>true</code> if the ruleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type ruleRecordType) {
        for (org.osid.rules.records.RuleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ruleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  ruleRecordType the rule record type 
     *  @return the rule query record 
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ruleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.records.RuleQueryRecord getRuleQueryRecord(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.records.RuleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ruleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ruleRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  ruleRecordType the rule record type 
     *  @return the rule query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ruleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.records.RuleQueryInspectorRecord getRuleQueryInspectorRecord(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.records.RuleQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(ruleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ruleRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param ruleRecordType the rule record type
     *  @return the rule search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>ruleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ruleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.records.RuleSearchOrderRecord getRuleSearchOrderRecord(org.osid.type.Type ruleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.records.RuleSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(ruleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ruleRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this rule. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param ruleQueryRecord the rule query record
     *  @param ruleQueryInspectorRecord the rule query inspector
     *         record
     *  @param ruleSearchOrderRecord the rule search order record
     *  @param ruleRecordType rule record type
     *  @throws org.osid.NullArgumentException
     *          <code>ruleQueryRecord</code>,
     *          <code>ruleQueryInspectorRecord</code>,
     *          <code>ruleSearchOrderRecord</code> or
     *          <code>ruleRecordTyperule</code> is
     *          <code>null</code>
     */
            
    protected void addRuleRecords(org.osid.rules.records.RuleQueryRecord ruleQueryRecord, 
                                      org.osid.rules.records.RuleQueryInspectorRecord ruleQueryInspectorRecord, 
                                      org.osid.rules.records.RuleSearchOrderRecord ruleSearchOrderRecord, 
                                      org.osid.type.Type ruleRecordType) {

        addRecordType(ruleRecordType);

        nullarg(ruleQueryRecord, "rule query record");
        nullarg(ruleQueryInspectorRecord, "rule query inspector record");
        nullarg(ruleSearchOrderRecord, "rule search odrer record");

        this.queryRecords.add(ruleQueryRecord);
        this.queryInspectorRecords.add(ruleQueryInspectorRecord);
        this.searchOrderRecords.add(ruleSearchOrderRecord);
        
        return;
    }
}
