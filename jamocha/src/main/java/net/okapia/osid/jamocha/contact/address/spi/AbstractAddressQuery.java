//
// AbstractAddressQuery.java
//
//     A template for making an Address Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.address.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for addresses.
 */

public abstract class AbstractAddressQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.contact.AddressQuery {

    private final java.util.Collection<org.osid.contact.records.AddressQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Matches the address text. 
     *
     *  @param  address the address text 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> address </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAddressText(String address, 
                                 org.osid.type.Type stringMatchType, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the text address terms. 
     */

    @OSID @Override
    public void clearAddressTextTerms() {
        return;
    }


    /**
     *  Sets the contact <code> Id </code> for this query to match contacts 
     *  assigned to addresses. 
     *
     *  @param  contactId a contact <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> contactId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchContactId(org.osid.id.Id contactId, boolean match) {
        return;
    }


    /**
     *  Clears the contact <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearContactIdTerms() {
        return;
    }


    /**
     *  Tests if a contact query is available. 
     *
     *  @return <code> true </code> if a contact query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address. 
     *
     *  @return the contact query 
     *  @throws org.osid.UnimplementedException <code> supportsContactQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactQuery getContactQuery() {
        throw new org.osid.UnimplementedException("supportsContactQuery() is false");
    }


    /**
     *  Matches addresses with any contact. 
     *
     *  @param  match <code> true </code> to match addresses with any contact, 
     *          <code> false </code> to match addresses with no contacts 
     */

    @OSID @Override
    public void matchAnyContact(boolean match) {
        return;
    }


    /**
     *  Clears the contact terms. 
     */

    @OSID @Override
    public void clearContactTerms() {
        return;
    }


    /**
     *  Sets the address <code> Id </code> for this query to match contacts 
     *  assigned to address books. 
     *
     *  @param  addressBookId an address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressBookId(org.osid.id.Id addressBookId, boolean match) {
        return;
    }


    /**
     *  Clears the address book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressBookIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book query. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsAddressBookQuery() is false");
    }


    /**
     *  Clears the address book terms. 
     */

    @OSID @Override
    public void clearAddressBookTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given address query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an address implementing the requested record.
     *
     *  @param addressRecordType an address record type
     *  @return the address query record
     *  @throws org.osid.NullArgumentException
     *          <code>addressRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(addressRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.records.AddressQueryRecord getAddressQueryRecord(org.osid.type.Type addressRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.records.AddressQueryRecord record : this.records) {
            if (record.implementsRecordType(addressRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(addressRecordType + " is not supported");
    }


    /**
     *  Adds a record to this address query. 
     *
     *  @param addressQueryRecord address query record
     *  @param addressRecordType address record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAddressQueryRecord(org.osid.contact.records.AddressQueryRecord addressQueryRecord, 
                                          org.osid.type.Type addressRecordType) {

        addRecordType(addressRecordType);
        nullarg(addressQueryRecord, "address query record");
        this.records.add(addressQueryRecord);        
        return;
    }
}
