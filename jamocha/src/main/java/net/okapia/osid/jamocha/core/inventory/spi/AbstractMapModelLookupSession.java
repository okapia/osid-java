//
// AbstractMapModelLookupSession
//
//    A simple framework for providing a Model lookup service
//    backed by a fixed collection of models.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Model lookup service backed by a
 *  fixed collection of models. The models are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Models</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapModelLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractModelLookupSession
    implements org.osid.inventory.ModelLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.inventory.Model> models = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.inventory.Model>());


    /**
     *  Makes a <code>Model</code> available in this session.
     *
     *  @param  model a model
     *  @throws org.osid.NullArgumentException <code>model<code>
     *          is <code>null</code>
     */

    protected void putModel(org.osid.inventory.Model model) {
        this.models.put(model.getId(), model);
        return;
    }


    /**
     *  Makes an array of models available in this session.
     *
     *  @param  models an array of models
     *  @throws org.osid.NullArgumentException <code>models<code>
     *          is <code>null</code>
     */

    protected void putModels(org.osid.inventory.Model[] models) {
        putModels(java.util.Arrays.asList(models));
        return;
    }


    /**
     *  Makes a collection of models available in this session.
     *
     *  @param  models a collection of models
     *  @throws org.osid.NullArgumentException <code>models<code>
     *          is <code>null</code>
     */

    protected void putModels(java.util.Collection<? extends org.osid.inventory.Model> models) {
        for (org.osid.inventory.Model model : models) {
            this.models.put(model.getId(), model);
        }

        return;
    }


    /**
     *  Removes a Model from this session.
     *
     *  @param  modelId the <code>Id</code> of the model
     *  @throws org.osid.NullArgumentException <code>modelId<code> is
     *          <code>null</code>
     */

    protected void removeModel(org.osid.id.Id modelId) {
        this.models.remove(modelId);
        return;
    }


    /**
     *  Gets the <code>Model</code> specified by its <code>Id</code>.
     *
     *  @param  modelId <code>Id</code> of the <code>Model</code>
     *  @return the model
     *  @throws org.osid.NotFoundException <code>modelId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>modelId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Model getModel(org.osid.id.Id modelId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.inventory.Model model = this.models.get(modelId);
        if (model == null) {
            throw new org.osid.NotFoundException("model not found: " + modelId);
        }

        return (model);
    }


    /**
     *  Gets all <code>Models</code>. In plenary mode, the returned
     *  list contains all known models or an error
     *  results. Otherwise, the returned list may contain only those
     *  models that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Models</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ModelList getModels()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inventory.model.ArrayModelList(this.models.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.models.clear();
        super.close();
        return;
    }
}
