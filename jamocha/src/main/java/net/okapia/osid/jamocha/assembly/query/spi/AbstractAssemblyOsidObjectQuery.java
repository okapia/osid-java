//
// AbstractAssemblyOsidObjectQuery.java
//
//     An OsidObjectQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OsidObjectQuery that stores terms.
 */

public abstract class AbstractAssemblyOsidObjectQuery
    extends AbstractAssemblyOsidIdentifiableQuery
    implements org.osid.OsidObjectQuery,
               org.osid.OsidObjectQueryInspector,
               org.osid.OsidObjectSearchOrder {

    private final AssemblyOsidBrowsableQuery query;
    

    /** 
     *  Constructs a new <code>AbstractAssemblyOsidObjectQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOsidObjectQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidBrowsableQuery(assembler);
        return;
    }
    
    
    /**
     *  Adds a display name to match. Multiple display name matches
     *  can be added to perform a boolean <code> OR </code> among
     *  them.
     *
     *  @param  displayName display name to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> displayName </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> displayName </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayName(String displayName, org.osid.type.Type stringMatchType, 
                                 boolean match) {
        getAssembler().addStringTerm(getDisplayNameColumn(), displayName, stringMatchType, match);
        return;
    }


    /**
     *  Matches any object with a display name. 
     *
     *  @param  match <code> true </code> to match any display name, <code> 
     *          false </code> to match objects with no display name 
     */

    @OSID @Override
    public void matchAnyDisplayName(boolean match) {
        getAssembler().addStringWildcardTerm(getDisplayNameColumn(), match);
        return;
    }


    /**
     *  Clears all display name terms. 
     */

    @OSID @Override
    public void clearDisplayNameTerms() {
        getAssembler().clearTerms(getDisplayNameColumn());
        return;
    }


    /**
     *  Gets the display name query terms.
     *
     *  @return the display name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayNameTerms() {
        return (getAssembler().getStringTerms(getDisplayNameColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the display 
     *  name. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisplayName(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDisplayNameColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the display name field.
     *
     *  @return the column name
     */

    protected String getDisplayNameColumn() {
        return ("display_name");
    }


    /**
     *  Adds a description to match. Multiple description matches
     *  can be added to perform a boolean <code> OR </code> among
     *  them.
     *
     *  @param  description description to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> description </code> 
     *          is not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> description </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDescription(String description, org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getDescriptionColumn(), description, stringMatchType, match);
        return;
    }


    /**
     *  Matches any object with a description.
     *
     *  @param  match <code> true </code> to match any description, <code> 
     *          false </code> to match objects with no description 
     */

    @OSID @Override
    public void matchAnyDescription(boolean match) {
        getAssembler().addStringWildcardTerm(getDescriptionColumn(), match);
        return;
    }


    /**
     *  Clears all description terms. 
     */

    @OSID @Override
    public void clearDescriptionTerms() {
        getAssembler().clearTerms(getDescriptionColumn());
        return;
    }


    /**
     *  Gets the description query terms.
     *
     *  @return the description terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDescriptionTerms() {
        return (getAssembler().getStringTerms(getDescriptionColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  description.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDescription(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDescriptionColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the description field.
     *
     *  @return the column name
     */

    protected String getDescriptionColumn() {
        return ("description");
    }


    /**
     *  Adds a genus type to match. Multiple genus type matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  genusType genus type to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> genusType
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchGenusType(org.osid.type.Type genusType, boolean match) {
        getAssembler().addTypeTerm(getGenusTypeColumn(), genusType, match);
        return;
    }


    /**
     *  Matches any object with a genus type.
     *
     *  @param match <code> true </code> to match any genus type,
     *          <code> false </code> to match objects with no genus
     *          type
     */

    @OSID @Override
    public void matchAnyGenusType(boolean match) {
        getAssembler().addTypeWildcardTerm(getGenusTypeColumn(), match);
        return;
    }


    /**
     *  Clears all genus type terms. 
     */

    @OSID @Override
    public void clearGenusTypeTerms() {
        getAssembler().clearTerms(getGenusTypeColumn());
        return;
    }


    /**
     *  Gets the genus type query terms.
     *
     *  @return the genus type terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getGenusTypeTerms() {
        return (getAssembler().getTypeTerms(getGenusTypeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  genus type.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGenusType(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getGenusTypeColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the genus type field.
     *
     *  @return the column name
     */

    protected String getGenusTypeColumn() {
        return ("genus_type");
    }


    /**
     *  Sets a <code> Type </code> for querying objects of a given
     *  genus. A genus type matches if the specified type is the same
     *  genus as the object or if the specified type is an ancestor of
     *  the object genus in a type hierarchy.
     *
     *  @param  genusType genus type to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> genusType
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchParentGenusType(org.osid.type.Type genusType, boolean match) {
        getAssembler().addTypeTerm(getGenusTypeColumn(), genusType, match);
        return;
    }

    
    /**
     *  Clears all genus type terms. 
     */

    @OSID @Override
    public void clearParentGenusTypeTerms() {
        getAssembler().clearTerms(getParentGenusTypeColumn());
        return;
    }


    /**
     *  Gets the parent genus type query terms.
     *
     *  @return the genus type terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getParentGenusTypeTerms() {
        return (getAssembler().getTypeTerms(getParentGenusTypeColumn()));
    }


    /**
     *  Gets the column name for the parent genus type field.
     *
     *  @return the column name
     */

    protected String getParentGenusTypeColumn() {
        return ("parent_genus_type");
    }


    /**
     *  Adds a subject Id to match. Multiple subject Id matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  subjectId subject Id to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchSubjectId(org.osid.id.Id subjectId, boolean match) {
        getAssembler().addIdTerm(getSubjectIdColumn(), subjectId, match);
        return;
    }


    /**
     *  Clears all subject Id terms. 
     */

    @OSID @Override
    public void clearSubjectIdTerms() {
        getAssembler().clearTerms(getSubjectIdColumn());
        return;
    }


    /**
     *  Gets the subject Id query terms.
     *
     *  @return the subject Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubjectIdTerms() {
        return (getAssembler().getIdTerms(getSubjectIdColumn()));
    }


    /**
     *  Gets the column name for the subject Id field.
     *
     *  @return the column name
     */

    protected String getSubjectIdColumn() {
        return ("subject_id");
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> supportsSubjectQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsSubjectQuery() is false");
    }


    /**
     *  Matches any object with a subject Id.
     *
     *  @param match <code> true </code> to match any subject Id,
     *          <code> false </code> to match objects with no subject
     *          Id
     */

    @OSID @Override
    public void matchAnySubject(boolean match) {
        getAssembler().addIdWildcardTerm(getSubjectColumn(), match);
        return;
    }


    /**
     *  Clears all subject terms. 
     */

    @OSID @Override
    public void clearSubjectTerms() {
        getAssembler().clearTerms(getSubjectColumn());
        return;
    }


    /**
     *  Gets the subject query terms. 
     *
     *  @return the subject query terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the column name for the subject field.
     *
     *  @return the column name
     */

    protected String getSubjectColumn() {
        return ("subject");
    }

    
    /**
     *  Tests if a <code> RelevancyQuery </code> is available to provide 
     *  queries about the relationships to <code> Subjects. </code> 
     *
     *  @return <code> true </code> if a relevancy entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubjectRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject relevancy. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the relevancy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubjectRelevancyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getSubjectRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsSubjectRelevancy() is false");
    }


    /**
     *  Clears all subject relevancy terms. 
     */

    @OSID @Override
    public void clearSubjectRelevancyTerms() {
        return;
    }


    /**
     *  Gets the subject relevancy query terms.
     *
     *  @return the subject relevancy query terms
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getSubjectRelevancyTerms() {
        return (new org.osid.ontology.RelevancyQueryInspector[0]);
    }


    /**
     *  Adds a state Id to match. Multiple state Id matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  stateId state Id to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchStateId(org.osid.id.Id stateId, boolean match) {
        getAssembler().addIdTerm(getStateIdColumn(), stateId, match);
        return;
    }


    /**
     *  Clears all state Id terms. 
     */

    @OSID @Override
    public void clearStateIdTerms() {
        getAssembler().clearTerms(getStateIdColumn());
        return;
    }


    /**
     *  Gets the state Id query terms.
     *
     *  @return the state Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStateIdTerms() {
        return (getAssembler().getIdTerms(getStateIdColumn()));
    }


    /**
     *  Gets the column name for the state Id field.
     *
     *  @return the column name
     */

    protected String getStateIdColumn() {
        return ("state_id");
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> supportsStateQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getStateQuery() {
        throw new org.osid.UnimplementedException("supportsStateQuery() is false");
    }

    
    /**
     *  Gets the state query terms. 
     *
     *  @return the state query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getStateTerms() {
        return (new org.osid.process.StateQueryInspector[0]);
    }


    /**
     *  Matches any object with a state Id.
     *
     *  @param match <code> true </code> to match any state Id,
     *          <code> false </code> to match objects with no state
     *          Id
     */

    @OSID @Override
    public void matchAnyState(boolean match) {
        getAssembler().addIdWildcardTerm(getStateColumn(), match);
        return;
    }


    /**
     *  Clears all state Id terms. 
     */

    @OSID @Override
    public void clearStateTerms() {
        getAssembler().clearTerms(getStateColumn());
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  state Id.
     *
     *  @param processId the Id of a process
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code>process</code> or
     *          <code>style</code> is <code> null </code>
     */

    @OSID @Override
    public void orderByState(org.osid.id.Id processId, org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStateIdColumn(), processId, style);
        return;
    }


    /**
     *  Gets the column name for the state field.
     *
     *  @return the column name
     */

    protected String getStateColumn() {
        return ("state");
    }


    /**
     *  Adds a comment Id to match. Multiple comment Id matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  commentId comment Id to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commentId
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchCommentId(org.osid.id.Id commentId, boolean match) {
        getAssembler().addIdTerm(getCommentIdColumn(), commentId, match);
        return;
    }


    /**
     *  Clears all comment Id terms. 
     */

    @OSID @Override
    public void clearCommentIdTerms() {
        getAssembler().clearTerms(getCommentIdColumn());
        return;
    }


    /**
     *  Gets the comment Id query terms.
     *
     *  @return the comment Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentIdTerms() {
        return (getAssembler().getIdTerms(getCommentIdColumn()));
    }


    /**
     *  Gets the column name for the comment Id field.
     *
     *  @return the column name
     */

    protected String getCommentIdColumn() {
        return ("comment_id");
    }


    /**
     *  Tests if a <code> CommentQuery </code> is available. 
     *
     *  @return <code> true </code> if a comment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a comment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the comment query 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuery getCommentQuery() {
        throw new org.osid.UnimplementedException("supportsCommentQuery() is false");
    }

    
    /**
     *  Gets the comment query terms. 
     *
     *  @return the comment query terms 
     */

    @OSID @Override
    public org.osid.commenting.CommentQueryInspector[] getCommentTerms() {
        return (new org.osid.commenting.CommentQueryInspector[0]);
    }


    /**
     *  Matches any object with a comment Id.
     *
     *  @param match <code> true </code> to match any comment Id,
     *          <code> false </code> to match objects with no comment
     *          Id
     */

    @OSID @Override
    public void matchAnyComment(boolean match) {
        getAssembler().addIdWildcardTerm(getCommentColumn(), match);
        return;
    }


    /**
     *  Clears all comment Id terms. 
     */

    @OSID @Override
    public void clearCommentTerms() {
        getAssembler().clearTerms(getCommentColumn());
        return;
    }

    
    /**
     *  Gets the column name for the comment field.
     *
     *  @return the column name
     */

    protected String getCommentColumn() {
        return ("comment");
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  cumulative rating in a comment book.
     *
     *  @param bookId the Id of a comment book
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code>bookId</code> or
     *          <code>style</code> is <code> null </code>
     */

    @OSID @Override
    public void orderByCumulativeRating(org.osid.id.Id bookId, org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCumulativeRatingColumn(), bookId, style);
        return;
    }


    /**
     *  Gets the column name for the comment field.
     *
     *  @return the column name
     */

    protected String getCumulativeRatingColumn() {
        return ("cumulative_rating");
    }


    /**
     *  Adds a journal entry Id to match. Multiple journal entry Id
     *  matches can be added to perform a boolean <code> OR </code>
     *  among them.
     *
     *  @param  journalEntryId journal entry Id to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalEntryId
     *          </code> or is <code> null </code>
     */

    @OSID @Override
    public void matchJournalEntryId(org.osid.id.Id journalEntryId, boolean match) {
        getAssembler().addIdTerm(getJournalEntryIdColumn(), journalEntryId, match);
        return;
    }


    /**
     *  Clears all journal entry Id terms. 
     */

    @OSID @Override
    public void clearJournalEntryIdTerms() {
        getAssembler().clearTerms(getJournalEntryIdColumn());
        return;
    }


    /**
     *  Gets the journal entry Id query terms.
     *
     *  @return the journal entry Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJournalEntryIdTerms() {
        return (getAssembler().getIdTerms(getJournalEntryIdColumn()));
    }


    /**
     *  Gets the column name for the journal entry Id field.
     *
     *  @return the column name
     */

    protected String getJournalEntryIdColumn() {
        return ("journal entry id");
    }


    /**
     *  Tests if a <code> JournalEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal entry. Multiple retrievals
     *  produce a nested <code> OR </code> term.
     *
     *  @return the journal entry query 
     *  @throws org.osid.UnimplementedException <code>
     *          supportsJournalEntryQuery() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getJournalEntryQuery() {
        throw new org.osid.UnimplementedException("supportsJournalEntryQuery() is false");
    }

    
    /**
     *  Gets the journal entry query terms. 
     *
     *  @return the journal entry query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQueryInspector[] getJournalEntryTerms() {
        return (new org.osid.journaling.JournalEntryQueryInspector[0]);
    }


    /**
     *  Matches any object with a journal entry Id.
     *
     *  @param match <code> true </code> to match any journal entry Id,
     *          <code> false </code> to match objects with no journal entry
     *          Id
     */

    @OSID @Override
    public void matchAnyJournalEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getJournalEntryColumn(), match);
        return;
    }


    /**
     *  Clears all journal entry Id terms. 
     */

    @OSID @Override
    public void clearJournalEntryTerms() {
        getAssembler().clearTerms(getJournalEntryColumn());
        return;
    }


    /**
     *  Gets the column name for the journal entry field.
     *
     *  @return the column name
     */

    protected String getJournalEntryColumn() {
        return ("journal entry");
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  create time.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code>style</code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByCreateTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreateTimeColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the create time field.
     *
     *  @return the column name
     */

    protected String getCreateTimeColumn() {
        return ("create_time");
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  last modified time.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code>style</code> is
     *          <code> null </code>
     */

    @OSID @Override
    public void orderByLastModifiedTime(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getLastModifiedTimeColumn(), style);
        return;
    }


    /**
     *  Gets the column name for the last modified time field.
     *
     *  @return the column name
     */

    protected String getLastModifiedTimeColumn() {
        return ("last_modified_time");
    }


    /**
     *  Tests if a <code> StatisticQuery </code> is available. 
     *
     *  @return <code> true </code> if a statistic query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticQuery() {
        return (false);
    }


    /**
     *  Gets the query for a statistic. Multiple retrievals
     *  produce a nested <code> OR </code> term.
     *
     *  @return the statistic query 
     *  @throws org.osid.UnimplementedException <code>
     *          supportsStatisticQuery() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.metering.StatisticQuery getStatisticQuery() {
        throw new org.osid.UnimplementedException("supportsStatisticQuery() is false");
    }

    
    /**
     *  Gets the statistic query terms. 
     *
     *  @return the statistic query terms 
     */

    @OSID @Override
    public org.osid.metering.StatisticQueryInspector[] getStatisticTerms() {
        return (new org.osid.metering.StatisticQueryInspector[0]);
    }


    /**
     *  Matches any object with a statistic Id.
     *
     *  @param match <code> true </code> to match any statistic Id,
     *          <code> false </code> to match objects with no statistic
     *          Id
     */

    @OSID @Override
    public void matchAnyStatistic(boolean match) {
        getAssembler().addIdWildcardTerm(getStatisticColumn(), match);
        return;
    }


    /**
     *  Clears all statistic Id terms. 
     */

    @OSID @Override
    public void clearStatisticTerms() {
        getAssembler().clearTerms(getStatisticColumn());
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  statistics for a meter.
     *
     *  @param meterId the Id of a meter
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code>meterId</code> or
     *          <code>style</code> is <code> null </code>
     */

    @OSID @Override
    public void orderByStatistic(org.osid.id.Id meterId, org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreditIdColumn(), meterId, style);
        return;
    }


    /**
     *  Gets the column name for the statistic field.
     *
     *  @return the column name
     */

    protected String getStatisticColumn() {
        return ("statistic");
    }


    /**
     *  Adds a credit Id to match. Multiple credit Id matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  creditId credit Id to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> creditId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCreditId(org.osid.id.Id creditId, boolean match) {
        getAssembler().addIdTerm(getCreditIdColumn(), creditId, match);
        return;
    }


    /**
     *  Clears all credit Id terms. 
     */

    @OSID @Override
    public void clearCreditIdTerms() {
        getAssembler().clearTerms(getCreditIdColumn());
        return;
    }


    /**
     *  Gets the credit Id query terms.
     *
     *  @return the credit Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCreditIdTerms() {
        return (getAssembler().getIdTerms(getCreditIdColumn()));
    }


    /**
     *  Gets the column name for the credit Id field.
     *
     *  @return the column name
     */

    protected String getCreditIdColumn() {
        return ("credit_id");
    }


    /**
     *  Tests if a <code> CreditQuery </code> is available. 
     *
     *  @return <code> true </code> if a credit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditQuery() {
        return (false);
    }


    /**
     *  Gets the query for a credit. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the credit query 
     *  @throws org.osid.UnimplementedException <code> supportsCreditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQuery getCreditQuery() {
        throw new org.osid.UnimplementedException("supportsCreditQuery() is false");
    }

    
    /**
     *  Gets the credit query terms. 
     *
     *  @return the credit query terms 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditQueryInspector[] getCreditTerms() {
        return (new org.osid.acknowledgement.CreditQueryInspector[0]);
    }


    /**
     *  Matches any object with a credit Id.
     *
     *  @param match <code> true </code> to match any credit Id,
     *          <code> false </code> to match objects with no credit
     *          Id
     */

    @OSID @Override
    public void matchAnyCredit(boolean match) {
        getAssembler().addIdWildcardTerm(getCreditColumn(), match);
        return;
    }


    /**
     *  Clears all credit Id terms. 
     */

    @OSID @Override
    public void clearCreditTerms() {
        getAssembler().clearTerms(getCreditColumn());
        return;
    }


    /**
     *  Gets the column name for the credit field.
     *
     *  @return the column name
     */

    protected String getCreditColumn() {
        return ("credit");
    }


    /**
     *  Adds a relationship Id to match. Multiple relationship Id
     *  matches can be added to perform a boolean <code> OR </code>
     *  among them.
     *
     *  @param  relationshipId relationship Id to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relationshipId
     *          </code> is <code> null </code>
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchRelationshipId(org.osid.id.Id relationshipId, boolean match) {
        getAssembler().addIdTerm(getRelationshipIdColumn(), relationshipId, match);
        return;
    }


    /**
     *  Clears all relationship Id terms. 
     */

    @OSID @Override
    public void clearRelationshipIdTerms() {
        getAssembler().clearTerms(getRelationshipIdColumn());
        return;
    }


    /**
     *  Gets the relationship Id query terms.
     *
     *  @return the relationship Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelationshipIdTerms() {
        return (getAssembler().getIdTerms(getRelationshipIdColumn()));
    }


    /**
     *  Gets the column name for the relationship Id field.
     *
     *  @return the column name
     */

    protected String getRelationshipIdColumn() {
        return ("relationship_id");
    }


    /**
     *  Tests if a <code> RelationshipQuery </code> is available. 
     *
     *  @return <code> true </code> if a relationship query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relationship. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the relationship query 
     *  @throws org.osid.UnimplementedException <code> supportsRelationshipQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQuery getRelationshipQuery() {
        throw new org.osid.UnimplementedException("supportsRelationshipQuery() is false");
    }

    
    /**
     *  Gets the relationship query terms. 
     *
     *  @return the relationship query terms 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipQueryInspector[] getRelationshipTerms() {
        return (new org.osid.relationship.RelationshipQueryInspector[0]);
    }


    /**
     *  Matches any object with a relationship Id.
     *
     *  @param match <code> true </code> to match any relationship Id,
     *          <code> false </code> to match objects with no relationship
     *          Id
     */

    @OSID @Override
    public void matchAnyRelationship(boolean match) {
        getAssembler().addIdWildcardTerm(getRelationshipColumn(), match);
        return;
    }


    /**
     *  Clears all relationship Id terms. 
     */

    @OSID @Override
    public void clearRelationshipTerms() {
        getAssembler().clearTerms(getRelationshipColumn());
        return;
    }


    /**
     *  Gets the column name for the relationship field.
     *
     *  @return the column name
     */

    protected String getRelationshipColumn() {
        return ("relationship");
    }


    /**
     *  Matches an object that has a relationship to the given peer <code> Id. 
     *  </code> 
     *
     *  @param  peerId a relationship peer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> peerId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRelationshipPeerId(org.osid.id.Id peerId, boolean match) {
        getAssembler().clearTerms(getRelationshipPeerColumn());
        return;
    }


    /**
     *  Clears all relationship <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRelationshipPeerIdTerms() {
        getAssembler().clearTerms(getRelationshipPeerColumn());
        return;
    }


    /**
     *  Gets the relationship peer <code> Id </code> query terms. 
     *
     *  @return the relationship peer <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelationshipPeerIdTerms() {
        return (getAssembler().getIdTerms(getRelationshipIdColumn()));
    }


    /**
     *  Gets the column name for the relationship peer field.
     *
     *  @return the column name
     */

    protected String getRelationshipPeerColumn() {
        return ("relationship_peer");
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.query.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code>true</code> if <code>recordType</code> is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.query.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.query.addRecordType(recordType);
        return;
    }


    /**
     *  Sets a <code> Type </code> for querying objects having records 
     *  implementing a given record type. 
     *
     *  @param  recordType a record type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecordType(org.osid.type.Type recordType, boolean match) {
        this.query.matchRecordType(recordType, match);
        return;
    }


    /**
     *  Matches an object that has any record. 
     *
     *  @param  match <code> true </code> to match any record, <code> false 
     *          </code> to match objects with no records 
     */

    @OSID @Override
    public void matchAnyRecord(boolean match) {
        this.query.matchAnyRecord(match);
        return;
    }


    /**
     *  Clears all record <code> Type </code> terms. 
     */

    @OSID @Override
    public void clearRecordTerms() {
        this.query.clearRecordTerms();
        return;
    }

    
    /**
     *  Gets the record type query terms. 
     *
     *  @return the record type terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getRecordTypeTerms() {
        return (this.query.getRecordTypeTerms());
    }        


    protected class AssemblyOsidBrowsableQuery
        extends AbstractAssemblyOsidBrowsableQuery
        implements org.osid.OsidBrowsableQuery,
                   org.osid.OsidBrowsableQueryInspector,
                   org.osid.OsidBrowsableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidBrowsableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidBrowsableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */
        
        @Override
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }
    }
}
