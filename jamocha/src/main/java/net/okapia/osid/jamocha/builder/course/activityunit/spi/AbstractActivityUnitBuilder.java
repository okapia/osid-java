//
// AbstractActivityUnit.java
//
//     Defines an ActivityUnit builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.activityunit.spi;


/**
 *  Defines an <code>ActivityUnit</code> builder.
 */

public abstract class AbstractActivityUnitBuilder<T extends AbstractActivityUnitBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOperableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.activityunit.ActivityUnitMiter activityUnit;


    /**
     *  Constructs a new <code>AbstractActivityUnitBuilder</code>.
     *
     *  @param activityUnit the activity unit to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractActivityUnitBuilder(net.okapia.osid.jamocha.builder.course.activityunit.ActivityUnitMiter activityUnit) {
        super(activityUnit);
        this.activityUnit = activityUnit;
        return;
    }


    /**
     *  Builds the activity unit.
     *
     *  @return the new activity unit
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.ActivityUnit build() {
        (new net.okapia.osid.jamocha.builder.validator.course.activityunit.ActivityUnitValidator(getValidations())).validate(this.activityUnit);
        return (new net.okapia.osid.jamocha.builder.course.activityunit.ImmutableActivityUnit(this.activityUnit));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the activity unit miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.activityunit.ActivityUnitMiter getMiter() {
        return (this.activityUnit);
    }

    
    /**
     *  Sets the course.
     *
     *  @param course the course 
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    public T course(org.osid.course.Course course) {
        getMiter().setCourse(course);
        return (self());
    }
    

    /**
     *  Sets the total target effort.
     *
     *  @param effort a total target effort
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public T totalTargetEffort(org.osid.calendaring.Duration effort) {
        getMiter().setTotalTargetEffort(effort);
        return (self());
    }


    /**
     *  Sets the total target contact time.
     *
     *  @param contactTime a total target contact time
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>contactTime</code> is <code>null</code>
     */

    public T totalTargetContactTime(org.osid.calendaring.Duration contactTime) {
        getMiter().setTotalTargetContactTime(contactTime);
        return (self());
    }


    /**
     *  Sets this is a contact activity. 
     *
     *  @param  contact <code> true </code> if this is a contact
     *          activity, <code> false </code> if an independent
     *          activity
     */

    public T contact(boolean contact) {
        getMiter().setContact(contact);
        return (self());
    }


    /**
     *  Sets the total target individual effort.
     *
     *  @param effort a total target individual effort
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>effort</code> is <code>null</code>
     */

    public T totalTargetIndividualEffort(org.osid.calendaring.Duration effort) {
        getMiter().setTotalTargetIndividualEffort(effort);
        return (self());
    }


    /**
     *  Sets the weekly effort.
     *
     *  @param effort a weekly effort
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public T weeklyEffort(org.osid.calendaring.Duration effort) {
        getMiter().setWeeklyEffort(effort);
        return (self());
    }


    /**
     *  Sets the weekly contact time.
     *
     *  @param contactTime a weekly contact time
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>contactTime</code> is <code>null</code>
     */

    public T weeklyContactTime(org.osid.calendaring.Duration contactTime) {
        getMiter().setWeeklyContactTime(contactTime);
        return (self());
    }


    /**
     *  Sets the weekly individual effort.
     *
     *  @param effort a weekly individual effort
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public T weeklyIndividualEffort(org.osid.calendaring.Duration effort) {
        getMiter().setWeeklyIndividualEffort(effort);
        return (self());
    }


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public T learningObjective(org.osid.learning.Objective objective) {
        getMiter().addLearningObjective(objective);
        return (self());
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    public T learningObjectives(java.util.Collection<org.osid.learning.Objective> objectives) {
        getMiter().setLearningObjectives(objectives);
        return (self());
    }


    /**
     *  Adds an ActivityUnit record.
     *
     *  @param record an activity unit record
     *  @param recordType the type of activity unit record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.records.ActivityUnitRecord record, org.osid.type.Type recordType) {
        getMiter().addActivityUnitRecord(record, recordType);
        return (self());
    }
}       


