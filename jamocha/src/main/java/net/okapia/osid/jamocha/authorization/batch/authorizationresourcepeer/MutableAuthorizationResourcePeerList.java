//
// MutableAuthorizationResourcePeerList.java
//
//     Implements an AuthorizationResourcePeerList. This list allows AuthorizationResourcePeers to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.batch.authorizationresourcepeer;


/**
 *  <p>Implements an AuthorizationResourcePeerList. This list allows AuthorizationResourcePeers to be
 *  added after this authorizationResourcePeer has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this authorizationResourcePeer must
 *  invoke <code>eol()</code> when there are no more authorizationResourcePeers to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>AuthorizationResourcePeerList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more authorizationResourcePeers are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more authorizationResourcePeers to be added.</p>
 */

public final class MutableAuthorizationResourcePeerList
    extends net.okapia.osid.jamocha.authorization.batch.authorizationresourcepeer.spi.AbstractMutableAuthorizationResourcePeerList
    implements org.osid.authorization.batch.AuthorizationResourcePeerList {


    /**
     *  Creates a new empty <code>MutableAuthorizationResourcePeerList</code>.
     */

    public MutableAuthorizationResourcePeerList() {
        super();
    }


    /**
     *  Creates a new <code>MutableAuthorizationResourcePeerList</code>.
     *
     *  @param authorizationResourcePeer an <code>AuthorizationResourcePeer</code>
     *  @throws org.osid.NullArgumentException <code>authorizationResourcePeer</code>
     *          is <code>null</code>
     */

    public MutableAuthorizationResourcePeerList(org.osid.authorization.batch.AuthorizationResourcePeer authorizationResourcePeer) {
        super(authorizationResourcePeer);
        return;
    }


    /**
     *  Creates a new <code>MutableAuthorizationResourcePeerList</code>.
     *
     *  @param array an array of authorizationresourcepeers
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableAuthorizationResourcePeerList(org.osid.authorization.batch.AuthorizationResourcePeer[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableAuthorizationResourcePeerList</code>.
     *
     *  @param collection a java.util.Collection of authorizationresourcepeers
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableAuthorizationResourcePeerList(java.util.Collection<org.osid.authorization.batch.AuthorizationResourcePeer> collection) {
        super(collection);
        return;
    }
}
