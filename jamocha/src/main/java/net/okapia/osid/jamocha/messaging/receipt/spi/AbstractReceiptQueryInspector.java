//
// AbstractReceiptQueryInspector.java
//
//     A template for making a ReceiptQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.receipt.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for receipts.
 */

public abstract class AbstractReceiptQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.messaging.ReceiptQueryInspector {

    private final java.util.Collection<org.osid.messaging.records.ReceiptQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the message <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMessageIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the message query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MessageQueryInspector[] getMessageTerms() {
        return (new org.osid.messaging.MessageQueryInspector[0]);
    }


    /**
     *  Gets the received time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getReceivedTimeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the receiving agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReceivingAgentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the receiving agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getReceivingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the recipient <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipientIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the recipient query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getRecipientTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the mailbox <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMailboxIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the mailbox query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.messaging.MailboxQueryInspector[] getMailboxTerms() {
        return (new org.osid.messaging.MailboxQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given receipt query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a receipt implementing the requested record.
     *
     *  @param receiptRecordType a receipt record type
     *  @return the receipt query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>receiptRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(receiptRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.ReceiptQueryInspectorRecord getReceiptQueryInspectorRecord(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.messaging.records.ReceiptQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(receiptRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(receiptRecordType + " is not supported");
    }


    /**
     *  Adds a record to this receipt query. 
     *
     *  @param receiptQueryInspectorRecord receipt query inspector
     *         record
     *  @param receiptRecordType receipt record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addReceiptQueryInspectorRecord(org.osid.messaging.records.ReceiptQueryInspectorRecord receiptQueryInspectorRecord, 
                                                   org.osid.type.Type receiptRecordType) {

        addRecordType(receiptRecordType);
        nullarg(receiptRecordType, "receipt record type");
        this.records.add(receiptQueryInspectorRecord);        
        return;
    }
}
