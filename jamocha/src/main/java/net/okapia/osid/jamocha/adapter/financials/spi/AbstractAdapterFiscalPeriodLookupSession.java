//
// AbstractAdapterFiscalPeriodLookupSession.java
//
//    A FiscalPeriod lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.financials.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A FiscalPeriod lookup session adapter.
 */

public abstract class AbstractAdapterFiscalPeriodLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.financials.FiscalPeriodLookupSession {

    private final org.osid.financials.FiscalPeriodLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterFiscalPeriodLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterFiscalPeriodLookupSession(org.osid.financials.FiscalPeriodLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Business/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Business Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the {@code Business} associated with this session.
     *
     *  @return the {@code Business} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform {@code FiscalPeriod} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupFiscalPeriods() {
        return (this.session.canLookupFiscalPeriods());
    }


    /**
     *  A complete view of the {@code FiscalPeriod} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFiscalPeriodView() {
        this.session.useComparativeFiscalPeriodView();
        return;
    }


    /**
     *  A complete view of the {@code FiscalPeriod} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFiscalPeriodView() {
        this.session.usePlenaryFiscalPeriodView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include fiscal periods in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the {@code FiscalPeriod} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code FiscalPeriod} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code FiscalPeriod} and
     *  retained for compatibility.
     *
     *  @param fiscalPeriodId {@code Id} of the {@code FiscalPeriod}
     *  @return the fiscal period
     *  @throws org.osid.NotFoundException {@code fiscalPeriodId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code fiscalPeriodId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod(org.osid.id.Id fiscalPeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFiscalPeriod(fiscalPeriodId));
    }


    /**
     *  Gets a {@code FiscalPeriodList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  fiscalPeriods specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code FiscalPeriods} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  fiscalPeriodIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code FiscalPeriod} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code fiscalPeriodIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByIds(org.osid.id.IdList fiscalPeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFiscalPeriodsByIds(fiscalPeriodIds));
    }


    /**
     *  Gets a {@code FiscalPeriodList} corresponding to the given
     *  fiscal period genus {@code Type} which does not include
     *  fiscal periods of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fiscalPeriodGenusType a fiscalPeriod genus type 
     *  @return the returned {@code FiscalPeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code fiscalPeriodGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByGenusType(org.osid.type.Type fiscalPeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFiscalPeriodsByGenusType(fiscalPeriodGenusType));
    }


    /**
     *  Gets a {@code FiscalPeriodList} corresponding to the given
     *  fiscal period genus {@code Type} and include any additional
     *  fiscal periods with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fiscalPeriodGenusType a fiscalPeriod genus type 
     *  @return the returned {@code FiscalPeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code fiscalPeriodGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByParentGenusType(org.osid.type.Type fiscalPeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFiscalPeriodsByParentGenusType(fiscalPeriodGenusType));
    }


    /**
     *  Gets a {@code FiscalPeriodList} containing the given
     *  fiscal period record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  fiscalPeriodRecordType a fiscalPeriod record type 
     *  @return the returned {@code FiscalPeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code fiscalPeriodRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByRecordType(org.osid.type.Type fiscalPeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFiscalPeriodsByRecordType(fiscalPeriodRecordType));
    }


    /**
     *  Gets a <code> FiscalPeriodList </code> containing the given
     *  date. In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned
     *  list may contain only those fiscal periods that are accessible
     *  through this session.
     *
     *  @param  date a date 
     *  @return the returned <code> FiscalPeriod </code> list 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriodsByDate(org.osid.calendaring.DateTime date)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFiscalPeriodsByDate(date));
    }


    /**
     *  Gets all {@code FiscalPeriods}. 
     *
     *  In plenary mode, the returned list contains all known
     *  fiscal periods or an error results. Otherwise, the returned list
     *  may contain only those fiscal periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code FiscalPeriods} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodList getFiscalPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getFiscalPeriods());
    }
}
