//
// MutableDictionaryBatchFormList.java
//
//     Implements a DictionaryBatchFormList. This list allows DictionaryBatchForms to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.dictionary.batch.dictionarybatchform;


/**
 *  <p>Implements a DictionaryBatchFormList. This list allows DictionaryBatchForms to be
 *  added after this dictionaryBatchForm has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this dictionaryBatchForm must
 *  invoke <code>eol()</code> when there are no more dictionaryBatchForms to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>DictionaryBatchFormList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more dictionaryBatchForms are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more dictionaryBatchForms to be added.</p>
 */

public final class MutableDictionaryBatchFormList
    extends net.okapia.osid.jamocha.dictionary.batch.dictionarybatchform.spi.AbstractMutableDictionaryBatchFormList
    implements org.osid.dictionary.batch.DictionaryBatchFormList {


    /**
     *  Creates a new empty <code>MutableDictionaryBatchFormList</code>.
     */

    public MutableDictionaryBatchFormList() {
        super();
    }


    /**
     *  Creates a new <code>MutableDictionaryBatchFormList</code>.
     *
     *  @param dictionaryBatchForm a <code>DictionaryBatchForm</code>
     *  @throws org.osid.NullArgumentException <code>dictionaryBatchForm</code>
     *          is <code>null</code>
     */

    public MutableDictionaryBatchFormList(org.osid.dictionary.batch.DictionaryBatchForm dictionaryBatchForm) {
        super(dictionaryBatchForm);
        return;
    }


    /**
     *  Creates a new <code>MutableDictionaryBatchFormList</code>.
     *
     *  @param array an array of dictionarybatchforms
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableDictionaryBatchFormList(org.osid.dictionary.batch.DictionaryBatchForm[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableDictionaryBatchFormList</code>.
     *
     *  @param collection a java.util.Collection of dictionarybatchforms
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableDictionaryBatchFormList(java.util.Collection<org.osid.dictionary.batch.DictionaryBatchForm> collection) {
        super(collection);
        return;
    }
}
