//
// AbstractCoursePlanManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCoursePlanManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.course.plan.CoursePlanManager,
               org.osid.course.plan.CoursePlanProxyManager {

    private final Types syllabusRecordTypes                = new TypeRefSet();
    private final Types syllabusSearchRecordTypes          = new TypeRefSet();

    private final Types planRecordTypes                    = new TypeRefSet();
    private final Types planSearchRecordTypes              = new TypeRefSet();

    private final Types lessonRecordTypes                  = new TypeRefSet();
    private final Types lessonSearchRecordTypes            = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCoursePlanManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCoursePlanManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any lesson federation is exposed. Federation is exposed when 
     *  a specific lesson may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  lessonsappears as a single lesson. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a plan lookup service. 
     *
     *  @return <code> true </code> if plan lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanLookup() {
        return (false);
    }


    /**
     *  Tests if querying plans is available. 
     *
     *  @return <code> true </code> if plan query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanQuery() {
        return (false);
    }


    /**
     *  Tests if searching for plans is available. 
     *
     *  @return <code> true </code> if plan search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanSearch() {
        return (false);
    }


    /**
     *  Tests if searching for plans is available. 
     *
     *  @return <code> true </code> if plan search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanAdmin() {
        return (false);
    }


    /**
     *  Tests if plan notification is available. 
     *
     *  @return <code> true </code> if plan notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanNotification() {
        return (false);
    }


    /**
     *  Tests if a plan to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if plan course catalog lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a plan to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if plan course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a plan smart course catalog session is available. 
     *
     *  @return <code> true </code> if plan smart course catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPlanSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests for the availability of a lesson lookup service. 
     *
     *  @return <code> true </code> if lesson lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonLookup() {
        return (false);
    }


    /**
     *  Tests if querying lessonsis available. 
     *
     *  @return <code> true </code> if lesson query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonQuery() {
        return (false);
    }


    /**
     *  Tests if searching for lessons is available. 
     *
     *  @return <code> true </code> if lesson search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a lesson administrative service for 
     *  creating and deleting lessons. 
     *
     *  @return <code> true </code> if lesson administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a lesson notification service. 
     *
     *  @return <code> true </code> if lesson notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonNotification() {
        return (false);
    }


    /**
     *  Tests if a lesson to course catalog lookup session is available. 
     *
     *  @return <code> true </code> if lesson course catalog lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a lesson to course catalog assignment session is available. 
     *
     *  @return <code> true </code> if lesson course catalog assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a lesson smart course catalog session is available. 
     *
     *  @return <code> true </code> if lesson smart course catalog is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking at lesson conflicts available. 
     *
     *  @return <code> true </code> if lesson conflict is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonConflict() {
        return (false);
    }


    /**
     *  Tests if anchoring lessons is available. 
     *
     *  @return <code> true </code> if lesson anchoring is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLessonAnchoring() {
        return (false);
    }


    /**
     *  Gets the supported <code> Syllabus </code> record types. 
     *
     *  @return a list containing the supported syllabus record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSyllabusRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.syllabusRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Syllabus </code> record type is supported. 
     *
     *  @param  syllabusRecordType a <code> Type </code> indicating a <code> 
     *          Syllabus </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> syllabusRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSyllabusRecordType(org.osid.type.Type syllabusRecordType) {
        return (this.syllabusRecordTypes.contains(syllabusRecordType));
    }


    /**
     *  Adds support for a syllabus record type.
     *
     *  @param syllabusRecordType a syllabus record type
     *  @throws org.osid.NullArgumentException
     *  <code>syllabusRecordType</code> is <code>null</code>
     */

    protected void addSyllabusRecordType(org.osid.type.Type syllabusRecordType) {
        this.syllabusRecordTypes.add(syllabusRecordType);
        return;
    }


    /**
     *  Removes support for a syllabus record type.
     *
     *  @param syllabusRecordType a syllabus record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>syllabusRecordType</code> is <code>null</code>
     */

    protected void removeSyllabusRecordType(org.osid.type.Type syllabusRecordType) {
        this.syllabusRecordTypes.remove(syllabusRecordType);
        return;
    }


    /**
     *  Gets the supported syllabus search record types. 
     *
     *  @return a list containing the supported syllabus search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSyllabusSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.syllabusSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given syllabus search record type is supported. 
     *
     *  @param  syllabusSearchRecordType a <code> Type </code> indicating a 
     *          syllabus record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> syllabusSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSyllabusSearchRecordType(org.osid.type.Type syllabusSearchRecordType) {
        return (this.syllabusSearchRecordTypes.contains(syllabusSearchRecordType));
    }


    /**
     *  Adds support for a syllabus search record type.
     *
     *  @param syllabusSearchRecordType a syllabus search record type
     *  @throws org.osid.NullArgumentException
     *  <code>syllabusSearchRecordType</code> is <code>null</code>
     */

    protected void addSyllabusSearchRecordType(org.osid.type.Type syllabusSearchRecordType) {
        this.syllabusSearchRecordTypes.add(syllabusSearchRecordType);
        return;
    }


    /**
     *  Removes support for a syllabus search record type.
     *
     *  @param syllabusSearchRecordType a syllabus search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>syllabusSearchRecordType</code> is <code>null</code>
     */

    protected void removeSyllabusSearchRecordType(org.osid.type.Type syllabusSearchRecordType) {
        this.syllabusSearchRecordTypes.remove(syllabusSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Plan </code> record types. 
     *
     *  @return a list containing the supported plan record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPlanRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.planRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Plan </code> record type is supported. 
     *
     *  @param  planRecordType a <code> Type </code> indicating a <code> Plan 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> planRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPlanRecordType(org.osid.type.Type planRecordType) {
        return (this.planRecordTypes.contains(planRecordType));
    }


    /**
     *  Adds support for a plan record type.
     *
     *  @param planRecordType a plan record type
     *  @throws org.osid.NullArgumentException
     *  <code>planRecordType</code> is <code>null</code>
     */

    protected void addPlanRecordType(org.osid.type.Type planRecordType) {
        this.planRecordTypes.add(planRecordType);
        return;
    }


    /**
     *  Removes support for a plan record type.
     *
     *  @param planRecordType a plan record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>planRecordType</code> is <code>null</code>
     */

    protected void removePlanRecordType(org.osid.type.Type planRecordType) {
        this.planRecordTypes.remove(planRecordType);
        return;
    }


    /**
     *  Gets the supported plan search record types. 
     *
     *  @return a list containing the supported plan search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPlanSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.planSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given plan search record type is supported. 
     *
     *  @param  planSearchRecordType a <code> Type </code> indicating a plan 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> planSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPlanSearchRecordType(org.osid.type.Type planSearchRecordType) {
        return (this.planSearchRecordTypes.contains(planSearchRecordType));
    }


    /**
     *  Adds support for a plan search record type.
     *
     *  @param planSearchRecordType a plan search record type
     *  @throws org.osid.NullArgumentException
     *  <code>planSearchRecordType</code> is <code>null</code>
     */

    protected void addPlanSearchRecordType(org.osid.type.Type planSearchRecordType) {
        this.planSearchRecordTypes.add(planSearchRecordType);
        return;
    }


    /**
     *  Removes support for a plan search record type.
     *
     *  @param planSearchRecordType a plan search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>planSearchRecordType</code> is <code>null</code>
     */

    protected void removePlanSearchRecordType(org.osid.type.Type planSearchRecordType) {
        this.planSearchRecordTypes.remove(planSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Lesson </code> record types. 
     *
     *  @return a list containing the supported lesson record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLessonRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.lessonRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Lesson </code> record type is supported. 
     *
     *  @param  lessonRecordType a <code> Type </code> indicating a <code> 
     *          Lesson </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> lessonRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLessonRecordType(org.osid.type.Type lessonRecordType) {
        return (this.lessonRecordTypes.contains(lessonRecordType));
    }


    /**
     *  Adds support for a lesson record type.
     *
     *  @param lessonRecordType a lesson record type
     *  @throws org.osid.NullArgumentException
     *  <code>lessonRecordType</code> is <code>null</code>
     */

    protected void addLessonRecordType(org.osid.type.Type lessonRecordType) {
        this.lessonRecordTypes.add(lessonRecordType);
        return;
    }


    /**
     *  Removes support for a lesson record type.
     *
     *  @param lessonRecordType a lesson record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>lessonRecordType</code> is <code>null</code>
     */

    protected void removeLessonRecordType(org.osid.type.Type lessonRecordType) {
        this.lessonRecordTypes.remove(lessonRecordType);
        return;
    }


    /**
     *  Gets the supported lesson search record types. 
     *
     *  @return a list containing the supported lesson search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLessonSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.lessonSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given lesson search record type is supported. 
     *
     *  @param  lessonSearchRecordType a <code> Type </code> indicating a 
     *          lesson record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> lessonSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLessonSearchRecordType(org.osid.type.Type lessonSearchRecordType) {
        return (this.lessonSearchRecordTypes.contains(lessonSearchRecordType));
    }


    /**
     *  Adds support for a lesson search record type.
     *
     *  @param lessonSearchRecordType a lesson search record type
     *  @throws org.osid.NullArgumentException
     *  <code>lessonSearchRecordType</code> is <code>null</code>
     */

    protected void addLessonSearchRecordType(org.osid.type.Type lessonSearchRecordType) {
        this.lessonSearchRecordTypes.add(lessonSearchRecordType);
        return;
    }


    /**
     *  Removes support for a lesson search record type.
     *
     *  @param lessonSearchRecordType a lesson search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>lessonSearchRecordType</code> is <code>null</code>
     */

    protected void removeLessonSearchRecordType(org.osid.type.Type lessonSearchRecordType) {
        this.lessonSearchRecordTypes.remove(lessonSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan lookup 
     *  service. 
     *
     *  @return a <code> PlanLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanLookupSession getPlanLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanLookupSession getPlanLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> PlanLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanLookupSession getPlanLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanLookupSession getPlanLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan query 
     *  service. 
     *
     *  @return a <code> PlanQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQuerySession getPlanQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQuerySession getPlanQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> PlanQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQuerySession getPlanQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanQuerySession getPlanQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan search 
     *  service. 
     *
     *  @return a <code> PlanSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSearchSession getPlanSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSearchSession getPlanSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> PlanSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSearchSession getPlanSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSearchSession getPlanSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  administration service. 
     *
     *  @return a <code> PlanAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanAdminSession getPlanAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanAdminSession getPlanAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> PlanAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanAdminSession getPlanAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPlanAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanAdminSession getPlanAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  notification service. 
     *
     *  @param  planReceiver the receiver 
     *  @return a <code> PlanNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> planReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanNotificationSession getPlanNotificationSession(org.osid.course.plan.PlanReceiver planReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  notification service. 
     *
     *  @param  planReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> PlanNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> planReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanNotificationSession getPlanNotificationSession(org.osid.course.plan.PlanReceiver planReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  notification service for the given course catalog. 
     *
     *  @param  planReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> PlanNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> planReceiver </code> or 
     *          <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanNotificationSession getPlanNotificationSessionForCourseCatalog(org.osid.course.plan.PlanReceiver planReceiver, 
                                                                                                   org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the plan 
     *  notification service for the given course catalog. 
     *
     *  @param  planReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PlanNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> planReceiver, 
     *          courseCatalogId, </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanNotificationSession getPlanNotificationSessionForCourseCatalog(org.osid.course.plan.PlanReceiver planReceiver, 
                                                                                                   org.osid.id.Id courseCatalogId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the session for retrieving plan to course catalog mappings. 
     *
     *  @return a <code> PlanCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanCourseCatalogSession getPlanCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for retrieving plan to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanCourseCatalogSession getPlanCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for assigning plan to course catalog mappings. 
     *
     *  @return a <code> PlanCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanCourseCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanCourseCatalogAssignmentSession getPlanCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning plan to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PlanCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanCourseCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanCourseCatalogAssignmentSession getPlanCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the plan smart course catalog for the 
     *  given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> PlanSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSmartCourseCatalogSession getPlanSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getPlanSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session associated with the plan smart course catalog for the 
     *  given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> PlanSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPlanSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.PlanSmartCourseCatalogSession getPlanSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getPlanSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson lookup 
     *  service. 
     *
     *  @return a <code> LessonLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonLookupSession getLessonLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonLookupSession getLessonLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> LessonLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonLookupSession getLessonLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonLookupSession getLessonLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson query 
     *  service. 
     *
     *  @return a <code> LessonQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQuerySession getLessonQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQuerySession getLessonQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> LessonQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQuerySession getLessonQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Lesson </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQuerySession getLessonQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson search 
     *  service. 
     *
     *  @return a <code> LessonSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSearchSession getLessonSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSearchSession getLessonSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> LessonSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSearchSession getLessonSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lesson </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSearchSession getLessonSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  administrative service. 
     *
     *  @return a <code> LessonAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonAdminSession getLessonAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonAdminSession getLessonAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  administrative service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> LessonAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonAdminSession getLessonAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lesson </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLessonAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonAdminSession getLessonAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  notification service. 
     *
     *  @param  lessonReceiver the receiver 
     *  @return a <code> LessonNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> lessonReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonNotificationSession getLessonNotificationSession(org.osid.course.plan.LessonReceiver lessonReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  notification service. 
     *
     *  @param  lessonReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> LessonNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> lessonReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonNotificationSession getLessonNotificationSession(org.osid.course.plan.LessonReceiver lessonReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  notification service for the given course catalog. 
     *
     *  @param  lessonReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> LessonNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> lessonReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonNotificationSession getLessonNotificationSessionForCourseCatalog(org.osid.course.plan.LessonReceiver lessonReceiver, 
                                                                                                       org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  notification service for the given course catalog. 
     *
     *  @param  lessonReceiver the receiver 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LessonNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lesson </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> lessonReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonNotificationSession getLessonNotificationSessionForCourseCatalog(org.osid.course.plan.LessonReceiver lessonReceiver, 
                                                                                                       org.osid.id.Id courseCatalogId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the session for retrieving lesson to course catalog mappings. 
     *
     *  @return a <code> LessonCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonCourseCatalogSession getLessonCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for retrieving lesson to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonCourseCatalogSession getLessonCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for assigning lesson to course catalog mappings. 
     *
     *  @return a <code> LessonCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonCourseCatalogAssignmentSession getLessonCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning lesson to course catalog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonCourseCatalogAssignmentSession getLessonCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the lesson smart course catalog for 
     *  the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> LessonSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSmartCourseCatalogSession getLessonSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic lesson course catalogs for the 
     *  given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of a course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> LessonSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException <code> courseCatalogId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonSmartCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonSmartCourseCatalogSession getLessonSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service. 
     *
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonConflictSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonConflictSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonConflictSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonConflictSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Lesson 
     *          </code> 
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonConflictSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonConflictSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonConflictSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonConflictSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service. 
     *
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonAnchoringSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonAnchoringSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  conflict service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LessonConflictSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonConflict() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonAnchoringSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonAnchoringSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  anchoring service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> Lesson 
     *          </code> 
     *  @return a <code> LessonAnchoringSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonAnchoring() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonAnchoringSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanManager.getLessonAnchoringSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lesson 
     *  anchoring service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy a proxy 
     *  @return a <code> LessonAnchoringSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLessonAnchoring() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.LessonConflictSession getLessonAnchoringSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.plan.CoursePlanProxyManager.getLessonAnchoringSessionForCourseCatalog not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.syllabusRecordTypes.clear();
        this.syllabusRecordTypes.clear();

        this.syllabusSearchRecordTypes.clear();
        this.syllabusSearchRecordTypes.clear();

        this.planRecordTypes.clear();
        this.planRecordTypes.clear();

        this.planSearchRecordTypes.clear();
        this.planSearchRecordTypes.clear();

        this.lessonRecordTypes.clear();
        this.lessonRecordTypes.clear();

        this.lessonSearchRecordTypes.clear();
        this.lessonSearchRecordTypes.clear();

        return;
    }
}
