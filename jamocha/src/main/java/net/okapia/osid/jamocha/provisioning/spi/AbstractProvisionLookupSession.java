//
// AbstractProvisionLookupSession.java
//
//    A starter implementation framework for providing a Provision
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Provision
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProvisions(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProvisionLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.ProvisionLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();
    

    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>Provision</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProvisions() {
        return (true);
    }


    /**
     *  A complete view of the <code>Provision</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProvisionView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Provision</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProvisionView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include provisions in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only provisions whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveProvisionView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All provisions of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProvisionView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Provision</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Provision</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Provision</code> and
     *  retained for compatibility.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionId <code>Id</code> of the
     *          <code>Provision</code>
     *  @return the provision
     *  @throws org.osid.NotFoundException <code>provisionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>provisionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getProvision(org.osid.id.Id provisionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.ProvisionList provisions = getProvisions()) {
            while (provisions.hasNext()) {
                org.osid.provisioning.Provision provision = provisions.getNextProvision();
                if (provision.getId().equals(provisionId)) {
                    return (provision);
                }
            }
        } 

        throw new org.osid.NotFoundException(provisionId + " not found");
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  provisions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Provisions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, provisions are returned that are currently effective.
     *  In any effective mode, effective provisions and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProvisions()</code>.
     *
     *  @param  provisionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>provisionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByIds(org.osid.id.IdList provisionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.Provision> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = provisionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProvision(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("provision " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.provision.LinkedProvisionList(ret));
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  provision genus <code>Type</code> which does not include
     *  provisions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently effective.
     *  In any effective mode, effective provisions and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProvisions()</code>.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionGenusFilterList(getProvisions(), provisionGenusType));
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  provision genus <code>Type</code> and include any additional
     *  provisions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProvisions()</code>.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByParentGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProvisionsByGenusType(provisionGenusType));
    }


    /**
     *  Gets a <code>ProvisionList</code> containing the given
     *  provision record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProvisions()</code>.
     *
     *  @param  provisionRecordType a provision record type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByRecordType(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionRecordFilterList(getProvisions(), provisionRecordType));
    }


    /**
     *  Gets a <code>ProvisionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *  
     *  In active mode, provisions are returned that are currently
     *  active. In any status mode, active and inactive provisions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Provision</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.TemporalProvisionFilterList(getProvisions(), from, to));
    }


    /**
     *  Gets a list of provisions corresponding to a broker
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the broker
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>brokerId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForBroker(org.osid.id.Id brokerId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilterList(new BrokerFilter(brokerId), getProvisions()));
    }


    /**
     *  Gets a list of provisions corresponding to a broker
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the broker
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>brokerId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForBrokerOnDate(org.osid.id.Id brokerId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.TemporalProvisionFilterList(getProvisionsForBroker(brokerId), from, to));
    }


    /**
     *  Gets a list of provisions corresponding to a provisionable
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForProvisionable(org.osid.id.Id provisionableId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilterList(new ProvisionableFilter(provisionableId), getProvisions()));
    }


    /**
     *  Gets a list of provisions corresponding to a provisionable
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableOnDate(org.osid.id.Id provisionableId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.TemporalProvisionFilterList(getProvisionsForProvisionable(provisionableId), from, to));
    }


    /**
     *  Gets a list of provisions corresponding to a recipient
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForRecipient(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilterList(new RecipientFilter(resourceId), getProvisions()));
    }


    /**
     *  Gets a list of provisions corresponding to a recipient
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForRecipientOnDate(org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.TemporalProvisionFilterList(getProvisionsForRecipient(resourceId), from, to));
    }


    /**
     *  Gets a list of provisions corresponding to provisionable and recipient
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableAndRecipient(org.osid.id.Id provisionableId,
                                                                        org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilterList(new RecipientFilter(resourceId), getProvisionsForProvisionable(provisionableId)));
    }


    /**
     *  Gets a list of provisions corresponding to provisionable and recipient
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableAndRecipientOnDate(org.osid.id.Id provisionableId,
                                                                                               org.osid.id.Id resourceId,
                                                                                               org.osid.calendaring.DateTime from,
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.TemporalProvisionFilterList(getProvisionsForProvisionableAndRecipient(provisionableId, resourceId), from, to));
    }


    /**
     *  Gets a list of provisions corresponding to a request
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  requestId the <code>Id</code> of the request
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>requestId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForRequest(org.osid.id.Id requestId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilterList(new RequestFilter(requestId), getProvisions()));
    }


    /**
     *  Gets all <code>Provisions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Provisions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.provisioning.ProvisionList getProvisions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the provision list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of provisions
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.provisioning.ProvisionList filterProvisionsOnViews(org.osid.provisioning.ProvisionList list)
        throws org.osid.OperationFailedException {

        org.osid.provisioning.ProvisionList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.provisioning.provision.EffectiveProvisionFilterList(ret);
        }

        return (ret);
    }


    public static class BrokerFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilter {
         
        private final org.osid.id.Id brokerId;
         
         
        /**
         *  Constructs a new <code>BrokerFilter</code>.
         *
         *  @param brokerId the broker to filter
         *  @throws org.osid.NullArgumentException
         *          <code>brokerId</code> is <code>null</code>
         */
        
        public BrokerFilter(org.osid.id.Id brokerId) {
            nullarg(brokerId, "broker Id");
            this.brokerId = brokerId;
            return;
        }

         
        /**
         *  Used by the ProvisionFilterList to filter the 
         *  provision list based on broker.
         *
         *  @param provision the provision
         *  @return <code>true</code> to pass the provision,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Provision provision) {
            return (provision.getBrokerId().equals(this.brokerId));
        }
    }


    public static class ProvisionableFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilter {
         
        private final org.osid.id.Id provisionableId;
         
         
        /**
         *  Constructs a new <code>ProvisionableFilter</code>.
         *
         *  @param provisionableId the provisionable to filter
         *  @throws org.osid.NullArgumentException
         *          <code>provisionableId</code> is <code>null</code>
         */
        
        public ProvisionableFilter(org.osid.id.Id provisionableId) {
            nullarg(provisionableId, "provisionable Id");
            this.provisionableId = provisionableId;
            return;
        }

         
        /**
         *  Used by the ProvisionFilterList to filter the 
         *  provision list based on provisionable.
         *
         *  @param provision the provision
         *  @return <code>true</code> to pass the provision,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Provision provision) {
            return (provision.getProvisionableId().equals(this.provisionableId));
        }
    }


    public static class RecipientFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>RecipientFilter</code>.
         *
         *  @param resourceId the recipient to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public RecipientFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "recipient Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the ProvisionFilterList to filter the 
         *  provision list based on recipient.
         *
         *  @param provision the provision
         *  @return <code>true</code> to pass the provision,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Provision provision) {
            return (provision.getRecipientId().equals(this.resourceId));
        }
    }


    public static class RequestFilter
        implements net.okapia.osid.jamocha.inline.filter.provisioning.provision.ProvisionFilter {
         
        private final org.osid.id.Id requestId;
         
         
        /**
         *  Constructs a new <code>RequestFilter</code>.
         *
         *  @param requestId the request to filter
         *  @throws org.osid.NullArgumentException
         *          <code>requestId</code> is <code>null</code>
         */
        
        public RequestFilter(org.osid.id.Id requestId) {
            nullarg(requestId, "request Id");
            this.requestId = requestId;
            return;
        }

         
        /**
         *  Used by the ProvisionFilterList to filter the 
         *  provision list based on request.
         *
         *  @param provision the provision
         *  @return <code>true</code> to pass the provision,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.provisioning.Provision provision) {
            if (provision.provisionedByRequest()) {
                return (provision.getRequestId().equals(this.requestId));
            } else {
                return (false);
            }
        }
    }
}
