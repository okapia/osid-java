//
// AbstractBiddingRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractBiddingRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.bidding.rules.BiddingRulesManager,
               org.osid.bidding.rules.BiddingRulesProxyManager {

    private final Types auctionConstrainerRecordTypes      = new TypeRefSet();
    private final Types auctionConstrainerSearchRecordTypes= new TypeRefSet();

    private final Types auctionConstrainerEnablerRecordTypes= new TypeRefSet();
    private final Types auctionConstrainerEnablerSearchRecordTypes= new TypeRefSet();

    private final Types auctionProcessorRecordTypes        = new TypeRefSet();
    private final Types auctionProcessorSearchRecordTypes  = new TypeRefSet();

    private final Types auctionProcessorEnablerRecordTypes = new TypeRefSet();
    private final Types auctionProcessorEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractBiddingRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractBiddingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up auction constrainer is supported. 
     *
     *  @return <code> true </code> if auction constrainer lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerLookup() {
        return (false);
    }


    /**
     *  Tests if querying auction constrainer is supported. 
     *
     *  @return <code> true </code> if auction constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerQuery() {
        return (false);
    }


    /**
     *  Tests if searching auction constrainer is supported. 
     *
     *  @return <code> true </code> if auction constrainer search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerSearch() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if auction constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerAdmin() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer notification service is supported. 
     *
     *  @return <code> true </code> if auction constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerNotification() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction constrainer auction house 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer auction house service is supported. 
     *
     *  @return <code> true </code> if auction constrainer auction house 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerAuctionHouseAssignment() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction constrainer auction house 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerSmartAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if an auction constrainer rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if an auction constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up auction constrainer enablers is supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying auction constrainer enablers is supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching auction constrainer enablers is supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler 
     *          notification is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer enabler auction house lookup service 
     *  is supported. 
     *
     *  @return <code> true </code> if an auction constrainer enabler auction 
     *          house lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer enabler auction house service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler auction 
     *          house assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerAuctionHouseAssignment() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer enabler auction house lookup service 
     *  is supported. 
     *
     *  @return <code> true </code> if an auction constrainer enabler auction 
     *          house service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerSmartAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction constrainer enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an auction constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up auction processor is supported. 
     *
     *  @return <code> true </code> if auction processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorLookup() {
        return (false);
    }


    /**
     *  Tests if querying auction processor is supported. 
     *
     *  @return <code> true </code> if auction processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorQuery() {
        return (false);
    }


    /**
     *  Tests if searching auction processor is supported. 
     *
     *  @return <code> true </code> if auction processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorSearch() {
        return (false);
    }


    /**
     *  Tests if an auction processor administrative service is supported. 
     *
     *  @return <code> true </code> if auction processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorAdmin() {
        return (false);
    }


    /**
     *  Tests if an auction processor notification service is supported. 
     *
     *  @return <code> true </code> if auction processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorNotification() {
        return (false);
    }


    /**
     *  Tests if an auction processor auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction processor auction house 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction processor auction house service is supported. 
     *
     *  @return <code> true </code> if auction processor auction house 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorAuctionHouseAssignment() {
        return (false);
    }


    /**
     *  Tests if an auction processor auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction processor auction house 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorSmartAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if an auction processor rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an auction processor rule application service is supported. 
     *
     *  @return <code> true </code> if auction processor rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up auction processor enablers is supported. 
     *
     *  @return <code> true </code> if auction processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying auction processor enablers is supported. 
     *
     *  @return <code> true </code> if auction processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching auction processor enablers is supported. 
     *
     *  @return <code> true </code> if auction processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an auction processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction processor enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an auction processor enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction processor enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an auction processor enabler auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction processor enabler auction 
     *          house lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction processor enabler auction house service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction processor enabler auction house 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerAuctionHouseAssignment() {
        return (false);
    }


    /**
     *  Tests if an auction processor enabler auction house lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an auction processor enabler auction 
     *          house service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerSmartAuctionHouse() {
        return (false);
    }


    /**
     *  Tests if an auction processor enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an auction processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if auction processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> AuctionConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> AuctionConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionConstrainerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionConstrainerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  auctionConstrainerRecordType a <code> Type </code> indicating 
     *          an <code> AuctionConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerRecordType(org.osid.type.Type auctionConstrainerRecordType) {
        return (this.auctionConstrainerRecordTypes.contains(auctionConstrainerRecordType));
    }


    /**
     *  Adds support for an auction constrainer record type.
     *
     *  @param auctionConstrainerRecordType an auction constrainer record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionConstrainerRecordType</code> is <code>null</code>
     */

    protected void addAuctionConstrainerRecordType(org.osid.type.Type auctionConstrainerRecordType) {
        this.auctionConstrainerRecordTypes.add(auctionConstrainerRecordType);
        return;
    }


    /**
     *  Removes support for an auction constrainer record type.
     *
     *  @param auctionConstrainerRecordType an auction constrainer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionConstrainerRecordType</code> is <code>null</code>
     */

    protected void removeAuctionConstrainerRecordType(org.osid.type.Type auctionConstrainerRecordType) {
        this.auctionConstrainerRecordTypes.remove(auctionConstrainerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> AuctionConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionConstrainerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionConstrainerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionConstrainer </code> search record 
     *  type is supported. 
     *
     *  @param  auctionConstrainerSearchRecordType a <code> Type </code> 
     *          indicating an <code> AuctionConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerSearchRecordType(org.osid.type.Type auctionConstrainerSearchRecordType) {
        return (this.auctionConstrainerSearchRecordTypes.contains(auctionConstrainerSearchRecordType));
    }


    /**
     *  Adds support for an auction constrainer search record type.
     *
     *  @param auctionConstrainerSearchRecordType an auction constrainer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void addAuctionConstrainerSearchRecordType(org.osid.type.Type auctionConstrainerSearchRecordType) {
        this.auctionConstrainerSearchRecordTypes.add(auctionConstrainerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an auction constrainer search record type.
     *
     *  @param auctionConstrainerSearchRecordType an auction constrainer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuctionConstrainerSearchRecordType(org.osid.type.Type auctionConstrainerSearchRecordType) {
        this.auctionConstrainerSearchRecordTypes.remove(auctionConstrainerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionConstrainerEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          AuctionConstrainerEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionConstrainerEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionConstrainerEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionConstrainerEnabler </code> record 
     *  type is supported. 
     *
     *  @param  auctionConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating an <code> AuctionConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType) {
        return (this.auctionConstrainerEnablerRecordTypes.contains(auctionConstrainerEnablerRecordType));
    }


    /**
     *  Adds support for an auction constrainer enabler record type.
     *
     *  @param auctionConstrainerEnablerRecordType an auction constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void addAuctionConstrainerEnablerRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType) {
        this.auctionConstrainerEnablerRecordTypes.add(auctionConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an auction constrainer enabler record type.
     *
     *  @param auctionConstrainerEnablerRecordType an auction constrainer enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void removeAuctionConstrainerEnablerRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType) {
        this.auctionConstrainerEnablerRecordTypes.remove(auctionConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionConstrainerEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          AuctionConstrainerEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionConstrainerEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionConstrainerEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionConstrainerEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  auctionConstrainerEnablerSearchRecordType a <code> Type 
     *          </code> indicating an <code> AuctionConstrainerEnabler </code> 
     *          search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionConstrainerEnablerSearchRecordType(org.osid.type.Type auctionConstrainerEnablerSearchRecordType) {
        return (this.auctionConstrainerEnablerSearchRecordTypes.contains(auctionConstrainerEnablerSearchRecordType));
    }


    /**
     *  Adds support for an auction constrainer enabler search record type.
     *
     *  @param auctionConstrainerEnablerSearchRecordType an auction constrainer enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addAuctionConstrainerEnablerSearchRecordType(org.osid.type.Type auctionConstrainerEnablerSearchRecordType) {
        this.auctionConstrainerEnablerSearchRecordTypes.add(auctionConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an auction constrainer enabler search record type.
     *
     *  @param auctionConstrainerEnablerSearchRecordType an auction constrainer enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuctionConstrainerEnablerSearchRecordType(org.osid.type.Type auctionConstrainerEnablerSearchRecordType) {
        this.auctionConstrainerEnablerSearchRecordTypes.remove(auctionConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> AuctionProcessor 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionProcessorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionProcessorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionProcessor </code> record type is 
     *  supported. 
     *
     *  @param  auctionProcessorRecordType a <code> Type </code> indicating an 
     *          <code> AuctionProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorRecordType(org.osid.type.Type auctionProcessorRecordType) {
        return (this.auctionProcessorRecordTypes.contains(auctionProcessorRecordType));
    }


    /**
     *  Adds support for an auction processor record type.
     *
     *  @param auctionProcessorRecordType an auction processor record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionProcessorRecordType</code> is <code>null</code>
     */

    protected void addAuctionProcessorRecordType(org.osid.type.Type auctionProcessorRecordType) {
        this.auctionProcessorRecordTypes.add(auctionProcessorRecordType);
        return;
    }


    /**
     *  Removes support for an auction processor record type.
     *
     *  @param auctionProcessorRecordType an auction processor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionProcessorRecordType</code> is <code>null</code>
     */

    protected void removeAuctionProcessorRecordType(org.osid.type.Type auctionProcessorRecordType) {
        this.auctionProcessorRecordTypes.remove(auctionProcessorRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionProcessor </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> AuctionProcessor 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionProcessorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionProcessorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionProcessor </code> search record type 
     *  is supported. 
     *
     *  @param  auctionProcessorSearchRecordType a <code> Type </code> 
     *          indicating an <code> AuctionProcessor </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorSearchRecordType(org.osid.type.Type auctionProcessorSearchRecordType) {
        return (this.auctionProcessorSearchRecordTypes.contains(auctionProcessorSearchRecordType));
    }


    /**
     *  Adds support for an auction processor search record type.
     *
     *  @param auctionProcessorSearchRecordType an auction processor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void addAuctionProcessorSearchRecordType(org.osid.type.Type auctionProcessorSearchRecordType) {
        this.auctionProcessorSearchRecordTypes.add(auctionProcessorSearchRecordType);
        return;
    }


    /**
     *  Removes support for an auction processor search record type.
     *
     *  @param auctionProcessorSearchRecordType an auction processor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuctionProcessorSearchRecordType(org.osid.type.Type auctionProcessorSearchRecordType) {
        this.auctionProcessorSearchRecordTypes.remove(auctionProcessorSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionProcessorEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> AuctionProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionProcessorEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionProcessorEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionProcessorEnabler </code> record type 
     *  is supported. 
     *
     *  @param  auctionProcessorEnablerRecordType a <code> Type </code> 
     *          indicating an <code> AuctionProcessorEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerRecordType(org.osid.type.Type auctionProcessorEnablerRecordType) {
        return (this.auctionProcessorEnablerRecordTypes.contains(auctionProcessorEnablerRecordType));
    }


    /**
     *  Adds support for an auction processor enabler record type.
     *
     *  @param auctionProcessorEnablerRecordType an auction processor enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void addAuctionProcessorEnablerRecordType(org.osid.type.Type auctionProcessorEnablerRecordType) {
        this.auctionProcessorEnablerRecordTypes.add(auctionProcessorEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an auction processor enabler record type.
     *
     *  @param auctionProcessorEnablerRecordType an auction processor enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void removeAuctionProcessorEnablerRecordType(org.osid.type.Type auctionProcessorEnablerRecordType) {
        this.auctionProcessorEnablerRecordTypes.remove(auctionProcessorEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuctionProcessorEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> AuctionProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuctionProcessorEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.auctionProcessorEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuctionProcessorEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  auctionProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> AuctionProcessorEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuctionProcessorEnablerSearchRecordType(org.osid.type.Type auctionProcessorEnablerSearchRecordType) {
        return (this.auctionProcessorEnablerSearchRecordTypes.contains(auctionProcessorEnablerSearchRecordType));
    }


    /**
     *  Adds support for an auction processor enabler search record type.
     *
     *  @param auctionProcessorEnablerSearchRecordType an auction processor enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>auctionProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addAuctionProcessorEnablerSearchRecordType(org.osid.type.Type auctionProcessorEnablerSearchRecordType) {
        this.auctionProcessorEnablerSearchRecordTypes.add(auctionProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an auction processor enabler search record type.
     *
     *  @param auctionProcessorEnablerSearchRecordType an auction processor enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>auctionProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuctionProcessorEnablerSearchRecordType(org.osid.type.Type auctionProcessorEnablerSearchRecordType) {
        this.auctionProcessorEnablerSearchRecordTypes.remove(auctionProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer lookup service. 
     *
     *  @return an <code> AuctionConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerLookupSession getAuctionConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerLookupSession getAuctionConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerLookupSession getAuctionConstrainerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerLookupSession getAuctionConstrainerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer query service. 
     *
     *  @return an <code> AuctionConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQuerySession getAuctionConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQuerySession getAuctionConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQuerySession getAuctionConstrainerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQuerySession getAuctionConstrainerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer search service. 
     *
     *  @return an <code> AuctionConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSearchSession getAuctionConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSearchSession getAuctionConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSearchSession getAuctionConstrainerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSearchSession getAuctionConstrainerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer administration service. 
     *
     *  @return an <code> AuctionConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAdminSession getAuctionConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAdminSession getAuctionConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAdminSession getAuctionConstrainerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAdminSession getAuctionConstrainerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer notification service. 
     *
     *  @param  auctionConstrainerReceiver the notification callback 
     *  @return an <code> AuctionConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerNotificationSession getAuctionConstrainerNotificationSession(org.osid.bidding.rules.AuctionConstrainerReceiver auctionConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer notification service. 
     *
     *  @param  auctionConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerNotificationSession getAuctionConstrainerNotificationSession(org.osid.bidding.rules.AuctionConstrainerReceiver auctionConstrainerReceiver, 
                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer notification service for the given auction house. 
     *
     *  @param  auctionConstrainerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerReceiver </code> or <code> auctionHouseId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerNotificationSession getAuctionConstrainerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionConstrainerReceiver auctionConstrainerReceiver, 
                                                                                                                                org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer notification service for the given auction house. 
     *
     *  @param  auctionConstrainerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerReceiver, auctionHouseId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerNotificationSession getAuctionConstrainerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionConstrainerReceiver auctionConstrainerReceiver, 
                                                                                                                                org.osid.id.Id auctionHouseId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction 
     *  constrainer/auction house mappings for auction constrainers. 
     *
     *  @return an <code> AuctionConstrainerAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAuctionHouse() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAuctionHouseSession getAuctionConstrainerAuctionHouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction 
     *  constrainer/auction house mappings for auction constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerAuctionHouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAuctionHouse() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAuctionHouseSession getAuctionConstrainerAuctionHouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  constrainer to auction houses. 
     *
     *  @return an <code> AuctionConstrainerAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAuctionHouseAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAuctionHouseAssignmentSession getAuctionConstrainerAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  constrainer to auction houses. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerAuctionHouseAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerAuctionHouseAssignmentSession getAuctionConstrainerAuctionHouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction constrainer 
     *  smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSmartAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSmartAuctionHouseSession getAuctionConstrainerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction constrainer 
     *  smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerSmartAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerSmartAuctionHouseSession getAuctionConstrainerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer auction mapping lookup service for looking up the rules 
     *  applied to the auction house. 
     *
     *  @return an <code> AuctionConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleLookupSession getAuctionConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  the auction house. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleLookupSession getAuctionConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer mapping lookup service for the given auction house for 
     *  looking up rules applied to an auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleLookupSession getAuctionConstrainerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerRuleLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer mapping lookup service for the given auction house for 
     *  looking up rules applied to an auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleLookupSession getAuctionConstrainerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerRuleLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer assignment service to apply to auction houses. 
     *
     *  @return an <code> AuctionConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleApplicationSession getAuctionConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer assignment service to apply to auction houses. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleApplicationSession getAuctionConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer assignment service for the given auction house to apply to 
     *  auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleApplicationSession getAuctionConstrainerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerRuleApplicationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer assignment service for the given auction house to apply to 
     *  auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerRuleApplicationSession getAuctionConstrainerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerRuleApplicationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler lookup service. 
     *
     *  @return an <code> AuctionConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession getAuctionConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession getAuctionConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession getAuctionConstrainerEnablerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession getAuctionConstrainerEnablerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler query service. 
     *
     *  @return an <code> AuctionConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerQuerySession getAuctionConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerQuerySession getAuctionConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerQuerySession getAuctionConstrainerEnablerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerQuerySession getAuctionConstrainerEnablerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler search service. 
     *
     *  @return an <code> AuctionConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSearchSession getAuctionConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSearchSession getAuctionConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enablers earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSearchSession getAuctionConstrainerEnablerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enablers earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSearchSession getAuctionConstrainerEnablerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler administration service. 
     *
     *  @return an <code> AuctionConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAdminSession getAuctionConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAdminSession getAuctionConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler administration service for the given auction 
     *  house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAdminSession getAuctionConstrainerEnablerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler administration service for the given auction 
     *  house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAdminSession getAuctionConstrainerEnablerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler notification service. 
     *
     *  @param  auctionConstrainerEnablerReceiver the notification callback 
     *  @return an <code> AuctionConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerReceiver </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerNotificationSession getAuctionConstrainerEnablerNotificationSession(org.osid.bidding.rules.AuctionConstrainerEnablerReceiver auctionConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler notification service. 
     *
     *  @param  auctionConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerNotificationSession getAuctionConstrainerEnablerNotificationSession(org.osid.bidding.rules.AuctionConstrainerEnablerReceiver auctionConstrainerEnablerReceiver, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler notification service for the given auction house. 
     *
     *  @param  auctionConstrainerEnablerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerReceiver </code> or <code> 
     *          auctionHouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerNotificationSession getAuctionConstrainerEnablerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionConstrainerEnablerReceiver auctionConstrainerEnablerReceiver, 
                                                                                                                                              org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler notification service for the given auction house. 
     *
     *  @param  auctionConstrainerEnablerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionConstrainerEnablerReceiver, auctionHouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerNotificationSession getAuctionConstrainerEnablerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionConstrainerEnablerReceiver auctionConstrainerEnablerReceiver, 
                                                                                                                                              org.osid.id.Id auctionHouseId, 
                                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction constrainer 
     *  enabler/auction house mappings for auction constrainer enablers. 
     *
     *  @return an <code> AuctionConstrainerEnablerAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAuctionHouseSession getAuctionConstrainerEnablerAuctionHouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction constrainer 
     *  enabler/auction house mappings for auction constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerAuctionHouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAuctionHouseSession getAuctionConstrainerEnablerAuctionHouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  constrainer enablers to auction houses. 
     *
     *  @return an <code> 
     *          AuctionConstrainerEnablerAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAuctionHouseAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAuctionHouseAssignmentSession getAuctionConstrainerEnablerAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  constrainer enablers to auction houses. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> 
     *          AuctionConstrainerEnablerAuctionHouseAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerAuctionHouseAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerAuctionHouseAssignmentSession getAuctionConstrainerEnablerAuctionHouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction constrainer 
     *  enabler smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerSmartAuctionHouseSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSmartAuctionHouse() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSmartAuctionHouseSession getAuctionConstrainerEnablerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction constrainer 
     *  enabler smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerSmartAuctionHouseSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerSmartAuctionHouse() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerSmartAuctionHouseSession getAuctionConstrainerEnablerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId, 
                                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return an <code> AuctionConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleLookupSession getAuctionConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleLookupSession getAuctionConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler mapping lookup service for the given auction 
     *  house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleLookupSession getAuctionConstrainerEnablerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerRuleLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler mapping lookup service for the given auction 
     *  house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleLookupSession getAuctionConstrainerEnablerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerRuleLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler assignment service. 
     *
     *  @return an <code> AuctionConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleApplicationSession getAuctionConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleApplicationSession getAuctionConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler assignment service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleApplicationSession getAuctionConstrainerEnablerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionConstrainerEnablerRuleApplicationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  constrainer enabler assignment service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerRuleApplicationSession getAuctionConstrainerEnablerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionConstrainerEnablerRuleApplicationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor lookup service. 
     *
     *  @return an <code> AuctionProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorLookupSession getAuctionProcessorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorLookupSession getAuctionProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorLookupSession getAuctionProcessorLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorLookupSession getAuctionProcessorLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor query service. 
     *
     *  @return an <code> AuctionProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuerySession getAuctionProcessorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuerySession getAuctionProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuerySession getAuctionProcessorQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuerySession getAuctionProcessorQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor search service. 
     *
     *  @return an <code> AuctionProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSearchSession getAuctionProcessorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSearchSession getAuctionProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSearchSession getAuctionProcessorSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSearchSession getAuctionProcessorSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor administration service. 
     *
     *  @return an <code> AuctionProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAdminSession getAuctionProcessorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAdminSession getAuctionProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAdminSession getAuctionProcessorAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAdminSession getAuctionProcessorAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor notification service. 
     *
     *  @param  auctionProcessorReceiver the notification callback 
     *  @return an <code> AuctionProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionProcessorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorNotificationSession getAuctionProcessorNotificationSession(org.osid.bidding.rules.AuctionProcessorReceiver auctionProcessorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor notification service. 
     *
     *  @param  auctionProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> auctionProcessoReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorNotificationSession getAuctionProcessorNotificationSession(org.osid.bidding.rules.AuctionProcessorReceiver auctionProcessorReceiver, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor notification service for the given auction house. 
     *
     *  @param  auctionProcessorReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionProcessorReceiver 
     *          </code> or <code> auctionHouseId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorNotificationSession getAuctionProcessorNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionProcessorReceiver auctionProcessorReceiver, 
                                                                                                                            org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor notification service for the given auction house. 
     *
     *  @param  auctionProcessoReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionProcessoReceiver, 
     *          auctionHouseId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorNotificationSession getAuctionProcessorNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionProcessorReceiver auctionProcessoReceiver, 
                                                                                                                            org.osid.id.Id auctionHouseId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction 
     *  processor/auction house mappings for auction processors. 
     *
     *  @return an <code> AuctionProcessorAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAuctionHouse() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAuctionHouseSession getAuctionProcessorAuctionHouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction 
     *  processor/auction house mappings for auction processors. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorAuctionHouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAuctionHouse() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAuctionHouseSession getAuctionProcessorAuctionHouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  processor to auction houses. 
     *
     *  @return an <code> AuctionProcessorAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAuctionHouseAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAuctionHouseAssignmentSession getAuctionProcessorAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  processor to auction houses. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorAuctionHouseAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorAuctionHouseAssignmentSession getAuctionProcessorAuctionHouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction processor smart 
     *  auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSmartAuctionHouse() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSmartAuctionHouseSession getAuctionProcessorSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction processor smart 
     *  auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorSmartAuctionHouseSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorSmartAuctionHouse() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorSmartAuctionHouseSession getAuctionProcessorSmartAuctionHouseSession(org.osid.id.Id auctionHouseId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor auction mapping lookup service for looking up the rules 
     *  applied to the auction house. 
     *
     *  @return an <code> AuctionProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleLookupSession getAuctionProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor mapping lookup service for looking up the rules applied to 
     *  the auction house. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleLookupSession getAuctionProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor mapping lookup service for the given auction house for 
     *  looking up rules applied to an auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleLookupSession getAuctionProcessorRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorRuleLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor mapping lookup service for the given auction house for 
     *  looking up rules applied to an auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleLookupSession getAuctionProcessorRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorRuleLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor assignment service. 
     *
     *  @return an <code> AuctionProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleApplicationSession getAuctionProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor assignment service to apply to auction houses. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleApplicationSession getAuctionProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor assignment service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleApplicationSession getAuctionProcessorRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorRuleApplicationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor assignment service for the given auction house to apply to 
     *  auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorRuleApplicationSession getAuctionProcessorRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorRuleApplicationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler lookup service. 
     *
     *  @return an <code> AuctionProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerLookupSession getAuctionProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerLookupSession getAuctionProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerLookupSession getAuctionProcessorEnablerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerLookupSession getAuctionProcessorEnablerLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler query service. 
     *
     *  @return an <code> AuctionProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerQuerySession getAuctionProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerQuerySession getAuctionProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerQuerySession getAuctionProcessorEnablerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler query service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerQuerySession getAuctionProcessorEnablerQuerySessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerQuerySessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler search service. 
     *
     *  @return an <code> AuctionProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSearchSession getAuctionProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSearchSession getAuctionProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enablers earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSearchSession getAuctionProcessorEnablerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enablers earch service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSearchSession getAuctionProcessorEnablerSearchSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerSearchSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler administration service. 
     *
     *  @return an <code> AuctionProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAdminSession getAuctionProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAdminSession getAuctionProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAdminSession getAuctionProcessorEnablerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler administration service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAdminSession getAuctionProcessorEnablerAdminSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerAdminSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler notification service. 
     *
     *  @param  auctionProcessorEnablerReceiver the notification callback 
     *  @return an <code> AuctionProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerNotificationSession getAuctionProcessorEnablerNotificationSession(org.osid.bidding.rules.AuctionProcessorEnablerReceiver auctionProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler notification service. 
     *
     *  @param  auctionProcessoEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessoEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerNotificationSession getAuctionProcessorEnablerNotificationSession(org.osid.bidding.rules.AuctionProcessorEnablerReceiver auctionProcessoEnablerReceiver, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler notification service for the given auction house. 
     *
     *  @param  auctionProcessorEnablerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessorEnablerReceiver </code> or <code> 
     *          auctionHouseId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerNotificationSession getAuctionProcessorEnablerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionProcessorEnablerReceiver auctionProcessorEnablerReceiver, 
                                                                                                                                          org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler notification service for the given auction house. 
     *
     *  @param  auctionProcessoEnablerReceiver the notification callback 
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no auction house found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          auctionProcessoEnablerReceiver, auctionHouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerNotificationSession getAuctionProcessorEnablerNotificationSessionForAuctionHouse(org.osid.bidding.rules.AuctionProcessorEnablerReceiver auctionProcessoEnablerReceiver, 
                                                                                                                                          org.osid.id.Id auctionHouseId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerNotificationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction processor 
     *  enabler/auction house mappings for auction processor enablers. 
     *
     *  @return an <code> AuctionProcessorEnablerAuctionHouseSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAuctionHouseSession getAuctionProcessorEnablerAuctionHouseSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup auction processor 
     *  enabler/auction house mappings for auction processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerAuctionHouseSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAuctionHouseSession getAuctionProcessorEnablerAuctionHouseSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  processor enablers to auction houses. 
     *
     *  @return an <code> AuctionProcessorEnablerAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAuctionHouseAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAuctionHouseAssignmentSession getAuctionProcessorEnablerAuctionHouseAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning auction 
     *  processor enablers to auction houses. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerAuctionHouseAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerAuctionHouseAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerAuctionHouseAssignmentSession getAuctionProcessorEnablerAuctionHouseAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerAuctionHouseAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction processor 
     *  enabler smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerSmartAuctionHouseSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSmartAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSmartAuctionHouseSession getAuctionProcessorEnablerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage auction processor 
     *  enabler smart auction houses. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerSmartAuctionHouseSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerSmartAuctionHouse() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerSmartAuctionHouseSession getAuctionProcessorEnablerSmartAuctionHouseSession(org.osid.id.Id auctionHouseId, 
                                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerSmartAuctionHouseSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler mapping lookup service. 
     *
     *  @return an <code> AuctionProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleLookupSession getAuctionProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleLookupSession getAuctionProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler mapping lookup service. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleLookupSession getAuctionProcessorEnablerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerRuleLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler mapping lookup service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleLookupSession getAuctionProcessorEnablerRuleLookupSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerRuleLookupSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler assignment service. 
     *
     *  @return an <code> AuctionProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleApplicationSession getAuctionProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleApplicationSession getAuctionProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler assignment service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @return an <code> AuctionProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleApplicationSession getAuctionProcessorEnablerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesManager.getAuctionProcessorEnablerRuleApplicationSessionForAuctionHouse not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the auction 
     *  processor enabler assignment service for the given auction house. 
     *
     *  @param  auctionHouseId the <code> Id </code> of the <code> 
     *          AuctionHouse </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuctionProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> AuctionHouse </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerRuleApplicationSession getAuctionProcessorEnablerRuleApplicationSessionForAuctionHouse(org.osid.id.Id auctionHouseId, 
                                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.bidding.rules.BiddingRulesProxyManager.getAuctionProcessorEnablerRuleApplicationSessionForAuctionHouse not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.auctionConstrainerRecordTypes.clear();
        this.auctionConstrainerRecordTypes.clear();

        this.auctionConstrainerSearchRecordTypes.clear();
        this.auctionConstrainerSearchRecordTypes.clear();

        this.auctionConstrainerEnablerRecordTypes.clear();
        this.auctionConstrainerEnablerRecordTypes.clear();

        this.auctionConstrainerEnablerSearchRecordTypes.clear();
        this.auctionConstrainerEnablerSearchRecordTypes.clear();

        this.auctionProcessorRecordTypes.clear();
        this.auctionProcessorRecordTypes.clear();

        this.auctionProcessorSearchRecordTypes.clear();
        this.auctionProcessorSearchRecordTypes.clear();

        this.auctionProcessorEnablerRecordTypes.clear();
        this.auctionProcessorEnablerRecordTypes.clear();

        this.auctionProcessorEnablerSearchRecordTypes.clear();
        this.auctionProcessorEnablerSearchRecordTypes.clear();

        return;
    }
}
