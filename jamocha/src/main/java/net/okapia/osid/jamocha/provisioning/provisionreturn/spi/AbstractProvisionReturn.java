//
// AbstractProvisionReturn.java
//
//     Defines a ProvisionReturn.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.provisionreturn.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ProvisionReturn</code>.
 */

public abstract class AbstractProvisionReturn
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.provisioning.ProvisionReturn {

    private org.osid.calendaring.DateTime returnDate;
    private org.osid.resource.Resource returner;
    private org.osid.authentication.Agent returningAgent;

    private final java.util.Collection<org.osid.provisioning.records.ProvisionReturnRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the date returned. The return date is the effective end
     *  date of the provision when the return is processed.
     *
     *  @return the date returned 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReturnDate() {
        return (this.returnDate);
    }


    /**
     *  Sets the return date.
     *
     *  @param date a return date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setReturnDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.returnDate = date;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the returner resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReturnerId() {
        return (this.returner.getId());
    }


    /**
     *  Gets the returner. 
     *
     *  @return the returner 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getReturner()
        throws org.osid.OperationFailedException {

        return (this.returner);
    }


    /**
     *  Sets the returner.
     *
     *  @param returner a returner
     *  @throws org.osid.NullArgumentException
     *          <code>returner</code> is <code>null</code>
     */

    protected void setReturner(org.osid.resource.Resource returner) {
        nullarg(returner, "returner");
        this.returner = returner;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the returning agent. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReturningAgentId() {
        return (this.returningAgent.getId());
    }


    /**
     *  Gets the returning agent. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getReturningAgent()
        throws org.osid.OperationFailedException {

        return (this.returningAgent);
    }


    /**
     *  Sets the returning agent.
     *
     *  @param agent a returning agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setReturningAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "returning agent");
        this.returningAgent = agent;
        return;
    }


    /**
     *  Tests if this provisionReturn supports the given record
     *  <code>Type</code>.
     *
     *  @param  provisionReturnRecordType a provision return record type 
     *  @return <code>true</code> if the provisionReturnRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type provisionReturnRecordType) {
        for (org.osid.provisioning.records.ProvisionReturnRecord record : this.records) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ProvisionEeturn</code> record <code>Type</code>.
     *
     *  @param  provisionReturnRecordType the provision return record type 
     *  @return the provision return record 
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(provisionReturnRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionReturnRecord getProvisionReturnRecord(org.osid.type.Type provisionReturnRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.ProvisionReturnRecord record : this.records) {
            if (record.implementsRecordType(provisionReturnRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(provisionReturnRecordType + " is not supported");
    }


    /**
     *  Adds a record to this provision return. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param provisionReturnRecord the provision return record
     *  @param provisionReturnRecordType provision return record type
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturnRecord</code> or
     *          <code>provisionReturnRecordTypeprovisionReturn</code> is
     *          <code>null</code>
     */
            
    protected void addProvisionReturnRecord(org.osid.provisioning.records.ProvisionReturnRecord provisionReturnRecord, 
                                            org.osid.type.Type provisionReturnRecordType) {
        
        nullarg(provisionReturnRecord, "provision return record");
        addRecordType(provisionReturnRecordType);
        this.records.add(provisionReturnRecord);
        
        return;
    }
}
