//
// AbstractCredentialEntrySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.credentialentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCredentialEntrySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.chronicle.CredentialEntrySearchResults {

    private org.osid.course.chronicle.CredentialEntryList credentialEntries;
    private final org.osid.course.chronicle.CredentialEntryQueryInspector inspector;
    private final java.util.Collection<org.osid.course.chronicle.records.CredentialEntrySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCredentialEntrySearchResults.
     *
     *  @param credentialEntries the result set
     *  @param credentialEntryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>credentialEntries</code>
     *          or <code>credentialEntryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCredentialEntrySearchResults(org.osid.course.chronicle.CredentialEntryList credentialEntries,
                                            org.osid.course.chronicle.CredentialEntryQueryInspector credentialEntryQueryInspector) {
        nullarg(credentialEntries, "credential entries");
        nullarg(credentialEntryQueryInspector, "credential entry query inspectpr");

        this.credentialEntries = credentialEntries;
        this.inspector = credentialEntryQueryInspector;

        return;
    }


    /**
     *  Gets the credential entry list resulting from a search.
     *
     *  @return a credential entry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntries() {
        if (this.credentialEntries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.chronicle.CredentialEntryList credentialEntries = this.credentialEntries;
        this.credentialEntries = null;
	return (credentialEntries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.chronicle.CredentialEntryQueryInspector getCredentialEntryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  credential entry search record <code> Type. </code> This method must
     *  be used to retrieve a credentialEntry implementing the requested
     *  record.
     *
     *  @param credentialEntrySearchRecordType a credentialEntry search 
     *         record type 
     *  @return the credential entry search
     *  @throws org.osid.NullArgumentException
     *          <code>credentialEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(credentialEntrySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntrySearchResultsRecord getCredentialEntrySearchResultsRecord(org.osid.type.Type credentialEntrySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.chronicle.records.CredentialEntrySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(credentialEntrySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(credentialEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record credential entry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCredentialEntryRecord(org.osid.course.chronicle.records.CredentialEntrySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "credential entry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
