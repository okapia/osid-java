//
// AbstractPoolProcessor.java
//
//     Defines a PoolProcessor.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>PoolProcessor</code>.
 */

public abstract class AbstractPoolProcessor
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessor
    implements org.osid.provisioning.rules.PoolProcessor {

    private boolean allocatesByLeastUse  = false;
    private boolean allocatesByMostUse   = false;
    private boolean allocatesByLeastCost = false;
    private boolean allocatesByMostCost  = false;

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if allocations balance the usage by preferring the least used 
     *  provisionables in the pool. 
     *
     *  @return <code> true </code> if the least used provisionables are 
     *          preferred, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean allocatesByLeastUse() {
        return (this.allocatesByLeastUse);
    }


    /**
     *  Sets the allocates by least use.
     *
     *  @param allocation <code> true </code> if the least used
     *          provisionables are preferred, <code> false </code>
     *          otherwise
     */

    protected void setAllocatesByLeastUse(boolean allocation) {
        this.allocatesByLeastUse = allocation;
        return;
    }


    /**
     *  Tests if allocations prefer the most used provisionables in the pool. 
     *
     *  @return <code> true </code> if the most used provisionables
     *          are preferred, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean allocatesByMostUse() {
        return (this.allocatesByMostUse);
    }


    /**
     *  Sets the allocates by most use.
     *
     *  @param allocation <code> true </code> if the most used
     *          provisionables are preferred, <code> false </code>
     *          otherwise
     */

    protected void setAllocatesByMostUse(boolean allocation) {
        this.allocatesByMostUse = allocation;
        return;
    }


    /**
     *  Tests if allocations prefer the cheapest provisionables. 
     *
     *  @return <code> true </code> if the cheapest provisionables are 
     *          preferred, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean allocatesByLeastCost() {
        return (this.allocatesByLeastCost);
    }


    /**
     *  Sets the allocates by least cost.
     *
     *  @param allocation <code> true </code> if the cheapest
     *          provisionables are preferred, <code> false </code>
     *          otherwise
     */

    protected void setAllocatesByLeastCost(boolean allocation) {
        this.allocatesByLeastCost = allocation;
        return;
    }


    /**
     *  Tests if allocations prefer the most expensive provisionables. 
     *
     *  @return <code> true </code> if the most expensive provisionables are 
     *          prefsrred, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean allocatesByMostCost() {
        return (this.allocatesByMostCost);
    }


    /**
     *  Sets the allocates by most cost.
     *
     *  @param allocation <code> true </code> if the most expensive
     *         provisionables are prefsrred, <code> false </code>
     *         otherwise
     */

    protected void setAllocatesByMostCost(boolean allocation) {
        this.allocatesByMostCost = allocation;
        return;
    }


    /**
     *  Tests if this poolProcessor supports the given record
     *  <code>Type</code>.
     *
     *  @param  poolProcessorRecordType a pool processor record type 
     *  @return <code>true</code> if the poolProcessorRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type poolProcessorRecordType) {
        for (org.osid.provisioning.rules.records.PoolProcessorRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>PoolProcessor</code> record <code>Type</code>.
     *
     *  @param  poolProcessorRecordType the pool processor record type 
     *  @return the pool processor record 
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(poolProcessorRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorRecord getPoolProcessorRecord(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this pool processor. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param poolProcessorRecord the pool processor record
     *  @param poolProcessorRecordType pool processor record type
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorRecord</code> or
     *          <code>poolProcessorRecordTypepoolProcessor</code> is
     *          <code>null</code>
     */
            
    protected void addPoolProcessorRecord(org.osid.provisioning.rules.records.PoolProcessorRecord poolProcessorRecord, 
                                          org.osid.type.Type poolProcessorRecordType) {

        nullarg(poolProcessorRecord, "pool processor record");
        addRecordType(poolProcessorRecordType);
        this.records.add(poolProcessorRecord);
        
        return;
    }
}
