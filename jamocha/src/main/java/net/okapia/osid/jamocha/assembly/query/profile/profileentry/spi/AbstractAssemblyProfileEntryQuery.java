//
// AbstractAssemblyProfileEntryQuery.java
//
//     A ProfileEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.profile.profileentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProfileEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyProfileEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.profile.ProfileEntryQuery,
               org.osid.profile.ProfileEntryQueryInspector,
               org.osid.profile.ProfileEntrySearchOrder {

    private final java.util.Collection<org.osid.profile.records.ProfileEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.profile.records.ProfileEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.profile.records.ProfileEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProfileEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProfileEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches implicit profile entries. 
     *
     *  @param  match <code> true </code> ito match implicit profile entries, 
     *          <code> false </code> to match implciit profile entries 
     */

    @OSID @Override
    public void matchImplicit(boolean match) {
        getAssembler().addBooleanTerm(getImplicitColumn(), match);
        return;
    }


    /**
     *  Clears the implicit profile entries query terms. 
     */

    @OSID @Override
    public void clearImplicitTerms() {
        getAssembler().clearTerms(getImplicitColumn());
        return;
    }


    /**
     *  Gets the implicit profile entries query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getImplicitTerms() {
        return (getAssembler().getBooleanTerms(getImplicitColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  implicit status.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByImplicit(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getImplicitColumn(), style);
        return;
    }


    /**
     *  Gets the ImplicitProfileEntries column name.
     *
     * @return the column name
     */

    protected String getImplicitColumn() {
        return ("implicit");
    }


    /**
     *  Adds an <code> Id </code> to match an explicit or implicitly related 
     *  profile entries depending on <code> matchExplicitProfileEntries(). 
     *  </code> Multiple <code> Ids </code> can be added to perform a boolean 
     *  <code> OR </code> among them. 
     *
     *  @param  id <code> Id </code> to match 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRelatedProfileEntryId(org.osid.id.Id id, boolean match) {
        getAssembler().addIdTerm(getRelatedProfileEntryIdColumn(), id, match);
        return;
    }


    /**
     *  Clears the related profile entry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRelatedProfileEntryIdTerms() {
        getAssembler().clearTerms(getRelatedProfileEntryIdColumn());
        return;
    }


    /**
     *  Gets the related profile entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelatedProfileEntryIdTerms() {
        return (getAssembler().getIdTerms(getRelatedProfileEntryIdColumn()));
    }


    /**
     *  Gets the RelatedProfileEntryId column name.
     *
     * @return the column name
     */

    protected String getRelatedProfileEntryIdColumn() {
        return ("related_profile_entry_id");
    }


    /**
     *  Tests if a <code> ProfileEntryQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelatedProfileEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a profile entry. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile entry query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelatedProfileEntryQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQuery getRelatedProfileEntryQuery() {
        throw new org.osid.UnimplementedException("supportsRelatedProfileEntryQuery() is false");
    }


    /**
     *  Clears the related profile entry query terms. 
     */

    @OSID @Override
    public void clearRelatedProfileEntryTerms() {
        getAssembler().clearTerms(getRelatedProfileEntryColumn());
        return;
    }


    /**
     *  Gets the related profile entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQueryInspector[] getRelatedProfileEntryTerms() {
        return (new org.osid.profile.ProfileEntryQueryInspector[0]);
    }


    /**
     *  Gets the RelatedProfileEntry column name.
     *
     * @return the column name
     */

    protected String getRelatedProfileEntryColumn() {
        return ("related_profile_entry");
    }


    /**
     *  Matches the resource identified by the given <code> Id. </code> 
     *
     *  @param  resourceId the Id of the <code> Resource </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the resource query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> ResourceQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a <code> Resource </code> is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Matches the agent identified by the given <code> Id. </code> 
     *
     *  @param  agentId the Id of the <code> Agent </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAgentColumn(), style);
        return;
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the agent query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> AgentQuery </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an <code> Agent </code> is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Matches the profile item identified by the given <code> Id. </code> 
     *
     *  @param  profileItemId the Id of the <code> ProfileItem </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileItemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileItemId(org.osid.id.Id profileItemId, boolean match) {
        getAssembler().addIdTerm(getProfileItemIdColumn(), profileItemId, match);
        return;
    }


    /**
     *  Clears the profile item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProfileItemIdTerms() {
        getAssembler().clearTerms(getProfileItemIdColumn());
        return;
    }


    /**
     *  Gets the profile item <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileItemIdTerms() {
        return (getAssembler().getIdTerms(getProfileItemIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the active 
     *  status. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProfileItem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProfileItemColumn(), style);
        return;
    }


    /**
     *  Gets the ProfileItemId column name.
     *
     * @return the column name
     */

    protected String getProfileItemIdColumn() {
        return ("profile_item_id");
    }


    /**
     *  Tests if a <code> ProfileItemQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile item query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemQuery() {
        return (false);
    }


    /**
     *  Gets the profile item query. 
     *
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @return the <code> FunctinQuery </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQuery getProfileItemQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsProfileItemQuery() is false");
    }


    /**
     *  Clears the profile item terms. 
     */

    @OSID @Override
    public void clearProfileItemTerms() {
        getAssembler().clearTerms(getProfileItemColumn());
        return;
    }


    /**
     *  Gets the profile item query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemQueryInspector[] getProfileItemTerms() {
        return (new org.osid.profile.ProfileItemQueryInspector[0]);
    }


    /**
     *  Tests if a <code> ProfileItem </code> is available. 
     *
     *  @return <code> true </code> if a profile item search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileItemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the profil eitem search order. 
     *
     *  @return the profile item search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileItemSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemSearchOrder getProfileItemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProfileItemSearchOrder() is false");
    }


    /**
     *  Gets the ProfileItem column name.
     *
     * @return the column name
     */

    protected String getProfileItemColumn() {
        return ("profile_item");
    }


    /**
     *  Sets the profile <code> Id </code> for this query. 
     *
     *  @param  profileId a profile <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> profileId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProfileId(org.osid.id.Id profileId, boolean match) {
        getAssembler().addIdTerm(getProfileIdColumn(), profileId, match);
        return;
    }


    /**
     *  Clears the profile <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProfileIdTerms() {
        getAssembler().clearTerms(getProfileIdColumn());
        return;
    }


    /**
     *  Gets the profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileIdTerms() {
        return (getAssembler().getIdTerms(getProfileIdColumn()));
    }


    /**
     *  Gets the ProfileId column name.
     *
     * @return the column name
     */

    protected String getProfileIdColumn() {
        return ("profile_id");
    }


    /**
     *  Tests if a <code> ProfileQuery </code> is available. 
     *
     *  @return <code> true </code> if a profile query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileQuery() {
        return (false);
    }


    /**
     *  Gets the query interface for a profile. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the profile query 
     *  @throws org.osid.UnimplementedException <code> supportsProfileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileQuery getProfileQuery() {
        throw new org.osid.UnimplementedException("supportsProfileQuery() is false");
    }


    /**
     *  Clears the profile entry query terms. 
     */

    @OSID @Override
    public void clearProfileTerms() {
        getAssembler().clearTerms(getProfileColumn());
        return;
    }


    /**
     *  Gets the profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }


    /**
     *  Gets the Profile column name.
     *
     * @return the column name
     */

    protected String getProfileColumn() {
        return ("profile");
    }


    /**
     *  Tests if this profileEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  profileEntryRecordType a profile entry record type 
     *  @return <code>true</code> if the profileEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type profileEntryRecordType) {
        for (org.osid.profile.records.ProfileEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  profileEntryRecordType the profile entry record type 
     *  @return the profile entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntryQueryRecord getProfileEntryQueryRecord(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  profileEntryRecordType the profile entry record type 
     *  @return the profile entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntryQueryInspectorRecord getProfileEntryQueryInspectorRecord(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param profileEntryRecordType the profile entry record type
     *  @return the profile entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.records.ProfileEntrySearchOrderRecord getProfileEntrySearchOrderRecord(org.osid.type.Type profileEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.records.ProfileEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(profileEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this profile entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param profileEntryQueryRecord the profile entry query record
     *  @param profileEntryQueryInspectorRecord the profile entry query inspector
     *         record
     *  @param profileEntrySearchOrderRecord the profile entry search order record
     *  @param profileEntryRecordType profile entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryQueryRecord</code>,
     *          <code>profileEntryQueryInspectorRecord</code>,
     *          <code>profileEntrySearchOrderRecord</code> or
     *          <code>profileEntryRecordTypeprofileEntry</code> is
     *          <code>null</code>
     */
            
    protected void addProfileEntryRecords(org.osid.profile.records.ProfileEntryQueryRecord profileEntryQueryRecord, 
                                      org.osid.profile.records.ProfileEntryQueryInspectorRecord profileEntryQueryInspectorRecord, 
                                      org.osid.profile.records.ProfileEntrySearchOrderRecord profileEntrySearchOrderRecord, 
                                      org.osid.type.Type profileEntryRecordType) {

        addRecordType(profileEntryRecordType);

        nullarg(profileEntryQueryRecord, "profile entry query record");
        nullarg(profileEntryQueryInspectorRecord, "profile entry query inspector record");
        nullarg(profileEntrySearchOrderRecord, "profile entry search odrer record");

        this.queryRecords.add(profileEntryQueryRecord);
        this.queryInspectorRecords.add(profileEntryQueryInspectorRecord);
        this.searchOrderRecords.add(profileEntrySearchOrderRecord);
        
        return;
    }
}
