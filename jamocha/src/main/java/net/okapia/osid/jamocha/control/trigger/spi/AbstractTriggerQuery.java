//
// AbstractTriggerQuery.java
//
//     A template for making a Trigger Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for triggers.
 */

public abstract class AbstractTriggerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.control.TriggerQuery {

    private final java.util.Collection<org.osid.control.records.TriggerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the controller <code> Id </code> for this query. 
     *
     *  @param  controllerId a controller <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> controllerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchControllerId(org.osid.id.Id controllerId, boolean match) {
        return;
    }


    /**
     *  Clears the controller <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearControllerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ControllerQuery </code> is available. 
     *
     *  @return <code> true </code> if a controller query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a controller. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the controller query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuery getControllerQuery() {
        throw new org.osid.UnimplementedException("supportsControllerQuery() is false");
    }


    /**
     *  Clears the controller query terms. 
     */

    @OSID @Override
    public void clearControllerTerms() {
        return;
    }


    /**
     *  Matches triggers listening for ON events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchTurnedOn(boolean match) {
        return;
    }


    /**
     *  Clears the ON event query terms. 
     */

    @OSID @Override
    public void clearTurnedOnTerms() {
        return;
    }


    /**
     *  Matches triggers listening for OFF events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchTurnedOff(boolean match) {
        return;
    }


    /**
     *  Clears the OFF event query terms. 
     */

    @OSID @Override
    public void clearTurnedOffTerms() {
        return;
    }


    /**
     *  Matches triggers listening for changed amount events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchChangedVariableAmount(boolean match) {
        return;
    }


    /**
     *  Clears the changed amount event query terms. 
     */

    @OSID @Override
    public void clearChangedVariableAmountTerms() {
        return;
    }


    /**
     *  Matches triggers listening for exceeds amount events between the given 
     *  values inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchExceedsVariableAmount(java.math.BigDecimal start, 
                                           java.math.BigDecimal end, 
                                           boolean match) {
        return;
    }


    /**
     *  Matches triggers with any exceeds variable amount. 
     *
     *  @param  match <code> true </code> to match triggers with any exceeds 
     *          amount, <code> false </code> to match triggers with no exceeds 
     *          amount 
     */

    @OSID @Override
    public void matchAnyExceedsVariableAmount(boolean match) {
        return;
    }


    /**
     *  Clears the exceeds amount event query terms. 
     */

    @OSID @Override
    public void clearExceedsVariableAmountTerms() {
        return;
    }


    /**
     *  Matches triggers listening for deceeds amount events between the given 
     *  values inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchDeceedsVariableAmount(java.math.BigDecimal start, 
                                           java.math.BigDecimal end, 
                                           boolean match) {
        return;
    }


    /**
     *  Matches triggers with any deceeds variable amount. 
     *
     *  @param  match <code> true </code> to match triggers with any deceeds 
     *          amount, <code> false </code> to match triggers with no deceeds 
     *          amount 
     */

    @OSID @Override
    public void matchAnyDeceedsVariableAmount(boolean match) {
        return;
    }


    /**
     *  Clears the deceeds amount event query terms. 
     */

    @OSID @Override
    public void clearDeceedsVariableAmountTerms() {
        return;
    }


    /**
     *  Matches triggers listening for changed state events. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchChangedDiscreetState(boolean match) {
        return;
    }


    /**
     *  Clears the changed state event query terms. 
     */

    @OSID @Override
    public void clearChangedDiscreetStateTerms() {
        return;
    }


    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId a state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDiscreetStateId(org.osid.id.Id stateId, boolean match) {
        return;
    }


    /**
     *  Clears the discreet state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a discreet state query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a discreet state. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the discreet state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDiscreetStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getDiscreetStateQuery() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateQuery() is false");
    }


    /**
     *  Matches triggers with any discreet state. 
     *
     *  @param  match <code> true </code> to match triggers with any discreet 
     *          state, <code> false </code> to match triggers with no discreet 
     *          states 
     */

    @OSID @Override
    public void matchAnyDiscreetState(boolean match) {
        return;
    }


    /**
     *  Clears the discreet state query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateTerms() {
        return;
    }


    /**
     *  Sets the action group <code> Id </code> for this query. 
     *
     *  @param  actionGroupId an action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActionGroupId(org.osid.id.Id actionGroupId, boolean match) {
        return;
    }


    /**
     *  Clears the action group <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActionGroupIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActionGroupQuery </code> is available. 
     *
     *  @return <code> true </code> if a action group query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the action group query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuery getActionGroupQuery() {
        throw new org.osid.UnimplementedException("supportsActionGroupQuery() is false");
    }


    /**
     *  Matches triggers with any action group. 
     *
     *  @param  match <code> true </code> to match triggers with any action 
     *          group,, <code> false </code> to match triggers with no action 
     *          groups 
     */

    @OSID @Override
    public void matchAnyActionGroup(boolean match) {
        return;
    }


    /**
     *  Clears the action group query terms. 
     */

    @OSID @Override
    public void clearActionGroupTerms() {
        return;
    }


    /**
     *  Sets the scene <code> Id </code> for this query. 
     *
     *  @param  sceneId a scene <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sceneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSceneId(org.osid.id.Id sceneId, boolean match) {
        return;
    }


    /**
     *  Clears the scene <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSceneIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SceneQuery </code> is available. 
     *
     *  @return <code> true </code> if a scene query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneQuery() {
        return (false);
    }


    /**
     *  Gets the query for a scene. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the scene query 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuery getSceneQuery() {
        throw new org.osid.UnimplementedException("supportsSceneQuery() is false");
    }


    /**
     *  Matches triggers with any scene. 
     *
     *  @param  match <code> true </code> to match triggers with any scene, 
     *          <code> false </code> to match triggers with no scenes 
     */

    @OSID @Override
    public void matchAnyScene(boolean match) {
        return;
    }


    /**
     *  Clears the scene query terms. 
     */

    @OSID @Override
    public void clearSceneTerms() {
        return;
    }


    /**
     *  Sets the setting <code> Id </code> for this query. 
     *
     *  @param  settingId a setting <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> settingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettingId(org.osid.id.Id settingId, boolean match) {
        return;
    }


    /**
     *  Clears the setting <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSettingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SettingQuery </code> is available. 
     *
     *  @return <code> true </code> if a setting query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a setting. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the setting query 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuery getSettingQuery() {
        throw new org.osid.UnimplementedException("supportsSettingQuery() is false");
    }


    /**
     *  Matches triggers with any setting. 
     *
     *  @param  match <code> true </code> to match triggers with any setting, 
     *          <code> false </code> to match triggers with no settings 
     */

    @OSID @Override
    public void matchAnySetting(boolean match) {
        return;
    }


    /**
     *  Clears the setting query terms. 
     */

    @OSID @Override
    public void clearSettingTerms() {
        return;
    }


    /**
     *  Sets the system <code> Id </code> for this query to match triggers 
     *  assigned to systems. 
     *
     *  @param  triggerId the trigger <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> triggerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id triggerId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given trigger query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a trigger implementing the requested record.
     *
     *  @param triggerRecordType a trigger record type
     *  @return the trigger query record
     *  @throws org.osid.NullArgumentException
     *          <code>triggerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.TriggerQueryRecord getTriggerQueryRecord(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.TriggerQueryRecord record : this.records) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this trigger query. 
     *
     *  @param triggerQueryRecord trigger query record
     *  @param triggerRecordType trigger record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTriggerQueryRecord(org.osid.control.records.TriggerQueryRecord triggerQueryRecord, 
                                          org.osid.type.Type triggerRecordType) {

        addRecordType(triggerRecordType);
        nullarg(triggerQueryRecord, "trigger query record");
        this.records.add(triggerQueryRecord);        
        return;
    }
}
