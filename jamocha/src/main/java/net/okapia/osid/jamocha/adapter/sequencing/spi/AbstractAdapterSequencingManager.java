//
// AbstractSequencingManager.java
//
//     An adapter for a SequencingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.sequencing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a SequencingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterSequencingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.sequencing.SequencingManager>
    implements org.osid.sequencing.SequencingManager {


    /**
     *  Constructs a new {@code AbstractAdapterSequencingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterSequencingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterSequencingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterSequencingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any action group federation is exposed. Federation is exposed 
     *  when a specific action group may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of action groups appears as a single action group. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up elements is supported. 
     *
     *  @return <code> true </code> if element lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsElementLookup() {
        return (getAdapteeManager().supportsElementLookup());
    }


    /**
     *  Tests if managing elements is supported. 
     *
     *  @return <code> true </code> if element management is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsElementAdmin() {
        return (getAdapteeManager().supportsElementAdmin());
    }


    /**
     *  Tests if sequencing elements is supported. 
     *
     *  @return <code> true </code> if element sequencing is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequencing() {
        return (getAdapteeManager().supportsSequencing());
    }


    /**
     *  Tests if looking up chains is supported. 
     *
     *  @return <code> true </code> if chain lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainLookup() {
        return (getAdapteeManager().supportsChainLookup());
    }


    /**
     *  Tests if querying chains is supported. 
     *
     *  @return <code> true </code> if chain query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainQuery() {
        return (getAdapteeManager().supportsChainQuery());
    }


    /**
     *  Tests if searching chains is supported. 
     *
     *  @return <code> true </code> if chain search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainSearch() {
        return (getAdapteeManager().supportsChainSearch());
    }


    /**
     *  Tests if chain <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if chain administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainAdmin() {
        return (getAdapteeManager().supportsChainAdmin());
    }


    /**
     *  Tests if a chain <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if chain notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainNotification() {
        return (getAdapteeManager().supportsChainNotification());
    }


    /**
     *  Tests if a chain antimatroid lookup service is supported. 
     *
     *  @return <code> true </code> if a chain antimatroid lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainAntimatroid() {
        return (getAdapteeManager().supportsChainAntimatroid());
    }


    /**
     *  Tests if a chain antimatroid assignment service is supported. 
     *
     *  @return <code> true </code> if a chain to antimatroid assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainAntimatroidAssignment() {
        return (getAdapteeManager().supportsChainAntimatroidAssignment());
    }


    /**
     *  Tests if a chain smart antimatroid service is supported. 
     *
     *  @return <code> true </code> if a chain smart antimatroid service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChainSmartAntimatroid() {
        return (getAdapteeManager().supportsChainSmartAntimatroid());
    }


    /**
     *  Tests if looking up antimatroids is supported. 
     *
     *  @return <code> true </code> if antimatroid lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidLookup() {
        return (getAdapteeManager().supportsAntimatroidLookup());
    }


    /**
     *  Tests if querying antimatroids is supported. 
     *
     *  @return <code> true </code> if an antimatroid query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidQuery() {
        return (getAdapteeManager().supportsAntimatroidQuery());
    }


    /**
     *  Tests if searching antimatroids is supported. 
     *
     *  @return <code> true </code> if antimatroid search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidSearch() {
        return (getAdapteeManager().supportsAntimatroidSearch());
    }


    /**
     *  Tests if antimatroid administrative service is supported. 
     *
     *  @return <code> true </code> if antimatroid administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidAdmin() {
        return (getAdapteeManager().supportsAntimatroidAdmin());
    }


    /**
     *  Tests if an antimatroid <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if antimatroid notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidNotification() {
        return (getAdapteeManager().supportsAntimatroidNotification());
    }


    /**
     *  Tests for the availability of an antimatroid hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if antimatroid hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidHierarchy() {
        return (getAdapteeManager().supportsAntimatroidHierarchy());
    }


    /**
     *  Tests for the availability of an antimatroid hierarchy design service. 
     *
     *  @return <code> true </code> if antimatroid hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidHierarchyDesign() {
        return (getAdapteeManager().supportsAntimatroidHierarchyDesign());
    }


    /**
     *  Gets the supported <code> Chain </code> record types. 
     *
     *  @return a list containing the supported <code> Chain </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChainRecordTypes() {
        return (getAdapteeManager().getChainRecordTypes());
    }


    /**
     *  Tests if the given <code> Chain </code> record type is supported. 
     *
     *  @param  chainRecordType a <code> Type </code> indicating a <code> 
     *          Chain </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> chainRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChainRecordType(org.osid.type.Type chainRecordType) {
        return (getAdapteeManager().supportsChainRecordType(chainRecordType));
    }


    /**
     *  Gets the supported <code> Chain </code> search types. 
     *
     *  @return a list containing the supported <code> Chain </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChainSearchRecordTypes() {
        return (getAdapteeManager().getChainSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Chain </code> search type is supported. 
     *
     *  @param  chainSearchRecordType a <code> Type </code> indicating a 
     *          <code> Chain </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> chainSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChainSearchRecordType(org.osid.type.Type chainSearchRecordType) {
        return (getAdapteeManager().supportsChainSearchRecordType(chainSearchRecordType));
    }


    /**
     *  Gets the supported <code> Antimatroid </code> record types. 
     *
     *  @return a list containing the supported <code> Antimatroid </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAntimatroidRecordTypes() {
        return (getAdapteeManager().getAntimatroidRecordTypes());
    }


    /**
     *  Tests if the given <code> Antimatroid </code> record type is 
     *  supported. 
     *
     *  @param  antimatroidRecordType a <code> Type </code> indicating an 
     *          <code> Antimatroid </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> antimatroidRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAntimatroidRecordType(org.osid.type.Type antimatroidRecordType) {
        return (getAdapteeManager().supportsAntimatroidRecordType(antimatroidRecordType));
    }


    /**
     *  Gets the supported <code> Antimatroid </code> search record types. 
     *
     *  @return a list containing the supported <code> Antimatroid </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAntimatroidSearchRecordTypes() {
        return (getAdapteeManager().getAntimatroidSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Antimatroid </code> search record type is 
     *  supported. 
     *
     *  @param  antimatroidSearchRecordType a <code> Type </code> indicating 
     *          an <code> Antimatroid </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          antimatroidSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAntimatroidSearchRecordType(org.osid.type.Type antimatroidSearchRecordType) {
        return (getAdapteeManager().supportsAntimatroidSearchRecordType(antimatroidSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element lookup 
     *  service. 
     *
     *  @return an <code> ElementLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsElementLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ElementLookupSession getElementLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getElementLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element lookup 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the antimatroid 
     *  @return an <code> ElementLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Antimatroid </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsElementLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ElementLookupSession getElementLookupSessionForAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getElementLookupSessionForAntimatroid(antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element admin 
     *  service. 
     *
     *  @return an <code> ElementAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsElementAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ElementAdminSession getElementAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getElementAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element admin 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the antimatroid 
     *  @return an <code> ElementAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Antimatroid </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsElementAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ElementAdminSession getElementAdminSessionForAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getElementAdminSessionForAntimatroid(antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element 
     *  sequencing service. 
     *
     *  @return a <code> SequencingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSequencingn() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.SequencingSession getSequencingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSequencingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the element 
     *  sequencing service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the antimatroid 
     *  @return a <code> SequencingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Antimatroid </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSequencing() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.SequencingSession getSequencingSessionForAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSequencingSessionForAntimatroid(antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain lookup 
     *  service. 
     *
     *  @return a <code> ChainLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainLookupSession getChainLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain lookup 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the antimatroid 
     *  @return a <code> ChainLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Antimatroid </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainLookupSession getChainLookupSessionForAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainLookupSessionForAntimatroid(antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain query 
     *  service. 
     *
     *  @return a <code> ChainQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainQuerySession getChainQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain query 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the <code> Antimatroid 
     *          </code> 
     *  @return a <code> ChainQuerySession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainQuerySession getChainQuerySessionForAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainQuerySessionForAntimatroid(antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain search 
     *  service. 
     *
     *  @return a <code> ChainSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainSearchSession getChainSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain search 
     *  service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the <code> Antimatroid 
     *          </code> 
     *  @return a <code> ChainSearchSession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainSearchSession getChainSearchSessionForAntimatroid(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainSearchSessionForAntimatroid(antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain 
     *  administration service. 
     *
     *  @return a <code> ChainAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainAdminSession getChainAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain 
     *  administration service for the given antimatroid. 
     *
     *  @param  antimatroidId the <code> Id </code> of the <code> Dostributor 
     *          </code> 
     *  @return a <code> ChainAdminSession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsChainAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainAdminSession getChainAdminSessionForInput(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainAdminSessionForInput(antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain 
     *  notification service. 
     *
     *  @param  chainReceiver the notification callback 
     *  @return a <code> ChainNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> chainReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainNotificationSession getChainNotificationSession(org.osid.sequencing.ChainReceiver chainReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainNotificationSession(chainReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the chain 
     *  notification service for the given antimatroid. 
     *
     *  @param  chainReceiver the notification callback 
     *  @param  antimatroidId the <code> Id </code> of the <code> Antimatroid 
     *          </code> 
     *  @return a <code> ChainNotificationSession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> chainReceiver </code> or 
     *          <code> antimatroidId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainNotificationSession getChainNotificationSessionForAntimatroid(org.osid.sequencing.ChainReceiver chainReceiver, 
                                                                                                  org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainNotificationSessionForAntimatroid(chainReceiver, antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup chain/antimatroid 
     *  mappings. 
     *
     *  @return a <code> ChainAntimatroidSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainAntimatroid() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainAntimatroidSession getChainAntimatroidSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainAntimatroidSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning chains 
     *  to antimatroids. 
     *
     *  @return a <code> ChainAntimatroidAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainAntimatroidAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainAntimatroidAssignmentSession getChainAntimatroidAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getChainAntimatroidAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  antimatroids. 
     *
     *  @param  antimatroidId the <code> Id </code> of the <code> Antimatroid 
     *          </code> 
     *  @return a <code> ChainSmartAntimatroidSession </code> 
     *  @throws org.osid.NotFoundException no antimatroid found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChainSmartAntimatroid() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.ChainSmartAntimatroidSession getChainSmartAntimatroidSession(org.osid.id.Id antimatroidId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getChainSmartAntimatroidSession(antimatroidId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  lookup service. 
     *
     *  @return a <code> AntimatroidLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidLookupSession getAntimatroidLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  query service. 
     *
     *  @return an <code> AntimatroidQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQuerySession getAntimatroidQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  search service. 
     *
     *  @return an <code> AntimatroidSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidSearchSession getAntimatroidSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  administrative service. 
     *
     *  @return an <code> AntimatroidAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidAdminSession getAntimatroidAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  notification service. 
     *
     *  @param  antimatroidReceiver the notification callback 
     *  @return an <code> AntimatroidNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> antimatroidReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidNotificationSession getAntimatroidNotificationSession(org.osid.sequencing.AntimatroidReceiver antimatroidReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidNotificationSession(antimatroidReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  hierarchy service. 
     *
     *  @return an <code> AntimatroidHierarchySession </code> for antimatroids 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidHierarchySession getAntimatroidHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the antimatroid 
     *  hierarchy design service. 
     *
     *  @return an <code> HierarchyDesignSession </code> for antimatroids 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidHierarchyDesignSession getAntimatroidHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAntimatroidHierarchyDesignSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
