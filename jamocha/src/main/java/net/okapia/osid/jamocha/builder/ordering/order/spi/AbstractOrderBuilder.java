//
// AbstractOrder.java
//
//     Defines an Order builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.order.spi;


/**
 *  Defines an <code>Order</code> builder.
 */

public abstract class AbstractOrderBuilder<T extends AbstractOrderBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.ordering.order.OrderMiter order;


    /**
     *  Constructs a new <code>AbstractOrderBuilder</code>.
     *
     *  @param order the order to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractOrderBuilder(net.okapia.osid.jamocha.builder.ordering.order.OrderMiter order) {
        super(order);
        this.order = order;
        return;
    }


    /**
     *  Builds the order.
     *
     *  @return the new order
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.ordering.Order build() {
        (new net.okapia.osid.jamocha.builder.validator.ordering.order.OrderValidator(getValidations())).validate(this.order);
        return (new net.okapia.osid.jamocha.builder.ordering.order.ImmutableOrder(this.order));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the order miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.ordering.order.OrderMiter getMiter() {
        return (this.order);
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>customer</code> is <code>null</code>
     */

    public T customer(org.osid.resource.Resource customer) {
        getMiter().setCustomer(customer);
        return (self());
    }


    /**
     *  Adds an item.
     *
     *  @param item an item
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    public T item(org.osid.ordering.Item item) {
        getMiter().addItem(item);
        return (self());
    }


    /**
     *  Sets all the items.
     *
     *  @param items a collection of items
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>items</code> is
     *          <code>null</code>
     */

    public T items(java.util.Collection<org.osid.ordering.Item> items) {
        getMiter().setItems(items);
        return (self());
    }


    /**
     *  Sets the total cost.
     *
     *  @param cost a total cost
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public T totalCost(org.osid.financials.Currency cost) {
        getMiter().setTotalCost(cost);
        return (self());
    }


    /**
     *  Sets the atomic flag.
     *
     *  @return the builder
     */

    public T atomic(boolean atomic) {
        getMiter().setAtomic(true);
        return (self());
    }


    /**
     *  Unsets the atomic flag.
     *
     *  @return the builder
     */

    public T molecular(boolean atomic) {
        getMiter().setAtomic(false);
        return (self());
    }


    /**
     *  Sets the submit date.
     *
     *  @param date a submit date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T submitDate(org.osid.calendaring.DateTime date) {
        getMiter().setSubmitDate(date);
        return (self());
    }


    /**
     *  Sets the submitter.
     *
     *  @param resource the submitter
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T submitter(org.osid.resource.Resource resource) {
        getMiter().setSubmitter(resource);
        return (self());
    }


    /**
     *  Sets the submitting agent.
     *
     *  @param agent the submitting agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T submittingAgent(org.osid.authentication.Agent agent) {
        getMiter().setSubmittingAgent(agent);
        return (self());
    }


    /**
     *  Sets the closed date.
     *
     *  @param date a closed date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T closedDate(org.osid.calendaring.DateTime date) {
        getMiter().setClosedDate(date);
        return (self());
    }


    /**
     *  Sets the closer.
     *
     *  @param resource the closer
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code> is
     *          <code>null</code>
     */

    public T closer(org.osid.resource.Resource resource) {
        getMiter().setCloser(resource);
        return (self());
    }


    /**
     *  Sets the closing agent.
     *
     *  @param agent the closing agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public T closingAgent(org.osid.authentication.Agent agent) {
        getMiter().setClosingAgent(agent);
        return (self());
    }


    /**
     *  Adds an Order record.
     *
     *  @param record an order record
     *  @param recordType the type of order record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.ordering.records.OrderRecord record, org.osid.type.Type recordType) {
        getMiter().addOrderRecord(record, recordType);
        return (self());
    }
}       


