//
// AbstractAssemblyActionEnablerQuery.java
//
//     An ActionEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.rules.actionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActionEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyActionEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.control.rules.ActionEnablerQuery,
               org.osid.control.rules.ActionEnablerQueryInspector,
               org.osid.control.rules.ActionEnablerSearchOrder {

    private final java.util.Collection<org.osid.control.rules.records.ActionEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.ActionEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.rules.records.ActionEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActionEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActionEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the action. 
     *
     *  @param  actionId the action <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledActionId(org.osid.id.Id actionId, boolean match) {
        getAssembler().addIdTerm(getRuledActionIdColumn(), actionId, match);
        return;
    }


    /**
     *  Clears the action <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledActionIdTerms() {
        getAssembler().clearTerms(getRuledActionIdColumn());
        return;
    }


    /**
     *  Gets the action <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledActionIdTerms() {
        return (getAssembler().getIdTerms(getRuledActionIdColumn()));
    }


    /**
     *  Gets the RuledActionId column name.
     *
     * @return the column name
     */

    protected String getRuledActionIdColumn() {
        return ("ruled_action_id");
    }


    /**
     *  Tests if an <code> ActionQuery </code> is available. 
     *
     *  @return <code> true </code> if an action query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledActionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the action query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledActionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionQuery getRuledActionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledActionQuery() is false");
    }


    /**
     *  Matches enablers mapped to any action. 
     *
     *  @param  match <code> true </code> for enablers mapped to any action, 
     *          <code> false </code> to match enablers mapped to no actions 
     */

    @OSID @Override
    public void matchAnyRuledAction(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledActionColumn(), match);
        return;
    }


    /**
     *  Clears the action query terms. 
     */

    @OSID @Override
    public void clearRuledActionTerms() {
        getAssembler().clearTerms(getRuledActionColumn());
        return;
    }


    /**
     *  Gets the action query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionQueryInspector[] getRuledActionTerms() {
        return (new org.osid.control.ActionQueryInspector[0]);
    }


    /**
     *  Gets the RuledAction column name.
     *
     * @return the column name
     */

    protected String getRuledActionColumn() {
        return ("ruled_action");
    }


    /**
     *  Matches enablers mapped to the system. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this actionEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  actionEnablerRecordType an action enabler record type 
     *  @return <code>true</code> if the actionEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type actionEnablerRecordType) {
        for (org.osid.control.rules.records.ActionEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(actionEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  actionEnablerRecordType the action enabler record type 
     *  @return the action enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.ActionEnablerQueryRecord getActionEnablerQueryRecord(org.osid.type.Type actionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.ActionEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(actionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  actionEnablerRecordType the action enabler record type 
     *  @return the action enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.ActionEnablerQueryInspectorRecord getActionEnablerQueryInspectorRecord(org.osid.type.Type actionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.ActionEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(actionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param actionEnablerRecordType the action enabler record type
     *  @return the action enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.ActionEnablerSearchOrderRecord getActionEnablerSearchOrderRecord(org.osid.type.Type actionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.rules.records.ActionEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(actionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this action enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param actionEnablerQueryRecord the action enabler query record
     *  @param actionEnablerQueryInspectorRecord the action enabler query inspector
     *         record
     *  @param actionEnablerSearchOrderRecord the action enabler search order record
     *  @param actionEnablerRecordType action enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerQueryRecord</code>,
     *          <code>actionEnablerQueryInspectorRecord</code>,
     *          <code>actionEnablerSearchOrderRecord</code> or
     *          <code>actionEnablerRecordTypeactionEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addActionEnablerRecords(org.osid.control.rules.records.ActionEnablerQueryRecord actionEnablerQueryRecord, 
                                      org.osid.control.rules.records.ActionEnablerQueryInspectorRecord actionEnablerQueryInspectorRecord, 
                                      org.osid.control.rules.records.ActionEnablerSearchOrderRecord actionEnablerSearchOrderRecord, 
                                      org.osid.type.Type actionEnablerRecordType) {

        addRecordType(actionEnablerRecordType);

        nullarg(actionEnablerQueryRecord, "action enabler query record");
        nullarg(actionEnablerQueryInspectorRecord, "action enabler query inspector record");
        nullarg(actionEnablerSearchOrderRecord, "action enabler search odrer record");

        this.queryRecords.add(actionEnablerQueryRecord);
        this.queryInspectorRecords.add(actionEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(actionEnablerSearchOrderRecord);
        
        return;
    }
}
