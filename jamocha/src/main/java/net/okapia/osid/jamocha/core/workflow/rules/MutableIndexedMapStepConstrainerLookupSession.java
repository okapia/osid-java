//
// MutableIndexedMapStepConstrainerLookupSession
//
//    Implements a StepConstrainer lookup service backed by a collection of
//    stepConstrainers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules;


/**
 *  Implements a StepConstrainer lookup service backed by a collection of
 *  step constrainers. The step constrainers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some step constrainers may be compatible
 *  with more types than are indicated through these step constrainer
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of step constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapStepConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.workflow.rules.spi.AbstractIndexedMapStepConstrainerLookupSession
    implements org.osid.workflow.rules.StepConstrainerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapStepConstrainerLookupSession} with no step constrainers.
     *
     *  @param office the office
     *  @throws org.osid.NullArgumentException {@code office}
     *          is {@code null}
     */

      public MutableIndexedMapStepConstrainerLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapStepConstrainerLookupSession} with a
     *  single step constrainer.
     *  
     *  @param office the office
     *  @param  stepConstrainer a single stepConstrainer
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepConstrainer} is {@code null}
     */

    public MutableIndexedMapStepConstrainerLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.rules.StepConstrainer stepConstrainer) {
        this(office);
        putStepConstrainer(stepConstrainer);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapStepConstrainerLookupSession} using an
     *  array of step constrainers.
     *
     *  @param office the office
     *  @param  stepConstrainers an array of step constrainers
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepConstrainers} is {@code null}
     */

    public MutableIndexedMapStepConstrainerLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.rules.StepConstrainer[] stepConstrainers) {
        this(office);
        putStepConstrainers(stepConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapStepConstrainerLookupSession} using a
     *  collection of step constrainers.
     *
     *  @param office the office
     *  @param  stepConstrainers a collection of step constrainers
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code stepConstrainers} is {@code null}
     */

    public MutableIndexedMapStepConstrainerLookupSession(org.osid.workflow.Office office,
                                                  java.util.Collection<? extends org.osid.workflow.rules.StepConstrainer> stepConstrainers) {

        this(office);
        putStepConstrainers(stepConstrainers);
        return;
    }
    

    /**
     *  Makes a {@code StepConstrainer} available in this session.
     *
     *  @param  stepConstrainer a step constrainer
     *  @throws org.osid.NullArgumentException {@code stepConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putStepConstrainer(org.osid.workflow.rules.StepConstrainer stepConstrainer) {
        super.putStepConstrainer(stepConstrainer);
        return;
    }


    /**
     *  Makes an array of step constrainers available in this session.
     *
     *  @param  stepConstrainers an array of step constrainers
     *  @throws org.osid.NullArgumentException {@code stepConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putStepConstrainers(org.osid.workflow.rules.StepConstrainer[] stepConstrainers) {
        super.putStepConstrainers(stepConstrainers);
        return;
    }


    /**
     *  Makes collection of step constrainers available in this session.
     *
     *  @param  stepConstrainers a collection of step constrainers
     *  @throws org.osid.NullArgumentException {@code stepConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putStepConstrainers(java.util.Collection<? extends org.osid.workflow.rules.StepConstrainer> stepConstrainers) {
        super.putStepConstrainers(stepConstrainers);
        return;
    }


    /**
     *  Removes a StepConstrainer from this session.
     *
     *  @param stepConstrainerId the {@code Id} of the step constrainer
     *  @throws org.osid.NullArgumentException {@code stepConstrainerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeStepConstrainer(org.osid.id.Id stepConstrainerId) {
        super.removeStepConstrainer(stepConstrainerId);
        return;
    }    
}
