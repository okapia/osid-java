//
// JournalEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.journalentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class JournalEntryElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the JournalEntryElement Id.
     *
     *  @return the journal entry element Id
     */

    public static org.osid.id.Id getJournalEntryEntityId() {
        return (makeEntityId("osid.journaling.JournalEntry"));
    }


    /**
     *  Gets the BranchId element Id.
     *
     *  @return the BranchId element Id
     */

    public static org.osid.id.Id getBranchId() {
        return (makeElementId("osid.journaling.journalentry.BranchId"));
    }


    /**
     *  Gets the Branch element Id.
     *
     *  @return the Branch element Id
     */

    public static org.osid.id.Id getBranch() {
        return (makeElementId("osid.journaling.journalentry.Branch"));
    }


    /**
     *  Gets the SourceId element Id.
     *
     *  @return the SourceId element Id
     */

    public static org.osid.id.Id getSourceId() {
        return (makeElementId("osid.journaling.journalentry.SourceId"));
    }


    /**
     *  Gets the VersionId element Id.
     *
     *  @return the VersionId element Id
     */

    public static org.osid.id.Id getVersionId() {
        return (makeElementId("osid.journaling.journalentry.VersionId"));
    }


    /**
     *  Gets the Timestamp element Id.
     *
     *  @return the Timestamp element Id
     */

    public static org.osid.id.Id getTimestamp() {
        return (makeElementId("osid.journaling.journalentry.Timestamp"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.journaling.journalentry.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.journaling.journalentry.Resource"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeElementId("osid.journaling.journalentry.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeElementId("osid.journaling.journalentry.Agent"));
    }


    /**
     *  Gets the EntriesSince element Id.
     *
     *  @return the EntriesSince element Id
     */

    public static org.osid.id.Id getEntriesSince() {
        return (makeQueryElementId("osid.journaling.journalentry.EntriesSince"));
    }


    /**
     *  Gets the JournalId element Id.
     *
     *  @return the JournalId element Id
     */

    public static org.osid.id.Id getJournalId() {
        return (makeQueryElementId("osid.journaling.journalentry.JournalId"));
    }


    /**
     *  Gets the Journal element Id.
     *
     *  @return the Journal element Id
     */

    public static org.osid.id.Id getJournal() {
        return (makeQueryElementId("osid.journaling.journalentry.Journal"));
    }


    /**
     *  Gets the Source element Id.
     *
     *  @return the Source element Id
     */

    public static org.osid.id.Id getSource() {
        return (makeSearchOrderElementId("osid.journaling.journalentry.Source"));
    }
}
