//
// AbstractImmutableItem.java
//
//     Wraps a mutable Item to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.item.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Item</code> to hide modifiers. This
 *  wrapper provides an immutized Item from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying item whose state changes are visible.
 */

public abstract class AbstractImmutableItem
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.assessment.Item {

    private final org.osid.assessment.Item item;


    /**
     *  Constructs a new <code>AbstractImmutableItem</code>.
     *
     *  @param item the item to immutablize
     *  @throws org.osid.NullArgumentException <code>item</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableItem(org.osid.assessment.Item item) {
        super(item);
        this.item = item;
        return;
    }


    /**
     *  Gets the <code> Id </code> of any <code> Objectives </code> 
     *  corresponding to this item. 
     *
     *  @return learning objective <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        return (this.item.getLearningObjectiveIds());
    }


    /**
     *  Gets the any <code> Objectives </code> corresponding to this item. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        return (this.item.getLearningObjectives());
    }



    /**
     *  Gets the <code> Id </code> of the <code> Question </code>. 
     *
     *  @return the question <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getQuestionId() {
        return (this.item.getQuestionId());
    }


    /**
     *  Gets the question. 
     *
     *  @return the question 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Question getQuestion()
        throws org.osid.OperationFailedException {

        return (this.item.getQuestion());
    }


    /**
     *  Gets the <code> Ids </code> of the <code> Answers </code>. 
     *
     *  @return the answer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAnswerIds() {
        return (this.item.getAnswerIds());
    }


    /**
     *  Gets the answer. 
     *
     *  @return the answers
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.AnswerList getAnswers()
        throws org.osid.OperationFailedException {

        return (this.item.getAnswers());
    }


    /**
     *  Gets the item record corresponding to the given <code> Item
     *  </code> record <code> Type. </code> This method is used to
     *  retrieve an object implementing the requested record. The
     *  <code> itemRecordType </code> may be the <code> Type </code>
     *  returned in <code> getRecordTypes() </code> or any of its
     *  parents in a <code> Type </code> hierarchy where <code>
     *  hasRecordType(itemRecordType) </code> is <code> true </code> .
     *
     *  @param  itemRecordType the type of the record to retrieve
     *  @return the item record
     *  @throws org.osid.NullArgumentException <code> itemRecordType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(itemRecordType) </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.assessment.records.ItemRecord getItemRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {
        
        return (this.getItemRecord(itemRecordType));
    }
}

