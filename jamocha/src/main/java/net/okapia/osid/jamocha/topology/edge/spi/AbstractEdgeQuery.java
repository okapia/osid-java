//
// AbstractEdgeQuery.java
//
//     A template for making an Edge Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.edge.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for edges.
 */

public abstract class AbstractEdgeQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.topology.EdgeQuery {

    private final java.util.Collection<org.osid.topology.records.EdgeQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the node <code> Id </code> for this query to match edges assigned 
     *  to nodes. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchSourceNodeId(org.osid.id.Id nodeId, boolean match) {
        return;
    }


    /**
     *  Clears the node <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceNodeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a node. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceNodeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getSourceNodeQuery() {
        throw new org.osid.UnimplementedException("supportsSourceNodeQuery() is false");
    }


    /**
     *  Clears the node terms. 
     */

    @OSID @Override
    public void clearSourceNodeTerms() {
        return;
    }


    /**
     *  Sets the related node <code> Id </code> for this query to match edges 
     *  assigned to nodes. 
     *
     *  @param  nodeId the node <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDestinationNodeId(org.osid.id.Id nodeId, boolean match) {
        return;
    }


    /**
     *  Clears the related node <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDestinationNodeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDestinationNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a related node. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDestinationNodeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getDestinationNodeQuery() {
        throw new org.osid.UnimplementedException("supportsDestinationNodeQuery() is false");
    }


    /**
     *  Clears the related node terms. 
     */

    @OSID @Override
    public void clearDestinationNodeTerms() {
        return;
    }


    /**
     *  Matches edges between the same node. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchSameNode(boolean match) {
        return;
    }


    /**
     *  Clears the same node terms. 
     */

    @OSID @Override
    public void clearSameNodeTerms() {
        return;
    }


    /**
     *  Matches edge directionality. 
     *
     *  @param match <code> true </code> to match directional edges,
     *         <code> false </code> to match edges with no direction
     */

    @OSID @Override
    public void matchDirectional(boolean match) {
        return;
    }


    /**
     *  Clears the directional terms. 
     */

    @OSID @Override
    public void clearDirectionalTerms() {
        return;
    }


    /**
     *  Matches bidirectional edges.
     *
     *  @param  match <code> true </code> to match bi-directional edges, 
     *          <code> false </code> to match uni-directional edges 
     */

    @OSID @Override
    public void matchBiDirectional(boolean match) {
        return;
    }


    /**
     *  Clears the directional terms. 
     */

    @OSID @Override
    public void clearBiDirectionalTerms() {
        return;
    }


    /**
     *  Matches edges that have the specified cost inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchCost(java.math.BigDecimal from, java.math.BigDecimal to, 
                          boolean match) {
        return;
    }


    /**
     *  Matches edges that has any cost assigned. 
     *
     *  @param  match <code> true </code> to match edges with any cost, <code> 
     *          false </code> to match edges with no cost assigned 
     */

    @OSID @Override
    public void matchAnyCost(boolean match) {
        return;
    }


    /**
     *  Clears the cost terms. 
     */

    @OSID @Override
    public void clearCostTerms() {
        return;
    }


    /**
     *  Matches edges that have the specified distance inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchDistance(java.math.BigDecimal from, 
                              java.math.BigDecimal to, boolean match) {
        return;
    }


    /**
     *  Matches edges that has any distance assigned. 
     *
     *  @param  match <code> true </code> to match edges with any distance, 
     *          <code> false </code> to match edges with no distance assigned 
     */

    @OSID @Override
    public void matchAnyDistance(boolean match) {
        return;
    }


    /**
     *  Clears the distance terms. 
     */

    @OSID @Override
    public void clearDistanceTerms() {
        return;
    }


    /**
     *  Sets the graph <code> Id </code> for this query to match edges 
     *  assigned to graphs. 
     *
     *  @param  graphId the graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGraphId(org.osid.id.Id graphId, boolean match) {
        return;
    }


    /**
     *  Clears the graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGraphIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> supportsGraphQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getGraphQuery() {
        throw new org.osid.UnimplementedException("supportsGraphQuery() is false");
    }


    /**
     *  Clears the graph terms. 
     */

    @OSID @Override
    public void clearGraphTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given edge query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an edge implementing the requested record.
     *
     *  @param edgeRecordType an edge record type
     *  @return the edge query record
     *  @throws org.osid.NullArgumentException
     *          <code>edgeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.EdgeQueryRecord getEdgeQueryRecord(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.EdgeQueryRecord record : this.records) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this edge query. 
     *
     *  @param edgeQueryRecord edge query record
     *  @param edgeRecordType edge record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEdgeQueryRecord(org.osid.topology.records.EdgeQueryRecord edgeQueryRecord, 
                                          org.osid.type.Type edgeRecordType) {

        addRecordType(edgeRecordType);
        nullarg(edgeQueryRecord, "edge query record");
        this.records.add(edgeQueryRecord);        
        return;
    }
}
