//
// AbstractMapAppointmentLookupSession
//
//    A simple framework for providing an Appointment lookup service
//    backed by a fixed collection of appointments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Appointment lookup service backed by a
 *  fixed collection of appointments. The appointments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Appointments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAppointmentLookupSession
    extends net.okapia.osid.jamocha.personnel.spi.AbstractAppointmentLookupSession
    implements org.osid.personnel.AppointmentLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.personnel.Appointment> appointments = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.personnel.Appointment>());


    /**
     *  Makes an <code>Appointment</code> available in this session.
     *
     *  @param  appointment an appointment
     *  @throws org.osid.NullArgumentException <code>appointment<code>
     *          is <code>null</code>
     */

    protected void putAppointment(org.osid.personnel.Appointment appointment) {
        this.appointments.put(appointment.getId(), appointment);
        return;
    }


    /**
     *  Makes an array of appointments available in this session.
     *
     *  @param  appointments an array of appointments
     *  @throws org.osid.NullArgumentException <code>appointments<code>
     *          is <code>null</code>
     */

    protected void putAppointments(org.osid.personnel.Appointment[] appointments) {
        putAppointments(java.util.Arrays.asList(appointments));
        return;
    }


    /**
     *  Makes a collection of appointments available in this session.
     *
     *  @param  appointments a collection of appointments
     *  @throws org.osid.NullArgumentException <code>appointments<code>
     *          is <code>null</code>
     */

    protected void putAppointments(java.util.Collection<? extends org.osid.personnel.Appointment> appointments) {
        for (org.osid.personnel.Appointment appointment : appointments) {
            this.appointments.put(appointment.getId(), appointment);
        }

        return;
    }


    /**
     *  Removes an Appointment from this session.
     *
     *  @param  appointmentId the <code>Id</code> of the appointment
     *  @throws org.osid.NullArgumentException <code>appointmentId<code> is
     *          <code>null</code>
     */

    protected void removeAppointment(org.osid.id.Id appointmentId) {
        this.appointments.remove(appointmentId);
        return;
    }


    /**
     *  Gets the <code>Appointment</code> specified by its <code>Id</code>.
     *
     *  @param  appointmentId <code>Id</code> of the <code>Appointment</code>
     *  @return the appointment
     *  @throws org.osid.NotFoundException <code>appointmentId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>appointmentId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Appointment getAppointment(org.osid.id.Id appointmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.personnel.Appointment appointment = this.appointments.get(appointmentId);
        if (appointment == null) {
            throw new org.osid.NotFoundException("appointment not found: " + appointmentId);
        }

        return (appointment);
    }


    /**
     *  Gets all <code>Appointments</code>. In plenary mode, the returned
     *  list contains all known appointments or an error
     *  results. Otherwise, the returned list may contain only those
     *  appointments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Appointments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentList getAppointments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.appointment.ArrayAppointmentList(this.appointments.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.appointments.clear();
        super.close();
        return;
    }
}
