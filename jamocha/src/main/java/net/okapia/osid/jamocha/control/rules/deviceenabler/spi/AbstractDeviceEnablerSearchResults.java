//
// AbstractDeviceEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.deviceenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDeviceEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.control.rules.DeviceEnablerSearchResults {

    private org.osid.control.rules.DeviceEnablerList deviceEnablers;
    private final org.osid.control.rules.DeviceEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.control.rules.records.DeviceEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDeviceEnablerSearchResults.
     *
     *  @param deviceEnablers the result set
     *  @param deviceEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>deviceEnablers</code>
     *          or <code>deviceEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDeviceEnablerSearchResults(org.osid.control.rules.DeviceEnablerList deviceEnablers,
                                            org.osid.control.rules.DeviceEnablerQueryInspector deviceEnablerQueryInspector) {
        nullarg(deviceEnablers, "device enablers");
        nullarg(deviceEnablerQueryInspector, "device enabler query inspectpr");

        this.deviceEnablers = deviceEnablers;
        this.inspector = deviceEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the device enabler list resulting from a search.
     *
     *  @return a device enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerList getDeviceEnablers() {
        if (this.deviceEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.control.rules.DeviceEnablerList deviceEnablers = this.deviceEnablers;
        this.deviceEnablers = null;
	return (deviceEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.control.rules.DeviceEnablerQueryInspector getDeviceEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  device enabler search record <code> Type. </code> This method must
     *  be used to retrieve a deviceEnabler implementing the requested
     *  record.
     *
     *  @param deviceEnablerSearchRecordType a deviceEnabler search 
     *         record type 
     *  @return the device enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>deviceEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(deviceEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.rules.records.DeviceEnablerSearchResultsRecord getDeviceEnablerSearchResultsRecord(org.osid.type.Type deviceEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.control.rules.records.DeviceEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(deviceEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(deviceEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record device enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDeviceEnablerRecord(org.osid.control.rules.records.DeviceEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "device enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
