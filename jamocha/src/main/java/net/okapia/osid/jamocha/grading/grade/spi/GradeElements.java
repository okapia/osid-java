//
// GradeElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.grade.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GradeElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the GradeElement Id.
     *
     *  @return the grade element Id
     */

    public static org.osid.id.Id getGradeEntityId() {
        return (makeEntityId("osid.grading.Grade"));
    }


    /**
     *  Gets the GradeSystemId element Id.
     *
     *  @return the GradeSystemId element Id
     */

    public static org.osid.id.Id getGradeSystemId() {
        return (makeElementId("osid.grading.grade.GradeSystemId"));
    }


    /**
     *  Gets the GradeSystem element Id.
     *
     *  @return the GradeSystem element Id
     */

    public static org.osid.id.Id getGradeSystem() {
        return (makeElementId("osid.grading.grade.GradeSystem"));
    }


    /**
     *  Gets the InputScoreStartRange element Id.
     *
     *  @return the InputScoreStartRange element Id
     */

    public static org.osid.id.Id getInputScoreStartRange() {
        return (makeElementId("osid.grading.grade.InputScoreStartRange"));
    }


    /**
     *  Gets the InputScoreEndRange element Id.
     *
     *  @return the InputScoreEndRange element Id
     */

    public static org.osid.id.Id getInputScoreEndRange() {
        return (makeElementId("osid.grading.grade.InputScoreEndRange"));
    }


    /**
     *  Gets the OutputScore element Id.
     *
     *  @return the OutputScore element Id
     */

    public static org.osid.id.Id getOutputScore() {
        return (makeElementId("osid.grading.grade.OutputScore"));
    }


    /**
     *  Gets the GradeEntryId element Id.
     *
     *  @return the GradeEntryId element Id
     */

    public static org.osid.id.Id getGradeEntryId() {
        return (makeQueryElementId("osid.grading.grade.GradeEntryId"));
    }


    /**
     *  Gets the GradeEntry element Id.
     *
     *  @return the GradeEntry element Id
     */

    public static org.osid.id.Id getGradeEntry() {
        return (makeQueryElementId("osid.grading.grade.GradeEntry"));
    }


    /**
     *  Gets the GradebookId element Id.
     *
     *  @return the GradebookId element Id
     */

    public static org.osid.id.Id getGradebookId() {
        return (makeQueryElementId("osid.grading.grade.GradebookId"));
    }


    /**
     *  Gets the Gradebook element Id.
     *
     *  @return the Gradebook element Id
     */

    public static org.osid.id.Id getGradebook() {
        return (makeQueryElementId("osid.grading.grade.Gradebook"));
    }


    /**
     *  Gets the InputScoreRange element Id.
     *
     *  @return the InputScoreRange element Id
     */

    public static org.osid.id.Id getInputScoreRange() {
        return (makeElementId("osid.grading.grade.InputScoreRange"));
    }
}
