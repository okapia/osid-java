//
// AbstractFederatingAwardEntryLookupSession.java
//
//     An abstract federating adapter for an AwardEntryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AwardEntryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAwardEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.chronicle.AwardEntryLookupSession>
    implements org.osid.course.chronicle.AwardEntryLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingAwardEntryLookupSession</code>.
     */

    protected AbstractFederatingAwardEntryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.chronicle.AwardEntryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>AwardEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAwardEntries() {
        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            if (session.canLookupAwardEntries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AwardEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAwardEntryView() {
        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            session.useComparativeAwardEntryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AwardEntry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAwardEntryView() {
        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            session.usePlenaryAwardEntryView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include award entries in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only award entries whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveAwardEntryView() {
        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            session.useEffectiveAwardEntryView();
        }

        return;
    }


    /**
     *  All award entries of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveAwardEntryView() {
        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            session.useAnyEffectiveAwardEntryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>AwardEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AwardEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AwardEntry</code> and
     *  retained for compatibility.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  awardEntryId <code>Id</code> of the
     *          <code>AwardEntry</code>
     *  @return the award entry
     *  @throws org.osid.NotFoundException <code>awardEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>awardEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntry getAwardEntry(org.osid.id.Id awardEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            try {
                return (session.getAwardEntry(awardEntryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(awardEntryId + " not found");
    }


    /**
     *  Gets an <code>AwardEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  awardEntries specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>AwardEntries</code> may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any qeffective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  awardEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByIds(org.osid.id.IdList awardEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.chronicle.awardentry.MutableAwardEntryList ret = new net.okapia.osid.jamocha.course.chronicle.awardentry.MutableAwardEntryList();

        try (org.osid.id.IdList ids = awardEntryIds) {
            while (ids.hasNext()) {
                ret.addAwardEntry(getAwardEntry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AwardEntryList</code> corresponding to the given
     *  award entry genus <code>Type</code> which does not include
     *  award entries of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those award entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  awardEntryGenusType an awardEntry genus type 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByGenusType(org.osid.type.Type awardEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesByGenusType(awardEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AwardEntryList</code> corresponding to the given
     *  award entry genus <code>Type</code> and include any additional
     *  award entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those award entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  awardEntryGenusType an awardEntry genus type 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByParentGenusType(org.osid.type.Type awardEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesByParentGenusType(awardEntryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AwardEntryList</code> containing the given
     *  award entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, award entries are returned that are currently
     *  effective.  In any effective mode, effective award entries and
     *  those currently expired are returned.
     *
     *  @param  awardEntryRecordType an awardEntry record type 
     *  @return the returned <code>AwardEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>awardEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesByRecordType(org.osid.type.Type awardEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesByRecordType(awardEntryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AwardEntryList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *  
     *  In active mode, award entries are returned that are currently
     *  active. In any status mode, active and inactive award entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AwardEntry</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of award entries corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those award entries that are accessible through
     *  this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.AwardEntryList getAwardEntriesForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesForStudent(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of award entries corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  award entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesForStudentOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of award entries corresponding to an award
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  award entries and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.chronicle.AwardEntryList getAwardEntriesForAward(org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesForAward(awardId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of award entries corresponding to an award
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known award
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those award entries that are accessible through
     *  this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>awardId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesForAwardOnDate(org.osid.id.Id awardId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesForAwardOnDate(awardId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of award entries corresponding to student and award
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known award
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those award entries that are accessible through
     *  this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  awardId the <code>Id</code> of the award
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesForStudentAndAward(org.osid.id.Id resourceId,
                                                                                      org.osid.id.Id awardId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesForStudentAndAward(resourceId, awardId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of award entries corresponding to student and award
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  award entries or an error results. Otherwise, the returned list
     *  may contain only those award entries that are accessible
     *  through this session.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @param  awardId the <code>Id</code> of the award
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>AwardEntryList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>awardId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntriesForStudentAndAwardOnDate(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id awardId,
                                                                                            org.osid.calendaring.DateTime from,
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntriesForStudentAndAwardOnDate(resourceId, awardId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AwardEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known award
     *  entries or an error results. Otherwise, the returned list may
     *  contain only those award entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, award entries are returned that are
     *  currently effective.  In any effective mode, effective award
     *  entries and those currently expired are returned.
     *
     *  @return a list of <code>AwardEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryList getAwardEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList ret = getAwardEntryList();

        for (org.osid.course.chronicle.AwardEntryLookupSession session : getSessions()) {
            ret.addAwardEntryList(session.getAwardEntries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.FederatingAwardEntryList getAwardEntryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.ParallelAwardEntryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.chronicle.awardentry.CompositeAwardEntryList());
        }
    }
}
