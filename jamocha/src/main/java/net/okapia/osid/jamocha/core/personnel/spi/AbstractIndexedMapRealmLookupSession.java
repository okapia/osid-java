//
// AbstractIndexedMapRealmLookupSession.java
//
//    A simple framework for providing a Realm lookup service
//    backed by a fixed collection of realms with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Realm lookup service backed by a
 *  fixed collection of realms. The realms are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some realms may be compatible
 *  with more types than are indicated through these realm
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Realms</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRealmLookupSession
    extends AbstractMapRealmLookupSession
    implements org.osid.personnel.RealmLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.personnel.Realm> realmsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Realm>());
    private final MultiMap<org.osid.type.Type, org.osid.personnel.Realm> realmsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.personnel.Realm>());


    /**
     *  Makes a <code>Realm</code> available in this session.
     *
     *  @param  realm a realm
     *  @throws org.osid.NullArgumentException <code>realm<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRealm(org.osid.personnel.Realm realm) {
        super.putRealm(realm);

        this.realmsByGenus.put(realm.getGenusType(), realm);
        
        try (org.osid.type.TypeList types = realm.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.realmsByRecord.put(types.getNextType(), realm);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a realm from this session.
     *
     *  @param realmId the <code>Id</code> of the realm
     *  @throws org.osid.NullArgumentException <code>realmId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRealm(org.osid.id.Id realmId) {
        org.osid.personnel.Realm realm;
        try {
            realm = getRealm(realmId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.realmsByGenus.remove(realm.getGenusType());

        try (org.osid.type.TypeList types = realm.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.realmsByRecord.remove(types.getNextType(), realm);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRealm(realmId);
        return;
    }


    /**
     *  Gets a <code>RealmList</code> corresponding to the given
     *  realm genus <code>Type</code> which does not include
     *  realms of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known realms or an error results. Otherwise,
     *  the returned list may contain only those realms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  realmGenusType a realm genus type 
     *  @return the returned <code>Realm</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>realmGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByGenusType(org.osid.type.Type realmGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.realm.ArrayRealmList(this.realmsByGenus.get(realmGenusType)));
    }


    /**
     *  Gets a <code>RealmList</code> containing the given
     *  realm record <code>Type</code>. In plenary mode, the
     *  returned list contains all known realms or an error
     *  results. Otherwise, the returned list may contain only those
     *  realms that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  realmRecordType a realm record type 
     *  @return the returned <code>realm</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>realmRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.RealmList getRealmsByRecordType(org.osid.type.Type realmRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.personnel.realm.ArrayRealmList(this.realmsByRecord.get(realmRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.realmsByGenus.clear();
        this.realmsByRecord.clear();

        super.close();

        return;
    }
}
