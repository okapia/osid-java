//
// InvariantMapPoolConstrainerLookupSession
//
//    Implements a PoolConstrainer lookup service backed by a fixed collection of
//    poolConstrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a PoolConstrainer lookup service backed by a fixed
 *  collection of pool constrainers. The pool constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapPoolConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapPoolConstrainerLookupSession
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolConstrainerLookupSession</code> with no
     *  pool constrainers.
     *  
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumnetException {@code distributor} is
     *          {@code null}
     */

    public InvariantMapPoolConstrainerLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolConstrainerLookupSession</code> with a single
     *  pool constrainer.
     *  
     *  @param distributor the distributor
     *  @param poolConstrainer a single pool constrainer
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainer} is <code>null</code>
     */

      public InvariantMapPoolConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.PoolConstrainer poolConstrainer) {
        this(distributor);
        putPoolConstrainer(poolConstrainer);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolConstrainerLookupSession</code> using an array
     *  of pool constrainers.
     *  
     *  @param distributor the distributor
     *  @param poolConstrainers an array of pool constrainers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainers} is <code>null</code>
     */

      public InvariantMapPoolConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.rules.PoolConstrainer[] poolConstrainers) {
        this(distributor);
        putPoolConstrainers(poolConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapPoolConstrainerLookupSession</code> using a
     *  collection of pool constrainers.
     *
     *  @param distributor the distributor
     *  @param poolConstrainers a collection of pool constrainers
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code poolConstrainers} is <code>null</code>
     */

      public InvariantMapPoolConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                               java.util.Collection<? extends org.osid.provisioning.rules.PoolConstrainer> poolConstrainers) {
        this(distributor);
        putPoolConstrainers(poolConstrainers);
        return;
    }
}
