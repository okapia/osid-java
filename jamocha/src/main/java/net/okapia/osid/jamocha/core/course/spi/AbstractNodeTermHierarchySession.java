//
// AbstractNodeTermHierarchySession.java
//
//     Defines a Term hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a term hierarchy session for delivering a hierarchy
 *  of terms using the TermNode interface.
 */

public abstract class AbstractNodeTermHierarchySession
    extends net.okapia.osid.jamocha.course.spi.AbstractTermHierarchySession
    implements org.osid.course.TermHierarchySession {

    private java.util.Collection<org.osid.course.TermNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root term <code> Ids </code> in this hierarchy.
     *
     *  @return the root term <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootTermIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.course.termnode.TermNodeToIdList(this.roots));
    }


    /**
     *  Gets the root terms in the term hierarchy. A node
     *  with no parents is an orphan. While all term <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root terms 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getRootTerms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.termnode.TermNodeToTermList(new net.okapia.osid.jamocha.course.termnode.ArrayTermNodeList(this.roots)));
    }


    /**
     *  Adds a root term node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootTerm(org.osid.course.TermNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root term nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootTerms(java.util.Collection<org.osid.course.TermNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root term node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootTerm(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.course.TermNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Term </code> has any parents. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @return <code> true </code> if the term has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentTerms(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getTermNode(termId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  term.
     *
     *  @param  id an <code> Id </code> 
     *  @param  termId the <code> Id </code> of a term 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> termId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> termId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfTerm(org.osid.id.Id id, org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.TermNodeList parents = getTermNode(termId).getParentTermNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextTermNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given term. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @return the parent <code> Ids </code> of the term 
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentTermIds(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.term.TermToIdList(getParentTerms(termId)));
    }


    /**
     *  Gets the parents of the given term. 
     *
     *  @param  termId the <code> Id </code> to query 
     *  @return the parents of the term 
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getParentTerms(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.termnode.TermNodeToTermList(getTermNode(termId).getParentTermNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  term.
     *
     *  @param  id an <code> Id </code> 
     *  @param  termId the Id of a term 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> termId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfTerm(org.osid.id.Id id, org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfTerm(id, termId)) {
            return (true);
        }

        try (org.osid.course.TermList parents = getParentTerms(termId)) {
            while (parents.hasNext()) {
                if (isAncestorOfTerm(id, parents.getNextTerm().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a term has any children. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @return <code> true </code> if the <code> termId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildTerms(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTermNode(termId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  term.
     *
     *  @param  id an <code> Id </code> 
     *  @param termId the <code> Id </code> of a 
     *         term
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> termId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> termId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfTerm(org.osid.id.Id id, org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfTerm(termId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  term.
     *
     *  @param  termId the <code> Id </code> to query 
     *  @return the children of the term 
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildTermIds(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.term.TermToIdList(getChildTerms(termId)));
    }


    /**
     *  Gets the children of the given term. 
     *
     *  @param  termId the <code> Id </code> to query 
     *  @return the children of the term 
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermList getChildTerms(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.course.termnode.TermNodeToTermList(getTermNode(termId).getChildTermNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  term.
     *
     *  @param  id an <code> Id </code> 
     *  @param termId the <code> Id </code> of a 
     *         term
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> termId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfTerm(org.osid.id.Id id, org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfTerm(termId, id)) {
            return (true);
        }

        try (org.osid.course.TermList children = getChildTerms(termId)) {
            while (children.hasNext()) {
                if (isDescendantOfTerm(id, children.getNextTerm().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  term.
     *
     *  @param  termId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified term node 
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getTermNodeIds(org.osid.id.Id termId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.course.termnode.TermNodeToNode(getTermNode(termId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given term.
     *
     *  @param  termId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified term node 
     *  @throws org.osid.NotFoundException <code> termId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> termId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.TermNode getTermNodes(org.osid.id.Id termId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getTermNode(termId));
    }


    /**
     *  Closes this <code>TermHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a term node.
     *
     *  @param termId the id of the term node
     *  @throws org.osid.NotFoundException <code>termId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>termId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.course.TermNode getTermNode(org.osid.id.Id termId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(termId, "term Id");
        for (org.osid.course.TermNode term : this.roots) {
            if (term.getId().equals(termId)) {
                return (term);
            }

            org.osid.course.TermNode r = findTerm(term, termId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(termId + " is not found");
    }


    protected org.osid.course.TermNode findTerm(org.osid.course.TermNode node, 
                                                org.osid.id.Id termId)
	throws org.osid.OperationFailedException { 

        try (org.osid.course.TermNodeList children = node.getChildTermNodes()) {
            while (children.hasNext()) {
                org.osid.course.TermNode term = children.getNextTermNode();
                if (term.getId().equals(termId)) {
                    return (term);
                }
                
                term = findTerm(term, termId);
                if (term != null) {
                    return (term);
                }
            }
        }

        return (null);
    }
}
