//
// AbstractSyllabusSearch.java
//
//     A template for making a Syllabus Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.syllabus.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing syllabus searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSyllabusSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.syllabus.SyllabusSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.SyllabusSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.syllabus.SyllabusSearchOrder syllabusSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of syllabi. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  syllabusIds list of syllabi
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSyllabi(org.osid.id.IdList syllabusIds) {
        while (syllabusIds.hasNext()) {
            try {
                this.ids.add(syllabusIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSyllabi</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of syllabus Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSyllabusIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  syllabusSearchOrder syllabus search order 
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>syllabusSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSyllabusResults(org.osid.course.syllabus.SyllabusSearchOrder syllabusSearchOrder) {
	this.syllabusSearchOrder = syllabusSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.syllabus.SyllabusSearchOrder getSyllabusSearchOrder() {
	return (this.syllabusSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given syllabus search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a syllabus implementing the requested record.
     *
     *  @param syllabusSearchRecordType a syllabus search record
     *         type
     *  @return the syllabus search record
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(syllabusSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.SyllabusSearchRecord getSyllabusSearchRecord(org.osid.type.Type syllabusSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.syllabus.records.SyllabusSearchRecord record : this.records) {
            if (record.implementsRecordType(syllabusSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(syllabusSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this syllabus search. 
     *
     *  @param syllabusSearchRecord syllabus search record
     *  @param syllabusSearchRecordType syllabus search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSyllabusSearchRecord(org.osid.course.syllabus.records.SyllabusSearchRecord syllabusSearchRecord, 
                                           org.osid.type.Type syllabusSearchRecordType) {

        addRecordType(syllabusSearchRecordType);
        this.records.add(syllabusSearchRecord);        
        return;
    }
}
