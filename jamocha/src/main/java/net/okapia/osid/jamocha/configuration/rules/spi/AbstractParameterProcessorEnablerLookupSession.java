//
// AbstractParameterProcessorEnablerLookupSession.java
//
//    A starter implementation framework for providing a ParameterProcessorEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a ParameterProcessorEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getParameterProcessorEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractParameterProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.configuration.rules.ParameterProcessorEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.configuration.Configuration configuration = new net.okapia.osid.jamocha.nil.configuration.configuration.UnknownConfiguration();
    

    /**
     *  Gets the <code>Configuration/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Configuration Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getConfigurationId() {
        return (this.configuration.getId());
    }


    /**
     *  Gets the <code>Configuration</code> associated with this 
     *  session.
     *
     *  @return the <code>Configuration</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Configuration getConfiguration()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.configuration);
    }


    /**
     *  Sets the <code>Configuration</code>.
     *
     *  @param  configuration the configuration for this session
     *  @throws org.osid.NullArgumentException <code>configuration</code>
     *          is <code>null</code>
     */

    protected void setConfiguration(org.osid.configuration.Configuration configuration) {
        nullarg(configuration, "configuration");
        this.configuration = configuration;
        return;
    }


    /**
     *  Tests if this user can perform <code>ParameterProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupParameterProcessorEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>ParameterProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeParameterProcessorEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ParameterProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryParameterProcessorEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include parameter processor enablers in configurations which are children
     *  of this configuration in the configuration hierarchy.
     */

    @OSID @Override
    public void useFederatedConfigurationView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this configuration only.
     */

    @OSID @Override
    public void useIsolatedConfigurationView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active parameter processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveParameterProcessorEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive parameter processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusParameterProcessorEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>ParameterProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ParameterProcessorEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ParameterProcessorEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, parameter processor enablers are returned that are currently
     *  active. In any status mode, active and inactive parameter processor enablers
     *  are returned.
     *
     *  @param  parameterProcessorEnablerId <code>Id</code> of the
     *          <code>ParameterProcessorEnabler</code>
     *  @return the parameter processor enabler
     *  @throws org.osid.NotFoundException <code>parameterProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>parameterProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnabler getParameterProcessorEnabler(org.osid.id.Id parameterProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.configuration.rules.ParameterProcessorEnablerList parameterProcessorEnablers = getParameterProcessorEnablers()) {
            while (parameterProcessorEnablers.hasNext()) {
                org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler = parameterProcessorEnablers.getNextParameterProcessorEnabler();
                if (parameterProcessorEnabler.getId().equals(parameterProcessorEnablerId)) {
                    return (parameterProcessorEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(parameterProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>ParameterProcessorEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  parameterProcessorEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ParameterProcessorEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, parameter processor enablers are returned that are currently
     *  active. In any status mode, active and inactive parameter processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getParameterProcessorEnablers()</code>.
     *
     *  @param  parameterProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ParameterProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablersByIds(org.osid.id.IdList parameterProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.configuration.rules.ParameterProcessorEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = parameterProcessorEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getParameterProcessorEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("parameter processor enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessorenabler.LinkedParameterProcessorEnablerList(ret));
    }


    /**
     *  Gets a <code>ParameterProcessorEnablerList</code> corresponding to the given
     *  parameter processor enabler genus <code>Type</code> which does not include
     *  parameter processor enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processor enablers or an error results. Otherwise, the returned list
     *  may contain only those parameter processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processor enablers are returned that are currently
     *  active. In any status mode, active and inactive parameter processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getParameterProcessorEnablers()</code>.
     *
     *  @param  parameterProcessorEnablerGenusType a parameterProcessorEnabler genus type 
     *  @return the returned <code>ParameterProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablersByGenusType(org.osid.type.Type parameterProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.rules.parameterprocessorenabler.ParameterProcessorEnablerGenusFilterList(getParameterProcessorEnablers(), parameterProcessorEnablerGenusType));
    }


    /**
     *  Gets a <code>ParameterProcessorEnablerList</code> corresponding to the given
     *  parameter processor enabler genus <code>Type</code> and include any additional
     *  parameter processor enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processor enablers or an error results. Otherwise, the returned list
     *  may contain only those parameter processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processor enablers are returned that are currently
     *  active. In any status mode, active and inactive parameter processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getParameterProcessorEnablers()</code>.
     *
     *  @param  parameterProcessorEnablerGenusType a parameterProcessorEnabler genus type 
     *  @return the returned <code>ParameterProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablersByParentGenusType(org.osid.type.Type parameterProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getParameterProcessorEnablersByGenusType(parameterProcessorEnablerGenusType));
    }


    /**
     *  Gets a <code>ParameterProcessorEnablerList</code> containing the given
     *  parameter processor enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  parameter processor enablers or an error results. Otherwise, the returned list
     *  may contain only those parameter processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, parameter processor enablers are returned that are currently
     *  active. In any status mode, active and inactive parameter processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getParameterProcessorEnablers()</code>.
     *
     *  @param  parameterProcessorEnablerRecordType a parameterProcessorEnabler record type 
     *  @return the returned <code>ParameterProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>parameterProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablersByRecordType(org.osid.type.Type parameterProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.rules.parameterprocessorenabler.ParameterProcessorEnablerRecordFilterList(getParameterProcessorEnablers(), parameterProcessorEnablerRecordType));
    }


    /**
     *  Gets a <code>ParameterProcessorEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  parameter processor enablers or an error results. Otherwise, the returned list
     *  may contain only those parameter processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, parameter processor enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive parameter processor enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ParameterProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.configuration.rules.parameterprocessorenabler.TemporalParameterProcessorEnablerFilterList(getParameterProcessorEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>ParameterProcessorEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processor enablers or an error results. Otherwise, the returned list
     *  may contain only those parameter processor enablers that are accessible
     *  through this session.
     *
     *  In active mode, parameter processor enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive parameter processor enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ParameterProcessorEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getParameterProcessorEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>ParameterProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  parameter processor enablers or an error results. Otherwise, the returned list
     *  may contain only those parameter processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, parameter processor enablers are returned that are currently
     *  active. In any status mode, active and inactive parameter processor enablers
     *  are returned.
     *
     *  @return a list of <code>ParameterProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.configuration.rules.ParameterProcessorEnablerList getParameterProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the parameter processor enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of parameter processor enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.configuration.rules.ParameterProcessorEnablerList filterParameterProcessorEnablersOnViews(org.osid.configuration.rules.ParameterProcessorEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.configuration.rules.ParameterProcessorEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.configuration.rules.parameterprocessorenabler.ActiveParameterProcessorEnablerFilterList(ret);
        }

        return (ret);
    }
}
