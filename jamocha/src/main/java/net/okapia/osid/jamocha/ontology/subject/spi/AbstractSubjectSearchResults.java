//
// AbstractSubjectSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.subject.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractSubjectSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ontology.SubjectSearchResults {

    private org.osid.ontology.SubjectList subjects;
    private final org.osid.ontology.SubjectQueryInspector inspector;
    private final java.util.Collection<org.osid.ontology.records.SubjectSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractSubjectSearchResults.
     *
     *  @param subjects the result set
     *  @param subjectQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>subjects</code>
     *          or <code>subjectQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractSubjectSearchResults(org.osid.ontology.SubjectList subjects,
                                            org.osid.ontology.SubjectQueryInspector subjectQueryInspector) {
        nullarg(subjects, "subjects");
        nullarg(subjectQueryInspector, "subject query inspectpr");

        this.subjects = subjects;
        this.inspector = subjectQueryInspector;

        return;
    }


    /**
     *  Gets the subject list resulting from a search.
     *
     *  @return a subject list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ontology.SubjectList getSubjects() {
        if (this.subjects == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ontology.SubjectList subjects = this.subjects;
        this.subjects = null;
	return (subjects);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ontology.SubjectQueryInspector getSubjectQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  subject search record <code> Type. </code> This method must
     *  be used to retrieve a subject implementing the requested
     *  record.
     *
     *  @param subjectSearchRecordType a subject search 
     *         record type 
     *  @return the subject search
     *  @throws org.osid.NullArgumentException
     *          <code>subjectSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(subjectSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.SubjectSearchResultsRecord getSubjectSearchResultsRecord(org.osid.type.Type subjectSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ontology.records.SubjectSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(subjectSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(subjectSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record subject search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addSubjectRecord(org.osid.ontology.records.SubjectSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "subject record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
