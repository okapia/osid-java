//
// InvariantMapProxyCandidateLookupSession
//
//    Implements a Candidate lookup service backed by a fixed
//    collection of candidates. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Candidate lookup service backed by a fixed
 *  collection of candidates. The candidates are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCandidateLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractMapCandidateLookupSession
    implements org.osid.voting.CandidateLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCandidateLookupSession} with no
     *  candidates.
     *
     *  @param polls the polls
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCandidateLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.proxy.Proxy proxy) {
        setPolls(polls);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCandidateLookupSession} with a single
     *  candidate.
     *
     *  @param polls the polls
     *  @param candidate a single candidate
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code candidate} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCandidateLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Candidate candidate, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putCandidate(candidate);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCandidateLookupSession} using
     *  an array of candidates.
     *
     *  @param polls the polls
     *  @param candidates an array of candidates
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code candidates} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCandidateLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Candidate[] candidates, org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putCandidates(candidates);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCandidateLookupSession} using a
     *  collection of candidates.
     *
     *  @param polls the polls
     *  @param candidates a collection of candidates
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code candidates} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCandidateLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.Candidate> candidates,
                                                  org.osid.proxy.Proxy proxy) {

        this(polls, proxy);
        putCandidates(candidates);
        return;
    }
}
