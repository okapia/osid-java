//
// AbstractIdLookupSession.java
//
//    A starter implementation framework for providing an Id
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.id.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Id
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getIds(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractIdLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.id.IdLookupSession {


    /**
     *  Tests if this user can perform <code>Id</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupIds() {
        return (true);
    }

     
    /**
     *  Gets an <code> Id. </code> This method serves to get the
     *  principal <code> Id </code> if the given <code> Id </code> Is an
     *  alias.
     *
     *  @param  id an <code>Id</code>
     *  @return the <code>Id</code>
     *  @throws org.osid.NotFoundException <code>id</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>id</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.Id getId(org.osid.id.Id id)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.id.IdList ids = getIds()) {
            while (ids.hasNext()) {
                if (ids.getNextId().equals(id)) {
                    return (id);
                }
            }
        } 

        throw new org.osid.NotFoundException(id + " not found");
    }


    /**
     *  Gets a list of <code>Ids</code>. This method serves to get the
     *  principal <code>Ids</code> if different from the given
     *  <code>Ids</code>.
     *
     *  @param  ids the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Id</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not found 
     *  @throws org.osid.NullArgumentException <code>ids</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getIdsByIds(org.osid.id.IdList ids)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.id.Id> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList idList = ids) {
            while (idList.hasNext()) {
                org.osid.id.Id id = idList.getNextId();
                ret.add(getId(id));
            }
        }
            
        return (new net.okapia.osid.jamocha.id.id.LinkedIdList(ret));
    }

    
    /**
     *  Gets a list of <code>Id</code> aliases of an <code>Id</code>.
     *
     *  @param  id an <code>Id</code> 
     *  @return a list of alias <code>Ids</code> 
     *  @throws org.osid.NotFoundException <code>id</code> is not found 
     *  @throws org.osid.NullArgumentException <code>id</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIdAliases(org.osid.id.Id id)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.id.id.SingleIdList(id));
    }


    /**
     *  Gets <code>Ids</code> by the given authority. 
     *
     *  @param  authority an authority 
     *  @return a list of <code>Ids</code> 
     *  @throws org.osid.NullArgumentException <code>authority</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.id.IdList getIdsByAuthority(String authority)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.id.id.IdFilterList(new AuthorityFilter(authority), getIds()));
    }


    /**
     *  Gets all <code>Ids</code>.
     *
     *  @return the list of all <code>Ids</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public abstract org.osid.id.IdList getIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Tests if the two <code>Ids</code> are equivalent. Two
     *  <code>Ids </code> are equivalent if they identify the same
     *  object. If one of the <code>Ids</code> is not known, they are
     *  not equivalent.
     *
     *  @param  id an <code>Id</code> 
     *  @param  equivalentId an <code>Id /code> 
     *  @return <code>true</code> if the <code>Ids</code> are
     *          equivalent, false otherwise
     *  @throws org.osid.NullArgumentException <code>null</code> argument 
     *          provided 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public boolean isEquivalent(org.osid.id.Id id, org.osid.id.Id equivalentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (id.equals(equivalentId));
    }


    public static class AuthorityFilter
        implements net.okapia.osid.jamocha.inline.filter.id.id.IdFilter {
         
        private final String authority;
         
         
        /**
         *  Constructs a new <code>AuthorityFilter</code>.
         *
         *  @param authority the authority to filter
         *  @throws org.osid.NullArgumentException
         *          <code>authority</code> is <code>null</code>
         */
        
        public AuthorityFilter(String authority) {
            nullarg(authority, "authority");
            this.authority = authority;
            return;
        }

         
        /**
         *  Used by the IdFilterList to filter the Id list based
         *  on authority.
         *
         *  @param id the Id
         *  @return <code>true</code> to pass the Id,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.id.Id id) {
            return (id.getAuthority().equals(this.authority));
        }
    }
}
