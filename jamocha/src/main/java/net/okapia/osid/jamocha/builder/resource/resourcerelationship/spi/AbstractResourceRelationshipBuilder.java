//
// AbstractResourceRelationship.java
//
//     Defines a ResourceRelationship builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.resourcerelationship.spi;


/**
 *  Defines a <code>ResourceRelationship</code> builder.
 */

public abstract class AbstractResourceRelationshipBuilder<T extends AbstractResourceRelationshipBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.resource.resourcerelationship.ResourceRelationshipMiter resourceRelationship;


    /**
     *  Constructs a new <code>AbstractResourceRelationshipBuilder</code>.
     *
     *  @param resourceRelationship the resource relationship to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractResourceRelationshipBuilder(net.okapia.osid.jamocha.builder.resource.resourcerelationship.ResourceRelationshipMiter resourceRelationship) {
        super(resourceRelationship);
        this.resourceRelationship = resourceRelationship;
        return;
    }


    /**
     *  Builds the resource relationship.
     *
     *  @return the new resource relationship
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.resource.ResourceRelationship build() {
        (new net.okapia.osid.jamocha.builder.validator.resource.resourcerelationship.ResourceRelationshipValidator(getValidations())).validate(this.resourceRelationship);
        return (new net.okapia.osid.jamocha.builder.resource.resourcerelationship.ImmutableResourceRelationship(this.resourceRelationship));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the resource relationship miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.resource.resourcerelationship.ResourceRelationshipMiter getMiter() {
        return (this.resourceRelationship);
    }


    /**
     *  Sets the source resource.
     *
     *  @param resource a source resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T sourceResource(org.osid.resource.Resource resource) {
        getMiter().setSourceResource(resource);
        return (self());
    }


    /**
     *  Sets the destination resource.
     *
     *  @param resource a destination resource
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    public T destinationResource(org.osid.resource.Resource resource) {
        getMiter().setDestinationResource(resource);
        return (self());
    }


    /**
     *  Adds a ResourceRelationship record.
     *
     *  @param record a resource relationship record
     *  @param recordType the type of resource relationship record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.resource.records.ResourceRelationshipRecord record, org.osid.type.Type recordType) {
        getMiter().addResourceRelationshipRecord(record, recordType);
        return (self());
    }
}       


