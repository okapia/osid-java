//
// AbstractAuctionConstrainerSearch.java
//
//     A template for making an AuctionConstrainer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing auction constrainer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAuctionConstrainerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.bidding.rules.AuctionConstrainerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.bidding.rules.AuctionConstrainerSearchOrder auctionConstrainerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of auction constrainers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  auctionConstrainerIds list of auction constrainers
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAuctionConstrainers(org.osid.id.IdList auctionConstrainerIds) {
        while (auctionConstrainerIds.hasNext()) {
            try {
                this.ids.add(auctionConstrainerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAuctionConstrainers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of auction constrainer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAuctionConstrainerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  auctionConstrainerSearchOrder auction constrainer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>auctionConstrainerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAuctionConstrainerResults(org.osid.bidding.rules.AuctionConstrainerSearchOrder auctionConstrainerSearchOrder) {
	this.auctionConstrainerSearchOrder = auctionConstrainerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.bidding.rules.AuctionConstrainerSearchOrder getAuctionConstrainerSearchOrder() {
	return (this.auctionConstrainerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given auction constrainer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction constrainer implementing the requested record.
     *
     *  @param auctionConstrainerSearchRecordType an auction constrainer search record
     *         type
     *  @return the auction constrainer search record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerSearchRecord getAuctionConstrainerSearchRecord(org.osid.type.Type auctionConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.bidding.rules.records.AuctionConstrainerSearchRecord record : this.records) {
            if (record.implementsRecordType(auctionConstrainerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction constrainer search. 
     *
     *  @param auctionConstrainerSearchRecord auction constrainer search record
     *  @param auctionConstrainerSearchRecordType auctionConstrainer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionConstrainerSearchRecord(org.osid.bidding.rules.records.AuctionConstrainerSearchRecord auctionConstrainerSearchRecord, 
                                           org.osid.type.Type auctionConstrainerSearchRecordType) {

        addRecordType(auctionConstrainerSearchRecordType);
        this.records.add(auctionConstrainerSearchRecord);        
        return;
    }
}
