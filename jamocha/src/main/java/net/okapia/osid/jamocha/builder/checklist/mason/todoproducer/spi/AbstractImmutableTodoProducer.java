//
// AbstractImmutableTodoProducer.java
//
//     Wraps a mutable TodoProducer to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.checklist.mason.todoproducer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>TodoProducer</code> to hide modifiers. This
 *  wrapper provides an immutized TodoProducer from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying todoProducer whose state changes are visible.
 */

public abstract class AbstractImmutableTodoProducer
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.checklist.mason.TodoProducer {

    private final org.osid.checklist.mason.TodoProducer todoProducer;


    /**
     *  Constructs a new <code>AbstractImmutableTodoProducer</code>.
     *
     *  @param todoProducer the todo producer to immutablize
     *  @throws org.osid.NullArgumentException <code>todoProducer</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableTodoProducer(org.osid.checklist.mason.TodoProducer todoProducer) {
        super(todoProducer);
        this.todoProducer = todoProducer;
        return;
    }


    /**
     *  Tests if a todo should be created or destroyed. 
     *
     *  @return <code> true </code> if a creation rule, <code> false </code> 
     *          if a destruction rule 
     */

    @OSID @Override
    public boolean isCreationRule() {
        return (this.todoProducer.isCreationRule());
    }


    /**
     *  Tests if a todo should be produced based on a time cycle. 
     *
     *  @return <code> true </code> if based on a timecycle, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isBasedOnTimeCycle() {
        return (this.todoProducer.isBasedOnTimeCycle());
    }


    /**
     *  Gets the cyclic event <code> Id. </code> 
     *
     *  @return the cyclic event <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isBasedOnTimeCycle() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCyclicEventId() {
        return (this.todoProducer.getCyclicEventId());
    }


    /**
     *  Gets the cyclic event. 
     *
     *  @return the cyclic event 
     *  @throws org.osid.IllegalStateException <code> isBasedOnTimeCycle() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEvent getCyclicEvent()
        throws org.osid.OperationFailedException {

        return (this.todoProducer.getCyclicEvent());
    }


    /**
     *  Tests if a todo should be produced based on an item level in a stock. 
     *
     *  @return <code> true </code> if based on a stock, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean isBasedOnStock() {
        return (this.todoProducer.isBasedOnStock());
    }


    /**
     *  Gets the stock level. 
     *
     *  @return the stock level 
     *  @throws org.osid.IllegalStateException <code> isBasedOnStock() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public long getStockLevel() {
        return (this.todoProducer.getStockLevel());
    }


    /**
     *  Gets the stock <code> Id. </code> 
     *
     *  @return the stock <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isBasedOnStock() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStockId() {
        return (this.todoProducer.getStockId());
    }


    /**
     *  Gets the stock. 
     *
     *  @return the stock 
     *  @throws org.osid.IllegalStateException <code> isBasedOnStock() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock()
        throws org.osid.OperationFailedException {

        return (this.todoProducer.getStock());
    }


    /**
     *  Gets the todo producer record corresponding to the given <code> 
     *  TodoProducer </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  todoProducerRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(todoProducerRecordType) </code> is <code> true </code> . 
     *
     *  @param  todoProducerRecordType the type of todo producer record to 
     *          retrieve 
     *  @return the todo producer record 
     *  @throws org.osid.NullArgumentException <code> todoProducerRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(todoProducerRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerRecord getTodoProducerRecord(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException {

        return (this.todoProducer.getTodoProducerRecord(todoProducerRecordType));
    }
}

