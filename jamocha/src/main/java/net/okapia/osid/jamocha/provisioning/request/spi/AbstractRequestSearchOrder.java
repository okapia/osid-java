//
// AbstractRequestSearchOdrer.java
//
//     Defines a RequestSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.request.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code RequestSearchOrder}.
 */

public abstract class AbstractRequestSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.provisioning.RequestSearchOrder {

    private final java.util.Collection<org.osid.provisioning.records.RequestSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by the request transaction. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequestTransaction(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a request transaction search order is available. 
     *
     *  @return <code> true </code> if a request transaction search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the request transaction search order. 
     *
     *  @return the request transaction search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRequestTransactionSearchOrder() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionSearchOrder getRequestTransactionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRequestTransactionSearchOrder() is false");
    }


    /**
     *  Orders the results by queue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQueue(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a queue search order is available. 
     *
     *  @return <code> true </code> if a queue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the queue search order. 
     *
     *  @return the queue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsQueueSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSearchOrder getQueueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsQueueSearchOrder() is false");
    }


    /**
     *  Orders the results by request date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequestDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the requester. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequester(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequesterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRequesterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getRequesterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRequesterSearchOrder() is false");
    }


    /**
     *  Orders the results by requesting agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRequestingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRequestingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getRequestingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRequestingAgentSearchOrder() is false");
    }


    /**
     *  Orders the results by pool. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPool(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a pool search order is available. 
     *
     *  @return <code> true </code> if a pool search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolSearchOrder() {
        return (false);
    }


    /**
     *  Gets the pool search order. 
     *
     *  @return the pool search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsPoolSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchOrder getPoolSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPoolSearchOrder() is false");
    }


    /**
     *  Orders the results by exchange resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByExchangeProvision(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an exchange provision search order is available. 
     *
     *  @return <code> true </code> if a provision search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExchangeProvisionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the exchange provision search order. 
     *
     *  @return the provision search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsExchangeProvisionSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchOrder getExchangeProvisionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsExchangeProvisionSearchOrder() is false");
    }


    /**
     *  Orders the results by origin provision. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOriginProvision(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an origin provision search order is available. 
     *
     *  @return <code> true </code> if a provision search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOriginProvisionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the origin provision search order. 
     *
     *  @return the provision search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsOriginProvisionSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchOrder getOriginProvisionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsOriginProvisionSearchOrder() is false");
    }


    /**
     *  Orders the results by position. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPosition(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the estimated waiting time. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEWA(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  requestRecordType a request record type 
     *  @return {@code true} if the requestRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code requestRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type requestRecordType) {
        for (org.osid.provisioning.records.RequestSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(requestRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  requestRecordType the request record type 
     *  @return the request search order record
     *  @throws org.osid.NullArgumentException
     *          {@code requestRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(requestRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestSearchOrderRecord getRequestSearchOrderRecord(org.osid.type.Type requestRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(requestRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this request. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param requestRecord the request search odrer record
     *  @param requestRecordType request record type
     *  @throws org.osid.NullArgumentException
     *          {@code requestRecord} or
     *          {@code requestRecordTyperequest} is
     *          {@code null}
     */
            
    protected void addRequestRecord(org.osid.provisioning.records.RequestSearchOrderRecord requestSearchOrderRecord, 
                                     org.osid.type.Type requestRecordType) {

        addRecordType(requestRecordType);
        this.records.add(requestSearchOrderRecord);
        
        return;
    }
}
