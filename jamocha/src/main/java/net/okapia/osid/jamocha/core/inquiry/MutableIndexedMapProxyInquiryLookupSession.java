//
// MutableIndexedMapProxyInquiryLookupSession
//
//    Implements an Inquiry lookup service backed by a collection of
//    inquiries indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements an Inquiry lookup service backed by a collection of
 *  inquiries. The inquiries are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some inquiries may be compatible
 *  with more types than are indicated through these inquiry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of inquiries can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyInquiryLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractIndexedMapInquiryLookupSession
    implements org.osid.inquiry.InquiryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInquiryLookupSession} with
     *  no inquiry.
     *
     *  @param inquest the inquest
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                                       org.osid.proxy.Proxy proxy) {
        setInquest(inquest);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInquiryLookupSession} with
     *  a single inquiry.
     *
     *  @param inquest the inquest
     *  @param  inquiry an inquiry
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiry}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                                       org.osid.inquiry.Inquiry inquiry, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putInquiry(inquiry);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInquiryLookupSession} using
     *  an array of inquiries.
     *
     *  @param inquest the inquest
     *  @param  inquiries an array of inquiries
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiries}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                                       org.osid.inquiry.Inquiry[] inquiries, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putInquiries(inquiries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyInquiryLookupSession} using
     *  a collection of inquiries.
     *
     *  @param inquest the inquest
     *  @param  inquiries a collection of inquiries
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code inquiries}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyInquiryLookupSession(org.osid.inquiry.Inquest inquest,
                                                       java.util.Collection<? extends org.osid.inquiry.Inquiry> inquiries,
                                                       org.osid.proxy.Proxy proxy) {
        this(inquest, proxy);
        putInquiries(inquiries);
        return;
    }

    
    /**
     *  Makes an {@code Inquiry} available in this session.
     *
     *  @param  inquiry an inquiry
     *  @throws org.osid.NullArgumentException {@code inquiry{@code 
     *          is {@code null}
     */

    @Override
    public void putInquiry(org.osid.inquiry.Inquiry inquiry) {
        super.putInquiry(inquiry);
        return;
    }


    /**
     *  Makes an array of inquiries available in this session.
     *
     *  @param  inquiries an array of inquiries
     *  @throws org.osid.NullArgumentException {@code inquiries{@code 
     *          is {@code null}
     */

    @Override
    public void putInquiries(org.osid.inquiry.Inquiry[] inquiries) {
        super.putInquiries(inquiries);
        return;
    }


    /**
     *  Makes collection of inquiries available in this session.
     *
     *  @param  inquiries a collection of inquiries
     *  @throws org.osid.NullArgumentException {@code inquiry{@code 
     *          is {@code null}
     */

    @Override
    public void putInquiries(java.util.Collection<? extends org.osid.inquiry.Inquiry> inquiries) {
        super.putInquiries(inquiries);
        return;
    }


    /**
     *  Removes an Inquiry from this session.
     *
     *  @param inquiryId the {@code Id} of the inquiry
     *  @throws org.osid.NullArgumentException {@code inquiryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeInquiry(org.osid.id.Id inquiryId) {
        super.removeInquiry(inquiryId);
        return;
    }    
}
