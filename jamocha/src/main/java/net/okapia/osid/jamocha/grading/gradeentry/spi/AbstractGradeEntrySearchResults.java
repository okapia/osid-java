//
// AbstractGradeEntrySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradeentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractGradeEntrySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.grading.GradeEntrySearchResults {

    private org.osid.grading.GradeEntryList gradeEntries;
    private final org.osid.grading.GradeEntryQueryInspector inspector;
    private final java.util.Collection<org.osid.grading.records.GradeEntrySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractGradeEntrySearchResults.
     *
     *  @param gradeEntries the result set
     *  @param gradeEntryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>gradeEntries</code>
     *          or <code>gradeEntryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractGradeEntrySearchResults(org.osid.grading.GradeEntryList gradeEntries,
                                            org.osid.grading.GradeEntryQueryInspector gradeEntryQueryInspector) {
        nullarg(gradeEntries, "grade entries");
        nullarg(gradeEntryQueryInspector, "grade entry query inspectpr");

        this.gradeEntries = gradeEntries;
        this.inspector = gradeEntryQueryInspector;

        return;
    }


    /**
     *  Gets the grade entry list resulting from a search.
     *
     *  @return a grade entry list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntries() {
        if (this.gradeEntries == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.grading.GradeEntryList gradeEntries = this.gradeEntries;
        this.gradeEntries = null;
	return (gradeEntries);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.grading.GradeEntryQueryInspector getGradeEntryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  grade entry search record <code> Type. </code> This method must
     *  be used to retrieve a gradeEntry implementing the requested
     *  record.
     *
     *  @param gradeEntrySearchRecordType a gradeEntry search 
     *         record type 
     *  @return the grade entry search
     *  @throws org.osid.NullArgumentException
     *          <code>gradeEntrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(gradeEntrySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeEntrySearchResultsRecord getGradeEntrySearchResultsRecord(org.osid.type.Type gradeEntrySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.grading.records.GradeEntrySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(gradeEntrySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(gradeEntrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record grade entry search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addGradeEntryRecord(org.osid.grading.records.GradeEntrySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "grade entry record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
