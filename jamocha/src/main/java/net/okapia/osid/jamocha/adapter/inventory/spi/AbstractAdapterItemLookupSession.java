//
// AbstractAdapterItemLookupSession.java
//
//    An Item lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Item lookup session adapter.
 */

public abstract class AbstractAdapterItemLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inventory.ItemLookupSession {

    private final org.osid.inventory.ItemLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterItemLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterItemLookupSession(org.osid.inventory.ItemLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Warehouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Warehouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the {@code Warehouse} associated with this session.
     *
     *  @return the {@code Warehouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform {@code Item} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupItems() {
        return (this.session.canLookupItems());
    }


    /**
     *  A complete view of the {@code Item} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeItemView() {
        this.session.useComparativeItemView();
        return;
    }


    /**
     *  A complete view of the {@code Item} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryItemView() {
        this.session.usePlenaryItemView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include items in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the {@code Item} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Item} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Item} and
     *  retained for compatibility.
     *
     *  @param itemId {@code Id} of the {@code Item}
     *  @return the item
     *  @throws org.osid.NotFoundException {@code itemId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code itemId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItem(itemId));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  items specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Items} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  itemIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code itemIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByIds(org.osid.id.IdList itemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByIds(itemIds));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  item genus {@code Type} which does not include
     *  items of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByGenusType(itemGenusType));
    }


    /**
     *  Gets an {@code ItemList} corresponding to the given
     *  item genus {@code Type} and include any additional
     *  items with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByParentGenusType(itemGenusType));
    }


    /**
     *  Gets an {@code ItemList} containing the given
     *  item record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned {@code Item} list
     *  @throws org.osid.NullArgumentException
     *          {@code itemRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItemsByRecordType(itemRecordType));
    }


    /**
     *  Gets an {@code ItemList} for the given stock. In plenary mode,
     *  the returned list contains all known items or an error
     *  results.  Otherwise, the returned list may contain only those
     *  items that are accessible through this session.
     *
     *  @param  stockId a stock {@code Id} 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code stockId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getItemsByStock(stockId));
    }


    /**
     *  Gets an {@code ItemList} for the given property tag. In
     *  plenary mode, the returned list contains all known items or an
     *  error results.  Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  property a property number 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code property} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByPropertyTag(String property)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getItemsByPropertyTag(property));
    }

    
    /**
     *  Gets an {@code ItemList} for the given serial number. In
     *  plenary mode, the returned list contains all known items or an
     *  error results. Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  serialNumber a serial number 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code serialNumber} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsBySerialNumber(String serialNumber)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getItemsBySerialNumber(serialNumber));
    }


    /**
     *  Gets an {@code ItemList} immediately contained within the
     *  given item. In plenary mode, the returned list contains all
     *  known items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through this
     *  session.
     *
     *  @param  itemId an item {@code Id} 
     *  @return the returned {@code Item} list 
     *  @throws org.osid.NullArgumentException {@code itemId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemParts(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getItemParts(itemId));
    }

    
    /**
     *  Gets all {@code Items}. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Items} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getItems());
    }
}
