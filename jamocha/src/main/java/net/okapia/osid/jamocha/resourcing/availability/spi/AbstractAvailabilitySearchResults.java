//
// AbstractAvailabilitySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.availability.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractAvailabilitySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.AvailabilitySearchResults {

    private org.osid.resourcing.AvailabilityList availabilities;
    private final org.osid.resourcing.AvailabilityQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.records.AvailabilitySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractAvailabilitySearchResults.
     *
     *  @param availabilities the result set
     *  @param availabilityQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>availabilities</code>
     *          or <code>availabilityQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractAvailabilitySearchResults(org.osid.resourcing.AvailabilityList availabilities,
                                            org.osid.resourcing.AvailabilityQueryInspector availabilityQueryInspector) {
        nullarg(availabilities, "availabilities");
        nullarg(availabilityQueryInspector, "availability query inspectpr");

        this.availabilities = availabilities;
        this.inspector = availabilityQueryInspector;

        return;
    }


    /**
     *  Gets the availability list resulting from a search.
     *
     *  @return an availability list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityList getAvailabilities() {
        if (this.availabilities == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.AvailabilityList availabilities = this.availabilities;
        this.availabilities = null;
	return (availabilities);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.AvailabilityQueryInspector getAvailabilityQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  availability search record <code> Type. </code> This method must
     *  be used to retrieve an availability implementing the requested
     *  record.
     *
     *  @param availabilitySearchRecordType an availability search 
     *         record type 
     *  @return the availability search
     *  @throws org.osid.NullArgumentException
     *          <code>availabilitySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(availabilitySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilitySearchResultsRecord getAvailabilitySearchResultsRecord(org.osid.type.Type availabilitySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.records.AvailabilitySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(availabilitySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(availabilitySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record availability search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addAvailabilityRecord(org.osid.resourcing.records.AvailabilitySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "availability record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
