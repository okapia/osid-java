//
// AbstractProfileEntryEnablerQueryInspector.java
//
//     A template for making a ProfileEntryEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.profile.rules.profileentryenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for profile entry enablers.
 */

public abstract class AbstractProfileEntryEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.profile.rules.ProfileEntryEnablerQueryInspector {

    private final java.util.Collection<org.osid.profile.rules.records.ProfileEntryEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the ruled profile entry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledProfileEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ruled profile entry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileEntryQueryInspector[] getRuledProfileEntryTerms() {
        return (new org.osid.profile.ProfileEntryQueryInspector[0]);
    }


    /**
     *  Gets the profile <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProfileIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the profile query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.profile.ProfileQueryInspector[] getProfileTerms() {
        return (new org.osid.profile.ProfileQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given profile entry enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a profile entry enabler implementing the requested record.
     *
     *  @param profileEntryEnablerRecordType a profile entry enabler record type
     *  @return the profile entry enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>profileEntryEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(profileEntryEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.profile.rules.records.ProfileEntryEnablerQueryInspectorRecord getProfileEntryEnablerQueryInspectorRecord(org.osid.type.Type profileEntryEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.profile.rules.records.ProfileEntryEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(profileEntryEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(profileEntryEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this profile entry enabler query. 
     *
     *  @param profileEntryEnablerQueryInspectorRecord profile entry enabler query inspector
     *         record
     *  @param profileEntryEnablerRecordType profileEntryEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProfileEntryEnablerQueryInspectorRecord(org.osid.profile.rules.records.ProfileEntryEnablerQueryInspectorRecord profileEntryEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type profileEntryEnablerRecordType) {

        addRecordType(profileEntryEnablerRecordType);
        nullarg(profileEntryEnablerRecordType, "profile entry enabler record type");
        this.records.add(profileEntryEnablerQueryInspectorRecord);        
        return;
    }
}
