//
// AbstractLoggingBatchManager.java
//
//     An adapter for a LoggingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.logging.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a LoggingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterLoggingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.logging.batch.LoggingBatchManager>
    implements org.osid.logging.batch.LoggingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterLoggingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterLoggingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterLoggingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterLoggingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of log entries is available. 
     *
     *  @return <code> true </code> if a log entry bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryBatchAdmin() {
        return (getAdapteeManager().supportsLogEntryBatchAdmin());
    }


    /**
     *  Tests if bulk purging of log entries is available. 
     *
     *  @return <code> true </code> if a log entry purge service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryPurgeSession() {
        return (getAdapteeManager().supportsLogEntryPurgeSession());
    }


    /**
     *  Tests if bulk administration of logs is available. 
     *
     *  @return <code> true </code> if a log bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogBatchAdmin() {
        return (getAdapteeManager().supportsLogBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log entry 
     *  administration service. 
     *
     *  @return a <code> LogEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryBatchAdminSession getLogEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log entry 
     *  administration service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryBatchAdminSession getLogEntryBatchAdminSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryBatchAdminSessionForLog(logId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  purge service. 
     *
     *  @return a <code> LogEntryPurgeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryPurge() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryPurgeSession getLogEntryPurgeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryPurgeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  purge service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryPurgeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryPurge() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryPurgeSession getLogEntryPurgeSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryPurgeSessionForLog(logId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log 
     *  administration service. 
     *
     *  @return a <code> LogBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogBatchAdminSession getLogBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
