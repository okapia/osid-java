//
// SubscriptionElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.subscription.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class SubscriptionElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the SubscriptionElement Id.
     *
     *  @return the subscription element Id
     */

    public static org.osid.id.Id getSubscriptionEntityId() {
        return (makeEntityId("osid.subscription.Subscription"));
    }


    /**
     *  Gets the DispatchId element Id.
     *
     *  @return the DispatchId element Id
     */

    public static org.osid.id.Id getDispatchId() {
        return (makeElementId("osid.subscription.subscription.DispatchId"));
    }


    /**
     *  Gets the Dispatch element Id.
     *
     *  @return the Dispatch element Id
     */

    public static org.osid.id.Id getDispatch() {
        return (makeElementId("osid.subscription.subscription.Dispatch"));
    }


    /**
     *  Gets the SubscriberId element Id.
     *
     *  @return the SubscriberId element Id
     */

    public static org.osid.id.Id getSubscriberId() {
        return (makeElementId("osid.subscription.subscription.SubscriberId"));
    }


    /**
     *  Gets the Subscriber element Id.
     *
     *  @return the Subscriber element Id
     */

    public static org.osid.id.Id getSubscriber() {
        return (makeElementId("osid.subscription.subscription.Subscriber"));
    }


    /**
     *  Gets the AddressId element Id.
     *
     *  @return the AddressId element Id
     */

    public static org.osid.id.Id getAddressId() {
        return (makeElementId("osid.subscription.subscription.AddressId"));
    }


    /**
     *  Gets the Address element Id.
     *
     *  @return the Address element Id
     */

    public static org.osid.id.Id getAddress() {
        return (makeElementId("osid.subscription.subscription.Address"));
    }


    /**
     *  Gets the PublisherId element Id.
     *
     *  @return the PublisherId element Id
     */

    public static org.osid.id.Id getPublisherId() {
        return (makeQueryElementId("osid.subscription.subscription.PublisherId"));
    }


    /**
     *  Gets the Publisher element Id.
     *
     *  @return the Publisher element Id
     */

    public static org.osid.id.Id getPublisher() {
        return (makeQueryElementId("osid.subscription.subscription.Publisher"));
    }
}
