//
// AbstractChecklistMasonManager.java
//
//     An adapter for a ChecklistMasonManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ChecklistMasonManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterChecklistMasonManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.checklist.mason.ChecklistMasonManager>
    implements org.osid.checklist.mason.ChecklistMasonManager {


    /**
     *  Constructs a new {@code AbstractAdapterChecklistMasonManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterChecklistMasonManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterChecklistMasonManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterChecklistMasonManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up todo producers is supported. 
     *
     *  @return <code> true </code> if todo producer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerLookup() {
        return (getAdapteeManager().supportsTodoProducerLookup());
    }


    /**
     *  Tests if querying todo producers is supported. 
     *
     *  @return <code> true </code> if todo producer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerQuery() {
        return (getAdapteeManager().supportsTodoProducerQuery());
    }


    /**
     *  Tests if searching todo producers is supported. 
     *
     *  @return <code> true </code> if todo producer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerSearch() {
        return (getAdapteeManager().supportsTodoProducerSearch());
    }


    /**
     *  Tests if a todo producer administrative service is supported. 
     *
     *  @return <code> true </code> if todo producer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerAdmin() {
        return (getAdapteeManager().supportsTodoProducerAdmin());
    }


    /**
     *  Tests if a todo producer notification service is supported. 
     *
     *  @return <code> true </code> if todo producer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerNotification() {
        return (getAdapteeManager().supportsTodoProducerNotification());
    }


    /**
     *  Tests if a todo producer checklist lookup service is supported. 
     *
     *  @return <code> true </code> if a todo producer checklist lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerChecklist() {
        return (getAdapteeManager().supportsTodoProducerChecklist());
    }


    /**
     *  Tests if a todo producer checklist service is supported. 
     *
     *  @return <code> true </code> if todo producer checklist assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerChecklistAssignment() {
        return (getAdapteeManager().supportsTodoProducerChecklistAssignment());
    }


    /**
     *  Tests if a todo producer checklist lookup service is supported. 
     *
     *  @return <code> true </code> if a todo producer checklist service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTodoProducerSmartChecklist() {
        return (getAdapteeManager().supportsTodoProducerSmartChecklist());
    }


    /**
     *  Gets the supported <code> TodoProducer </code> record types. 
     *
     *  @return a list containing the supported <code> TodoProducer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTodoProducerRecordTypes() {
        return (getAdapteeManager().getTodoProducerRecordTypes());
    }


    /**
     *  Tests if the given <code> TodoProducer </code> record type is 
     *  supported. 
     *
     *  @param  todoProducerRecordType a <code> Type </code> indicating a 
     *          <code> TodoProducer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> todoProducerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTodoProducerRecordType(org.osid.type.Type todoProducerRecordType) {
        return (getAdapteeManager().supportsTodoProducerRecordType(todoProducerRecordType));
    }


    /**
     *  Gets the supported <code> TodoProducer </code> search record types. 
     *
     *  @return a list containing the supported <code> TodoProducer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTodoProducerSearchRecordTypes() {
        return (getAdapteeManager().getTodoProducerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> TodoProducer </code> search record type is 
     *  supported. 
     *
     *  @param  todoProducerSearchRecordType a <code> Type </code> indicating 
     *          a <code> TodoProducer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          todoProducerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTodoProducerSearchRecordType(org.osid.type.Type todoProducerSearchRecordType) {
        return (getAdapteeManager().supportsTodoProducerSearchRecordType(todoProducerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  lookup service. 
     *
     *  @return a <code> TodoProducerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerLookupSession getTodoProducerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  lookup service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerLookupSession getTodoProducerLookupSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerLookupSessionForChecklist(checklistId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  query service. 
     *
     *  @return a <code> TodoProducerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerQuerySession getTodoProducerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  query service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerQuerySession getTodoProducerQuerySessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerQuerySessionForChecklist(checklistId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  search service. 
     *
     *  @return a <code> TodoProducerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSearchSession getTodoProducerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producers 
     *  earch service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSearchSession getTodoProducerSearchSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerSearchSessionForChecklist(checklistId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  administration service. 
     *
     *  @return a <code> TodoProducerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerAdminSession getTodoProducerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  administration service for the given checklist. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerAdminSession getTodoProducerAdminSessionForChecklist(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerAdminSessionForChecklist(checklistId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  notification service. 
     *
     *  @param  todoProducerReceiver the notification callback 
     *  @return a <code> TodoProducerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> todoProducerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerNotificationSession getTodoProducerNotificationSession(org.osid.checklist.mason.TodoProducerReceiver todoProducerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerNotificationSession(todoProducerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the todo producer 
     *  notification service for the given checklist. 
     *
     *  @param  todoProducerReceiver the notification callback 
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no checklist found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> todoProducerReceiver 
     *          </code> or <code> checklistId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerNotificationSession getTodoProducerNotificationSessionForChecklist(org.osid.checklist.mason.TodoProducerReceiver todoProducerReceiver, 
                                                                                                                   org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerNotificationSessionForChecklist(todoProducerReceiver, checklistId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup todo producer/checklist 
     *  mappings for todo producers. 
     *
     *  @return a <code> TodoProducerChecklistSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerChecklist() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerChecklistSession getTodoProducerChecklistSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerChecklistSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning todo 
     *  producers to checklists for todo. 
     *
     *  @return a <code> TodoProducerChecklistAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerChecklistAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerChecklistAssignmentSession getTodoProducerChecklistAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerChecklistAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage todo producer smart 
     *  checklists. 
     *
     *  @param  checklistId the <code> Id </code> of the <code> Checklist 
     *          </code> 
     *  @return a <code> TodoProducerSmartChecklistSession </code> 
     *  @throws org.osid.NotFoundException no <code> Checklist </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTodoProducerSmartChecklist() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerSmartChecklistSession getTodoProducerSmartChecklistSession(org.osid.id.Id checklistId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTodoProducerSmartChecklistSession(checklistId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
