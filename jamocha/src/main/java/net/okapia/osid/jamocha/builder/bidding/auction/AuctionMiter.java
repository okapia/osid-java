//
// AuctionMiter.java
//
//     Defines an Auction miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.auction;


/**
 *  Defines an <code>Auction</code> miter for use with the builders.
 */

public interface AuctionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidGovernatorMiter,
            net.okapia.osid.jamocha.builder.spi.TemporalMiter,
            org.osid.bidding.Auction {


    /**
     *  Sets the currency type.
     *
     *  @param currencyType a currency type
     *  @throws org.osid.NullArgumentException
     *          <code>currencyType</code> is <code>null</code>
     */

    public void setCurrencyType(org.osid.type.Type currencyType);


    /**
     *  Sets the minimum bidders.
     *
     *  @param bidders a minimum bidders
     *  @throws org.osid.NullArgumentException <code>bidders</code> is
     *          <code>null</code>
     */

    public void setMinimumBidders(long bidders);


    /**
     *  Sets the sealed flag.
     *
     *  @param sealed <code> true </code> if this auction is sealed,
     *         <code> false </code> otherwise
     */

    public void setSealed(boolean sealed);


    /**
     *  Sets the seller.
     *
     *  @param seller a seller
     *  @throws org.osid.NullArgumentException <code>seller</code> is
     *          <code>null</code>
     */

    public void setSeller(org.osid.resource.Resource seller);


    /**
     *  Sets the item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException <code>item</code> is
     *          <code>null</code>
     */

    public void setItem(org.osid.resource.Resource item);


    /**
     *  Sets the lot size.
     *
     *  @param size a lot size
     *  @throws org.osid.NullArgumentException <code>size</code> is
     *          <code>null</code>
     */

    public void setLotSize(long size);


    /**
     *  Sets the remaining items.
     *
     *  @param items a remaining items
     *  @throws org.osid.NullArgumentException <code>items</code> is
     *          <code>null</code>
     */

    public void setRemainingItems(long items);


    /**
     *  Sets the item limit.
     *
     *  @param limit an item limit
     *  @throws org.osid.NullArgumentException <code>limit</code> is
     *          <code>null</code>
     */

    public void setItemLimit(long limit);


    /**
     *  Sets the starting price.
     *
     *  @param price a starting price
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    public void setStartingPrice(org.osid.financials.Currency price);


    /**
     *  Sets the price increment.
     *
     *  @param increment a price increment
     *  @throws org.osid.NullArgumentException <code>increment</code>
     *          is <code>null</code>
     */

    public void setPriceIncrement(org.osid.financials.Currency increment);


    /**
     *  Sets the reserve price.
     *
     *  @param price a reserve price
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    public void setReservePrice(org.osid.financials.Currency price);


    /**
     *  Sets the buyout price.
     *
     *  @param price a buyout price
     *  @throws org.osid.NullArgumentException <code>price</code> is
     *          <code>null</code>
     */

    public void setBuyoutPrice(org.osid.financials.Currency price);


    /**
     *  Adds an Auction record.
     *
     *  @param record an auction record
     *  @param recordType the type of auction record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAuctionRecord(org.osid.bidding.records.AuctionRecord record, org.osid.type.Type recordType);
}       


