//
// AbstractEvent.java
//
//     Defines an Event builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.event.spi;


/**
 *  Defines an <code>Event</code> builder.
 */

public abstract class AbstractEventBuilder<T extends AbstractEventBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractTemporalOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.event.EventMiter event;


    /**
     *  Constructs a new <code>AbstractEventBuilder</code>.
     *
     *  @param event the event to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractEventBuilder(net.okapia.osid.jamocha.builder.calendaring.event.EventMiter event) {
        super(event);
        this.event = event;
        return;
    }


    /**
     *  Builds the event.
     *
     *  @return the new event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.Event build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.event.EventValidator(getValidations())).validate(this.event);
        return (new net.okapia.osid.jamocha.builder.calendaring.event.ImmutableEvent(this.event));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the event miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.event.EventMiter getMiter() {
        return (this.event);
    }


    /**
     *  Sets the sequestered flag.
     */

    public T sequestered() {
        getMiter().setSequestered(true);
        return (self());
    }


    /**
     *  Sets the unsequestered flag.
     */

    public T unsequestered() {
        getMiter().setSequestered(false);
        return (self());
    }


    /**
     *  Sets the implicit flag.
     *
     *  @return the builder
     */

    public T implicit() {
        getMiter().setImplicit(true);
        return (self());
    }


    /**
     *  Unsets the implicit flag.
     *
     *  @return the builder
     */

    public T explicit() {
        getMiter().setImplicit(false);
        return (self());
    }


    /**
     *  Sets the in recurring series flag.
     *
     *  @return the ebuilder
     */

    public T recurring() {
        getMiter().setInRecurringSeries(true);
        return (self());
    }


    /**
     *  Unsets the in recurring series flag.
     *
     *  @return the ebuilder
     */

    public T notRecurring() {
        getMiter().setInRecurringSeries(false);
        return (self());
    }


    /**
     *  Sets the superseding event flag.
     *
     *  @return the builder
     */

    public T superseding() {
        getMiter().setSupersedingEvent(true);
        return (self());
    }


    /**
     *  Unsets the superseding event flag.
     *
     *  @return the builder
     */

    public T notSuperseding() {
        getMiter().setSupersedingEvent(false);
        return (self());
    }



    /**
     *  Sets the offset event flag.
     *
     *  @return the builder
     */

    public T offset() {
        getMiter().setOffsetEvent(true);
        return (self());
    }


    /**
     *  Unsets the offset event flag.
     *
     *  @return the builder
     */

    public T notOffset() {
        getMiter().setOffsetEvent(false);
        return (self());
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    public T duration(org.osid.calendaring.Duration duration) {
        getMiter().setDuration(duration);
        return (self());
    }


    /**
     *  Sets the location description.
     *
     *  @param locationDescription a location description
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>locationDescription</code> is <code>null</code>
     */

    public T locationDescription(org.osid.locale.DisplayText locationDescription) {
        getMiter().setLocationDescription(locationDescription);
        return (self());
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>location</code> is <code>null</code>
     */

    public T location(org.osid.mapping.Location location) {
        getMiter().setLocation(location);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Adds an Event record.
     *
     *  @param record an event record
     *  @param recordType the type of event record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.records.EventRecord record, org.osid.type.Type recordType) {
        getMiter().addEventRecord(record, recordType);
        return (self());
    }
}       


