//
// AbstractRecipeManager.java
//
//     An adapter for a RecipeManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RecipeManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRecipeManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.recipe.RecipeManager>
    implements org.osid.recipe.RecipeManager {


    /**
     *  Constructs a new {@code AbstractAdapterRecipeManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRecipeManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRecipeManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRecipeManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any cook book federation is exposed. Federation is exposed 
     *  when a specific cook book may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of cook books appears as a single cookbook. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up recipes is supported. 
     *
     *  @return <code> true </code> if recipe lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeLookup() {
        return (getAdapteeManager().supportsRecipeLookup());
    }


    /**
     *  Tests if querying recipes is supported. 
     *
     *  @return <code> true </code> if recipe query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeQuery() {
        return (getAdapteeManager().supportsRecipeQuery());
    }


    /**
     *  Tests if searching recipes is supported. 
     *
     *  @return <code> true </code> if recipe search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeSearch() {
        return (getAdapteeManager().supportsRecipeSearch());
    }


    /**
     *  Tests if recipe administrative service is supported. 
     *
     *  @return <code> true </code> if recipe administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeAdmin() {
        return (getAdapteeManager().supportsRecipeAdmin());
    }


    /**
     *  Tests if a recipe notification service is supported. 
     *
     *  @return <code> true </code> if recipe notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeNotification() {
        return (getAdapteeManager().supportsRecipeNotification());
    }


    /**
     *  Tests if a recipe cook book lookup service is supported. 
     *
     *  @return <code> true </code> if a recipe cook book lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeCookbook() {
        return (getAdapteeManager().supportsRecipeCookbook());
    }


    /**
     *  Tests if a recipe cook book service is supported. 
     *
     *  @return <code> true </code> if recipe to cook book assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeCookbookAssignment() {
        return (getAdapteeManager().supportsRecipeCookbookAssignment());
    }


    /**
     *  Tests if a recipe smart cook book lookup service is supported. 
     *
     *  @return <code> true </code> if a recipe smart cook book service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeSmartCookbook() {
        return (getAdapteeManager().supportsRecipeSmartCookbook());
    }


    /**
     *  Tests if looking up directions is supported. 
     *
     *  @return <code> true </code> if direction lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionLookup() {
        return (getAdapteeManager().supportsDirectionLookup());
    }


    /**
     *  Tests if querying directions is supported. 
     *
     *  @return <code> true </code> if direction query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionQuery() {
        return (getAdapteeManager().supportsDirectionQuery());
    }


    /**
     *  Tests if searching directions is supported. 
     *
     *  @return <code> true </code> if direction search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionSearch() {
        return (getAdapteeManager().supportsDirectionSearch());
    }


    /**
     *  Tests if direction <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if direction administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionAdmin() {
        return (getAdapteeManager().supportsDirectionAdmin());
    }


    /**
     *  Tests if a direction <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if direction notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionNotification() {
        return (getAdapteeManager().supportsDirectionNotification());
    }


    /**
     *  Tests if a direction cook book lookup service is supported. 
     *
     *  @return <code> true </code> if a direction cook book lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionCookbook() {
        return (getAdapteeManager().supportsDirectionCookbook());
    }


    /**
     *  Tests if a direction cook book assignment service is supported. 
     *
     *  @return <code> true </code> if a direction to cook book assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionCookbookAssignment() {
        return (getAdapteeManager().supportsDirectionCookbookAssignment());
    }


    /**
     *  Tests if a direction smart cook book service is supported. 
     *
     *  @return <code> true </code> if a direction smart cook book service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionSmartCookbook() {
        return (getAdapteeManager().supportsDirectionSmartCookbook());
    }


    /**
     *  Tests for the availability of a procedure lookup service. 
     *
     *  @return <code> true </code> if procedure lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureLookup() {
        return (getAdapteeManager().supportsProcedureLookup());
    }


    /**
     *  Tests if querying procedures is available. 
     *
     *  @return <code> true </code> if procedure query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureQuery() {
        return (getAdapteeManager().supportsProcedureQuery());
    }


    /**
     *  Tests if searching for procedures is available. 
     *
     *  @return <code> true </code> if procedure search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureSearch() {
        return (getAdapteeManager().supportsProcedureSearch());
    }


    /**
     *  Tests if searching for procedures is available. 
     *
     *  @return <code> true </code> if procedure search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureAdmin() {
        return (getAdapteeManager().supportsProcedureAdmin());
    }


    /**
     *  Tests if procedure notification is available. 
     *
     *  @return <code> true </code> if procedure notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureNotification() {
        return (getAdapteeManager().supportsProcedureNotification());
    }


    /**
     *  Tests if a procedure to cook book lookup session is available. 
     *
     *  @return <code> true </code> if procedure cook book lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureCookbook() {
        return (getAdapteeManager().supportsProcedureCookbook());
    }


    /**
     *  Tests if a procedure to cook book assignment session is available. 
     *
     *  @return <code> true </code> if procedure cook book assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureCookbookAssignment() {
        return (getAdapteeManager().supportsProcedureCookbookAssignment());
    }


    /**
     *  Tests if a procedure smart cook book session is available. 
     *
     *  @return <code> true </code> if procedure smart cook book is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureSmartCookbook() {
        return (getAdapteeManager().supportsProcedureSmartCookbook());
    }


    /**
     *  Tests for the availability of a cook book lookup service. 
     *
     *  @return <code> true </code> if cook book lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookLookup() {
        return (getAdapteeManager().supportsCookbookLookup());
    }


    /**
     *  Tests if querying cook books is available. 
     *
     *  @return <code> true </code> if cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookQuery() {
        return (getAdapteeManager().supportsCookbookQuery());
    }


    /**
     *  Tests if searching for cook books is available. 
     *
     *  @return <code> true </code> if cook book search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookSearch() {
        return (getAdapteeManager().supportsCookbookSearch());
    }


    /**
     *  Tests for the availability of a cook book administrative service for 
     *  creating and deleting cook books. 
     *
     *  @return <code> true </code> if cook book administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookAdmin() {
        return (getAdapteeManager().supportsCookbookAdmin());
    }


    /**
     *  Tests for the availability of a cook book notification service. 
     *
     *  @return <code> true </code> if cook book notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookNotification() {
        return (getAdapteeManager().supportsCookbookNotification());
    }


    /**
     *  Tests for the availability of a cook book hierarchy traversal service. 
     *
     *  @return <code> true </code> if cook book hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookHierarchy() {
        return (getAdapteeManager().supportsCookbookHierarchy());
    }


    /**
     *  Tests for the availability of a cook book hierarchy design service. 
     *
     *  @return <code> true </code> if cook book hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookHierarchyDesign() {
        return (getAdapteeManager().supportsCookbookHierarchyDesign());
    }


    /**
     *  Tests for the availability of a batch recipie service. 
     *
     *  @return <code> true </code> if a batch recipie service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipieBatch() {
        return (getAdapteeManager().supportsRecipieBatch());
    }


    /**
     *  Gets the supported <code> Recipe </code> record types. 
     *
     *  @return a list containing the supported <code> Recipe </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecipeRecordTypes() {
        return (getAdapteeManager().getRecipeRecordTypes());
    }


    /**
     *  Tests if the given <code> Recipe </code> record type is supported. 
     *
     *  @param  recipeRecordType a <code> Type </code> indicating an <code> 
     *          Recipe </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recipeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecipeRecordType(org.osid.type.Type recipeRecordType) {
        return (getAdapteeManager().supportsRecipeRecordType(recipeRecordType));
    }


    /**
     *  Gets the supported <code> Recipe </code> search record types. 
     *
     *  @return a list containing the supported <code> Recipe </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecipeSearchRecordTypes() {
        return (getAdapteeManager().getRecipeSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Recipe </code> search record type is 
     *  supported. 
     *
     *  @param  recipeSearchRecordType a <code> Type </code> indicating an 
     *          <code> Recipe </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recipeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecipeSearchRecordType(org.osid.type.Type recipeSearchRecordType) {
        return (getAdapteeManager().supportsRecipeSearchRecordType(recipeSearchRecordType));
    }


    /**
     *  Gets the supported <code> Direction </code> record types. 
     *
     *  @return a list containing the supported <code> Direction </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectionRecordTypes() {
        return (getAdapteeManager().getDirectionRecordTypes());
    }


    /**
     *  Tests if the given <code> Direction </code> record type is supported. 
     *
     *  @param  directionRecordType a <code> Type </code> indicating a <code> 
     *          Direction </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> directionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDirectionRecordType(org.osid.type.Type directionRecordType) {
        return (getAdapteeManager().supportsDirectionRecordType(directionRecordType));
    }


    /**
     *  Gets the supported <code> Direction </code> search types. 
     *
     *  @return a list containing the supported <code> Direction </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectionSearchRecordTypes() {
        return (getAdapteeManager().getDirectionSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Direction </code> search type is supported. 
     *
     *  @param  directionSearchRecordType a <code> Type </code> indicating a 
     *          <code> Direction </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          directionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDirectionSearchRecordType(org.osid.type.Type directionSearchRecordType) {
        return (getAdapteeManager().supportsDirectionSearchRecordType(directionSearchRecordType));
    }


    /**
     *  Gets the supported <code> Ingredient </code> record types. 
     *
     *  @return a list containing the supported <code> Ingredient </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIngredientRecordTypes() {
        return (getAdapteeManager().getIngredientRecordTypes());
    }


    /**
     *  Tests if the given <code> Ingredient </code> record type is supported. 
     *
     *  @param  ingredientRecordType a <code> Type </code> indicating an 
     *          <code> Ingredient </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ingredientRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIngredientRecordType(org.osid.type.Type ingredientRecordType) {
        return (getAdapteeManager().supportsIngredientRecordType(ingredientRecordType));
    }


    /**
     *  Gets the supported <code> Procedure </code> record types. 
     *
     *  @return a list containing the supported procedure record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcedureRecordTypes() {
        return (getAdapteeManager().getProcedureRecordTypes());
    }


    /**
     *  Tests if the given <code> Procedure </code> record type is supported. 
     *
     *  @param  procedureRecordType a <code> Type </code> indicating a <code> 
     *          Procedure </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> procedureRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcedureRecordType(org.osid.type.Type procedureRecordType) {
        return (getAdapteeManager().supportsProcedureRecordType(procedureRecordType));
    }


    /**
     *  Gets the supported procedure search record types. 
     *
     *  @return a list containing the supported procedure search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcedureSearchRecordTypes() {
        return (getAdapteeManager().getProcedureSearchRecordTypes());
    }


    /**
     *  Tests if the given procedure search record type is supported. 
     *
     *  @param  procedureSearchRecordType a <code> Type </code> indicating a 
     *          procedure record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          procedureSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcedureSearchRecordType(org.osid.type.Type procedureSearchRecordType) {
        return (getAdapteeManager().supportsProcedureSearchRecordType(procedureSearchRecordType));
    }


    /**
     *  Gets the supported cook book record types. 
     *
     *  @return a list containing the supported cook book record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCookbookRecordTypes() {
        return (getAdapteeManager().getCookbookRecordTypes());
    }


    /**
     *  Tests if the given cook book record type is supported. 
     *
     *  @param  cookbookRecordType a <code> Type </code> indicating a cook 
     *          book record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> cookbookRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCookbookRecordType(org.osid.type.Type cookbookRecordType) {
        return (getAdapteeManager().supportsCookbookRecordType(cookbookRecordType));
    }


    /**
     *  Gets the supported cook book search record types. 
     *
     *  @return a list containing the supported cook book search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCookbookSearchRecordTypes() {
        return (getAdapteeManager().getCookbookSearchRecordTypes());
    }


    /**
     *  Tests if the given cook book search record type is supported. 
     *
     *  @param  cookbookSearchRecordType a <code> Type </code> indicating a 
     *          cook book record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> cookbookSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCookbookSearchRecordType(org.osid.type.Type cookbookSearchRecordType) {
        return (getAdapteeManager().supportsCookbookSearchRecordType(cookbookSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe lookup 
     *  service. 
     *
     *  @return a <code> RecipeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeLookupSession getRecipeLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe lookup 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return q <code> RecipeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeLookupSession getRecipeLookupSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeLookupSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe query 
     *  service. 
     *
     *  @return a <code> RecipeQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuerySession getRecipeQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe query 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeQuerySession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuerySession getRecipeQuerySessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeQuerySessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe search 
     *  service. 
     *
     *  @return a <code> RecipeSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSearchSession getRecipeSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe search 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeSearchSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSearchSession getRecipeSearchSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeSearchSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  administration service. 
     *
     *  @return a <code> RecipeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeAdminSession getRecipeAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeAdminSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeAdminSession getRecipeAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeAdminSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  notification service. 
     *
     *  @param  recipeReceiver the notification callback 
     *  @return a <code> RecipeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> recipeReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeNotificationSession getRecipeNotificationSession(org.osid.recipe.RecipeReceiver recipeReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeNotificationSession(recipeReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  notification service for the given cookbook. 
     *
     *  @param  recipeReceiver the notification callback 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> recipeReceiver </code> 
     *          or <code> cookbookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeNotificationSession getRecipeNotificationSessionForCookbook(org.osid.recipe.RecipeReceiver recipeReceiver, 
                                                                                             org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeNotificationSessionForCookbook(recipeReceiver, cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup recipe/cook book 
     *  mappings. 
     *
     *  @return a <code> RecipeCookbookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeCookbookSession getRecipeCookbookSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeCookbookSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning recipes 
     *  to cook books. 
     *
     *  @return a <code> RecipeCookbookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeCookbookAssignmentSession getRecipeCookbookAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeCookbookAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage recipe smart cook books. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeSmartCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSmartCookbookSession getRecipeSmartCookbookSession(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeSmartCookbookSession(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  lookup service. 
     *
     *  @return a <code> DirectionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionLookupSession getDirectionLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  lookup service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the cook book 
     *  @return a <code> DirectionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionLookupSession getDirectionLookupSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionLookupSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  query service. 
     *
     *  @return a <code> DirectionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuerySession getDirectionQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  query service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionQuerySession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuerySession getDirectionQuerySessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionQuerySessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  search service. 
     *
     *  @return a <code> DirectionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSearchSession getDirectionSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  search service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionSearchSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSearchSession getDirectionSearchSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionSearchSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  administration service. 
     *
     *  @return a <code> DirectionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionAdminSession getDirectionAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionAdminSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionAdminSession getDirectionAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionAdminSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  notification service. 
     *
     *  @param  directionReceiver the notification callback 
     *  @return a <code> DirectionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> directionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionNotificationSession getDirectionNotificationSession(org.osid.recipe.DirectionReceiver directionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionNotificationSession(directionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  notification service for the given cookbook. 
     *
     *  @param  directionReceiver the notification callback 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> directionReceiver 
     *          </code> or <code> cookbookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionNotificationSession getDirectionNotificationSessionForCookbook(org.osid.recipe.DirectionReceiver directionReceiver, 
                                                                                                   org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionNotificationSessionForCookbook(directionReceiver, cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup direction/cook book 
     *  ingredients. 
     *
     *  @return a <code> DirectionCookbookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionCookbookSession getDirectionCookbookSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionCookbookSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  directions to cook books. 
     *
     *  @return a <code> DirectionCookbookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionCookbookAssignmentSession getDirectionCookbookAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionCookbookAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart cook 
     *  books. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSmartCookbook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSmartCookbookSession getDirectionSmartCookbookSession(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectionSmartCookbookSession(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  lookup service. 
     *
     *  @return a <code> ProcedureLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureLookupSession getProcedureLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  lookup service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureLookupSession getProcedureLookupSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureLookupSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  query service. 
     *
     *  @return a <code> ProcedureQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuerySession getProcedureQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  query service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuerySession getProcedureQuerySessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureQuerySessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  search service. 
     *
     *  @return a <code> ProcedureSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSearchSession getProcedureSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  search service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSearchSession getProcedureSearchSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureSearchSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  administration service. 
     *
     *  @return a <code> ProcedureAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureAdminSession getProcedureAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureAdminSession getProcedureAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureAdminSessionForCookbook(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  notification service. 
     *
     *  @param  procedureReceiver the receiver 
     *  @return a <code> ProcedureNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> procedureReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureNotificationSession getProcedureNotificationSession(org.osid.recipe.ProcedureReceiver procedureReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureNotificationSession(procedureReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  notification service for the given cookbook. 
     *
     *  @param  procedureReceiver the receiver 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> procedureReceiver 
     *          </code> or <code> cookbookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureNotificationSession getProcedureNotificationSessionForCookbook(org.osid.recipe.ProcedureReceiver procedureReceiver, 
                                                                                                   org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureNotificationSessionForCookbook(procedureReceiver, cookbookId));
    }


    /**
     *  Gets the session for retrieving procedure to cook book mappings. 
     *
     *  @return a <code> ProcedureCookbookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureCookbookSession getProcedureCookbookSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureCookbookSession());
    }


    /**
     *  Gets the session for assigning procedure to cook book mappings. 
     *
     *  @return a <code> ProcedureCookbookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureCookbookAssignmentSession getProcedureCookbookAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureCookbookAssignmentSession());
    }


    /**
     *  Gets the session associated with the procedure smart cook book for the 
     *  given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the cookbook 
     *  @return a <code> ProcedureSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException <code> procedureBookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> procedureBookId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSmartCookbook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSmartCookbookSession getProcedureSmartCookbookSession(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProcedureSmartCookbookSession(cookbookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  lookup service. 
     *
     *  @return a <code> CookbookLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookLookupSession getCookbookLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCookbookLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  query service. 
     *
     *  @return a <code> CookbookQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuerySession getCookbookQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCookbookQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  search service. 
     *
     *  @return a <code> CookbookSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookSearchSession getCookbookSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCookbookSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  administrative service. 
     *
     *  @return a <code> CookbookAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookAdminSession getCookbookAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCookbookAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  notification service. 
     *
     *  @param  cookbookReceiver the receiver 
     *  @return a <code> CookbookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookNotificationSession getCookbookNotificationSession(org.osid.recipe.CookbookReceiver cookbookReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCookbookNotificationSession(cookbookReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  hierarchy service. 
     *
     *  @return a <code> CookbookHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookHierarchySession getCookbookHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCookbookHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  hierarchy design service. 
     *
     *  @return a <code> CookbookHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookHierarchyDesignSession getCookbookHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCookbookHierarchyDesignSession());
    }


    /**
     *  Gets the <code> RecipieBatchManager </code> . 
     *
     *  @return a <code> RecipeBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchManager getRecipeBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecipeBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
