//
// GradeSystemTransformToIdList.java
//
//     Implements a GradeSystemTransform IdList. 
//
//
// Tom Coppeto
// Okapia
// 15 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.grading.transform.gradesystemtransform;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  Implements an IdList converting the underlying GradeSystemTransformList to a
 *  list of Ids.
 */

public final class GradeSystemTransformToIdList
    extends net.okapia.osid.jamocha.adapter.id.id.spi.AbstractAdapterIdList
    implements org.osid.id.IdList {

    private final org.osid.grading.transform.GradeSystemTransformList gradeSystemTransformList;


    /**
     *  Creates a new {@code GradeSystemTransformToIdList}.
     *
     *  @param gradeSystemTransformList a {@code GradeSystemTransformList}
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransformList}
     *          is {@code null}
     */

    public GradeSystemTransformToIdList(org.osid.grading.transform.GradeSystemTransformList gradeSystemTransformList) {
        super(gradeSystemTransformList);
        this.gradeSystemTransformList = gradeSystemTransformList;
        return;
    }


    /**
     *  Creates a new {@code GradeSystemTransformToIdList}.
     *
     *  @param gradeSystemTransforms a collection of GradeSystemTransforms
     *  @throws org.osid.NullArgumentException {@code gradeSystemTransforms}
     *          is {@code null}
     */

    public GradeSystemTransformToIdList(java.util.Collection<org.osid.grading.transform.GradeSystemTransform> gradeSystemTransforms) {
        this(new net.okapia.osid.jamocha.grading.transform.gradesystemtransform.ArrayGradeSystemTransformList(gradeSystemTransforms)); 
        return;
    }


    /**
     *  Gets the next {@code Id} in this list. 
     *
     *  @return the next {@code Id} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Id} is available before calling this method.
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.Id getNextId()
        throws org.osid.OperationFailedException {

        return (this.gradeSystemTransformList.getNextGradeSystemTransform().getId());
    }


    /**
     *  Closes this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.gradeSystemTransformList.close();
        return;
    }
}
