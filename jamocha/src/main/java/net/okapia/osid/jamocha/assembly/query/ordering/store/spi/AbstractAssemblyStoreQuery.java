//
// AbstractAssemblyStoreQuery.java
//
//     A StoreQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ordering.store.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A StoreQuery that stores terms.
 */

public abstract class AbstractAssemblyStoreQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.ordering.StoreQuery,
               org.osid.ordering.StoreQueryInspector,
               org.osid.ordering.StoreSearchOrder {

    private final java.util.Collection<org.osid.ordering.records.StoreQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.StoreQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ordering.records.StoreSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyStoreQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyStoreQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the order <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  orderId a order <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> orderId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOrderId(org.osid.id.Id orderId, boolean match) {
        getAssembler().addIdTerm(getOrderIdColumn(), orderId, match);
        return;
    }


    /**
     *  Clears the order <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrderIdTerms() {
        getAssembler().clearTerms(getOrderIdColumn());
        return;
    }


    /**
     *  Gets the order <code> Id </code> terms. 
     *
     *  @return the order <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrderIdTerms() {
        return (getAssembler().getIdTerms(getOrderIdColumn()));
    }


    /**
     *  Gets the OrderId column name.
     *
     * @return the column name
     */

    protected String getOrderIdColumn() {
        return ("order_id");
    }


    /**
     *  Tests if a order query is available. 
     *
     *  @return <code> true </code> if a order query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderQuery() {
        return (false);
    }


    /**
     *  Gets the query for an order. 
     *
     *  @return the order query 
     *  @throws org.osid.UnimplementedException <code> supportsOrderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderQuery getOrderQuery() {
        throw new org.osid.UnimplementedException("supportsOrderQuery() is false");
    }


    /**
     *  Matches stores with any order. 
     *
     *  @param  match <code> true </code> to match stores with any order, 
     *          <code> false </code> to match stores with no orders 
     */

    @OSID @Override
    public void matchAnyOrder(boolean match) {
        getAssembler().addIdWildcardTerm(getOrderColumn(), match);
        return;
    }


    /**
     *  Clears the order terms. 
     */

    @OSID @Override
    public void clearOrderTerms() {
        getAssembler().clearTerms(getOrderColumn());
        return;
    }


    /**
     *  Gets the order terms. 
     *
     *  @return the order terms 
     */

    @OSID @Override
    public org.osid.ordering.OrderQueryInspector[] getOrderTerms() {
        return (new org.osid.ordering.OrderQueryInspector[0]);
    }


    /**
     *  Gets the Order column name.
     *
     * @return the column name
     */

    protected String getOrderColumn() {
        return ("order");
    }


    /**
     *  Sets the product <code> Id </code> for this query. 
     *
     *  @param  productId a product <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> productId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProductId(org.osid.id.Id productId, boolean match) {
        getAssembler().addIdTerm(getProductIdColumn(), productId, match);
        return;
    }


    /**
     *  Clears the product <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProductIdTerms() {
        getAssembler().clearTerms(getProductIdColumn());
        return;
    }


    /**
     *  Gets the product <code> Id </code> terms. 
     *
     *  @return the product <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProductIdTerms() {
        return (getAssembler().getIdTerms(getProductIdColumn()));
    }


    /**
     *  Gets the ProductId column name.
     *
     * @return the column name
     */

    protected String getProductIdColumn() {
        return ("product_id");
    }


    /**
     *  Tests if a product query is available. 
     *
     *  @return <code> true </code> if a product query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductQuery() {
        return (false);
    }


    /**
     *  Gets the query for a product. 
     *
     *  @return the product query 
     *  @throws org.osid.UnimplementedException <code> supportsProductQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ProductQuery getProductQuery() {
        throw new org.osid.UnimplementedException("supportsProductQuery() is false");
    }


    /**
     *  Matches stores with any product. 
     *
     *  @param  match <code> true </code> to match stores with any product, 
     *          <code> false </code> to match stores with no products 
     */

    @OSID @Override
    public void matchAnyProduct(boolean match) {
        getAssembler().addIdWildcardTerm(getProductColumn(), match);
        return;
    }


    /**
     *  Clears the product terms. 
     */

    @OSID @Override
    public void clearProductTerms() {
        getAssembler().clearTerms(getProductColumn());
        return;
    }


    /**
     *  Gets the product terms. 
     *
     *  @return the product terms 
     */

    @OSID @Override
    public org.osid.ordering.ProductQueryInspector[] getProductTerms() {
        return (new org.osid.ordering.ProductQueryInspector[0]);
    }


    /**
     *  Gets the Product column name.
     *
     * @return the column name
     */

    protected String getProductColumn() {
        return ("product");
    }


    /**
     *  Sets the price schedule <code> Id </code> for this query. 
     *
     *  @param  priceScheduleId a price schedule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchPriceScheduleId(org.osid.id.Id priceScheduleId, 
                                     boolean match) {
        getAssembler().addIdTerm(getPriceScheduleIdColumn(), priceScheduleId, match);
        return;
    }


    /**
     *  Clears the price schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPriceScheduleIdTerms() {
        getAssembler().clearTerms(getPriceScheduleIdColumn());
        return;
    }


    /**
     *  Gets the price schedule <code> Id </code> terms. 
     *
     *  @return the price schedule <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPriceScheduleIdTerms() {
        return (getAssembler().getIdTerms(getPriceScheduleIdColumn()));
    }


    /**
     *  Gets the PriceScheduleId column name.
     *
     * @return the column name
     */

    protected String getPriceScheduleIdColumn() {
        return ("price_schedule_id");
    }


    /**
     *  Tests if a price schedule query is available. 
     *
     *  @return <code> true </code> if a price schedule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price schedule. 
     *
     *  @return the price schedule query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuery getPriceScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsPriceScheduleQuery() is false");
    }


    /**
     *  Matches products with any price schedule. 
     *
     *  @param  match <code> true </code> to match products with any price 
     *          schedule, <code> false </code> to match products with no price 
     *          schedule 
     */

    @OSID @Override
    public void matchAnyPriceSchedule(boolean match) {
        getAssembler().addIdWildcardTerm(getPriceScheduleColumn(), match);
        return;
    }


    /**
     *  Clears the price schedule terms. 
     */

    @OSID @Override
    public void clearPriceScheduleTerms() {
        getAssembler().clearTerms(getPriceScheduleColumn());
        return;
    }


    /**
     *  Gets the price schedule terms. 
     *
     *  @return the price schedule terms 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQueryInspector[] getPriceScheduleTerms() {
        return (new org.osid.ordering.PriceScheduleQueryInspector[0]);
    }


    /**
     *  Gets the PriceSchedule column name.
     *
     * @return the column name
     */

    protected String getPriceScheduleColumn() {
        return ("price_schedule");
    }


    /**
     *  Sets the store <code> Id </code> for this query to match stores that 
     *  have the specified store as an ancestor. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorStoreId(org.osid.id.Id storeId, boolean match) {
        getAssembler().addIdTerm(getAncestorStoreIdColumn(), storeId, match);
        return;
    }


    /**
     *  Clears the ancestor store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorStoreIdTerms() {
        getAssembler().clearTerms(getAncestorStoreIdColumn());
        return;
    }


    /**
     *  Gets the ancestor store <code> Id </code> terms. 
     *
     *  @return the ancestor store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorStoreIdTerms() {
        return (getAssembler().getIdTerms(getAncestorStoreIdColumn()));
    }


    /**
     *  Gets the AncestorStoreId column name.
     *
     * @return the column name
     */

    protected String getAncestorStoreIdColumn() {
        return ("ancestor_store_id");
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorStoreQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getAncestorStoreQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorStoreQuery() is false");
    }


    /**
     *  Matches stores with any ancestor. 
     *
     *  @param  match <code> true </code> to match stores with any ancestor, 
     *          <code> false </code> to match root stores 
     */

    @OSID @Override
    public void matchAnyAncestorStore(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorStoreColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor store terms. 
     */

    @OSID @Override
    public void clearAncestorStoreTerms() {
        getAssembler().clearTerms(getAncestorStoreColumn());
        return;
    }


    /**
     *  Gets the ancestor store terms. 
     *
     *  @return the ancestor store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getAncestorStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the AncestorStore column name.
     *
     * @return the column name
     */

    protected String getAncestorStoreColumn() {
        return ("ancestor_store");
    }


    /**
     *  Sets the store <code> Id </code> for this query to match stores that 
     *  have the specified store as a descendant. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantStoreId(org.osid.id.Id storeId, boolean match) {
        getAssembler().addIdTerm(getDescendantStoreIdColumn(), storeId, match);
        return;
    }


    /**
     *  Clears the descendant store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantStoreIdTerms() {
        getAssembler().clearTerms(getDescendantStoreIdColumn());
        return;
    }


    /**
     *  Gets the descendant store <code> Id </code> terms. 
     *
     *  @return the descendant store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantStoreIdTerms() {
        return (getAssembler().getIdTerms(getDescendantStoreIdColumn()));
    }


    /**
     *  Gets the DescendantStoreId column name.
     *
     * @return the column name
     */

    protected String getDescendantStoreIdColumn() {
        return ("descendant_store_id");
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantStoreQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getDescendantStoreQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantStoreQuery() is false");
    }


    /**
     *  Matches stores with any descendant. 
     *
     *  @param  match <code> true </code> to match stores with any descendant, 
     *          <code> false </code> to match leaf stores 
     */

    @OSID @Override
    public void matchAnyDescendantStore(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantStoreColumn(), match);
        return;
    }


    /**
     *  Clears the descendant store terms. 
     */

    @OSID @Override
    public void clearDescendantStoreTerms() {
        getAssembler().clearTerms(getDescendantStoreColumn());
        return;
    }


    /**
     *  Gets the descendant store terms. 
     *
     *  @return the descendant store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getDescendantStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }


    /**
     *  Gets the DescendantStore column name.
     *
     * @return the column name
     */

    protected String getDescendantStoreColumn() {
        return ("descendant_store");
    }


    /**
     *  Tests if this store supports the given record
     *  <code>Type</code>.
     *
     *  @param  storeRecordType a store record type 
     *  @return <code>true</code> if the storeRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type storeRecordType) {
        for (org.osid.ordering.records.StoreQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(storeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  storeRecordType the store record type 
     *  @return the store query record 
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(storeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.StoreQueryRecord getStoreQueryRecord(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.StoreQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(storeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(storeRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  storeRecordType the store record type 
     *  @return the store query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(storeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.StoreQueryInspectorRecord getStoreQueryInspectorRecord(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.StoreQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(storeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(storeRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param storeRecordType the store record type
     *  @return the store search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(storeRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.StoreSearchOrderRecord getStoreSearchOrderRecord(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.StoreSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(storeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(storeRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this store. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param storeQueryRecord the store query record
     *  @param storeQueryInspectorRecord the store query inspector
     *         record
     *  @param storeSearchOrderRecord the store search order record
     *  @param storeRecordType store record type
     *  @throws org.osid.NullArgumentException
     *          <code>storeQueryRecord</code>,
     *          <code>storeQueryInspectorRecord</code>,
     *          <code>storeSearchOrderRecord</code> or
     *          <code>storeRecordTypestore</code> is
     *          <code>null</code>
     */
            
    protected void addStoreRecords(org.osid.ordering.records.StoreQueryRecord storeQueryRecord, 
                                      org.osid.ordering.records.StoreQueryInspectorRecord storeQueryInspectorRecord, 
                                      org.osid.ordering.records.StoreSearchOrderRecord storeSearchOrderRecord, 
                                      org.osid.type.Type storeRecordType) {

        addRecordType(storeRecordType);

        nullarg(storeQueryRecord, "store query record");
        nullarg(storeQueryInspectorRecord, "store query inspector record");
        nullarg(storeSearchOrderRecord, "store search odrer record");

        this.queryRecords.add(storeQueryRecord);
        this.queryInspectorRecords.add(storeQueryInspectorRecord);
        this.searchOrderRecords.add(storeSearchOrderRecord);
        
        return;
    }
}
