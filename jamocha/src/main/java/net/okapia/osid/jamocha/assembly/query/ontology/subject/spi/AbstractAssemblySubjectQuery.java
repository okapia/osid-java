//
// AbstractAssemblySubjectQuery.java
//
//     A SubjectQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.ontology.subject.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SubjectQuery that stores terms.
 */

public abstract class AbstractAssemblySubjectQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.ontology.SubjectQuery,
               org.osid.ontology.SubjectQueryInspector,
               org.osid.ontology.SubjectSearchOrder {

    private final java.util.Collection<org.osid.ontology.records.SubjectQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.records.SubjectQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.ontology.records.SubjectSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySubjectQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySubjectQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the subject <code> Id </code> for this query to match subjects 
     *  that have the specified subject as an ancestor. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorSubjectId(org.osid.id.Id subjectId, boolean match) {
        getAssembler().addIdTerm(getAncestorSubjectIdColumn(), subjectId, match);
        return;
    }


    /**
     *  Clears the ancestor subject <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearAncestorSubjectIdTerms() {
        getAssembler().clearTerms(getAncestorSubjectIdColumn());
        return;
    }


    /**
     *  Gets the ancestor subject <code> Id </code> terms. 
     *
     *  @return the ancestor subject <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorSubjectIdTerms() {
        return (getAssembler().getIdTerms(getAncestorSubjectIdColumn()));
    }


    /**
     *  Gets the AncestorSubjectId column name.
     *
     * @return the column name
     */

    protected String getAncestorSubjectIdColumn() {
        return ("ancestor_subject_id");
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorSubjectQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getAncestorSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorSubjectQuery() is false");
    }


    /**
     *  Matches subjects with any ancestor. 
     *
     *  @param  match <code> true </code> to match subjects with any ancestor, 
     *          <code> false </code> to match root subjects 
     */

    @OSID @Override
    public void matchAnyAncestorSubject(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorSubjectColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor subject terms for this query. 
     */

    @OSID @Override
    public void clearAncestorSubjectTerms() {
        getAssembler().clearTerms(getAncestorSubjectColumn());
        return;
    }


    /**
     *  Gets the ancestor subject terms. 
     *
     *  @return the ancestor subject terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getAncestorSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the AncestorSubject column name.
     *
     * @return the column name
     */

    protected String getAncestorSubjectColumn() {
        return ("ancestor_subject");
    }


    /**
     *  Sets the subject <code> Id </code> for this query to match subjects 
     *  that have the specified subject as a descendant. 
     *
     *  @param  subjectId a subject <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subjectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantSubjectId(org.osid.id.Id subjectId, 
                                         boolean match) {
        getAssembler().addIdTerm(getDescendantSubjectIdColumn(), subjectId, match);
        return;
    }


    /**
     *  Clears the descendant subject <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearDescendantSubjectIdTerms() {
        getAssembler().clearTerms(getDescendantSubjectIdColumn());
        return;
    }


    /**
     *  Gets the descendant subject <code> Id </code> terms. 
     *
     *  @return the descendant subject <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantSubjectIdTerms() {
        return (getAssembler().getIdTerms(getDescendantSubjectIdColumn()));
    }


    /**
     *  Gets the DescendantSubjectId column name.
     *
     * @return the column name
     */

    protected String getDescendantSubjectIdColumn() {
        return ("descendant_subject_id");
    }


    /**
     *  Tests if a <code> SubjectQuery </code> is available. 
     *
     *  @return <code> true </code> if a subject query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantSubjectQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subject. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the subject query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantSubjectQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQuery getDescendantSubjectQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantSubjectQuery() is false");
    }


    /**
     *  Matches subjects with any descendant. 
     *
     *  @param  match <code> true </code> to match subjects with any 
     *          descendant, <code> false </code> to match leaf subjects 
     */

    @OSID @Override
    public void matchAnyDescendantSubject(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantSubjectColumn(), match);
        return;
    }


    /**
     *  Clears the descendant subject terms for this query. 
     */

    @OSID @Override
    public void clearDescendantSubjectTerms() {
        getAssembler().clearTerms(getDescendantSubjectColumn());
        return;
    }


    /**
     *  Gets the descendant subject terms. 
     *
     *  @return the descendant subject terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getDescendantSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the DescendantSubject column name.
     *
     * @return the column name
     */

    protected String getDescendantSubjectColumn() {
        return ("descendant_subject");
    }


    /**
     *  Sets the relevancy <code> Id </code> for this query. 
     *
     *  @param  relevancyId a relevancy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relevancyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRelevancyId(org.osid.id.Id relevancyId, boolean match) {
        getAssembler().addIdTerm(getRelevancyIdColumn(), relevancyId, match);
        return;
    }


    /**
     *  Clears the relevancy <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearRelevancyIdTerms() {
        getAssembler().clearTerms(getRelevancyIdColumn());
        return;
    }


    /**
     *  Gets the relevancy <code> Id </code> terms. 
     *
     *  @return the relevancy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelevancyIdTerms() {
        return (getAssembler().getIdTerms(getRelevancyIdColumn()));
    }


    /**
     *  Gets the RelevancyId column name.
     *
     * @return the column name
     */

    protected String getRelevancyIdColumn() {
        return ("relevancy_id");
    }


    /**
     *  Tests if a <code> RelevancyQuery </code> is available. 
     *
     *  @return <code> true </code> if a relevancy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relevancy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the relevancy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsRelevancyQuery() is false");
    }


    /**
     *  Matches ontologies that have any relevancy. 
     *
     *  @param  match <code> true </code> to match ontologies with any 
     *          relevancy, <code> false </code> to match ontologies with no 
     *          relevancy 
     */

    @OSID @Override
    public void matchAnyRelevancy(boolean match) {
        getAssembler().addIdWildcardTerm(getRelevancyColumn(), match);
        return;
    }


    /**
     *  Clears the relevancy terms for this query. 
     */

    @OSID @Override
    public void clearRelevancyTerms() {
        getAssembler().clearTerms(getRelevancyColumn());
        return;
    }


    /**
     *  Gets the relevancy terms. 
     *
     *  @return the relevancy terms 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getRelevancyTerms() {
        return (new org.osid.ontology.RelevancyQueryInspector[0]);
    }


    /**
     *  Gets the Relevancy column name.
     *
     * @return the column name
     */

    protected String getRelevancyColumn() {
        return ("relevancy");
    }


    /**
     *  Sets the ontology <code> Id </code> for this query. 
     *
     *  @param  ontologyId a ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOntologyId(org.osid.id.Id ontologyId, boolean match) {
        getAssembler().addIdTerm(getOntologyIdColumn(), ontologyId, match);
        return;
    }


    /**
     *  Clears the ontology <code> Id </code> terms for this query. 
     */

    @OSID @Override
    public void clearOntologyIdTerms() {
        getAssembler().clearTerms(getOntologyIdColumn());
        return;
    }


    /**
     *  Gets the ontology <code> Id </code> terms. 
     *
     *  @return the ontology <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOntologyIdTerms() {
        return (getAssembler().getIdTerms(getOntologyIdColumn()));
    }


    /**
     *  Gets the OntologyId column name.
     *
     * @return the column name
     */

    protected String getOntologyIdColumn() {
        return ("ontology_id");
    }


    /**
     *  Tests if a <code> OntologyQuery </code> is available for querying 
     *  ontologies. 
     *
     *  @return <code> true </code> if a ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsOntologyQuery() is false");
    }


    /**
     *  Clears the ontology terms for this query. 
     */

    @OSID @Override
    public void clearOntologyTerms() {
        getAssembler().clearTerms(getOntologyColumn());
        return;
    }


    /**
     *  Gets the ontology terms. 
     *
     *  @return the ontology terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }


    /**
     *  Gets the Ontology column name.
     *
     * @return the column name
     */

    protected String getOntologyColumn() {
        return ("ontology");
    }


    /**
     *  Tests if this subject supports the given record
     *  <code>Type</code>.
     *
     *  @param  subjectRecordType a subject record type 
     *  @return <code>true</code> if the subjectRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type subjectRecordType) {
        for (org.osid.ontology.records.SubjectQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(subjectRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  subjectRecordType the subject record type 
     *  @return the subject query record 
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subjectRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.SubjectQueryRecord getSubjectQueryRecord(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.SubjectQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(subjectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subjectRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  subjectRecordType the subject record type 
     *  @return the subject query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subjectRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.SubjectQueryInspectorRecord getSubjectQueryInspectorRecord(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.SubjectQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(subjectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subjectRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param subjectRecordType the subject record type
     *  @return the subject search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subjectRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.SubjectSearchOrderRecord getSubjectSearchOrderRecord(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.SubjectSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(subjectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subjectRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this subject. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param subjectQueryRecord the subject query record
     *  @param subjectQueryInspectorRecord the subject query inspector
     *         record
     *  @param subjectSearchOrderRecord the subject search order record
     *  @param subjectRecordType subject record type
     *  @throws org.osid.NullArgumentException
     *          <code>subjectQueryRecord</code>,
     *          <code>subjectQueryInspectorRecord</code>,
     *          <code>subjectSearchOrderRecord</code> or
     *          <code>subjectRecordTypesubject</code> is
     *          <code>null</code>
     */
            
    protected void addSubjectRecords(org.osid.ontology.records.SubjectQueryRecord subjectQueryRecord, 
                                      org.osid.ontology.records.SubjectQueryInspectorRecord subjectQueryInspectorRecord, 
                                      org.osid.ontology.records.SubjectSearchOrderRecord subjectSearchOrderRecord, 
                                      org.osid.type.Type subjectRecordType) {

        addRecordType(subjectRecordType);

        nullarg(subjectQueryRecord, "subject query record");
        nullarg(subjectQueryInspectorRecord, "subject query inspector record");
        nullarg(subjectSearchOrderRecord, "subject search odrer record");

        this.queryRecords.add(subjectQueryRecord);
        this.queryInspectorRecords.add(subjectQueryInspectorRecord);
        this.searchOrderRecords.add(subjectSearchOrderRecord);
        
        return;
    }
}
