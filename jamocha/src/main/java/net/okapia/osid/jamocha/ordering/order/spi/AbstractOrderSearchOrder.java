//
// AbstractOrderSearchOdrer.java
//
//     Defines an OrderSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code OrderSearchOrder}.
 */

public abstract class AbstractOrderSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.ordering.OrderSearchOrder {

    private final java.util.Collection<org.osid.ordering.records.OrderSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the customer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomer(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a customer search order is available. 
     *
     *  @return <code> true </code> if a customer order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the customer search order. 
     *
     *  @return the customer search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getCustomerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCustomerSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the total cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTotalCost(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the submitted 
     *  date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmitDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the submitter. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmitter(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a submitter search order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmitterSearchOrder() {
        return (false);
    }


    /**
     *  Gets the submitter search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmitterSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSubmitterSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubmitterSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the submitting 
     *  agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySubmittingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a submitting agent search order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubmittingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the submitting agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubmittingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getSubmittingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSubmittingAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the closed date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosedDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the closer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCloser(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a closer search order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCloserSearchOrder() {
        return (false);
    }


    /**
     *  Gets the closer search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCloserSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getCloserSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCloserSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the closer. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClosingAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a closing agent search order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsClosingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the closing agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsClosingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getClosingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsClosingAgentSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  orderRecordType an order record type 
     *  @return {@code true} if the orderRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code orderRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type orderRecordType) {
        for (org.osid.ordering.records.OrderSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(orderRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  orderRecordType the order record type 
     *  @return the order search order record
     *  @throws org.osid.NullArgumentException
     *          {@code orderRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(orderRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.ordering.records.OrderSearchOrderRecord getOrderSearchOrderRecord(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.OrderSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(orderRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(orderRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this order. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param orderRecord the order search odrer record
     *  @param orderRecordType order record type
     *  @throws org.osid.NullArgumentException
     *          {@code orderRecord} or
     *          {@code orderRecordTypeorder} is
     *          {@code null}
     */
            
    protected void addOrderRecord(org.osid.ordering.records.OrderSearchOrderRecord orderSearchOrderRecord, 
                                     org.osid.type.Type orderRecordType) {

        addRecordType(orderRecordType);
        this.records.add(orderSearchOrderRecord);
        
        return;
    }
}
