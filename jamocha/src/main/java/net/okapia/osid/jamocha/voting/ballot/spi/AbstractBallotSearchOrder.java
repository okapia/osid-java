//
// AbstractBallotSearchOdrer.java
//
//     Defines a BallotSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.ballot.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code BallotSearchOrder}.
 */

public abstract class AbstractBallotSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorSearchOrder
    implements org.osid.voting.BallotSearchOrder {

    private final java.util.Collection<org.osid.voting.records.BallotSearchOrderRecord> records = new java.util.LinkedHashSet<>();

    private final OsidTemporalSearchOrder order = new OsidTemporalSearchOrder();


    /**
     *  Specifies a preference for ordering the result set by the
     *  effective status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByEffective(org.osid.SearchOrderStyle style) {
        this.order.orderByEffective(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  start date.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        this.order.orderByStartDate(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the end
     *  date.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        order.orderByEndDate(style);
        return;
    }


    /**
     *  Orders by the revote flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRevote(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  ballotRecordType a ballot record type 
     *  @return {@code true} if the ballotRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code ballotRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type ballotRecordType) {
        for (org.osid.voting.records.BallotSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  ballotRecordType the ballot record type 
     *  @return the ballot search order record
     *  @throws org.osid.NullArgumentException
     *          {@code ballotRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(ballotRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.voting.records.BallotSearchOrderRecord getBallotSearchOrderRecord(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.BallotSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this ballot. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param ballotRecord the ballot search odrer record
     *  @param ballotRecordType ballot record type
     *  @throws org.osid.NullArgumentException
     *          {@code ballotRecord} or
     *          {@code ballotRecordTypeballot} is
     *          {@code null}
     */
            
    protected void addBallotRecord(org.osid.voting.records.BallotSearchOrderRecord ballotSearchOrderRecord, 
                                   org.osid.type.Type ballotRecordType) {

        addRecordType(ballotRecordType);
        this.records.add(ballotSearchOrderRecord);
        
        return;
    }


    protected class OsidTemporalSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidTemporalSearchOrder
        implements org.osid.OsidTemporalSearchOrder {
    }
}
