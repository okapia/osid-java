//
// AbstractIndexedMapRelationshipEnablerLookupSession.java
//
//    A simple framework for providing a RelationshipEnabler lookup service
//    backed by a fixed collection of relationship enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a RelationshipEnabler lookup service backed by a
 *  fixed collection of relationship enablers. The relationship enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some relationship enablers may be compatible
 *  with more types than are indicated through these relationship enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>RelationshipEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRelationshipEnablerLookupSession
    extends AbstractMapRelationshipEnablerLookupSession
    implements org.osid.relationship.rules.RelationshipEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.relationship.rules.RelationshipEnabler> relationshipEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.relationship.rules.RelationshipEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.relationship.rules.RelationshipEnabler> relationshipEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.relationship.rules.RelationshipEnabler>());


    /**
     *  Makes a <code>RelationshipEnabler</code> available in this session.
     *
     *  @param  relationshipEnabler a relationship enabler
     *  @throws org.osid.NullArgumentException <code>relationshipEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRelationshipEnabler(org.osid.relationship.rules.RelationshipEnabler relationshipEnabler) {
        super.putRelationshipEnabler(relationshipEnabler);

        this.relationshipEnablersByGenus.put(relationshipEnabler.getGenusType(), relationshipEnabler);
        
        try (org.osid.type.TypeList types = relationshipEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.relationshipEnablersByRecord.put(types.getNextType(), relationshipEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a relationship enabler from this session.
     *
     *  @param relationshipEnablerId the <code>Id</code> of the relationship enabler
     *  @throws org.osid.NullArgumentException <code>relationshipEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRelationshipEnabler(org.osid.id.Id relationshipEnablerId) {
        org.osid.relationship.rules.RelationshipEnabler relationshipEnabler;
        try {
            relationshipEnabler = getRelationshipEnabler(relationshipEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.relationshipEnablersByGenus.remove(relationshipEnabler.getGenusType());

        try (org.osid.type.TypeList types = relationshipEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.relationshipEnablersByRecord.remove(types.getNextType(), relationshipEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRelationshipEnabler(relationshipEnablerId);
        return;
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> corresponding to the given
     *  relationship enabler genus <code>Type</code> which does not include
     *  relationship enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known relationship enablers or an error results. Otherwise,
     *  the returned list may contain only those relationship enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  relationshipEnablerGenusType a relationship enabler genus type 
     *  @return the returned <code>RelationshipEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByGenusType(org.osid.type.Type relationshipEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.rules.relationshipenabler.ArrayRelationshipEnablerList(this.relationshipEnablersByGenus.get(relationshipEnablerGenusType)));
    }


    /**
     *  Gets a <code>RelationshipEnablerList</code> containing the given
     *  relationship enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known relationship enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  relationship enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  relationshipEnablerRecordType a relationship enabler record type 
     *  @return the returned <code>relationshipEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.rules.RelationshipEnablerList getRelationshipEnablersByRecordType(org.osid.type.Type relationshipEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.relationship.rules.relationshipenabler.ArrayRelationshipEnablerList(this.relationshipEnablersByRecord.get(relationshipEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.relationshipEnablersByGenus.clear();
        this.relationshipEnablersByRecord.clear();

        super.close();

        return;
    }
}
