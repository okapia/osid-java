//
// AbstractIndexedMapAssessmentEntryLookupSession.java
//
//    A simple framework for providing an AssessmentEntry lookup service
//    backed by a fixed collection of assessment entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AssessmentEntry lookup service backed by a
 *  fixed collection of assessment entries. The assessment entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some assessment entries may be compatible
 *  with more types than are indicated through these assessment entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AssessmentEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAssessmentEntryLookupSession
    extends AbstractMapAssessmentEntryLookupSession
    implements org.osid.course.chronicle.AssessmentEntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.AssessmentEntry> assessmentEntriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.AssessmentEntry>());
    private final MultiMap<org.osid.type.Type, org.osid.course.chronicle.AssessmentEntry> assessmentEntriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.chronicle.AssessmentEntry>());


    /**
     *  Makes an <code>AssessmentEntry</code> available in this session.
     *
     *  @param  assessmentEntry an assessment entry
     *  @throws org.osid.NullArgumentException <code>assessmentEntry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAssessmentEntry(org.osid.course.chronicle.AssessmentEntry assessmentEntry) {
        super.putAssessmentEntry(assessmentEntry);

        this.assessmentEntriesByGenus.put(assessmentEntry.getGenusType(), assessmentEntry);
        
        try (org.osid.type.TypeList types = assessmentEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentEntriesByRecord.put(types.getNextType(), assessmentEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an assessment entry from this session.
     *
     *  @param assessmentEntryId the <code>Id</code> of the assessment entry
     *  @throws org.osid.NullArgumentException <code>assessmentEntryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAssessmentEntry(org.osid.id.Id assessmentEntryId) {
        org.osid.course.chronicle.AssessmentEntry assessmentEntry;
        try {
            assessmentEntry = getAssessmentEntry(assessmentEntryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.assessmentEntriesByGenus.remove(assessmentEntry.getGenusType());

        try (org.osid.type.TypeList types = assessmentEntry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.assessmentEntriesByRecord.remove(types.getNextType(), assessmentEntry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAssessmentEntry(assessmentEntryId);
        return;
    }


    /**
     *  Gets an <code>AssessmentEntryList</code> corresponding to the given
     *  assessment entry genus <code>Type</code> which does not include
     *  assessment entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known assessment entries or an error results. Otherwise,
     *  the returned list may contain only those assessment entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  assessmentEntryGenusType an assessment entry genus type 
     *  @return the returned <code>AssessmentEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByGenusType(org.osid.type.Type assessmentEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.assessmententry.ArrayAssessmentEntryList(this.assessmentEntriesByGenus.get(assessmentEntryGenusType)));
    }


    /**
     *  Gets an <code>AssessmentEntryList</code> containing the given
     *  assessment entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known assessment entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessment entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  assessmentEntryRecordType an assessment entry record type 
     *  @return the returned <code>assessmentEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryList getAssessmentEntriesByRecordType(org.osid.type.Type assessmentEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.assessmententry.ArrayAssessmentEntryList(this.assessmentEntriesByRecord.get(assessmentEntryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentEntriesByGenus.clear();
        this.assessmentEntriesByRecord.clear();

        super.close();

        return;
    }
}
