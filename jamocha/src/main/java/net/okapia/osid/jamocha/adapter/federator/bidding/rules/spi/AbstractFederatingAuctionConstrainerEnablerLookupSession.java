//
// AbstractFederatingAuctionConstrainerEnablerLookupSession.java
//
//     An abstract federating adapter for an AuctionConstrainerEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AuctionConstrainerEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAuctionConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession>
    implements org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();


    /**
     *  Constructs a new <code>AbstractFederatingAuctionConstrainerEnablerLookupSession</code>.
     */

    protected AbstractFederatingAuctionConstrainerEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>AuctionConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionConstrainerEnablers() {
        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            if (session.canLookupAuctionConstrainerEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AuctionConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionConstrainerEnablerView() {
        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            session.useComparativeAuctionConstrainerEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AuctionConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionConstrainerEnablerView() {
        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            session.usePlenaryAuctionConstrainerEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction constrainer enablers in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            session.useFederatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            session.useIsolatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Only active auction constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionConstrainerEnablerView() {
        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            session.useActiveAuctionConstrainerEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive auction constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionConstrainerEnablerView() {
        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            session.useAnyStatusAuctionConstrainerEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>AuctionConstrainerEnabler</code> specified by
     *  its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionConstrainerEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AuctionConstrainerEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  auctionConstrainerEnablerId <code>Id</code> of the
     *          <code>AuctionConstrainerEnabler</code>
     *  @return the auction constrainer enabler
     *  @throws org.osid.NotFoundException <code>auctionConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnabler getAuctionConstrainerEnabler(org.osid.id.Id auctionConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            try {
                return (session.getAuctionConstrainerEnabler(auctionConstrainerEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(auctionConstrainerEnablerId + " not found");
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code>
     *  corresponding to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionConstrainerEnablers specified in the <code>Id</code>
     *  list, in the order of the list, including duplicates, or an
     *  error results if an <code>Id</code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible
     *  <code>AuctionConstrainerEnablers</code> may be omitted from
     *  the list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  auctionConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByIds(org.osid.id.IdList auctionConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.MutableAuctionConstrainerEnablerList ret = new net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.MutableAuctionConstrainerEnablerList();

        try (org.osid.id.IdList ids = auctionConstrainerEnablerIds) {
            while (ids.hasNext()) {
                ret.addAuctionConstrainerEnabler(getAuctionConstrainerEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code>
     *  corresponding to the given auction constrainer enabler genus
     *  <code>Type</code> which does not include auction constrainer
     *  enablers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  auctionConstrainerEnablerGenusType an auctionConstrainerEnabler genus type 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByGenusType(org.osid.type.Type auctionConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.FederatingAuctionConstrainerEnablerList ret = getAuctionConstrainerEnablerList();

        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            ret.addAuctionConstrainerEnablerList(session.getAuctionConstrainerEnablersByGenusType(auctionConstrainerEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code>
     *  corresponding to the given auction constrainer enabler genus
     *  <code>Type</code> and include any additional auction
     *  constrainer enablers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  auctionConstrainerEnablerGenusType an auctionConstrainerEnabler genus type 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByParentGenusType(org.osid.type.Type auctionConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.FederatingAuctionConstrainerEnablerList ret = getAuctionConstrainerEnablerList();

        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            ret.addAuctionConstrainerEnablerList(session.getAuctionConstrainerEnablersByParentGenusType(auctionConstrainerEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> containing
     *  the given auction constrainer enabler record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  auctionConstrainerEnablerRecordType an auctionConstrainerEnabler record type 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.FederatingAuctionConstrainerEnablerList ret = getAuctionConstrainerEnablerList();

        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            ret.addAuctionConstrainerEnablerList(session.getAuctionConstrainerEnablersByRecordType(auctionConstrainerEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session.
     *  
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.FederatingAuctionConstrainerEnablerList ret = getAuctionConstrainerEnablerList();

        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            ret.addAuctionConstrainerEnablerList(session.getAuctionConstrainerEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>AuctionConstrainerEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                                             org.osid.calendaring.DateTime from,
                                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.FederatingAuctionConstrainerEnablerList ret = getAuctionConstrainerEnablerList();

        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            ret.addAuctionConstrainerEnablerList(session.getAuctionConstrainerEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AuctionConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known auction
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those auction constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction constrainer enablers are returned.
     *
     *  @return a list of <code>AuctionConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.FederatingAuctionConstrainerEnablerList ret = getAuctionConstrainerEnablerList();

        for (org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession session : getSessions()) {
            ret.addAuctionConstrainerEnablerList(session.getAuctionConstrainerEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.FederatingAuctionConstrainerEnablerList getAuctionConstrainerEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.ParallelAuctionConstrainerEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionconstrainerenabler.CompositeAuctionConstrainerEnablerList());
        }
    }
}
