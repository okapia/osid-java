//
// AbstractWarehouseSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.warehouse.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractWarehouseSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.inventory.WarehouseSearchResults {

    private org.osid.inventory.WarehouseList warehouses;
    private final org.osid.inventory.WarehouseQueryInspector inspector;
    private final java.util.Collection<org.osid.inventory.records.WarehouseSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractWarehouseSearchResults.
     *
     *  @param warehouses the result set
     *  @param warehouseQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>warehouses</code>
     *          or <code>warehouseQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractWarehouseSearchResults(org.osid.inventory.WarehouseList warehouses,
                                            org.osid.inventory.WarehouseQueryInspector warehouseQueryInspector) {
        nullarg(warehouses, "warehouses");
        nullarg(warehouseQueryInspector, "warehouse query inspectpr");

        this.warehouses = warehouses;
        this.inspector = warehouseQueryInspector;

        return;
    }


    /**
     *  Gets the warehouse list resulting from a search.
     *
     *  @return a warehouse list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.inventory.WarehouseList getWarehouses() {
        if (this.warehouses == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.inventory.WarehouseList warehouses = this.warehouses;
        this.warehouses = null;
	return (warehouses);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.inventory.WarehouseQueryInspector getWarehouseQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  warehouse search record <code> Type. </code> This method must
     *  be used to retrieve a warehouse implementing the requested
     *  record.
     *
     *  @param warehouseSearchRecordType a warehouse search 
     *         record type 
     *  @return the warehouse search
     *  @throws org.osid.NullArgumentException
     *          <code>warehouseSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(warehouseSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.records.WarehouseSearchResultsRecord getWarehouseSearchResultsRecord(org.osid.type.Type warehouseSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.inventory.records.WarehouseSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(warehouseSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(warehouseSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record warehouse search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addWarehouseRecord(org.osid.inventory.records.WarehouseSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "warehouse record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
