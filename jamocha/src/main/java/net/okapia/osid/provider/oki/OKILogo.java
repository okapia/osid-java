//
// OKILogo.java
//
//     Defines a logo for the O.K.I. provider.
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider.oki;

import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  Defines an asset for the O.K.I. provider.
 */

public final class OKILogo
    extends net.okapia.osid.provider.spi.AbstractLogo
    implements org.osid.repository.Asset {

    private static final org.osid.id.Id ID                        = BasicId.valueOf("repository.Asset.logos:O.K.I.@okapia.net");
    private static final org.osid.locale.DisplayText DISPLAY_NAME = Plain.valueOf("O.K.I.");
    private static final org.osid.locale.DisplayText DESCRIPTION  = Plain.valueOf("The logo for MIT's Open Knowledge Initiative.");
    private static final org.osid.locale.DisplayText TITLE        = Plain.valueOf("O.K.I. Logo");
    private static final org.osid.locale.DisplayText COPYRIGHT    = Plain.valueOf("Copyright &#169; 2001 Massachusetts Institute of Technology.");


    /**
     *  Creates a new <code>OKILogo</code>.
     *
     *  @throws org.osid.OsidRuntimeException
     */

    public OKILogo()
        throws org.osid.OsidRuntimeException {

        setId(ID);
        setDisplayName(DISPLAY_NAME);
        setDescription(DESCRIPTION);
        setTitle(TITLE);
        setPublicDomain(false);
        setCopyright(COPYRIGHT);
        setPublishedDate(GregorianUTCDateTime.valueOf("2001"));
        setDistributionRights(true, false, false);
        setProvider(net.okapia.osid.provider.Providers.OKI.getProvider());

        try {
            /*
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this, "O.K.I. Logo Small Image", "A small version of the O.K.I. logo.", "logos/oki/oki-small.png"));
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this, "O.K.I. Logo Medium Image", "A medium sized version of the O.K.I. logo.", "logos/oki/oki-medium.png"));
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this,  "O.K.I. Logo Large Image", "A larger version of the O.K.I. logo.", "logos/oki/oki-large.png"));
            */
        } catch (Exception e) {
            throw new org.osid.OsidRuntimeException("could not get OKI logo started", e);
        }
        
        return;
    }                    
}
