//
// AbstractProvider
//
//     A composite object that provides a simple place to define
//     a provider for use with OsidManagers.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider.spi;

import net.okapia.osid.primordium.type.BasicType;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This abstract class combines the <code>Resource</code> interface
 *  and other provider methods to supply the
 *  <code>AbstractOsidManager</code> with a provider identity.
 */

public class AbstractProvider 
    extends net.okapia.osid.jamocha.resource.resource.spi.AbstractResource
    implements org.osid.resource.Resource,
               net.okapia.osid.provider.Provider {

    private static final org.osid.type.Type GENUS_TYPE = BasicType.valueOf("resource:provider@okapia.net");

    private final java.util.Collection<org.osid.repository.Asset> assets = new java.util.ArrayList<>();
    private org.osid.locale.DisplayText license = Plain.valueOf("");


    /**
     *  Constructs a new <code>AbstractProvider</code>.
     */

    protected AbstractProvider() {
        setGenusType(GENUS_TYPE);
        return;
    }


    /**
     *  Gets a branding, such as an image or logo, expressed using the
     *  <code> Asset </code> interface.
     *
     *  @return a list of assets 
     */

    @Override    
    public java.util.Collection<org.osid.repository.Asset> getBranding() {
        return (java.util.Collections.unmodifiableCollection(this.assets));
    }


    /**
     *  Adds an asset to a branding list.
     *
     *  @param asset asset to add
     *  @throws org.osid.NullArgumentException <code>asset</code>
     *          is <code>null</code>
     */

    protected void addAssetToBranding(org.osid.repository.Asset asset) {
        nullarg(asset, "asset");
        this.assets.add(asset);
        return;
    }


    /**
     *  Gets the terms of usage with respect to this provider.
     *
     *  @return the license 
     */

    @Override
    public org.osid.locale.DisplayText getLicense() {
        return (this.license);
    }


    /**
     *  Sets the license for this service implementation.
     *
     *  @param license the new license
     *  @throws org.osid.NullArgumentexception <code>license</code>
     *          is <code>null</code>
     */

    protected void setLicense(org.osid.locale.DisplayText license) {
        nullarg(license, "license");
        this.license = license;
        return;
    }


    /**
     *  Gets the resource associated with this provider.
     *
     *  @return the resource
     */

    @Override
    public org.osid.resource.Resource getResource() {
        return (this);
    }


    /**
     *  Validates data.
     *
     *  @throws org.osid.NullReturnException a data element is null
     */

    @Override
    public void validate() {
        try {
            nullarg(getLicense(), "license");
            nullarg(getResource(), "resource");
        } catch (org.osid.NullArgumentException nae) {
            throw new org.osid.NullReturnException(nae.getMessage());
        }

        return;
    }
}
