//
// AbstractLogo
//
//     Defines a logo for a provider.
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider.spi;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  Defines a logo for a provider.
 */

public abstract class AbstractLogo
    extends net.okapia.osid.jamocha.repository.asset.spi.AbstractAsset
    implements org.osid.repository.Asset {
    
    private final static org.osid.type.Type GENUS_TYPE = net.okapia.osid.primordium.type.BasicType.valueOf("asset:logo@okapia.net");
    

    /**
     *  Creates an <code>AbstractLogo</code>.
     */

    protected AbstractLogo() {
        setDisplayName(Plain.valueOf("Abstract Logo"));
        setDescription(Plain.valueOf("A logo for something?"));
        setGenusType(GENUS_TYPE);
        
        return;
    }
}
