//
// ServiceProvider
//
//     An interface to capture a provider identity.
//
//
// Tom Coppeto
// Okapia
// 20 September 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider;


/**
 *  An interface to capture a provider identity.
 */

public interface ServiceProvider {


    /**
     *  Gets the Id of the service.
     *
     *  @return the service Id
     */

    public org.osid.id.Id getServiceId();


    /**
     *  Gets the name of the service.
     *
     *  @return the name
     */

    public org.osid.locale.DisplayText getServiceName();


    /**
     *  Gets the description of the service.
     *
     *  @return the description
     */

    public org.osid.locale.DisplayText getServiceDescription();


    /**
     *  Gets the implementation version of the service.
     *
     *  @return the version
     */

    public org.osid.installation.Version getImplementationVersion();

 
    /**
     *  Gets the Release date of the service.
     *
     *  @return the date
     */

    public org.osid.calendaring.DateTime getReleaseDate();


    /**
     *  Gets the Provider of the service.
     *
     *  @return the provider
     */

    public net.okapia.osid.provider.Provider getProvider();


    /**
     *  Validates the data.
     *
     *  @throws org.osid.NullReturnException data is null
     */

    public void validate();
}




    


