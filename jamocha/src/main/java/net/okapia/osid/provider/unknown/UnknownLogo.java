//
// UnkownLogo.java
//
//     Defines a logo for the Unknown provider.
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider.unknown;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.calendaring.GregorianUTCDateTime;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;


/**
 *  Defines an asset for the Unknown provider.
 */

public final class UnknownLogo
    extends net.okapia.osid.provider.spi.AbstractLogo
    implements org.osid.repository.Asset {

    private static final org.osid.id.Id ID                        = BasicId.valueOf("repository.Asset.logos:Unknown@okapia.net");
    private static final org.osid.locale.DisplayText DISPLAY_NAME = Plain.valueOf("The Unknown Provider");
    private static final org.osid.locale.DisplayText DESCRIPTION  = Plain.valueOf("A photograph of the Tomb of the Unknown Soldier in Arlington National Cemetery.");
    private static final org.osid.locale.DisplayText TITLE        = Plain.valueOf("Tomb of the Unknown Provider");
    

    /**
     *  Creates a new <code>UnkownLogo</code>.
     *
     *  @throws org.osid.OsidRuntimeException
     */

    public UnknownLogo()
        throws org.osid.OsidRuntimeException 
{
        setId(ID);
        setDisplayName(DISPLAY_NAME);
        setDescription(DESCRIPTION);
        setTitle(TITLE);
        setPublicDomain(true);
        setDistributionRights(true, true, true);
       
        setProvider(net.okapia.osid.provider.Providers.OKAPIA.getProvider());
        
        setSource((new net.okapia.osid.jamocha.builder.resource.resource.ResourceBuilder())
                  .id(new BasicId("okapia.net", "provider", "Library Of Congress"))
                  .displayName(Plain.valueOf("Library of Congress"))
                  .description(Plain.valueOf("The oldest federal cultural institution in the United States."))
                  .build());
        
        setCreatedDate(GregorianUTCDateTime.valueOf("1943-5"));
        setPrincipalCreditString(Plain.valueOf("John Collier"));

        try {
            /*
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this, "logos/unknown/unknown-1024.jpg"));
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this, "logos/unknown/unknown-512.jpg"));
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this, "logos/unknown/unknown-256.jpg"));
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this, "logos/unknown/unknown-128.jpg"));
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this, "logos/unknown/unknown-64.jpg"));
            addAssetContent(new org.osid.impl.repository.ImageURLAssetContent(this, "logos/unknown/unknown-32.jpg"));
            */
        } catch (Exception e) {
            System.err.println("cannot get logo " + DISPLAY_NAME + " " + e.getMessage());
            throw new org.osid.OsidRuntimeException("could not get Unkown logo started", e);
        }

        return;
    }                    
}
