//
// Providers
//
//     A utility class for stashing provider entities.
//
//
// Tom Coppeto
// Okapia
// 20 September 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider;


/**
 *  A factory for accessing built-in invariant provider entities.
 */

public enum Providers {

    UNKNOWN(new net.okapia.osid.provider.unknown.UnknownProvider()),
    OKAPIA(new net.okapia.osid.provider.oki.OKIProvider()),
    OKI(new net.okapia.osid.provider.oki.OKIProvider());

    private Provider provider;

    
    Providers(Provider provider) {
        this.provider = provider;
        return;
    }


    public Provider getProvider() {     
        return (this.provider);
    }
}




    


