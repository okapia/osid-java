//
// AbstractProvider
//
//     A composite object that provides a simple place to define
//     a provider for use with OsidManagers.
//
//
// Tom Coppeto
// Okapia
// 20 September 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This abstract class combines the {@code Resource} interface and
 *  other provider methods to supply the {@code AbstractOsidManager}
 *  with a provider identity.
 */

public class AbstractServiceProvider
    implements net.okapia.osid.provider.ServiceProvider {

    private org.osid.id.Id id;
    private org.osid.locale.DisplayText name;
    private org.osid.locale.DisplayText description;
    private org.osid.installation.Version implVersion;
    private org.osid.calendaring.DateTime releaseDate;
    private net.okapia.osid.provider.Provider provider;


    /**
     *  Constructs a new {@code AbstractServiceProvider}.
     */

    protected AbstractServiceProvider() {
        return;
    }


    /**
     *  Gets the Id of the service.
     *
     *  @return the service Id
     */

    @Override
    public org.osid.id.Id getServiceId() {
        return (this.id);
    }


    /**
     *  Sets the Id for this service implementation.
     *
     *  @param id the service Id
     *  @throws org.osid.NullArgumentException {@code id} is {@code
     *          null}
     */

    protected void setServiceId(org.osid.id.Id id) {
        nullarg(id, "service Id");
        this.id = id;
        return;
    }


    /**
     *  Sets the Id for this service implementation using a URN
     *  format.
     *
     *  urn:inet:<authority>:identifiers:providers:<osid>:<name>
     *
     *  @param id the service Id
     *  @throws org.osid.InvalidArgumentException cannot parse {@code id}
     *  @throws org.osid.NullArgumentException {@code id} is {@code
     *          null}
     */

    protected void setServiceId(String id) {
        this.id = net.okapia.osid.primordium.id.URNId.valueOf(id);
        return;
    }


    /**
     *  Gets the name of the service.
     *
     *  @return the name
     */

    @Override
    public org.osid.locale.DisplayText getServiceName() {
        return (this.name);
    }


    /**
     *  Sets the name this service implementation.
     *
     *  @param name the service name
     *  @throws org.osid.NullArgumentException {@code name} is {@code
     *          null}
     */

    protected void setServiceName(org.osid.locale.DisplayText name) {
        nullarg(name, "name");
        this.name = name;
        return;
    }


    /**
     *  Gets the description of the service.
     *
     *  @return the description
     */

    @Override
    public org.osid.locale.DisplayText getServiceDescription() {
        return (this.description);
    }


    /**
     *  Sets the description this service implementation.
     *
     *  @param description the service description
     *  @throws org.osid.NullArgumentException {@code description} is
     *          {@code null}
     */

    protected void setServiceDescription(org.osid.locale.DisplayText description) {
        nullarg(description, "description");
        this.description = description;
        return;
    }


    /**
     *  Gets the implementation version of the service.
     *
     *  @return the version
     */

    @Override
    public org.osid.installation.Version getImplementationVersion() {
        return (this.implVersion);
    }


    /**
     *  Sets the version of this service implementation.
     *
     *  @param version the implementation version
     *  @throws org.osid.NullArgumentException {@code version} is
     *          {@code null}
     */

    protected void setImplementationVersion(org.osid.installation.Version version) {
        nullarg(version, "implementation version");
        this.implVersion = version;
        return;
    }


    /**
     *  Sets the version this service implementation in X.Y.Z format.
     *
     *  @param version the implementation version
     *  @throws org.osid.InvalidArgumentException cannot parse {@code version} 
     *  @throws org.osid.NullArgumentException {@code version} is
     *          {@code null}
     */

    protected void setImplementationVersion(String version) {
        this.implVersion = net.okapia.osid.primordium.installation.SoftwareVersion.valueOf(version);
        return;
    }

 
    /**
     *  Gets the release date of the service.
     *
     *  @return the date
     */

    @Override
    public org.osid.calendaring.DateTime getReleaseDate() {
        return (this.releaseDate);
    }


    /**
     *  Sets the release date this service implementation.
     *
     *  @param date the release date
     *  @throws org.release.NullArgumentException {@code date} is
     *          {@code null}
     */

    protected void setReleaseDate(org.osid.calendaring.DateTime date) {
        nullarg(releaseDate, "release date");
        this.releaseDate = date;
        return;
    }


    /**
     *  Sets the Release date this service implementation in ISO date
     *  format.
     *
     *  YYYY-MM-DD
     *  YYY-MM-DDThh:mm
     *
     *  @param date the Release date
     *  @throws org.release.InvalidArgumentException cannot parse
     *          {@code date}
     *  @throws org.release.NullArgumentException {@code date} is
     *          {@code null}
     */

    protected void setReleaseDate(String date) {
        this.releaseDate = net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.valueOf(date);
        return;
    }


    /**
     *  Gets the Provider of the service.
     *
     *  @return the provider
     */

    @Override
    public net.okapia.osid.provider.Provider getProvider() {
        return (this.provider);
    }


    /**
     *  Sets the Provider of this service.
     *
     *  @param provider the provider
     *  @throws org.release.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected void setProvider(net.okapia.osid.provider.Provider provider) {
        nullarg(provider, "provider");
        this.provider = provider;
        return;
    }


    /**
     *  Validates data.
     *
     *  @throws org.osid.NullReturnException a data element is null
     */

    @Override
    public void validate() {
        try {
            nullarg(getServiceId(), "service Id");
            nullarg(getServiceName(), "service display name");
            nullarg(getServiceDescription(), "service description");
            nullarg(getImplementationVersion(), "implementation version");
            nullarg(getReleaseDate(), "release date");
        } catch (org.osid.NullArgumentException nae) {
            throw new org.osid.NullReturnException(nae.getMessage());
        }

        getProvider().validate();
        return;
    }
}