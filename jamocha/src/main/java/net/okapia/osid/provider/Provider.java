//
// Provider
//
//     An interface to capture a provider identity.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider;


/**
 *  An interface to capture a provider identity.
 */

public interface Provider  {

    /**
     *  Gets a branding, such as an image or logo, expressed using the <code> 
     *  Asset </code> interface. 
     *
     *  @return a list of assets 
     */
    
    public java.util.Collection<org.osid.repository.Asset> getBranding();


    /**
     *  Gets the terms of usage with respect to this provider.
     *
     *  @return the license 
     */

    public org.osid.locale.DisplayText getLicense();


    /**
     *  Gets the resource associated with this provider.
     *
     *  @return the resource
     */

    public org.osid.resource.Resource getResource();


    /**
     *  Validates the data.
     *
     *  @throws org.osid.NullReturnException data is null
     */

    public void validate();
}




    


