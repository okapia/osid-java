//
// OKIProvider
//
//     Defines a Provider for O.K.I.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.provider.oki;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.id.BasicId;
import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import net.okapia.osid.primordium.locale.text.eng.us.Html;


/**
 *  Defines a <code>Provider</code> for O.K.I.
 */

public final class OKIProvider 
    extends net.okapia.osid.provider.spi.AbstractProvider
    implements org.osid.resource.Resource {

    private static final org.osid.id.Id ID = BasicId.valueOf("provider:O.K.I.@okapia.net");

    private static final org.osid.locale.DisplayText DISPLAY_NAME = Plain.valueOf("O.K.I.");
    private static final org.osid.locale.DisplayText DESCRIPTION  = Html.valueOf("<p>The Open Knowledge Initiative (O.K.I.) develops and promotes specifications that describe how the components of a software environment communicate with each other and with other enterprise systems. O.K.I. specifications enable sustainable interoperability and integration by defining standards for Service Oriented Architecture (SOA). Through this work O.K.I. seeks to open new market opportunities across a wide range of software application domains.</p>");

    //private static final org.osid.repository.Asset LOGO = new OKILogo();

    private static final org.osid.locale.DisplayText LICENSE = Html.valueOf("<p>This implementation (\"Work\") and the information contained herein is provided on an \"AS IS\" basis. The Massachusetts Institute of Technology, the Open Knowledge Initiative, and THE AUTHORS DISCALIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OF IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS IN THE WORK.</p><p>Permission to use, copy, modify, adapt and distribute this Work, for any purpose, without fee or royalty is hereby granted, provided that you include the above copyright notice and the terms of this license on ALL copies of the Work of portions thereof.</p><p>The export of software employing encryption technology may require a specific license from the United States Government. It is the responsibility of any person or organization contemplating export to obtain such a license before exporting this Work.</p>");


    /**
     *  Constructs a new <code>OKIProvider</code>.
     *
     *  @throws org.osid.ConfigurationErrorException
     */

    public OKIProvider() {
        setId(ID);
        setDisplayName(DISPLAY_NAME);
        setDescription(DESCRIPTION);
        setLicense(LICENSE);
        //        addAssetBrand(LOGO);

        return;
    }


    /**
     *  This information doesn't change.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean isCurrent() {
        return (true);
    }
}




    


