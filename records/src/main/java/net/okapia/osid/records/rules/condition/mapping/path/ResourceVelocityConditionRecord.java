//
// ResourceVelocityConditionRecord.java
//
//     A condition for passing ResourceVelocities to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.mapping.path;


/**
 *  A condition for passing ResourceVelocities to a Rules service.
 */

public interface ResourceVelocityConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:mapping:path:ResourceVelocity");

    
    /**
     *  Adds a ResourceVelocity to this condition.
     *
     *  @param resourceVelocity a resource velocity
     *  @throws org.osid.NullArgumentException <code>resourceVelocity</code>
     *          is <code>null</code>
     */

    public void addResourceVelocity(org.osid.mapping.path.ResourceVelocity resourceVelocity);


    /**
     *  Adds a list of resource velocities to this condition.
     *
     *  @param resourceVelocities a list of resource velocities
     *  @throws org.osid.NullArgumentException <code>resourceVelocities</code>
     *          is <code>null</code>
     */

    public void addResourceVelocityList(org.osid.mapping.path.ResourceVelocityList resourceVelocities);
}
