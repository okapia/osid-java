//
// OfferingConstrainerEnablerConditionRecord.java
//
//     A condition for passing OfferingConstrainerEnablers to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.offering.rules;


/**
 *  A condition for passing OfferingConstrainerEnablers to a Rules service.
 */

public interface OfferingConstrainerEnablerConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:offering:rules:OfferingConstrainerEnabler");

    
    /**
     *  Adds an OfferingConstrainerEnabler to this condition.
     *
     *  @param offeringConstrainerEnabler an offering constrainer enabler
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnabler</code>
     *          is <code>null</code>
     */

    public void addOfferingConstrainerEnabler(org.osid.offering.rules.OfferingConstrainerEnabler offeringConstrainerEnabler);


    /**
     *  Adds a list of offering constrainer enablers to this condition.
     *
     *  @param offeringConstrainerEnablers a list of offering constrainer enablers
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablers</code>
     *          is <code>null</code>
     */

    public void addOfferingConstrainerEnablerList(org.osid.offering.rules.OfferingConstrainerEnablerList offeringConstrainerEnablers);
}
