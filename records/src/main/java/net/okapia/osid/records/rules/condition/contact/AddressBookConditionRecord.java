//
// AddressBookConditionRecord.java
//
//     A condition for passing AddressBooks to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.contact;


/**
 *  A condition for passing AddressBooks to a Rules service.
 */

public interface AddressBookConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:contact:AddressBook");

    
    /**
     *  Adds an AddressBook to this condition.
     *
     *  @param addressBook an address book
     *  @throws org.osid.NullArgumentException <code>addressBook</code>
     *          is <code>null</code>
     */

    public void addAddressBook(org.osid.contact.AddressBook addressBook);


    /**
     *  Adds a list of address books to this condition.
     *
     *  @param addressBooks a list of address books
     *  @throws org.osid.NullArgumentException <code>addressBooks</code>
     *          is <code>null</code>
     */

    public void addAddressBookList(org.osid.contact.AddressBookList addressBooks);
}
