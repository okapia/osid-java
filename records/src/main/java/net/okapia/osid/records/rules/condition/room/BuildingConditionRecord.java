//
// BuildingConditionRecord.java
//
//     A condition for passing Buildings to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.room;


/**
 *  A condition for passing Buildings to a Rules service.
 */

public interface BuildingConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:room:Building");

    
    /**
     *  Adds a Building to this condition.
     *
     *  @param building a building
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    public void addBuilding(org.osid.room.Building building);


    /**
     *  Adds a list of buildings to this condition.
     *
     *  @param buildings a list of buildings
     *  @throws org.osid.NullArgumentException <code>buildings</code>
     *          is <code>null</code>
     */

    public void addBuildingList(org.osid.room.BuildingList buildings);
}
