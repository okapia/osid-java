//
// GradebookColumnCalculationConditionRecord.java
//
//     A condition for passing GradebookColumnCalculations to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.grading.calculation;


/**
 *  A condition for passing GradebookColumnCalculations to a Rules service.
 */

public interface GradebookColumnCalculationConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:grading:calculation:GradebookColumnCalculation");

    
    /**
     *  Adds a GradebookColumnCalculation to this condition.
     *
     *  @param gradebookColumnCalculation a gradebook column calculation
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculation</code>
     *          is <code>null</code>
     */

    public void addGradebookColumnCalculation(org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation);


    /**
     *  Adds a list of gradebook column calculations to this condition.
     *
     *  @param gradebookColumnCalculations a list of gradebook column calculations
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculations</code>
     *          is <code>null</code>
     */

    public void addGradebookColumnCalculationList(org.osid.grading.calculation.GradebookColumnCalculationList gradebookColumnCalculations);
}
