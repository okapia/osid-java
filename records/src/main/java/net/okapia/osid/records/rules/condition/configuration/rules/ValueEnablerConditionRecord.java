//
// ValueEnablerConditionRecord.java
//
//     A condition for passing ValueEnablers to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.configuration.rules;


/**
 *  A condition for passing ValueEnablers to a Rules service.
 */

public interface ValueEnablerConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:configuration:rules:ValueEnabler");

    
    /**
     *  Adds a ValueEnabler to this condition.
     *
     *  @param valueEnabler a value enabler
     *  @throws org.osid.NullArgumentException <code>valueEnabler</code>
     *          is <code>null</code>
     */

    public void addValueEnabler(org.osid.configuration.rules.ValueEnabler valueEnabler);


    /**
     *  Adds a list of value enablers to this condition.
     *
     *  @param valueEnablers a list of value enablers
     *  @throws org.osid.NullArgumentException <code>valueEnablers</code>
     *          is <code>null</code>
     */

    public void addValueEnablerList(org.osid.configuration.rules.ValueEnablerList valueEnablers);
}
