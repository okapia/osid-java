//
// AssessmentSectionConditionRecord.java
//
//     A condition for passing AssessmentSections to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.assessment;


/**
 *  A condition for passing AssessmentSections to a Rules service.
 */

public interface AssessmentSectionConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:assessment:AssessmentSection");

    
    /**
     *  Adds an AssessmentSection to this condition.
     *
     *  @param assessmentSection an assessment section
     *  @throws org.osid.NullArgumentException <code>assessmentSection</code>
     *          is <code>null</code>
     */

    public void addAssessmentSection(org.osid.assessment.AssessmentSection assessmentSection);


    /**
     *  Adds a list of assessment sections to this condition.
     *
     *  @param assessmentSections a list of assessment sections
     *  @throws org.osid.NullArgumentException <code>assessmentSections</code>
     *          is <code>null</code>
     */

    public void addAssessmentSectionList(org.osid.assessment.AssessmentSectionList assessmentSections);
}
