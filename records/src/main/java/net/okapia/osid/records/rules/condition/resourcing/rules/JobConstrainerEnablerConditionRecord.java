//
// JobConstrainerEnablerConditionRecord.java
//
//     A condition for passing JobConstrainerEnablers to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.resourcing.rules;


/**
 *  A condition for passing JobConstrainerEnablers to a Rules service.
 */

public interface JobConstrainerEnablerConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:resourcing:rules:JobConstrainerEnabler");

    
    /**
     *  Adds a JobConstrainerEnabler to this condition.
     *
     *  @param jobConstrainerEnabler a job constrainer enabler
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnabler</code>
     *          is <code>null</code>
     */

    public void addJobConstrainerEnabler(org.osid.resourcing.rules.JobConstrainerEnabler jobConstrainerEnabler);


    /**
     *  Adds a list of job constrainer enablers to this condition.
     *
     *  @param jobConstrainerEnablers a list of job constrainer enablers
     *  @throws org.osid.NullArgumentException <code>jobConstrainerEnablers</code>
     *          is <code>null</code>
     */

    public void addJobConstrainerEnablerList(org.osid.resourcing.rules.JobConstrainerEnablerList jobConstrainerEnablers);
}
