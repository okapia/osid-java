//
// ActivityBundleConditionRecord.java
//
//     A condition for passing ActivityBundles to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.course.registration;


/**
 *  A condition for passing ActivityBundles to a Rules service.
 */

public interface ActivityBundleConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:course:registration:ActivityBundle");

    
    /**
     *  Adds an ActivityBundle to this condition.
     *
     *  @param activityBundle an activity bundle
     *  @throws org.osid.NullArgumentException <code>activityBundle</code>
     *          is <code>null</code>
     */

    public void addActivityBundle(org.osid.course.registration.ActivityBundle activityBundle);


    /**
     *  Adds a list of activity bundles to this condition.
     *
     *  @param activityBundles a list of activity bundles
     *  @throws org.osid.NullArgumentException <code>activityBundles</code>
     *          is <code>null</code>
     */

    public void addActivityBundleList(org.osid.course.registration.ActivityBundleList activityBundles);
}
