//
// ItemConditionRecord.java
//
//     A condition for passing Items to a Rules service.
//
//
// Tom Coppeto
// Okapia
// 5 March 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.records.rules.condition.billing;


/**
 *  A condition for passing Items to a Rules service.
 */

public interface ItemConditionRecord
    extends org.osid.rules.records.ConditionRecord {

    public org.osid.type.Type TYPE = net.okapia.osid.primordium.type.URNType.valueOf("urn:osid:okapia.net:types:records:rules:Condition:billing:Item");

    
    /**
     *  Adds an Item to this condition.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException <code>item</code>
     *          is <code>null</code>
     */

    public void addItem(org.osid.billing.Item item);


    /**
     *  Adds a list of items to this condition.
     *
     *  @param items a list of items
     *  @throws org.osid.NullArgumentException <code>items</code>
     *          is <code>null</code>
     */

    public void addItemList(org.osid.billing.ItemList items);
}
